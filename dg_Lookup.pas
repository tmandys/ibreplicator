unit dg_Lookup;

interface

uses Windows, SysUtils, Classes, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, IBSQL, IBReplicator, IBDatabase, ComCtrls;

type
  TLookupDlg = class(TForm)
    OKBtn: TButton;
    Button2: TButton;
    ListBox: TListView;
    procedure ListBoxDblClick(Sender: TObject);
  private
    procedure SetSelected(const aSelected: string);
    function GetSelected: string;
    { Private declarations }
  public
    property Selected: string read GetSelected write SetSelected;
    procedure AssignSQL(aMultiSelect: Boolean; aSelectSQL: string; const aCaptions: array of string);
  end;

var
  LookupDlg: TLookupDlg;

implementation
uses
  dm_IBRepl;

{$IFDEF LINUX}
{$R *.xfm}
{$ELSE}
{$R *.dfm}
{$ENDIF}

procedure TLookupDlg.ListBoxDblClick(Sender: TObject);
begin
  if ListBox.Selected <> nil then
    if ListBox.Checkboxes then
      ListBox.Selected.Checked:= not ListBox.Selected.Checked
    else
      OkBtn.Click;
end;

procedure TLookupDlg.AssignSQL;
var
  Q: TIBSQL;
  SaveConnected: Boolean;
  I, J: Integer;
  S: string;
  LI: TListItem;
  LC: TListColumn;
begin
  ListBox.Items.Clear;
  repeat
    I:= Pos('/*PREFIX*/', aSelectSQL);
    if I > 0 then
    begin
      Delete(aSelectSQL, I, Length('/*PREFIX*/'));
      J:= I;
      while (J <= Length(aSelectSQL)) and (aSelectSQL[J] > ' ') do
        Inc(J);
      S:= TIBReplicator.FormatIdentifier(IBReplDataModule.IBReplicator.ConfigDatabase.SQLDialect, IBReplDataModule.IBReplicator.ConfigDatabasePrefix+Copy(aSelectSQL, I, J-I));
      Delete(aSelectSQL, I, J-I);
      Insert(S, aSelectSQL, I);
    end;
  until I = 0;

  ListBox.Columns.Clear;
  for I:= 0 to Length(aCaptions)-1 do
  begin
    LC:= ListBox.Columns.Add;
    LC.Width:= 70;
    LC.Caption:= aCaptions[I];
    LC.AutoSize:= Pos('Id', aCaptions[I]) = 0;
  end;

  Q:= TIBSQL.Create(nil);
  try
    Q.Database:= IBReplDataModule.IBReplicator.ConfigDatabase;
    Q.Transaction:= TIBTransaction.Create(Q);
    Q.Transaction.DefaultDatabase:= Q.Database;
    Q.Transaction.Params.Text:= tranRO;
    Q.SQL.Text:= aSelectSQL;
    SaveConnected:= Q.Database.Connected;
    Q.Database.Open;
    try
      Q.Transaction.StartTransaction;
      try
        Q.ExecQuery;
        while not Q.EOF do
        begin
          LI:= ListBox.Items.Add;
          LI.Caption:= Q.FieldByName('ID').asString;
          while LI.SubItems.Count <= Length(aCaptions)-1 do
            LI.SubItems.Add('');

          for I:= 1 to Length(aCaptions)-1 do
          begin
            LI.SubItems[I-1]:= Q.FieldByName('NAME_'+IntToStr(I-1)).asString;
          end;
          Q.Next;
        end;
      finally
        Q.Transaction.Commit;
      end;
    finally
      Q.Database.Connected:= SaveConnected;
    end;
  finally
    Q.Free;
  end;
  ListBox.Checkboxes:= aMultiSelect;
end;

procedure TLookupDlg.SetSelected;
var
  I, J: Integer;
  Arr: TintegerOpenArray;
  F: Boolean;
begin
  Arr:= StringArrayToIntegerArray(StringToArray(aSelected, ','));
  for J:= 0 to ListBox.Items.Count-1 do
    ListBox.Items[J].Checked:= False;
  for I:= 0 to Length(Arr)-1 do
  begin
    for J:= 0 to ListBox.Items.Count-1 do
    begin
      F:= StrToInt(ListBox.Items[J].Caption) = Arr[I];
      if not ListBox.Checkboxes then
        begin
          if F then
          begin
            ListBox.Selected:= ListBox.Items[J];
            Exit;
          end;
        end
      else
        begin
          if F then
            ListBox.Items[J].Checked:= F;
        end;
    end;
  end;
end;

function TLookupDlg.GetSelected;
var
  I: Integer;
begin
  Result:= '';
  if ListBox.Checkboxes then
    begin
      for I:= 0 to ListBox.Items.Count-1 do
        if ListBox.Items[I].Checked and (Pos(','+ListBox.Items[I].Caption+',', ','+Result+',') = 0) then
        begin
          if Result <> '' then
            Result:= Result+',';
          Result:= Result+ListBox.Items[I].Caption;
        end;
    end
  else
    if ListBox.Selected <> nil then
      Result:= ListBox.Selected.Caption;
end;

end.
