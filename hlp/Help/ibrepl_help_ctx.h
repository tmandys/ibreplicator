#define faq 45

#define ibrepl_contents 0

#define ibreplserver 20000
#define registration 20001

#define ibreplserver_logs 21000

#define ibreplserver_setup 21100
#define ibreplserver_setup_configdb 21101
#define ibreplserver_setup_appear 21102
#define ibreplserver_setup_log 21103
#define ibreplserver_setup_repl 21104
#define ibreplserver_setup_schedule 21105

#define ibreplserver_task 21200
#define ibreplserver_task_common 21201
#define ibreplserver_task_general 21202
#define ibreplserver_task_offline 21203
#define ibreplserver_task_log 21204
#define ibreplserver_task_schedule 21205

#define ibreplserver_menu 22000
/* #define ibreplserver_menu_file 22000 
#define ibreplserver_menu_edit 22100 */

#define ibreplserver_environment 23000

#define ibreplman 30000
#define ibreplman_log 30100
#define ibreplman_editor 31000
#define ibreplman_databases 31100
#define ibreplman_prop_database 31110
#define ibreplman_schema 31200
#define ibreplman_prop_schema 31210
#define ibreplman_prop_srcdb 31220
#define ibreplman_groups 31300
#define ibreplman_groups2 31400
#define ibreplman_prop_tgtdb 31410
#define ibreplman_relations 31500
#define ibreplman_prop_relation 31510
#define ibreplman_prop_relation_actions 31520
#define ibreplman_fields 31600
#define ibreplman_prop_field 31610
#define ibreplman_prop_field_conflict 31620
#define ibreplman_statistics 30200

#define ibreplman_setup 32000
#define ibreplman_setup_configdb 32001
#define ibreplman_setup_appear 32002
#define ibreplman_setup_log 32003
#define ibreplman_setup_repl 32004

#define ibreplman_copyfrom 30300
#define ibreplman_sort 30400
