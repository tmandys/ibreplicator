inherited SchemaTargetDialog: TSchemaTargetDialog
  Left = 292
  Top = 117
  HelpContext = 31410
  Caption = 'Target database properties'
  ClientHeight = 694
  ClientWidth = 529
  OnCreate = FormCreate
  PixelsPerInch = 120
  TextHeight = 16
  inherited Panel1: TPanel
    Left = 421
    Height = 694
  end
  inherited Panel2: TPanel
    Width = 421
    Height = 694
    object Label1: TLabel
      Left = 20
      Top = 20
      Width = 63
      Height = 16
      Caption = 'Schema&ID'
      FocusControl = DBEdit1
    end
    object Label5: TLabel
      Left = 20
      Top = 138
      Width = 58
      Height = 16
      Caption = 'Repl &user'
      FocusControl = DBEdit4
    end
    object Label6: TLabel
      Left = 148
      Top = 138
      Width = 91
      Height = 16
      Caption = 'Repl &password'
      FocusControl = DBEdit5
    end
    object Label7: TLabel
      Left = 284
      Top = 138
      Width = 55
      Height = 16
      Caption = 'Repl &role'
      FocusControl = DBEdit6
    end
    object Label10: TLabel
      Left = 20
      Top = 236
      Width = 57
      Height = 16
      Caption = 'Co&mment'
      FocusControl = DBMemo2
    end
    object Label2: TLabel
      Left = 20
      Top = 79
      Width = 101
      Height = 16
      Caption = '&Target database'
      FocusControl = DBLookupComboBox1
    end
    object Label3: TLabel
      Left = 284
      Top = 79
      Width = 55
      Height = 16
      Caption = 'DB Mas&k'
      FocusControl = DBComboBox3
    end
    object Label9: TLabel
      Left = 116
      Top = 20
      Width = 53
      Height = 16
      Caption = '&Group ID'
      FocusControl = GroupId
    end
    object Label13: TLabel
      Left = 286
      Top = 186
      Width = 60
      Height = 16
      Caption = '&Separator'
      FocusControl = DBEdit7
    end
    object DBEdit1: TDBEdit
      Left = 20
      Top = 39
      Width = 70
      Height = 24
      TabStop = False
      DataField = 'SCHEMAID'
      DataSource = DataSource1
      ParentColor = True
      ReadOnly = True
      TabOrder = 0
    end
    object DBMemo2: TDBMemo
      Left = 20
      Top = 256
      Width = 389
      Height = 110
      DataField = 'COMMENT'
      DataSource = DataSource1
      ScrollBars = ssVertical
      TabOrder = 10
    end
    object DBEdit4: TDBEdit
      Left = 20
      Top = 158
      Width = 119
      Height = 24
      CharCase = ecUpperCase
      DataField = 'REPLUSER'
      DataSource = DataSource1
      TabOrder = 4
    end
    object DBEdit5: TDBEdit
      Left = 148
      Top = 158
      Width = 102
      Height = 24
      DataField = 'REPLPSW'
      DataSource = DataSource1
      TabOrder = 5
    end
    object DBEdit6: TDBEdit
      Left = 284
      Top = 158
      Width = 119
      Height = 24
      DataField = 'REPLROLE'
      DataSource = DataSource1
      TabOrder = 7
    end
    object DBCheckBox3: TDBCheckBox
      Left = 20
      Top = 187
      Width = 131
      Height = 21
      Caption = '&Disabled'
      DataField = 'DISABLED'
      DataSource = DataSource1
      TabOrder = 9
      ValueChecked = 'Y'
      ValueUnchecked = 'N'
    end
    object DBLookupComboBox1: TDBLookupComboBox
      Left = 20
      Top = 98
      Width = 247
      Height = 24
      DataField = 'DBID'
      DataSource = DataSource1
      DropDownRows = 10
      KeyField = 'DBID'
      ListField = 'NAME'
      ListSource = DataSource2
      TabOrder = 2
      OnDropDown = DBLookupComboBox1DropDown
    end
    object GroupBox1: TGroupBox
      Left = 10
      Top = 384
      Width = 405
      Height = 297
      Caption = 'Offline'
      TabOrder = 11
      object Label4: TLabel
        Left = 20
        Top = 30
        Width = 114
        Height = 16
        Caption = '&Alternate database'
        FocusControl = RxDBComboBox1
      end
      object Label8: TLabel
        Left = 20
        Top = 89
        Width = 90
        Height = 16
        Caption = 'Transfer &library'
        FocusControl = DBComboBox1
      end
      object Label11: TLabel
        Left = 167
        Top = 89
        Width = 66
        Height = 16
        Caption = 'En&coder(s)'
        FocusControl = DBComboBox2
      end
      object Label12: TLabel
        Left = 20
        Top = 148
        Width = 70
        Height = 16
        Caption = 'Param&eters'
        FocusControl = DBMemo1
      end
      object DBComboBox1: TDBComboBox
        Left = 20
        Top = 108
        Width = 139
        Height = 24
        DataField = 'OFFTRANSFER'
        DataSource = DataSource1
        ItemHeight = 16
        Items.Strings = (
          'email'
          'ftp'
          'netdir'
          '')
        TabOrder = 1
      end
      object DBMemo1: TDBMemo
        Left = 20
        Top = 167
        Width = 365
        Height = 110
        DataField = 'OFFPARAMS'
        DataSource = DataSource1
        ScrollBars = ssVertical
        TabOrder = 3
        WordWrap = False
      end
      object DBComboBox2: TDBComboBox
        Left = 167
        Top = 108
        Width = 218
        Height = 24
        DataField = 'OFFENCODER'
        DataSource = DataSource1
        ItemHeight = 16
        Items.Strings = (
          'cmdline')
        TabOrder = 2
      end
      object RxDBComboBox1: TRxDBComboBox
        Left = 20
        Top = 49
        Width = 365
        Height = 24
        Style = csDropDownList
        DataField = 'OFFDBID'
        DataSource = DataSource1
        EnableValues = True
        ItemHeight = 16
        TabOrder = 0
      end
      object Button1: TButton
        Left = 364
        Top = 145
        Width = 20
        Height = 24
        Caption = '*'
        TabOrder = 4
        OnClick = Button1Click
      end
    end
    object GroupId: TDBEdit
      Left = 118
      Top = 39
      Width = 67
      Height = 24
      DataField = 'GROUPID'
      DataSource = DataSource1
      TabOrder = 1
    end
    object DBComboBox3: TDBComboBox
      Left = 284
      Top = 97
      Width = 119
      Height = 24
      Style = csDropDownList
      DataField = 'DBMASK'
      DataSource = DataSource1
      ItemHeight = 16
      TabOrder = 3
    end
    object Button2: TButton
      Left = 249
      Top = 159
      Width = 17
      Height = 24
      Caption = '*'
      TabOrder = 6
      OnClick = Button2Click
    end
    object DBEdit7: TDBEdit
      Left = 286
      Top = 206
      Width = 115
      Height = 24
      DataField = 'SEPARATOR'
      DataSource = DataSource1
      TabOrder = 8
    end
  end
  inherited Table: TIBDataSet
    AfterOpen = TableAfterOpen
    SelectSQL.Strings = (
      'SELECT * FROM'
      'SCHEMADB'
      '')
    Left = 93
    Top = 32
    object TableSCHEMAID: TIntegerField
      FieldName = 'SCHEMAID'
      Required = True
    end
    object TableGROUPID: TIntegerField
      FieldName = 'GROUPID'
      Required = True
    end
    object TableDBID: TIntegerField
      FieldName = 'DBID'
      Required = True
    end
    object TableDBMASK: TIntegerField
      FieldName = 'DBMASK'
      Required = True
      OnGetText = TableDBMASKGetText
      OnSetText = TableDBMASKSetText
    end
    object TableDISABLED: TIBStringField
      FieldName = 'DISABLED'
      Size = 1
    end
    object TableREPLUSER: TIBStringField
      FieldName = 'REPLUSER'
      Size = 31
    end
    object TableREPLROLE: TIBStringField
      FieldName = 'REPLROLE'
      Size = 50
    end
    object TableREPLPSW: TIBStringField
      FieldName = 'REPLPSW'
      Size = 30
    end
    object TableSEPARATOR: TIntegerField
      FieldName = 'SEPARATOR'
      Required = True
    end
    object TableCOMMENT: TMemoField
      FieldName = 'COMMENT'
      BlobType = ftMemo
      Size = 8
    end
    object TableOFFTRANSFER: TIBStringField
      FieldName = 'OFFTRANSFER'
      Size = 10
    end
    object TableOFFPARAMS: TMemoField
      FieldName = 'OFFPARAMS'
      BlobType = ftMemo
      Size = 8
    end
    object TableOFFENCODER: TIBStringField
      FieldName = 'OFFENCODER'
      Size = 50
    end
    object TableOFFDBID: TIntegerField
      FieldName = 'OFFDBID'
    end
  end
  inherited DataSource1: TDataSource
    OnDataChange = DataSource1DataChange
    Left = 112
    Top = 32
  end
  object IBQuery1: TIBQuery
    Database = IBReplDataModule.IBCfgDB
    Transaction = IBReplDataModule.IBCfgTransaction
    AfterOpen = IBQuery1AfterOpen
    BeforeOpen = IBQuery1BeforeOpen
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'SELECT * FROM '
      'DATABASES'
      'ORDER BY NAME')
    Left = 168
    Top = 16
  end
  object DataSource2: TDataSource
    DataSet = IBQuery1
    Left = 184
    Top = 32
  end
end
