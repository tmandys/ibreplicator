object SchemaRelationsSyncActionsDialog: TSchemaRelationsSyncActionsDialog
  Left = 375
  Top = 141
  HelpContext = 31520
  BorderStyle = bsDialog
  Caption = 'Sync actions designer'
  ClientHeight = 624
  ClientWidth = 482
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object Panel2: TPanel
    Left = 0
    Top = 521
    Width = 482
    Height = 200
    Align = alTop
    BevelOuter = bvNone
    Caption = ';;;;;;;;'
    TabOrder = 4
    object Label7: TLabel
      Left = 10
      Top = 0
      Width = 176
      Height = 16
      Caption = 'Conditional &defines (WHERE)'
      FocusControl = Condition
    end
    object Condition: TMemo
      Left = 9
      Top = 18
      Width = 464
      Height = 127
      ScrollBars = ssVertical
      TabOrder = 0
      WordWrap = False
      OnExit = ConditionExit
    end
  end
  object Notebook1: TPanel
    Left = 0
    Top = 398
    Width = 482
    Height = 123
    Align = alTop
    BevelOuter = bvNone
    Caption = ' '
    TabOrder = 3
    Visible = False
    object Label3: TLabel
      Left = 10
      Top = 20
      Width = 23
      Height = 16
      Caption = '&Log'
      FocusControl = ComboBox2
    end
    object Label5: TLabel
      Left = 11
      Top = 72
      Width = 56
      Height = 16
      Caption = '&Condition'
      FocusControl = Where2_Src
    end
    object ComboBox2: TComboBox
      Left = 10
      Top = 39
      Width = 463
      Height = 24
      Style = csDropDownList
      ItemHeight = 16
      TabOrder = 0
      OnChange = LoopChange
      Items.Strings = (
        'log to SQL log defined in DATABASES'
        'log to DB Log')
    end
    object Where2_Src: TComboBox
      Left = 10
      Top = 90
      Width = 143
      Height = 24
      ItemHeight = 16
      TabOrder = 1
      OnChange = LoopChange
    end
  end
  object Notebook2: TPanel
    Left = 0
    Top = 169
    Width = 482
    Height = 229
    Align = alTop
    BevelOuter = bvNone
    Caption = ' '
    TabOrder = 2
    Visible = False
    object Label1: TLabel
      Left = 10
      Top = 10
      Width = 45
      Height = 16
      Caption = '&Method'
      FocusControl = Loop
    end
    object Label2: TLabel
      Left = 10
      Top = 186
      Width = 23
      Height = 16
      Caption = '&Log'
      FocusControl = ComboBox1
    end
    object Label4: TLabel
      Left = 11
      Top = 64
      Width = 108
      Height = 16
      Caption = '&Condition (source)'
      FocusControl = Where1_Src
    end
    object Label6: TLabel
      Left = 171
      Top = 64
      Width = 101
      Height = 16
      Caption = 'Condition (target)'
      FocusControl = Where1_Tgt
    end
    object Loop: TComboBox
      Left = 10
      Top = 30
      Width = 455
      Height = 24
      Style = csDropDownList
      ItemHeight = 16
      TabOrder = 0
      OnChange = LoopChange
      Items.Strings = (
        'loop source table'
        'loop target table')
    end
    object Update: TCheckBox
      Left = 10
      Top = 117
      Width = 444
      Height = 21
      Caption = '&UPDATE non equal records'
      TabOrder = 3
      OnClick = LoopChange
    end
    object Insert: TCheckBox
      Left = 10
      Top = 137
      Width = 444
      Height = 21
      Caption = '&INSERT new records'
      TabOrder = 4
      OnClick = LoopChange
    end
    object Delete: TCheckBox
      Left = 10
      Top = 156
      Width = 444
      Height = 21
      Caption = '&DELETE non existing records'
      TabOrder = 5
      OnClick = LoopChange
    end
    object ComboBox1: TComboBox
      Left = 10
      Top = 206
      Width = 463
      Height = 24
      Style = csDropDownList
      ItemHeight = 16
      TabOrder = 6
      OnChange = LoopChange
      Items.Strings = (
        'direct synchronization'
        'log differential SQL commands')
    end
    object Where1_Src: TComboBox
      Left = 10
      Top = 82
      Width = 151
      Height = 24
      ItemHeight = 16
      TabOrder = 1
      OnChange = LoopChange
    end
    object Where1_Tgt: TComboBox
      Left = 170
      Top = 82
      Width = 151
      Height = 24
      ItemHeight = 16
      TabOrder = 2
      OnChange = LoopChange
    end
  end
  object OKBtn: TButton
    Left = 69
    Top = 580
    Width = 92
    Height = 30
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 5
  end
  object CancelBtn: TButton
    Left = 197
    Top = 580
    Width = 92
    Height = 30
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 6
  end
  object DatabaseType: TRadioGroup
    Left = 0
    Top = 0
    Width = 482
    Height = 50
    Align = alTop
    Caption = 'Database &type'
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Interbase'
      'SQL log')
    TabOrder = 0
    OnClick = DatabaseTypeClick
  end
  object Panel1: TPanel
    Left = 0
    Top = 50
    Width = 482
    Height = 119
    Align = alTop
    BevelOuter = bvNone
    Caption = ' '
    TabOrder = 1
    object ListBox1: TListBox
      Left = 0
      Top = 0
      Width = 395
      Height = 119
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Courier'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      TabOrder = 0
      OnClick = ListBox1Click
    end
    object Button1: TButton
      Left = 404
      Top = 20
      Width = 70
      Height = 30
      Caption = '&Add'
      TabOrder = 1
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 404
      Top = 59
      Width = 70
      Height = 31
      Caption = 'D&elete'
      TabOrder = 2
      OnClick = Button2Click
    end
  end
  object HelpBtn: TButton
    Left = 325
    Top = 580
    Width = 92
    Height = 30
    Caption = '&Help'
    TabOrder = 7
    OnClick = HelpBtnClick
  end
end
