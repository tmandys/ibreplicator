(* IB Replication Suite
 * Copyright (C) 2002,2003 by Tomas Mandys-MandySoft
 *)

unit dg_SchemaFieldsConflictOptions;

interface

uses
  {$IFDEF LINUX}
  Libc, QGraphics, QForms, QControls, QStdCtrls, QButtons, QExtCtrls,
  {$ELSE}
  Windows, Graphics, Forms, Controls, StdCtrls, Buttons, ExtCtrls,
  {$ENDIF}
  SysUtils, Classes;

type
  TSchemaFieldsConflictOptionsDialog = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    HelpBtn: TButton;
    GroupBox1: TGroupBox;
    Options_3: TCheckBox;
    Options_4: TCheckBox;
    Options_5: TCheckBox;
    GroupBox2: TGroupBox;
    Options_8: TCheckBox;
    Label1: TLabel;
    Options_2: TCheckBox;
    Options_1: TCheckBox;
    Options_0: TCheckBox;
    GroupBox3: TGroupBox;
    Options_9: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure OptionsChange(Sender: TObject);
    procedure HelpBtnClick(Sender: TObject);
  private
    function GetOptions: Integer;
    procedure SetOptions(const Value: Integer);
    procedure AdjustControls;
    { Private declarations }
  public
    FieldType: Byte;
    property Options: Integer read GetOptions write SetOptions;
    { Public declarations }
  end;

var
  SchemaFieldsConflictOptionsDialog: TSchemaFieldsConflictOptionsDialog;

implementation
uses
  IBReplicator;

{$IFDEF LINUX}
{$R *.xfm}
{$ELSE}
{$R *.dfm}
{$ENDIF}

{ TSchemaFieldsConflictOptionsDialog }

function TSchemaFieldsConflictOptionsDialog.GetOptions: Integer;
var
  I: Integer;
  C: TComponent;
begin
  Result:= 0;
  for I:= 0 to 15 do
  begin
    C:= FindComponent(Format('Options_%d', [I]));
    if (C <> nil) and TCheckBox(C).Checked then
      Result:= Result or (1 shl I);
  end;
end;

procedure TSchemaFieldsConflictOptionsDialog.SetOptions(
  const Value: Integer);
var
  I: Integer;
  C: TComponent;
begin
  for I:= 0 to 15 do
  begin
    C:= FindComponent(Format('Options_%d', [I]));
    if C <> nil then
      TCheckBox(C).Checked:= Value and (1 shl I) <> 0;
  end;
  AdjustControls;
end;

procedure TSchemaFieldsConflictOptionsDialog.FormShow(Sender: TObject);
begin
  AdjustControls;
end;

procedure TSchemaFieldsConflictOptionsDialog.AdjustControls;
begin
  Options_2.Enabled:= (FieldType <> 1) and not Options_1.Checked;
  Options_3.Enabled:= (FieldType <> 1) and not Options_1.Checked and not Options_4.Checked and not Options_5.Checked;
  Options_4.Enabled:= (FieldType <> 1) and not Options_1.Checked and not Options_5.Checked;
  Options_5.Enabled:= (FieldType <> 1) and not Options_1.Checked;
end;

procedure TSchemaFieldsConflictOptionsDialog.OptionsChange(Sender: TObject);
begin
  AdjustControls;
end;

procedure TSchemaFieldsConflictOptionsDialog.HelpBtnClick(Sender: TObject);
begin
  Application.{$IFDEF LINUX}ContextHelp{$ELSE}HelpContext{$ENDIF}(HelpContext);
end;

end.
