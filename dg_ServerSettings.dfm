object ServerSettingsDialog: TServerSettingsDialog
  Left = 298
  Top = 223
  HelpContext = 21100
  BorderStyle = bsDialog
  Caption = 'Settings'
  ClientHeight = 553
  ClientWidth = 415
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object Label4: TLabel
    Left = 0
    Top = 0
    Width = 415
    Height = 48
    Align = alTop
    Caption = 
      'Parameters indicated by blue color are default ones for replicat' +
      'ion server tasks and may be overriden in "Task settings" dialog ' +
      'of the replication server.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    WordWrap = True
  end
  object Bevel1: TBevel
    Left = 0
    Top = 449
    Width = 413
    Height = 9
    Anchors = [akLeft, akTop, akRight]
    Shape = bsBottomLine
  end
  object OKBtn: TButton
    Left = 80
    Top = 464
    Width = 92
    Height = 31
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
  end
  object CancelBtn: TButton
    Left = 80
    Top = 512
    Width = 92
    Height = 31
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
  object HelpBtn: TButton
    Left = 248
    Top = 464
    Width = 92
    Height = 31
    Caption = '&Help'
    TabOrder = 4
    OnClick = HelpBtnClick
  end
  object Button1: TButton
    Left = 248
    Top = 512
    Width = 92
    Height = 31
    HelpContext = 23000
    Caption = '&Environment...'
    TabOrder = 5
    OnClick = Button1Click
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 48
    Width = 415
    Height = 377
    ActivePage = TabSheet3
    Align = alTop
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Config.database'
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 117
        Height = 16
        Caption = 'Database file name'
        FocusControl = Database
      end
      object Label2: TLabel
        Left = 16
        Top = 144
        Width = 66
        Height = 16
        Caption = 'User name'
        FocusControl = UserName
      end
      object Label3: TLabel
        Left = 208
        Top = 144
        Width = 60
        Height = 16
        Caption = 'Password'
        FocusControl = Password
      end
      object Label10: TLabel
        Left = 16
        Top = 192
        Width = 56
        Height = 16
        Caption = 'Obj.prefix'
        FocusControl = ObjPrefix
      end
      object Label15: TLabel
        Left = 16
        Top = 240
        Width = 69
        Height = 16
        Caption = 'SQL dialect'
        FocusControl = SQLDialect
      end
      object Label16: TLabel
        Left = 208
        Top = 192
        Width = 71
        Height = 16
        Caption = 'Def.char set'
        FocusControl = CharSet
      end
      object Label6: TLabel
        Left = 16
        Top = 56
        Width = 370
        Height = 64
        Caption = 
          'For local server use preffered "localhost:path/database.gdb" syn' +
          'tax.'#13#10'Do not use local Interbase server database, it causes hang' +
          'ing of OS in multi-threading environment !'
        WordWrap = True
      end
      object Database: TEdit
        Left = 16
        Top = 32
        Width = 377
        Height = 24
        TabOrder = 0
        OnChange = DatabaseChange
        OnExit = DatabaseExit
      end
      object UserName: TEdit
        Left = 16
        Top = 160
        Width = 185
        Height = 24
        TabOrder = 1
      end
      object Password: TEdit
        Left = 208
        Top = 160
        Width = 169
        Height = 24
        TabOrder = 2
        OnChange = PasswordChange
      end
      object ObjPrefix: TEdit
        Left = 16
        Top = 208
        Width = 185
        Height = 24
        TabOrder = 4
      end
      object CharSet: TComboBox
        Left = 208
        Top = 208
        Width = 185
        Height = 24
        ItemHeight = 16
        TabOrder = 5
        Items.Strings = (
          'NONE'
          'ASCII'
          'BIG_5'
          'CYRL'
          'DOS437'
          'DOS850'
          'DOS852'
          'DOS857'
          'DOS860'
          'DOS861'
          'DOS863'
          'DOS865'
          'EUCJ_0208'
          'GB_2312'
          'ISO8859_1'
          'ISO8859_2'
          'KSC_5601'
          'NEXT'
          'OCTETS'
          'SJIS_0208'
          'UNICODE_FSS'
          'WIN1250'
          'WIN1251'
          'WIN1252'
          'WIN1253'
          'WIN1254')
      end
      object Button2: TButton
        Left = 376
        Top = 160
        Width = 17
        Height = 24
        Caption = '*'
        TabOrder = 3
        OnClick = Button2Click
      end
      object SQLDialect: TComboBox
        Left = 16
        Top = 256
        Width = 89
        Height = 24
        ItemHeight = 16
        TabOrder = 6
        Items.Strings = (
          '1'
          '3')
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Appearence'
      ImageIndex = 1
      object Label14: TLabel
        Left = 16
        Top = 16
        Width = 119
        Height = 16
        Caption = 'Max lines in terminal'
        FocusControl = MaxLines
      end
      object Label12: TLabel
        Left = 16
        Top = 152
        Width = 61
        Height = 16
        Caption = 'InstanceId'
        FocusControl = InstanceId
      end
      object Label17: TLabel
        Left = 16
        Top = 272
        Width = 384
        Height = 48
        Caption = 
          'Multi-threading replication causes machine crash when is used a ' +
          'local server database - database name without server prefix. For' +
          ' local server use "localhost:path/database.gdb" syntax. '
        WordWrap = True
      end
      object Label11: TLabel
        Left = 16
        Top = 224
        Width = 99
        Height = 16
        Caption = 'Max.repl.threads'
        FocusControl = MaxReplThreads
      end
      object MaxLines: TEdit
        Left = 16
        Top = 32
        Width = 80
        Height = 24
        TabOrder = 0
      end
      object ShowOnSysTray: TCheckBox
        Left = 16
        Top = 64
        Width = 149
        Height = 21
        Caption = 'Show on systray'
        TabOrder = 1
      end
      object OnlyOneInstance: TCheckBox
        Left = 16
        Top = 128
        Width = 139
        Height = 20
        Caption = 'Only one instance'
        TabOrder = 3
        OnClick = OnlyOneInstanceClick
      end
      object InstanceId: TEdit
        Left = 16
        Top = 168
        Width = 81
        Height = 24
        TabOrder = 4
      end
      object RunMinimized: TCheckBox
        Left = 16
        Top = 96
        Width = 119
        Height = 21
        Caption = 'Run minimized'
        TabOrder = 2
      end
      object MultiThreading: TCheckBox
        Left = 16
        Top = 200
        Width = 209
        Height = 17
        Caption = 'Multi-threading replication'
        TabOrder = 5
        OnClick = MultiThreadingClick
      end
      object MaxReplThreads: TEdit
        Left = 16
        Top = 240
        Width = 81
        Height = 24
        TabOrder = 6
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Log'
      ImageIndex = 2
      object GroupBox1: TGroupBox
        Left = 8
        Top = 208
        Width = 385
        Height = 129
        Caption = 'Repl log'
        TabOrder = 2
        object Label21: TLabel
          Left = 16
          Top = 24
          Width = 167
          Height = 16
          Caption = 'File name ("NUL" = disable)'
          FocusControl = ReplLog
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label22: TLabel
          Left = 16
          Top = 72
          Width = 117
          Height = 16
          Caption = 'Max file size [Bytes]'
          FocusControl = ReplLogMaxSize
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label23: TLabel
          Left = 168
          Top = 72
          Width = 97
          Height = 16
          Caption = 'Rotate log count'
          FocusControl = ReplLogRotate
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object ReplLog: TEdit
          Left = 16
          Top = 40
          Width = 353
          Height = 24
          TabOrder = 0
        end
        object ReplLogMaxSize: TEdit
          Left = 16
          Top = 88
          Width = 113
          Height = 24
          TabOrder = 1
        end
        object ReplLogRotate: TEdit
          Left = 168
          Top = 88
          Width = 80
          Height = 24
          TabOrder = 2
        end
      end
      object GroupBox4: TGroupBox
        Left = 8
        Top = 8
        Width = 385
        Height = 169
        Caption = 'DB Log'
        TabOrder = 0
        object Label24: TLabel
          Left = 16
          Top = 24
          Width = 167
          Height = 16
          Caption = 'File name ("NUL" = disable)'
          FocusControl = DBLog
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label25: TLabel
          Left = 16
          Top = 72
          Width = 119
          Height = 16
          Caption = 'Max log size [Bytes]'
          FocusControl = DBLogMaxSize
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label26: TLabel
          Left = 152
          Top = 72
          Width = 97
          Height = 16
          Caption = 'Rotate log count'
          FocusControl = DBLogRotate
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label18: TLabel
          Left = 256
          Top = 72
          Width = 79
          Height = 16
          Caption = 'Syslog name'
          FocusControl = DBSysLog
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object DBLog: TEdit
          Left = 16
          Top = 40
          Width = 353
          Height = 24
          TabOrder = 0
        end
        object DBLogMaxSize: TEdit
          Left = 16
          Top = 88
          Width = 113
          Height = 24
          TabOrder = 1
        end
        object DBLogRotate: TEdit
          Left = 152
          Top = 88
          Width = 80
          Height = 24
          TabOrder = 2
        end
        object LogErrSQLCmds: TCheckBox
          Left = 16
          Top = 120
          Width = 218
          Height = 21
          Caption = 'Log error SQL commands'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
        end
        object LogErrSQLParams: TCheckBox
          Left = 16
          Top = 144
          Width = 218
          Height = 21
          Caption = 'Log error SQL params'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
        end
        object DBSysLog: TEdit
          Left = 256
          Top = 88
          Width = 113
          Height = 24
          TabOrder = 3
        end
      end
      object LogVerbose: TCheckBox
        Left = 24
        Top = 184
        Width = 218
        Height = 21
        Caption = 'Verbose log (backup/restore)'
        TabOrder = 1
        Visible = False
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Replication'
      ImageIndex = 3
      object Label13: TLabel
        Left = 8
        Top = 48
        Width = 149
        Height = 16
        Caption = 'Offline package directory'
        FocusControl = OfflineDir
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object NowAsUTC: TCheckBox
        Left = 8
        Top = 16
        Width = 152
        Height = 21
        Caption = 'Now as UTC'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object OfflineDir: TEdit
        Left = 8
        Top = 64
        Width = 385
        Height = 24
        TabOrder = 1
      end
    end
    object TabSheet5: TTabSheet
      Caption = 'Scheduler'
      ImageIndex = 4
      object Label7: TLabel
        Left = 8
        Top = 48
        Width = 247
        Height = 16
        Caption = 'Replicate when time matches cron pattern'
        FocusControl = Cron
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label9: TLabel
        Left = 8
        Top = 256
        Width = 160
        Height = 16
        Caption = 'Do not replicate if file exists'
        FocusControl = FileLock
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label5: TLabel
        Left = 8
        Top = 96
        Width = 43
        Height = 16
        Caption = 'Interval'
        FocusControl = TimerInterval
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label8: TLabel
        Left = 8
        Top = 200
        Width = 97
        Height = 16
        Caption = 'Max.repl.runtime'
        FocusControl = MaxReplRunTime
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object DisableAll: TCheckBox
        Left = 8
        Top = 16
        Width = 265
        Height = 21
        Caption = 'Disable timers && events for all tasks'
        TabOrder = 0
      end
      object Cron: TEdit
        Left = 8
        Top = 64
        Width = 385
        Height = 24
        TabOrder = 1
      end
      object FileLock: TEdit
        Left = 8
        Top = 272
        Width = 385
        Height = 24
        TabOrder = 6
      end
      object TimerInterval: TEdit
        Left = 8
        Top = 112
        Width = 177
        Height = 24
        TabOrder = 2
      end
      object OnEvent: TCheckBox
        Left = 8
        Top = 142
        Width = 225
        Height = 21
        Caption = 'Replicate on event'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
      end
      object Event: TEdit
        Left = 8
        Top = 160
        Width = 385
        Height = 24
        TabOrder = 4
      end
      object MaxReplRunTime: TEdit
        Left = 8
        Top = 216
        Width = 177
        Height = 24
        TabOrder = 5
      end
    end
  end
  object IniFile: TEdit
    Left = 8
    Top = 432
    Width = 394
    Height = 24
    TabStop = False
    Anchors = [akLeft, akTop, akRight]
    BorderStyle = bsNone
    ParentColor = True
    ReadOnly = True
    TabOrder = 1
  end
end
