(* IB Replication Suite
 * Copyright (C) 2002,2003 by Tomas Mandys-MandySoft
 *)

unit dg_SchemaRelations;

interface

uses
  {$IFDEF LINUX}
  Libc, QGraphics, QForms, QControls, QStdCtrls, QButtons, QExtCtrls, QDialogs, QMask, QDBCtrls, QRxDBComb, Qt,
  {$ELSE}
  Windows, Messages, Graphics, Forms, Controls, StdCtrls, Buttons, ExtCtrls, Dialogs, Mask, DBCtrls, RxDBComb,
  {$ENDIF}
  SysUtils, Classes, dg_SchemaTemplate, Db, IBCustomDataSet, IBSQL;

type
  TSchemaRelationsDialog = class(TSchemaTemplateDialog)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    RxDBComboBox1: TRxDBComboBox;
    DBEdit3: TDBEdit;
    TableSCHEMAID: TIntegerField;
    TableRELATIONID: TIntegerField;
    TableRELATIONNAME: TIBStringField;
    TableGROUPID: TIntegerField;
    TableTARGETNAME: TIBStringField;
    TableTARGETTYPE: TIBStringField;
    TableDISABLED: TIBStringField;
    TableS_KEEP: TIBStringField;
    TableSYNCACTIONS: TMemoField;
    TableSYNCORDER: TIntegerField;
    TableCONDITION: TMemoField;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    DBMemo1: TDBMemo;
    DBMemo2: TDBMemo;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    DBEdit5: TDBEdit;
    TableCOMMENT: TMemoField;
    TableS_INSERT: TIntegerField;
    TableS_UPDATE: TIntegerField;
    TableS_DELETE: TIntegerField;
    TableS_CONFLICT: TIntegerField;
    TableS_ERROR: TIntegerField;
    TableS_MSEC: TIntegerField;
    TableOPTIONS: TIntegerField;
    DBMemo3: TDBMemo;
    Button1: TButton;
    Label9: TLabel;
    DBEdit4: TDBEdit;
    Button2: TButton;
    procedure TableNewRecord(DataSet: TDataSet);
    procedure TableRELATIONNAMEChange(Sender: TField);
    procedure TableBeforeEdit(DataSet: TDataSet);
    procedure Button1Click(Sender: TObject);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    fLastRelationName: string;
  protected
    procedure ChangeDependant(aFrom, aTo{-MaxInt...delete}: Integer); override;
  public
    SchemaType: Integer;
  end;

var
  SchemaRelationsDialog: TSchemaRelationsDialog;

implementation

uses
  dm_IBRepl, IBReplicator, dg_SchemaRelationsSyncActions, dg_SchemaRelationsOptions;

{$IFDEF LINUX}
{$R *.xfm}
{$ELSE}
{$R *.dfm}
{$ENDIF}

procedure TSchemaRelationsDialog.TableNewRecord(DataSet: TDataSet);
var
  I: Integer;
begin
  inherited;
  with DataSet do
  begin
    FieldByName('TARGETTYPE').asString:= 'T';
    FieldByName('DISABLED').asString:= 'N';
    FieldByName('OPTIONS').asInteger:= 0;
    for I:= 0 to FieldCount-1 do
      if Pos('S_', Fields[I].FieldName) = 1 then
        Fields[I].asInteger:= 0;
    FieldByName('S_KEEP').asString:= 'N';
  end;
end;

procedure TSchemaRelationsDialog.TableRELATIONNAMEChange(Sender: TField);
begin
  inherited;
  if fLastRelationName = Sender.DataSet.FieldByName('TARGETNAME').asString then
    Sender.DataSet.FieldByName('TARGETNAME').asString:= Sender.asString;
  fLastRelationName:= Sender.asString;
end;

procedure TSchemaRelationsDialog.TableBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  fLastRelationName:= DataSet.FieldByName('RELATIONNAME').asString;
end;

procedure TSchemaRelationsDialog.ChangeDependant(aFrom, aTo: Integer);
var
  Q: TIBSQL;
begin
  inherited;
  Q:= TIBSQL.Create(nil);
  try
    Q.Database:= Table.Database;
    Q.Transaction:= Table.Transaction;
    if aTo = - MaxInt then
      Q.SQL.Text:= Format('DELETE FROM %s', [TIBReplicator.FormatIdentifier(Q.Database.SQLDialect, IBReplDataModule.IBReplicator.ConfigDatabasePrefix+'FIELDS')])
    else
      Q.SQL.Text:= Format('UPDATE %s SET RELATIONID=%d', [TIBReplicator.FormatIdentifier(Q.Database.SQLDialect, IBReplDataModule.IBReplicator.ConfigDatabasePrefix+'FIELDS'), aTo]);
    Q.SQL.Add(Format('WHERE SCHEMAID=%d AND GROUPID=%d AND RELATIONID=%d', [Table.FieldByName('SCHEMAID').asInteger, Table.FieldByName('GROUPID').asInteger, aFrom]));
    Q.ExecQuery;
  finally
    Q.Free;
  end;
end;

procedure TSchemaRelationsDialog.Button1Click(
  Sender: TObject);
begin
  inherited;
  with TSchemaRelationsSyncActionsDialog.Create(Self) do
  try
    SyncActions:= {$IFDEF LINUX}TMemo{$ENDIF}(DBMemo3).Lines.Text;
    if ShowModal = mrOk then
    begin
      Table.Edit;
//      DBMemo3.Lines.Text:= SyncActions;
      DBMemo3.Field.AsString:= SyncActions;
    end;
  finally
    Release;
  end;
end;

procedure TSchemaRelationsDialog.DBEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = {$IFDEF LINUX}Key_down{$ELSE}VK_DOWN{$ENDIF}) and ([ssAlt] = Shift) then
  begin
    Button1Click(nil);
    Key:= 0;
  end;
end;

procedure TSchemaRelationsDialog.FormShow(Sender: TObject);
begin
  inherited;
  DBEdit3.Enabled:= SchemaType = schtReplication;
  DBMemo3.Enabled:= DBEdit3.Enabled;
  DBEdit5.Enabled:= DBEdit3.Enabled;
  Button1.Enabled:= DBEdit3.Enabled;
  RxDBComboBox1.Enabled:= DBEdit3.Enabled;
  DBEdit4.Enabled:= DBEdit3.Enabled;
  Button2.Enabled:= DBEdit3.Enabled;
  DBCheckBox2.Enabled:= DBEdit3.Enabled;
end;

procedure TSchemaRelationsDialog.Button2Click(Sender: TObject);
begin
  inherited;
  with TSchemaRelationsOptionsDialog.Create(Self) do
  try
    TargetType:= (Table.FieldByName('TARGETTYPE').asString+' ')[1];
    Options:= StrToInt(DBEdit4.Text);
    if ShowModal = mrOk then
    begin
      Table.Edit;
      DBEdit4.Text:= IntToStr(Options);
    end;
  finally
    Release;
  end;
end;

end.


