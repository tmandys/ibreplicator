(* IB Replication Suite
 * Copyright (C) 2002,2003 by Tomas Mandys-MandySoft
 *)

unit dg_Schema;

interface

uses
  {$IFDEF LINUX}
  Libc, QGraphics, QForms, QControls, QStdCtrls, QButtons, QExtCtrls, QDialogs, QMask, QDBCtrls, QRxDBComb,
  {$ELSE}
  Windows, Messages, Graphics, Forms, Controls, StdCtrls, Buttons, ExtCtrls, Dialogs, Mask, DBCtrls, RxDBComb,
  {$ENDIF}
  SysUtils, Classes, dg_SchemaTemplate, Db, IBCustomDataSet, IBQuery, IBSQL;

type
  TSchemaDialog = class(TSchemaTemplateDialog)
    Label1: TLabel;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label10: TLabel;
    DBMemo2: TDBMemo;
    DBCheckBox2: TDBCheckBox;
    Label3: TLabel;
    RxDBComboBox1: TRxDBComboBox;
    procedure TableNewRecord(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure TableBeforePost(DataSet: TDataSet);
    procedure DataSource1DataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SchemaDialog: TSchemaDialog;

implementation

uses dm_IBRepl, IBReplicator;

{$IFDEF LINUX}
{$R *.xfm}
{$ELSE}
{$R *.dfm}
{$ENDIF}

resourcestring
  sCannotDeleteSchema = 'Schema is used in SCHEMADB';

procedure TSchemaDialog.TableNewRecord(DataSet: TDataSet);
var
  I: Integer;
begin
  inherited;
  with DataSet do
  begin
    FieldByName('SCHEMATYPE').asInteger:= schtReplication;
    for I:= 0 to FieldCount-1 do
      if Pos('S_', Fields[I].FieldName) = 1 then
        Fields[I].asInteger:= 0;
    FieldByName('S_KEEP').asString:= 'N';
  end;
end;

procedure TSchemaDialog.FormCreate(Sender: TObject);
begin
  inherited;
  BatchReindexingForbidden:= True;
end;

procedure TSchemaDialog.TableBeforePost(DataSet: TDataSet);
var
  Q: TIBSQL;
begin
  if LastCountPrimaryValue <> DataSet.FieldByName('SCHEMAID').asInteger then
  begin
    Q:= TIBSQL.Create(nil);
    try
      Q.Database:= Table.Database;
      Q.Transaction:= Table.Transaction;
      Q.SQL.Text:= Format('SELECT COUNT(*) FROM %s WHERE SCHEMAID=%d', [TIBReplicator.FormatIdentifier(Q.Database.SQLDialect, IBReplDataModule.IBReplicator.ConfigDatabasePrefix+'SCHEMADB'), LastCountPrimaryValue]);
      Q.ExecQuery;
      if Q.Fields[0].asInteger > 0 then
        IBReplicatorError(sCannotDeleteSchema, 2);
    finally
      Q.Free;
    end;
  end;
  inherited;
end;

procedure TSchemaDialog.DataSource1DataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  DBCheckBox2.Enabled:= Table.FieldByName('SCHEMATYPE').asInteger = schtReplication;
end;

end.
