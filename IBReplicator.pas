(* IBReplicator - Interbase replicator library

 * Copyright (c) 2002,2004 by Mandys Tomas-MandySoft
 *)

{ URL: http://www.2p.cz }

{$B-}
unit IBReplicator;

{ IBReplicator.htx - do not update .htx because Opus compiler has a problem with overloaded functions }

interface
uses
  {$IFDEF LINUX}
  Libc, Types, Qt,
  {$ELSE}
  Windows, Messages,
  {$ENDIF}
  {$IFDEF VER130}
  SystemD6,
  {$ENDIF}
  SysUtils, Classes,
  IBDatabase, IBServices, Db, IBSQL, IBCustomDataSet, Connect, IB, IBBlob, IBDataSet2, IBHeader,
  Transfer, Magic, Crc32, IBStoredProc, Contnrs;

const
  saUpd    = $01;
  saInsDel = $02;
  saSrcTgt = $04;  // 0..SELECT SRC, 1..SELECT TGT
  saRO     = $08;  // 1..make incremental SQL
{
  0(8) .. (SQL diff), select src, none
  1(9) ..                         update
  2(A) ..                         insert           (dump db to file / log)
  3(B) ..                         insert&update
  4(C) .. (SQL diff), select tgt, none
  5(D) ..                         update
  6(E) ..                         delete
  7(F) ..                         delete&update
}

const
  dbtInterbase = 0;
  dbtLog = 1;

const
  optIsRelRecord = $01;
  optIsForeignRecord = $02;
  optUTF8 = $04;
  optIsExternalRecord = $08;

const
  optextData = $01;

const
  ackflManual = $01;
  ackflReasonStr = $02;
  ackflConflict = $04;

const
  offrtRelationDef = 1;
  offrtReplLog = 2;
  offrtReplAck = 3;
  offrrtReplAckAck = 4;
  offrrtReplAckAckAck = 5;
  offrReplResend= 6;

const
  schtReplication = 0;
  schtRecordHistory = 1;

const
  repoptReplicateLog = $01;       // do not replicate records but move repl$log from source to target
  repoptReportToSource = $02;     // report problem to source repl$man
  repoptReportToTarget = $04;     // report problems to target repl$man
  repoptExtConflictCheck = $08;   // enable extended conflict checking
  repoptTargetReplication = $10;  // replication of target log

const
  confoptUnknown = 0;
  confoptPrimaryKeyViolation = 1;
  confoptRecordViolation = 2;
  confoptFieldViolation = 3;


const
  reloptWipeLogOnDelete = $01;
  reloptInsertUpdateMangling = $02;

const
  fldoptDoNotInsert = $01;
  fldoptDoNotUpdate = $02;
  fldoptUpdateOnlyWhenChanged = $04;
  fldoptTargetPriorityConflict = $08;
  fldoptLogConflict = $10;
  fldoptDoNotUpdateRecordWhenConflict = $20;
  fldoptDoNotDeleteRecordWhenConflict = $100;
  fldoptExternalFile = $200;

const
  xmloptHeader = $03;
  xmloptExternal = $04;
  
type
  TDatabaseVersion = (verCurrent, verUpgradable, verNonUpgradable);

type
  TStringOpenArray = array of string;
  TIntegerOpenArray = array of Integer;


type
  TOffStamp = array[1..9] of Char;
  TRelName = array[1..30] of Char;

  TOffHeader = packed record
    Magic: TOffMagic;
    SchemaId: Longint;
    TransferId: Longint;
    Status: Char;  { Source/Target }
    SrcDBId: Longint;
    TgtDBId: Longint;
    GroupId: Longint;
    Stamp: TOffStamp;
    Crc: TCRC32;
    Reserved: array[0..15+7] of Byte;
  end;

  TOffSrcRelDefField = packed record
    FieldType: Byte;
    Name: TRelName;
  end;

  TOffSrcRelDef = packed record
    RecType: Byte;   {=1}
    RelationId: Longint;
    Name: TRelName;
    Count: Word;
  { Fields: array of TOffSrcRelDefField; }
  end;

  TOffSrcReplLog = packed record
    RecType: Byte;  {=2}
    RelationId: Longint;
    SeqId: Longword;
    RepType: Char;
    Sep: Char;
    Stamp: TOffStamp;
    Options: Byte;
  { Keys: array of Char;  // string(OLDKEY, NEWKEY, NEWFKEY (in new package is empty))
    if Options and optIsForeignRecord <> 0
    begin
      NewForeignRecord: TOffSrcRelRec;
      OldForeignRecord: TOffSrcRelRec;
    end;
    if Options and optIsRelRecord <> 0
      RelRecord: TOffSrcRelRec;
    if Options and optIsExternalRecord <> 0
      ExternalRecord: TOffSrcExternalRec;
    }
  end;

  TOffSrcRelRec = packed record
    FieldCount: Word;
  { Values: array of TOffReplRecValue; }
  end;

  TOffReplRecValue = packed record
    DataType: Char; // N..null, C..string(65535), H..short plain string(255), O..blob/memo/binary; D..date; T..time; S..datetime; B..boolean; I..Integer; J..Int64; F..float, ' '..field not found
    FieldType: Byte;  // TFieldType
{
    N..
    C..  Len: word; widestring
    O..  Len: longword; data
    I..  Longint
    J..  Int64
    B..  Byte
    D..  yyyymmdd
    T..  hhnnss
    S..  yyyymnddhhnnsszzz
    F..  extended
    M..  Len: longword; widestring
}
  end;

  TOffSrcExternalValue = packed record
    Idx: Word;
    Options: Word;
    Stamp: TOffStamp;
    Size: LongWord;
    { Len: Word; Name: widestring;
      if Options and optextData <> 0
        Data of Size
    }
  end;

  TOffSrcExternalRec = packed record
    FieldCount: Word;
  { Values: array of TOffSrcExternalValue; }
  end;

  TOffTgtReplAckRec = packed record
    Flag: Byte;   // 0..delete, 1..to manual
    SeqId: Longint;
  { if Flags and ackflReasonStr <> 0 then
      Len: Word; ReasonStr: UTF8String;
    if Flags and ackflConflict <> 0 then
    begin
      Conflict: TOffTgtReplAckConflictRec;
    end;
  }
  end;

  TOffTgtReplAckConflictRec = packed record
    Flag: Byte;   // bit 0=1..conflicting primary key
    Stamp: TOffStamp;
    TgtDBId: Longint;
    Count: Word;
  {  TgtRelationName: string }
  { Values: array[0..Count-1] of packed record
    begin
      SrcFieldName: string[Length(SrcFieldName)];
      SrcValue: TOffReplRecValue;
      TgtFieldName: string[Length(TgtFieldName)];
      TgtValue: TOffReplRecValue;
    end;
  }
  end;

  TOffTgtReplAck = packed record
    RecType: Byte;    {=3}
    TransferIdAck: Longint;
    StampRec: TOffStamp;
    StampProc: TOffStamp;
    ProcCount: Longint;
  { SeqIds: Array[0..ProcCount-1] of TOffTgtReplAckRec; }
  end;

  TOffSrcReplAckAck = packed record
    RecType: Byte;   {=4}
    TransferId: Longint; // of StampRec 'S' record
    TransferIdAck: Longint;  // of 'T' record
    StampSent: TOffStamp;
    StampRec: TOffStamp;
    StampProc: TOffStamp;
  end;

  TOffTgtReplAckAckAck = packed record
    RecType: Byte;   {=5}
    TransferIdAck: Longint;
    StampSent: TOffStamp;
  end;

  TOffReplResend = packed record
    RecType: Byte;    {=6}
    TransferId: Longint;
    TransferIdAck: Longint;
    StampRec: TOffStamp;
    StampProc: TOffStamp;
  end;

type
  TOnUpdateStatusEvent = procedure(Sender: TObject; aTicks, aCnt: LongWord) of object;

type
  TSyncRecordCounters = record
    Total, Inserted, Updated, Deleted, Errors, Conflicts: Integer;
    StartTick: Longword;
  end;

  TSyncAction = record
    Action: Byte;
    Where_Src, Where_Tgt: string;
  end;

  TSyncCondition = record
    Alias: string;
    Where: string;
  end;

  TSyncActions = record
    Acts: array of TSyncAction;
    Cond: array of TSyncCondition;
  end;

  TSyncRecord = record
    SchemaId, GroupId, SrcDBId, TgtDBId, RelationId: Integer;
    RelationName: string;
    Tag: Integer;
    Action: TSyncAction;
  end;

{ cached configuration }
type

  TSortedClassItem = class
  public
    Id: Integer;
  end;

{ optimized list sorted by Id, fill incrementaly & read }

  TSortedClassList = class(TList)
  private
    fCurItem: TSortedClassItem;
    fCurIdx: Integer;
    fLastFindId: Integer;
  protected
    procedure Notify(Ptr: Pointer; Action: TListNotification); override;
  public
    function Find(const aId: Integer): TSortedClassItem;
    procedure Add(aItem: TSortedClassItem);
  end;

  TReplCachedSchemaItem = class(TSortedClassItem)
  public
    Groups: TSortedClassList;
    // temp runtime
    Stats: record
      Fl: Boolean;
      Cnt_ManI, Cnt_ManU, Cnt_LogD, Cnt_LogU, Cnt: Integer;
    end;
    constructor Create;
    destructor Destroy; override;
  end;

  TReplTargetDatabase = class;

  TReplCachedGroupItem = class(TSortedClassItem)
  public
    Relations: TSortedClassList;
    // temp runtime
    DBs: array[0..31] of record
      DBId: Integer;
      Tgt: TReplTargetDatabase;
      Fl: Byte;  // 0..not connected, 1..connected, 2..connection broken, do not try to reconnect
      Stats: record
        Cnt_U, Cnt_I, Cnt_D: Integer;
        Cnt2_U, Cnt2_I, Cnt2_D: Integer;
        Cnt_Total: Integer;
      end;
    end;
    constructor Create;
    destructor Destroy; override;
  end;

  TReplCachedRelationItem = class(TSortedClassItem)
  public
    Name: string;
    TargetName: string;
    TType: Char;
    Options: Longword;
    Fields: TSortedClassList;
    // temp runtime
    OffRelationName: string;
    OffRelDone: Boolean;
    StoredProcSQL: string;
    RemapFields: array of Integer;
    RemapKeys: array[1..2] of
      record
        PkgKeyToPkgFieldIdx: array of Integer;
        CfgKeyToPkgKey: array of Integer;
      end;
    constructor Create;
    destructor Destroy; override;
  end;

  TReplCachedFieldItem = class(TSortedClassItem)
  public
    FType: Byte;
    Name: string;
    TargetName: string;
    Options: Longword;
  end;

  TIBReplicator = class;

  TReplFieldItem = record
    FieldName_Src, FieldName_Tgt: string;
    FieldId: Integer;
    Options: Integer;
    Idx: Integer;
    Tag: Integer;
  end;

  TReplFieldRef = record
    FieldType: Byte;
    Idx: Integer;
  end;

  TReplFieldDefs = class
  public
    Fields: array[1..3] of array of TReplFieldItem;
    FieldRefs: array of TReplFieldRef;
    RelationName_Src, RelationName_Tgt: string;
    Type_Tgt: Char;
    Where_Src, Where_Tgt, Where_Tgt_New: string;
    WhereParam_Src, WhereParam_Tgt: string;
    InsertSQL_1, InsertSQL_2, ModifySQL: string;
    RelationOptions: Integer;

    destructor Destroy; override;
    procedure ClearTags(aTagMask: Integer);
    procedure BuildFieldDefs(CRI: TReplCachedRelationItem; aSQLDialect_Src, aSQLDialect_Tgt: Integer);
    procedure GetWhereParams(aSQLDialect_Src, aSQLDialect_Tgt: Integer; const aPrefix_Tgt: string; var aWhere_Src, aWhere_Tgt: string);
    procedure GetWhereValues(aSQLDialect_Src, aSQLDialect_Tgt: Integer; IUD: Char; const aOldPKey, aNewPKey: TStringOpenArray; var aWhere_Src, aWhere_Tgt, aWhere_Tgt_New: string);
    function Is_Updatable_Tgt(aFieldType: Byte; Idx: Integer; IUD: Char): Boolean;
  end;

  PReplOffRecordBuffer = ^TReplOffRecordBuffer;
  TReplOffRecordBuffer = record
    Buffer: TMemoryStream;
    UTF8: Boolean;
    Pos: array of Integer;
  end;

  TReplLogExternal = record
    Idx: Integer;
    FileName: string;
    Stamp: TDateTime;
    Data: TStream;
    DataPos: LongInt;
    DataSize: LongInt;
  end;

  TReplLogExternalOpenArray = array of TReplLogExternal;

  TReplLogField = class
  protected
    function GetDataType_F: Char;
    function GetDataType: TFieldType; virtual; abstract;
    function GetAsString: string; virtual; abstract;
    function GetAsVariant: Variant; virtual; abstract;
    function GetAsInt64: Int64; virtual; abstract;
    function GetAsFloat: Extended; virtual; abstract;
    function GetAsInteger: Longint; virtual; abstract;
    function GetAsDateTime: TDateTime; virtual; abstract;
  public
    property DataType: TFieldType read GetDataType;
    property DataType_F: Char read GetDataType_F;
    property asString: string read GetAsString;
    property asVariant: Variant read GetAsVariant;
    property asInteger: LongInt read GetAsInteger;
    property asFloat: Extended read GetAsFloat;
    property asInt64: Int64 read GetAsInt64;
    property asDateTime: TDateTime read GetAsDateTime;
    function IsNull: Boolean; virtual; abstract;
    function IsBlob: Boolean;
  end;

  TReplLogRecord = class
  private
    fSrc_Record: array of TReplLogField;
    fReplicator: TIBReplicator;
    function GetSrc_Record(Idx: Integer): TReplLogField;
  protected
    function GetSrc_IsEmpty: Boolean; virtual; abstract;
  public
    SchemaId, GroupId, SeqId, RelationId: Integer;
    CRI: TReplCachedRelationItem;
    IUD: Char;
    OldPKey, NewPKey, NewFKey{in offline for backward package compatability only}: TStringOpenArray;
    Separator: Char;
    Stamp: TDateTime;
    Externals: TReplLogExternalOpenArray;

    FieldDefs: TReplFieldDefs;

    function Src_RecordCount: Integer;
    function Src_FieldExists(Idx: Integer): Boolean;
    function FieldQ_Locate(aNew: Char; aIdx: Integer): TReplLogField; virtual; abstract;
    function FieldQ_IsEmpty: Boolean; virtual; abstract;
    constructor Create(aReplicator: TIBReplicator);
    destructor Destroy; override;
    property Src_Record[Idx: Integer]: TReplLogField read GetSrc_Record;
    property Src_IsEmpty: Boolean read GetSrc_IsEmpty;
  end;

  TReplDatabaseProperties = record
    DBId: Integer;
    DBName: string;
    ObjPrefix: string;
    ExtFilePath: string;
  end;

  TReplTargetDatabase = class
  private
    fReplicator: TIBReplicator;
    function GetSQLDialect: Integer; virtual; abstract;
  public
    DBProps: TReplDatabaseProperties;
    DBMask: Integer;
    procedure SynchronizeTable(const aSyncRec: TSyncRecord; aSrcDB: TIBDatabase; var aCounters: TSyncRecordCounters); virtual; abstract;
    procedure ReplicateRecord(const aReplLogRec: TReplLogRecord; aReplOptions: Word; aConflict: TStream; var aCounter: Integer); virtual; abstract;
    procedure WriteExtFiles(const aReplLogRec: TReplLogRecord; aIdx: Integer; aPath: string);
    property Replicator: TIBReplicator read fReplicator;
    property SQLDialect: Integer read GetSQLDialect;
  end;

  TReplTargetDatabase_IB = class(TReplTargetDatabase)
  private
    function GetSQLDialect: Integer; override;
    function InsertToTargetLog(const aReplLogRec: TReplLogRecord; aManual: Boolean; const aDescription: string; aConflict: TStream): Integer;
  public
    Database: TIBDatabase;
    SnapshotQ: TIBSQL;
    destructor Destroy; override;
    procedure SynchronizeTable(const aSyncRec: TSyncRecord; aSrcDB: TIBDatabase; var aCounters: TSyncRecordCounters); override;
    procedure ReplicateRecord(const aReplLogRec: TReplLogRecord; aReplOptions: Word; aConflict: TStream; var aCounter: Integer); override;
  end;

  TReplTargetDatabase_Logger = class(TReplTargetDatabase)
  private
    fSQLDialect: Integer;
    function GetSQLDialect: Integer; override;
  public
    Logger: TFileLogger;
    destructor Destroy; override;
    procedure SynchronizeTable(const aSyncRec: TSyncRecord; aSrcDB: TIBDatabase; var aCounters: TSyncRecordCounters); override;
    procedure ReplicateRecord(const aReplLogRec: TReplLogRecord; aReplOptions: Word; aConflict: TStream; var aCounter: Integer); override;
  end;

  TReplLogField_IBSQL = class(TReplLogField)
  private
    IBXSQLVAR: TIBXSQLVAR;
  protected
    function GetDataType: TFieldType; override;
    function GetAsString: string; override;
    function GetAsVariant: Variant; override;
    function GetAsInt64: Int64; override;
    function GetAsFloat: Extended; override;
    function GetAsInteger: Longint; override;
    function GetAsDateTime: TDateTime; override;
  public
    function IsNull: Boolean; override;
  end;

  TReplLogField_Offline = class(TReplLogField)
  private
    fSQRecord: PReplOffRecordBuffer;
    fFieldIdx: Integer;
    function GetRRV(var RRV: TOffReplRecValue): Boolean;
  protected
    function GetDataType: TFieldType; override;
    function GetAsString: string; override;
    function GetAsVariant: Variant; override;
    function GetAsInt64: Int64; override;
    function GetAsFloat: Extended; override;
    function GetAsInteger: Longint; override;
    function GetAsDateTime: TDateTime; override;
  public
    function IsNull: Boolean; override;
  end;

  TReplLogRecord_Online = class(TReplLogRecord)
  private
    fFieldQ: TIBSQL;
    fFieldQReplField: TReplLogField_IBSQL;
    fFieldQIsEmpty: Boolean;
    fSrc_IsEmpty: Boolean;
    procedure FreeExternals;
  protected
    function GetSrc_IsEmpty: Boolean; override;
  public
    constructor Create(aReplicator: TIBReplicator);
    destructor Destroy; override;
    function FieldQ_Locate(aNew: Char; aIdx: Integer): TReplLogField; override;
    function FieldQ_IsEmpty: Boolean; override;
    procedure AssignReplQ(aReplQ: TIBSQL);
    procedure AssignFieldQ(aFieldQ: TIBSQL);
    procedure AssignSQ(SQ: TIBSQL);
    procedure AssignExtFileQ(ExtFileQ: TIBSQL);
    procedure AssignExtFiles(const aDBProps: TReplDatabaseProperties);
  end;

  TReplLogRecord_Offline = class(TReplLogRecord)
  private
    SQRecord, FOldQRecord, FNewQRecord: TReplOffRecordBuffer; // .Buffer = nil if is empty
    fFieldQReplField: TReplLogField_Offline;
  protected
    function GetSrc_IsEmpty: Boolean; override;
  public
    constructor Create(aReplicator: TIBReplicator);
    destructor Destroy; override;
    function FieldQ_Locate(aNew: Char; aIdx: Integer): TReplLogField; override;
    function FieldQ_IsEmpty: Boolean; override;
    procedure Assign(const aHeader: TOffHeader; const aReplLog: TOffSrcReplLog);
  end;

  TIBObjectType = (ibobjRelation, ibobjProcedure, ibobjTrigger, ibobjFunction, ibobjGenerator, ibobjRole, ibobjDomain);

  TIBReplicator = class(TComponent)
  private
    fConfigDatabase: TIBDatabase;
    fConfigDatabaseTransaction: TIBTransaction;
    fCachedSchemes: TSortedClassList;
    fNowAsUTC: Boolean;
    fDBLog: TLogger;
    fReplLog: TLogger;
    fLogName: string;
    fConfigDatabasePrefix: string;
    fLogErrSQLCmds: Boolean;
    fLogErrSQLParams: Boolean;
    fLogVerbose: Boolean;
    fImplicitEnvironment, fEnvironment: TStrings;
    fOnUpdateStatus: TOnUpdateStatusEvent;
    fUTF8: Boolean;
    fTraceSQL: Boolean;
    fIBUtilPath: string;
    procedure LogParams(aParams: TIBXSQLDA);
    procedure GenTransferId(aDB: TIBDatabase; var aId: Longint; const aDBProps: TReplDatabaseProperties; aTrans: TIBTransaction);
    procedure SetEnvironment(const Value: TStrings);
    procedure CreateCustomObjects(DB: TIBDatabase; const aDBProps: TReplDatabaseProperties; aDBID: Integer);
    procedure PutOffReplString(Tgt: TStream; const Val: string; LSize: Byte; asWide, aUTF8: Boolean);
    procedure PutOffReplRecValue(Tgt: TStream; F1: TIBXSQLVAR); overload;
    procedure PutOffReplRecValue(Tgt: TStream; F1: TReplLogField); overload;
    class function GetOffReplString(Src: TStream; LSize: Byte; asWide: Boolean; aUTF8: Boolean): string;
    class function GetOffReplVariant(Src: TStream; aUTF8: Boolean): Variant;
    class function GetOffReplValueLength(Src: TStream; aUTF8: Boolean): Longword;
    procedure SetConfigDatabase(const Value: TIBDatabase);
    function ParseParams(const aPar: string; aDB: TIBDatabase = nil; const aObjPrefix: string = ''): string;
    function LocateFieldQuery(aFieldQ: TIBSQL; aNew: Char; aFieldId: Integer): Boolean;
    procedure ExecGBAK(aDB: TIBDatabase; aParams: string);
    procedure FillLogDeleteDesc(aSP: TIBStoredProc; const aDescription: string; aConflict: TStream);

    class function FormatXMLValue(const V: Variant): UTF8String;
    class procedure IndexReservedWords;
    function GetXMLElement(const aTag: string; const aAttrNames: array of string; aAttrValues: array of Variant; const aContent: UTF8string; aContentCRLF: Boolean; aIndent: Integer = 0): UTF8String;
    function GetXMLHeader(const aMasterTag, aDTD: string; aOptions: Byte): UTF8String;
    function GetXMLValue(Src: TStream; const aFieldName: string; aUTF8: Boolean): UTF8string;
    function GetXMLConflict(Src: TStream): UTF8String;
  protected
    property CachedSchemes: TSortedClassList read fCachedSchemes;
    procedure DBLogVerbose(const S: string);
    function GetTriggerName(aSchemaID, aGroupId, aRelationId: Integer; const aRelationName: string; const aAction: string; const aDBProps: TReplDatabaseProperties): string; virtual;
    function RelationExists(aDB: TIBDatabase; const aRelName: string): Boolean;
    function RelationFieldExists(aDB: TIBDatabase; const aRelName, aFieldName: string): Boolean;
    function IndexExists(aDB: TIBDatabase; const aName: string): Boolean;
    function PrimaryKeyExists(aDB: TIBDatabase; const aRelName: string; const aFields: array of string): Boolean;
    function DBUserExists(aDBServ: TIBSecurityService; const aName: string): Boolean;
    function DependentExists(aDB: TIBDatabase; const aName: string): Boolean;
    function GetRelationFieldType(aDB: TIBDatabase; const aRelName, aFieldName: string; var aType: string; var aLen, aScale, aSubType: Integer): Boolean;
    function GetRelationFieldLength(aDB: TIBDatabase; const aRelName, aFieldName: string): Integer;
    function GetMaxVARCHARlength(aDB: TIBDatabase; const aObjPrefix: string; aMin, aMax: Integer): Integer;
    function GenerateId(aDB: TIBDatabase; const aName: string; aInc: Integer): Integer;
    procedure SetGenerator(aDB: TIBDatabase; const aName: string; const aId: Integer);
    function ObjectExists(aDB: TIBDatabase; aObj: TIBObjectType; const aName: string): Boolean;
    function CreateObject(aDB: TIBDatabase; aObj: TIBObjectType; const aName, aParams: string): Boolean;
    procedure DropObject(aDB: TIBDatabase; aObj: TIBObjectType; const aName: string; aOpenClose: Boolean = False);

    function ReadCachedFields(aSchemaId, aGroupId, aRelId: Integer; aReadFields: Boolean; var CRI: TReplCachedRelationItem): Boolean;

    procedure PrepareSourceOfflineRecord(aReplLogRec: TReplLogRecord; Tgt: TStream; var aRecCounter, aRelCounter: Integer); virtual;

    function PrepareSourceOfflinePackage(aDB: TIBDatabase; const aDBProps: TReplDatabaseProperties; aSchemaId: Integer; aGroupId, aSrcDbId, aTgtDBId: Integer; Tgt: TStream; aIfReplRecord: Boolean): Integer{TransferId}; virtual;
    function ProcessSourceOfflinePackageAndPrepareTargetOfflinePackage(aDB: TIBDatabase; const aDBProps: TReplDatabaseProperties; aSchemaId: Integer; aGroupId, aSrcDbId, aTgtDBId: Integer; Src: TStream; Tgt: TStream; const aOfflineDir: string; aStampRec: TDateTime; aReplOptions: Word; var aStamp: TDateTime): Integer{TransferId}; virtual;
    procedure ProcessTargetOfflinePackage(aDB: TIBDatabase; const aDBProps: TReplDatabaseProperties; aSchemaId: Integer; aGroupId, aSrcDBId, aTgtDBId, aTransferId: Integer; Src: TStream; const aOfflineDir: string; aReplOptions: Word); virtual;

    function GetReplTargetDatabase(aSchemaId, aGroupId: Integer; aFldName: string; aDBMask: Integer): TReplTargetDatabase; virtual;
    function GetOfflinePackageName(aSchemaId: Integer; aStatus: Char; aGroupId, aSrcDBId, aTgtDBId, aTransferId: Integer): string; virtual;

    function EncodeBLOBMetaSQL(const aTable, aFieldName, aWhere: string; aBinary: Boolean; const aData: string): string; virtual;
    procedure DecodeBLOBMetaSQL(aLog: string; var aTable, aFieldName, aWhere, aData: string); virtual;

    function GetTempFileName: string;
    procedure DoUpdateStatus(aTicks, aCnt: LongWord); virtual;
    procedure ProcessMessages;

    function ForceStop: Boolean; virtual;
  published
    property ConfigDatabase: TIBDatabase read fConfigDatabase write SetConfigDatabase;
    property ConfigDatabasePrefix: string read fConfigDatabasePrefix write fConfigDatabasePrefix;

    property DBLog: TLogger read fDBLog write fDBLog;
    property ReplLog: TLogger read fReplLog write fReplLog;
    property LogName: string read fLogName write fLogName;
    property IBUtilPath: string read fIBUtilPath write fIBUtilPath;

    property Environment: TStrings read fEnvironment write SetEnvironment;
    property NowAsUTC: Boolean read fNowAsUTC write fNowAsUTC;
    property LogErrSQLCmds: Boolean read fLogErrSQLCmds write fLogErrSQLCmds default False;
    property LogErrSQLParams: Boolean read fLogErrSQLParams write fLogErrSQLParams default False;
    property LogVerbose: Boolean read fLogVerbose write fLogVerbose default False;

    property TraceSQL: Boolean read fTraceSQL write fTraceSQL;

    property OnUpdateStatus: TOnUpdateStatusEvent read fOnUpdateStatus write fOnUpdateStatus;
  public
    ManualStop: Boolean;
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    { replication managment routines }
    procedure CreateConfigDatabase(aAllowCreate: Boolean);
    procedure UpgradeConfigDatabase;
    function CheckConfigDatabaseVersion: TDatabaseVersion;
    procedure UpgradeDatabase(aDBId: Integer);

    procedure GenerateFields(aSchemaId, aTargetGroupId: Integer);
    procedure RemoveOrphanedFields(aSchemaId, aTargetGroupId: Integer);
    procedure ValidateSchema(aSchemaId, aTargetGroupId: Integer; aTargetDBId: Integer = 0);
    procedure CreateSystemObjects(aSchemaId: Integer; aKeyLength: Integer; aGroupId{master database}: Integer; aDBId: Integer = 0); overload;
    procedure DropSystemObjects(aSchemaId: Integer; aGroupId{master database}: Integer; aDBId: Integer = 0; aOnlyTriggers: Boolean = False); overload;
    procedure CreateServerObjects(aSchemaId: Integer; aGroupId{master database}: Integer; aDBId: Integer = 0); overload;
    procedure DropServerObjects(aSchemaId: Integer; aGroupId{master database}: Integer; aDBId: Integer = 0); overload;
    procedure CreateSystemObjects(aDBId: Integer; aKeyLength: Integer); overload;
    procedure DropSystemObjects(aDBId: Integer; aOnlyTriggers: Boolean = False); overload;
    procedure CreateServerObjects(aDBId: Integer); overload;
    procedure DropServerObjects(aDBId: Integer); overload;
    procedure CreateStoredProcedureTemplates(aSchemaId, aGroupId, aDBId: Integer);
    procedure ClearSchemaStatistics(aSchemaId: Integer);
    procedure ClearRelationStatistics(aSchemaId, aGroupId, aRelationId: Integer);
    procedure DeleteSourceSystemData(aSchemaId: Integer; aGroupId: Integer; aTgtDBId: Integer = 0);

    { open database routines }
    function GetSourceDBId(aSchemaId: Integer): Integer;
    function GetSchemaType(aSchemaId: Integer): Integer;
    function GetDatabaseName(aDBId: Integer): string;
    function GetDatabaseType(aDBId: Integer): Integer;
    function GetDBMask(aSchemaId, aGroupId, aDBId: Integer): Integer;
    function AssignDBParams(aDB: TIBDatabase; aId: Integer; var aDBProps: TReplDatabaseProperties): Boolean;

    procedure SetDBParams(aDB: TIBDatabase; aId: Integer; var aDBProps: TReplDatabaseProperties);
    procedure SetDBParams_Repl(aDB: TIBDatabase; aSchemaId, aGroupId, aDBId: Integer; aAlternateTgt{pro offdbid}: Boolean; var aDBProps: TReplDatabaseProperties);
    procedure SetDBServiceParams(aDBServ: TIBControlService; aDBId: Integer; var aDatabaseFileName: string); overload;
    procedure SetDBServiceParams(aDBServ: TIBControlService; aDB: TIBDatabase; const aDatabaseName: string; var aDatabaseFileName: string); overload;
    class function GetDBProtocol(aDatabaseName: string; var aServerName, aDatabaseFileName: string): IBServices.TProtocol;

    { auxiliary sql routines }
    function DBSQLExec(aDB: TIBDatabase; aSQL: string; aTrans: TIBTransaction = nil): Integer;
    function DBSQLRecord(aDB: TIBDatabase; aSQL: string; aTrans: TIBTransaction = nil): Variant;
    procedure SafeStartTransaction(T: TIBTransaction);
    procedure SafeCommit(T: TIBTransaction; aRetaining: Boolean=False);
    procedure SafeRollback(T: TIBTransaction; aRetaining: Boolean=False; aDoNotRaise: Boolean=True);
    procedure SafeNext(Q: TIBSQL); overload;
    procedure SafeNext(Q: TIBDataSet); overload;
    procedure SafeOpen(Q: TIBDataSet);
    procedure SafeClose(Q: TIBDataSet; aDoNotRaise: Boolean=True); overload;
    procedure SafeClose(Q: TIBSQL; aDoNotRaise: Boolean=True); overload;
    procedure SafeExecQuery(Q: TIBSQL);
    procedure SafeExecProc(Q: TIBStoredProc);
    function Now2: TDateTime;

    procedure ClearCachedValues(aOnlyRuntimeValues: Boolean = False);
    procedure DBEnvironmentWriteValue(aDB: TIBDatabase; const aObjPrefix, aName, aValue: string);
    function DBEnvironmentReadValue(aDB: TIBDatabase; const aObjPrefix, aName: string; const aDefault: string = ''): string;
    procedure DBEnvironmentWriteValues(aDB: TIBDatabase; const aObjPrefix: string; aValues: TStrings);
    procedure DBEnvironmentReadValues(aDB: TIBDatabase; const aObjPrefix: string; aValues: TStrings; aSystemKeys: Boolean = False);
    function ReadEnvironment(const aName: string; const aDefault: string = ''; aDB: TIBDatabase = nil; const aObjPrefix: string = ''): string;
    function ParseStr(const aPar: string; aDB: TIBDatabase = nil; const aObjPrefix: string = ''): string;
    procedure BuildImplicitEnvironment(const aNames, aValues: array of string; aClear: Boolean=True);

    { synchronization routines }
    procedure Synchronize(aSchemaId: Integer; const aGroupIds, aTgtDBIds: TIntegerOpenArray);
    class function DecodeSyncActions(const aActS: string; aResolve: Boolean): TSyncActions;
    class function EncodeSyncActions(const aActions: TSyncActions): string;

    { online replication routines }
    procedure ReplicateOnline(aDB: TIBDatabase; const aDBProps: TReplDatabaseProperties; const aSchemaIds, aGroupIds, aTgtDBIds: TIntegerOpenArray; aReplOptions: Word);

    { offline replication routines - source side }
    procedure SourceOfflineBatch(aDB: TIBDatabase; const aDBProps: TReplDatabaseProperties; const aSchemaIds, aGroupIds, aTgtDBIds: TIntegerOpenArray; const aOfflineDir: string; aReceiveData, aProcessReceived, aProcess, aSendData, aResendData: Boolean; aReplOptions: Word);
    function PrepareSourceOfflinePackageToFile(aDB: TIBDatabase; const aDBProps: TReplDatabaseProperties; aSchemaId: Integer; aGroupId, aSrcDbId, aTgtDBId: Integer; const aOfflineDir: string; aIfReplRecord: Boolean): Integer{TransferId};

    { offline replication routines - target side }
    procedure TargetOfflineBatch(aDB: TIBDatabase; const aDBProps: TReplDatabaseProperties; const aSchemaIds, aGroupIds, aTgtDBIds: TIntegerOpenArray; const aOfflineDir: string; aReceiveData, aProcess, aSendData, aResendData: Boolean; aReplOptions: Word);
    function ProcessSourceOfflinePackageAndPrepareTargetOfflinePackageToFile(aDB: TIBDatabase; const aDBProps: TReplDatabaseProperties; aSchemaId: Integer; aGroupId, aSrcDBId, aTgtDBId: Integer; const aOfflineDir: string; Src: TStream; aStampRec: TDateTime; aReplOptions: Word; var aStamp: TDateTime): Integer{TransferId};

    { offline replication routines - both sides }
    procedure DecodePackage(Src: TStream; var St: TStream);
    procedure ReceivePackages(aDB: TIBDatabase; const aDBProps: TReplDatabaseProperties; aSchemaId: Integer; aStatus: Char; aGroupId, aSrcDbId, aTgtDBId: Integer; const aOfflineDir: string);
    procedure SendPendingPackages(aDB: TIBDatabase; const aDBProps: TReplDatabaseProperties; aSchemaId: Integer; aStatus: Char; aGroupId, aSrcDbId, aTgtDBId: Integer; const aOfflineDir: string; aResendData: Boolean);
    procedure ProcessReceivedPackages(aDB: TIBDatabase; const aDBProps: TReplDatabaseProperties; aSchemaId: Integer; aStatus: Char; aGroupId, aSrcDbId, aTgtDBId: Integer; const aOfflineDir: string; aReplOptions: Word);

    { database recovery }
    procedure ExecSQLScript(aSchemaId, aGroupId, aDBId{0=source database}: Integer; aFromSeqId, aToSeqId: Integer; aSnapshot: Boolean; aFromDT, aToDT: TDateTime; aSQL: TStream; var aCntTotal, aCntProc, aCntBlob: Integer);

    { backup/clone database }
    procedure BackupDatabase(aDB: TIBDatabase; const aDatabaseName, aBackupFileName: string; aOptions: TBackupOptions = [ConvertExtTables]); overload;
    procedure RestoreDatabase(aDB: TIBDatabase; const aDatabaseName, aBackupFileName: string; aOptions: TRestoreOptions = [Replace]; aPageSize: Integer = 4096); overload;
    procedure BackupDatabase(aDBId: Integer; const aBackupFileName: string; aOptions: TBackupOptions = [ConvertExtTables]); overload;
    procedure RestoreDatabase(aDBId: Integer; const aBackupFileName: string; aOptions: TRestoreOptions = [Replace]; aPageSize: Integer = 4096); overload;
    procedure CloneSourceDatabase(aSchemaId, aGroupId, aDBId: Integer; const aMetadataOnly: Boolean);
    procedure CompressDatabase(aDBId: Integer);

    class function ScramblePassword(aPass: string; aEncode: Boolean): string;
    class function IsScrambled(const aPass: string): Boolean;

    { functions added due incompatability of IBX, IBX 5.03 changes functionality of FormatIdentifier and FormatIdentifierValue }
    class function FormatIdentifier(Dialect: Integer; Value: String): String;
    class function FormatIdentifierValue(Dialect: Integer; Value: String): String;

    class function IsReservedWord(const Value: string): Boolean;

    { xml support }
    function OfflinePackageToXML(aSrc: TStream; aOptions: Byte): UTF8String;
    function ConflictToXML(Src: TStream; aOptions: Byte): UTF8String;

    property ConfigDatabaseTransaction: TIBTransaction read fConfigDatabaseTransaction;

  end;

type
  EIBReplicatorError = class(Exception)
    Kind: Byte;
  end;

function IBDT2DT(S: string): TDateTime;
function DT2IBDT(DT: TDateTime): string;
function DT2OffStamp(DT: TDateTime): TOffStamp;
function OffStamp2DT(S: TOffStamp): TDateTime;
function NowUTC: TDateTime;
function Blob2SafeSQL(const S: string): string;
function CheckIBConnectionError(E: Exception; aDB: TIBDatabase): Boolean;
procedure CheckDatabaseName(const aDatabaseName, aDescr: string);

function IntegerToIntegerArray(const I: Integer): TIntegerOpenArray;
function NullIntegerArray: TIntegerOpenArray;
function StringArrayToIntegerArray(const aArr: TStringOpenArray): TIntegerOpenArray;
function IntegerArrayToStringArray(const aArr: TIntegerOpenArray): TStringOpenArray;
function StringToArray(S: string; aSeparator: Char): TStringOpenArray;
function ArrayToString(const aArr: TStringOpenArray; const aSep: string): string;
function IntegerArrayToSQLCondition(const aArr: TIntegerOpenArray; const aField: string; const aPrefix: string = ''; const aSuffix: string = ''): string;

procedure IBReplicatorError(const Msg: string; aKind: Byte);

const
  tranRO = 'read'#13#10'read_committed';
  tranRW = 'write'#13#10'read_committed'#13#10'rec_version'#13#10'wait';

{$I Version.inc}
  
procedure Register;

implementation
uses
  {$IFDEF REGISTRATION}IBReplicatorDeploy, {$IFDEF LINUX}{$IFNDEF CONSOLE}QForms, {$ENDIF}{$ELSE}Registry, {$ENDIF}{$ENDIF}
  IBExternals, Math, {$IFNDEF LINUX}FileCtrl, {$ENDIF}IBErrorCodes, Encoder{$IFNDEF VER130}, Variants{$ELSE}{$ENDIF};

resourcestring
  sDBConnecting = 'Connecting "%s"';
  sDBServiceConnecting = 'Connecting service "%s"';
  sReplConflictIns = 'Inserted record (#%d) does exist in target table "%s". PK: %s';
  sReplConflictUpd = 'Updated record (#%d) doesn''t exist in target table "%s". PK: %s';
  sReplConflictDel = 'Deleted record (#%d) doesn''t exist in target table "%s". PK: %s';
  sReplConflictRecord = 'Record conflict (#%d) in target table "%s", PK: %s:'#13#10'%s';
  sReplConflictRecord2 = 'Field conflict (#%d) in target table "%s", PK: %s:'#13#10'%s';
  sReplDatabaseError = 'Database error "%s" (#%d) on target table "%s". PK: %s';
  sReplDatabaseError2 = 'Database error "%s" (#%d)';
  sSyncDatabaseError = 'Database error "%s", %s';
  sSnapshotError = 'Error writing to snaphot table "%s"';
  sNoSchema = 'Schema %d not defined';
  sNoDatabase = 'Database %d not defined';
  sNoTargetGroup = 'Target group does not exist SCHEMAID:%d, GROUPID:%d';
  sNoTargetDatabase = 'Target database does not exist SCHEMAID:%d, DBID:%d';
  sNoPrimaryKey = 'No primary key for "%s"';
  sNoTransferId = 'TransferId (%d) not found';
  sNoReplUser = 'REPLUSER not defined or SCHEMADB record not found SchemaId: %d, GroupId: %d, DBId: %d';
  sNoRequiredTarget = 'Target database does not exist, SchemaId: %d, GroupId: %d, DBId: %d';
  sDBSQLExecError = 'SQL exec error "%s" in "%s"';
  sPutSQLFieldNotFound = 'Field "%s" not found in relation "%s"';
  sReplOffTotal = 'Processed: %d record(s) in %d relation(s), %d acknowledge(s), %d resend(s)';
  sReplOffTotal2 = 'Processed: total: %d, %d REPL$LOG, %d REPL$MAN record(s), received %d acknowledge(s), %d resend(s)';
  sReplOffTotal3 = 'Prepared: %d acknowledge(s)';
  sReplOffTotal4 = 'Prepared: %d reacknowledge(s)';
  sReplOffAckTotal = 'Processed: %d acknowledge(s) (%d REPL$LOG record(s)), %d 2nd acknowledge(s), %d resend(s)';
  sReplOffErr = 'Offline replication error: "%s", SeqId: %d';
  sBadOffMagicNumber = 'Bad offline package magic number';
  sBadOffPackage = 'Bad package';
  sCannotLoadTransferLib = 'Cannot load transfer library "%s"';
  sCannotLoadEncoderLib = 'Cannot load encoder library "%s"';
  sEncoderErr = 'Encoder error: "%s": "%s"';
  sNoOfflineMethod = 'No offline method, SchemaId: %d, GroupId: %d';
  sTransferSent = 'Sent %d package(s) using "%s"';
  sTransferReceive = 'Received %d package(s) using "%s"';
  sTransferProcessed = 'Processed %d package(s)';
  sSyncCount = 'TBL: %d, D:%d, U:%d, I: %d, TIME: %d sec, AVRG: %s rec/min';
  sSyncTotal = 'TOTAL: %d, D:%d, U:%d, I: %d';
  sSyncRepl = 'GroupId: %d, DBId: %d, %d-"%s", act: %x/%s';
  sReplCount = 'DB: "%s" I: %d/%d  U: %d/%d  D: %d/%d';
  sReplTotal = 'REPL$LOG: D:%d, U:%d  To REPL$MAN: I:%d, U:%d';
  sUpdateStatRelErr = 'RELATIONS statistic update error (overflow ?) (SCHEMAID:%d, GROUPID%d, RELATIONID:%d)';
  sUpdateStatSchErr = 'SCHEMATA statistic update error (overflow ?) (SCHEMAID:%d)';
  sOfflineUnexpectedPackageContent = 'Unexpected offline package content name "%s"';
  sOfflineRelationNotDefined2 = 'Relation %d not defined in database';
  sOfflineRelationNotDefined = 'Relation %d not defined in package';
  sOfflinePackageMismatch = 'Package name corrected according content "%s"->"%s".';
  sOffRemap1 = 'Relation "%s"(%d),  field "%s" not found in package';
  sOffRemap2 = 'Relation "%s"(%d),  field "%s" type mismatch %d/%d';
  sOffRemap3 = 'Relation "%s"(%d),  relation package name mismatch "%s"';
  sReplOffNoData = 'No data in REPL$LOG';
  sCannotSynchronizeToStoredProc = 'Cannot synchronize to stored procedure "%s"';
  sNoOfflineSchema = 'There is no valid offline replication schema to process';
  sLoadTransferLibrary = 'Loading "%s" transfer library';
  sSchemaIsNotReplication = 'Schema %d is not replication type';
  sDatabaseTypeRequired = '"Interbase" type of the database required';
  sCreatingObject = 'Creating %s "%s"';
  sDroppingObject = 'Dropping %s "%s"';
  sUpgradeAddField = 'Adding field "%s"."%s"';
  sUpgradeDropField = 'Dropping field "%s"."%s"';
  sUpgradeUpdateField = 'Updating field "%s"."%s"';

const
  VersionEnvironmentName = '_VERSION';
  CurVersion = '2.0.05';

type
  PComp = ^Comp;

{$IFDEF LINUX}
  {$DEFINE IBREPL_SQLVAR}
{$ELSE}
  {$IFDEF VER130}
  {$UNDEF IBREPL_SQLVAR}
  {$ELSE}
  {$DEFINE IBREPL_SQLVAR}
  {$ENDIF}
{$ENDIF}

procedure Register;
begin
  RegisterComponents('InterBase', [TIBReplicator]);
end;

var
  EmptyStringArray: array of string;
  EmptyVariantArray: array of Variant;

{$IFDEF REGISTRATION}
{$I RegClass.inc}
{$I RegIBReplicator.inc}
{$I RegCheckReg.inc}

{$IFNDEF SDK}
{$I RegIBReplicatorCheckPro.inc}
{$DEFINE REGISTRATION_CHECK_PRO}
{$ENDIF}

{$ENDIF}

type
  TAuxIBStoredProc = class(TIBStoredProc)
  public
    property SelectSQL;
  end;

procedure IBReplicatorError;
var
  E: EIBReplicatorError;
begin
  E:= EIBReplicatorError.Create(Msg);
  E.Kind:= aKind;
  raise E;
end;

procedure CheckDatabaseName(const aDatabaseName, aDescr: string);
var
  S1, S2: string;
resourcestring
  sBadDatabaseName = 'Cannot open local server database "%s" %s in non-VCL thread';
begin
  if (MainThreadId <> GetCurrentThreadId()) and (TIBReplicator.GetDBProtocol(aDatabaseName, S1, S2) = IBServices.Local) then
  begin
    S2:= aDescr;
    if S2 <> '' then
      S2:= '['+S2+']';
    IBReplicatorError(Format(sBadDatabaseName, [aDatabaseName, S2]), 0);
  end;
end;

function CheckTransferError(LibHandle: THandle; H: TTransferHandle; Err: Integer): Integer;
var
  Buff: array[1..1024] of Char;
begin
  Result:= Err;
  case Err of
    errNullHandle: IBReplicatorError(sNullTransferHandle, 1);
    errBadHandle: IBReplicatorError(sBadTransferHandle, 1);
    errErrorS:
      begin
        if tTransferError(GetProcAddress(LibHandle, 'TransferError'))(H, @Buff, SizeOf(Buff)) <> 0 then
          IBReplicatorError(StrPas(@Buff), 1);
      end;
  end;
end;

function CheckIBConnectionError(E: Exception; aDB: TIBDatabase): Boolean;
begin
  Result:= (E is EIBInterbaseError) and
           (EIBInterBaseError(E).IBErrorCode >= isc_net_init_error) and
           (EIBInterBaseError(E).IBErrorCode <= isc_net_write_err) or
           (EIBInterBaseError(E).IBErrorCode = isc_lost_db_connection);
  if Result and (aDB <> nil) then
    try
      Result:= not aDB.TestConnected;
    except
    end;
end;

function DT2OffStamp(DT: TDateTime): TOffStamp;
var
  Y, M, D, Ho, Mi, Se, S100: Word;
  function ToBCD(B: Byte): Char;
  begin
    Result:= Char(16*(B div 10)+B mod 10);
  end;
begin
  Y:= 0; M:= 0; D:= 0;
  if DT > 1 then
    DecodeDate(DT, Y, M, D);
  DecodeTime(DT, Ho, Mi, Se, S100);
  Result[1]:= ToBcd(Y div 100);
  Result[2]:= ToBcd(Y mod 100);
  Result[3]:= ToBcd(M);
  Result[4]:= ToBcd(D);
  Result[5]:= ToBcd(Ho);
  Result[6]:= ToBcd(Mi);
  Result[7]:= ToBcd(Se);
  Result[8]:= ToBcd(S100 div 100);
  Result[9]:= ToBcd(S100 mod 100);
end;

function ZeroAsNull(I: Integer): string;
begin
  if I = 0 then
    Result:= 'NULL'
  else
    Result:= IntToStr(I);
end;

function OffStamp2DT(S: TOffStamp): TDateTime;
  function FromBCD(B: Char): Word;
  begin
    Result:= (Byte(B) div 16) *10 + Byte(B) mod 16;
  end;
begin
  Result:= 0;
  try
    Result:= EncodeDate(100*FromBCD(S[1])+FromBCD(S[2]), FromBCD(S[3]), FromBCD(S[4]));
  except
  end;
  try
    Result:= Result+EncodeTime(FromBCD(S[5]), FromBCD(S[6]), FromBCD(S[7]), 100*FromBCD(S[8])+FromBCD(S[9]));
  except
  end;
end;

const
  prefSQLDT = '@';
  prefSQLSeqId = '#';
  prefSQLBLOB_Txt = 'BLOB_TXT:';
  prefSQLBLOB_Bin = 'BLOB_BIN:';  // the same length as prefSQLBLOB_Txt !!!

function FormatSQLDT(DT: TDateTime): string;
begin
  Result:= prefSQLDT+FormatDateTime('yyyymmdd hhnnss', DT);
end;

function FormatSQLSeqId(aSeqId: Integer): string;
begin
  Result:= Format('%s%d', [prefSQLSeqId, aSeqId]);
end;

function Iif(aCond: Boolean; const S1, S2: string): string;
begin
  if aCond then
    Result:= S1
  else
    Result:= S2;
end;

{$IFDEF LINUX}
function GetTickCount(): LongWord; {ms}
var
  tms: TTimes;
begin
  Result:= times(tms)*1000 div CLK_TCK{tick->ms};
end;
{$ENDIF}

function StringToArray(S: string; aSeparator: Char): TStringOpenArray;
var
  I: Integer;
begin
  SetLength(Result, 0);
  while S <> '' do
  begin
    SetLength(Result, Length(Result)+1);
    I:= Pos(aSeparator, S);
    if I = 0 then
      I:= Length(S)+1;
    Result[Length(Result)-1]:= Copy(S, 1, I-1);
    Delete(S, 1, I);
  end;
end;

function IntegerToIntegerArray(const I: Integer): TIntegerOpenArray;
begin
  SetLength(Result, 1);
  Result[0]:= I;
end;

function NullIntegerArray: TIntegerOpenArray;
begin
  SetLength(Result, 0);
end;

function IntegerArrayToStringArray(const aArr: TIntegerOpenArray): TStringOpenArray;
var
  I: Integer;
begin
  SetLength(Result, Length(aArr));
  for I:= 0 to Length(aArr)-1 do
    Result[I]:= IntToStr(aArr[I]);
end;

function StringArrayToIntegerArray(const aArr: TStringOpenArray): TIntegerOpenArray;
var
  I: Integer;
begin
  SetLength(Result, Length(aArr));
  for I:= 0 to Length(aArr)-1 do
    Result[I]:= StrToInt(aArr[I]);
end;

function ArrayToString(const aArr: TStringOpenArray; const aSep: string): string;
var
  I: Integer;
begin
  Result:= '';
  for I:= 0 to Length(aArr)-1 do
  begin
    if Result <> '' then
      Result:= Result+aSep;
    Result:= Result+aArr[I];
  end;
end;

function IntegerArrayToSQLCondition(const aArr: TIntegerOpenArray; const aField: string; const aPrefix: string = ''; const aSuffix: string = ''): string;
begin
  if Length(aArr) = 0 then
    Result:= ''
  else if Length(aArr) = 1 then
    Result:= aField+'='+IntToStr(aArr[0])
  else
    Result:= aField+' IN ('+ArrayToString(IntegerArrayToStringArray(aArr), ',')+')';
  if Result <> '' then
    Result:= aPrefix+Result+aSuffix;
end;

function IBDT2DT(S: string): TDateTime;  { 2001-10-04 14:12:49.0000 | 4-OCT-2001 .... }
  function NextSep(const S: string; var I: Integer; var It: string): Boolean;
  var
    J: Integer;
  begin
    J:= I+1;
    repeat
      Inc(I);
      Result:= (I <= Length(S)) and (S[I] in ['/','-',',',':','.']);
    until (I > Length(S)) or Result;
    if Result then
      It:= UpperCase(Copy(S, J, I-J));
  end;
var
  I: Integer;
  D, M, Y: Word;
  F: Boolean;
  It: array[1..3] of string;
const
  MonthShort: array[1..12] of string[3] =
    ('JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC');
begin
  Result:= 0;
  try
    I:= 0;
    if NextSep(S, I, It[1]) and (S[I] <> ':') then
    begin
      F:= S[I] = '/';  // mm/dd/yyyy or dd.mm.yyyy
      if NextSep(S, I, It[2]) then
      begin
        Inc(I);
        It[3]:= '';
        while I <= Length(S) do
        begin
          if not (S[I] in ['0'..'9']) then
            Break;
          It[3]:= It[3]+S[I];
          Inc(I);
        end;
        Delete(S, 1, I-1);
        if Length(It[1]) = 4 then
          begin   // yyyy-mm-dd
            Y:= StrToInt(It[1]);
            D:= StrToInt(It[3]);
          end
        else
          begin
            Y:= StrToInt(It[3]);
            if F then
              begin   // mm/dd/yyyy
                D:= StrToInt(It[2]);
                It[2]:= It[1];
              end
            else     // dd.mm.yyyy
              begin
                D:= StrToInt(It[1]);
              end;
          end;
        M:= 0;
        for I:= Low(MonthShort) to High(MonthShort) do
        begin
          if MonthShort[I] = It[2] then
          begin
            M:= I;
            Break;
          end;
        end;
        if M = 0 then
          M:= StrToInt(It[2]);
        Result:= EncodeDate(Y, M, D);
      end;
    end;
  except
  end;
  try
    S:= Trim(S);
    M:= 0;
    if Length(S) >= 8 then
      M:= StrToInt(Copy(S, 7, 2));
    Result:= Result+EncodeTime(StrToInt(Copy(S, 1, 2)), StrToInt(Copy(S, 4, 2)), M, 0);
  except
  end;
end;

function DT2IBDT(DT: TDateTime): string;
begin
  if DT = 0 then
    Result:= 'NULL'
  else
    Result:= ''''+FormatDateTime('dd.mm.yyyy hh:nn:ss', DT)+'''';
end;

function NowUTC: TDateTime;
var
{$IFDEF LINUX}
  T: TTime_T;
  TV: TTimeVal;
  UT: TUnixTime;
{$ELSE}
  SystemTime: TSystemTime;
{$ENDIF}
begin
{$IFDEF LINUX}
  gettimeofday(TV, nil);
  T := TV.tv_sec;
  gmtime_r(@T, UT);
  Result := EncodeDate(UT.tm_year + 1900, UT.tm_mon + 1, UT.tm_mday) +
    EncodeTime(UT.tm_hour, UT.tm_min, UT.tm_sec, TV.tv_usec div 1000);
{$ELSE}
  GetSystemTime(SystemTime);
  with SystemTime do
    Result := EncodeDate(wYear, wMonth, wDay)+
              EncodeTime(wHour, wMinute, wSecond, wMilliseconds);
{$ENDIF}
end;

function Blob2SafeSQL(const S: string): string;
var
  I: Integer;
begin
  Result:= '';
  for I:= 0 to Length(S) do
  begin
    if (S[I] in [#0..#31, '\']) or (S[I] = '*') and (I < Length(S)) and (S[I+1] = '/') then
      Result:= Result+'\'+IntToHex(Byte(S[I]), 2)
    else
      Result:= Result+S[I];
  end;
end;

function AddApostrophs(const S: string): string;
var
  I: Integer;
begin
  Result:= S;
  I:= 1;
  while I <= Length(Result) do
    if Result[I] in [''''] then
      begin
        Insert('''', Result, I);
        Inc(I, 2);
      end
    else
      Inc(I);
end;

function FieldToSQLString(F: TField): string;
var
  SaveDecimalSeparator, SaveThousandSeparator: Char;
begin
  if F.IsNull then
    Result:= 'NULL'
  else if F.IsBlob then
    Result:= 'NULL /*'+Blob2SafeSQL(F.asString)+'*/'
  else
    begin
      case F.DataType of
        ftDateTime:
          Result:= FormatDateTime('yyyy-mm-dd hh-nn-ss.zzz', F.AsDateTime);
        ftDate:
          Result:= FormatDateTime('yyyy-mm-dd', F.AsDateTime);
        ftTime:
          Result:= FormatDateTime('hh-nn-ss.zzz', F.AsDateTime);
        ftFloat, ftVariant:
          begin
            SaveDecimalSeparator:= DecimalSeparator;
            SaveThousandSeparator:= ThousandSeparator;
            DecimalSeparator:= '.';
            ThousandSeparator:= ',';
            try
              Result:= AddApostrophs(F.asString);
            finally
              DecimalSeparator:= SaveDecimalSeparator;
              ThousandSeparator:= SaveThousandSeparator;
            end;

          end;
      else
        Result:= AddApostrophs(F.asString);
      end;
      Result:= ''''+Result+'''';
    end;
end;

{ TIBReplicator }

constructor TIBReplicator.Create(aOwner: TComponent);
begin
  inherited;
  fCachedSchemes:= TSortedClassList.Create;
  fImplicitEnvironment:= TStringList.Create;
  fEnvironment:= TStringList.Create;
  BuildImplicitEnvironment(['DUMMY'], ['DUMMY']);  // error when called 2nd time
  fUTF8:= True;
  fConfigDatabaseTransaction:= TIBTransaction.Create(Self);
  fConfigDatabaseTransaction.Params.Text:= tranRW;
  fConfigDatabaseTransaction.DefaultAction:= TACommitRetaining;
  fConfigDatabaseTransaction.IdleTimer:= 10000;
end;

destructor TIBReplicator.Destroy;
begin
  fCachedSchemes.Free;
  fImplicitEnvironment.Free;
  fEnvironment.Free;
  inherited;
end;

function TIBReplicator.Now2;
begin
  if fNowAsUTC then
    Result:= NowUTC
  else
    Result:= Now;
end;

const
  CreateEnvironmentTable =
      '('+
      '  NAME       VARCHAR(50) NOT NULL,'+
      '  VAL      VARCHAR(%d),'+
      ' PRIMARY KEY (NAME)'+
      ')';

procedure TIBReplicator.CreateConfigDatabase;
var
  Sg: TStrings;
begin
  {$IFDEF REGISTRATION}
  TReg_CheckRegistration(False);
  {$ENDIF}
  if not fConfigDatabase.Connected then
  begin
    try
      fConfigDatabase.Open;  // call BeforeOpen
    except
      if aAllowCreate and not FileExists(fConfigDatabase.DatabaseName) then
      begin
        Sg:= TStringList.Create;
        try
          Sg.Assign(fConfigDatabase.Params);
          try
            fConfigDatabase.Params.Text:= Format('USER ''%s'' PASSWORD ''%s'' PAGE_SIZE %d DEFAULT CHARACTER SET %s', [Sg.Values['user_name'], Sg.Values['password'], 4096, Sg.Values['lc_ctype']]);
            DBLogVerbose(Format(sCreatingObject, ['DATABASE', fConfigDatabase.DatabaseName]));
            fConfigDatabase.CreateDatabase;
            fConfigDatabase.Close;
          finally
            fConfigDatabase.Params.Assign(Sg);
          end;
        finally
          Sg.Free;
        end;
        fConfigDatabase.Open;
      end;
    end;
  end;
  CreateObject(fConfigDatabase, ibobjRelation, fConfigDatabasePrefix+'DATABASES',
    '('+
    '  DBID           INTEGER NOT NULL,'+
    '  FILENAME       VARCHAR(254),'+
    '  DBTYPE         INTEGER DEFAULT 0 NOT NULL,'+
    '  NAME           VARCHAR(100),'+
    '  EXTFILEPATH    VARCHAR(254) DEFAULT '''','+
    '  ADMINUSER      VARCHAR(50) DEFAULT ''SYSDBA'','+
    '  ADMINPSW       VARCHAR(50),'+
    '  ADMINROLE      VARCHAR(50),'+
    '  CHARSET        VARCHAR(20) DEFAULT '''' NOT NULL,'+
    '  SQLDIALECT     INTEGER DEFAULT 1,'+
    '  OBJPREFIX      VARCHAR(6) DEFAULT ''REPL$'' NOT NULL,'+
    '  CUSTOMFIELDS   BLOB SUB_TYPE TEXT SEGMENT SIZE 80,'+
    '  COMMENT        BLOB SUB_TYPE TEXT SEGMENT SIZE 80,'+
    ' PRIMARY KEY (DBID)'+
    ')'
  );
  CreateObject(fConfigDatabase, ibobjRelation, fConfigDatabasePrefix+'SCHEMATA',
    '('+
    '  SCHEMAID       INTEGER NOT NULL,'+
    '  NAME           VARCHAR(100) NOT NULL,'+
    '  SCHEMATYPE     INTEGER DEFAULT 0 NOT NULL,'+
    '  S_KEEP         CHAR(1) DEFAULT ''Y'' NOT NULL,'+
    '  S_INSERT       INTEGER DEFAULT 0 NOT NULL,'+
    '  S_UPDATE       INTEGER DEFAULT 0 NOT NULL,'+
    '  S_DELETE       INTEGER DEFAULT 0 NOT NULL,'+
    '  S_CONFLICT     INTEGER DEFAULT 0 NOT NULL,'+
    '  S_ERROR        INTEGER DEFAULT 0 NOT NULL,'+
    '  S_MSEC         INTEGER DEFAULT 0 NOT NULL,'+
    '  COMMENT        BLOB SUB_TYPE TEXT SEGMENT SIZE 80,'+
    ' PRIMARY KEY (SCHEMAID)'+
    ')'
  );
  CreateObject(fConfigDatabase, ibobjRelation, fConfigDatabasePrefix+'SCHEMADB',
    '('+
    '  SCHEMAID       INTEGER NOT NULL,'+
    '  GROUPID        INTEGER NOT NULL,'+  { if master GROUPID = 0 }
    '  DBID           INTEGER NOT NULL,'+
    '  DBMASK         INTEGER DEFAULT 0 NOT NULL,'+
    '  DISABLED       CHAR(1) DEFAULT ''N'' NOT NULL,'+
    '  REPLUSER       VARCHAR(31),'+
    '  REPLROLE       VARCHAR(50),'+
    '  REPLPSW        VARCHAR(30),'+
    '  SEPARATOR      INTEGER DEFAULT 5 NOT NULL,'+
    '  OFFDBID        INTEGER,'+
    '  OFFTRANSFER    VARCHAR(10),'+
    '  OFFENCODER     VARCHAR(50),'+
    '  OFFPARAMS      BLOB SUB_TYPE TEXT SEGMENT SIZE 80,'+
    '  COMMENT        BLOB SUB_TYPE TEXT SEGMENT SIZE 80,'+
    ' PRIMARY KEY (SCHEMAID, GROUPID, DBID)'+
    ')'
  );
  CreateObject(fConfigDatabase, ibobjRelation, fConfigDatabasePrefix+'RELATIONS',
    '('+
    '  SCHEMAID       INTEGER NOT NULL,'+
    '  GROUPID        INTEGER NOT NULL,'+
    '  RELATIONID     INTEGER NOT NULL,'+
    '  RELATIONNAME   VARCHAR(100),'+
    '  TARGETNAME     VARCHAR(100),'+
    '  TARGETTYPE     CHAR(1) DEFAULT ''T'' NOT NULL,'+
    '  DISABLED       CHAR(1) DEFAULT ''N'' NOT NULL,'+
    '  OPTIONS        INTEGER DEFAULT 0 NOT NULL,'+
    '  CONDITION      BLOB SUB_TYPE TEXT SEGMENT SIZE 80,'+
    '  SYNCACTIONS    BLOB SUB_TYPE TEXT SEGMENT SIZE 80,'+
    '  SYNCORDER      INTEGER,'+
    '  S_KEEP         CHAR(1) DEFAULT ''Y'' NOT NULL,'+
    '  S_INSERT       INTEGER DEFAULT 0 NOT NULL,'+
    '  S_UPDATE       INTEGER DEFAULT 0 NOT NULL,'+
    '  S_DELETE       INTEGER DEFAULT 0 NOT NULL,'+
    '  S_CONFLICT     INTEGER DEFAULT 0 NOT NULL,'+
    '  S_ERROR        INTEGER DEFAULT 0 NOT NULL,'+
    '  S_MSEC         INTEGER DEFAULT 0 NOT NULL,'+
    '  COMMENT        BLOB SUB_TYPE TEXT SEGMENT SIZE 80,'+
    ' PRIMARY KEY (SCHEMAID, GROUPID, RELATIONID)'+
    ')'
  );
  CreateObject(fConfigDatabase, ibobjRelation, fConfigDatabasePrefix+'FIELDS',
    '('+
    '  SCHEMAID       INTEGER NOT NULL,'+
    '  GROUPID        INTEGER NOT NULL,'+
    '  RELATIONID     INTEGER NOT NULL,'+
    '  FIELDID        INTEGER NOT NULL,'+
    '  FIELDNAME      VARCHAR(100),'+
    '  FIELDTYPE      INTEGER NOT NULL,'+
    '  TARGETNAME     VARCHAR(100),'+
    '  OPTIONS        INTEGER DEFAULT 0 NOT NULL,'+
    ' PRIMARY KEY (SCHEMAID, GROUPID, RELATIONID, FIELDID)'+
    ')'
  );
  if CreateObject(fConfigDatabase, ibobjRelation, fConfigDatabasePrefix+'ENVIRONMENT', Format(CreateEnvironmentTable, [GetMaxVARCHARlength(fConfigDatabase, fConfigDatabasePrefix, 255, 1024)])) then
    DBEnvironmentWriteValue(fConfigDatabase, fConfigDatabasePrefix, VersionEnvironmentName, CurVersion);
end;

procedure TIBReplicator.UpgradeConfigDatabase;
var
  Ver, TypeS: string;
  L, SubType, Scale, I: Integer;
  Q: TIBSQL;
  Acts: array of record
    SchemaId, GroupId, RelationId: Integer;
    Action: string;
  end;
begin
  ConfigDatabase.Open;
  Ver:= DBEnvironmentReadValue(fConfigDatabase, fConfigDatabasePrefix, VersionEnvironmentName, '');
// 1.x -> 2.0
//  if Ver <> CurVersion then
  begin
    if not RelationFieldExists(fConfigDatabase, fConfigDatabasePrefix+'DATABASES', 'DBTYPE') then
    begin
      DBLogVerbose(Format(sUpgradeAddField, [fConfigDatabasePrefix+'DATABASES', 'DBTYPE']));
      DBSQLExec(fConfigDatabase,
        'ALTER TABLE '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'DATABASES')+' '+
          'ADD DBTYPE         INTEGER DEFAULT 0 NOT NULL'
      );
    end;
    if not RelationFieldExists(fConfigDatabase, fConfigDatabasePrefix+'DATABASES', 'CUSTOMFIELDS') then
    begin
      DBLogVerbose(Format(sUpgradeAddField, [fConfigDatabasePrefix+'DATABASES', 'CUSTOMFIELDS']));
      DBSQLExec(fConfigDatabase,
        'ALTER TABLE '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'DATABASES')+' '+
          'ADD CUSTOMFIELDS   BLOB SUB_TYPE TEXT SEGMENT SIZE 80'
      );
    end;
    if not RelationFieldExists(fConfigDatabase, fConfigDatabasePrefix+'DATABASES', 'EXTFILEPATH') then
    begin
      DBLogVerbose(Format(sUpgradeAddField, [fConfigDatabasePrefix+'DATABASES', 'EXTFILEPATH']));
      DBSQLExec(fConfigDatabase,
        'ALTER TABLE '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'DATABASES')+' '+
          'ADD EXTFILEPATH    VARCHAR(254) DEFAULT '''''
      );
    end;
    if not RelationFieldExists(fConfigDatabase, fConfigDatabasePrefix+'SCHEMATA', 'SCHEMATYPE') then
    begin
      DBLogVerbose(Format(sUpgradeAddField, [fConfigDatabasePrefix+'SCHEMATA', 'SCHEMATYPE']));
      DBSQLExec(fConfigDatabase,
        'ALTER TABLE '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'SCHEMATA')+' '+
          'ADD SCHEMATYPE     INTEGER DEFAULT 0 NOT NULL'
      );
    end;
    if not RelationFieldExists(fConfigDatabase, fConfigDatabasePrefix+'FIELDS', 'OPTIONS') then
    begin
      DBLogVerbose(Format(sUpgradeAddField, [fConfigDatabasePrefix+'FIELDS', 'OPTIONS']));
      DBSQLExec(fConfigDatabase,
        'ALTER TABLE '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'FIELDS')+' '+
          'ADD OPTIONS INTEGER DEFAULT 0 NOT NULL'
      );
    end;
    if RelationFieldExists(fConfigDatabase, fConfigDatabasePrefix+'FIELDS', 'UPDATEDISABLED') then
    begin
      DBLogVerbose(Format(sUpgradeUpdateField, [fConfigDatabasePrefix+'FIELDS', 'OPTIONS']));
      DBSQLExec(fConfigDatabase,
        'UPDATE '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'FIELDS')+' '+
          'SET OPTIONS=1 WHERE UPDATEDISABLED=''Y'' AND OPTIONS=0'
      );
      DBLogVerbose(Format(sUpgradeDropField, [fConfigDatabasePrefix+'FIELDS', 'UPDATEDISABLED']));
      DBSQLExec(fConfigDatabase,
        'ALTER TABLE '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'FIELDS')+' '+
          'DROP UPDATEDISABLED'
      );
    end;
    if not RelationFieldExists(fConfigDatabase, fConfigDatabasePrefix+'RELATIONS', 'OPTIONS') then
    begin
      DBLogVerbose(Format(sUpgradeAddField, [fConfigDatabasePrefix+'RELATIONS', 'OPTIONS']));
      DBSQLExec(fConfigDatabase,
        'ALTER TABLE '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'RELATIONS')+' '+
          'ADD OPTIONS INTEGER DEFAULT 0 NOT NULL'
      );
    end;
    if RelationFieldExists(fConfigDatabase, fConfigDatabasePrefix+'RELATIONS', 'WIPELOGONDELETE') then
    begin
      DBLogVerbose(Format(sUpgradeUpdateField, [fConfigDatabasePrefix+'RELATIONS', 'OPTIONS']));
      DBSQLExec(fConfigDatabase,
        'UPDATE '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'RELATIONS')+' '+
          'SET OPTIONS=1 WHERE WIPELOGONDELETE=''Y'' AND OPTIONS=0'
      );
      DBLogVerbose(Format(sUpgradeDropField, [fConfigDatabasePrefix+'RELATIONS', 'WIPELOGONDELETE']));
      DBSQLExec(fConfigDatabase,
        'ALTER TABLE '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'RELATIONS')+' '+
          'DROP WIPELOGONDELETE'
      );
    end;

    if RelationFieldExists(fConfigDatabase, fConfigDatabasePrefix+'RELATIONS', 'SYNCACTIONS') then
    begin
      GetRelationFieldType(fConfigDatabase, fConfigDatabasePrefix+'RELATIONS', 'SYNCACTIONS', TypeS, L, Scale, SubType);
      if TypeS <> 'BLOB' then
      begin
        SetLength(Acts, 0);
        Q:= TIBSQL.Create(nil);
        try
          Q.Database:= fConfigDatabase;
          Q.Transaction:= Q.Database.DefaultTransaction;
          Q.SQL.Text:= 'SELECT * FROM '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'RELATIONS')+' WHERE SYNCACTIONS IS NOT NULL AND SYNCACTIONS<>''''';
          SafeStartTransaction(Q.Transaction);
          try
            SafeExecQuery(Q);
            while not Q.EOF do
            begin
              if Trim(Q.FieldByName('SYNCACTIONS').asString) <> '' then
              begin
                SetLength(Acts, Length(Acts)+1);
                with Acts[Length(Acts)-1] do
                begin
                  SchemaId:= Q.FieldByName('SCHEMAID').asInteger;
                  GroupId:= Q.FieldByName('GROUPID').asInteger;
                  RelationId:= Q.FieldByName('RELATIONID').asInteger;
                  Action:= Q.FieldByName('SYNCACTIONS').asString;
                end;
              end;
              SafeNext(Q);
            end;
            SafeClose(Q);
            DBLogVerbose(Format(sUpgradeDropField, [fConfigDatabasePrefix+'RELATIONS', 'SYNCACTIONS']));
            DBSQLExec(fConfigDatabase,
              'ALTER TABLE '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'RELATIONS')+' '+
                'DROP SYNCACTIONS'
            );
            DBLogVerbose(Format(sUpgradeDropField, [fConfigDatabasePrefix+'RELATIONS', 'SYNCACTIONS']));
            DBSQLExec(fConfigDatabase,
              'ALTER TABLE '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'RELATIONS')+' '+
                'ADD SYNCACTIONS BLOB SUB_TYPE TEXT SEGMENT SIZE 80'
            );

            SafeCommit(Q.Transaction, False);
            SafeStartTransaction(Q.Transaction); // restarting necessary, otherwise truncation error

            DBLogVerbose(Format(sUpgradeUpdateField, [fConfigDatabasePrefix+'RELATIONS', 'SYNCACTIONS']));
            Q.SQL.Text:= 'UPDATE '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'RELATIONS')+' SET SYNCACTIONS=:SyncActions WHERE SCHEMAID=:SchemaId AND GROUPID=:GroupId AND RELATIONID=:RelationId';
            Q.Prepare;
            for I:= 0 to Length(Acts)-1 do
              with Acts[I] do
              begin
                Q.Params.ByName('SchemaId').asInteger:= SchemaId;
                Q.Params.ByName('GroupId').asInteger:= GroupId;
                Q.Params.ByName('RelationId').asInteger:= RelationId;
                Q.Params.ByName('SyncActions').asString:= Action;
                SafeExecQuery(Q);
              end;
            SafeCommit(Q.Transaction, False);
          except
            SafeRollback(Q.Transaction, False);
            raise;
          end;
        finally
          Q.Free;
        end;
      end;
    end;
    if RelationFieldExists(fConfigDatabase, fConfigDatabasePrefix+'SCHEMADB', 'SYNCDISABLED') then
    begin
      DBLogVerbose(Format(sUpgradeDropField, [fConfigDatabasePrefix+'SCHEMADB', 'SYNCDISABLED']));
      DBSQLExec(fConfigDatabase,
        'ALTER TABLE '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'SCHEMADB')+' '+
          'DROP SYNCDISABLED'
      );
    end;
    if Ver = '' then
    begin
      DBLogVerbose(Format(sUpgradeUpdateField, [fConfigDatabasePrefix+'DBTYPE', 'DATABASES']));
      DBSQLExec(fConfigDatabase,
        'UPDATE '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'DATABASES')+' '+
        '  SET DBTYPE = 1 WHERE UPPER(FILENAME) NOT LIKE ''%GDB'' AND UPPER(FILENAME) NOT LIKE ''%FDB'''
      );
    end;

    CreateConfigDatabase(False);
    DBEnvironmentWriteValue(fConfigDatabase, fConfigDatabasePrefix, VersionEnvironmentName, CurVersion);
  end;
end;

function TIBReplicator.CheckConfigDatabaseVersion;
var
  Ver: string;
begin
  Result:= verNonUpgradable;
  Ver:= DBEnvironmentreadValue(fConfigDatabase, fConfigDatabasePrefix, VersionEnvironmentName, '');
  if (Ver = CurVersion) or (Ver >= '2.0.05') then
    Result:= verCurrent
  else if (Ver = '') or (Ver >= '2.0.01') then
    Result:= verUpgradable;
end;

function TIBReplicator.DBSQLExec;
var
  Q: TIBSQL;
  F: Boolean;
begin
  Result:= 0;
  Q:= TIBSQL.Create(nil);
  try
    Q.Database:= aDB;
    if aTrans = nil then
      begin
        if Q.Database = fConfigDatabase then
          Q.Transaction:= fConfigDatabaseTransaction
        else
          Q.Transaction:= aDB.DefaultTransaction
      end
    else
      Q.Transaction:= aTrans;
    Q.ParamCheck:= False;
    Q.SQL.Text:= aSQL;
    F:= Q.Transaction.InTransaction;
    if not F then
      SafeStartTransaction(Q.Transaction);
    try
      SafeExecQuery(Q);
      if not F then
        SafeCommit(Q.Transaction);
    except
      if not F then
        SafeRollback(Q.Transaction);
      raise;
    end;
    Result:= Q.RowsAffected;
  finally
    Q.Free;
  end;
end;

function TIBReplicator.DBSQLRecord;
var
  Q: TIBSQL;
  F: Boolean;
  I: Integer;
  function GetAsVar(F: TIBXSQLVAR): Variant;
  begin
    case F.{$IFDEF IBREPL_SQLVAR}SQLVar{$ELSE}asXSQLVar{$ENDIF}.sqltype and (not 1) of
      SQL_BLOB:
        Result := F.asString; {do not localize}
    else
      Result:= F.Value;
    end;
  end;
begin
  Result:= Null;
  Q:= TIBSQL.Create(nil);
  try
    Q.Database:= aDB;
    if aTrans = nil then
      begin
        if Q.Database = fConfigDatabase then
          Q.Transaction:= fConfigDatabaseTransaction
        else
          Q.Transaction:= aDB.DefaultTransaction
      end
    else
      Q.Transaction:= aTrans;
    Q.SQL.Text:= aSQL;
    F:= Q.Transaction.InTransaction;
    if not F then
      SafeStartTransaction(Q.Transaction);
    try
      SafeExecQuery(Q);
      if Q.RecordCount > 0 then
        if Q.Current.Count = 1 then
          Result:= GetAsVar(Q.Fields[0])
        else if Q.Current.Count > 1 then
          begin
            Result:= VarArrayCreate([0, Q.Current.Count-1], varVariant);
            for I:= 0 to Q.Current.Count-1 do
              Result[I]:= GetAsVar(Q.Fields[I]);
          end;
      if not F then
        SafeCommit(Q.Transaction);
    except
      if not F then
        SafeRollback(Q.Transaction);
      raise;
    end;
  finally
    Q.Free;
  end;
end;

function StripLastCRLF(S: string): string;
begin
  Result:= S;
  while (Result <> '') and (Result[Length(Result)] in [#13,#10]) do
    Delete(Result, Length(Result), 1);
end;

function FormatParamValue(const S: string): string;
var
  I: Integer;
begin
  Result:= '';
  for I:= 1 to Length(S) do
    if S[I] in [#0..#31] then
      Result:= Result+Format('\x%.2x', [Byte(S[I])])
    else if S[I] in ['''', '\'] then
      Result:= Result+'\'+S[I]
    else
      Result:= Result+S[I];
end;

procedure TIBReplicator.DBLogVerbose(const S: string);
begin
  if fLogVerbose then
    fDBLog.Log(fLogName, lchNull, S);
end;

procedure TIBReplicator.LogParams(aParams: TIBXSQLDA);
var
  I: Integer;
  S, S2: string;
resourcestring
  sParam = '"%s"=%s';
begin
  S2:= '';
  if aParams.Count > 0 then
    for I:= 0 to aParams.Count-1 do
      with aParams[I] do
      begin
        if S2 <> '' then
          S2:= S2+',';
        if isNull then
          S:= 'NULL'
        else
          S:= ''''+FormatParamValue(asString)+'''';
        S2:= S2+Format(sParam, [Name, S]);
      end;
  if S2 <> '' then
    fDBLog.Log(fLogName, lchError, S2);
end;

procedure TIBReplicator.SafeNext(Q: TIBSQL);
begin
  try
    Q.Next;
  except
    on E: Exception do
    begin
      CheckIBConnectionError(E, Q.Database);
      raise;
    end;
  end;
end;

procedure TIBReplicator.SafeNext(Q: TIBDataSet);
begin
  try
    Q.Next;
  except
    on E: Exception do
    begin
      CheckIBConnectionError(E, Q.Database);
      raise;
    end;
  end;
end;

procedure TIBReplicator.SafeOpen(Q: TIBDataSet);
begin
  try
    Q.Open;
  except
    on E: Exception do
    begin
      CheckIBConnectionError(E, Q.Database);
      if fLogErrSQLCmds then
        fDBLog.Log(fLogName, lchError, Format(sDBSQLExecError, [E.Message, StripLastCRLF(Q.SelectSQL.Text)]));
      if fLogErrSQLParams and Q.Prepared then
      begin
        LogParams(Q.Params);
      end;
      raise;
    end;
  end;
end;

procedure TIBReplicator.SafeClose(Q: TIBDataSet; aDoNotRaise: Boolean=True);
begin
  try
    Q.Close;
  except
    on E: Exception do
    begin
      if not CheckIBConnectionError(E, Q.Database) or not aDoNotRaise then
        raise;
    end;
  end;
end;

procedure TIBReplicator.SafeClose(Q: TIBSQL; aDoNotRaise: Boolean=True);
begin
  try
    Q.Close;
  except
    on E: Exception do
    begin
      if not CheckIBConnectionError(E, Q.Database) or not aDoNotRaise then
        raise;
    end;
  end;
end;

procedure TIBReplicator.SafeExecQuery;
begin
  try
    Q.ExecQuery;
  except
    on E: Exception do
    begin
      CheckIBConnectionError(E, Q.Database);
      if fLogErrSQLCmds then
        fDBLog.Log(fLogName, lchError, Format(sDBSQLExecError, [E.Message, StripLastCRLF(Q.SQL.Text)]));
      if fLogErrSQLParams and Q.Prepared then
        LogParams(Q.Params);
      raise;
    end;
  end;
end;

procedure TIBReplicator.SafeStartTransaction(T: TIBTransaction);
begin
  try
    T.StartTransaction;
  except
    on E: Exception do
    begin
      CheckIBConnectionError(E, T.DefaultDatabase);
      raise;
    end;
  end;
end;

procedure TIBReplicator.SafeCommit(T: TIBTransaction; aRetaining: Boolean=False);
begin
  if not T.InTransaction then   { SafeCommit widely used in except/finally statement, if connection is lost then transaction is internally closed, in this case real reason would have changed to "Transation is not opened" }
    Exit;
  try
    if aRetaining then
      T.CommitRetaining
    else
      T.Commit;
  except
    on E: Exception do
    begin
      CheckIBConnectionError(E, T.DefaultDatabase);
      raise;
    end;
  end;
end;

procedure TIBReplicator.SafeRollback(T: TIBTransaction; aRetaining: Boolean=False; aDoNotRaise: Boolean=True);
begin
  if not T.InTransaction then   { SafeRollback widely used in except/finally statement, if connection is lost then transaction is internally closed, in this case real reason would have changed to "Transation is not opened" }
    Exit;
  try
    if aRetaining then
      T.RollbackRetaining
    else
      T.Rollback;
  except
    on E: Exception do
    begin
      if not CheckIBConnectionError(E, T.DefaultDatabase) or not aDoNotRaise then
        raise;
    end;
  end;
end;

procedure TIBReplicator.SafeExecProc;
  procedure LogParams(aParams: TParams);
  var
    I: Integer;
    S, S2: string;
  const
    ParS: array[TParamType] of string = ('U', 'I', 'O', 'I/O', 'R');
  resourcestring
    sParam = '"%s":%s=%s';
  begin
    S2:= '';
    if aParams.Count > 0 then
      for I:= 0 to aParams.Count-1 do
        with aParams[I] do
        begin
          if S2 <> '' then
            S2:= S2+',';
          if isNull then
            S:= 'NULL'
          else
            S:= ''''+FormatParamValue(asString)+'''';
          S2:= S2+Format(sParam, [Name, ParS[ParamType], S]);
        end;
    if S2 <> '' then
      fDBLog.Log(fLogName, lchError, S2);
  end;
begin
  try
    Q.ExecProc;
  except
    on E: Exception do
    begin
      CheckIBConnectionError(E, Q.Database);
      if fLogErrSQLCmds then
        fDBLog.Log(fLogName, lchError, Format(sDBSQLExecError, [E.Message, StripLastCRLF(TAuxIBStoredProc(Q).SelectSQL.Text)]));
      if fLogErrSQLParams and Q.Prepared then
      begin
        LogParams(Q.Params);
      end;
      raise;
    end;
  end;
end;

procedure TIBReplicator.GenerateFields;
var
  Q1, Q2, RMQ: TIBSQL;
  DB: TIBDatabase;
  DBId, RelId: Integer;
  DBProps: TReplDatabaseProperties;
  V: Variant;
  SchemaType: Integer;

  procedure InsertFields(Q: TIBSQL; aRelId: Integer; const aRelName: string; aFieldType: Byte);
  var
    V: Variant;
  resourcestring
    sInsertField = 'New field: "%s"."%s", Type: %d';
    sUpdateField = 'Update field: "%s"."%s", Type: %d';
  begin
    while not Q.EOF do
    begin
      if DBSQLRecord(fConfigDatabase, Format('SELECT CAST(COUNT(*) AS INTEGER) FROM %s F WHERE F.SCHEMAID=%d AND F.GROUPID=%d AND F.RELATIONID=''%d'' AND F.FIELDNAME=''%s''', [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'FIELDS'), aSchemaId, aTargetGroupId, aRelId, TrimRight(Q.Fields[0].asString)])) = 0 then
        begin
          V:= DBSQLRecord(fConfigDatabase, Format('SELECT MAX(FIELDID) FROM %s WHERE SCHEMAID=%d AND GROUPID=%d AND RELATIONID=%d', [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'FIELDS'), aSchemaId, aTargetGroupId, aRelId]));
          if VarIsNull(V) then
            V:= 0;
          DBLogVerbose(Format(sInsertField, [aRelName, TrimRight(Q.Fields[0].asString), aFieldType]));
          DBSQLExec(fConfigDatabase, Format('INSERT INTO %s(SCHEMAID,RELATIONID,FIELDID,FIELDNAME,FIELDTYPE,GROUPID,TARGETNAME) VALUES (%d,%d,%d,''%s'',%d,%d,%s)', [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'FIELDS'), aSchemaId, aRelId, Integer(V)+1, TrimRight(Q.Fields[0].asString), aFieldType, aTargetGroupId, iif(SchemaType = schtRecordHistory, 'NULL', ''''+TrimRight(Q.Fields[0].asString)+'''')]));
        end
      else
        begin
          // adjust field type if worser than possible
          if DBSQLExec(fConfigDatabase, Format('UPDATE %s F SET FIELDTYPE=%d WHERE F.SCHEMAID=%d AND F.GROUPID=%d AND F.RELATIONID=''%d'' AND F.FIELDNAME=''%s'' AND F.FIELDTYPE>%d', [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'FIELDS'), aFieldType, aSchemaId, aTargetGroupId, aRelId, TrimRight(Q.Fields[0].asString), aFieldType])) > 0 then
            DBLogVerbose(Format(sUpdateField, [aRelName, TrimRight(Q.Fields[0].asString), aFieldType]));
        end;
      SafeNext(Q);
    end;
  end;
resourcestring
  sInsertRelation = 'New realation: "%s"';
begin
{$IFDEF REGISTRATION}
  TReg_CheckRegistration(False);
{$ENDIF}
  SafeStartTransaction(fConfigDatabaseTransaction);
  try
    DBId:= GetSourceDBId(aSchemaId);
    SchemaType:= GetSchemaType(aSchemaId);
    if SchemaType = schtRecordHistory then
      aTargetGroupId:= 0
    else
      begin
        V:= DBSQLRecord(fConfigDatabase, Format('SELECT CAST(COUNT(*) AS INTEGER) FROM %s WHERE SCHEMAID=%d AND GROUPID=%d AND GROUPID<>0', [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'SCHEMADB'), aSchemaId, aTargetGroupId]));
        if Integer(V) = 0 then
          IBReplicatorError(Format(sNoTargetGroup, [aSchemaId, aTargetGroupId]), 2);
      end;
    DB:= TIBDatabase.Create(nil);
    try
      SetDBParams(DB, DBId, DBProps);
      SafeStartTransaction(DB.DefaultTransaction);
      try
        Q1:= TIBSQL.Create(nil);
        try
          Q1.Database:= DB;
          Q2:= TIBSQL.Create(nil);
          try
            Q2.Database:= DB;
            RMQ:= TIBSQL.Create(nil);
            try
              RMQ.Database:= fConfigDatabase;
              RMQ.Transaction:= fConfigDatabaseTransaction;
              { select all tables }
              Q1.SQL.Add('SELECT RL.RDB$RELATION_NAME');
              Q1.SQL.Add('FROM RDB$RELATIONS RL');
              Q1.SQL.Add(Format('WHERE RL.RDB$SYSTEM_FLAG=0 AND RL.RDB$RELATION_NAME NOT LIKE ''%s%%'' AND RL.RDB$VIEW_SOURCE IS NULL', [FormatIdentifierValue(DB.SQLDialect, DBProps.ObjPrefix)]));
              SafeExecQuery(Q1);
              while not Q1.EOF do
              begin
                if DBSQLRecord(fConfigDatabase, Format('SELECT CAST(COUNT(*) AS INTEGER) FROM %s R WHERE R.SCHEMAID=%d AND R.GROUPID=%d AND R.RELATIONNAME=''%s''', [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'RELATIONS'), aSchemaId, aTargetGroupId, TrimRight(Q1.Fields[0].asString)])) = 0 then
                begin
                  { insert relation }
                  V:= DBSQLRecord(fConfigDatabase, Format('SELECT MAX(R.RELATIONID) FROM %s R WHERE R.SCHEMAID=%d AND R.GROUPID=%d', [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'RELATIONS'), aSchemaId, aTargetGroupId]));
                  if VarIsNull(V) then
                    V:= 0;
                  DBLogVerbose(Format(sInsertRelation, [TrimRight(Q1.Fields[0].asString)]));
                  DBSQLExec(fConfigDatabase, Format('INSERT INTO %s(SCHEMAID,RELATIONID,RELATIONNAME,GROUPID,TARGETNAME) VALUES (%d,%d,''%s'',%d,%s)', [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'RELATIONS'), aSchemaId, Integer(V)+1, TrimRight(Q1.Fields[0].asString), aTargetGroupId, iif(SchemaType = schtRecordHistory, 'NULL', ''''+TrimRight(Q1.Fields[0].asString)+'''')]));
                end;
                RelId:= DBSQLRecord(fConfigDatabase, Format('SELECT R.RELATIONID FROM %s R WHERE R.SCHEMAID=%d AND R.GROUPID=%d AND R.RELATIONNAME=''%s''', [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'RELATIONS'), aSchemaId, aTargetGroupId, TrimRight(Q1.Fields[0].asString)]));
                { select all primary key }
                SafeClose(Q2);
                Q2.SQL.Clear;
                Q2.SQL.Add('SELECT DISTINCT ISG.RDB$FIELD_NAME');
                Q2.SQL.Add('FROM RDB$RELATION_CONSTRAINTS RLC,RDB$INDICES ID,RDB$INDEX_SEGMENTS ISG');
                Q2.SQL.Add('WHERE RLC.RDB$CONSTRAINT_TYPE=''PRIMARY KEY''');
                Q2.SQL.Add(Format('AND RLC.RDB$INDEX_NAME=ID.RDB$INDEX_NAME AND ID.RDB$INDEX_NAME=ISG.RDB$INDEX_NAME AND ID.RDB$RELATION_NAME=''%s''', [TrimRight(Q1.Fields[0].asString)]));
                Q2.SQL.Add('ORDER BY RLC.RDB$CONSTRAINT_NAME,ISG.RDB$FIELD_POSITION;');
                SafeExecQuery(Q2);
                InsertFields(Q2, RelId, TrimRight(Q1.Fields[0].asString), 1);
                { select all foreign keys, except PK }
                SafeClose(Q2);
                Q2.SQL[2]:= 'WHERE RLC.RDB$CONSTRAINT_TYPE=''FOREIGN KEY''';
                SafeExecQuery(Q2);
                InsertFields(Q2, RelId, TrimRight(Q1.Fields[0].asString), 2);
                { select all data fields, except PK&FK }
                SafeClose(Q2);
                Q2.SQL.Clear;
                Q2.SQL.Add('SELECT DISTINCT RF.RDB$FIELD_NAME');
                Q2.SQL.Add('FROM RDB$RELATION_FIELDS RF INNER JOIN RDB$FIELDS F ON RF.RDB$FIELD_SOURCE=F.RDB$FIELD_NAME');
                Q2.SQL.Add(Format('WHERE RF.RDB$RELATION_NAME=''%s'' AND F.RDB$COMPUTED_SOURCE IS NULL', [TrimRight(Q1.Fields[0].asString)]));
                Q2.SQL.Add('ORDER BY RF.RDB$FIELD_POSITION;');
                SafeExecQuery(Q2);
                InsertFields(Q2, RelId, TrimRight(Q1.Fields[0].asString), 3);
                SafeNext(Q1);
              end;
            finally
              RMQ.Free;
            end;
          finally
            Q2.Free;
          end;
        finally
          Q1.Free;
        end;
        SafeCommit(DB.DefaultTransaction);
      except
        SafeRollback(DB.DefaultTransaction);
        raise;
      end;
    finally
      DB.Free;
    end;
    SafeCommit(fConfigDatabaseTransaction);
  except
    SafeRollback(fConfigDatabaseTransaction);
    raise;
  end;
end;

procedure TIBReplicator.RemoveOrphanedFields;
var
  V: Variant;
  Q1, Q2: TIBSQL;
  DB: TIBDatabase;
  DBId: Integer;
  DBProps: TReplDatabaseProperties;
  SchemaType: Integer;
resourcestring
  sRemoveRelation = 'Delete realation: "%s"';
  sRemoveField = 'Delete realation: "%s", field: "%s"';
begin
{$IFDEF REGISTRATION}
  TReg_CheckRegistration(False);
{$ENDIF}
  SafeStartTransaction(fConfigDatabaseTransaction);
  try
    DBId:= GetSourceDBId(aSchemaId);
    SchemaType:= GetSchemaType(aSchemaId);
    if SchemaType = schtRecordHistory then
      aTargetGroupId:= 0
    else
      begin
        V:= DBSQLRecord(fConfigDatabase, Format('SELECT CAST(COUNT(*) AS INTEGER) FROM %s WHERE SCHEMAID=%d AND GROUPID=%d AND GROUPID<>0', [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'SCHEMADB'), aSchemaId, aTargetGroupId]));
        if Integer(V) = 0 then
          IBReplicatorError(Format(sNoTargetGroup, [aSchemaId, aTargetGroupId]), 2);
      end;
    DB:= TIBDatabase.Create(nil);
    try
      SetDBParams(DB, DBId, DBProps);
      SafeStartTransaction(DB.DefaultTransaction);
      try
        Q1:= TIBSQL.Create(nil);
        try
          Q1.Database:= fConfigDatabase;
          Q1.Transaction:= fConfigDatabaseTransaction;
          Q1.SQL.Add(Format('SELECT * FROM %s WHERE SCHEMAID=%d AND GROUPID=%d', [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'RELATIONS'), aSchemaId, aTargetGroupId]));
          Q2:= TIBSQL.Create(nil);
          try
            Q2.Database:= Q1.Database;
            Q2.Transaction:= Q1.Transaction;
            Q2.SQL.Add(Format('SELECT * FROM %s', [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'FIELDS')]));
            Q2.SQL.Add('');
            SafeExecQuery(Q1);
            while not Q1.EOF do
            begin
              if not RelationExists(DB, Q1.FieldByName('RELATIONNAME').asString) then
                begin
                  DBLogVerbose(Format(sRemoveRelation, [Q1.FieldByName('RELATIONNAME').asString]));
                  DBSQLExec(fConfigDatabase, Format('DELETE FROM %s WHERE SCHEMAID=%d AND GROUPID=%d AND RELATIONID=%d', [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'RELATIONS'), aSchemaId, aTargetGroupId, Q1.FieldByName('RELATIONID').asInteger]));
                  DBSQLExec(fConfigDatabase, Format('DELETE FROM %s WHERE SCHEMAID=%d AND GROUPID=%d AND RELATIONID=%d', [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'FIELDS'), aSchemaId, aTargetGroupId, Q1.FieldByName('RELATIONID').asInteger]));
                end
              else
                begin
                  Q2.SQL[1]:= Format('WHERE SCHEMAID=%d AND GROUPID=%d AND RELATIONID=%d', [aSchemaId, aTargetGroupId, Q1.FieldByName('RELATIONID').asInteger]);
                  SafeExecQuery(Q2);
                  while not Q2.EOF do
                  begin
                    if not RelationFieldExists(DB, Q1.FieldByName('RELATIONNAME').asString, Q2.FieldByName('FIELDNAME').asString) then
                    begin
                      DBLogVerbose(Format(sRemoveField, [Q1.FieldByName('RELATIONNAME').asString, Q2.FieldByName('FIELDNAME').asString]));
                      DBSQLExec(fConfigDatabase, Format('DELETE FROM %s WHERE SCHEMAID=%d AND GROUPID=%d AND RELATIONID=%d AND FIELDID=%d',
                        [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'FIELDS'), aSchemaId, aTargetGroupId, Q2.FieldByName('RELATIONID').asInteger, Q2.FieldByName('FIELDID').asInteger]));
                    end;
                    SafeNext(Q2);
                  end;
                  SafeClose(Q2);
                end;
              SafeNext(Q1);
            end;
            SafeClose(Q1);
          finally
            Q2.Free;
          end;
        finally
          Q1.Free;
        end;
        SafeCommit(DB.DefaultTransaction);
      except
        SafeRollback(DB.DefaultTransaction);
        raise;
      end;
    finally
      DB.Free;
    end;
    SafeCommit(fConfigDatabaseTransaction);
  except
    SafeRollback(fConfigDatabaseTransaction);
    raise;
  end;
end;

procedure TIBReplicator.ValidateSchema(aSchemaId, aTargetGroupId: Integer; aTargetDBId: Integer = 0);
var
  V: Variant;
  Q1, Q2: TIBSQL;
  DB1, DB2: TIBDatabase;
  DBId: Integer;
  DB1Props, DB2Props: TReplDatabaseProperties;
  SchemaType, PKeyNum: Integer;
  RelFlag, Rel1Ex, Rel2Ex, FldFlag: Boolean;

resourcestring
  sValidatingSchema = 'Validating schema: "%s"';
  sRelation = 'Relation: "%s"-"%s"';
  sField = 'Field: "%s"-"%s"';
  sNoFields = '! No fields';
  sNoPrimaryFields = '! No primary field(s)';
  sMissingInDatabase = '! Missing in %s database "%s"';
  sSource = 'source';
  sTarget = 'target';

  procedure LogRelName(Q1: TIBSQL);
  begin
    if not RelFlag then
      fDBLog.Log(fLogName, lchNull, '  '+Format(sRelation, [Q1.FieldByName('RELATIONNAME').asString, Q1.FieldByName('TARGETNAME').asString]));
    RelFlag:= True;
  end;
  procedure LogFldName(Q2: TIBSQL);
  begin
    if not FldFlag then
      fDBLog.Log(fLogName, lchNull, '    '+Format(sField, [Q2.FieldByName('FIELDNAME').asString, Q2.FieldByName('TARGETNAME').asString]));
    FldFlag:= True;
  end;
  procedure LogMissingInDb(const aIndent: string; const S: string; const aDBProps: TReplDatabaseProperties);
  begin
    fDBLog.Log(fLogName, lchNull, aIndent+Format(sMissingInDatabase, [S, aDBProps.DBName]));
  end;

begin
{$IFDEF REGISTRATION}
  TReg_CheckRegistration(False);
{$ENDIF}
  SafeStartTransaction(fConfigDatabaseTransaction);
  try
    DBId:= GetSourceDBId(aSchemaId);
    SchemaType:= GetSchemaType(aSchemaId);
    if SchemaType = schtRecordHistory then
      aTargetGroupId:= 0
    else
      begin
        V:= DBSQLRecord(fConfigDatabase, Format('SELECT CAST(COUNT(*) AS INTEGER) FROM %s WHERE SCHEMAID=%d AND GROUPID=%d AND GROUPID<>0', [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'SCHEMADB'), aSchemaId, aTargetGroupId]));
        if Integer(V) = 0 then
          IBReplicatorError(Format(sNoTargetGroup, [aSchemaId, aTargetGroupId]), 2);
      end;
    fDBLog.Log(fLogName, lchNull, Format(sValidatingSchema, [DBSQLRecord(fConfigDatabase, Format('SELECT NAME FROM SCHEMATA WHERE SCHEMAID=%d', [aSchemaId]))]));
    DB1:= TIBDatabase.Create(nil);
    try
      try
        SetDBParams(DB1, DBId, DB1Props);
        SafeStartTransaction(DB1.DefaultTransaction);
      except
        on E: Exception do
        begin
          if (aTargetDBId = 0) then
          begin
            raise;
          end;
          { allow to check target database }
          fDBLog.Log(fLogName, lchError, E.Message);
        end;
      end;
      try
        DB2:= TIBDatabase.Create(nil);
        try
          if (aTargetDBId <> 0) then
          begin
            SetDBParams(DB2, aTargetDBId, DB2Props);
            SafeStartTransaction(DB2.DefaultTransaction);
          end;
          try
            Q1:= TIBSQL.Create(nil);
            try
              Q1.Database:= fConfigDatabase;
              Q1.Transaction:= fConfigDatabaseTransaction;
              Q1.SQL.Add(Format('SELECT * FROM %s WHERE SCHEMAID=%d AND GROUPID=%d AND DISABLED=''N''', [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'RELATIONS'), aSchemaId, aTargetGroupId]));
              Q2:= TIBSQL.Create(nil);
              try
                Q2.Database:= Q1.Database;
                Q2.Transaction:= Q1.Transaction;
                Q2.SQL.Add(Format('SELECT * FROM %s', [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'FIELDS')]));
                Q2.SQL.Add('');
                SafeExecQuery(Q1);
                while not Q1.EOF do
                begin
                  PKeyNum:= 0;
                  RelFlag:= False;
                  Rel1Ex:= False;
                  Rel2Ex:= False;
                  if (DB1.Connected) then
                  begin
                    Rel1Ex:= RelationExists(DB1, Q1.FieldByName('RELATIONNAME').asString);
                    if not Rel1Ex then
                    begin
                      LogRelName(Q1);
                      LogMissingInDB('    ', sSource, DB1Props);
                    end;
                  end;
                  if DB2.Connected and (Q1.FieldByName('TARGETTYPE').asString = 'T') then
                  begin
                    Rel2Ex:= RelationExists(DB2, Q1.FieldByName('TARGETNAME').asString);
                    if not Rel2Ex then
                    begin
                      LogRelName(Q1);
                      LogMissingInDB('    ', sTarget, DB2Props);
                    end;
                  end;

                  Q2.SQL[1]:= Format('WHERE SCHEMAID=%d AND GROUPID=%d AND RELATIONID=%d', [aSchemaId, aTargetGroupId, Q1.FieldByName('RELATIONID').asInteger]);
                  SafeExecQuery(Q2);
                  if Q2.EOF then
                    begin
                      LogRelName(Q1);
                      fDBLog.Log(fLogName, lchNull, '    '+sNoFields);
                    end
                  else
                    begin
                      while not Q2.EOF do
                      begin
                        if Q2.FieldByName('FIELDTYPE').asInteger = 1 then
                          Inc(PKeyNum);
                        if (Q2.FieldByName('OPTIONS').asInteger and (fldoptDoNotInsert or fldoptDoNotUpdate)) <> (fldoptDoNotInsert or fldoptDoNotUpdate) then
                        begin
                          FldFlag:= False;
                          if Rel1Ex then
                          begin
                            if not RelationFieldExists(DB1, Q1.FieldByName('RELATIONNAME').asString, Q2.FieldByName('FIELDNAME').asString) then
                            begin
                              LogRelName(Q1);
                              LogFldName(Q2);
                              LogMissingInDB('      ', sSource, DB1Props);
                            end;
                          end;
                          if Rel2Ex then
                          begin
                            if not RelationFieldExists(DB2, Q1.FieldByName('TARGETNAME').asString, Q2.FieldByName('TARGETNAME').asString) then
                            begin
                              LogRelName(Q1);
                              LogFldName(Q2);
                              LogMissingInDB('      ', sTarget, DB2Props);
                            end;
                          end;
                        end;
                        SafeNext(Q2);
                      end;
                      if PKeyNum = 0 then
                      begin
                        LogRelName(Q1);
                        fDBLog.Log(fLogName, lchNull, '    '+sNoPrimaryFields);
                      end;
                    end;
                  SafeClose(Q2);
                  SafeNext(Q1);
                end;
                SafeClose(Q1);
              finally
                Q2.Free;
              end;
            finally
              Q1.Free;
            end;
          finally
            if DB2.Connected then
              SafeRollback(DB2.DefaultTransaction);
          end;
        finally
          DB2.Free;
        end;
      finally
        if DB1.Connected then
          SafeRollback(DB1.DefaultTransaction);
      end;
    finally
      DB1.Free;
    end;
    fDBLog.Log(fLogName, lchNull, '');
    SafeCommit(fConfigDatabaseTransaction);
  except
    SafeRollback(fConfigDatabaseTransaction);
    raise;
  end;
end;

function TIBReplicator.AssignDBParams(aDB: TIBDatabase; aId: Integer; var aDBProps: TReplDatabaseProperties): Boolean;
var
  V: Variant;
begin
  V:= DBSQLRecord(fConfigDatabase, Format('SELECT FILENAME,ADMINUSER,ADMINPSW,ADMINROLE,CHARSET,SQLDIALECT,OBJPREFIX,NAME,EXTFILEPATH FROM %s WHERE DBID=%d', [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'DATABASES'), aId]));
  Result:= VarIsArray(V);
  if Result then
  begin
    BuildImplicitEnvironment(['DBID'], [IntToStr(aId)]);
    if V[0]<>Null then
      aDB.DatabaseName:= ParseStr(V[0]);
    CheckDatabaseName(aDB.DatabaseName, Format('DBId:%d', [aId]));

    if V[1]<>Null then
      aDB.Params.Values['user_name']:= ParseStr(V[1]);
    if V[2]<>Null then
      aDB.Params.Values['password']:= ScramblePassword(ParseStr(ScramblePassword(V[2], False)), False);
    if V[3]<>Null then
      aDB.Params.Values['sql_role_name']:= ParseStr(V[3]);
    if V[4]<>Null then
      aDB.Params.Values['lc_ctype']:= ParseStr(V[4]);
    aDB.SQLDialect:= Max(Min(V[5], 3), 1);
    aDBProps.DBId:= aId;
    if V[6]<>Null then
      aDBProps.ObjPrefix:= V[6];
    if V[7]<>Null then
      aDBProps.DBName:= V[7];
    if V[8]<>Null then
      aDBProps.ExtFilePath:= V[8];
  end;
end;

procedure TIBReplicator.SetDBParams(aDB: TIBDatabase; aId: Integer; var aDBProps: TReplDatabaseProperties);
begin
  if AssignDBParams(aDB, aId, aDBProps) then
  begin
    aDB.DefaultTransaction:= TIBTransaction.Create(aDB);
    aDB.DefaultTransaction.Params.Text:= tranRW;
    aDB.LoginPrompt:= False;
    if fTraceSQL then
      aDB.TraceFlags:= [tfQPrepare,tfQExecute,tfQFetch,tfError,tfStmt,tfConnect,tfTransact,tfBlob,tfService,tfMisc]
    else
      aDB.TraceFlags:= [];

    fDBLog.Log(fLogName, lchNull, Format(sDBConnecting, [aDBProps.DBName]));
    aDB.Open;
  end;
end;

procedure TIBReplicator.SetDBParams_Repl;
var
  V: Variant;
  DBId2: Integer;
begin
  DBId2:= aDBId;
  if aAlternateTgt then
  begin
    V:= DBSQLRecord(fConfigDatabase, Format('SELECT OFFDBID FROM %s S WHERE S.SCHEMAID=%d AND S.GROUPID=%d AND S.DBID=%d', [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'SCHEMADB'), aSchemaId,aGroupId,aDBId]));
    if (V <> Null) and (V <> 0) then
      DBId2:= V;
  end;
  V:= DBSQLRecord(fConfigDatabase, Format('SELECT D.FILENAME,S.REPLUSER,S.REPLPSW,S.REPLROLE,D.CHARSET,D.SQLDIALECT,D.OBJPREFIX,S.OFFDBID,D.NAME,D.EXTFILEPATH FROM %s D INNER JOIN %s S ON S.DBID=%d AND D.DBID=%d WHERE S.SCHEMAID=%d AND S.GROUPID=%d', [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'DATABASES'),FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'SCHEMADB'),aDBId,DBId2,aSchemaId,aGroupId]));
  if VarIsArray(V) then
  begin
    BuildImplicitEnvironment(['SCHEMAID','GROUPID','DBID'], [IntToStr(aSchemaId), IntToStr(aGroupId), IntToStr(DBId2)]);
    if V[0]<>Null then
      aDB.DatabaseName:= ParseStr(V[0]);
    CheckDatabaseName(aDB.DatabaseName, Format('DBId:%d', [DBId2]));

    if V[1]<>Null then
      aDB.Params.Values['user_name']:= ParseStr(V[1]);
    if V[2]<>Null then
      aDB.Params.Values['password']:= ScramblePassword(ParseStr(ScramblePassword(V[2], False)), False);
    if V[3]<>Null then
      aDB.Params.Values['sql_role_name']:= ParseStr(V[3]);
    if V[4]<>Null then
      aDB.Params.Values['lc_ctype']:= ParseStr(V[4]);
    aDB.SQLDialect:= Max(Min(V[5], 3), 1);
    aDBProps.DBId:= DBId2;
    if V[8]<>Null then
      aDBProps.DBName:= V[8];
    if V[6]<>Null then
      aDBProps.ObjPrefix:= V[6];
    if V[9]<>Null then
      aDBProps.ExtFilePath:= V[9];
    aDB.DefaultTransaction:= TIBTransaction.Create(aDB);
    aDB.DefaultTransaction.DefaultDatabase:= aDB;
    aDB.DefaultTransaction.Params.Text:= tranRW;
    aDB.LoginPrompt:= False;
    fDBLog.Log(fLogName, lchNull, Format(sDBConnecting, [aDBProps.DBName]));
    if fTraceSQL then
      aDB.TraceFlags:= [tfQPrepare,tfQExecute,tfQFetch,tfError,tfStmt,tfConnect,tfTransact,tfBlob,tfService,tfMisc]
    else
      aDB.TraceFlags:= [];
    aDB.Open;
  end;
end;

procedure TIBReplicator.SetDBServiceParams(aDBServ: TIBControlService; aDBId: Integer; var aDatabaseFileName: string);
var
  DB: TIBDatabase;
  DBProps: TReplDatabaseProperties;
begin
  DB:= TIBDatabase.Create(nil);
  try
    if AssignDBParams(DB, aDBId, DBProps) then
    begin
      SetDBServiceParams(aDBServ, DB, DBProps.DBName, aDatabaseFileName);
    end;
  finally
    DB.Free;
  end;
end;

class function TIBReplicator.GetDBProtocol;
begin
  if Pos('\\', aDatabaseName) = 1 then         // "\\<server_name>\<filename>"
    begin
      Result:= IBServices.NamedPipe;
      Delete(aDatabaseName, 1, 2);
      aDatabaseFileName:= Copy(aDatabaseName, Pos('\',aDatabaseName+'\')+1, Length(aDatabaseName));
      Delete(aDatabaseName, Pos('\', aDatabaseName+'\'), Length(aDatabaseName));
    end
  else if Pos('@', aDatabaseName) > 0 then     // "<server_name>@<filename>"
    begin
      Result:= IBServices.SPX;
      aDatabaseFileName:= Copy(aDatabaseName, Pos('@',aDatabaseName+'@')+1, Length(aDatabaseName));
      Delete(aDatabaseName, Pos('@', aDatabaseName), Length(aDatabaseName));
    end
  else if Pos(':', aDatabaseName) > 2 then     // "<server_name>:<filename>"
    begin
      Result:= IBServices.TCP;
      aDatabaseFileName:= Copy(aDatabaseName, Pos(':',aDatabaseName+':')+1, Length(aDatabaseName));
      Delete(aDatabaseName, Pos(':', aDatabaseName), Length(aDatabaseName));
    end
  else
    begin                          // "C:\path\name" or "c:/path/name" or "/path/name"
      Result:= IBServices.Local;
      aDatabaseFileName:= aDatabaseName;
      aDatabaseName:= 'Local Server';
    end;
  aServerName:= aDatabaseName;
end;

procedure TIBReplicator.SetDBServiceParams(aDBServ: TIBControlService; aDB: TIBDatabase; const aDatabaseName: string; var aDatabaseFileName: string);
var
  S: string;
begin
  aDBServ.Protocol:= GetDBProtocol(aDB.DatabaseName, S, aDatabaseFileName);
  if aDBServ.Protocol <> IBServices.Local then
    aDBServ.ServerName := S;

  aDBServ.Params.Assign(aDB.Params);
  aDBServ.Params.Values['lc_ctype']:= '';
  aDBServ.LoginPrompt:= False;
  fDBLog.Log(fLogName, lchNull, Format(sDBServiceConnecting, [aDatabaseName]));
  aDBServ.Attach;
end;

function TIBReplicator.GetSourceDBId(aSchemaId: Integer): Integer;
var
  V: Variant;
begin
  V:= DBSQLRecord(fConfigDatabase, Format('SELECT SD.DBID FROM %s SD WHERE SD.SCHEMAID=%d AND SD.GROUPID=0', [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'SCHEMADB'),aSchemaId]));
  if VarIsNull(V) then
    IBReplicatorError(Format(sNoSchema, [aSchemaId]), 1);
  Result:= V;
end;

function TIBReplicator.GetSchemaType(aSchemaId: Integer): Integer;
var
  V: Variant;
begin
  V:= DBSQLRecord(fConfigDatabase, Format('SELECT S.SCHEMATYPE FROM %s S WHERE S.SCHEMAID=%d', [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'SCHEMATA'),aSchemaId]));
  if VarIsNull(V) then
    IBReplicatorError(Format(sNoSchema, [aSchemaId]), 1);
  Result:= V;
end;

function TIBReplicator.GetDatabaseName(aDBId: Integer): string;
var
  V: Variant;
begin
  V:= DBSQLRecord(fConfigDatabase, Format('SELECT D.NAME FROM %s D WHERE D.DBID=%d', [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'DATABASES'), aDBId]));
  if VarIsNull(V) then
    IBReplicatorError(Format(sNoDatabase, [aDBId]), 1);
  Result:= V;
end;

function TIBReplicator.GetDatabaseType(aDBId: Integer): Integer;
var
  V: Variant;
begin
  V:= DBSQLRecord(fConfigDatabase, Format('SELECT D.DBTYPE FROM %s D WHERE D.DBID=%d', [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'DATABASES'), aDBId]));
  if VarIsNull(V) then
    IBReplicatorError(Format(sNoDatabase, [aDBId]), 1);
  Result:= V;
end;

function TIBReplicator.GetDBMask(aSchemaId, aGroupId, aDBId: Integer): Integer;
var
  V: Variant;
begin
  V:= DBSQLRecord(fConfigDatabase, Format('SELECT DBMASK FROM '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'SCHEMADB')+' S WHERE S.SCHEMAID=%d AND S.GROUPID=%d AND S.DBID=%d', [aSchemaId,aGroupId,aDbId]));
  if VarIsNull(V) then
    Result:= -1
  else
    Result:= V;
end;

procedure TIBReplicator.DBEnvironmentWriteValue(aDB: TIBDatabase; const aObjPrefix, aName, aValue: string);
var
  V: Variant;
  S: string;
begin
  S:= UpperCase(aName);
  V:= DBSQLRecord(aDB, Format('SELECT CAST(COUNT(*) AS INTEGER) FROM %s WHERE UPPER(NAME)=''%s''', [FormatIdentifier(aDB.SQLDialect, aObjPrefix+'ENVIRONMENT'), S]));
  if Integer(V) = 0 then
    DBSQLExec(aDB, Format('INSERT INTO %s (NAME,VAL) VALUES(''%s'', ''%s'');', [FormatIdentifier(aDB.SQLDialect, aObjPrefix+'ENVIRONMENT'), S, aValue]))
  else
    DBSQLExec(aDB, Format('UPDATE %s SET VAL=''%s'' WHERE NAME=''%s''', [FormatIdentifier(aDB.SQLDialect, aObjPrefix+'ENVIRONMENT'), aValue, S]));
end;

function TIBReplicator.DBEnvironmentReadValue(aDB: TIBDatabase; const aObjPrefix, aName: string; const aDefault: string): string;
var
  V: Variant;
begin
  Result:= aDefault;
  if (aDB <> nil) and aDB.Connected and ObjectExists(aDB, ibobjRelation, aObjPrefix+'ENVIRONMENT') then
  begin
    V:= DBSQLRecord(aDB, Format('SELECT VAL FROM %s WHERE UPPER(NAME)=''%s''', [FormatIdentifier(aDB.SQLDialect, aObjPrefix+'ENVIRONMENT'), UpperCase(aName)]));
    if not VarIsNull(V) then
      Result:= V;
  end;
end;

procedure TIBReplicator.DBEnvironmentWriteValues(aDB: TIBDatabase; const aObjPrefix: string; aValues: TStrings);
var
  Q: TIBSQL;
  I: Integer;
  S: string;
  InTran: Boolean;
begin
  Q:= TIBSQL.Create(nil);
  try
    Q.Database:= aDB;
    Q.Transaction:= aDB.defaultTransaction;
    Q.SQL.Text:= Format('DELETE FROM %s WHERE NAME NOT STARTING ''_''', [FormatIdentifier(aDB.SQLDialect, aObjPrefix+'ENVIRONMENT')]);
    InTran:= Q.Transaction.InTransaction;
    if not InTran then
      SafeStartTransaction(Q.Transaction);
    try
      SafeExecQuery(Q);
      while not Q.EOF do
      begin
        aValues.Values[Q.FieldByName('NAME').asString]:= Q.FieldByName('VAL').asString;
        SafeNext(Q);
      end;
      if not InTran then
        SafeCommit(Q.Transaction);
    except
      if not InTran then
        SafeRollback(Q.Transaction);
      raise;
    end;
  finally
    Q.Free;
  end;
  for I:= 0 to aValues.Count-1 do
  begin
    S:= Trim(aValues.Names[I]);
    if (S <> '') and (Pos('_', S) <> 1) then
      DBEnvironmentWriteValue(aDB, aObjPrefix, S, aValues.Values[S]);
  end
end;

procedure TIBReplicator.DBEnvironmentReadValues(aDB: TIBDatabase; const aObjPrefix: string; aValues: TStrings; aSystemKeys: Boolean = False);
var
  Q: TIBSQL;
  InTran: Boolean;
begin
  Q:= TIBSQL.Create(nil);
  try
    Q.Database:= aDB;
    Q.Transaction:= aDB.DefaultTransaction;
    Q.SQL.Text:= Format('SELECT * FROM %s %s ORDER BY NAME', [FormatIdentifier(aDB.SQLDialect, aObjPrefix+'ENVIRONMENT'), iif(aSystemKeys, '', 'WHERE NAME NOT STARTING ''_''')]);
    InTran:= Q.Transaction.InTransaction;
    if not InTran then
      SafeStartTransaction(Q.Transaction);
    try
      SafeExecQuery(Q);
      while not Q.EOF do
      begin
        aValues.Values[Q.FieldByName('NAME').asString]:= Q.FieldByName('VAL').asString;
        SafeNext(Q);
      end;
    finally
      if not InTran then
        SafeCommit(Q.Transaction);
    end;
  finally
    Q.Free;
  end;
end;

function TIBReplicator.ReadEnvironment(const aName: string; const aDefault: string; aDB: TIBDatabase; const aObjPrefix: string): string;

  function GetEnv(const aName: string; var Res: string): Boolean;
  {$IFNDEF LINUX}
  var
    Buff: array[0..2048] of Char;
    I: Integer;
  {$ENDIF}
  begin
  {$IFDEF LINUX}
  Res:= string(Libc.GetEnv(PChar(aName)));
  Result:= Res <> '';
  {$ELSE}
    I:= GetEnvironmentVariable(PChar(aName), @Buff, SizeOf(Buff));
    Result:= I > 0;
    if Result then
      Res:= StrPas(@Buff);
  {$ENDIF}
  end;
begin
  Result:= aDefault;
  if fImplicitEnvironment.IndexOfName(aName) >= 0 then
    Result:= fImplicitEnvironment.Values[aName]
  else
    begin
      if fEnvironment.IndexOfName(aName) >= 0 then
        Result:= fEnvironment.Values[aName]
      else
        begin
          Result:= DBEnvironmentReadValue(aDB, aObjPrefix, aName, #0);
          if Result = #0 then
          begin
            Result:= DBEnvironmentReadValue(fConfigDatabase, fConfigDatabasePrefix, aName, #0);
            if Result = #0 then
            begin
              if not GetEnv('IBREPL_'+aName, Result) then
                if not GetEnv(aName, Result) then
                  Result:= aDefault;
            end;
          end;
        end;
    end;
end;

procedure TIBReplicator.BuildImplicitEnvironment;
var
  I: Integer;
begin
  if aClear then
    fImplicitEnvironment.Clear;
  fImplicitEnvironment.Values['_VER_']:= CurVersion;
  fImplicitEnvironment.Values['_DIR_']:= ExtractFilePath(ParamStr(0));
  for I:= Low(aNames) to High(aNames) do
    fImplicitEnvironment.Values[aNames[I]]:= aValues[I];
end;

type
  TDLLFunctionItem = record
    Name: string;
    Params: string;
  end;
const
  DLLName = 'ib_repl';
  DLLFunctions: array[1..20] of TDLLFunctionItem = (
    (Name: 'NowUTC'; Params: 'returns TimeStamp free_it'),
    (Name: 'Cmp_Blobs'; Params: 'blob,blob returns integer by value'),
    (Name: 'Integer_To_Blob'; Params: 'integer,blob returns parameter 2'),
    (Name: 'Double_To_Blob'; Params: 'double precision,blob returns parameter 2'),
    (Name: 'TimeStamp_To_Blob'; Params: 'timestamp,blob returns parameter 2'),
    (Name: 'String_To_Blob'; Params: 'CSTRING(%d),blob returns parameter 2'),
    (Name: 'Blob_To_Integer'; Params: 'blob returns integer by value'),
    (Name: 'Blob_To_Double'; Params: 'blob returns double precision by value'),
    (Name: 'Blob_To_TimeStamp'; Params: 'blob returns TimeStamp free_it'),
    (Name: 'Blob_To_String'; Params: 'blob returns CSTRING(%d) free_it'),
    (Name: 'Blob_Length'; Params: 'blob returns integer by value'),
    (Name: 'Concat_Blobs'; Params: 'blob,blob,blob returns parameter 3'),
    (Name: 'String_Length'; Params: 'CSTRING(%d) returns integer by value'),
    (Name: 'String_Copy'; Params: 'CSTRING(%d),integer,integer returns CSTRING(%d) free_it'),
    (Name: 'Memo_Line_Count'; Params: 'blob returns integer by value'),
    (Name: 'Memo_Get_Line'; Params: 'blob,integer returns CSTRING(%d) free_it'),
    (Name: 'Bit_And'; Params: 'integer,integer returns integer by value'),
    (Name: 'Bit_Or'; Params: 'integer,integer returns integer by value'),
    (Name: 'Bit_Xor'; Params: 'integer,integer returns integer by value'),
    (Name: 'Bit_Not'; Params: 'integer returns integer by value')
  );
procedure TIBReplicator.CreateSystemObjects(aSchemaId: Integer; aKeyLength: Integer; aGroupId{master database}: Integer; aDBId: Integer);
var
  DB: TIBDatabase;
  DBProps: TReplDatabaseProperties;
const
  ValidDataTypes = ['B', 'S', 'I', 'T', 'D', 'M'];

  function GetFieldType(const aRelName, aFieldName: string; var aDataType: string): Char;   // B..blob,I..integer,T..timestamp,S..(var)char,D..double,
  var
    S: string;
    I, SubT, Scale: Integer;
  const
    sFieldNotFound = 'Field not found "%s"."%s"';
  begin
    if not GetRelationFieldType(DB, aRelName, aFieldName, S, I, Scale, SubT) then
      IBReplicatorError(Format(sFieldNotFound, [aRelName, aFieldName]), 1);
    if (S = 'TEXT') or (S = 'VARYING') or (S = 'CSTRING') then
      aDataType:= 'STRING'
    else if (S = 'SHORT') or (S = 'LONG') or (S = 'INT64') then
      begin
        if Scale < 0 then
          aDataType:= 'DOUBLE'
        else
          aDataType:= 'INTEGER';
      end
    else if (S = 'DOUBLE') or (S = 'FLOAT') then
      aDataType:= 'DOUBLE'
    else if (S = 'DATE') or (S = 'TIME') or (S = 'TIMESTAMP') then
      aDataType:= 'TIMESTAMP'
    else if (S = 'BLOB') then
      if SubT = 1 then
        aDataType:= 'MEMO'
      else
        aDataType:= 'BLOB'
    else
      aDataType:= 'NONE';
    Result:= aDataType[1];
  end;

  function ParseCondition(const aDruh: string; aCondition: string): string;
  var
    I: Integer;
    S: string;
    F: Boolean;
  begin
    Result:= '';
    repeat
      I:= Pos('{#', aCondition);
      if I = 0 then
        begin
          Result:= Result+aCondition;
          Break;
        end
      else
        begin
          Result:= Result+Copy(aCondition, 1, I-1);
          Delete(aCondition, 1, I+2-1);
          I:= Pos('#}', aCondition);
          if I = 0 then
            Break;
          S:= UpperCase(Trim(Copy(aCondition, 1, I-1)));
          Delete(aCondition, 1, I+2-1);
          if S = 'CXO' then
            begin
              if aDruh[1] = 'I' then
                Result:= Result+'NEW'
              else
                Result:= Result+'OLD';
            end
          else if S = 'CXN' then
            begin
              if aDruh[1] = 'D' then
                Result:= Result+'OLD'
              else
                Result:= Result+'NEW';
            end
          else if Pos('IF ', S) = 1 then
            begin
              Delete(S, 1, 3);
              I:= Pos('{#IF ', aCondition);
              if I = 0 then
                I:= Length(aCondition)+1;
              F:= (S = 'ALL') or
                  (Pos(aDruh, S) > 0) and (Pos('!'+aDruh, S) = 0) or
                  ((Pos('!'+aDruh, S) = 0) and
                   ((Pos('!DELETE', S) <> 0) or (Pos('!INSERT', S) <> 0) or (Pos('!UPDATE', S) <> 0)));
              if not F then
                Delete(aCondition, 1, I-1);
            end;
        end;
    until False;
  end;

  function DivideCondition(const aCondition: string): Integer;
  var
    I: Integer;
  begin
    I:= 1;  { strip declare variable section and condition }
    repeat
      Result:= I;
      while (I <= Length(aCondition)) and (aCondition[I] in [' ',#13,#10]) do
        Inc(I);
      if StrIComp('DECLARE ', PChar(Copy(aCondition, I, Length('DECLARE ')))) <> 0 then
        Break;
      Inc(I, Length('DECLARE '));
      while (I <= Length(aCondition)) and (aCondition[I] in [' ',#13,#10]) do
        Inc(I);
      if StrIComp('VARIABLE', PChar(Copy(aCondition, I, Length('VARIABLE')))) <> 0 then
        Break;
      while (I <= Length(aCondition)) and not (aCondition[I] in [';']) do
        Inc(I);
      Inc(I, 1);
      while (I <= Length(aCondition)) and (aCondition[I] in [#13,#10]) do
        Inc(I);
    until False;
  end;

  function KeyArrayToString(const Arr: TStringOpenArray; aNO: string): string;
  var
    I: Integer;
  begin
    Result:= '';
    for I:= 0 to Length(Arr)-1 do
    begin
      if Result <> '' then
        Result:= Result+'||VAR$SEPARATOR||';
      Result:= Result+aNO+'.'+FormatIdentifier(DB.SQLDialect, Arr[I]);
    end;
  end;


  procedure CreateReplTg(aWhen, aDruh: string; aGrpId, aRelId: Integer; const aRelName: string; const aPK, aFK: TStringOpenArray; const aFKId: TIntegerOpenArray; aCondition: string; const aReplUser: string; aWipeF: Boolean);
    function InsFldTg(const aRelName, aFldS: string; aNew: Boolean; aFldId: Integer): string;
    var
      TypeS: string;
    const
      NewOldS: array[Boolean] of string = ('OLD', 'NEW');
    begin
      if GetFieldType(aRelName, aFldS, TypeS) in ValidDataTypes then
        Result:= '        IF ('+NewOldS[aNew]+'.'+FormatIdentifier(DB.SQLDialect, aFldS)+' IS NULL) THEN'+#13#10+
          Format('          INSERT INTO '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'FIELD')+'(SCHEMAID,SEQID,FIELDID,MODE,DATATYPE) VALUES (:VAR$SCHEMAID,:VAR$ID,%d,''%s'',''N'');', [aFldId, NewOldS[aNew][1]])+#13#10+
                 '        ELSE'+#13#10+
          Format('          INSERT INTO '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'FIELD')+'(SCHEMAID,SEQID,FIELDID,MODE,DATATYPE,DATA) VALUES (:VAR$SCHEMAID,:VAR$ID,%d,''%s'',''%s'','+iif(TypeS[1] in ['B','M'], '', FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+TypeS+'_TO_BLOB'))+'('+NewOldS[aNew]+'.'+FormatIdentifier(DB.SQLDialect, aFldS)+'));', [aFldId, NewOldS[aNew][1], TypeS[1]])+#13#10
        else
          Result:= '';
    end;
  var
    S, TypeS: string;
    I, CondIdx: Integer;
  begin
    if Length(aPK) = 0 then { no primary key }
      Exit;
    aCondition:= ParseCondition(aDruh, aCondition);

    CondIdx:= DivideCondition(aCondition);

    S:= Format('FOR %s', [FormatIdentifier(DB.SQLDialect, aRelName)])+#13#10+
        Format('ACTIVE %s %s POSITION 32767', [aWhen, aDruh])+#13#10+
               'AS'+#13#10+
               'DECLARE VARIABLE VAR$SEPARATOR CHAR(1);'+#13#10+
        Format('DECLARE VARIABLE VAR$OLDPKEY VARCHAR(%d);', [aKeyLength])+#13#10+
        Format('DECLARE VARIABLE VAR$NEWPKEY VARCHAR(%d);', [aKeyLength])+#13#10+
               'DECLARE VARIABLE VAR$ID INTEGER;'+#13#10+
               'DECLARE VARIABLE VAR$COND INTEGER;'+#13#10+
               'DECLARE VARIABLE VAR$SCHEMAID INTEGER;'+#13#10+
               'DECLARE VARIABLE VAR$RELATIONID INTEGER;'+#13#10+
               'DECLARE VARIABLE VAR$GROUPID INTEGER;'+#13#10+
               'DECLARE VARIABLE VAR$DBMASK INTEGER;'+#13#10+
               'DECLARE VARIABLE VAR$RELATIONNAME VARCHAR(100);'+#13#10+
               'DECLARE VARIABLE VAR$OPER CHAR(1);'+#13#10+
               'DECLARE VARIABLE VAR$DISABLED CHAR(1);'+#13#10+
               'DECLARE VARIABLE VAR$REPLUSER CHAR(31);'+#13#10;
    if CondIdx > 1 then
      S:= S + Copy(aCondition, 1, CondIdx-1)+#13#10;

    S:= S+     'BEGIN'+#13#10+
               '  SELECT SCHEMAID,RELATIONID,GROUPID,DBMASK,SEPARATOR,DISABLED,RELATIONNAME,REPLUSER FROM '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'CONFIG')+#13#10+
        Format('  WHERE SCHEMAID=%d AND GROUPID=%d AND RELATIONID=%d', [aSchemaId, aGrpId, aRelId])+#13#10+
               '  INTO VAR$SCHEMAID,VAR$RELATIONID,VAR$GROUPID,VAR$DBMASK,VAR$SEPARATOR,VAR$DISABLED,VAR$RELATIONNAME,VAR$REPLUSER;'+#13#10;
    S:= S+     '  IF (USER <> VAR$REPLUSER) THEN'+#13#10+
               '  BEGIN'+#13#10;
    S:= S+
        Format('    VAR$OPER=''%s'';'+#13#10, [aDruh[1]]);
    S:= S+     '    IF (VAR$DISABLED = ''N'') THEN'+#13#10;
    S:= S+     '    BEGIN'+#13#10;
    S:= S+     '      VAR$COND = 1;'+#13#10;
    if aDruh[1] in ['I','U'] then
      S:= S+Format('      VAR$NEWPKEY=%s;', [KeyArrayToString(aPK, 'NEW')])+#13#10;
    if aDruh[1] in ['D','U'] then
      S:= S+Format('      VAR$OLDPKEY=%s;', [KeyArrayToString(aPK, 'OLD')])+#13#10;
    if Trim(aCondition) = '' then
      S:= S +  '      /* no RELATION.CONDITION */'+#13#10
    else
      S:= S +  '      /* RELATION.CONDITION */'+#13#10+
               Copy(aCondition, CondIdx, Length(aCondition))+#13#10;
    S:= S+     '      IF (VAR$COND>0) THEN'+#13#10;
    S:= S+     '      BEGIN'+#13#10;
    if aWipeF then
    begin
    S:= S+     '        DELETE FROM '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'LOG')+#13#10+
               '        WHERE SCHEMAID=:VAR$SCHEMAID AND GROUPID=:VAR$GROUPID AND RELATIONID=:VAR$RELATIONID AND (NEWPKEY=:VAR$OLDPKEY OR OLDPKEY=:VAR$OLDPKEY);'+#13#10;
    S:= S+     '        DELETE FROM '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'MAN')+#13#10+
               '        WHERE SCHEMAID=:VAR$SCHEMAID AND GROUPID=:VAR$GROUPID AND RELATIONID=:VAR$RELATIONID AND (NEWPKEY=:VAR$OLDPKEY OR OLDPKEY=:VAR$OLDPKEY);'+#13#10;
    end;

    S:= S+     '        VAR$ID = GEN_ID('+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'GEN_SEQID')+',1);'+#13#10;
    S:= S+     '        INSERT INTO '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'LOG')+'(SEQID,SCHEMAID,GROUPID,DBMASK,RELATIONID,REPLTYPE,OLDPKEY,NEWPKEY)'+#13#10+
               '        VALUES(:VAR$ID,:VAR$SCHEMAID,:VAR$GROUPID,:VAR$DBMASK,:VAR$RELATIONID,:VAR$OPER,:VAR$OLDPKEY,:VAR$NEWPKEY);'+#13#10;

    for I:= 0 to Length(aFKId)-1 do
    begin
      case aDruh[1] of
        'U':
          begin
            if GetFieldType(aRelName, aFK[I], TypeS) in ValidDataTypes then
    S:= S+     '        IF (NEW.'+FormatIdentifier(DB.SQLDialect, aFK[I])+' IS NOT NULL OR OLD.'+FormatIdentifier(DB.SQLDialect, aFK[I])+' IS NOT NULL) THEN'+#13#10+
               '          IF (OLD.'+FormatIdentifier(DB.SQLDialect, aFK[I])+' IS NULL) THEN'+#13#10+
        Format('            INSERT INTO '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'FIELD')+'(SCHEMAID,SEQID,FIELDID,MODE,DATATYPE) VALUES (:VAR$SCHEMAID,:VAR$ID,%d,''O'',''N'');', [aFKId[I]])+#13#10+
               '          ELSE IF (NEW.'+FormatIdentifier(DB.SQLDialect, aFK[I])+' IS NULL OR '+iif(TypeS[1] in ['B','M'], FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'CMP_BLOBS')+'(NEW.'+FormatIdentifier(DB.SQLDialect, aFK[I])+',OLD.'+FormatIdentifier(DB.SQLDialect, aFK[I])+')=0','NEW.'+FormatIdentifier(DB.SQLDialect, aFK[I])+' <> OLD.'+FormatIdentifier(DB.SQLDialect, aFK[I]))+') THEN'+#13#10+
        Format('            INSERT INTO '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'FIELD')+'(SCHEMAID,SEQID,FIELDID,MODE,DATATYPE,DATA) VALUES (:VAR$SCHEMAID,:VAR$ID,%d,''O'',''%s'','+iif(TypeS[1] in ['B','M'], '', FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+TypeS+'_TO_BLOB'))+'(OLD.'+FormatIdentifier(DB.SQLDialect, aFK[I])+'));', [aFKId[I], TypeS[1]])+#13#10;
            S:= S+ InsFldTg(aRelName, aFK[I], True, aFKId[I]);
          end;
        'I':
          begin
            S:= S+ InsFldTg(aRelName, aFK[I], True, aFKId[I]);
          end;
        'D':
          begin
            S:= S+ InsFldTg(aRelName, aFK[I], False, aFKId[I]);
          end;
      end;
    end;

    S:= S+     '      END'+#13#10+
               '    END'+#13#10+
               '  END'+#13#10+
               'END';
    CreateObject(DB, ibobjTrigger, GetTriggerName(aSchemaID, aGrpId, aRelId, aRelName, aDruh[1], DBProps), S);
  end;

  procedure CreateReplTgs(aGrpId, aRelId: Integer; const aRelName: string; const aPK, aFK: TStringOpenArray; const aFKId: TIntegerOpenArray; const aCondition, aReplUser: string; aWipeLog: Boolean);
  begin
    if Length(aPK) = 0 then
      fDBLog.Log(fLogName, lchError, Format(sNoPrimaryKey, [aRelName]));
    CreateReplTg('AFTER','INSERT', aGrpId, aRelId, aRelName, aPK, aFK, aFKId, aCondition, aReplUser, False);
    CreateReplTg('AFTER','UPDATE', aGrpId, aRelId, aRelName, aPK, aFK, aFKId, aCondition, aReplUser, False);
    CreateReplTg('BEFORE','DELETE', aGrpId, aRelId, aRelName, aPK, aFK, aFKId, aCondition, aReplUser, aWipeLog);
  end;

  procedure CreateVerTg(aWhen, aDruh: string; aGrpId, aRelId: Integer; const aRelName: string; const aPK, aFK: TStringOpenArray; const aFKId: TIntegerOpenArray; aCondition: string);
  var
    S, TypeS: string;
    CondIdx, I: Integer;
  begin
    if Length(aPK) = 0 then { no primary key }
      Exit;
    aCondition:= ParseCondition(aDruh, aCondition);

    CondIdx:= DivideCondition(aCondition);

    S:= Format('FOR %s', [FormatIdentifier(DB.SQLDialect, aRelName)])+#13#10+
        Format('ACTIVE %s %s POSITION 32767', [aWhen, aDruh])+#13#10+
               'AS'+#13#10+
               'DECLARE VARIABLE VAR$SEPARATOR CHAR(1);'+#13#10+
        Format('DECLARE VARIABLE VAR$OLDPKEY VARCHAR(%d);', [aKeyLength])+#13#10+
        Format('DECLARE VARIABLE VAR$NEWPKEY VARCHAR(%d);', [aKeyLength])+#13#10+
               'DECLARE VARIABLE VAR$ID INTEGER;'+#13#10+
               'DECLARE VARIABLE VAR$COND INTEGER;'+#13#10+
               'DECLARE VARIABLE VAR$SCHEMAID INTEGER;'+#13#10+
               'DECLARE VARIABLE VAR$RELATIONID INTEGER;'+#13#10+
               'DECLARE VARIABLE VAR$GROUPID INTEGER;'+#13#10+
               'DECLARE VARIABLE VAR$RELATIONNAME VARCHAR(100);'+#13#10+
               'DECLARE VARIABLE VAR$OPER CHAR(1);'+#13#10+
               'DECLARE VARIABLE VAR$DISABLED CHAR(1);'+#13#10;
    if CondIdx > 1 then
      S:= S + Copy(aCondition, 1, CondIdx-1)+#13#10;

    S:= S+     'BEGIN'+#13#10+
        Format('  VAR$OPER=''%s'';'+#13#10, [aDruh[1]]);

    S:= S+     '  SELECT SCHEMAID,RELATIONID,GROUPID,SEPARATOR,DISABLED,RELATIONNAME FROM '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'CONFIG')+#13#10+
        Format('  WHERE SCHEMAID=%d AND GROUPID=%d AND RELATIONID=%d', [aSchemaId, aGrpId, aRelId])+#13#10+
               '  INTO VAR$SCHEMAID,VAR$RELATIONID,VAR$GROUPID,VAR$SEPARATOR,VAR$DISABLED,VAR$RELATIONNAME;'+#13#10;
    S:= S+     '  IF (VAR$DISABLED = ''N'') THEN'+#13#10;
    S:= S+     '  BEGIN'+#13#10;
    if aDruh[1] in ['I','U'] then
      S:= S+Format('    VAR$NEWPKEY=%s;', [KeyArrayToString(aPK, 'NEW')])+#13#10;
    if aDruh[1] in ['D','U'] then
      S:= S+Format('    VAR$OLDPKEY=%s;', [KeyArrayToString(aPK, 'OLD')])+#13#10;

    S:= S+     '    VAR$COND = 1;'+#13#10;
    if Trim(aCondition) = '' then
      S:= S +  '    /* no RELATION.CONDITION */'+#13#10
    else
      S:= S +  '    /* RELATION.CONDITION */'+#13#10+
               Copy(aCondition, CondIdx, Length(aCondition))+#13#10;
    S:= S+     '    IF (VAR$COND>0) THEN'+#13#10;
    S:= S+     '    BEGIN'+#13#10;
    S:= S+     '      VAR$ID = GEN_ID('+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'GEN_SEQID')+',1);'+#13#10;

    S:= S+     '      INSERT INTO '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'HISTORY')+'(SEQID,SCHEMAID,GROUPID,RELATIONID,VERTYPE,NEWPKEY,OLDPKEY)'+#13#10+
               '      VALUES(:VAR$ID,:VAR$SCHEMAID,:VAR$GROUPID,:VAR$RELATIONID,:VAR$OPER,:VAR$OLDPKEY,:VAR$NEWPKEY);'+#13#10;
    for I:= 0 to Length(aFKId)-1 do
    begin
      if aDruh[1] = 'U' then
      begin
        if GetFieldType(aRelName, aFK[I], TypeS) in ValidDataTypes then
    S:= S+     '      IF (NEW.'+FormatIdentifier(DB.SQLDialect, aFK[I])+' IS NOT NULL OR OLD.'+FormatIdentifier(DB.SQLDialect, aFK[I])+' IS NOT NULL) THEN'+#13#10+
               '        IF (OLD.'+FormatIdentifier(DB.SQLDialect, aFK[I])+' IS NULL) THEN'+#13#10+
        Format('          INSERT INTO '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'FIELD')+'(SCHEMAID,SEQID,FIELDID,MODE,DATATYPE) VALUES (:VAR$SCHEMAID,:VAR$ID,%d,''O'',''N'');', [aFKId[I]])+#13#10+
               '        ELSE IF (NEW.'+FormatIdentifier(DB.SQLDialect, aFK[I])+' IS NULL OR '+iif(TypeS[1] in ['B','M'], FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'CMP_BLOBS')+'(NEW.'+FormatIdentifier(DB.SQLDialect, aFK[I])+',OLD.'+FormatIdentifier(DB.SQLDialect, aFK[I])+')=0','NEW.'+FormatIdentifier(DB.SQLDialect, aFK[I])+' <> OLD.'+FormatIdentifier(DB.SQLDialect, aFK[I]))+') THEN'+#13#10+
        Format('          INSERT INTO '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'FIELD')+'(SCHEMAID,SEQID,FIELDID,MODE,DATATYPE,DATA) VALUES (:VAR$SCHEMAID,:VAR$ID,%d,''O'',''%s'','+iif(TypeS[1] in ['B','M'], '', FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+TypeS+'_TO_BLOB'))+'(OLD.'+FormatIdentifier(DB.SQLDialect, aFK[I])+'));', [aFKId[I], TypeS[1]])+#13#10;
      end;
      if aDruh[1] = 'D' then
      begin
        if GetFieldType(aRelName, aFK[I], TypeS) in ValidDataTypes then
    S:= S+     '      IF (OLD.'+FormatIdentifier(DB.SQLDialect, aFK[I])+' IS NULL) THEN'+#13#10+
        Format('        INSERT INTO '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'FIELD')+'(SCHEMAID,SEQID,FIELDID,MODE,DATATYPE) VALUES (:VAR$SCHEMAID,:VAR$ID,%d,''O'',''N'');', [aFKId[I]])+#13#10+
               '      ELSE'+#13#10+
        Format('        INSERT INTO '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'FIELD')+'(SCHEMAID,SEQID,FIELDID,MODE,DATATYPE,DATA) VALUES (:VAR$SCHEMAID,:VAR$ID,%d,''O'',''%s'','+iif(TypeS[1] in ['B','M'], '', FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+TypeS+'_TO_BLOB'))+'(OLD.'+FormatIdentifier(DB.SQLDialect, aFK[I])+'));', [aFKId[I], TypeS[1]])+#13#10;
      end;
    end;
    S:= S+     '    END'+#13#10+
               '  END'+#13#10+
               'END';
    CreateObject(DB, ibobjTrigger, GetTriggerName(aSchemaID, aGrpId, aRelId, aRelName, aDruh[1], DBProps), S);
  end;

  procedure CreateVerTgs(aGrpId, aRelId: Integer; const aRelName: string; const aPK, aFK: TStringOpenArray; const aFKIds: TIntegerOpenArray; aCondition: string);
  begin
    if Length(aPK) = 0 then
      fDBLog.Log(fLogName, lchError, Format(sNoPrimaryKey, [aRelName]));
    CreateVerTg('AFTER','INSERT', aGrpId, aRelId, aRelName, aPK, aFK, aFKIds, aCondition);
    CreateVerTg('AFTER','UPDATE', aGrpId, aRelId, aRelName, aPK, aFK, aFKIds, aCondition);
    CreateVerTg('AFTER','DELETE', aGrpId, aRelId, aRelName, aPK, aFK, aFKIds, aCondition);
  end;

  procedure CreateT(const aTbl: string);
  begin
    CreateObject(DB, ibobjRelation, DBProps.ObjPrefix+aTbl,
        '('+
        '  SCHEMAID   INTEGER NOT NULL,'+
        '  GROUPID    INTEGER NOT NULL,'+
        '  DBMASK     INTEGER NOT NULL,'+
        '  DBMASK_PENDING     INTEGER DEFAULT 0 NOT NULL,'+
        '  SEQID      INTEGER NOT NULL,'+
        '  RELATIONID INTEGER NOT NULL,'+
        '  REPLTYPE   CHAR(1),'+
        '  STAMP      TIMESTAMP DEFAULT ''NOW'' NOT NULL,'+
 Format('  OLDPKEY    VARCHAR(%d),', [aKeyLength])+
 Format('  NEWPKEY    VARCHAR(%d),', [aKeyLength])+
 iif(aTbl='MAN', 'DESCRIPTION BLOB SUB_TYPE TEXT SEGMENT SIZE 80,', '')+
 iif(aTbl='MAN', 'CONFLICT BLOB,', '')+
        ' PRIMARY KEY (SCHEMAID, GROUPID, SEQID)'+
        ')'
    );

    if not IndexExists(DB, 'I_'+DBProps.ObjPrefix+aTbl+'_SEQID') then
    begin
      DBLogVerbose(Format(sCreatingObject, ['INDEX', 'I_'+DBProps.ObjPrefix+aTbl+'_SEQID']));
      DBSQLExec(DB,
        Format('CREATE UNIQUE INDEX %s ON %s(SCHEMAID,SEQID)', [FormatIdentifier(DB.SQLDialect, 'I_'+DBProps.ObjPrefix+aTbl+'_SEQID'), FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+aTbl)])
      );
    end;
  end;
  procedure CreateTTg(const aTbl, aTbl2: string);
  begin
    CreateObject(DB, ibobjTrigger, 'TG_D_'+DBProps.ObjPrefix+aTbl+'_FIELD',
      'FOR '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+aTbl)+#13#10+
      'ACTIVE AFTER DELETE POSITION 0'+#13#10+
      'AS'+#13#10+
      'DECLARE VARIABLE VAR$N INTEGER;'+#13#10+
      'BEGIN'+#13#10+
      '  SELECT COUNT(*) FROM '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+aTbl2)+ ' WHERE SCHEMAID=OLD.SCHEMAID AND SEQID=OLD.SEQID INTO VAR$N;'+#13#10+
      '  IF (VAR$N = 0) THEN'+#13#10+
      '  BEGIN'+#13#10+
      '    DELETE FROM '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'FIELD')+' WHERE SCHEMAID=OLD.SCHEMAID AND SEQID=OLD.SEQID;'+#13#10+
      '    DELETE FROM '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'EXT_FILE')+' WHERE SCHEMAID=OLD.SCHEMAID AND SEQID=OLD.SEQID;'+#13#10+
      '  END'+#13#10+
      'END'
    );
  end;
var
  I: Integer;
  SchemaType: Integer;
  InTran: Boolean;
  Separator: Char;
  Q1: TIBSQL;
  RelId, GrpId: Integer;
  RS, ReplUserS, ReplRoleS, S, TgCondition, Log2S: string;
  V: Variant;
  WipeF, SourceF: Boolean;
  PKS, FKS: TStringOpenArray;
  FKIds: TIntegerOpenArray;
  MaxVARCHARLength: Integer;
resourcestring
  sRelationDoesNotExists = 'Relation "%s" does not exists';
  sProcedureDoesNotExists = 'Procedure "%s" does not exists';
begin
{$IFDEF REGISTRATION}
  TReg_CheckRegistration(False);
{$ENDIF}
  InTran:= fConfigDatabaseTransaction.InTransaction;
  if not InTran then
    SafeStartTransaction(fConfigDatabaseTransaction);
  try
    SourceF:= aGroupId = 0;
    if SourceF and (aSchemaId <> 0) then
      aDBId:= GetSourceDBId(aSchemaId);
    if GetDatabaseType(aDBId) <> dbtInterbase then
      Abort; // to close transaction

    DB:= TIBDatabase.Create(nil);
    try
      SetDBParams(DB, aDBId, DBProps);
      if not DB.Connected then
        IBReplicatorError(Format(sNoDatabase, [aDBId]), 1);
      CreateObject(DB, ibobjGenerator, DBProps.ObjPrefix+'GEN_TRANSFERID', '');

      CreateObject(DB, ibobjRelation, DBProps.ObjPrefix+'TRANSFER',
        '('+
        '  TRANSFERID INTEGER NOT NULL,'+
        '  STATUS CHAR(1) NOT NULL, /* S..source, T..target */'+

        '  SCHEMAID INTEGER NOT NULL,'+
        '  GROUPID INTEGER NOT NULL,'+
        '  DBID INTEGER NOT NULL,'+
        '  STAMP TIMESTAMP,'+
        '  STAMP_SENT TIMESTAMP,'+
        '  STAMP_REC TIMESTAMP,'+
        '  STAMP_PROC TIMESTAMP,'+
        '  TRANSFERID_ACK INTEGER,'+
        '  OFFTRANSFER VARCHAR(10),'+
        '  RESEND_FLAG CHAR(1) DEFAULT ''N'' NOT NULL,'+
        ' PRIMARY KEY (SCHEMAID,TRANSFERID,STATUS,GROUPID,DBID)'+
        ')'
      );
      CreateObject(DB, ibobjRelation, DBProps.ObjPrefix+'SNAPSHOT',
        '('+
        '  SCHEMAID   INTEGER NOT NULL,'+
        '  GROUPID    INTEGER NOT NULL,'+
        '  DBID       INTEGER NOT NULL,'+
        '  SEQID      INTEGER NOT NULL,'+
        ' PRIMARY KEY (SCHEMAID, GROUPID, DBID)'+
        ')'
      );
      MaxVARCHARLength:= GetMaxVARCHARlength(DB, DBProps.ObjPrefix, 255, 32765);
      I:= 1024;
      if I > MaxVARCHARLength then
        I:= MaxVARCHARLength;
      CreateObject(DB, ibobjRelation, DBProps.ObjPrefix+'ENVIRONMENT', Format(CreateEnvironmentTable, [I]));
      for I:= Low(DLLFunctions) to High(DLLFunctions) do
        CreateObject(DB, ibobjFunction, DBProps.ObjPrefix+UpperCase(DLLFunctions[I].Name),
          Format(DLLFunctions[I].Params, [MaxVARCHARLength, MaxVARCHARLength, MaxVARCHARLength, MaxVARCHARLength])+
          ' ENTRY_POINT '''+DLLFunctions[I].Name+''' MODULE_NAME '''+DLLName+''''
        );

      CreateObject(DB, ibobjRelation, DBProps.ObjPrefix+'FIELD',
        '('+
        '  SCHEMAID   INTEGER NOT NULL,'+
        '  SEQID   INTEGER NOT NULL,'+
        '  MODE    CHAR(1) NOT NULL,'+
        '  FIELDID INTEGER NOT NULL,'+
        '  DATATYPE CHAR(1) NOT NULL,'+
        '  DATA    BLOB SUB_TYPE 0 SEGMENT SIZE 80,'+
        ' PRIMARY KEY (SCHEMAID, SEQID, MODE, FIELDID)'+
        ')'
      );
      CreateObject(DB, ibobjRelation, DBProps.ObjPrefix+'EXT_FILE',
        '('+
        '  SCHEMAID   INTEGER NOT NULL,'+
        '  SEQID   INTEGER NOT NULL,'+
        '  FIELDID INTEGER NOT NULL,'+
        '  NAME    VARCHAR(128) NOT NULL,'+
        '  STAMP   TIMESTAMP NOT NULL,'+
        '  DATA    BLOB SUB_TYPE 0 SEGMENT SIZE 512,'+
        ' PRIMARY KEY (SCHEMAID, SEQID, FIELDID, NAME)'+
        ')'
      );
      CreateT('LOG');
      CreateT('MAN');
      CreateTTg('LOG', 'MAN');
      CreateTTg('MAN', 'LOG');
      CreateObject(DB, ibobjGenerator, DBProps.ObjPrefix+'GEN_SEQID', '');
      CreateObject(DB, ibobjTrigger, 'TG_I_'+DBProps.ObjPrefix+'LOG_SEQID',
        'FOR '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'LOG')+#13#10+
        'ACTIVE BEFORE INSERT POSITION 0'+#13#10+
        'AS'+#13#10+
        'BEGIN'+#13#10+
        '  IF (NEW.SEQID IS NULL) THEN'+#13#10+
        '    NEW.SEQID = GEN_ID('+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'GEN_SEQID')+',1);'+#13#10+
        Iif(fNowAsUTC, '  NEW.STAMP = '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'NOWUTC')+'();'+#13#10, '')+
        '  POST_EVENT '''+DBProps.ObjPrefix+'REPLICATENOW'';'+#13#10+  // ??? FormatIdentifier
        'END'
      );

      CreateObject(DB, ibobjRelation, DBProps.ObjPrefix+'CONFIG',
        '('+
        '  SCHEMAID   INTEGER NOT NULL,'+
        '  GROUPID    INTEGER NOT NULL,'+
        '  RELATIONID INTEGER NOT NULL,'+
        '  RELATIONNAME       VARCHAR(100),'+
        '  DBMASK     INTEGER NOT NULL,'+
        '  REPLUSER   VARCHAR(31) NOT NULL,'+
        '  SEPARATOR  CHAR(1),'+
        '  DISABLED   CHAR(1) NOT NULL,'+
        '  SRCDBID    INTEGER NOT NULL,'+
        ' PRIMARY KEY (SCHEMAID, GROUPID, RELATIONID)'+
        ')'
      );

      CreateObject(DB, ibobjTrigger, 'TG_D_'+DBProps.ObjPrefix+'LOG_SEQID',
        'FOR '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'LOG')+#13#10+
        'ACTIVE AFTER DELETE POSITION 0'+#13#10+
        'AS'+#13#10+
        '  DECLARE VARIABLE VAR$REPLUSER CHAR(31);'+#13#10+
        'BEGIN'+#13#10+
        '  SELECT REPLUSER FROM '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'CONFIG')+#13#10+
        '  WHERE SCHEMAID=OLD.SCHEMAID AND GROUPID=OLD.GROUPID AND RELATIONID=OLD.RELATIONID'+#13#10+
        '  INTO VAR$REPLUSER;'+#13#10+
        '  IF (USER=VAR$REPLUSER) THEN'+#13#10+
        '    UPDATE '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'SNAPSHOT')+' SET SEQID=OLD.SEQID WHERE SCHEMAID=OLD.SCHEMAID AND GROUPID=OLD.GROUPID AND DBID=0 AND SEQID<OLD.SEQID;'+#13#10+
        'END'
      );

      Log2S:=
        '    FOR SELECT SCHEMAID,GROUPID,DBMASK,DBMASK_PENDING,SEQID,RELATIONID,REPLTYPE,STAMP,OLDPKEY,NEWPKEY FROM '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'LOG')+#13#10+
        '        %s'#13#10+
        '        ORDER BY SCHEMAID, GROUPID, SEQID'+#13#10+
        '        INTO :schemaid,:groupid,:dbmask,:dbmask_pending,:seqid,:relationid,:repltype,:stamp,:oldpkey,:newpkey DO'+#13#10+
        '    BEGIN'+#13#10+
        '      relationname = NULL;'+#13#10+
        '      separator = NULL;'+#13#10+
        '      SELECT RELATIONNAME,SEPARATOR FROM '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'CONFIG')+#13#10+
        '      WHERE SCHEMAID=:schemaid AND GROUPID=:groupid AND RELATIONID=:relationid'+#13#10+
        '      INTO :relationname,:separator;'+#13#10+
        '      SUSPEND;'+#13#10+
        '    END'+#13#10;

      CreateObject(DB, ibobjProcedure, DBProps.ObjPrefix+'LOG2',
        '('+#13#10+
        '  PSCHEMAID INTEGER,'+#13#10+
        '  PGROUPID INTEGER'+#13#10+
        ')'+#13#10+
        'RETURNS'+#13#10+
        '('+#13#10+
        '  SCHEMAID	INTEGER,'+#13#10+
        '  GROUPID	INTEGER,'+#13#10+
        '  DBMASK	INTEGER,'+#13#10+
        '  DBMASK_PENDING	INTEGER,'+#13#10+
        '  SEQID	INTEGER,'+#13#10+
        '  RELATIONID	INTEGER,'+#13#10+
        '  REPLTYPE	CHAR(1),'+#13#10+
        '  STAMP	        TIMESTAMP,'+#13#10+
 Format('  OLDPKEY	VARCHAR(%d),', [aKeyLength])+#13#10+
 Format('  NEWPKEY	VARCHAR(%d),', [aKeyLength])+#13#10+
        '  SEPARATOR	CHAR(1),'+#13#10+
        '  RELATIONNAME	VARCHAR(100)'+#13#10+
        ')'+#13#10+
        'AS'+#13#10+
        'BEGIN'+#13#10+
        '  IF (PSCHEMAID<>0 AND PGROUPID<>0) THEN'#13#10+  // fork for Interbase performance optimization
  Format(Log2S, ['WHERE SCHEMAID=:pschemaid AND GROUPID=:pgroupid'])+
        '  ELSE IF (PSCHEMAID<>0) THEN'#13#10+
  Format(Log2S, ['WHERE SCHEMAID=:pschemaid'])+
        '  ELSE IF (PGROUPID<>0) THEN'#13#10+
  Format(Log2S, ['WHERE GROUPID=:pgroupid'])+
        '  ELSE'#13#10+
  Format(Log2S, ['/*all*/'])+
        'END'
      );
      CreateObject(DB, ibobjProcedure, DBProps.ObjPrefix+'LOG_DELETE',
        '('+#13#10+
        '  SCHEMAID   INTEGER,'+#13#10+
        '  SEQID   INTEGER,'+#13#10+
        '  DBMASK  INTEGER,'+#13#10+
        '  ACTIONTYPE INTEGER,'+#13#10+  // 0..delete,1..set pending,2..move to man */
        '  DESCRIPTION BLOB,'+#13#10+
        '  CONFLICT BLOB'+#13#10+
        ')'+#13#10+
        'RETURNS ('+#13#10+
        '  RESULT INTEGER'+#13#10+   // bit 0..3=0..no action, bit 1..updated DBMASK, bit 2..deleted from log, bit 4..7=1..updated(i.e. $10)/2..inserted into man (i.e. $20)
        ')'+#13#10+
        'AS'+#13#10+
        '   DECLARE VARIABLE VAR$DBMASK INTEGER;'+#13#10+
        '   DECLARE VARIABLE VAR$DBMASK_PENDING INTEGER;'+#13#10+
        '   DECLARE VARIABLE VAR$DBMASK2 INTEGER;'+#13#10+
        '   DECLARE VARIABLE VAR$DBMASK_PENDING2 INTEGER;'+#13#10+
        '   DECLARE VARIABLE VAR$DESCRIPTION BLOB;'+#13#10+
        '   DECLARE VARIABLE VAR$CONFLICT BLOB;'+#13#10+
        'BEGIN'+#13#10+
        '  RESULT = 0;'+#13#10+
        '  SELECT DBMASK,DBMASK_PENDING FROM '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'LOG')+' WHERE SCHEMAID=:SCHEMAID AND SEQID=:SEQID INTO VAR$DBMASK,VAR$DBMASK_PENDING;'+#13#10+
        '  IF (VAR$DBMASK IS NULL) THEN'+#13#10+
        '    VAR$DBMASK = 0;'+#13#10+
        '  ELSE'+#13#10+
        '    IF (ACTIONTYPE IN (0,2)) THEN'+#13#10+
        '      VAR$DBMASK = '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'BIT_OR')+'(VAR$DBMASK,VAR$DBMASK_PENDING);'+#13#10+
        '  IF (DBMASK IS NULL) THEN'+#13#10+
        '    DBMASK = VAR$DBMASK;'+#13#10+
        '  ELSE'+#13#10+
        '    DBMASK = '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'BIT_AND')+'(VAR$DBMASK, DBMASK);'+#13#10+
        '  VAR$DBMASK = NULL;'+#13#10+
        '  VAR$DBMASK_PENDING = NULL;'+#13#10+
        '  IF (ACTIONTYPE = 2) THEN'+#13#10+
        '  BEGIN'+#13#10+
        '    SELECT DBMASK,DESCRIPTION,CONFLICT FROM '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'MAN')+' WHERE SCHEMAID=:SCHEMAID AND SEQID=:SEQID INTO VAR$DBMASK,VAR$DESCRIPTION,VAR$CONFLICT;'+#13#10+
        '    IF (VAR$DBMASK IS NOT NULL) THEN'+#13#10+
        '      BEGIN'+#13#10+
        '        VAR$DBMASK2 = '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'BIT_OR')+'(VAR$DBMASK, DBMASK);'+#13#10+
        '        IF (VAR$DBMASK<>VAR$DBMASK2) THEN'+#13#10+
        '        BEGIN'+#13#10+
        '          RESULT = RESULT+16;'+#13#10+
        '        END'+#13#10+
        '        IF (DESCRIPTION IS NOT NULL) THEN'+#13#10+
        '          IF (VAR$DESCRIPTION IS NOT NULL) THEN'+#13#10+
        '            VAR$DESCRIPTION = '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'CONCAT_BLOBS')+'(VAR$DESCRIPTION,DESCRIPTION);'+#13#10+
        '          ELSE'+#13#10+
        '            VAR$DESCRIPTION = DESCRIPTION;'+#13#10+
        '        IF (CONFLICT IS NOT NULL) THEN'+#13#10+
        '          IF (VAR$CONFLICT IS NOT NULL) THEN'+#13#10+
        '            VAR$CONFLICT = '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'CONCAT_BLOBS')+'(VAR$CONFLICT,CONFLICT);'+#13#10+
        '          ELSE'+#13#10+
        '            VAR$CONFLICT = CONFLICT;'+#13#10+
        '        IF (VAR$DBMASK<>VAR$DBMASK2 OR DESCRIPTION IS NOT NULL OR CONFLICT IS NOT NULL) THEN'+#13#10+
        '        BEGIN'+#13#10+
        '          UPDATE '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'MAN')+' SET DBMASK=:VAR$DBMASK2,DESCRIPTION=:VAR$DESCRIPTION,CONFLICT=:VAR$CONFLICT WHERE SCHEMAID=:SCHEMAID AND SEQID=:SEQID;'+#13#10+
        '        END'+#13#10+
        '      END'+#13#10+
        '    ELSE'+#13#10+
        '      BEGIN'+#13#10+
        '        INSERT INTO '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'MAN')+'(SCHEMAID,GROUPID,DBMASK,SEQID,RELATIONID,REPLTYPE,STAMP,OLDPKEY,NEWPKEY,DESCRIPTION,CONFLICT)'+#13#10+
        '                      SELECT SCHEMAID,GROUPID,:DBMASK,SEQID,RELATIONID,REPLTYPE,STAMP,OLDPKEY,NEWPKEY,:DESCRIPTION,:CONFLICT FROM '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'LOG')+''+#13#10+
        '                      WHERE SCHEMAID=:SCHEMAID AND SEQID=:SEQID;'+#13#10+
        '        VAR$DBMASK = NULL;'+#13#10+
        '        SELECT DBMASK FROM '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'MAN')+' WHERE SCHEMAID=:SCHEMAID AND SEQID=:SEQID INTO VAR$DBMASK;'+#13#10+
        '        IF (VAR$DBMASK IS NOT NULL) THEN'+#13#10+
        '          RESULT = RESULT+32;'+#13#10+
        '      END'+#13#10+
        '  END'+#13#10+
        '  VAR$DBMASK = NULL;'+#13#10+
        '  SELECT DBMASK,DBMASK_PENDING FROM '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'LOG')+' WHERE SCHEMAID=:SCHEMAID AND SEQID=:SEQID INTO VAR$DBMASK,VAR$DBMASK_PENDING;'+#13#10+
        '  IF (VAR$DBMASK IS NOT NULL) THEN'+#13#10+
        '  BEGIN'+#13#10+
        '    VAR$DBMASK2 = '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'BIT_AND')+'(VAR$DBMASK, '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'BIT_NOT')+'(DBMASK));'+#13#10+
        '    IF (ACTIONTYPE = 1) THEN'+#13#10+
        '      VAR$DBMASK_PENDING2 = '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'BIT_OR')+'(VAR$DBMASK_PENDING, DBMASK);'+#13#10+
        '    ELSE'+#13#10+
        '      VAR$DBMASK_PENDING2 = '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'BIT_AND')+'(VAR$DBMASK_PENDING, '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'BIT_NOT')+'(DBMASK));'+#13#10+
        '    IF (VAR$DBMASK2<>0 OR VAR$DBMASK_PENDING2<>0) THEN'+#13#10+
        '      BEGIN'+#13#10+
        '        IF (VAR$DBMASK<>VAR$DBMASK2 OR VAR$DBMASK_PENDING<>VAR$DBMASK_PENDING2) THEN'+#13#10+
        '        BEGIN'+#13#10+
        '          RESULT = RESULT+1;'+#13#10+
        '          UPDATE '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'LOG')+' SET DBMASK=:VAR$DBMASK2,DBMASK_PENDING=:VAR$DBMASK_PENDING2 WHERE SCHEMAID=:SCHEMAID AND SEQID=:SEQID;'+#13#10+
        '        END'+#13#10+
        '      END'+#13#10+
        '    ELSE'+#13#10+
        '      BEGIN'+#13#10+
        '        DELETE FROM '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'LOG')+' WHERE SCHEMAID=:SCHEMAID AND SEQID=:SEQID;'+#13#10+
        '        RESULT = RESULT+2;'+#13#10+
        '      END'+#13#10+
        '  END'+#13#10+
        'END'
      );
      CreateObject(DB, ibobjProcedure, DBProps.ObjPrefix+'MAN_DELETE',
        '('+#13#10+
        '  SCHEMAID   INTEGER,'+#13#10+
        '  SEQID   INTEGER,'+#13#10+
        '  DBMASK  INTEGER,'+#13#10+
        '  ACTIONTYPE INTEGER'+#13#10+  // 0..delete,1..move to log */
        ')'+#13#10+
        'RETURNS ('+#13#10+
        '  RESULT INTEGER'+#13#10+   // bit 0..3=0..no action, bit 1..updated DBMASK, bit 2..deleted from man, bit 4..7=1..updated/2..inserted into log
        ')'+#13#10+               
        'AS'+#13#10+
        '   DECLARE VARIABLE VAR$DBMASK INTEGER;'+#13#10+
        '   DECLARE VARIABLE VAR$DBMASK2 INTEGER;'+#13#10+
        'BEGIN'+#13#10+
        '  RESULT = 0;'+#13#10+
        '  SELECT DBMASK FROM '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'MAN')+' WHERE SCHEMAID=:SCHEMAID AND SEQID=:SEQID INTO VAR$DBMASK;'+#13#10+
        '  IF (VAR$DBMASK IS NULL) THEN'+#13#10+
        '    VAR$DBMASK = 0;'+#13#10+
        '  IF (DBMASK IS NULL) THEN'+#13#10+
        '    DBMASK = VAR$DBMASK;'+#13#10+
        '  ELSE'+#13#10+
        '    DBMASK = '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'BIT_AND')+'(VAR$DBMASK, DBMASK);'+#13#10+
        '  VAR$DBMASK = NULL;'+#13#10+
        '  IF (ACTIONTYPE = 1) THEN'+#13#10+
        '  BEGIN'+#13#10+
        '    SELECT DBMASK FROM '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'LOG')+' WHERE SCHEMAID=:SCHEMAID AND SEQID=:SEQID INTO VAR$DBMASK;'+#13#10+
        '    IF (VAR$DBMASK IS NOT NULL) THEN'+#13#10+
        '      BEGIN'+#13#10+
        '        VAR$DBMASK2 = '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'BIT_OR')+'(VAR$DBMASK, DBMASK);'+#13#10+
        '        IF (VAR$DBMASK<>VAR$DBMASK2) THEN'+#13#10+
        '        BEGIN'+#13#10+
        '          RESULT = RESULT+16;'+#13#10+
        '          UPDATE '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'LOG')+' SET DBMASK=:VAR$DBMASK2,DBMASK_PENDING='+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'BIT_AND')+'(DBMASK_PENDING,'+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'BIT_NOT')+'(:DBMASK)) WHERE SCHEMAID=:SCHEMAID AND SEQID=:SEQID;'+#13#10+
        '        END'+#13#10+
        '      END'+#13#10+
        '    ELSE'+#13#10+
        '      BEGIN'+#13#10+
        '        INSERT INTO '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'LOG')+'(SCHEMAID,GROUPID,DBMASK,SEQID,RELATIONID,REPLTYPE,STAMP,OLDPKEY,NEWPKEY)'+#13#10+
        '                      SELECT SCHEMAID,GROUPID,:DBMASK,SEQID,RELATIONID,REPLTYPE,STAMP,OLDPKEY,NEWPKEY FROM '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'MAN')+''+#13#10+
        '                      WHERE SCHEMAID=:SCHEMAID AND SEQID=:SEQID;'+#13#10+
        '        VAR$DBMASK = NULL;'+#13#10+
        '        SELECT DBMASK FROM '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'LOG')+' WHERE SCHEMAID=:SCHEMAID AND SEQID=:SEQID INTO VAR$DBMASK;'+#13#10+
        '        IF (VAR$DBMASK IS NOT NULL) THEN'+#13#10+
        '          RESULT = RESULT+32;'+#13#10+
        '      END'+#13#10+
        '  END'+#13#10+
        '  VAR$DBMASK = NULL;'+#13#10+
        '  SELECT DBMASK FROM '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'MAN')+' WHERE SCHEMAID=:SCHEMAID AND SEQID=:SEQID INTO VAR$DBMASK;'+#13#10+
        '  IF (VAR$DBMASK IS NOT NULL) THEN'+#13#10+
        '  BEGIN'+#13#10+
        '    VAR$DBMASK2 = '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'BIT_AND')+'(VAR$DBMASK, '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'BIT_NOT')+'(DBMASK));'+#13#10+
        '    IF (VAR$DBMASK2<>0) THEN'+#13#10+
        '      BEGIN'+#13#10+
        '        IF (VAR$DBMASK<>VAR$DBMASK2) THEN'+#13#10+
        '        BEGIN'+#13#10+
        '          UPDATE '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'MAN')+' SET DBMASK=:VAR$DBMASK2 WHERE SCHEMAID=:SCHEMAID AND SEQID=:SEQID;'+#13#10+
        '          RESULT = RESULT+1;'+#13#10+
        '        END'+#13#10+
        '      END'+#13#10+
        '    ELSE'+#13#10+
        '      BEGIN'+#13#10+
        '        DELETE FROM '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'MAN')+' WHERE SCHEMAID=:SCHEMAID AND SEQID=:SEQID;'+#13#10+
        '        RESULT = RESULT+2;'+#13#10+
        '      END'+#13#10+
        '  END'+#13#10+
        'END'
      );
      CreateObject(DB, ibobjProcedure, DBProps.ObjPrefix+'LOG_INSERT',
        '('+#13#10+
        '  SCHEMAID   INTEGER,'+
        '  GROUPID    INTEGER,'+
        '  DBMASK     INTEGER,'+
        '  SEQID      INTEGER,'+
        '  RELATIONID INTEGER,'+
        '  REPLTYPE   CHAR(1),'+
        '  STAMP      TIMESTAMP,'+
 Format('  OLDPKEY    VARCHAR(%d),', [GetRelationFieldLength(DB, DBProps.ObjPrefix+'LOG', 'OLDPKEY')])+
 Format('  NEWPKEY    VARCHAR(%d)', [GetRelationFieldLength(DB, DBProps.ObjPrefix+'LOG', 'NEWPKEY')])+
        ')'+#13#10+
        'RETURNS ('+#13#10+
        '  RESULT INTEGER'+#13#10+
        ')'+#13#10+
        'AS'+#13#10+
        '   DECLARE VARIABLE VAR$DBMASK INTEGER;'+#13#10+
        '   DECLARE VARIABLE VAR$DBMASK2 INTEGER;'+#13#10+
        'BEGIN'+#13#10+
        '  RESULT = 0;'+#13#10+
        '  SELECT DBMASK FROM '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'LOG')+' WHERE SCHEMAID=:SCHEMAID AND SEQID=:SEQID INTO VAR$DBMASK;'+#13#10+
        '  IF (VAR$DBMASK IS NOT NULL) THEN'+#13#10+
        '    BEGIN'+#13#10+
        '      VAR$DBMASK2 = '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'BIT_OR')+'(VAR$DBMASK, DBMASK);'+#13#10+
        '      IF (VAR$DBMASK<>VAR$DBMASK2) THEN'+#13#10+
        '      BEGIN'+#13#10+
        '        RESULT = 1;'+#13#10+
        '        UPDATE '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'LOG')+' SET DBMASK=:VAR$DBMASK2 WHERE SCHEMAID=:SCHEMAID AND SEQID=:SEQID;'+#13#10+
        '      END'+#13#10+
        '    END'+#13#10+
        '  ELSE'+#13#10+
        '    BEGIN'+#13#10+
        '      INSERT INTO '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'LOG')+'(SCHEMAID,GROUPID,DBMASK,SEQID,RELATIONID,REPLTYPE,STAMP,OLDPKEY,NEWPKEY)'+#13#10+
        '                    VALUES(:SCHEMAID,:GROUPID,:DBMASK,:SEQID,:RELATIONID,:REPLTYPE,:STAMP,:OLDPKEY,:NEWPKEY);'+#13#10+
        '      RESULT = 2;'+#13#10+
        '    END'+#13#10+
        'END'
      );

      CreateObject(DB, ibobjProcedure, DBProps.ObjPrefix+'FIELD_INSERT',
        ' ('+#13#10+
        '     SCHEMAID INTEGER,'+#13#10+
        '     SEQID    INTEGER,'+#13#10+
        '     MODE     CHAR(1),'+#13#10+
        '     FIELDID  INTEGER,'+#13#10+
        '     DATATYPE CHAR(1),'+#13#10+
        '     DATA_N   INTEGER,'+#13#10+
        '     DATA_B   BLOB ,'+#13#10+
        '     DATA_M   BLOB ,'+#13#10+
        '     DATA_I   INTEGER,'+#13#10+
        '     DATA_T   TIMESTAMP,'+#13#10+
        '     DATA_D   DOUBLE PRECISION,'+#13#10+
 Format('     DATA_S   VARCHAR(%d))'+#13#10, [MaxVARCHARLength])+
        'AS'+#13#10+
        '  DECLARE VARIABLE VAR$N INTEGER;'+#13#10+
        'BEGIN'+#13#10+
        '  SELECT COUNT(*) FROM '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'FIELD')+' WHERE SCHEMAID=:SCHEMAID AND SEQID=:SEQID AND MODE=:MODE AND FIELDID=:FIELDID INTO VAR$N;'+#13#10+
        '  IF (VAR$N > 0) THEN'+#13#10+
        '    EXIT;'+#13#10+
        '  IF (DATATYPE IS NULL) THEN'+#13#10+
        '    DATATYPE = ''N'';'+#13#10+
        '  IF (DATATYPE<>''B'') THEN'+#13#10+
        '    BEGIN'+#13#10+
        '      IF (DATATYPE<>''M'') THEN'+#13#10+
        '        BEGIN'+#13#10+
        '          IF (DATATYPE<>''I'') THEN'+#13#10+
        '            BEGIN'+#13#10+
        '              IF (DATATYPE<>''T'') THEN'+#13#10+
        '                BEGIN'+#13#10+
        '                  IF (DATATYPE<>''D'') THEN'+#13#10+
        '                    BEGIN'+#13#10+
        '                      IF (DATATYPE<>''S'') THEN'+#13#10+
        '                        BEGIN'+#13#10+
        '                          DATA_B = NULL;'+#13#10+
        '                          DATATYPE = ''N'';'+#13#10+
        '                        END'+#13#10+
        '                      ELSE'+#13#10+
        '                        BEGIN'+#13#10+
        '                          DATA_B = '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'STRING_TO_BLOB')+'(DATA_S);'+#13#10+
        '                        END'+#13#10+
        '                    END'+#13#10+
        '                  ELSE'+#13#10+
        '                    BEGIN'+#13#10+
        '                      DATA_B = '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'DOUBLE_TO_BLOB')+'(DATA_D);'+#13#10+
        '                    END'+#13#10+
        '                END'+#13#10+
        '              ELSE'+#13#10+
        '                BEGIN'+#13#10+
        '                  DATA_B = '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'TIMESTAMP_TO_BLOB')+'(DATA_T);'+#13#10+
        '                END'+#13#10+
        '            END'+#13#10+
        '          ELSE'+#13#10+
        '            BEGIN'+#13#10+
        '              DATA_B = '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'INTEGER_TO_BLOB')+'(DATA_I);'+#13#10+
        '            END'+#13#10+
        '        END'+#13#10+
        '      ELSE'+#13#10+
        '        BEGIN'+#13#10+
        '          DATA_B = DATA_M;'+#13#10+
        '        END'+#13#10+
        '    END'+#13#10+
        '  INSERT INTO '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'FIELD')+'(SCHEMAID, SEQID, MODE, FIELDID, DATATYPE, DATA) VALUES (:SCHEMAID, :SEQID, :MODE, :FIELDID, :DATATYPE, :DATA_B);'+#13#10+
        'END'
      );

      CreateObject(DB, ibobjProcedure, DBProps.ObjPrefix+'FIELD2',
        '('+#13#10+
        '  PSCHEMAID INTEGER,'+#13#10+
        '  PSEQID INTEGER,'+#13#10+
        '  PMODE CHAR(1),   /* not used */'+#13#10+
        '  PFIELDID INTEGER /* not used */'+#13#10+
        ')'+#13#10+
        'RETURNS'+#13#10+
        '('+#13#10+
        '  SCHEMAID	INTEGER,'+#13#10+
        '  SEQID	INTEGER,'+#13#10+
        '  MODE	CHAR(1),'+#13#10+
        '  FIELDID	INTEGER,'+#13#10+
        '  DATATYPE	CHAR(1),'+#13#10+
        '  DATA_N	INTEGER,'+#13#10+
        '  DATA_B   BLOB,'+#13#10+
        '  DATA_M   BLOB,'+#13#10+
        '  DATA_I   INTEGER,'+#13#10+
        '  DATA_D   DOUBLE PRECISION,'+#13#10+
        '  DATA_T   TIMESTAMP,'+#13#10+
 Format('  DATA_S   VARCHAR(%d)'#13#10, [MaxVARCHARLength])+
        ')'+#13#10+
        'AS'+#13#10+
        'BEGIN'+#13#10+
        '  Data_N = NULL;'+#13#10+
        '  FOR SELECT SCHEMAID,SEQID,FIELDID,MODE,DATATYPE,DATA FROM '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'FIELD')+#13#10+
//      '      WHERE (SCHEMAID=:pschemaid OR :pschemaid=0) AND (SEQID=:pseqid OR :pseqid=0) AND (MODE=:pmode OR :pmode = '''') AND (FIELDID=:pfieldid OR :pfieldid=0)'+#13#10+ // very slow on large tables
        '      WHERE SCHEMAID=:pschemaid AND SEQID=:pseqid'+#13#10+
        '      ORDER BY SCHEMAID,SEQID,MODE,FIELDID'+#13#10+
        '      INTO :schemaid,:seqid,:fieldid,:mode,:datatype,:data_b DO'+#13#10+
        '  BEGIN'+#13#10+
        '    IF (datatype = ''T'') THEN'+#13#10+
        '      Data_T = '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'BLOB_TO_TIMESTAMP')+'(:data_b);'+#13#10+
        '    ELSE'+#13#10+
        '      Data_T = NULL;'+#13#10+
        '    IF (datatype = ''I'') THEN'+#13#10+
        '      Data_I = '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'BLOB_TO_INTEGER')+'(:data_b);'+#13#10+
        '    ELSE'+#13#10+
        '      Data_I = NULL;'+#13#10+
        '    IF (datatype = ''D'') THEN'+#13#10+
        '      Data_D = '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'BLOB_TO_DOUBLE')+'(:data_b);'+#13#10+
        '    ELSE'+#13#10+
        '      Data_D = NULL;'+#13#10+
        '    IF (datatype = ''S'') THEN'+#13#10+
        '      Data_S = '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'STRING_COPY')+'('+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'BLOB_TO_STRING')+'(:data_b),1,'+IntToStr(MaxVARCHARLength)+');'#13#10+
        '    ELSE'+#13#10+
        '      Data_S = NULL;'+#13#10+
        '    IF (datatype = ''M'') THEN'+#13#10+
        '      Data_M = Data_B;'+#13#10+
        '    ELSE'+#13#10+
        '      Data_M = NULL;'+#13#10+
        '    SUSPEND;'+#13#10+
        '  END'+#13#10+
        'END'
      );
      if SourceF then
      begin
        CreateObject(DB, ibobjRelation, DBProps.ObjPrefix+'HISTORY',
          '('+
          '  SCHEMAID   INTEGER NOT NULL,'+
          '  GROUPID    INTEGER NOT NULL,'+
          '  RELATIONID INTEGER NOT NULL,'+
          '  SEQID      INTEGER NOT NULL,'+
          '  STAMP      TIMESTAMP DEFAULT ''NOW'' NOT NULL,'+
          '  VERTYPE    CHAR(1),'+
   Format('  OLDPKEY	  VARCHAR(%d),', [aKeyLength])+#13#10+
   Format('  NEWPKEY	  VARCHAR(%d),', [aKeyLength])+#13#10+
          ' PRIMARY KEY (SCHEMAID,GROUPID,RELATIONID,SEQID)'+
          ')'
        );
        if not IndexExists(DB, 'I_'+DBProps.ObjPrefix+'HISTORY_SEQID') then
        begin
          DBLogVerbose(Format(sCreatingObject, ['INDEX', 'I_'+DBProps.ObjPrefix+'HISTORY_SEQID']));
          DBSQLExec(DB,
            Format('CREATE UNIQUE INDEX %s ON %s(SCHEMAID,SEQID)', [FormatIdentifier(DB.SQLDialect, 'I_'+DBProps.ObjPrefix+'HISTORY_SEQID'), FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'HISTORY')])
          );
        end;
        CreateObject(DB, ibobjTrigger, 'TG_D_'+DBProps.ObjPrefix+'HISTORY_FIELD',
          'FOR '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'HISTORY')+#13#10+
          'ACTIVE AFTER DELETE POSITION 0'+#13#10+
          'AS'+#13#10+
          'BEGIN'+#13#10+
          '  DELETE FROM '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'FIELD')+' WHERE SCHEMAID=OLD.SCHEMAID AND SEQID=OLD.SEQID;'+#13#10+
          'END'
        );
        CreateObject(DB, ibobjTrigger, 'TG_I_'+DBProps.ObjPrefix+'HISTORY_SEQID',
          'FOR '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'HISTORY')+#13#10+
          'ACTIVE BEFORE INSERT POSITION 0'+#13#10+
          'AS'+#13#10+
          'BEGIN'+#13#10+
          '  IF (NEW.SEQID IS NULL) THEN'+#13#10+
          '    NEW.SEQID = GEN_ID('+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'GEN_SEQID')+',1);'+#13#10+
          Iif(fNowAsUTC, '  NEW.STAMP = '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'NOWUTC')+'();'+#13#10, '')+
          'END'
        );
      end;
//    SafeStartTransaction(DB.DefaultTransaction);
      try
        if aSchemaId <> 0 then
        begin
          BuildImplicitEnvironment(['SCHEMAID', 'GROUPID'], [IntToStr(aSchemaId), IntToStr(aGroupId)], False);
          SchemaType:= GetSchemaType(aSchemaId);
          Q1:= TIBSQL.Create(nil);
          try
            Q1.Database:= fConfigDatabase;
            Q1.Transaction:= fConfigDatabaseTransaction;

            V:= DBSQLRecord(fConfigDatabase, Format('SELECT REPLUSER,REPLROLE,SEPARATOR FROM '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'SCHEMADB')+' WHERE SCHEMAID=%d AND GROUPID=%d AND DBID=%d', [aSchemaId, aGroupId, aDBId]));
            if not VarIsArray(V) or (V[0]=Null) or (TrimRight(V[0]) = '') then
              IBReplicatorError(Format(sNoReplUser, [aSchemaId, aGroupId, aDBId]), 1);
            Separator:= Chr(Integer(V[2]));
            ReplUserS:= UpperCase(ParseStr(V[0]));
            if V[1] <> Null then
              ReplRoleS:= ParseStr(V[1])
            else
              ReplRoleS:= '';

            // grant rights to objects
            S:= ReplUserS;
            if ReplRoleS <> '' then
            begin
              S:= ReplRoleS;
              CreateObject(DB, ibobjRole, DBProps.ObjPrefix+ReplRoleS, '');
              DBSQLExec(DB, Format('GRANT %s TO %s;', [FormatIdentifier(DB.SQLDialect, ReplRoleS), FormatIdentifier(DB.SQLDialect, ReplUserS)]));
            end;

            DBSQLExec(DB, Format('DELETE FROM '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'CONFIG')+' WHERE SCHEMAID=%d AND (GROUPID=%d OR %d=0)', [aSchemaId, aGroupId, aGroupId]));
          { refresh REPL$CONFIG table }
            if SourceF then
              begin
                Q1.SQL.Add('SELECT S.GROUPID,R.RELATIONID,R.RELATIONNAME,CAST(SUM(S.DBMASK) AS INTEGER) AS DBMASK FROM '+FormatIdentifier(Q1.Database.SQLDialect, fConfigDatabasePrefix+'RELATIONS')+' R INNER JOIN '+FormatIdentifier(Q1.Database.SQLDialect, fConfigDatabasePrefix+'SCHEMADB')+' S ON R.SCHEMAID=S.SCHEMAID AND R.GROUPID=S.GROUPID');
                Q1.SQL.Add(Format('WHERE R.SCHEMAID=%d AND S.DISABLED=''N'' AND R.DISABLED=''N'' GROUP BY S.GROUPID,R.RELATIONID,R.RELATIONNAME', [aSchemaId]));
              end
            else
              begin
                Q1.SQL.Add('SELECT S.GROUPID,R.RELATIONID,R.TARGETTYPE,R.TARGETNAME AS RELATIONNAME,CAST(SUM(S.DBMASK) AS INTEGER) AS DBMASK FROM '+FormatIdentifier(Q1.Database.SQLDialect, fConfigDatabasePrefix+'RELATIONS')+' R INNER JOIN '+FormatIdentifier(Q1.Database.SQLDialect, fConfigDatabasePrefix+'SCHEMADB')+' S ON R.SCHEMAID=S.SCHEMAID AND R.GROUPID=S.GROUPID');
                Q1.SQL.Add(Format('WHERE R.SCHEMAID=%d AND S.GROUPID=%d AND S.DBID=%d AND S.DISABLED=''N'' AND R.DISABLED=''N'' GROUP BY S.GROUPID,R.RELATIONID,R.TARGETNAME,R.TARGETTYPE', [aSchemaId, aGroupId, aDBId]));
              end;
            SafeExecQuery(Q1);
            while not Q1.EOF do
            begin
              DBSQLExec(DB, 'INSERT INTO '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'CONFIG')+'(SCHEMAID,GROUPID,DBMASK,RELATIONID,RELATIONNAME,SEPARATOR,DISABLED,SRCDBID,REPLUSER) '+
                                     Format('VALUES (%d,%d,%d,%d,''%s'',''%s'',''N'',%d,''%s'');',
                                            [aSchemaId, Q1.FieldByName('GROUPID').asInteger, Q1.FieldByName('DBMASK').asInteger, Q1.FieldByName('RELATIONID').asInteger, TrimRight(Q1.FieldByName('RELATIONNAME').asString), Separator, GetSourceDBId(aSchemaId), ReplUserS]));
              if SourceF or (Q1.FieldByName('TARGETTYPE').asString = 'T') then
                begin
                  if ObjectExists(DB, ibobjRelation, TrimRight(Q1.FieldByName('RELATIONNAME').asString)) then
                    DBSQLExec(DB, Format('GRANT ALL ON %s TO %s;', [FormatIdentifier(DB.SQLDialect, TrimRight(Q1.FieldByName('RELATIONNAME').asString)), FormatIdentifier(DB.SQLDialect, S)]))
                  else
                    fDBLog.Log(fLogName, lchError, Format(sRelationDoesNotExists, [TrimRight(Q1.FieldByName('RELATIONNAME').asString)]));
                end
              else if Q1.FieldByName('TARGETTYPE').asString = 'P' then
                begin
                  if ObjectExists(DB, ibobjProcedure, TrimRight(Q1.FieldByName('RELATIONNAME').asString)) then
                    DBSQLExec(DB, Format('GRANT EXECUTE ON PROCEDURE %s TO %s;', [FormatIdentifier(DB.SQLDialect, TrimRight(Q1.FieldByName('RELATIONNAME').asString)), FormatIdentifier(DB.SQLDialect, S)]))
                  else
                    fDBLog.Log(fLogName, lchError, Format(sProcedureDoesNotExists, [TrimRight(Q1.FieldByName('RELATIONNAME').asString)]));
                end;
              SafeNext(Q1);
            end;
            SafeClose(Q1);

            DBSQLExec(DB, Format('GRANT ALL ON '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'TRANSFER')+' TO %s;', [FormatIdentifier(DB.SQLDialect, S)]));
            DBSQLExec(DB, Format('GRANT ALL ON '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'LOG')+' TO %s;', [FormatIdentifier(DB.SQLDialect, S)]));
            DBSQLExec(DB, Format('GRANT ALL ON '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'MAN')+' TO %s;', [FormatIdentifier(DB.SQLDialect, S)]));
            DBSQLExec(DB, Format('GRANT ALL ON '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'CONFIG')+' TO %s;', [FormatIdentifier(DB.SQLDialect, S)]));
            DBSQLExec(DB, Format('GRANT ALL ON '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'FIELD')+' TO %s;', [FormatIdentifier(DB.SQLDialect, S)]));
            DBSQLExec(DB, Format('GRANT ALL ON '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'EXT_FILE')+' TO %s;', [FormatIdentifier(DB.SQLDialect, S)]));
            DBSQLExec(DB, Format('GRANT EXECUTE ON PROCEDURE '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'LOG2')+' TO %s;', [FormatIdentifier(DB.SQLDialect, S)]));
            DBSQLExec(DB, Format('GRANT EXECUTE ON PROCEDURE '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'LOG_DELETE')+' TO %s;', [FormatIdentifier(DB.SQLDialect, S)]));
            DBSQLExec(DB, Format('GRANT EXECUTE ON PROCEDURE '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'MAN_DELETE')+' TO %s;', [FormatIdentifier(DB.SQLDialect, S)]));
            DBSQLExec(DB, Format('GRANT EXECUTE ON PROCEDURE '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'LOG_INSERT')+' TO %s;', [FormatIdentifier(DB.SQLDialect, S)]));
            DBSQLExec(DB, Format('GRANT EXECUTE ON PROCEDURE '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'FIELD_INSERT')+' TO %s;', [FormatIdentifier(DB.SQLDialect, S)]));
            DBSQLExec(DB, Format('GRANT EXECUTE ON PROCEDURE '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'FIELD2')+' TO %s;', [FormatIdentifier(DB.SQLDialect, S)]));
            DBSQLExec(DB, Format('GRANT ALL ON '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'ENVIRONMENT')+' TO %s;', [FormatIdentifier(DB.SQLDialect, S)]));
            DBSQLExec(DB, Format('GRANT ALL ON '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'SNAPSHOT')+' TO %s;', [FormatIdentifier(DB.SQLDialect, S)]));
            if SourceF then
            begin
              DBSQLExec(DB, Format('GRANT ALL ON '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'HISTORY')+' TO %s;', [FormatIdentifier(DB.SQLDialect, S)]));
            end;

            // add records to REPL$SNAPSHOT table
            Q1.SQL.Clear;
            Q1.SQL.Add('SELECT DISTINCT GROUPID');
            Q1.SQL.Add('FROM '+FormatIdentifier(Q1.Database.SQLDialect, fConfigDatabasePrefix+'SCHEMADB'));
            Q1.SQL.Add(Format('WHERE SCHEMAID=%d AND '+iif(SourceF, 'GROUPID<>0 /*%d %d*/', 'GROUPID=%d AND DBID=%d')+' AND DISABLED=''N''', [aSchemaId, aGroupId, aDBId]));
            SafeExecQuery(Q1);
            while not Q1.EOF do
            begin
              if DBSQLRecord(DB, Format('SELECT CAST(COUNT(*) AS INTEGER) FROM %s WHERE SCHEMAID=%d AND GROUPID=%d AND DBID=0', [FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'SNAPSHOT'), aSchemaId, Q1.FieldByName('GROUPID').asInteger])) = 0 then
                DBSQLExec(DB, Format('INSERT INTO %s (SCHEMAID,GROUPID,DBID,SEQID) VALUES (%d,%d,0,0);', [FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'SNAPSHOT'), aSchemaId, Q1.FieldByName('GROUPID').asInteger]));
              SafeNext(Q1);
            end;
            SafeClose(Q1);
            if not SourceF then
            begin
              if DBSQLRecord(DB, Format('SELECT CAST(COUNT(*) AS INTEGER) FROM %s WHERE SCHEMAID=%d AND GROUPID=%d AND DBID=%d', [FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'SNAPSHOT'), aSchemaId, aGroupId, aDBId])) = 0 then
                DBSQLExec(DB, Format('INSERT INTO %s (SCHEMAID,GROUPID,DBID,SEQID) VALUES (%d,%d,%d,0);', [FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'SNAPSHOT'), aSchemaId, aGroupId, aDBId]));
            end;

            // create replication/version triggers
            if SourceF then
            begin
              Q1.SQL.Clear;
              Q1.SQL.Add('SELECT R.GROUPID,R.RELATIONID,R.RELATIONNAME,R.OPTIONS,R.CONDITION,F.FIELDNAME,F.FIELDTYPE,F.FIELDID');
              Q1.SQL.Add('FROM '+FormatIdentifier(Q1.Database.SQLDialect, fConfigDatabasePrefix+'RELATIONS')+' R INNER JOIN '+FormatIdentifier(Q1.Database.SQLDialect, fConfigDatabasePrefix+'FIELDS')+' F ON R.SCHEMAID=F.SCHEMAID AND R.GROUPID=F.GROUPID AND R.RELATIONID=F.RELATIONID');
              Q1.SQL.Add(Format('WHERE R.SCHEMAID=%d AND (R.GROUPID=%d OR %d=0) AND R.DISABLED=''N''', [aSchemaId, aGroupId, aGroupId]));
              Q1.SQL.Add('ORDER BY R.GROUPID,R.RELATIONID,F.FIELDTYPE,F.FIELDID');
              SafeExecQuery(Q1);
              RelId:= -1; WipeF:= False; GrpId:= -1;
              while not Q1.EOF do
              begin
                if (GrpId <> Q1.FieldByName('GROUPID').asInteger) or (RelId <> Q1.FieldByName('RELATIONID').asInteger) then
                begin
                  if RelId <> -1 then
                  begin
                    if SchemaType = schtRecordHistory then
                      CreateVerTgs(GrpId, RelId, RS, PKS, FKS, FKIds, TgCondition)
                    else
                      CreateReplTgs(GrpId, RelId, RS, PKS, FKS, FKIds, TgCondition, ReplUserS, WipeF);
                  end;
                  GrpId:= Q1.FieldByName('GROUPID').asInteger;
                  RelId:= Q1.FieldByName('RELATIONID').asInteger;
                  RS:= Q1.FieldByName('RELATIONNAME').asString;
                  WipeF:= Q1.FieldByName('OPTIONS').asInteger and reloptWipeLogOnDelete <> 0;
                  TgCondition:= Q1.FieldByName('CONDITION').asString;
                  SetLength(PKS, 0);
                  SetLength(FKS, 0);
                  SetLength(FKIds, 0);
                end;
                case Q1.FieldByName('FIELDTYPE').asInteger of
                  1:
                    begin
                      SetLength(PKS, Length(PKS)+1);
                      PKS[Length(PKS)-1]:= Q1.FieldByName('FIELDNAME').asString;
                      if SchemaType = schtRecordHistory then
                      begin
                        SetLength(FKS, Length(FKS)+1);
                        FKS[Length(FKS)-1]:= Q1.FieldByName('FIELDNAME').asString;
                        SetLength(FKIds, Length(FKIds)+1);
                        FKIds[Length(FKIds)-1]:= Q1.FieldByName('FIELDID').asInteger;
                      end;
                    end;
                  2:
                    begin
                      SetLength(FKS, Length(FKS)+1);
                      FKS[Length(FKS)-1]:= Q1.FieldByName('FIELDNAME').asString;
                      SetLength(FKIds, Length(FKIds)+1);
                      FKIds[Length(FKIds)-1]:= Q1.FieldByName('FIELDID').asInteger;
                    end;
                end;
                SafeNext(Q1);
              end;
              if RelId <> -1 then
              begin
                if SchemaType = schtRecordHistory then
                  CreateVerTgs(GrpId, RelId, RS, PKS, FKS, FKIds, TgCondition)
                else
                  CreateReplTgs(GrpId, RelId, RS, PKS, FKS, FKIds, TgCondition, ReplUserS, WipeF);
              end;
            end;
          finally
            Q1.Free;
          end;
        end;
        DBEnvironmentWriteValue(DB, DBProps.ObjPrefix, VersionEnvironmentName, CurVersion);
        CreateCustomObjects(DB, DBProps, aDBId);
//      SafeCommit(DB.DefaultTransaction);
      except
//      SafeRollback(DB.DefaultTransaction);
        raise;
      end;
    finally
      DB.Free;
    end;
    if not InTran then
      SafeCommit(fConfigDatabaseTransaction);
  except
    if not InTran then
      SafeRollback(fConfigDatabaseTransaction);
    raise;
  end;
end;

procedure TIBReplicator.DropSystemObjects(aSchemaId: Integer; aGroupId{master database}: Integer; aDBId: Integer = 0; aOnlyTriggers: Boolean = False);
var
  Q1: TIBSQL;
  DB: TIBDatabase;
  V: Variant;
  DBProps: TReplDatabaseProperties;
  SourceF, LastF: Boolean;
  I: Integer;
  InTran: Boolean;

  procedure DropTg(const aDruh: string; aGrpId, aRelId: Integer; aRelName: string);
  begin
    DropObject(DB, ibobjTrigger, GetTriggerName(aSchemaID, aGrpId, aRelId, aRelName, aDruh[1], DBProps));
  end;
  procedure DelT(const aTbl: string);
  begin
    if ObjectExists(DB, ibobjRelation, DBProps.ObjPrefix+aTbl) then
      DBSQLExec(DB, Format('DELETE FROM '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+aTbl)+' WHERE SCHEMAID=%d AND (GROUPID=%d OR %d=0)', [aSchemaId, aGroupId, aGroupId]));
  end;
begin
{$IFDEF REGISTRATION}
  TReg_CheckRegistration(False);
{$ENDIF}
  InTran:= fConfigDatabaseTransaction.InTransaction;
  if not InTran then
    SafeStartTransaction(fConfigDatabaseTransaction);
  try
    SourceF:= aGroupId = 0;
    if SourceF and (aDBId = 0{enable overriding, used when dropping according REPL$LOG}) then
      aDBId:= GetSourceDBId(aSchemaId);
    if GetDatabaseType(aDBId) <> dbtInterbase then
      Abort; // to close transaction
    DB:= TIBDatabase.Create(nil);
    try
      SetDBParams(DB, aDBId, DBProps);
      if not DB.Connected then
        IBReplicatorError(Format(sNoTargetDatabase, [aSchemaId, aDBID]), 1);
      SafeStartTransaction(DB.DefaultTransaction);
      try
        Q1:= TIBSQL.Create(nil);
        try
          Q1.Database:= fConfigDatabase;
          Q1.Transaction:= fConfigDatabaseTransaction;

          Q1.SQL.Clear;
          Q1.SQL.Add('SELECT DISTINCT R.GROUPID,R.RELATIONID,R.RELATIONNAME');
          Q1.SQL.Add('FROM '+FormatIdentifier(Q1.Database.SQLDialect, fConfigDatabasePrefix+'RELATIONS')+' R');
          Q1.SQL.Add(Format('WHERE R.SCHEMAID=%d AND (R.GROUPID=%d OR %d=0)', [aSchemaId, aGroupId, aGroupId]));
          Q1.SQL.Add('ORDER BY R.GROUPID,R.RELATIONID');
          SafeExecQuery(Q1);
          while not Q1.EOF do
          begin
            DropTg('INSERT', Q1.FieldByName('GROUPID').asInteger, Q1.FieldByName('RELATIONID').asInteger, Q1.FieldByName('RELATIONNAME').asString);
            DropTg('UPDATE', Q1.FieldByName('GROUPID').asInteger, Q1.FieldByName('RELATIONID').asInteger, Q1.FieldByName('RELATIONNAME').asString);
            DropTg('DELETE', Q1.FieldByName('GROUPID').asInteger, Q1.FieldByName('RELATIONID').asInteger, Q1.FieldByName('RELATIONNAME').asString);
            SafeNext(Q1);
          end;
          SafeClose(Q1);
          // delete remaining triggers in repl$config (fex. deactivated directly in database)
          if ObjectExists(DB, ibobjRelation, DBProps.ObjPrefix+'CONFIG') then
          begin
            Q1.Database:= DB;

            Q1.Transaction:= DB.DefaultTransaction;
            Q1.SQL.Clear;
            Q1.SQL.Add(Format('SELECT * FROM %s WHERE SCHEMAID=%d AND (GROUPID=%d OR %d=0)', [FormatIdentifier(Q1.Database.SQLDialect, DBProps.ObjPrefix+'CONFIG'), aSchemaId, aGroupId, aGroupId]));
            SafeExecQuery(Q1);
            while not Q1.EOF do
            begin
              DropTg('INSERT', Q1.FieldByName('GROUPID').asInteger, Q1.FieldByName('RELATIONID').asInteger, Q1.FieldByName('RELATIONNAME').asString);
              DropTg('UPDATE', Q1.FieldByName('GROUPID').asInteger, Q1.FieldByName('RELATIONID').asInteger, Q1.FieldByName('RELATIONNAME').asString);
              DropTg('DELETE', Q1.FieldByName('GROUPID').asInteger, Q1.FieldByName('RELATIONID').asInteger, Q1.FieldByName('RELATIONNAME').asString);
              DBSQLExec(DB, Format('DELETE FROM %s WHERE SCHEMAID=%d AND GROUPID=%d AND RELATIONID=%d', [FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'CONFIG'), Q1.FieldByName('SCHEMAID').asInteger, Q1.FieldByName('GROUPID').asInteger, Q1.FieldByName('RELATIONID').asInteger]));
              SafeNext(Q1);
            end;
            SafeClose(Q1);
          end;
        finally
          Q1.Free;
        end;
        SafeCommit(DB.DefaultTransaction);
      except
        SafeRollback(DB.DefaultTransaction);
        raise;
      end;
      if SourceF then
      begin   // due backaward compatability only
        DropObject(DB, ibobjTrigger, Format('TG_D_'+DBProps.ObjPrefix+'LOG_SEQID_%d', [aSchemaId]));
      end;
      if not aOnlyTriggers then
      begin
        DelT('LOG');
        DelT('MAN');
        DelT('HISTORY');
        DelT('TRANSFER');
        if ObjectExists(DB, ibobjRelation, DBProps.ObjPrefix+'CONFIG') then
          LastF:= DBSQLRecord(DB, Format('SELECT CAST(COUNT(*) AS INTEGER) FROM %s', [FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'CONFIG')])) = 0
        else
          LastF:= True; // we should test if a trigger dependant on REPL$LOG exists, but REPL$CONFIG should always exists so it's not worth doing it
        if LastF then
        begin
          DropObject(DB, ibobjProcedure, DBProps.ObjPrefix+'LOG2');
          DropObject(DB, ibobjProcedure, DBProps.ObjPrefix+'LOG_DELETE');
          DropObject(DB, ibobjProcedure, DBProps.ObjPrefix+'LOG_INSERT');
          DropObject(DB, ibobjProcedure, DBProps.ObjPrefix+'MAN_DELETE');
          DropObject(DB, ibobjProcedure, DBProps.ObjPrefix+'FIELD2');
          DropObject(DB, ibobjProcedure, DBProps.ObjPrefix+'FIELD_INSERT');
          DropObject(DB, ibobjTrigger, 'TG_D_'+DBProps.ObjPrefix+'LOG_FIELD');
          DropObject(DB, ibobjTrigger, 'TG_D_'+DBProps.ObjPrefix+'MAN_FIELD');
          DropObject(DB, ibobjRelation, DBProps.ObjPrefix+'LOG');
          if not ObjectExists(DB, ibobjRelation, DBProps.ObjPrefix+'LOG') then
          // delete only when last remaining scheme
          DropObject(DB, ibobjRelation, DBProps.ObjPrefix+'MAN');
    {      DropObject(DB, ibobjTrigger, 'TG_I_'+DBProps.ObjPrefix+'LOG_SEQID');
          DropObject(DB, ibobjGenerator, DBProps.ObjPrefix+'GEN_SEQID'); }
  //        if SourceF then  // delete even in target table, happened when cloned
          begin
            DropObject(DB, ibobjRelation, DBProps.ObjPrefix+'HISTORY');
          end;
          DropObject(DB, ibobjRelation, DBProps.ObjPrefix+'FIELD', True);
          DropObject(DB, ibobjRelation, DBProps.ObjPrefix+'CONFIG', True);

    {     DropObject(DB, ibobjGenerator, DBProps.ObjPrefix+'GEN_TRANSFERID'); }

          for I:= Low(DLLFunctions) to High(DLLFunctions) do
            DropObject(DB, ibobjFunction, DBProps.ObjPrefix+UpperCase(DLLFunctions[I].Name));

          DropObject(DB, ibobjRelation, DBProps.ObjPrefix+'EXT_FILE');
          DropObject(DB, ibobjRelation, DBProps.ObjPrefix+'ENVIRONMENT');
          DropObject(DB, ibobjRelation, DBProps.ObjPrefix+'SNAPSHOT');
          DropObject(DB, ibobjRelation, DBProps.ObjPrefix+'TRANSFER');
        end;
        V:= DBSQLRecord(fConfigDatabase, Format('SELECT REPLROLE FROM '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'SCHEMADB')+' WHERE SCHEMAID=%d AND GROUPID=%d;', [aSchemaId, aGroupId]));
        if VarIsArray(V) and (V <> Null) and (V <> '') then
          DropObject(DB, ibobjRole, DBProps.ObjPrefix+string(V));
      end;
    finally
      DB.Free;
    end;
    if not InTran then
      SafeCommit(fConfigDatabaseTransaction);
  except
    if not InTran then
      SafeRollback(fConfigDatabaseTransaction);
    raise;
  end;
end;

procedure TIBReplicator.DeleteSourceSystemData;
var
  Q1: TIBSQL;
  DB: TIBDatabase;
  DBProps: TReplDatabaseProperties;
  DBMask, SrcDBId: Integer;
  InTran: Boolean;
  SP: TIBStoredProc;
begin
{$IFDEF REGISTRATION}
  TReg_CheckRegistration(False);
{$ENDIF}
  InTran:= fConfigDatabaseTransaction.InTransaction;
  if not InTran then
    SafeStartTransaction(fConfigDatabaseTransaction);
  try
    DB:= TIBDatabase.Create(nil);
    try
      SrcDBId:= GetSourceDBId(aSchemaId);
      SetDBParams_Repl(DB, aSchemaId, 0, SrcDBId, False, DBProps);
      if not DB.Connected then
        IBReplicatorError(Format(sNoDatabase, [SrcDBId]), 1);
      SafeStartTransaction(DB.DefaultTransaction);
      try
        DBMask:= GetDBMask(aSchemaId, aGroupId, aTgtDBId);
        if aTgtDBId = 0 then
          begin
            DBSQLExec(DB, Format('DELETE FROM %s WHERE SCHEMAID=%d AND GROUPID=%d', [FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'LOG'), aSchemaId, aGroupId]));
            DBSQLExec(DB, Format('DELETE FROM %s WHERE SCHEMAID=%d AND GROUPID=%d', [FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'MAN'), aSchemaId, aGroupId]));
          end
        else
          begin
            SP:= TIBStoredProc.Create(nil);
            try
              SP.Database:= DB;
              SP.Transaction:= DB.DefaultTransaction;
              Q1:= TIBSQL.Create(nil);
              try
                Q1.Database:= DB;
                Q1.Transaction:= DB.DefaultTransaction;
                Q1.SQL.Text:= Format('SELECT * FROM %s WHERE SCHEMAID=%d AND GROUPID=%d', [FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'LOG'), aSchemaId, aGroupId]);
                SafeExecQuery(Q1);
                SP.StoredProcName:= DBProps.ObjPrefix+'LOG_DELETE';
                SP.Prepare;
                while not Q1.EOF do
                begin
                  if (Q1.FieldByName('DBMASK').asInteger and DBMask <> 0) or
                     (Q1.FieldByName('DBMASK_PENDING').asInteger and DBMask <> 0) then
                  begin
                    SP.ParamByName('SCHEMAID').asInteger:= aSchemaId;
                    SP.ParamByName('SEQID').asInteger:= Q1.FieldByName('SEQID').asInteger;
                    SP.ParamByName('DBMASK').asInteger:= DBMask;
                    SP.ParamByName('ACTIONTYPE').asInteger:= 0;
                    SP.ParamByName('DESCRIPTION').Value:= Null;
                    SP.ParamByName('CONFLICT').Value:= Null;
                    SafeExecProc(SP);
                  end;
                  SafeNext(Q1);
                end;
                SafeClose(Q1);
                Q1.SQL.Text:= Format('SELECT * FROM %s WHERE SCHEMAID=%d AND GROUPID=%d', [FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'MAN'), aSchemaId, aGroupId]);
                SafeExecQuery(Q1);
                SP.StoredProcName:= DBProps.ObjPrefix+'MAN_DELETE';
                SP.Prepare;
                while not Q1.EOF do
                begin
                  if Q1.FieldByName('DBMASK').asInteger and DBMask <> 0 then
                  begin
                    SP.ParamByName('SCHEMAID').asInteger:= aSchemaId;
                    SP.ParamByName('SEQID').asInteger:= Q1.FieldByName('SEQID').asInteger;
                    SP.ParamByName('DBMASK').asInteger:= DBMask;
                    SP.ParamByName('ACTIONTYPE').asInteger:= 0;
                    SafeExecProc(SP);
                  end;
                  SafeNext(Q1);
                end;
              finally
                Q1.Free;
              end;
            finally
              SP.Free;
            end;
          end;
        DBSQLExec(DB, Format('DELETE FROM %s WHERE SCHEMAID=%d AND GROUPID=%d AND (DBID=%d OR %d=0)', [FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'TRANSFER'), aSchemaId, aGroupId, aTgtDBId, aTgtDBId]));
        SafeCommit(DB.DefaultTransaction);
      except
        SafeRollback(DB.DefaultTransaction);
        raise;
      end;
    finally
      DB.Free;
    end;
    if not InTran then
      SafeCommit(fConfigDatabaseTransaction);
  except
    if not InTran then
      SafeRollback(fConfigDatabaseTransaction);
    raise;
  end;
end;

procedure TIBReplicator.CreateCustomObjects(DB: TIBDatabase; const aDBProps: TReplDatabaseProperties; aDBID: Integer);
var
  V: Variant;
  Sg: TStrings;
  S, RelS, FldS, ParS: string;
  I: Integer;
  function GetS(var S: string): string;
  var
    I: Integer;
  const
    Delim = '|';
  begin
    I:= Pos(Delim, S);
    if I = 0 then
      I:= Length(S)+1;
    Result:= Trim(Copy(S, 1, I-1));
    Delete(S, 1, I);
  end;
resourcestring
  sBadCustomField = 'Bad custom field definition "%s"';
  sCustomFieldRelation = 'Custom field relation does not exist "%s"';
begin
  V:= DBSQLRecord(fConfigDatabase, Format('SELECT CUSTOMFIELDS FROM %s WHERE DBID=%d;', [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'DATABASES'), aDBId]));
  if VarIsNull(V) then
    Exit;
  Sg:= TStringList.Create;
  try
    Sg.Text:= V;
    for I:= 0 to Sg.Count-1 do
    begin
      S:= Trim(Sg[I]);
      if (S <> '') and not (S[1] in ['#',';','/']) then  // comments
      begin
        RelS:= GetS(S);
        FldS:= GetS(S);
        ParS:= GetS(S);
        if (RelS = '') or (FldS = '') or (ParS = '') then
        begin
          fDBLog.Log(fLogName, lchError, Format(sBadCustomField, [Sg[I]]));
          Continue;
        end;
        RelS:= aDBProps.ObjPrefix+RelS;
        if ObjectExists(DB, ibobjRelation, RelS) then
          begin
            if not RelationFieldExists(DB, RelS, FldS) then
            begin
              DBLogVerbose(Format(sUpgradeAddField, [RelS, FldS]));
              DBSQLExec(DB, Format('ALTER TABLE %s ADD %s %s;', [FormatIdentifier(DB.SQLDialect, RelS), FormatIdentifier(DB.SQLDialect, FldS), ParS]));
            end;
          end
        else
          fDBLog.Log(fLogName, lchError, Format(sCustomFieldRelation, [RelS]));
      end;
    end;
  finally
    Sg.Free;
  end;
end;

procedure TIBReplicator.UpgradeDatabase;
var
  Q1: TIBSQL;
  DB: TIBDatabase;
  TypeS: string;
  DBProps: TReplDatabaseProperties;
  KeyLength: Integer;
  Ver: string;
  Cached: TStringList;
resourcestring
  sUpgradeAdd = 'Add %s "%s"."%s"';
  sUpgradeDrop  = 'Drop %s "%s"."%s"';

  procedure DropTg(aDruh: string; aSchemaId, aGrpId, aRelId: Integer; const aRelName: string);
  var
    V: Variant;
  begin
    DropObject(DB, ibobjTrigger, GetTriggerName(aSchemaId, aGrpId, aRelId, aRelName, aDruh[1], DBProps));
    // old trigger name convention
    V:= DBSQLRecord(DB, Format('SELECT RDB$TRIGGER_NAME FROM RDB$TRIGGERS WHERE RDB$RELATION_NAME=''%s'' AND RDB$TRIGGER_NAME LIKE ''%s%%''',
                               [FormatIdentifierValue(DB.SQLDialect, aRelName),
                                FormatIdentifierValue(DB.SQLDialect, Format('TG_%s_%s', [Copy(aDruh, 1, 1), DBProps.ObjPrefix]))]));
    if not VarIsNull(V) then
      DropObject(DB, ibobjTrigger, string(V));
  end;

  procedure InsNewFKeys(const aTbl: string);
  var
    Q1, Q2, Q3: TIBSQL;
    I, J, N, Sep: Integer;
    S, FKey: string;
    V: Variant;
  begin
    if RelationFieldExists(DB, DBProps.ObjPrefix+aTbl, 'NEWFKEY') then
    begin
      Q1:= TIBSQL.Create(nil);
      try
        Q1.Database:= DB;
        Q1.SQL.Text:= 'SELECT * FROM '+FormatIdentifier(Q1.Database.SQLDialect, DBProps.ObjPrefix+aTbl);
        Q2:= TIBSQL.Create(nil);
        try
          Q2.Database:= Q1.Database;
          // cannot prepare IBSQL containing external pass-through function */
          Q2.SQL.Text:= Format('INSERT INTO %s(SCHEMAID,SEQID,MODE,FIELDID,DATATYPE,DATA) VALUES(:SchemaId,:SeqId,''N'',:FieldId,''S'',/*%sSTRING_TO_BLOB*/(:Data))', [FormatIdentifier(Q1.Database.SQLDialect, DBProps.ObjPrefix+'FIELD'), DBProps.ObjPrefix]);
//          Q2.SQL.Text:= Format('INSERT INTO %s(SEQID,MODE,FIELDID,DATATYPE,DATA) VALUES(:SeqId,''N'',:FieldId,''S'',%sSTRING_TO_BLOB(:Data))', [FormatIdentifier(Q1.Database.SQLDialect, DBProps.ObjPrefix+'FIELD'), DBProps.ObjPrefix]);
          // delete triggers to drop NEWFKEY dependency
          Q3:= TIBSQL.Create(nil);
          try
            Q3.Database:= fConfigDatabase;
            Q3.Transaction:= fConfigDatabaseTransaction;
            Q3.SQL.Text:= 'SELECT FIELDID FROM '+FormatIdentifier(Q3.Database.SQLDialect, fConfigDatabasePrefix+'FIELDS')+' WHERE SCHEMAID=:SchemaId AND GROUPID=:GroupId AND RELATIONID=:RelationId AND FIELDTYPE=2 ORDER BY FIELDID';
            SafeExecQuery(Q1);
            try
              while not Q1.EOF do
              begin
                FKey:= Q1.FieldByName('NEWFKEY').asString;
                if FKey <> '' then
                begin
                  S:= Format('%d_%d_%d_', [Q1.FieldByName('SCHEMAID').asInteger, Q1.FieldByName('GROUPID').asInteger, Q1.FieldByName('RELATIONID').asInteger]);
                  N:= StrToIntDef(Cached.Values[S+'Count'], -1);
                  if N < 0 then
                  begin
                    N:= 0;
                    Q3.Params.ByName('SchemaId').asInteger:= Q1.FieldByName('SCHEMAID').asInteger;
                    Q3.Params.ByName('GroupId').asInteger:= Q1.FieldByName('GROUPID').asInteger;
                    Q3.Params.ByName('RelationId').asInteger:= Q1.FieldByName('RELATIONID').asInteger;
                    SafeExecQuery(Q3);
                    try
                      while not Q3.EOF do
                      begin
                        Cached.Add(S+IntToStr(N)+'='+Q3.FieldByName('FIELDID').asString);
                        SafeNext(Q3);
                        Inc(N);
                      end;
                    finally
                      SafeClose(Q3);
                    end;
                    Cached.Add(S+'Count='+IntToStr(N));
                    V:= DBSQLRecord(DB, Format('SELECT SEPARATOR FROM '+FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'CONFIG')+' WHERE SCHEMAID=%d AND GROUPID=%d AND RELATIONID=%d', [Q1.FieldByName('SCHEMAID').asInteger, Q1.FieldByName('GROUPID').asInteger, Q1.FieldByName('RELATIONID').asInteger]));
                    if not VarIsNull(V) then
                      Cached.Add(S+'Sep='+IntToStr(Ord(string(V)[1])));
                  end;

                  Sep:= StrToIntDef(Cached.Values[S+'Sep'], -1);
                  if Sep >= 0 then
                  begin
                    for I:= 0 to N-1 do
                    begin
                      Q2.Params.ByName('SchemaId').asInteger:= Q1.FieldByName('SCHEMAID').asInteger;
                      Q2.Params.ByName('SeqId').asInteger:= Q1.FieldByName('SEQID').asInteger;
                      Q2.Params.ByName('FieldId').asInteger:= StrToInt(Cached.Values[S+IntToStr(I)]);
                      J:= Pos(Chr(Sep), FKey);
                      if J = 0 then
                        J:= Length(FKey)+1;
                      if J > 1 then  // if empty do not insert
                      begin
                        Q2.Params.ByName('Data').asString:= Copy(FKey, 1, J-1);
                        SafeExecQuery(Q2);
                      end;
                      Delete(FKey, 1, J);
                      if FKey = '' then
                        Break;
                    end;
                    DBLogVerbose(Format(sUpgradeUpdateField, [DBProps.ObjPrefix+aTbl, 'NEWFKEY']));
                    DBSQLExec(DB, Format('UPDATE %s SET NEWFKEY=NULL WHERE SEQID=%d;', [FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+aTbl), Q1.FieldByName('SEQID').asInteger]), Q1.Transaction);
                    SafeCommit(Q1.Transaction, True);
                  end;
                end;
                SafeNext(Q1);
              end;
            finally
              SafeClose(Q1);
            end;
          finally
            Q3.Free;
          end;
        finally
          Q2.Free;
        end;
      finally
        Q1.Free;
      end;
      DBLogVerbose(Format(sUpgradeDropField, [DBProps.ObjPrefix+aTbl, 'NEWFKEY']));
      DBSQLExec(DB, Format('ALTER TABLE %s DROP NEWFKEY;', [FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+aTbl)]));
    end;
  end;

  procedure UpdLog(const aTbl: string);
  begin
    if ObjectExists(DB, ibobjRelation, DBProps.ObjPrefix+aTbl) then
    begin
      if not RelationFieldExists(DB, DBProps.ObjPrefix+aTbl, 'DBMASK_PENDING') then
      begin
        DBLogVerbose(Format(sUpgradeAddField, [DBProps.ObjPrefix+aTbl, 'DBMASK_PENDING']));
        DBSQLExec(DB, Format('ALTER TABLE %s ADD DBMASK_PENDING INTEGER DEFAULT 0 NOT NULL;', [FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+aTbl)]));
      end;
      if not RelationFieldExists(DB, DBProps.ObjPrefix+aTbl, 'STAMP') then
      begin
        DBLogVerbose(Format(sUpgradeAddField, [DBProps.ObjPrefix+aTbl, 'STAMP']));
        DBSQLExec(DB, Format('ALTER TABLE %s ADD STAMP TIMESTAMP DEFAULT ''NOW'' NOT NULL;', [FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+aTbl)]));
      end;
    end;
    Q1:= TIBSQL.Create(nil);
    try
      Q1.Database:= DB;
      Q1.Transaction:= DB.DefaultTransaction;
      Q1.SQL.Text:= Format('SELECT RDB$CONSTRAINT_NAME FROM RDB$RELATION_CONSTRAINTS WHERE RDB$RELATION_NAME=''%s'' AND RDB$CONSTRAINT_TYPE=''UNIQUE''', [FormatIdentifierValue(DB.SQLDialect, DBProps.ObjPrefix+aTbl)]);
      SafeExecQuery(Q1);
      while not Q1.EOF do
      begin
        DBLogVerbose(Format(sUpgradeDrop, ['CONSTRAINT', DBProps.ObjPrefix+aTbl, Trim(Q1.Fields[0].asString)]));
        DBSQLExec(DB, Format('ALTER TABLE %s DROP CONSTRAINT %s', [FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+aTbl), Trim(Q1.Fields[0].asString)]));
        SafeNext(Q1);
      end;
    finally
      Q1.Free;
    end;
  end;

  procedure DropPKField(const aTbl, aFld, aPKey: string);
  var
    V: Variant;
  begin
    if ObjectExists(DB, ibobjRelation, DBProps.ObjPrefix+aTbl) then
    begin
      if RelationFieldExists(DB, DBProps.ObjPrefix+aTbl, aFld) then
      begin
        V:= DBSQLRecord(DB, Format('SELECT RDB$CONSTRAINT_NAME FROM RDB$RELATION_CONSTRAINTS WHERE RDB$RELATION_NAME=''%s'' AND RDB$CONSTRAINT_TYPE=''PRIMARY KEY''', [FormatIdentifierValue(DB.SQLDialect, DBProps.ObjPrefix+aTbl)]));
        if not VarIsNull(V) then
        begin
          DBLogVerbose(Format(sUpgradeDrop, ['CONSTRAINT', DBProps.ObjPrefix+aTbl, Trim(string(V))]));
          DBSQLExec(DB, Format('ALTER TABLE %s DROP CONSTRAINT %s', [FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+aTbl), Trim(string(V))]));
          DBLogVerbose(Format(sUpgradeDropField, [DBProps.ObjPrefix+aTbl, aFld]));
          DBSQLExec(DB, Format('ALTER TABLE %s DROP %s', [FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+aTbl), aFld]));
          DBLogVerbose(Format(sUpgradeAdd, ['PRIMARY KEY', DBProps.ObjPrefix+aTbl, aPKey]));
          DBSQLExec(DB, Format('ALTER TABLE %s ADD PRIMARY KEY(%s)', [FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+aTbl), aPKey]));
          SafeCommit(DB.DefaultTransaction, True);
        end;
      end;
    end;
  end;
var
  Id, SubT, Scale: Integer;
resourcestring
  sUpgradeTargetDatabase = 'Cannot finish upgrading database #%d. Exec CreateSystemObjects procedure to do it';
  sUpgradeSet = 'SET %s "%s"';
begin
{$IFDEF REGISTRATION}
  TReg_CheckRegistration(False);
{$ENDIF}
  SafeStartTransaction(fConfigDatabaseTransaction);
  try
    if GetDatabaseType(aDBId) <> dbtInterbase then
      Abort; // to close transaction
    DB:= TIBDatabase.Create(nil);
    try
      SetDBParams(DB, aDBId, DBProps);
      if not DB.Connected then
        IBReplicatorError(Format(sNoDatabase, [aDBID]), 1);
      SafeStartTransaction(DB.DefaultTransaction);
      try
        Ver:= DBEnvironmentReadValue(DB, DBProps.ObjPrefix, VersionEnvironmentName, '');
      // 1.x -> 2.0
//        if Ver < '2.02' then   // universal procedure
        begin
          if ObjectExists(DB, ibobjRelation, DBProps.ObjPrefix+'CONFIG') then
          begin
            if not RelationFieldExists(DB, DBProps.ObjPrefix+'CONFIG', 'DISABLED') then
            begin
              DBLogVerbose(Format(sUpgradeAddField, [DBProps.ObjPrefix+'CONFIG', 'DISABLED']));
              DBSQLExec(DB, Format('ALTER TABLE %s ADD DISABLED CHAR(1) NOT NULL;', [FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'CONFIG')]));
            end;
            if not RelationFieldExists(DB, DBProps.ObjPrefix+'CONFIG', 'SRCDBID') then
            begin
              DBLogVerbose(Format(sUpgradeAddField, [DBProps.ObjPrefix+'CONFIG', 'SRCDBID']));
              DBSQLExec(DB, Format('ALTER TABLE %s ADD SRCDBID INTEGER NOT NULL;', [FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'CONFIG')]));
            end;
            DBLogVerbose(Format(sUpgradeUpdateField, [DBProps.ObjPrefix+'CONFIG', 'DISABLED']));
            DBSQLExec(DB, Format('UPDATE %s SET DISABLED=''N'' WHERE DISABLED IS NULL;', [FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'CONFIG')]));
            DBLogVerbose(Format(sUpgradeUpdateField, [DBProps.ObjPrefix+'CONFIG', 'SRCDBID']));
            DBSQLExec(DB, Format('UPDATE %s SET SRCDBID=%d WHERE SRCDBID IS NULL;', [FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'CONFIG'), aDBId]));
            if not RelationFieldExists(DB, DBProps.ObjPrefix+'CONFIG', 'REPLUSER') then
            begin
              DBLogVerbose(Format(sUpgradeAddField, [DBProps.ObjPrefix+'CONFIG', 'REPLUSER']));
              DBSQLExec(DB, Format('ALTER TABLE %s ADD REPLUSER VARCHAR(31) NOT NULL;', [FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'CONFIG')]));
            end;
            DBLogVerbose(Format(sUpgradeUpdateField, [DBProps.ObjPrefix+'CONFIG', 'REPLUSER']));
            DBSQLExec(DB, Format('UPDATE %s SET REPLUSER='''' WHERE REPLUSER IS NULL;', [FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'CONFIG')]));  // dummy but it's not null field
          end;
          if ObjectExists(DB, ibobjRelation, DBProps.ObjPrefix+'TRANSFER') then
          begin
            if not RelationFieldExists(DB, DBProps.ObjPrefix+'TRANSFER', 'RESEND_FLAG') then
            begin
              DBLogVerbose(Format(sUpgradeAddField, [DBProps.ObjPrefix+'TRANSFER', 'RESEND_FLAG']));
              DBSQLExec(DB, Format('ALTER TABLE %s ADD RESEND_FLAG CHAR(1) DEFAULT ''N'' NOT NULL', [FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'TRANSFER')]));
            end;
          end;

          SafeCommit(DB.DefaultTransaction, True);
          UpdLog('LOG');
          UpdLog('MAN');
          SafeCommit(DB.DefaultTransaction, True);
          DropObject(DB, ibobjProcedure, DBProps.ObjPrefix+'LOG2');
          DropObject(DB, ibobjProcedure, DBProps.ObjPrefix+'FIELD2');  // recreate it, bug in < 2.0.9
          DropObject(DB, ibobjProcedure, DBProps.ObjPrefix+'LOG_DELETE'); // recreate it, bugs in < 2.0.8/9

          DropObject(DB, ibobjTrigger, 'TG_I_'+DBProps.ObjPrefix+'LOG_SEQID');
          if RelationFieldExists(DB, DBProps.ObjPrefix+'LOG', 'NEWFKEY') then
          begin
            Q1:= TIBSQL.Create(nil);
            try
              Q1.Database:= DB;
              Q1.SQL.Text:= 'SELECT * FROM '+FormatIdentifier(Q1.Database.SQLDialect, DBProps.ObjPrefix+'CONFIG');
              SafeExecQuery(Q1);
              // delete triggers to drop NEWFKEY dependency
              try
                while not Q1.EOF do
                begin
                  DropObject(DB, ibobjTrigger, Format('TG_D_'+DBProps.ObjPrefix+'LOG_SEQID_%d', [Q1.FieldByName('SCHEMAID').asInteger]));
                  DropTg('INSERT', Q1.FieldByName('SCHEMAID').asInteger, Q1.FieldByName('GROUPID').asInteger, Q1.FieldByName('RELATIONID').asInteger, Q1.FieldByName('RELATIONNAME').asString);
                  DropTg('UPDATE', Q1.FieldByName('SCHEMAID').asInteger, Q1.FieldByName('GROUPID').asInteger, Q1.FieldByName('RELATIONID').asInteger, Q1.FieldByName('RELATIONNAME').asString);
                  DropTg('DELETE', Q1.FieldByName('SCHEMAID').asInteger, Q1.FieldByName('GROUPID').asInteger, Q1.FieldByName('RELATIONID').asInteger, Q1.FieldByName('RELATIONNAME').asString);
                  SafeNext(Q1);
                end;
              finally
                SafeClose(Q1);
              end;
            finally
              Q1.Free;
            end;
          end;
          SafeCommit(DB.DefaultTransaction, True);

          if ObjectExists(DB, ibobjRelation, DBProps.ObjPrefix+'MAN') then
          begin
            if not RelationFieldExists(DB, DBProps.ObjPrefix+'MAN', 'DESCRIPTION') then
            begin
              DBLogVerbose(Format(sUpgradeAddField, [DBProps.ObjPrefix+'MAN', 'DESCRIPTION']));
              DBSQLExec(DB, Format('ALTER TABLE %s ADD DESCRIPTION BLOB SUB_TYPE TEXT SEGMENT SIZE 80;', [FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'MAN')]));
            end;
            if not RelationFieldExists(DB, DBProps.ObjPrefix+'MAN', 'CONFLICT') then
            begin
              DBLogVerbose(Format(sUpgradeAddField, [DBProps.ObjPrefix+'MAN', 'CONFLICT']));
              DBSQLExec(DB, Format('ALTER TABLE %s ADD CONFLICT BLOB;', [FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'MAN')]));
            end;
            if IndexExists(DB, 'I_'+DBProps.ObjPrefix+'MAN_SEQID') then
            begin
              DBLogVerbose(Format(sUpgradeDrop, ['INDEX', DBProps.ObjPrefix+'MAN', 'I_'+DBProps.ObjPrefix+'MAN_SEQID']));
              DBSQLExec(DB, Format('DROP INDEX %s;', [FormatIdentifier(DB.SQLDialect, 'I_'+DBProps.ObjPrefix+'MAN_SEQID')]));
            end;
          end;
          DropPKField('CONFIG', 'DBID', 'SCHEMAID,GROUPID,RELATIONID');
          DropPKField('LOG', 'DBID', 'SCHEMAID,GROUPID,SEQID');
          DropPKField('MAN', 'DBID', 'SCHEMAID,GROUPID,SEQID');
          DropObject(DB, ibobjTrigger, 'TG_D_'+DBProps.ObjPrefix+'LOG_FIELD');
          DropObject(DB, ibobjTrigger, 'TG_D_'+DBProps.ObjPrefix+'MAN_FIELD');

          if ObjectExists(DB, ibobjRelation, DBProps.ObjPrefix+'CONFIG') then
          begin
            SafeCommit(DB.DefaultTransaction, True);
            Q1:= TIBSQL.Create(nil);
            try
              Q1.Database:= DB;
              Q1.SQL.Text:= 'SELECT DISTINCT SCHEMAID,GROUPID,SRCDBID FROM '+FormatIdentifier(Q1.Database.SQLDialect, DBProps.ObjPrefix+'CONFIG');
              SafeExecQuery(Q1);
              // create REPL$FIELDS and recreate triggers, external functions etc.
              try
                GetRelationFieldType(DB, DBProps.ObjPrefix+'LOG', 'NEWPKEY', TypeS, KeyLength, Scale, SubT);
                while not Q1.EOF do
                begin
                  if (Q1.FieldIndex['SRCDBID']>=0) and (Q1.FieldByName('SRCDBID').asInteger <> aDBId) and (Q1.FieldByName('SRCDBID').asInteger <> 0) then
                    CreateSystemObjects(Q1.FieldByName('SCHEMAID').asInteger, KeyLength, Q1.FieldByName('GROUPID').asInteger, aDBId)
                  else
                    CreateSystemObjects(Q1.FieldByName('SCHEMAID').asInteger, KeyLength, 0, 0);
                  SafeNext(Q1);
                end;
              finally
                SafeClose(Q1);
              end;
            finally
              Q1.Free;
            end;
          end;
          Id:= GenerateId(DB, 'GEN_'+DBProps.ObjPrefix+'SEQID', 0);
          if Id > GenerateId(DB, DBProps.ObjPrefix+'GEN_SEQID', 0) then
          begin
            DBLogVerbose(Format(sUpgradeSet, ['GENERATOR', DBProps.ObjPrefix+'GEN_SEQID']));
            DBSQLExec(DB, Format('SET GENERATOR %s TO %d', [FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'GEN_SEQID'), Id]));
          end;
          CreateObject(DB, ibobjRelation, DBProps.ObjPrefix+'ENVIRONMENT', Format(CreateEnvironmentTable, [GetMaxVARCHARlength(fConfigDatabase, fConfigDatabasePrefix, 255, 1024)]));  // target database has no REPL$CONFIG table inside
          SafeCommit(DB.DefaultTransaction, True);

          DBEnvironmentWriteValue(DB, DBProps.ObjPrefix, VersionEnvironmentName, '');
          Cached:= TStringList.Create;
          try
            Cached.Sorted:= True;
            InsNewFKeys('LOG');
            InsNewFKeys('MAN');
          finally
            Cached.Free;
          end;
          SafeCommit(DB.DefaultTransaction, True);
          if not ObjectExists(DB, ibobjRelation, DBProps.ObjPrefix+'LOG') or not ObjectExists(DB, ibobjRelation, DBProps.ObjPrefix+'MAN') or not ObjectExists(DB, ibobjRelation, DBProps.ObjPrefix+'FIELD') then
            IBReplicatorError(Format(sUpgradeTargetDatabase, [aDBID]), 1);

          DBEnvironmentWriteValue(DB, DBProps.ObjPrefix, VersionEnvironmentName, CurVersion);
        end;
        CreateCustomObjects(DB, DBProps, aDBId);
        SafeCommit(DB.DefaultTransaction);
      except
        SafeRollback(DB.DefaultTransaction);
        raise;
      end;
    finally
      DB.Free;
    end;
    SafeCommit(fConfigDatabaseTransaction);
  except
    SafeRollback(fConfigDatabaseTransaction);
    raise;
  end;
end;

function TIBReplicator.DBUserExists(aDBServ: TIBSecurityService; const aName: string): Boolean;
var
  I: Integer;
  lUserInfo: TUserInfo;
  LastS: string;
begin
  Result:= False;
  with aDBServ do
  begin
    DisplayUsers;
    while (IsServiceRunning) and (not ForceStop) do
      ProcessMessages;
    I:= 0; LastS:= '';
    repeat
      lUserInfo := UserInfo[I];
      if (lUserInfo = nil) or (lUserInfo.UserName = LastS) or (lUserInfo.UserName = '') then  // from IBConsole sources
        Break;
      LastS:= lUserInfo.UserName;
      Result:= SameText(LastS, aName);
      Inc(I);
    until Result;
  end;
end;

procedure TIBReplicator.CreateServerObjects(aSchemaId: Integer; aGroupId{master database}: Integer; aDBId: Integer);
var
  lSecurityService: TIBSecurityService;
  SourceF: Boolean;
  ReplUserS, Dummy: string;
  V: Variant;
  InTran: Boolean;
begin
  InTran:= fConfigDatabaseTransaction.InTransaction;
  if not InTran then
    SafeStartTransaction(fConfigDatabaseTransaction);
  try
    SourceF:= aGroupId = 0;
    if SourceF then
      aDBId:= GetSourceDBId(aSchemaId);
    lSecurityService:= TIBSecurityService.Create(nil);
    try
      SetDBServiceParams(lSecurityService, aDBId, Dummy);
      BuildImplicitEnvironment(['SCHEMAID', 'GROUPID'], [IntToStr(aSchemaId), IntToStr(aGroupId)], False);
      with lSecurityService do
      begin
        if Active then
        begin
          if SourceF then
            begin
              V:= DBSQLRecord(fConfigDatabase, Format('SELECT REPLUSER,REPLROLE,REPLPSW FROM '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'SCHEMADB')+' WHERE SCHEMAID=%d AND GROUPID=0 AND DBID=%d', [aSchemaId, aDBID]));
            end
          else
            begin
              V:= DBSQLRecord(fConfigDatabase, Format('SELECT REPLUSER,REPLROLE,REPLPSW FROM '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'SCHEMADB')+' WHERE SCHEMAID=%d AND GROUPID=%d AND DBID=%d', [aSchemaId, aGroupId, aDBID]));
            end;
          if not VarIsArray(V) or (V[0]=Null) or (TrimRight(V[0]) = '') then
            IBReplicatorError(Format(sNoReplUser, [aSchemaId, aGroupId, aDBID]), 1);
          ReplUserS:= UpperCase(ParseStr(V[0]));

          if not VarIsNull(V[2]) then
            V[2]:= ScramblePassword(ParseStr(ScramblePassword(V[2], False)), False)
          else
            V[2]:= '';
          if ReplUserS = 'SYSDBA' then
            Exit;
          if not DBUserExists(lSecurityService, ReplUserS) then
            begin
              DBLogVerbose(Format(sCreatingObject, ['USER', ReplUserS]));
              UserName:= ReplUserS;
              Password:= V[2];
              LastName:= 'Replicator';

              AddUser();   // try adding user to security database
            end
          else    // update password
            begin
              UserName:= ReplUserS;
              Password:= V[2];
              ModifyUser();
            end;
          while IsServiceRunning and not ForceStop do
            ProcessMessages;
        end;
      end;
    finally
      if lSecurityService.Active then
        lSecurityService.Detach;
      lSecurityService.Free;
    end;
  finally
    if not InTran then
      SafeCommit(fConfigDatabaseTransaction);  // no changes
  end;
end;

procedure TIBReplicator.DropServerObjects(aSchemaId: Integer; aGroupId{master database}: Integer; aDBId: Integer);
var
  lSecurityService: TIBSecurityService;
  SourceF: Boolean;
  ReplUserS, Dummy: string;
  V: Variant;
  InTran: Boolean;
begin
  InTran:= fConfigDatabaseTransaction.InTransaction;
  if not InTran then
    SafeStartTransaction(fConfigDatabaseTransaction);
  try
    SourceF:= aGroupId = 0;
    if SourceF then
      aDBId:= GetSourceDBId(aSchemaId);
    lSecurityService:= TIBSecurityService.Create(nil);
    try
      SetDBServiceParams(lSecurityService, aDBId, Dummy);
      BuildImplicitEnvironment(['SCHEMAID', 'GROUPID'], [IntToStr(aSchemaId), IntToStr(aGroupId)], False);
      with lSecurityService do
      begin
        if Active then
        begin
          if SourceF then
            begin
              V:= DBSQLRecord(fConfigDatabase, Format('SELECT REPLUSER,REPLROLE,REPLPSW FROM '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'SCHEMADB')+' WHERE SCHEMAID=%d AND GROUPID=0 AND DBID=%d', [aSchemaId, aDBID]));
            end
          else
            begin
              V:= DBSQLRecord(fConfigDatabase, Format('SELECT REPLUSER,REPLROLE,REPLPSW FROM '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'SCHEMADB')+' WHERE SCHEMAID=%d AND GROUPID=%d AND DBID=%d', [aSchemaId, aGroupId, aDBID]));
            end;
          if not VarIsArray(V) or (V[0]=Null) or (TrimRight(V[0]) = '') then
            Exit;
          ReplUserS:= UpperCase(ParseStr(V[0]));
          if ReplUserS = 'SYSDBA' then
            Exit;

          if DBUserExists(lSecurityService, ReplUserS) then
          begin
            DBLogVerbose(Format(sDroppingObject, ['USER', ReplUserS]));
            UserName:= ReplUserS;
            DeleteUser();
            while IsServiceRunning and not ForceStop do
              ProcessMessages;
          end;
        end;
      end;
    finally
      if lSecurityService.Active then
        lSecurityService.Detach;
      lSecurityService.Free;
    end;
  finally
    if not InTran then
      SafeCommit(fConfigDatabaseTransaction);  // no changes
  end;
end;

procedure TIBReplicator.CreateSystemObjects(aDBId: Integer; aKeyLength: Integer);
var
  InTran: Boolean;
  Q: TIBSQL;
begin
  InTran:= fConfigDatabaseTransaction.InTransaction;
  if not InTran then
    SafeStartTransaction(fConfigDatabaseTransaction);
  try
    Q:= TIBSQL.Create(nil);
    try
      Q.Database:= fConfigDatabase;
      Q.Transaction:= fConfigDatabaseTransaction;
      Q.SQL.Text:= Format('SELECT * FROM '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'SCHEMADB')+' WHERE DBID=%d AND DISABLED=''N''', [aDBId]);
      SafeExecQuery(Q);
      while not Q.EOF do
      begin
        CreateSystemObjects(Q.FieldByName('SCHEMAID').asInteger, aKeyLength, Q.FieldByName('GROUPID').asInteger, Q.FieldByName('DBID').asInteger);
        SafeNext(Q);
      end;
    finally
      Q.Free;
    end;
  finally
    if not InTran then
      SafeCommit(fConfigDatabaseTransaction);  // no changes
  end;
end;

procedure TIBReplicator.DropSystemObjects(aDBId: Integer; aOnlyTriggers: Boolean);
var
  InTran: Boolean;
  Q: TIBSQL;
  DB: TIBDatabase;
  DBProps: TReplDatabaseProperties;
  Arr: TIntegerOpenArray;
  I: Integer;
begin
  InTran:= fConfigDatabaseTransaction.InTransaction;
  if not InTran then
    SafeStartTransaction(fConfigDatabaseTransaction);
  try
    Q:= TIBSQL.Create(nil);
    try
      Q.Database:= fConfigDatabase;
      Q.Transaction:= fConfigDatabaseTransaction;
      Q.SQL.Text:= Format('SELECT * FROM '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'SCHEMADB')+' WHERE DBID=%d', [aDBId]);
      SafeExecQuery(Q);
      while not Q.EOF do
      begin
        DropSystemObjects(Q.FieldByName('SCHEMAID').asInteger, Q.FieldByName('GROUPID').asInteger, Q.FieldByName('DBID').asInteger, aOnlyTriggers);
        SafeNext(Q);
      end;
    finally
      Q.Free;
    end;
  finally
    if not InTran then
      SafeCommit(fConfigDatabaseTransaction);  // no changes
  end;
  // drop remaining schemes that are not listed in config database (e.g.cloning)
  SetLength(Arr, 0);
  DB:= TIBDatabase.Create(nil);
  try
    SetDBParams(DB, aDBId, DBProps);
    if DB.Connected and ObjectExists(DB, ibobjRelation, DBProps.ObjPrefix+'CONFIG') then
    begin
      SafeStartTransaction(DB.DefaultTransaction);
      try
        Q:= TIBSQL.Create(nil);
        try
          Q.Database:= DB;
          Q.Transaction:= Q.Database.DefaultTransaction;
          Q.SQL.Text:= 'SELECT DISTINCT(SCHEMAID) FROM '+FormatIdentifier(Q.Database.SQLDialect, DBProps.ObjPrefix+'CONFIG');
          SafeExecQuery(Q);
          while not Q.EOF do
          begin
            SetLength(Arr, Length(Arr)+1);
            Arr[Length(Arr)-1]:= Q.Fields[0].asInteger;
            SafeNext(Q);
          end;
        finally
          Q.Free;
        end;
      finally
        SafeCommit(DB.DefaultTransaction);
      end;
    end;
  finally
    DB.Free;
  end;

  for I:= 0 to Length(Arr)-1 do
    DropSystemObjects(Arr[I], 0{source}, aDBId{force source database}, aOnlyTriggers);
end;

procedure TIBReplicator.CreateServerObjects(aDBId: Integer);
var
  InTran: Boolean;
  Q: TIBSQL;
begin
  InTran:= fConfigDatabaseTransaction.InTransaction;
  if not InTran then
    SafeStartTransaction(fConfigDatabaseTransaction);
  try
    Q:= TIBSQL.Create(nil);
    try
      Q.Database:= fConfigDatabase;
      Q.Transaction:= fConfigDatabaseTransaction;
      Q.SQL.Text:= Format('SELECT * FROM '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'SCHEMADB')+' WHERE DBID=%d AND DISABLED=''N''', [aDBId]);
      SafeExecQuery(Q);
      while not Q.EOF do
      begin
        CreateServerObjects(Q.FieldByName('SCHEMAID').asInteger, Q.FieldByName('GROUPID').asInteger, Q.FieldByName('DBID').asInteger);
        SafeNext(Q);
      end;
    finally
      Q.Free;
    end;
  finally
    if not InTran then
      SafeCommit(fConfigDatabaseTransaction);  // no changes
  end;
end;

procedure TIBReplicator.DropServerObjects(aDBId: Integer);
var
  InTran: Boolean;
  Q: TIBSQL;
begin
  InTran:= fConfigDatabaseTransaction.InTransaction;
  if not InTran then
    SafeStartTransaction(fConfigDatabaseTransaction);
  try
    Q:= TIBSQL.Create(nil);
    try
      Q.Database:= fConfigDatabase;
      Q.Transaction:= fConfigDatabaseTransaction;
      Q.SQL.Text:= Format('SELECT * FROM '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'SCHEMADB')+' WHERE DBID=%d', [aDBId]);
      SafeExecQuery(Q);
      while not Q.EOF do
      begin
        DropServerObjects(Q.FieldByName('SCHEMAID').asInteger, Q.FieldByName('GROUPID').asInteger, Q.FieldByName('DBID').asInteger);
        SafeNext(Q);
      end;
    finally
      Q.Free;
    end;
  finally
    if not InTran then
      SafeCommit(fConfigDatabaseTransaction);  // no changes
  end;
end;

procedure TIBReplicator.CreateStoredProcedureTemplates;

  function GetFieldType(DB: TIBDatabase; const aRelName, aFieldName: string; var aTypeS: string): Boolean;
  var
    I, SubT, Scale: Integer;
  const
    sFieldNotFound = 'Field not found "%s"."%s"';
  begin
    if not GetRelationFieldType(DB, aRelName, aFieldName, aTypeS, I, Scale, SubT) then
      IBReplicatorError(Format(sFieldNotFound, [aRelName, aFieldName]), 1);

    Result:= True;
    if aTypeS = 'TEXT' then
      aTypeS:= Format('CHAR(%d)', [I])
    else if aTypeS = 'VARYING' then
      aTypeS:= Format('VARCHAR(%d)', [I])
    else if (aTypeS = 'SHORT') or (aTypeS = 'LONG') or (aTypeS = 'INT64') then
      begin
        if Scale < 0 then
          aTypeS:= 'DOUBLE PRECISION'
        else
          aTypeS:= 'INTEGER';
      end
    else if (aTypeS = 'DOUBLE') or (aTypeS = 'FLOAT') then
      aTypeS:= 'DOUBLE PRECISION'
    else if (aTypeS = 'DATE') or (aTypeS = 'TIME') or (aTypeS = 'TIMESTAMP') then
    else if (aTypeS = 'BLOB') then
    else
      Result:= False;
  end;

var
  S, S2, S3: string;
  DBProps: TReplDatabaseProperties;
  Q, Q2: TIBSQL;
  SrcDB, TgtDB: TIBDatabase;
resourcestring
  sSP_UIDInfo = '/* I..insert,U..update,D..delete */';
  sSP_UnknownTypeField = 'Undeclarable variables:';
  sSP_Comment = 'custom implementation code comes here';
begin
{$IFDEF REGISTRATION}
  TReg_CheckRegistration(False);
{$ENDIF}
  SafeStartTransaction(fConfigDatabaseTransaction);
  try
    if GetSchemaType(aSchemaId) <> schtReplication then
      IBReplicatorError(Format(sSchemaIsNotReplication, [aSchemaId]), 1);

    Q:= TIBSQL.Create(nil);
    try
      Q.Database:= fConfigDatabase;
      Q.Transaction:= fConfigDatabaseTransaction;
      Q.SQL.Text:= Format('SELECT R.RELATIONID,R.RELATIONNAME,R.TARGETNAME FROM %s R WHERE R.SCHEMAID=%d AND R.GROUPID=%d AND R.TARGETTYPE=''P'' AND R.DISABLED=''N'' ORDER BY R.RELATIONID',
                          [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'RELATIONS'), aSchemaId, aGroupId]);
      SafeExecQuery(Q);
      Q2:= TIBSQL.Create(nil);
      try
        Q2.Database:= Q.Database;
        Q2.Transaction:= Q.Transaction;
        Q2.SQL.Text:= Format('SELECT F.FIELDNAME,F.TARGETNAME FROM %s F WHERE F.SCHEMAID=%d AND F.GROUPID=%d AND F.RELATIONID=:RelationId ORDER BY F.FIELDID',
                            [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'FIELDS'), aSchemaId, aGroupId]);

        SrcDB:= TIBDatabase.Create(nil);
        try
          SetDBParams(SrcDB, GetSourceDBId(aSchemaId), DBProps);
          TgtDB:= TIBDatabase.Create(nil);
          try
            SetDBParams(TgtDB, aDBId, DBProps);
            if not TgtDB.Connected then
              IBReplicatorError(Format(sNoTargetDatabase, [aSchemaId, aDBID]), 1);

            while not Q.EOF do
            begin
              if not ObjectExists(TgtDB, ibobjProcedure, Q.FieldByName('TARGETNAME').asString) then
              begin
                Q2.Params.ByName('RelationId').asInteger:= Q.FieldByName('RELATIONID').asInteger;
                SafeExecQuery(Q2);
                try
                  S:= ''; S3:= '';
                  while not Q2.EOF do
                  begin
                    if GetFieldType(SrcDB, Q.FieldByName('RELATIONNAME').asString, Q2.FieldByName('FIELDNAME').asString, S2) then
                      S:= S+','+#13#10'  '+Q2.FieldByName('TARGETNAME').asString+' '+S2
                    else
                      begin
                        if S3 <> '' then
                          S3:= S3+', ';
                        S3:= S3+'  '+Q2.FieldByName('TARGETNAME').asString+' '+S2;
                      end;
                    SafeNext(Q2);
                  end;

                  CreateObject(TgtDB, ibobjProcedure, Q.FieldByName('TARGETNAME').asString,
                    '('+#13#10+
                     '  VAR$OPER CHAR(1)'+sSP_UIDInfo+
                        S+#13#10+
                    ')'+#13#10+
        //            'RETURNS'+#13#10+
        //            '('+#13#10+
        //            ')'+#13#10+
                    'AS'+#13#10+
                    'BEGIN'+#13#10+
                    iif (S3 <> '', '/* ' + sSP_UnknownTypeField+' '+S3+' */'#13#10, '')+
                    '/* '+sSP_Comment+'*/'+#13#10+
                    'EXIT;'+#13#10+
                    'END'
                  );
                finally
                  SafeClose(Q2);
                end;
              end;
              SafeNext(Q);
            end;
          finally
            TgtDB.Free;
          end;
        finally
          SrcDB.Free;
        end;
      finally
        Q2.Free;
      end;
    finally
      Q.Free;
    end;
    SafeCommit(fConfigDatabaseTransaction);
  except
    SafeRollback(fConfigDatabaseTransaction);
    raise;
  end;
end;

function TIBReplicator.GetTriggerName;
begin
  Result:= Format('TG_%s_%s%s_%d_%d_%d', [Copy(aAction, 1, 1), aDBProps.ObjPrefix, Copy(aRelationName, 1, 10), aSchemaId, aGroupId, aRelationId]);
end;

function TIBReplicator.RelationExists(aDB: TIBDatabase; const aRelName: string): Boolean;
begin
  Result:= DBSQLRecord(aDB, Format('SELECT CAST(COUNT(*) AS INTEGER) FROM RDB$RELATIONS RL WHERE RL.RDB$SYSTEM_FLAG=0 AND RL.RDB$RELATION_NAME=''%s'' AND RL.RDB$VIEW_SOURCE IS NULL',
    [FormatIdentifierValue(aDB.SQLDialect, aRelName)])) > 0;
end;

function TIBReplicator.RelationFieldExists;
begin
  Result:= DBSQLRecord(aDB, Format('SELECT CAST(COUNT(*) AS INTEGER) FROM RDB$RELATION_FIELDS WHERE RDB$RELATION_NAME=''%s'' AND RDB$FIELD_NAME=''%s''', [FormatIdentifierValue(aDB.SQLDialect, aRelName), FormatIdentifierValue(aDB.SQLDialect, aFieldName)]))>0;
end;

function TIBReplicator.GenerateId(aDB: TIBDatabase; const aName: string; aInc: Integer): Integer;
var
  V: Variant;
begin
  V:= Null;
  if ObjectExists(aDB, ibobjGenerator, aName) then
    V:= DBSQLRecord(aDB, Format('SELECT CAST(GEN_ID(%s,%d) AS INTEGER) FROM RDB$GENERATORS WHERE RDB$GENERATOR_NAME=''%s''',
                               [FormatIdentifier(aDB.SQLDialect, aName), aInc, FormatIdentifierValue(aDB.SQLDialect, aName)]));
  if VarIsNull(V) then
    Result:= 0
  else
    Result:= V;
end;

procedure TIBReplicator.SetGenerator(aDB: TIBDatabase; const aName: string; const aId: Integer);
begin
  DBSQLExec(aDB, Format('SET GENERATOR %s TO %d', [FormatIdentifier(aDB.SQLDialect, aName), aId]));
end;

function TIBReplicator.IndexExists(aDB: TIBDatabase; const aName: string): Boolean;
begin
  Result:= DBSQLRecord(aDB, Format('SELECT CAST(COUNT(*) AS INTEGER) FROM RDB$INDICES WHERE RDB$INDEX_NAME=''%s''', [FormatIdentifierValue(aDB.SQLDialect, aName)]))>0;
end;

function TIBReplicator.PrimaryKeyExists(aDB: TIBDatabase; const aRelName: string; const aFields: array of string): Boolean;
var
  Q: TIBSQL;
  N, I: Integer;
  F: Boolean;
label
  Found, Ex;
begin
  Result:= False;
  Q:= TIBSQL.Create(nil);
  try
    Q.Database:= aDB;
    if Q.Database = fConfigDatabase then
      Q.Transaction:= fConfigDatabaseTransaction
    else
      Q.Transaction:= aDB.DefaultTransaction;
    Q.SQL.Text:= Format('SELECT DISTINCT ISG.RDB$FIELD_NAME FROM RDB$RELATION_CONSTRAINTS RLC,RDB$INDICES ID,RDB$INDEX_SEGMENTS ISG WHERE RLC.RDB$CONSTRAINT_TYPE=''PRIMARY KEY'''+
                  'AND RLC.RDB$INDEX_NAME=ID.RDB$INDEX_NAME AND ID.RDB$INDEX_NAME=ISG.RDB$INDEX_NAME AND ID.RDB$RELATION_NAME=''%s'''+
                  'ORDER BY RLC.RDB$CONSTRAINT_NAME,ISG.RDB$FIELD_POSITION;', [FormatIdentifierValue(aDB.SQLDialect, aRelName)]);
    F:= Q.Transaction.InTransaction;
    if not F then
      SafeStartTransaction(Q.Transaction);
    try
      SafeExecQuery(Q);
      if Q.Eof then
        goto Ex;
      N:= 0;
      if (Length(aFields) > 0) then
      begin
        while not Q.EOF do
        begin
          for I:= 0 to Length(aFields)-1 do
          begin
            if FormatIdentifierValue(aDB.SQLDialect, Q.Fields[0].AsString) = FormatIdentifierValue(aDB.SQLDialect, aFields[I]) then
              goto Found;
          end;
          goto Ex;
        Found:
          Inc(N);
          Q.Next;
        end;
      end;
      Result:= N = Length(aFields);
    ex:
      if not F then
        SafeCommit(Q.Transaction);
    except
      if not F then
        SafeRollback(Q.Transaction);
      raise;
    end;
  finally
    Q.Free;
  end;
end;

function TIBReplicator.DependentExists(aDB: TIBDatabase; const aName: string): Boolean;
begin
  Result:= DBSQLRecord(aDB, Format('SELECT CAST(COUNT(*) AS INTEGER) FROM RDB$DEPENDENCIES WHERE RDB$DEPENDED_ON_NAME=''%s'' AND NOT (RDB$DEPENDENT_NAME LIKE ''TG@__@_%s@_%%'' ESCAPE ''@'' AND RDB$DEPENDENT_TYPE=2 AND RDB$DEPENDED_ON_TYPE=0)',
                                   [FormatIdentifierValue(aDB.SQLDialect, aName), FormatIdentifierValue(aDB.SQLDialect, aName)]))>0;
end;

const
  ObjTypes: array[TIBObjectType] of string = ('TABLE', 'PROCEDURE', 'TRIGGER', 'EXTERNAL FUNCTION', 'GENERATOR', 'ROLE', 'DOMAIN');
  ObjRelations: array[TIBObjectType] of string = ('RELATION', 'PROCEDURE', 'TRIGGER', 'FUNCTION', 'GENERATOR', 'ROLE', 'FIELD');

function TIBReplicator.ObjectExists(aDB: TIBDatabase; aObj: TIBObjectType; const aName: string): Boolean;
begin
  Result:= DBSQLRecord(aDB, Format('SELECT CAST(COUNT(*) AS INTEGER) FROM RDB$%sS WHERE RDB$%s_NAME=''%s''', [ObjRelations[aObj], ObjRelations[aObj], FormatIdentifierValue(aDB.SQLDialect, aName)]))>0;
end;

function TIBReplicator.CreateObject(aDB: TIBDatabase; aObj: TIBObjectType; const aName, aParams: string): Boolean;
begin
  Result:= not ObjectExists(aDB, aObj, aName);
  if Result then
  begin
    DBLogVerbose(Format(sCreatingObject, [ObjTypes[aObj], aName]));
    DBSQLExec(aDB, iif(aObj = ibobjFunction, 'DECLARE', 'CREATE')+' '+
                   ObjTypes[aObj]+' '+
                   FormatIdentifier(aDB.SQLDialect, aName)+' '+
                   aParams);
  end;
end;

procedure TIBReplicator.DropObject(aDB: TIBDatabase; aObj: TIBObjectType; const aName: string; aOpenClose: Boolean);
resourcestring
  sDepExists = 'An object is depending on "%s"';
begin
  if ObjectExists(aDB, aObj, aName) then
  begin
    DBLogVerbose(Format(sDroppingObject, [ObjTypes[aObj], aName]));
    if aOpenClose then
    begin
      aDB.Close; aDB.Open;  // there is some locking interbase problem sometimes
    end;
    if not DependentExists(aDB, aName) then
      DBSQLExec(aDB, Format('DROP %s %s;', [ObjTypes[aObj], FormatIdentifier(aDB.SQLDialect, aName)]))
    else
      DBLogVerbose(Format(sDepExists, [aName]));
  end;
end;

function TIBReplicator.GetRelationFieldType(aDB: TIBDatabase; const aRelName, aFieldName: string; var aType: string; var aLen, aScale, aSubType: Integer): Boolean;
var
  V: Variant;
begin
  V:= DBSQLRecord(aDB, Format('SELECT T.RDB$TYPE_NAME,F.RDB$FIELD_LENGTH,F.RDB$FIELD_SUB_TYPE,F.RDB$FIELD_SCALE '+
                              'FROM RDB$RELATION_FIELDS RF INNER JOIN RDB$FIELDS F INNER JOIN RDB$TYPES T ON F.RDB$FIELD_TYPE=T.RDB$TYPE AND T.RDB$FIELD_NAME=''RDB$FIELD_TYPE'' ON RF.RDB$FIELD_SOURCE=F.RDB$FIELD_NAME ' +
                              'WHERE RF.RDB$RELATION_NAME=''%s'' AND RF.RDB$FIELD_NAME=''%s''', [aRelName, aFieldName]));
  Result:= VarIsArray(V);
  if Result then
  begin
    aType:= Trim(V[0]);
    aLen:= V[1];
    if VarIsNull(V[2]) then
      aSubType:= -1
    else
      aSubType:= V[2];
    aScale:= V[3];
  end;
end;

function TIBReplicator.GetRelationFieldLength;
var
  S: string;
  SubT, Scale: Integer;
begin
  if not GetRelationFieldType(aDB, aRelName, aFieldName, S, Result, Scale, SubT) then
    Result:= -1;
end;

function TIBReplicator.GetMaxVARCHARlength(aDB: TIBDatabase; const aObjPrefix: string; aMin, aMax: Integer): Integer;
resourcestring
  sMaxVARCHAR = 'Checking max.VARCHAR length [%d..%d]';
  sMaxVARCHAR2 = 'Supported length: %d';
  sMaxVARCHAR3 = 'Supported length does not fit in required range';
  function CheckVARCHAR(aLen: Integer): Boolean;
  var
    SaveLogErrSQLCmds: Boolean;
  begin
    Result:= False;
    try
      SaveLogErrSQLCmds:= fLogErrSQLCmds;
      fLogErrSQLCmds:= False;  // do not log testing SQL commands to error log, does affect other threads but it's used just in manager
      try
        CreateObject(aDB, ibobjDomain, aObjPrefix+'TEST_VARCHAR', Format('AS VARCHAR(%d)', [aLen]));
      finally
        fLogErrSQLCmds:= SaveLogErrSQLCmds;
      end;
      Result:= True;
    except
      on E: Exception do
      begin
        if (E is EIBInterbaseError) and {EIBInterbaseError(E).SQLCode = -204 and }(EIBInterbaseError(E).IBErrorCode = isc_dsql_error) then  // SQL error code = -204 Data type unknown Implementation limit exceeded
        else
          raise;
      end;
    end;
    if Result then
      DropObject(aDB, ibobjDomain, aObjPrefix+'TEST_VARCHAR');
  end;
const
  StandardLen = 32765;
  FB1_3Len = 10000;
begin
// max.VARCHAR length is dependant on value that is configured when SQL server is compiled
  DBLogVerbose(Format(sMaxVARCHAR, [aMin, aMax]));
  DropObject(aDB, ibobjDomain, aObjPrefix+'TEST_VARCHAR');
  Result:= aMax;
  repeat
    if CheckVARCHAR(Result) then
      begin
        if Result >= aMax then
          Break
        else
          if not CheckVARCHAR(Result+1) then
            Break
          else
            aMin:= Result+1;
      end
    else
      begin
        if Result <= aMin then
          IBReplicatorError(sMaxVARCHAR3, 1);
        aMax:= Result-1;
      end;
    if (aMin < StandardLen) and (aMax > StandardLen) then
      Result:= StandardLen  // standard value
    else if (aMin < FB1_3Len) and (aMax > FB1_3Len) then
      Result:= FB1_3Len   // optimization: Firebird 1.0.3 uses 10000
    else
      Result:= (aMin+aMax) div 2;
  until False;

  DBLogVerbose(Format(sMaxVARCHAR2, [Result]));
end;

function TIBReplicator.GetReplTargetDatabase;
var
  V: Variant;
  DBProps2: TReplDatabaseProperties;
begin
  Result:= nil;
  V:= DBSQLRecord(fConfigDatabase, Format('SELECT D.FILENAME,D.DBID,D.NAME,D.OBJPREFIX,D.NAME,D.SQLDIALECT,D.DBTYPE,S.DBMASK,D.EXTFILEPATH FROM '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'DATABASES')+' D INNER JOIN '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'SCHEMADB')+' S ON S.DBID=D.DBID WHERE S.SCHEMAID=%d AND S.GROUPID=%d AND S.%s=%d', [aSchemaId,aGroupId,aFldName,aDBMask]));
  if VarIsArray(V) then
  begin
    BuildImplicitEnvironment(['SCHEMAID','GROUPID','DBID'], [IntToStr(aSchemaId), IntToStr(aGroupId), IntToStr(V[1])]);
    if Integer(V[6]) = dbtInterbase then
      begin { Interbase db }
        Result:= TReplTargetDatabase_IB.Create;
        with TReplTargetDatabase_IB(Result) do
        begin
          Database:= TIBDatabase.Create(nil);
          SetDBParams_Repl(Database, aSchemaId, aGroupId, V[1], False, DBProps2);
          SnapshotQ:= TIBSQL.Create(nil);
          SnapshotQ.Transaction:= Database.DefaultTransaction;
          SnapshotQ.SQL.Text:= Format('UPDATE %s SET SEQID=:SEQID WHERE SCHEMAID=%d AND GROUPID=%d AND DBID=%d AND SEQID<:SEQID2', [FormatIdentifier(Database.SQLDialect, DBProps2.ObjPrefix+'SNAPSHOT'), aSchemaId, aGroupId, Integer(V[1])]);
        end;
      end
    else    { log to file }
      begin
        fDBLog.Log(fLogName, lchNull, Format(sDBConnecting, [string(V[4])]));
        Result:= TReplTargetDatabase_Logger.Create;
        with TReplTargetDatabase_Logger(Result) do
        begin
          Logger:= TFileLogger.Create(nil);
          if V[0]<>Null then
            Logger.LogFile:= ParseStr(V[0]);
          Logger.AutoOpen:= True;
          Logger.LogFlags:= [];
          Logger.MaxLineLength:= 0;
          fSQLDialect:= Max(Min(V[5], 3), 1);
        end;
      end;
    with Result.DBProps do
    begin
      if V[1]<>Null then
        DBId:= V[1];
      if V[2]<>Null then
        DBName:= V[2];
      if V[3]<>Null then
        ObjPrefix:= V[3];
      if V[8]<>Null then
        ExtFilePath:= V[8];
    end;
    if V[7]<>Null then
      Result.DBMask:= V[7];
    Result.fReplicator:= Self;
  end;
end;

function TIBReplicator.ReadCachedFields;
var
  F: Boolean;
  Q1: TIBSQL;
  CSI: TReplCachedSchemaItem;
  CGI: TReplCachedGroupItem;
  CFI: TReplCachedFieldItem;
begin
  CSI:= TReplCachedSchemaItem(CachedSchemes.Find(aSchemaId));
  if CSI = nil then
  begin
    CSI:= TReplCachedSchemaItem.Create;
    CSI.Id:= aSchemaId;
    CachedSchemes.Add(CSI);
  end;
  CGI:= TReplCachedGroupItem(CSI.Groups.Find(aGroupId));
  if CGI = nil then
  begin
    CGI:= TReplCachedGroupItem.Create;
    CGI.Id:= aGroupId;
    CSI.Groups.Add(CGI);
  end;
  CRI:= TReplCachedRelationItem(CGI.Relations.Find(aRelId));
  if CRI = nil then
  begin
    CRI:= TReplCachedRelationItem.Create;
    CRI.Id:= aRelId;
    if aReadFields then
    begin
      Q1:= TIBSQL.Create(nil);
      try
        Q1.Database:= fConfigDatabase;
        Q1.Transaction:= fConfigDatabaseTransaction;
        F:= Q1.Transaction.InTransaction;
        if not F then
          SafeStartTransaction(Q1.Transaction);
        try
          Q1.SQL.Add('SELECT R.RELATIONNAME,R.TARGETNAME AS TARGETTNAME,R.TARGETTYPE,F.FIELDID,F.FIELDNAME,F.TARGETNAME,F.FIELDTYPE,F.TARGETNAME AS TARGETFNAME,F.OPTIONS,R.OPTIONS AS RELOPTIONS FROM '+FormatIdentifier(Q1.Database.SQLDialect, fConfigDatabasePrefix+'RELATIONS')+' R INNER JOIN '+FormatIdentifier(Q1.Database.SQLDialect, fConfigDatabasePrefix+'FIELDS')+' F ON R.SCHEMAID=F.SCHEMAID AND R.GROUPID=F.GROUPID AND R.RELATIONID=F.RELATIONID');
          Q1.SQL.Add(Format('WHERE R.SCHEMAID=%d AND R.RELATIONID=%d AND R.GROUPID=%d ORDER BY F.FIELDID', [aSchemaId, aRelId, aGroupId]));
          SafeExecQuery(Q1);
          if not Q1.EOF then
          begin
            CRI.Name:= Q1.FieldByName('RELATIONNAME').asString;
            CRI.TargetName:= Q1.FieldByName('TARGETTNAME').asString;
            CRI.TType:= (Q1.FieldByName('TARGETTYPE').asString+' ')[1];
            CRI.Options:= Q1.FieldByName('RELOPTIONS').asInteger;
            while not Q1.EOF do
            begin
              CFI:= TReplCachedFieldItem.Create;
              CFI.Id:= Q1.FieldByName('FIELDID').asInteger;
              CFI.Name:= Q1.FieldByName('FIELDNAME').asString;
              CFI.FType:= Q1.FieldByName('FIELDTYPE').asInteger;
              CFI.TargetName:= Q1.FieldByName('TARGETFNAME').asString;
              CFI.Options:= Q1.FieldByName('OPTIONS').asInteger;
              CRI.Fields.Add(CFI);
              SafeNext(Q1);
            end;
          end;
        finally
          if not F then
            SafeRollback(Q1.Transaction, False, False);  { no changes }
        end;
      finally
        Q1.Free;
      end;
    end;
    CGI.Relations.Add(CRI);
  end;

  Result:= CRI.Fields.Count <> 0;
end;

function TIBReplicator.EncodeBLOBMetaSQL(const aTable, aFieldName, aWhere: string; aBinary: Boolean; const aData: string): string;
var
  S: string;
  I: integer;
const
  BinLineLength = 80;
begin
  Result:= '';
  if aData = '' then
    Exit;
  if aBinary then
    S:= prefSQLBLOB_Bin
  else
    S:= prefSQLBLOB_Txt;

  Result:= Format('/* %s%s.%s %s'#13#10, [S, aTable, aFieldName, aWhere]);
  if aBinary then
    begin
      S:= Bin2Hex(aData);
      I:= BinLineLength+1;
      while I < Length(S) do
      begin
        Insert(#13#10, S, I);
        Inc(I, BinLineLength+2);
      end;
    end
  else
    begin
      S:= StringReplace(aData, '\', '\\', [rfReplaceAll]);
      S:= StringReplace(S, '*/', '*\_/', [rfReplaceAll]);
    end;
  Result:= Result+ S +
           '*/'#13#10#13#10;
end;

procedure TIBReplicator.DecodeBLOBMetaSQL(aLog: string; var aTable, aFieldName, aWhere, aData: string);
  function ExtractPar(var S: string): string;
  var
    I: Integer;
  begin
    Result:= '';
    S:= Trim(S);
    while (S<>'') and (S[1] in [' ', '.']) do
      Delete(S, 1, 1);
    if (S <> '') and (S[1] = '"') then
      I:= Pos('"', Copy(S, 2, Length(S))+'"')+1+1
    else
      I:= Min(Pos(' ', S+' '), Pos('.', S+'.'));
    Result:= Copy(S, 1, I-1);
    Delete(S, 1, I);
  end;
  function ExtractEOL(var S: string): string;
  var
    I: Integer;
  begin
    I:= Pos(#13, S+#13);
    Result:= Copy(S, 1, I-1);
    Delete(S, 1, I);
    if (S <> '') and (S[1] = #10) then
      Delete(S, 1, 1);
  end;
var
  BinF: Boolean;
  I: Integer;
begin
  BinF:= Pos(prefSQLBLOB_Bin, aLog) = 1;
  Delete(aLog, 1, Length(prefSQLBLOB_Txt));
  aTable:= ExtractPar(aLog);
  aFieldName:= ExtractPar(aLog);
  aWhere:= Trim(ExtractEOL(aLog));

  if BinF then
    begin
      I:= 1;
      while I <= Length(aLog) do
        if aLog[I] < ' ' then
          Delete(aLog, I, 1)
        else
          Inc(I);
      aData:= Hex2Bin(aLog);
    end
  else
    begin
      aLog:= StringReplace(aLog, '*\_/', '*/', [rfReplaceAll]);
      aData:= StringReplace(aLog, '\\', '\', [rfReplaceAll]);   // note: no CRLF added before */
    end;
end;

function TIBReplicator.LocateFieldQuery(aFieldQ: TIBSQL; aNew: Char; aFieldId: Integer): Boolean;
begin   // FieldQ ordered by FIELDTYPE,FIELDID
  if not aFieldQ.BOF and ((aFieldQ.FieldByName('MODE').asString > aNew) or ((aFieldQ.FieldByName('MODE').asString = aNew) and (aFieldQ.FieldByName('FIELDID').asInteger > aFieldId))) then
  begin
    SafeClose(aFieldQ);
    SafeExecQuery(aFieldQ);  // aFieldQ.First;
  end;
  while not aFieldQ.EOF and ((aFieldQ.FieldByName('MODE').asString < aNew) or (aFieldQ.FieldByName('MODE').asString = aNew) and (aFieldQ.FieldByName('FIELDID').asInteger < aFieldId)) do
  begin
    SafeNext(aFieldQ);
  end;
  Result:= not aFieldQ.EOF and (aFieldQ.FieldByName('MODE').asString = aNew) and (aFieldQ.FieldByName('FIELDID').asInteger = aFieldId);
  if Result then
    aFieldQ.FieldByName('DATA_M').{$IFDEF IBREPL_SQLVAR}SQLVar{$ELSE}asXSQLVar{$ENDIF}.SQLSubType:= 1;  // override param from blob to memo
end;

procedure TIBReplicator.FillLogDeleteDesc(aSP: TIBStoredProc; const aDescription: string; aConflict: TStream);
begin
  if aDescription <> '' then
    aSP.ParamByName('DESCRIPTION').asString:= DateTimemsToStr(Now2)+ ': '+aDescription+#13#10 // blob
  else
    aSP.ParamByName('DESCRIPTION').Value:= Null;
  if (aConflict <> nil) and (aConflict.Size > 0) then
    begin
      aConflict.Position:= 0;
      aSP.ParamByName('CONFLICT').asString:= StreamToString(aConflict)
    end
  else
    aSP.ParamByName('CONFLICT').Value:= Null;
end;

procedure TIBReplicator.ClearCachedValues;
var
  I, J, K, L: Integer;
  CSI: TReplCachedSchemaItem;
  CGI: TReplCachedGroupItem;
  CRI: TReplCachedRelationItem;
begin
  if aOnlyRuntimeValues then
    begin
      for I:= 0 to fCachedSchemes.Count-1 do
      begin
        CSI:= TReplCachedSchemaItem(fCachedSchemes[I]);
        FillChar(CSI.Stats, SizeOf(CSI.Stats), 0);
        for J:= 0 to CSI.Groups.Count-1 do
        begin
          CGI:= TReplCachedGroupItem(CSI.Groups[J]);
          for K:= 0 to CGI.Relations.Count-1 do
          begin
            CGI.DBs[K].Tgt:= nil;
            CGI.DBs[K].Fl:= 0;
            FillChar(CGI.DBs[K].Stats, SizeOf(CGI.DBs[K].Stats), 0);
            CRI:= TReplCachedRelationItem(CGI.Relations[K]);
            CRI.OffRelationName:= '';
            CRI.OffRelDone:= False;
            CRI.StoredProcSQL:= '';
            SetLength(CRI.RemapFields, 0);
            for L:= Low(CRI.RemapKeys) to High(CRI.RemapKeys) do
            begin
              SetLength(CRI.RemapKeys[L].PkgKeyToPkgFieldIdx, 0);
              SetLength(CRI.RemapKeys[L].CfgKeyToPkgKey, 0);
            end;
          end;
        end;
      end;
    end
  else
    fCachedSchemes.Clear;
end;

const
  DeleteStatS = 'UPDATE %s SET S_INSERT=0,S_DELETE=0,S_UPDATE=0,S_MSEC=0,S_ERROR=0,S_CONFLICT=0 WHERE S_KEEP=''N''';
  UpdateStatS = 'UPDATE %s SET %sS_MSEC=S_MSEC+%d WHERE SCHEMAID=%d';

procedure TIBReplicator.ReplicateOnline;
var
  FieldQ, ReplQ, ExtFileQ, SQ: TIBSQL;
  LogDeleteSP: TIBStoredProc;
  SchemaId, GroupId, SeqId, I, J, TgtId, Cnt_Total, LogAct: Integer;
  V: Variant;
  TickCount, TickCount2: Longword;
  StatS, Dummy, LogDesc, S: string;
  CSI: TReplCachedSchemaItem;
  CGI: TReplCachedGroupItem;
  CRI: TReplCachedRelationItem;
  LogConf: TStream;
  ReplLogRec: array[1..3] of TReplLogRecord_Online;
  CurReplLogRec: ^TReplLogRecord_Online;
resourcestring
  sBadParam = 'Bad %s value';
  sReplSchemaTotal = 'SchemaId #%d: LOG: D:%d, U:%d  MAN: I:%d, U:%d';
begin
{$IFDEF REGISTRATION}
  TReg_CheckRegistration(False);
{$ENDIF}
  if aReplOptions and repoptTargetReplication <> 0 then
  begin
    if Length(aTgtDBIds) <> 1 then
      IBReplicatorError(Format(sBadParam, ['TgtDBIds']), 0);
  end;
  ClearCachedValues(True);
  ReplQ:= TIBSQL.Create(nil);
  try
    ReplQ.Database:= aDB;
    ReplQ.Transaction:= TIBTransaction.Create(ReplQ);
    ReplQ.Transaction.Params.Text:= tranRO;
    ReplQ.Transaction.DefaultDatabase:= ReplQ.Database;
//    ReplQ.SQL.Text:= Format('SELECT R.*,S.RELATIONNAME,S.SEPARATOR FROM REPL$LOG R,REPL$CONFIG S WHERE R.DBID=%d AND R.SCHEMAID=S.SCHEMAID AND R.DBID=S.DBID AND R.GROUPID=S.GROUPID AND R.RELATIONID=S.RELATIONID ORDER BY R.DBID,R.GROUPID,R.SEQID', [GetSourceDBId(aSchemaId)]);
    I:= 0; S:= '';
    if Length(aSchemaIds) = 1 then
      begin
        I:= aSchemaIds[0];
        if I = 0 then
          IBReplicatorError(Format(sBadParam, ['SchemaIds']), 0);
      end
    else if Length(aSchemaIds) > 1 then
      S:= IntegerArrayToSQLCondition(aSchemaIds, 'SCHEMAID');
    J:= 0;
    if Length(aGroupIds) = 1 then
      J:= aGroupIds[0]
    else if Length(aGroupIds) > 1 then
      begin
        if S <> '' then
          S:= S+ ' AND ';
        S:= S+IntegerArrayToSQLCondition(aGroupIds, 'GROUPID');
      end;
    if S <> '' then
      S:= ' WHERE '+S;
    ReplQ.SQL.Text:= Format('SELECT * FROM '+FormatIdentifier(ReplQ.Database.SQLDialect, aDBProps.ObjPrefix+'LOG2')+'(%d,%d) %s', [I, J, S]);
    if aReplOptions and repoptTargetReplication <> 0 then
      ReplQ.SQL.Text:= ReplQ.SQL.Text + ' ORDER BY STAMP';

    SafeStartTransaction(ReplQ.Transaction);
    try
      SafeExecQuery(ReplQ);
      FieldQ:= TIBSQL.Create(nil);
      try
        FieldQ.Database:= ReplQ.Database;
        FieldQ.Transaction:= ReplQ.Transaction;
        FieldQ.SQL.Text:= 'SELECT * FROM '+FormatIdentifier(FieldQ.Database.SQLDialect, aDBProps.ObjPrefix+'FIELD2')+'(:SCHEMAID,:SEQID,'''',0)';
        FieldQ.Prepare;
        SQ:= TIBSQL.Create(nil);
        try
          SQ.Database:= FieldQ.Database;
          SQ.Transaction:= FieldQ.Transaction;

          DBSQLExec(fConfigDatabase, Format(DeleteStatS, [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'RELATIONS')])+IntegerArrayToSQLCondition(aSchemaIds, 'SCHEMAID', ' AND '));
          DBSQLExec(fConfigDatabase, Format(DeleteStatS, [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'SCHEMATA')])+IntegerArrayToSQLCondition(aSchemaIds, 'SCHEMAID', ' AND '));

          ExtFileQ:= TIBSQL.Create(nil);
          try
            ExtFileQ.Database:= ReplQ.Database;
            ExtFileQ.Transaction:= ReplQ.Transaction;
            ExtFileQ.SQL.Text:= 'SELECT * FROM '+FormatIdentifier(FieldQ.Database.SQLDialect, aDBProps.ObjPrefix+'EXT_FILE')+' WHERE SCHEMAID=:SCHEMAID AND SEQID=:SEQID';

            LogDeleteSP:= TIBStoredProc.Create(nil);  // need blob parameter
            try
              LogDeleteSP.Database:= ReplQ.Database;
              LogDeleteSP.Transaction:= ReplQ.Database.DefaultTransaction;
              LogDeleteSP.StoredProcName:= aDBProps.ObjPrefix+'LOG_DELETE';
              LogDeleteSP.Prepare;

              Cnt_Total:= 0;
              try
                TickCount2:= GetTickCount();

                DoUpdateStatus(GetTickCount()-TickCount2, Cnt_Total);
                try
                  while not ReplQ.EOF and not ForceStop do
                  begin
                    ProcessMessages;
                    SchemaId:= ReplQ.FieldByName('SCHEMAID').asInteger;
                    SeqId:= ReplQ.FieldByName('SEQID').asInteger;

                    CSI:= TReplCachedSchemaItem(fCachedSchemes.Find(SchemaId));
                    if CSI = nil then
                    begin
                      CSI:= TReplCachedSchemaItem.Create;
                      CSI.Id:= SchemaId;
                      fCachedSchemes.Add(CSI);
                    end;
                    CSI.Stats.Fl:= True;
                    GroupId:= ReplQ.FieldByName('GROUPID').asInteger;
                    CGI:= TReplCachedGroupItem(CSI.Groups.Find(GroupId));
                    if CGI = nil then
                    begin
                      CGI:= TReplCachedGroupItem.Create;
                      CGI.Id:= GroupId;
                      for TgtId:= Low(CGI.DBs) to High(CGI.DBs) do
                      begin
                        V:= DBSQLRecord(fConfigDatabase, Format('SELECT D.DBID FROM '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'DATABASES')+' D INNER JOIN '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'SCHEMADB')+' S ON S.DBID=D.DBID WHERE S.SCHEMAID=%d AND S.GROUPID=%d AND S.DBMASK=%d', [SchemaId,GroupId,1 shl TgtId]));
                        if not VarIsEmpty(V) and not VarIsNull(V) then
                          begin
                            if Length(aTgtDBIds) = 0 then
                              CGI.DBs[TgtId].DBId:= V
                            else
                              for J:= 0 to Length(aTgtDBIds)-1 do
                                if (aTgtDBIds[J] = V) or (aTgtDBIds[J] = 0) then
                                begin
                                  CGI.DBs[TgtId].DBId:= V;
                                  Break;
                                end;
                          end;
                      end;
                      CSI.Groups.Add(CGI);
                    end;

                    for TgtId:= Low(CGI.DBs) to High(CGI.DBs) do
                    begin
                      if (CGI.DBs[TgtId].DBId <> 0) and ((1 shl TgtId) and ReplQ.FieldByName('DBMASK').asInteger <> 0) then
                      begin
                        if ForceStop then
                          Break;
                        try
                          if CGI.DBs[TgtId].Fl = 0 then
                          begin
                            CGI.DBs[TgtId].Tgt:= GetReplTargetDatabase(SchemaId, GroupId, 'DBMASK', 1 shl TgtId);
                            if CGI.DBs[TgtId].Tgt <> nil then
                              CGI.DBs[TgtId].Fl:= 1
                            else
                              CGI.DBs[TgtId].Fl:= 2;
                          end;
                          if CGI.DBs[TgtId].Fl = 1 then  { succesufully connected }
                          begin
                            CurReplLogRec:= @ReplLogRec[CGI.DBs[TgtId].Tgt.SQLDialect];
                            if CurReplLogRec^ = nil then
                              CurReplLogRec^:= TReplLogRecord_Online.Create(Self);
                            if (CurReplLogRec^.SeqId <> SeqId) or
                               (CurReplLogRec^.SchemaId <> SchemaId) then
                              begin
                                CurReplLogRec^.AssignReplQ(ReplQ);   // next replq record
                              end
                            else
                              CurReplLogRec^.IUD:= (ReplQ.FieldByName('REPLTYPE').asString+' ')[1];  // may be mangled from ReplicateOnline

                            with CGI.DBs[TgtId].Stats do
                              case CurReplLogRec^.IUD of
                                'I': Inc(Cnt_I);
                                'U': Inc(Cnt_U);
                                'D': Inc(Cnt_D);
                              end;
                            TickCount:= GetTickCount();
                            StatS:= '';
                            try
                              LogDesc:= '';
                              LogAct:= 0;
                              LogConf:= TMemoryStream.Create;
                              try
                                try
                                  if ReadCachedFields(SchemaId, GroupId, CurReplLogRec^.RelationId, True, CRI) then
                                    begin
                                      if CurReplLogRec^.CRI <> CRI then   // i.e. SchemaId<> or GroupId<> or RelationId <>
                                      begin
                                        CurReplLogRec^.CRI:= CRI;
                                        FreeAndNil(CurReplLogRec^.FieldDefs);  // do not clear p/fk
                                        CurReplLogRec^.FieldDefs:= TReplFieldDefs.Create;
                                        CurReplLogRec^.FieldDefs.BuildFieldDefs(CurReplLogRec^.CRI, SQ.Database.SQLDialect, CGI.DBs[TgtId].Tgt.SQLDialect);
                                      end;
                                      CurReplLogRec^.FieldDefs.GetWhereValues(SQ.Database.SQLDialect, CGI.DBs[TgtId].Tgt.SQLDialect, CurReplLogRec^.IUD, CurReplLogRec^.OldPKey, CurReplLogRec^.NewPKey, CurReplLogRec^.FieldDefs.Where_Src, CurReplLogRec^.FieldDefs.Where_Tgt, CurReplLogRec^.FieldDefs.Where_Tgt_New);
                                      if CurReplLogRec^.FieldDefs.Where_Src = '' then
                                        IBReplicatorError(Format(sNoPrimaryKey, [CurReplLogRec^.FieldDefs.RelationName_Src]), 2);
                                      FieldQ.Params.ByName('SCHEMAID').asInteger:= SchemaId;
                                      FieldQ.Params.ByName('SEQID').asInteger:= SeqId;
                                      SafeExecQuery(FieldQ);
                                      try
                                        CurReplLogRec^.AssignFieldQ(FieldQ);
                                        SQ.SQL.Text:= Format('SELECT * FROM %s WHERE %s', [FormatIdentifier(SQ.Database.SQLDialect, CurReplLogRec^.FieldDefs.RelationName_Src), CurReplLogRec^.FieldDefs.Where_Src]);
                                        if (CurReplLogRec^.IUD <> 'D') or (CurReplLogRec^.FieldDefs.Type_Tgt = 'P') or (aReplOptions and repoptExtConflictCheck <> 0) then
                                          SafeExecQuery(SQ);  // do not open if delete �n table and no potential conflict checking
                                        try
                                          CurReplLogRec^.AssignSQ(SQ);
                                          try
                                            if aReplOptions and repoptTargetReplication <> 0 then
                                              begin
                                                ExtFileQ.Params.ByName('SCHEMAID').asInteger:= SchemaId;
                                                ExtFileQ.Params.ByName('SEQID').asInteger:= SeqId;
                                                SafeExecQuery(ExtFileQ);
                                                CurReplLogRec^.AssignExtFileQ(ExtFileQ);
                                              end
                                            else
                                              CurReplLogRec^.AssignExtFiles(aDBProps);
                                            CGI.DBs[TgtId].Tgt.ReplicateRecord(CurReplLogRec^, aReplOptions, LogConf, CGI.DBs[TgtId].Stats.Cnt_Total);
                                          finally
                                            SafeClose(ExtFileQ);
                                          end;
                                        finally
                                          SafeClose(SQ);
                                        end;
                                      finally
                                        SafeClose(FieldQ);
                                      end;
                                    end;
                                  with CGI.DBs[TgtId].Stats do
                                    case CurReplLogRec^.IUD of
                                      'I':
                                        begin
                                          StatS:= 'S_INSERT';
                                          Inc(Cnt2_I);
                                        end;
                                      'U':
                                        begin
                                          StatS:= 'S_UPDATE';
                                          Inc(Cnt2_U);
                                        end;
                                      'D':
                                        begin
                                          StatS:= 'S_DELETE';
                                          Inc(Cnt2_D);
                                        end;
                                    end;
                                except
                                  on E: Exception do
                                  begin
                                    if E is EIBReplicatorError then  { replication conflict - log to manual log }
                                      begin
                                        if EIBReplicatorError(E).Kind = 1 then
                                          StatS:= 'S_CONFLICT'
                                        else
                                          StatS:= 'S_ERROR';
                                        if (EIBReplicatorError(E).Kind > 0) and (aReplOptions and repoptReportToSource <> 0) then
                                        begin
                                          LogDesc:= E.Message;
                                          LogAct:= 2;
                                        end;
                                        fDBLog.Log(fLogName, lchError, E.Message);
                                      end
                                    else if CheckIBConnectionError(E, TIBDatabase(CGI.DBs[TgtId].Tgt)) then
                                      begin
                                        if ReplQ.Database.Connected then
                                          CGI.DBs[TgtId].Fl:= 2;    // do not replicate if connection has been broken to target database
                                        raise;
                                      end
                                    else
                                      begin
                                        StatS:= 'S_ERROR';
                                        raise;
                                      end;
                                  end;
                                end;

                                LogDeleteSP.ParamByName('SCHEMAID').asInteger:= SchemaId;
                                LogDeleteSP.ParamByName('SEQID').asInteger:= SeqId;
                                LogDeleteSP.ParamByName('DBMASK').asInteger:= 1 shl TgtId;
                                LogDeleteSP.ParamByName('ACTIONTYPE').asInteger:= LogAct;
                                FillLogDeleteDesc(LogDeleteSP, LogDesc, LogConf);
                                SafeExecProc(LogDeleteSP);
                                LogAct:= LogDeleteSP.ParamByName('RESULT').asInteger;
                                SafeCommit(LogDeleteSP.Transaction, True);
                                case LogAct and $0F of
                                  1: Inc(CSI.Stats.Cnt_LogU);
                                  2: Inc(CSI.Stats.Cnt_LogD);
                                end;
                                case (LogAct and $F0) shr 4 of
                                  1: Inc(CSI.Stats.Cnt_ManU);
                                  2: Inc(CSI.Stats.Cnt_ManI);
                                end;
                              finally
                                LogConf.Free;
                              end;
                            finally
                              if StatS <> '' then
                                StatS:= StatS+'='+Stats+'+1,'; // possible integer overflow
                              try
                                DBSQLExec(fConfigDatabase, Format(UpdateStatS+' AND RELATIONID=%d AND GROUPID=%d', [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'RELATIONS'), StatS, Abs(GetTickCount()-TickCount), SchemaId, CurReplLogRec^.RelationId, GroupId]));
                              except
                                fDBLog.Log(fLogName, lchError, Format(sUpdateStatRelErr, [SchemaId, GroupId, CurReplLogRec^.RelationId]));
                              end;
                              try
                                DBSQLExec(fConfigDatabase, Format(UpdateStatS, [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'SCHEMATA'), StatS, Abs(GetTickCount()-TickCount), SchemaId]));
                              except
                                fDBLog.Log(fLogName, lchError, Format(sUpdateStatSchErr, [SchemaId]));
                              end;
                            end;
                          end;
                        except
                          on E: Exception do
                          begin
                            if CheckIBConnectionError(E, aDB) then
                              raise
                            else if E is EIBReplicatorError then
                              fReplLog.Log(fLogName, lchError, E.Message)
                            else
                              fDBLog.Log(fLogName, lchError, Format('%s %s #%d: %s', [ReplQ.FieldByName('RELATIONNAME').asString, ReplQ.FieldByName('REPLTYPE').asString, SeqId, E.Message]));
                          end;
                        end;
                        ProcessMessages;
                      end;
                    end;
                    SafeNext(ReplQ);
                    Inc(Cnt_Total);
                    DoUpdateStatus(GetTickCount()-TickCount2, Cnt_Total);
                  end;
                finally
                  for I:= Low(ReplLogRec) to High(ReplLogRec) do
                    ReplLogRec[I].Free;
                end;
              finally
                for I:= 0 to fCachedSchemes.Count-1 do
                begin
                  CSI:= TReplCachedSchemaItem(fCachedSchemes[I]);
                  if CSI.Stats.Fl then
                  begin
                    fReplLog.Log(fLogName, lchNull, Format(sReplSchemaTotal, [CSI.Id, CSI.Stats.Cnt_LogD, CSI.Stats.Cnt_LogU, CSI.Stats.Cnt_ManI, CSI.Stats.Cnt_ManU]));
                    for J:= 0 to CSI.Groups.Count-1 do
                    begin
                      CGI:= TReplCachedGroupItem(CSI.Groups[J]);
                      for TgtId:= Low(CGI.DBs) to High(CGI.DBs) do
                      begin
                        if CGI.DBs[TgtId].Fl > 0 then
                        begin
                          fReplLog.Log(fLogName, lchNull, Format(sReplCount, [CGI.DBs[TgtId].Tgt.DBProps.DBName, CGI.DBs[TgtId].Stats.Cnt2_I, CGI.DBs[TgtId].Stats.Cnt_I, CGI.DBs[TgtId].Stats.Cnt2_U, CGI.DBs[TgtId].Stats.Cnt_U, CGI.DBs[TgtId].Stats.Cnt2_D, CGI.DBs[TgtId].Stats.Cnt_D]));
                        end;
                        FreeAndNil(CGI.DBs[TgtId].Tgt);
                      end;
                    end;
                  end;
                end;
              end;
            finally
              LogDeleteSP.Free;
            end;
          finally
            ExtFileQ.Free;
          end;
        finally
          SQ.Free;
        end;
      finally
        FieldQ.Free;
      end;
    finally
      SafeCommit(ReplQ.Transaction); { no changes made in this transaction }
    end;
  finally
    ReplQ.Free;
  end;
end;

class function TIBReplicator.DecodeSyncActions(const aActS: string; aResolve: Boolean): TSyncActions;

  function GetNextW(var S: string): string;
  begin
    Result:= '';
    S:= Trim(S);
    while (S<>'') and (S[1] in ['A'..'Z','a'..'z','_','0'..'9']) do
    begin
      Result:= Result+S[1];
      Delete(S, 1, 1);
    end;
  end;

  function IndexOfCond(const S: string; var Res: TSyncActions): Integer;
  var
    J: Integer;
  begin
    Result:= -1;
    for J:= 0 to Length(Res.Cond)-1 do
    begin
      if SameText(Res.Cond[J].Alias, S) then
      begin
        Result:= J;
        Break;
      end;
    end;
  end;
var
  Sg, Sg2: TStrings;
  S1, S2, ActS: string;
  I, J: Integer;
  Fl: Boolean;
begin
  SetLength(Result.Acts, 0);
  SetLength(Result.Cond, 0);
  Sg:= TStringList.Create;
  try
    Sg.Text:= aActS;
    for I:= 0 to Sg.Count-1 do
    begin
      S1:= Trim(Sg[I]);
      J:= 1; Fl:= True;
      while J <= Length(S1) do
      begin
        if S1[J] in ['0'..'9', 'a'..'f','A'..'F'] then
          Inc(J)
        else if S1[J] = ';' then
          Break
        else
          begin
            Fl:= False;
            Break;
          end;
      end;

      if (J > 1) and Fl then               // action[; ....]
        begin
          ActS:= Copy(S1, 1, J-1);
          Delete(S1, 1, J);
          Sg2:= TStringList.Create;
          try
            Sg2.Text:= StringReplace(S1, ';', #13#10, [rfReplaceAll]);
            for J:= 1 to Length(ActS) do
            begin
              SetLength(Result.Acts, Length(Result.Acts)+1);
              with Result.Acts[Length(Result.Acts)-1] do
              begin
                Action:= StrToInt('$'+ActS[J]);
                Where_Src:= Sg2.Values['WHERE:SRC'];
                Where_Tgt:= Sg2.Values['WHERE:TGT'];
              end;
            end;
          finally
            Sg2.Free;
          end;
        end
      else
        begin
          S2:= GetNextW(S1);
          S1:= Trim(S1);
          if (S2 <> '') and (S1 <> '') and (S1[1] = '=') then
          begin
            if IndexOfCond(S2, Result) < 0 then
            begin
              SetLength(Result.Cond, Length(Result.Cond)+1);
              Result.Cond[Length(Result.Cond)-1].Alias:= S2;
              Result.Cond[Length(Result.Cond)-1].Where:= Trim(Copy(S1, 2, Length(S1)));
            end;
          end;
        end;
    end;
  finally
    Sg.Free;
  end;

  if aResolve then
  begin
    for I:= 0 to Length(Result.Acts)-1 do
      with Result.Acts[I] do
      begin
        J:= IndexOfCond(Result.Acts[I].Where_Src, Result);
        if J >= 0 then
          Where_Src:= Result.Cond[J].Where
        else
          Where_Src:= '';
        J:= IndexOfCond(Result.Acts[I].Where_Tgt, Result);
        if J >= 0 then
          Where_Tgt:= Result.Cond[J].Where
        else
          Where_Tgt:= '';
      end;
  end;
end;

class function TIBReplicator.EncodeSyncActions(const aActions: TSyncActions): string;
var
  I, J: Integer;
  S: string;
begin
  I:= 0;
  Result:= '';
  while I < Length(aActions.Acts) do
  begin
    J:= I+1;
    while (J < Length(aActions.Acts)) and (aActions.Acts[J].Where_Src = aActions.Acts[I].Where_Src) and (aActions.Acts[J].Where_Tgt = aActions.Acts[I].Where_Tgt) do
      Inc(J);
    if Result <> '' then
      Result:= Result+#13#10;

    S:= iif(Trim(aActions.Acts[I].Where_Src) <> '', ';WHERE:SRC='+Trim(aActions.Acts[I].Where_Src), '')+
        iif(Trim(aActions.Acts[I].Where_Tgt) <> '', ';WHERE:TGT='+Trim(aActions.Acts[I].Where_Tgt), '');
    while I < J do
    begin
      Result:= Result+Format('%x', [aActions.Acts[I].Action]);
      Inc(I);
    end;
    Result:= Result+S;
  end;
  for I:= 0 to Length(aActions.Cond)-1 do
  begin
    if Result <> '' then
      Result:= Result+#13#10;
    Result:= Result+aActions.Cond[I].Alias+'='+aActions.Cond[I].Where;
  end;
end;

procedure TIBReplicator.Synchronize(aSchemaId: Integer; const aGroupIds, aTgtDBIds: TIntegerOpenArray);
var
  Q, Q2: TIBSQL;
  SrcDB: TIBDatabase;
  TgtDB: TReplTargetDatabase;
  SyncRec: TSyncRecord;
  TotalCnt, Cnt: TSyncRecordCounters;
  ActI, N: Integer;
  EndFl: Boolean;
  CondS: string;
  DBProps: TReplDatabaseProperties;
  Act: TSyncActions;
begin
{$IFDEF REGISTRATION}
  TReg_CheckRegistration(False);
{$ENDIF}
  SyncRec.SchemaId:= aSchemaId;
  if GetSchemaType(SyncRec.SchemaId) <> schtReplication then
    IBReplicatorError(Format(sSchemaIsNotReplication, [SyncRec.SchemaId]), 1);
  SafeStartTransaction(fConfigDatabaseTransaction);
  try
    CondS:= 'S.SCHEMAID='+IntToStr(SyncRec.SchemaId)+
            IntegerArrayToSQLCondition(aGroupIds, 'S.GROUPID', ' AND ');
    SyncRec.SrcDBId:= GetSourceDBId(SyncRec.SchemaId);
    SrcDB:= TIBDatabase.Create(nil);
    try
      SetDBParams_Repl(SrcDB, SyncRec.SchemaId, 0, SyncRec.SrcDBId, False, DBProps);

      DBSQLExec(fConfigDatabase, Format(DeleteStatS, [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'RELATIONS')+' S'])+' AND '+CondS);
      CondS:= CondS+
              IntegerArrayToSQLCondition(aTgtDBIds, 'S.DBID', ' AND ');
      SafeCommit(fConfigDatabaseTransaction, True);
      FillChar(TotalCnt, SizeOf(TotalCnt), 0);
      TotalCnt.StartTick:= GetTickCount();
      N:= 0;
      ClearCachedValues(True);
      Q:= TIBSQL.Create(nil);
      try
        Q.Database:= fConfigDatabase;
        Q.Transaction:= fConfigDatabaseTransaction;
        Q.SQL.Text:= 'SELECT DISTINCT S.GROUPID,S.DBID FROM '+FormatIdentifier(Q.Database.SQLDialect, fConfigDatabasePrefix+'RELATIONS')+' R INNER JOIN '+FormatIdentifier(Q.Database.SQLDialect, fConfigDatabasePrefix+'SCHEMADB')+' S ON R.SCHEMAID=S.SCHEMAID AND R.GROUPID=S.GROUPID WHERE '+CondS+' AND S.GROUPID<>0 ORDER BY S.GROUPID,S.DBID';
        SafeExecQuery(Q);
        while not Q.EOF and not ForceStop do
        begin
          try
            SyncRec.GroupId:= Q.FieldByName('GROUPID').asInteger;
            SyncRec.TgtDBId:= Q.FieldByName('DBID').asInteger;
            FillChar(Cnt, SizeOf(Cnt), 0);
            TgtDB:= GetReplTargetDatabase(SyncRec.SchemaId, SyncRec.GroupId, 'DBID', SyncRec.TgtDBId);
            try

              if TgtDB <> nil then  { succesfully connected }
              begin
                Q2:= TIBSQL.Create(nil);
                try
                  Q2.Database:= fConfigDatabase;
                  Q2.Transaction:= fConfigDatabaseTransaction;
                  Q2.SQL.Text:= Format('SELECT RELATIONID,SYNCACTIONS,RELATIONNAME FROM '+FormatIdentifier(Q2.Database.SQLDialect, fConfigDatabasePrefix+'RELATIONS')+' WHERE SCHEMAID=%d AND GROUPID=%d AND SYNCACTIONS IS NOT NULL ORDER BY SYNCORDER', [SyncRec.SchemaId, SyncRec.GroupId]);
                  ActI:= 0;
                  repeat   // loop positions of SYNCACTIONS
                    EndFl:= True;
                    SafeExecQuery(Q2);
                    try
                      while not Q2.EOF and not ForceStop do
                      begin   // process all records for current position
                        SyncRec.RelationId:= Q2.FieldByName('RELATIONID').asInteger;
                        SyncRec.RelationName:= Q2.FieldByName('RELATIONNAME').asString;
                        BuildImplicitEnvironment(['SCHEMAID','GROUPID','DBID','DB2ID','RELATIONID'],
                                                 [IntToStr(SyncRec.SchemaId), IntToStr(SyncRec.GroupId), IntToStr(SyncRec.SrcDBId), IntToStr(SyncRec.TgtDBId), IntToStr(SyncRec.RelationId)]);
                        Act:= DecodeSyncActions(Q2.FieldByName('SYNCACTIONS').asString, True);
                        try
                          if (ActI < Length(Act.Acts)) then
                          begin
                            if ActI < Length(Act.Acts)-1 then
                              EndFl:= False;
                            SyncRec.Action:= Act.Acts[ActI];
                            SyncRec.Action.Where_Src:= ParseStr(SyncRec.Action.Where_Src, SrcDB, DBProps.ObjPrefix);
                            SyncRec.Action.Where_Tgt:= ParseStr(SyncRec.Action.Where_Tgt, SrcDB, DBProps.ObjPrefix);
                            fReplLog.Log(fLogName, lchNull, Format(sSyncRepl, [SyncRec.GroupId, SyncRec.TgtDBId, ActI, SyncRec.RelationName, SyncRec.Action.Action, SyncRec.Action.Where_Src])); // only to screen ?
                            FillChar(Cnt, SizeOf(Cnt), 0);
                            Cnt.StartTick:= GetTickCount();
                            TgtDB.SynchronizeTable(SyncRec, SrcDB, Cnt);
                            fReplLog.Log(fLogName, lchNull, Format(sSyncCount, [Cnt.Total, Cnt.Deleted, Cnt.Updated, Cnt.Inserted, Abs(GetTickCount()-Cnt.StartTick) div 1000, FloatToStrF(Cnt.Total / (Abs(GetTickCount()-TotalCnt.StartTick+1) / 1000 / 60), ffGeneral, 2, 0)]));

                            Inc(TotalCnt.Inserted, Cnt.Inserted);
                            Inc(TotalCnt.Deleted, Cnt.Deleted);
                            Inc(TotalCnt.Updated, Cnt.Updated);
                            Inc(TotalCnt.Errors, Cnt.Errors);
                            Inc(TotalCnt.Conflicts, Cnt.Conflicts);
                            if SyncRec.Action.Action and sasrcTgt <> 0 then  // log only once processed source/target looped records
                              begin
                                if SyncRec.Tag and $01 = 0 then
                                  Inc(TotalCnt.Total, Cnt.Total);
                                SyncRec.Tag:= SyncRec.Tag or $01;
                              end
                            else
                              begin
                                if SyncRec.Tag and $02 = 0 then
                                  Inc(TotalCnt.Total, Cnt.Total);
                                SyncRec.Tag:= SyncRec.Tag or $02;
                              end;
                            try
                              DBSQLExec(fConfigDatabase, Format('UPDATE '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'RELATIONS')+' SET S_MSEC=S_MSEC+%d,S_INSERT=S_INSERT+%d,S_UPDATE=S_UPDATE+%d,S_DELETE=S_DELETE+%d,S_ERROR=S_ERROR+%d,S_CONFLICT=S_CONFLICT+%d', [Abs(GetTickCount()-TotalCnt.StartTick), Cnt.Inserted, Cnt.Updated, Cnt.Deleted, Cnt.Errors, Cnt.Conflicts])+
                                                         Format('WHERE SCHEMAID=%d AND GROUPID=%d AND RELATIONID=%d', [SyncRec.SchemaId, SyncRec.GroupId, SyncRec.RelationId]));
                            except
                              fDBLog.Log(fLogName, lchError, Format(sUpdateStatRelErr, [SyncRec.SchemaId, SyncRec.GroupId, SyncRec.RelationId]));
                            end;
                            SafeCommit(Q.Transaction, True);
                            Inc(N, Cnt.Total);
                            DoUpdateStatus(N, GetTickCount()-TotalCnt.StartTick);
                          end;
                        except
                          on E: Exception do
                          begin
                            if CheckIBConnectionError(E, nil) then
                              raise;
                            SafeRollback(Q.Transaction, True);
                            fDBLog.Log(fLogName, lchError, E.Message);
                          end;
                        end;
                        SafeNext(Q2);
                      end;
                    finally
                      SafeClose(Q2);
                    end;
                    Inc(ActI);
                  until EndFl;
                finally
                  Q2.Free;
                end;
              end;
            finally
              TgtDB.Free;
            end;
            SafeCommit(Q.Transaction, True);
          except
            on E: Exception do
            begin
              SafeRollback(Q.Transaction, True);
              if CheckIBConnectionError(E, nil) then
                raise;
              fDBLog.Log(fLogName, lchError, E.Message)
            end;
          end;
          SafeNext(Q);
          ProcessMessages;
        end;
      finally
        Q.Free;
      end;
      try
        DBSQLExec(fConfigDatabase, Format('UPDATE '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'SCHEMATA')+' SET S_MSEC=%d,S_INSERT=%d,S_UPDATE=%d,S_DELETE=%d,S_ERROR=%d,S_CONFLICT=%d', [Abs(GetTickCount()-Cnt.StartTick), TotalCnt.Inserted, TotalCnt.Updated, TotalCnt.Deleted, TotalCnt.Errors, TotalCnt.Conflicts])+
                                                  Format('WHERE SCHEMAID=%d', [SyncRec.SchemaId]));
      except
        fDBLog.Log(fLogName, lchError, Format(sUpdateStatSchErr, [SyncRec.SchemaId]));
      end;
    finally
      fReplLog.Log(fLogName, lchNull, Format(sSyncTotal, [TotalCnt.Total, TotalCnt.Deleted, TotalCnt.Updated, TotalCnt.Inserted]));
      SrcDB.Free;
    end;
  finally
    SafeCommit(fConfigDatabaseTransaction);
  end;
end;

procedure TIBReplicator.PutOffReplString(Tgt: TStream; const Val: string; LSize: Byte; asWide, aUTF8: Boolean);
var
  L, L2: Longword;
  Dest: PWideChar;
  UTFS: UTF8string;
begin
  if asWide and aUTF8 then
    begin
      UTFS:= AnsiToUTF8(Val);
      L:= Length(UTFS);
    end
  else
    begin
      UTFS:= '';  // unhint
      L:= Length(Val);
    end;
  Tgt.WriteBuffer(L, LSize);
  L2:= 0;
  Move(L, L2, LSize);
  if asWide then
    begin
      if aUTF8 then
        begin
          if UTFS <> '' then
            Tgt.WriteBuffer(UTFS[1], L2);
        end
      else
        begin
          GetMem(Dest, 2*(L2+1));
          try
            StringToWideChar(Val, Dest, L2+1);
            Tgt.WriteBuffer(Dest^, 2*L2);
          finally
            FreeMem(Dest);
          end
        end;
    end
  else
    Tgt.WriteBuffer(Val[1], L2);
end;

class function TIBReplicator.GetOffReplString(Src: TStream; LSize: Byte; asWide: Boolean; aUTF8: Boolean): string;
var
  L: Longword;
  Dest: PWideChar;
  UTFS: UTF8string;
begin
  L:= 0;
  Src.ReadBuffer(L, LSize);
  if asWide then
    begin
      if aUTF8 then
        begin
          SetLength(UTFS, L);
          if L > 0 then
            Src.ReadBuffer(UTFS[1], L);
          Result:= Utf8ToAnsi(UTFS);
        end
      else
        begin
          GetMem(Dest, 2*(L+1));
          try
            Src.ReadBuffer(Dest^, 2*L);
            Dest[L]:= #0;
            Result:= WideCharToString(Dest);
          finally
            FreeMem(Dest);
          end
        end;
    end
  else
    begin
      SetLength(Result, L);
      Src.ReadBuffer(Result[1], L);
    end;
end;

procedure TIBReplicator.PutOffReplRecValue(Tgt: TStream; F1: TIBXSQLVAR);
var
  RLF: TReplLogField_IBSQL;
begin
  RLF:= TReplLogField_IBSQL.Create;
  try
    RLF.IBXSQLVAR:= F1;
    PutOffReplRecValue(Tgt, RLF);
  finally
    RLF.Free;
  end;
end;

procedure TIBReplicator.PutOffReplRecValue(Tgt: TStream; F1: TReplLogField);
var
  RRV: TOffReplRecValue;
  I: Longint;
  L: Int64;
  F: Extended;
  S: string;
  B: Byte;
  SaveDecimalSeparator, SaveThousandSeparator: Char;
begin
  SaveDecimalSeparator:= DecimalSeparator;
  SaveThousandSeparator:= ThousandSeparator;
  DecimalSeparator:= '.';
  ThousandSeparator:= ',';
  try
    if F1 <> nil then
      begin
        RRV.FieldType:= Byte(F1.DataType);
        if F1.isNull then
          RRV.DataType:= 'N'
        else
          case TFieldType(RRV.FieldType) of
            ftString:
              begin
                RRV.DataType:= 'C';
                S:= F1.asString;
              end;
            ftSmallint, ftInteger, ftWord:
              begin
                RRV.DataType:= 'I';
                I:= F1.asInteger;
              end;
            ftLargeInt:
              begin
                RRV.DataType:= 'J';
                L:= F1.asInt64;
              end;
            {ftBoolean:   // not possible
              begin
                RRV.DataType:= 'B';
                B:= Byte(F1.asBoolean);
              end;}
            ftFloat:
              begin
                RRV.DataType:= 'F';
                F:= F1.asFloat;
              end;
            ftDate:
              begin
                RRV.DataType:= 'D';
                S:= Copy(DT2OffStamp(F1.AsDateTime), 1, 4);
              end;
            ftTime:
              begin
                RRV.DataType:= 'T';
                S:= Copy(DT2OffStamp(F1.AsDateTime), 4, 5);
              end;
            ftDateTime:
              begin
                RRV.DataType:= 'S';
                S:= DT2OffStamp(F1.AsDateTime);
              end;
            ftMemo:
              begin
                RRV.DataType:= 'M';
              end;
            ftBCD:     // some kind of float/currency is ftBCD fieldtype
              begin
                RRV.DataType:= 'H';
                S:= F1.asString;
              end;
            else
              begin
                RRV.DataType:= 'O';
              end;
          end;
      end
    else
      begin
        RRV.DataType:= ' ';
        RRV.FieldType:= 0;
      end;
    Tgt.WriteBuffer(RRV, SizeOf(RRV));

    case RRV.DataType of
      'C': PutOffReplString(Tgt, S, SizeOf(Longword), True, fUTF8);
      'H': PutOffReplString(Tgt, S, SizeOf(Byte), False, fUTF8);
      'M': PutOffReplString(Tgt, F1.asString, SizeOf(Longword), True, fUTF8);
      'D','T','S': Tgt.WriteBuffer(S[1], Length(S));
      'I': Tgt.WriteBuffer(I, SizeOf(I));
      'J': Tgt.WriteBuffer(L, SizeOf(L));
      'B': Tgt.WriteBuffer(B, SizeOf(B));
      'F': Tgt.WriteBuffer(F, SizeOf(F));
      'O': PutOffReplString(Tgt, F1.asString, SizeOf(Longword), False, fUTF8);
    end;
  finally
    DecimalSeparator:= SaveDecimalSeparator;
    ThousandSeparator:= SaveThousandSeparator;
  end;
end;

class function TIBReplicator.GetOffReplValueLength(Src: TStream; aUTF8: Boolean): Longword;
var
  RRV: TOffReplRecValue;
  StPos: LongInt;
begin
  StPos:= Src.Position;
  Src.ReadBuffer(RRV, SizeOf(RRV));
  Result:= 0;
  case RRV.DataType of
    'N',' ': Result:= 0;
    'I': Result:= SizeOf(Longint);
    'J': Result:= SizeOf(Int64);
    'B': Result:= SizeOf(Byte);
    'F': Result:= SizeOf(Extended);
    'D': Result:= 4;
    'T': Result:= 5;
    'S': Result:= SizeOf(TOffStamp);
    'M','C':
      begin
        Src.ReadBuffer(Result, SizeOf(Longword));
        if not aUTF8 then
          Result:= 2*Result;
        Inc(Result, SizeOf(Longword));
      end;
    'H':
      begin
        Src.ReadBuffer(Result, SizeOf(Byte));
        Result:= Result+SizeOf(Byte);
      end;
    'O':
      begin
        Src.ReadBuffer(Result, SizeOf(Longword));
        Result:= Result+SizeOf(Longword);
      end;
  else
    begin
      IBReplicatorError(sBadOffPackage, 1);
    end;
  end;
  Inc(Result, SizeOf(RRV));
  Src.Position:= StPos;   // return at position just before read length
end;


class function TIBReplicator.GetOffReplVariant(Src: TStream; aUTF8: Boolean): Variant;
var
  RRV: TOffReplRecValue;
  DT: TOffStamp;
  L: Longint;
  C: Comp;
  B: Byte;
  E: Extended;
begin
  Src.ReadBuffer(RRV, SizeOf(RRV));
  FillChar(DT, SizeOf(DT), 0);
  case RRV.DataType of
    ' ': Result:= Unassigned;
    'N': Result:= Null;
    'C','M': Result:= GetOffReplString(Src, SizeOf(Longword), True, aUTF8);
    'H': Result:= GetOffReplString(Src, SizeOf(Byte), False, False);
    'I':
      begin
        Src.ReadBuffer(L, SizeOf(L));
        Result:= L;
      end;
    'J':
      begin
        Src.ReadBuffer(C, SizeOf(C));
        Result:= C;  // cannot assign Int64 to variant
      end;
    'B':
      begin
        Src.ReadBuffer(B, SizeOf(B));
        Result:= B <> 0;
      end;
    'F':
      begin
        Src.ReadBuffer(E, SizeOf(E));
        Result:= E;
      end;
    'D':
      begin
        Src.ReadBuffer(DT[1], 4);
        Result:= VarFromDateTime(OffStamp2DT(DT));
      end;
    'T':
      begin
        Src.ReadBuffer(DT[5], 5);
        Result:= VarFromDateTime(OffStamp2DT(DT));
      end;
    'S':
      begin
        Src.ReadBuffer(DT, SizeOf(DT));
        Result:= VarFromDateTime(OffStamp2DT(DT));
      end;
    'O':
     begin
        Result:= GetOffReplString(Src, SizeOf(Longword), False, False);
     end;
  end;
end;

procedure TIBReplicator.PrepareSourceOfflineRecord;
var
  OldK, NewK, S: string;
  I: Integer;
  RD: TOffSrcRelDef;
  RDF: TOffSrcRelDefField;
  RL: TOffSrcReplLog;
  RR: TOffSrcRelRec;
  REx: TOffSrcExternalRec;
  REV: TOffSrcExternalValue;
  SavePos, SavePos2: LongWord;

  procedure PutValue(Tgt: TStream; F1: TReplLogField; const FName1: string);
  begin
    if F1 = nil then
      fDBLog.Log(fLogName, lchError, Format(sPutSQLFieldNotFound, [FName1, aReplLogRec.FieldDefs.RelationName_Src]));
    PutOffReplRecValue(Tgt, F1);
  end;

  procedure PutFValues(Tgt: TStream; aNew: Char; aCurrentIfNotFound: Boolean);
  var
    I: Integer;
    RR: TOffSrcRelRec;
    SavePos: LongWord;
    RRV: TOffReplRecValue;
    F: TReplLogField;
  begin
    RR.FieldCount:= 0;
    SavePos:= Tgt.Position;
    Tgt.WriteBuffer(RR, SizeOf(RR));  // place holder
    for I:= 0 to Length(aReplLogRec.FieldDefs.Fields[2])-1 do  // in cached list sorted by FIELDID and in FieldQ sort by MODE {'N','O'},FIELDID
    begin
      F:= aReplLogRec.FieldQ_Locate(aNew, aReplLogRec.FieldDefs.Fields[2][I].Idx);
      if (F = nil) and aCurrentIfNotFound then   // if not found in REPL$FIELD get current value (current is probably better than nothing)
        F:= aReplLogRec.Src_Record[aReplLogRec.FieldDefs.Fields[2][I].Idx];
      if F <> nil then
        PutValue(Tgt, F, aReplLogRec.FieldDefs.Fields[2][I].FieldName_Src)
      else
        begin   // not found
          RRV.DataType:= ' ';
          RRV.FieldType:= Byte(ftInteger);
          Tgt.WriteBuffer(RRV, SizeOf(RRV));
        end;
      Inc(RR.FieldCount);
    end;

    Tgt.Position:= SavePos;
    Tgt.WriteBuffer(RR, SizeOf(RR));
    Tgt.Position:= Tgt.Size;
  end;

resourcestring
  serrReadExtFile = 'Error reading file "%s"';
begin
  if not aReplLogRec.CRI.OffRelDone then  // relation definition has been written
  begin
    RD.RecType:= offrtRelationDef;
    RD.RelationId:= aReplLogRec.RelationId;
    RD.Count:= Length(aReplLogRec.FieldDefs.FieldRefs);
    FillChar(RD.Name, SizeOf(RD.Name), Ord(' '));
    S:= Copy(aReplLogRec.FieldDefs.RelationName_Src, 1, SizeOf(RD.Name));
    Move(S[1], RD.Name, Length(S));
    Tgt.WriteBuffer(RD, SizeOf(RD));
    for I:= 0 to Length(aReplLogRec.FieldDefs.FieldRefs)-1 do
    begin
      FillChar(RDF.Name, SizeOf(RDF.Name), Ord(' '));
      with aReplLogRec.FieldDefs.FieldRefs[I] do
      begin
        S:= Copy(aReplLogRec.FieldDefs.Fields[FieldType][Idx].FieldName_Src, 1, SizeOf(RDF.Name));
        Move(S[1], RDF.Name, Length(S));
        RDF.FieldType:= FieldType;
      end;
      Tgt.WriteBuffer(RDF, SizeOf(RDF));
    end;
    aReplLogRec.CRI.OffRelDone:= True;
    Inc(aRelCounter);
  end;
  RL.RecType:= offrtReplLog;
  RL.RelationId:= aReplLogRec.RelationId;
  RL.SeqId:= aReplLogRec.SeqId;
  RL.RepType:= aReplLogRec.IUD;
  RL.Sep:= aReplLogRec.Separator;
  RL.Stamp:= DT2OffStamp(aReplLogRec.Stamp);
  OldK:= ArrayToString(aReplLogRec.OldPKey, aReplLogRec.Separator);
  NewK:= ArrayToString(aReplLogRec.NewPKey, aReplLogRec.Separator);

  RL.Options:= 0;
  if not aReplLogRec.Src_IsEmpty then
    RL.Options:= RL.Options or optIsRelRecord;
  if not aReplLogRec.FieldQ_IsEmpty then
    RL.Options:= RL.Options or optIsForeignRecord;
  if fUTF8 then
    RL.Options:= RL.Options or optUTF8;

  if Length(aReplLogRec.Externals) > 0 then
    RL.Options:= RL.Options or optIsExternalRecord;

  Tgt.WriteBuffer(RL, SizeOf(RL));

  PutOffReplString(Tgt, OldK, SizeOf(Word), True, fUTF8);
  PutOffReplString(Tgt, NewK, SizeOf(Word), True, fUTF8);
  PutOffReplString(Tgt, '', SizeOf(Word), True, fUTF8);  // backward compatability only

  if RL.Options and optIsForeignRecord <> 0 then
  begin
    PutFValues(Tgt, 'N', True);   // 'N' < 'O'
    PutFValues(Tgt, 'O', False);
  end;

  if RL.Options and optIsRelRecord <> 0 then   // export values from table
  begin
    RR.FieldCount:= Length(aReplLogRec.FieldDefs.FieldRefs);
    Tgt.WriteBuffer(RR, SizeOf(RR));
    for I:= 0 to Length(aReplLogRec.FieldDefs.FieldRefs)-1 do
      with aReplLogRec.FieldDefs.FieldRefs[I] do
        PutValue(Tgt, aReplLogRec.Src_Record[I], aReplLogRec.FieldDefs.Fields[FieldType][Idx].FieldName_Src);
  end;
  if Length(aReplLogRec.Externals) > 0 then
  begin
    FillChar(REx, SizeOf(REx), 0);
    REx.FieldCount:= Length(aReplLogRec.Externals);
    Tgt.WriteBuffer(REx, SizeOf(REx));
    for I:= 0 to Length(aReplLogRec.Externals)-1 do
    begin
      SavePos:= Tgt.Position;
      FillChar(REV, SizeOf(REV), 0);
      with aReplLogRec.Externals[I] do
      begin
        REV.Idx:= Idx;
        REV.Stamp:= DT2OffStamp(Stamp);
        REV.Size:= DataSize;
        if Data <> nil then
        begin
          REV.Options:= REV.Options or optextData;
          Data.Position:= DataPos;
        end;
        Tgt.WriteBuffer(REV, SizeOf(REV));
        PutOffReplString(Tgt, FileName, SizeOf(Byte), True, fUTF8);
      end;
      if REV.Size <> 0 then
      begin
        SavePos2:= Tgt.Position;
        try
          if REV.Size <> LongWord(Tgt.CopyFrom(aReplLogRec.Externals[I].Data, REV.Size)) then
            IBReplicatorError(Format(serrReadExtFile, [aReplLogRec.Externals[I].FileName]), 1);
        except
          on E: Exception do
          begin
            fDBLog.Log(fLogName, lchError, E.Message);
            Tgt.Position:= SavePos;
            REV.Options:= REV.Options and not optextData;
            Tgt.WriteBuffer(REV, SizeOf(REV));
            Tgt.Size:= SavePos2;
          end;
        end;
      end;
    end;
  end;
end;

procedure CheckMagic2(M: TOffMagic);
begin
  if not CheckMagic(M) then
    IBReplicatorError(sBadOffMagicNumber, 1);
end;

function CalcCRC(St: TStream; aMoveBehindHeader: Boolean): TCRC32;
var
  SavePos: Integer;
  P: Pointer;
  Sz: Integer;
begin
  SavePos:= St.Position;
  if aMoveBehindHeader then
    St.Position:= St.Position+SizeOf(TOffHeader);
  Sz:= St.Size-St.Position;
  GetMem(P, Sz);
  try
    St.ReadBuffer(P^, Sz);
    CalcCRC32(P^, Sz, Result);
  finally
    FreeMem(P);
  end;
  St.Position:= SavePos;
end;

procedure CheckCRC(H: TOffHeader; St: TStream);
var
  Crc: TCrc32;
resourcestring
  sBadCRC = 'Stream corrupted, bad CRC';
begin
  if H.Magic.Version < offVersionCRC then
    Exit;
  Crc:= CalcCRC(St, False);
  if Crc <> H.Crc then
    IBReplicatorError(sBadCRC, 1);
end;

procedure TIBReplicator.GenTransferId;
begin
  if aId = 0 then
  begin
    aId:= GenerateId(aDB, aDBProps.ObjPrefix+'GEN_TRANSFERID', 1);
  end;
end;

{
  TRANSFER flow
  A1) source: build package, new 'S' record RS1
  A2) source: send it and fill RS1.STAMP_SENT
  A3) target: receive it, process, insert into table 'S' record RS1, fill RS1.STAMP_REC
  A4) target: process received data and build answer package, new 'T' record RT1, RS1.TRANSFERID_ACK_SENT:= RT1, insert REPL$LOG/MAN seqids into package
  A5) target: send it and fill RT1.STAMP_SENT
  A6) source: receive answer package, insert into table 'T' record RT1, fill RT1.STAMP_REC,
  A7) source: process received data (REPL$LOG/MAN), RS1.TRANSFERID_ACK_SENT:= RT1;

  B1) source: ..., for all

  Source:
  1) byl prenos prijat?  RS1.TRANSFERID_ACK <> NULL
  2) vi target, ze jsem dostal odpoved: RT1[RS1.TRANSFERID_ACK].TRANSFERID_ACK <> NULL
  3) mam odeslat, ze jsem to zpracoval: RS1.TRANSFERID_ACK = NULL
}

function TIBReplicator.PrepareSourceOfflinePackage;
var
  HeaderPos: Integer;
  ReplQ, FieldQ, SQ: TIBSQL;
  LogDeleteSP: TIBStoredProc;
  DBMask: Integer;
  Cnt1, Cnt2, Cnt3, Cnt4: Integer;
  H: TOffHeader;
  RRS: TOffReplResend;
  RA: TOffSrcReplAckAck;
  DT: TDateTime;
  StartTick: Longword;
  ReplLogRec: TReplLogRecord_Online;
begin
  Result:= 0;
  ReplQ:= TIBSQL.Create(nil);
  try
    ReplQ.Database:= aDB;
    ReplQ.Transaction:= TIBTransaction.Create(ReplQ);
    ReplQ.Transaction.Params.Text:= tranRW;
    ReplQ.Transaction.DefaultDatabase:= ReplQ.Database;
//    ReplQ.SQL.Text:= Format('SELECT R.*,S.RELATIONNAME,S.SEPARATOR FROM REPL$LOG R,REPL$CONFIG S WHERE R.DBID=%d AND R.SCHEMAID=S.SCHEMAID AND R.DBID=S.DBID AND R.GROUPID=S.GROUPID AND R.RELATIONID=S.RELATIONID ORDER BY R.DBID,R.GROUPID,R.SEQID', [GetSourceDBId(aSchemaId)]);
    SafeStartTransaction(ReplQ.Transaction);
    try
      FieldQ:= TIBSQL.Create(nil);
      try
        FieldQ.Database:= ReplQ.Database;
        FieldQ.Transaction:= ReplQ.Transaction;
        FieldQ.SQL.Text:= 'SELECT * FROM '+FormatIdentifier(FieldQ.Database.SQLDialect, aDBProps.ObjPrefix+'FIELD2')+'(:SCHEMAID,:SEQID,'''',0)';
        FieldQ.Prepare;
        DT:= Now2();
        FillChar(H, SizeOf(H), 0);
        FillMagic(H.Magic, offVersionV2);
        H.SchemaId:= aSchemaId;
        H.SrcDbId:= aSrcDbId;
        H.TgtDBId:= aTgtDBId;
        H.GroupId:= aGroupId;
        H.Stamp:= DT2OffStamp(DT);
        H.Status:= 'S';
        HeaderPos:= Tgt.Position;
        ReplQ.SQL.Text:= Format('SELECT * FROM '+FormatIdentifier(ReplQ.Database.SQLDialect, aDBProps.ObjPrefix+'LOG2')+'(%d,%d) WHERE DBMASK<>0', [aSchemaId,aGroupId]);
        SafeExecQuery(ReplQ);

        DBMask:= GetDBMask(aSchemaId, aGroupId, aTgtDBId);
        if DBMask = -1 then
          IBReplicatorError(Format(sNoRequiredTarget, [aSchemaId, aGroupId, aTgtDBId]), 1);

        SQ:= TIBSQL.Create(nil);
        try
          SQ.Database:= ReplQ.Database;
          SQ.Transaction:= ReplQ.Transaction;

          LogDeleteSP:= TIBStoredProc.Create(nil);  // need blob parameter
          try
            LogDeleteSP.Database:= ReplQ.Database;
            LogDeleteSP.Transaction:= ReplQ.Transaction;
            LogDeleteSP.StoredProcName:= aDBProps.ObjPrefix+'LOG_DELETE';
            LogDeleteSP.Prepare;

            Cnt1:= 0; Cnt2:= 0; StartTick:= GetTickCount();
            ClearCachedValues(True);
            while not ReplQ.EOF and not ForceStop do
            begin
              if (ReplQ.FieldByName('DBMASK').asInteger and DBMask <> 0) and not ReplQ.FieldByName('RELATIONNAME').IsNull { should not happen unless repl$log is not consistent with repl$config } then
              begin
                ReplLogRec:= TReplLogRecord_Online.Create(Self);
                try
                  ReplLogRec.AssignReplQ(ReplQ);

                  if ReadCachedFields(ReplLogRec.SchemaId, ReplLogRec.GroupId, ReplLogRec.RelationId, True, ReplLogRec.CRI) then  { any definition ? }
                  begin
                    ReplLogRec.FieldDefs.BuildFieldDefs(ReplLogRec.CRI, ReplQ.Database.SQLDialect, 3{dummy});
                    ReplLogRec.FieldDefs.GetWhereValues(SQ.Database.SQLDialect, 3{dummy}, ReplLogRec.IUD, ReplLogRec.OldPKey, ReplLogRec.NewPKey, ReplLogRec.FieldDefs.Where_Src, ReplLogRec.FieldDefs.Where_Tgt, ReplLogRec.FieldDefs.Where_Tgt_New);
                    if ReplLogRec.FieldDefs.Where_Src = '' then
                      IBReplicatorError(Format(sNoPrimaryKey, [ReplLogRec.FieldDefs.RelationName_Src]), 2);

                    if Tgt.Position = HeaderPos then
                      Tgt.WriteBuffer(H, SizeOf(H));
                    try
                      FieldQ.Params.ByName('SCHEMAID').asInteger:= aSchemaId;
                      FieldQ.Params.ByName('SEQID').asInteger:= ReplLogRec.SeqId;
                      SafeExecQuery(FieldQ);
                      try
                        ReplLogRec.AssignFieldQ(FieldQ);
                        SQ.SQL.Text:= Format('SELECT * FROM %s WHERE %s', [FormatIdentifier(SQ.Database.SQLDialect, ReplLogRec.FieldDefs.RelationName_Src), ReplLogRec.FieldDefs.Where_Src]);
                        SafeExecQuery(SQ);  // always open, current record is exported
                        try
                          ReplLogRec.AssignSQ(SQ);
                          ReplLogRec.AssignExtFiles(aDBProps);
                          PrepareSourceOfflineRecord(ReplLogRec, Tgt, Cnt1, Cnt2);
                        finally
                          SafeClose(SQ);
                        end;
                      finally
                        SafeClose(FieldQ);
                      end;

                      LogDeleteSP.ParamByName('SCHEMAID').asInteger:= aSchemaId;
                      LogDeleteSP.ParamByName('SEQID').asInteger:= ReplLogRec.SeqId;
                      LogDeleteSP.ParamByName('DBMASK').asInteger:= DBMask;
                      LogDeleteSP.ParamByName('ACTIONTYPE').asInteger:= 1;
                      FillLogDeleteDesc(LogDeleteSP, '', nil);
                      SafeExecProc(LogDeleteSP);
                      SafeCommit(LogDeleteSP.Transaction, True);
                    except
                      on E: Exception do
                      begin
                        fDBLog.Log(fLogName, lchError, Format(sReplOffErr, [E.Message, ReplLogRec.SeqId]));
                        raise;
                      end;
                    end;
                  end;
                finally
                  ReplLogRec.Free;
                end;
              end;
              DoUpdateStatus(Cnt1, GetTickCount()-StartTick);
              SafeNext(ReplQ);
            end;
          finally
            LogDeleteSP.Free;
          end;
        finally
          SQ.Free;
        end;
        if (Tgt.Position = HeaderPos) and aIfReplRecord then
        begin
          fReplLog.Log(fLogName, lchNull, sReplOffNoData);   // no data, if REPL$LOG is empty do not acknowledge T record
          Exit;
        end;
        SafeClose(ReplQ);  // acknowledge receiving of acknowledge (CZ: potvrzeni o prijeti potvrzeni)

        ReplQ.SQL.Text:= Format('SELECT T.TRANSFERID,T.STAMP_REC,T.STAMP_PROC,S.STAMP_SENT,S.TRANSFERID AS TRANSFERIDS FROM '+FormatIdentifier(ReplQ.Database.SQLDialect, aDBProps.ObjPrefix+'TRANSFER')+' S INNER JOIN '+FormatIdentifier(ReplQ.Database.SQLDialect, aDBProps.ObjPrefix+'TRANSFER')+' T ON S.STATUS=''S'' AND T.STATUS=''T'' AND S.TRANSFERID_ACK=T.TRANSFERID AND S.SCHEMAID=T.SCHEMAID AND T.TRANSFERID_ACK IS NULL WHERE S.SCHEMAID=%d AND S.GROUPID=%d AND S.DBID=%d', [aSchemaId, aGroupId, aTgtDBId]);
        SafeExecQuery(ReplQ);
        Cnt3:= 0;
        if not ReplQ.EOF then
        begin
          if Tgt.Position = HeaderPos then
            Tgt.WriteBuffer(H, SizeOf(H));
          GenTransferId(ReplQ.Database, H.TransferId, aDBProps, ReplQ.Transaction);
          while not ReplQ.EOF and not ForceStop do
          begin
            RA.RecType:= offrrtReplAckAck;
            RA.TransferId:= ReplQ.FieldByName('TRANSFERIDS').asInteger;
            RA.TransferIdAck:= ReplQ.FieldByName('TRANSFERID').asInteger;
            RA.StampSent:= DT2OffStamp(ReplQ.FieldByName('STAMP_SENT').asDateTime);    // flag has been filled after creating of package, I'll send the information in addition
            RA.StampRec:= DT2OffStamp(ReplQ.FieldByName('STAMP_REC').asDateTime);
            RA.StampProc:= DT2OffStamp(ReplQ.FieldByName('STAMP_PROC').asDateTime);  // warning: sent acknowledge but not yet processed - condition is (CZ: pozor kdyby se odeslalo potvrzeni a jeste nebylo zpracovano podm.) STAMP_PROC IS NOT NULL ???
            Tgt.WriteBuffer(RA, SizeOf(RA));
            DBSQLExec(ReplQ.Database, Format('UPDATE '+FormatIdentifier(ReplQ.Database.SQLDialect, aDBProps.ObjPrefix+'TRANSFER')+' SET TRANSFERID_ACK=%d WHERE SCHEMAID=%d AND STATUS=''T'' AND TRANSFERID=%d AND GROUPID=%d AND DBID=%d', [H.TransferId, aSchemaId, ReplQ.FieldByName('TRANSFERID').asInteger, aGroupId, aTgtDbId]), ReplQ.Transaction);
            SafeNext(ReplQ);
            Inc(Cnt3);
          end;
        end;

        SafeClose(ReplQ);
        { process rereceived packages, probably sent by source because not acknowledged (and not processed REPL$LOG) }
        ReplQ.SQL.Text:= Format('SELECT * FROM '+FormatIdentifier(ReplQ.Database.SQLDialect, aDBProps.ObjPrefix+'TRANSFER')+' T WHERE T.STATUS=''T'' AND T.SCHEMAID=%d AND T.GROUPID=%d AND T.DBID=%d AND RESEND_FLAG=''Y''', [aSchemaId, aGroupId, aTgtDBId]);
        SafeExecQuery(ReplQ);
        Cnt4:= 0;
        while not ReplQ.EOF and not ForceStop do
        begin
          if Tgt.Position = HeaderPos then
            Tgt.WriteBuffer(H, SizeOf(H));
          RRS.RecType:= offrReplResend;
          RRS.TransferId:= ReplQ.FieldByName('TRANSFERID').asInteger;
          RRS.TransferIdAck:= ReplQ.FieldByName('TRANSFERID_ACK').asInteger;
          RRS.StampRec:= DT2OffStamp(ReplQ.FieldByName('STAMP_REC').asDateTime);
          RRS.StampProc:= DT2OffStamp(ReplQ.FieldByName('STAMP_PROC').asDateTime);
          Tgt.WriteBuffer(RRS, SizeOf(RRS));
          DBSQLExec(ReplQ.Database, Format('UPDATE '+FormatIdentifier(ReplQ.Database.SQLDialect, aDBProps.ObjPrefix+'TRANSFER')+' SET RESEND_FLAG=''N'' WHERE SCHEMAID=%d AND STATUS=''T'' AND TRANSFERID=%d AND GROUPID=%d AND DBID=%d', [aSchemaId, ReplQ.FieldByName('TRANSFERID').asInteger, aGroupId, aTgtDbId]), ReplQ.Transaction);
          SafeNext(ReplQ);
          Inc(Cnt4);
        end;

        fReplLog.Log(fLogName, lchNull, Format(sReplOffTotal, [Cnt1, Cnt2, Cnt3, Cnt4]));
        if Tgt.Position = HeaderPos then  // no data
          Exit;
        GenTransferId(ReplQ.Database, H.TransferId, aDBProps, ReplQ.Transaction);
        Tgt.Position:= HeaderPos;
        H.CRC:= CalcCRC(Tgt, True);
        Tgt.WriteBuffer(H, SizeOf(H));
        Tgt.Position:= Tgt.Size;
        DBSQLExec(ReplQ.Database, Format('INSERT INTO '+FormatIdentifier(ReplQ.Database.SQLDialect, aDBProps.ObjPrefix+'TRANSFER')+'(TRANSFERID,STATUS,SCHEMAID,GROUPID,DBID,STAMP) VALUES (%d,''S'',%d,%d,%d,%s)', [H.TransferId, aSchemaId, aGroupId, aTgtDBId, DT2IBDT(DT)]), ReplQ.Transaction);
        Result:= H.TransferId;
      finally
        FieldQ.Free;
      end;
    finally
      SafeCommit(ReplQ.Transaction); { no changes made in this transaction }
    end;
  finally
    ReplQ.Free;
  end;
end;

function TIBReplicator.ProcessSourceOfflinePackageAndPrepareTargetOfflinePackage;
var
  Q: TIBSQL;
  B: Byte;
  H1, H2: TOffHeader;
  RD: TOffSrcRelDef;
  RDF: TOffSrcRelDefField;
  RL: TOffSrcReplLog;
  RAA: TOffSrcReplAckAck;
  RAAA: TOffTgtReplAckAckAck;
  RA: TOffTgtReplAck;
  RRS: TOffReplResend;
  HeaderPos, I, Cnt3, Cnt4, CntManI, CntReplD, Cnt: Integer;
  V: Variant;
  OldPKey, NewPKey, NewFKey, Dummy, StatS: string;
  CRI: TReplCachedRelationItem;
  Sg: TStrings;
  TickCount, TickCount2: Longword;
  TgtDB: record
    Tgt: TReplTargetDatabase;
    Fl: Byte;
    CntU, CntI, CntD: Integer;
    Cnt2U, Cnt2I, Cnt2D: Integer;
    Counter: Integer;
  end;
  ReplLogRec: TReplLogRecord_Offline;

  function GetS(aName: TRelName): string;
  begin
    SetLength(Result, SizeOf(aName));
    Move(aName, Result[1], Length(Result));
    Result:= TrimRight(Result);
  end;

  procedure CheckMapping(CRI: TReplCachedRelationItem; const aRelName: string; aRelId: Integer; aFields: TStrings);
  var
    I, J, K, L: Integer;
    FType: Byte;
    Cnt: array[1..2] of Integer;
  begin
    if CRI.Name <> aRelName then // critical error
    begin
      CRI.Fields.Clear;
      fDBLog.Log(fLogName, lchError, Format(sOffRemap3, [CRI.Name, aRelId, aRelName]));
    end;
    CRI.OffRelationName:= aRelName;
    SetLength(CRI.RemapFields, aFields.Count);

    for I:= Low(CRI.RemapKeys) to High(CRI.RemapKeys) do
    begin
      SetLength(CRI.RemapKeys[I].CfgKeyToPkgKey, 0);
      SetLength(CRI.RemapKeys[I].PkgKeyToPkgFieldIdx, 0);
    end;
    for I:= 0 to aFields.Count-1 do
      CRI.RemapFields[I]:= -1;
    FillChar(Cnt, SizeOf(Cnt), 0);
    for I:= 0 to CRI.Fields.Count-1 do
    begin
      J:= aFields.IndexOf(TReplCachedFieldItem(CRI.Fields[I]).Name);
      FType:= TReplCachedFieldItem(CRI.Fields[I]).FType;
      L:= -1;

      if J = -1 then
        begin
          fDBLog.Log(fLogName, lchError, Format(sOffRemap1, [CRI.Name, aRelId, TReplCachedFieldItem(CRI.Fields[I]).Name]));
        end
      else
        begin
          CRI.RemapFields[J]:= I;   // pos in offline package -> pos in config db
          if FType <> Byte(aFields.Objects[J]) then
          begin
            fDBLog.Log(fLogName, lchError, Format(sOffRemap2, [CRI.Name, aRelId, TReplCachedFieldItem(CRI.Fields[I]).Name, FType, Byte(aFields.Objects[J])]));
          end;
          if Byte(aFields.Objects[J]) in [Low(CRI.RemapKeys)..High(CRI.RemapKeys)] then   // pos in package key -> field idx in package
          begin
            L:= Length(CRI.RemapKeys[Byte(aFields.Objects[J])].PkgKeyToPkgFieldIdx);
            SetLength(CRI.RemapKeys[Byte(aFields.Objects[J])].PkgKeyToPkgFieldIdx, Length(CRI.RemapKeys[Byte(aFields.Objects[J])].PkgKeyToPkgFieldIdx)+1);
            if FType <> Byte(aFields.Objects[J]) then
              K:= MaxInt
            else
              K:= J;
            CRI.RemapKeys[Byte(aFields.Objects[J])].PkgKeyToPkgFieldIdx[Length(CRI.RemapKeys[Byte(aFields.Objects[J])].PkgKeyToPkgFieldIdx)-1]:= K;
          end;
        end;
      if FType in [Low(Cnt)..High(Cnt)] then   // primary a foreign
      begin   // p/f key pos in config db --> pos in offline key
        SetLength(CRI.RemapKeys[FType].CfgKeyToPkgKey, Length(CRI.RemapKeys[FType].CfgKeyToPkgKey)+1);
        CRI.RemapKeys[FType].CfgKeyToPkgKey[Length(CRI.RemapKeys[FType].CfgKeyToPkgKey)-1]:= L;
      end;
    end;
  end;

  function RemapKey(CRI: TReplCachedRelationItem; const aKey: string; FType: Integer; aSep: Char): TStringOpenArray;
  var
    I, J, K: Integer;
  begin
    SetLength(Result, 0);
    for I:= 0 to Length(CRI.RemapKeys[FType].CfgKeyToPkgKey)-1 do
    begin
      J:= CRI.RemapKeys[FType].CfgKeyToPkgKey[I];
      if J >= 0 then  // does exist in package?
      begin
        K:= 1;
        while (J > 0) and (K <= Length(aKey)) do
        begin
          if aKey[K] = aSep then
            Dec(J);
          Inc(K);
        end;
        J:= 0;
        while (K+J <= Length(aKey)) and (aKey[K+J] <> aSep) do
          Inc(J);
        SetLength(Result, Length(Result)+1);
        Result[Length(Result)-1]:= Copy(aKey, K, J);
      end;
    end;
  end;

  procedure LogSeqId(aFlag: Byte; aSeqId: Integer; const aReasonStr: string; aConflict: TStream; var Cnt: Longint);
  var
    RAR: TOffTgtReplAckRec;
  begin
    RAR.Flag:= aFlag;
    if aReasonStr <> '' then
      RAR.Flag:= RAR.Flag or ackflReasonStr;
    if (aConflict <> nil) and (aConflict.Size > 0) then
      RAR.Flag:= RAR.Flag or ackflConflict;
    RAR.SeqId:= aSeqId;
    Tgt.WriteBuffer(RAR, SizeOf(RAR));
    if aReasonStr <> '' then
    begin
      PutOffReplString(Tgt, aReasonStr, 2, True, True);
    end;
    if RAR.Flag and ackflConflict <> 0 then
    begin
      aConflict.Position:= 0;
      Tgt.CopyFrom(aConflict, aConflict.Size);
    end;
    Inc(Cnt);
  end;

  procedure GetValue(Src: TStream; Idx: Integer; const aSQRecord: TReplOffRecordBuffer);
  var
    StPos: LongInt;
    L: LongWord;
  begin
    L:= GetOffReplValueLength(Src, RL.Options and optUTF8 <> 0);
    StPos:= Src.Position;
    if Idx < Length(CRI.RemapFields) then   // was not defined in offline definition, so omit
    begin
      // remap layout
      if CRI.RemapFields[Idx] >= 0 then // only meaningful field values
      begin
        aSQRecord.Pos[CRI.RemapFields[Idx]]:= aSQRecord.Buffer.Position;
        aSQRecord.Buffer.CopyFrom(Src, L);
      end;
    end;
    Src.Position:= Longword(StPos)+L;  // shift even no field is read
  end;

  procedure ReadValBuf(var aRec: TReplOffRecordBuffer; aKey: Byte);
  var
    I, J: Integer;
    RR: TOffSrcRelRec;
  begin
    aRec.Buffer:= TMemoryStream.Create;
    SetLength(aRec.Pos, CRI.Fields.Count);
    for I:= 0 to Length(aRec.Pos)-1 do
      aRec.Pos[I]:= -1;
    Src.ReadBuffer(RR, SizeOf(RR));
    for I:= 0 to RR.FieldCount-1 do
    begin
      if aKey > 0 then
        J:= CRI.RemapKeys[aKey].PkgKeyToPkgFieldIdx[I]  // it's MaxInt filled former if key types do not correspond
      else
        J:= I;
      GetValue(Src, J, aRec);
    end;
  end;

  procedure ReadExtBuf();
  var
    I: Integer;
    RR: TOffSrcExternalRec;
    RRV: TOffSrcExternalValue;
  begin
    Src.ReadBuffer(RR, SizeOf(RR));
    SetLength(ReplLogRec.Externals, RR.FieldCount);
    for I:= 0 to RR.FieldCount-1 do
    begin
      Src.ReadBuffer(RRV, SizeOf(RRV));
      ReplLogRec.Externals[I].Idx:= -1;
      ReplLogRec.Externals[I].FileName:= GetOffReplString(Src, SizeOf(Byte), True, RL.Options and optUTF8 <> 0);
      if RRV.Idx < Length(CRI.RemapFields) then
      begin
        if CRI.RemapFields[RRV.Idx] >= 0 then
          ReplLogRec.Externals[I].Idx:= CRI.RemapFields[RRV.Idx];
      end;
      ReplLogRec.Externals[I].Stamp:= OffStamp2DT(RRV.Stamp);
      ReplLogRec.Externals[I].DataSize:= RRV.Size;
      ReplLogRec.Externals[I].DataPos:= Src.Position;
      if RRV.Options and optextData <> 0 then
        ReplLogRec.Externals[I].Data:= Src
      else
        ReplLogRec.Externals[I].Data:= nil;
      Src.Seek(RRV.Size, soFromCurrent);
    end;
  end;

var
  TIdForPass4: string;
  LogDesc: string;
  LogAct: Byte;
  LogConf: TStream;
begin
  Result:= 0;
  aStamp:= Now2();
  Src.ReadBuffer(H1, SizeOf(H1));
  CheckMagic2(H1.Magic);
  if (H1.SchemaId <> aSchemaId) or (H1.GroupId <> aGroupId) or (H1.SrcDBId <> aSrcDBId) or (H1.TgtDBId <> aTgtDBId) or (H1.Status <> 'S') then
    IBReplicatorError(sBadOffPackage, 1);
  CheckCRC(H1, Src);
  FillChar(H2, SizeOf(H2), 0);
  FillMagic(H2.Magic, offVersionV2);
  H2.SchemaId:= aSchemaId;
  H2.SrcDBId:= aSrcDBId;
  H2.TgtDBId:= aTgtDBId;
  H2.GroupId:= aGroupId;
  H2.Stamp:= DT2OffStamp(aStamp);
  H2.Status:= 'T';
  HeaderPos:= Tgt.Position;
  Q:= TIBSQL.Create(nil);
  try
    Q.Database:= aDB;
    Q.Transaction:= TIBTransaction.Create(Q);
    Q.Transaction.Params.Text:= tranRW;
    Q.Transaction.DefaultDatabase:= Q.Database;
    SafeStartTransaction(Q.Transaction);
    try
      ClearCachedValues(True);
      Cnt:= 0;
      Cnt3:= 0;
      Cnt4:= 0;
      CntManI:= 0; CntReplD:= 0;
      RA.RecType:= offrtReplAck;
      RA.TransferIdAck:= H1.TransferId;
      RA.StampRec:= DT2OffStamp(aStampRec);
      RA.StampProc:= DT2OffStamp(aStamp);
      RA.ProcCount:= 0;
      Tgt.WriteBuffer(H2, SizeOf(H2));
      Tgt.WriteBuffer(RA, SizeOf(RA));

      FillChar(TgtDB, SizeOf(TgtDB), 0);
      DBSQLExec(fConfigDatabase, Format(DeleteStatS, [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'RELATIONS')])+' AND SCHEMAID='+IntToStr(H1.SchemaId));
      DBSQLExec(fConfigDatabase, Format(DeleteStatS, [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'SCHEMATA')])+' AND SCHEMAID='+IntToStr(H1.SchemaId));
      try
        TickCount2:= GetTickCount();

        while (Src.Position < Src.Size) and not ForceStop do
        begin
          DoUpdateStatus(GetTickCount()-TickCount2, Cnt);
          Src.ReadBuffer(B, SizeOf(B));
          Src.Position:= Src.Position-SizeOf(B);
          case B of
            offrtRelationDef:
              begin
                Src.ReadBuffer(RD, SizeOf(RD));
                Sg:= TStringList.Create;
                try
                  for I:= 0 to RD.Count-1 do
                  begin
                    Src.ReadBuffer(RDF, SizeOf(RDF));
                    Sg.AddObject(GetS(RDF.Name), Pointer(RDF.FieldType));
                  end;
                  if ReadCachedFields(H1.SchemaId, H1.GroupId, RD.RelationId, True, CRI) then
                  begin
                    CheckMapping(CRI, GetS(RD.Name), RD.RelationId, Sg);
                  end;
                finally
                  Sg.Free;
                end;
              end;
            offrtReplLog:
              begin
                Src.ReadBuffer(RL, SizeOf(RL));
                OldPKey:= GetOffReplString(Src, SizeOf(Word), True, RL.Options and optUTF8 <> 0);
                NewPKey:= GetOffReplString(Src, SizeOf(Word), True, RL.Options and optUTF8 <> 0);
                NewFKey:= GetOffReplString(Src, SizeOf(Word), True, RL.Options and optUTF8 <> 0);
                if not ReadCachedFields(H1.SchemaId, H1.GroupId, RL.RelationId, True, CRI) then
                  begin
                    fDBLog.Log(fLogName, lchError, Format(sOfflineRelationNotDefined2, [RL.RelationId]));
                    LogSeqId(ackflManual, RL.SeqId, '', nil, RA.ProcCount);
                    Inc(CntManI);
                  end
                else if CRI.OffRelationName = '' then
                  begin
                    fDBLog.Log(fLogName, lchError, Format(sOfflineRelationNotDefined, [RL.RelationId]));
                    LogSeqId(ackflManual, RL.SeqId, '', nil, RA.ProcCount);
                    Inc(CntManI);
                  end
                else
                  begin
                    ReplLogRec:= TReplLogRecord_Offline.Create(Self);
                    try
                      ReplLogRec.OldPKey:= RemapKey(CRI, OldPKey, 1, RL.Sep);
                      ReplLogRec.NewPKey:= RemapKey(CRI, NewPKey, 1, RL.Sep);
                      ReplLogRec.NewFKey:= RemapKey(CRI, NewFKey, 2, RL.Sep);
                      if RL.Options and optIsForeignRecord <> 0 then
                      begin
                        ReadValBuf(ReplLogRec.FNewQRecord, 2);
                        ReadValBuf(ReplLogRec.FOldQRecord, 2);
                        ReplLogRec.FNewQRecord.UTF8:= RL.Options and optUTF8 <> 0;
                        ReplLogRec.FOldQRecord.UTF8:= RL.Options and optUTF8 <> 0;
                      end;
                      if RL.Options and optIsRelRecord <> 0 then
                      begin
                        ReadValBuf(ReplLogRec.SQRecord, 0);
                        ReplLogRec.SQRecord.UTF8:= RL.Options and optUTF8 <> 0;
                      end;
                      if RL.Options and optIsExternalRecord <> 0 then
                      begin
                        ReadExtBuf();
                      end;
                      try
                        if TgtDB.Fl = 0 then
                        begin
                          TgtDB.Tgt:= GetReplTargetDatabase(H1.SchemaId, H1.GroupId, 'DBID', aTgtDBId);
                          if TgtDB.Tgt <> nil then
                            TgtDB.Fl:= 1
                          else
                            TgtDB.Fl:= 2;
                        end;
                        if TgtDB.Fl = 1 then  { succesufully connected }
                        begin
                          ReplLogRec.CRI:= CRI;
                          ReplLogRec.FieldDefs.BuildFieldDefs(CRI, 3{dummy}, TgtDB.Tgt.SQLDialect);
                          ReplLogRec.Assign(H1, RL);
                          ReplLogRec.FieldDefs.GetWhereValues(3{dummy}, TgtDB.Tgt.SQLDialect, RL.RepType, ReplLogRec.OldPKey, ReplLogRec.NewPKey, ReplLogRec.FieldDefs.Where_Src, ReplLogRec.FieldDefs.Where_Tgt, ReplLogRec.FieldDefs.Where_Tgt_New);

                          with TgtDB do
                            case RL.RepType of
                              'I': Inc(CntI);
                              'U': Inc(CntU);
                              'D': Inc(CntD);
                            end;
                          TickCount:= GetTickCount(); StatS:= '';
                          try
                            LogDesc:= '';
                            LogAct:= 0;
                            LogConf:= TMemoryStream.Create;
                            try
                              try
                                TgtDB.Tgt.ReplicateRecord(ReplLogRec, aReplOptions, LogConf, TgtDB.Counter);

                                case RL.RepType of
                                  'I':
                                    begin
                                      StatS:= 'S_INSERT';
                                      Inc(TgtDb.Cnt2I);
                                    end;
                                  'U':
                                    begin
                                      StatS:= 'S_UPDATE';
                                      Inc(TgtDb.Cnt2U);
                                    end;
                                  'D':
                                    begin
                                      StatS:= 'S_DELETE';
                                      Inc(TgtDb.Cnt2D);
                                    end;
                                end;
                              except
                                on E: Exception do
                                begin
                                  if E is EIBReplicatorError then  { replication conflict - log to manual log }
                                    begin
                                      if EIBReplicatorError(E).Kind = 1 then
                                        StatS:= 'S_CONFLICT'
                                      else
                                        StatS:= 'S_ERROR';
                                      if EIBReplicatorError(E).Kind > 0 then
                                      begin
                                        LogDesc:= E.Message;
                                        LogAct:= ackflManual;
                                        Inc(CntManI);
                                      end;
                                      fDBLog.Log(fLogName, lchError, E.Message);
                                    end
                                  else if CheckIBConnectionError(E, TIBDatabase(TgtDB.Tgt)) then
                                    begin
                                      TgtDB.Fl:= 2;  // do not replicate if connection has been broken
                                      raise;
                                    end
                                  else
                                    begin
                                      StatS:= 'S_ERROR';
                                      raise;
                                    end;
                                end;
                              end;
                              LogSeqId(LogAct, RL.SeqId, LogDesc, LogConf, RA.ProcCount); // delete from log
                              if LogAct = 0 then
                                Inc(CntReplD);
                            finally
                              LogConf.Free;
                            end;
                          finally
                            if StatS <> '' then
                              StatS:= StatS+'='+Stats+'+1,'; // possible integer overflow
                            try
                              DBSQLExec(fConfigDatabase, Format(UpdateStatS+' AND GROUPID=%d AND RELATIONID=%d', [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'RELATIONS'), StatS, Abs(GetTickCount()-TickCount), H1.SchemaId, H1.GroupId, RL.RelationId]));
                            except
                              fDBLog.Log(fLogName, lchError, Format(sUpdateStatRelErr, [H1.SchemaId, H1.GroupId, RL.RelationId]));
                            end;
                            try
                              DBSQLExec(fConfigDatabase, Format(UpdateStatS, [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'SCHEMATA'), StatS, Abs(GetTickCount()-TickCount), H1.SchemaId]));
                            except
                              fDBLog.Log(fLogName, lchError, Format(sUpdateStatSchErr, [H1.SchemaId]));
                            end;
                          end;
                        end;

                      except
                        on E: Exception do
                        begin
                          if CheckIBConnectionError(E, TIBDatabase(TgtDB.Tgt)) then
                            begin
                              TgtDB.Fl:= 2;  // do not replicate if connection has been broken
                              raise;
                            end
                          else if E is EIBReplicatorError then
                            fReplLog.Log(fLogName, lchError, E.Message)
                          else
                            fDBLog.Log(fLogName, lchError, Format('%s %s #%d: %s', [CRI.Name, RL.RepType, RL.SeqId, E.Message]));
                        end;
                      end;
                    finally
                      ReplLogRec.Free;
                    end;
                  end;
                Inc(Cnt);
              end;
            offrrtReplAckAck:
              begin
                Src.ReadBuffer(RAA, SizeOf(RAA));
                DBSQLExec(Q.Database, Format('UPDATE '+FormatIdentifier(Q.Database.SQLDialect, aDBProps.ObjPrefix+'TRANSFER')+' SET TRANSFERID_ACK=%d,STAMP_REC=%s,STAMP_PROC=%s WHERE SCHEMAID=%d AND STATUS=''T'' AND TRANSFERID=%d AND GROUPID=%d AND DBID=%d', [H1.TransferId,DT2IBDT(OffStamp2DT(RAA.StampRec)), DT2IBDT(OffStamp2DT(RAA.StampProc)), H1.SchemaId, RAA.TransferIdAck, H1.GroupId, H1.TgtDbId]), Q.Transaction);
                if TIdForPass4 <> '' then
                  TIdForPass4:= TIdForPass4+',';
                TIdForPass4:= TIdForPass4+IntToStr(RAA.TransferIdAck);
                DeleteFile(aOfflineDir+GetOfflinePackageName(H1.SchemaId, 'T', H1.GroupId, H1.SrcDbId, H1.TgtDBId, RAA.TransferIdAck));

                DBSQLExec(Q.Database, Format('UPDATE '+FormatIdentifier(Q.Database.SQLDialect, aDBProps.ObjPrefix+'TRANSFER')+' SET STAMP_SENT=%s WHERE SCHEMAID=%d AND STATUS=''S'' AND TRANSFERID=%d AND GROUPID=%d AND DBID=%d', [DT2IBDT(OffStamp2DT(RAA.StampSent)), H1.SchemaId, RAA.TransferId, H1.GroupId, H1.TgtDbId]), Q.Transaction);
                SafeCommit(Q.Transaction, True);
                Inc(Cnt3);
              end;
            offrReplResend:
              begin
                Src.ReadBuffer(RRS, SizeOf(RRS));
                DBSQLExec(Q.Database, Format('UPDATE '+FormatIdentifier(Q.Database.SQLDialect, aDBProps.ObjPrefix+'TRANSFER')+' SET STAMP_REC=%s,STAMP_PROC=%s,TRANSFERID_ACK=%s WHERE SCHEMAID=%d AND STATUS=''T'' AND TRANSFERID=%d AND GROUPID=%d AND DBID=%d', [DT2IBDT(OffStamp2DT(RRS.StampRec)), DT2IBDT(OffStamp2DT(RRS.StampProc)), ZeroAsNull(RRS.TransferIdAck), H1.SchemaId, RRS.TransferId, H1.GroupId, H1.TgtDbId]));
                if OffStamp2DT(RRS.StampRec) > 0 then
                  DeleteFile(aOfflineDir+GetOfflinePackageName(H1.SchemaId, 'T', H1.GroupId, H1.SrcDbId, H1.TgtDBId, RRS.TransferId));
                Inc(Cnt4);
              end;
          else
            IBReplicatorError(sBadOffPackage, 1);
          end;
          ProcessMessages;
        end;

        if RA.ProcCount > 0 then
        begin
          Tgt.Position:= HeaderPos+SizeOf(TOffHeader);
          Tgt.WriteBuffer(RA, SizeOf(RA));
          Tgt.Position:= Tgt.Size;
        end;
      finally
        with TgtDb do
        begin
          if Fl > 0 then
            fReplLog.Log(fLogName, lchNull, Format(sReplCount, [Tgt.DBProps.DBName, Cnt2I, CntI, Cnt2U, CntU, Cnt2D, CntD]));
          Tgt.Free;
        end;
      end;
      fReplLog.Log(fLogName, lchNull, Format(sReplOffTotal2, [Cnt, CntReplD, CntManI, Cnt3, Cnt4]));

      SafeClose(Q);  // send when was sent unacknowledged (CZ: odeslani kdy bylo odeslano u nepotvrzenych)
      if TIdForPass4 = '' then
        TIdForPass4:= '0'{dummy};
      Q.SQL.Text:= Format('SELECT TRANSFERID,STAMP_SENT FROM '+FormatIdentifier(Q.Database.SQLDialect, aDBProps.ObjPrefix+'TRANSFER')+' WHERE STATUS=''T'' AND (TRANSFERID_ACK IS NULL OR TRANSFERID IN (%s)) AND SCHEMAID=%d AND GROUPID=%d AND DBID=%d', [TIdForPass4, H1.SchemaId, H1.GroupId, H1.TgtDBId]);
      SafeExecQuery(Q);
      Cnt3:= 0;
      if not Q.EOF then
      begin
        while not Q.EOF and not ForceStop do
        begin
          RAAA.RecType:= offrrtReplAckAckAck;
          RAAA.TransferIdAck:= Q.FieldByName('TRANSFERID').asInteger;
          RAAA.StampSent:= DT2OffStamp(Q.FieldByName('STAMP_SENT').asDateTime);    // flag was filled after creating of package, so I'll send it in addition
          Tgt.WriteBuffer(RAAA, SizeOf(RAAA));
          SafeNext(Q);
          Inc(Cnt3);
        end;
      end;
      fReplLog.Log(fLogName, lchNull, Format(sReplOffTotal3, [Cnt3]));

      SafeClose(Q);
      { process rereceived packages, probably sent by source because not acknowledged (and not processed REPL$LOG) }
      Q.SQL.Text:= Format('SELECT * FROM '+FormatIdentifier(Q.Database.SQLDialect, aDBProps.ObjPrefix+'TRANSFER')+' T WHERE T.STATUS=''S'' AND T.SCHEMAID=%d AND T.GROUPID=%d AND T.DBID=%d AND RESEND_FLAG=''Y''', [aSchemaId, aGroupId, aTgtDBId]);
      SafeExecQuery(Q);
      Cnt3:= 0;
      if not Q.EOF then
      begin
        while not Q.EOF and not ForceStop do
        begin
          RRS.RecType:= offrReplResend;
          RRS.TransferId:= Q.FieldByName('TRANSFERID').asInteger;
          RRS.TransferIdAck:= Q.FieldByName('TRANSFERID_ACK').asInteger;
          RRS.StampRec:= DT2OffStamp(Q.FieldByName('STAMP_REC').asDateTime);
          RRS.StampProc:= DT2OffStamp(Q.FieldByName('STAMP_PROC').asDateTime);
          Tgt.WriteBuffer(RRS, SizeOf(RRS));
          DBSQLExec(Q.Database, Format('UPDATE '+FormatIdentifier(Q.Database.SQLDialect, aDBProps.ObjPrefix+'TRANSFER')+' SET RESEND_FLAG=''N'' WHERE SCHEMAID=%d AND STATUS=''S'' AND TRANSFERID=%d AND GROUPID=%d AND DBID=%d', [aSchemaId, Q.FieldByName('TRANSFERID').asInteger, aGroupId, aTgtDbId]), Q.Transaction);
          SafeNext(Q);
          Inc(Cnt3);
        end;
      end;
      fReplLog.Log(fLogName, lchNull, Format(sReplOffTotal4, [Cnt3]));

      GenTransferId(Q.Database, H2.TransferId, aDBProps, Q.Transaction);

      Tgt.Position:= HeaderPos;
      H2.CRC:= CalcCRC(Tgt, True);
      Tgt.WriteBuffer(H2, SizeOf(H2));
      Tgt.Position:= Tgt.Size;
      DBSQLExec(Q.Database, Format('INSERT INTO '+FormatIdentifier(Q.Database.SQLDialect, aDBProps.ObjPrefix+'TRANSFER')+'(TRANSFERID,STATUS,SCHEMAID,GROUPID,DBID,STAMP) VALUES (%d,''T'',%d,%d,%d,%s)', [H2.TransferId, H2.SchemaId, H2.GroupId, H2.TgtDBId, DT2IBDT(aStamp)]), Q.Transaction);
      Result:= H2.TransferId;
    finally
      SafeCommit(Q.Transaction);
    end;
  finally
    Q.Free;
  end;
end;

procedure TIBReplicator.ProcessTargetOfflinePackage;
var
  Q: TIBSQL;
  LogDeleteSP: TIBStoredProc;
  DBMask: Integer;
  Cnt1, Cnt2, Cnt3, Cnt4, I, J, LogAct: Integer;
  H: TOffHeader;
  RA: TOffTgtReplAck;
  RAC: TOffTgtReplAckRec;
  RAAA: TOffTgtReplAckAckAck;
  RACR: TOffTgtReplAckConflictRec;
  RRS: TOffReplResend;
  B: Byte;
  LogConf: TStream;
  S, ReasonS: string;
  L: LongInt;
begin
  Src.ReadBuffer(H, SizeOf(H));
  CheckMagic2(H.Magic);
  if (H.SchemaId <> aSchemaId) or (H.GroupId <> aGroupId) or (H.TgtDBId <> aTgtDBId) or (H.SrcDBId <> aSrcDBId) or (H.TransferId <> aTransferId) or (H.Status <> 'T') then
    IBReplicatorError(sBadOffPackage, 1);
  CheckCRC(H, Src);
  Q:= TIBSQL.Create(nil);
  try
    DBMask:= GetDBMask(H.SchemaId, H.GroupId, H.TgtDBId);
    if DBMask = -1 then
      IBReplicatorError(sBadOffPackage, 1);

    Q.Database:= aDB;
    Q.Transaction:= TIBTransaction.Create(Q);
    Q.Transaction.Params.Text:= tranRW;
    Q.Transaction.DefaultDatabase:= Q.Database;
    LogDeleteSP:= TIBStoredProc.Create(nil);
    try
      SafeStartTransaction(Q.Transaction);
      LogDeleteSP.Database:= Q.Database;
      LogDeleteSP.Transaction:= Q.Transaction;
      LogDeleteSP.StoredProcName:= aDBProps.ObjPrefix+'LOG_DELETE';
      LogDeleteSP.Prepare;

      try
        Cnt1:= 0; Cnt2:= 0; Cnt3:= 0; Cnt4:= 0;

        while Src.Position < Src.Size do
        begin
          Src.ReadBuffer(B, SizeOf(B));
          Src.Position:= Src.Position-SizeOf(B);
          case B of
            offrtReplAck:
              begin
                Src.ReadBuffer(RA, SizeOf(RA));
                DBSQLExec(Q.Database, Format('UPDATE '+FormatIdentifier(Q.Database.SQLDialect, aDBProps.ObjPrefix+'TRANSFER')+' SET TRANSFERID_ACK=%d,STAMP_REC=%s,STAMP_PROC=%s WHERE SCHEMAID=%d AND STATUS=''S'' AND TRANSFERID=%d', [H.TransferId, DT2IBDT(OffStamp2DT(RA.StampRec)), DT2IBDT(OffStamp2DT(RA.StampProc)), H.SchemaId, RA.TransferIdAck]));
                DeleteFile(aOfflineDir+GetOfflinePackageName(H.SchemaId, 'S', H.GroupId, H.SrcDbId, H.TgtDBId, RA.TransferIdAck));
                Inc(Cnt1);
                for I:= 0 to RA.ProcCount-1 do
                begin
                  Src.ReadBuffer(RAC, SizeOf(RAC));
                  ReasonS:= '';
                  if RAC.Flag and ackflReasonStr <> 0 then
                  begin
                    ReasonS:= GetOffReplString(Src, 2, True, True);
                  end;
                  if (RAC.Flag and ackflManual <> 0) and (aReplOptions and repoptReportToSource <> 0) then  // to man log
                    LogAct:= 2
                  else
                    LogAct:= 0;
                  LogConf:= nil;
                  try
                    if RAC.Flag and ackflConflict <> 0 then
                    begin
                      LogConf:= TMemoryStream.Create;
                      Src.ReadBuffer(RACR, SizeOf(RACR));
                      LogConf.WriteBuffer(RACR, SizeOf(RACR));
                      S:= GetOffReplString(Src, 1, False, False);
                      PutOffReplString(LogConf, S, 1, False, fUTF8);
                      for J:= 0 to RACR.Count-1 do
                      begin
                        S:= GetOffReplString(Src, 1, False, False);
                        PutOffReplString(LogConf, S, 1, False, fUTF8);

                        L:= GetOffReplValueLength(Src, True);
                        LogConf.CopyFrom(Src, L);

                        S:= GetOffReplString(Src, 1, False, False);
                        PutOffReplString(LogConf, S, 1, False, fUTF8);

                        L:= GetOffReplValueLength(Src, True);
                        LogConf.CopyFrom(Src, L);
                      end;
                    end;
                    LogDeleteSP.ParamByName('SCHEMAID').asInteger:= aSchemaId;
                    LogDeleteSP.ParamByName('SEQID').asInteger:= RAC.SeqId;
                    LogDeleteSP.ParamByName('DBMASK').asInteger:= DBMask;
                    LogDeleteSP.ParamByName('ACTIONTYPE').asInteger:= LogAct;
                    FillLogDeleteDesc(LogDeleteSP, ReasonS, LogConf);
                    SafeExecProc(LogDeleteSP);
                    SafeCommit(LogDeleteSP.Transaction, True);
                  finally
                    LogConf.Free;
                  end;
                  Inc(Cnt2);
                end;
              end;
            offrrtReplAckAckAck:
              begin
                Src.ReadBuffer(RAAA, SizeOf(RAAA));
                DBSQLExec(Q.Database, Format('UPDATE '+FormatIdentifier(Q.Database.SQLDialect, aDBProps.ObjPrefix+'TRANSFER')+' SET STAMP_SENT=%s WHERE SCHEMAID=%d AND STATUS=''T'' AND TRANSFERID=%d AND GROUPID=%d AND DBID=%d', [DT2IBDT(OffStamp2DT(RAAA.StampSent)), H.SchemaId, RAAA.TransferIdAck, H.GroupId, H.TgtDbId]));
                Inc(Cnt3);
              end;
            offrReplResend:
              begin
                Src.ReadBuffer(RRS, SizeOf(RRS));
                DBSQLExec(Q.Database, Format('UPDATE '+FormatIdentifier(Q.Database.SQLDialect, aDBProps.ObjPrefix+'TRANSFER')+' SET STAMP_REC=%s,STAMP_PROC=%s,TRANSFERID_ACK=%s WHERE SCHEMAID=%d AND STATUS=''S'' AND TRANSFERID=%d AND GROUPID=%d AND DBID=%d', [DT2IBDT(OffStamp2DT(RRS.StampRec)), DT2IBDT(OffStamp2DT(RRS.StampProc)), ZeroAsNull(RRS.TransferIdAck), H.SchemaId, RRS.TransferId, H.GroupId, H.TgtDbId]));
                if OffStamp2DT(RRS.StampRec) > 0 then
                  DeleteFile(aOfflineDir+GetOfflinePackageName(H.SchemaId, 'S', H.GroupId, H.SrcDbId, H.TgtDBId, RRS.TransferId));
                Inc(Cnt4);
              end;
          else
            IBReplicatorError(sBadOffPackage, 1);
          end;
        end;
        fReplLog.Log(fLogName, lchNull, Format(sReplOffAckTotal, [Cnt1, Cnt2, Cnt3, Cnt4]));
      finally
        SafeCommit(Q.Transaction); { no changes made in the transaction }
      end;
    finally
      LogDeleteSP.Free;
    end;
  finally
    Q.Free;
  end;
end;

function TIBReplicator.GetOfflinePackageName;
var
  S: string;
begin
  S:= '';
  if aSchemaId <> 0 then
  begin
    S:= S+'%.5d_%s_';
    if aGroupId <> 0 then
    begin
      S:= S+'%.5d_';
      if aTgtDBId <> 0 then
      begin
        S:= S+'%.5d_';
        if aTransferId <> 0 then
          S:= S+'%.10d';
      end;
    end;
  end;
  Result:= LowerCase(Format(S, [aSchemaId, aStatus, aGroupId, aTgtDBId, aTransferId]));
end;

procedure TIBReplicator.SendPendingPackages(aDB: TIBDatabase; const aDBProps: TReplDatabaseProperties; aSchemaId: Integer; aStatus: Char; aGroupId, aSrcDbId, aTgtDBId: Integer; const aOfflineDir: string; aResendData: Boolean);
var
  Q: TIBSQL;
  LibHandle, EncHandle: THandle;
  H: TTransferHandle;
  S, S2, S3, FileN, Enc, Params, ResendS: string;
  V, V2: Variant;
  St, St2: TStream;
  I, J: Integer;
  Buff: array[1..255] of Char;
resourcestring
  sSending = 'Sending package: "%s"';
  sResending = 'Resending package: "%s"';
  sEncoding = 'Encoding: %s';
  sCopying = 'Copying';
  sOpening = 'Session opening';
begin
  try
    BuildImplicitEnvironment(['SCHEMAID','GROUPID','DBID','DB2ID','OFFDIR'], [IntToStr(aSchemaId),IntToStr(aGroupId),IntToStr(aSrcDBId),IntToStr(aTgtDBId), aStatus]);
    V:= DBSQLRecord(fConfigDatabase, Format('SELECT OFFTRANSFER,OFFPARAMS,OFFENCODER FROM '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'SCHEMADB')+' WHERE SCHEMAID=%d AND OFFTRANSFER IS NOT NULL AND GROUPID=%d AND DBID=%d', [aSchemaId, aGroupId, aTgtDBId]));
    if VarIsEmpty(V) then
      IBReplicatorError(Format(sNoOfflineMethod, [aSchemaId, aTgtDBId]), 1);
    Q:= TIBSQL.Create(nil);
    try
      Q.Database:= aDB;
      Q.Transaction:= TIBTransaction.Create(Q);
      Q.Transaction.Params.Text:= tranRW;
      Q.Transaction.DefaultDatabase:= Q.Database;
      SafeStartTransaction(Q.Transaction);
      try
        ResendS:= '';
        if aResendData then
        begin
          V2:= DBSQLRecord(Q.Database, Format('SELECT MAX(STAMP_SENT) FROM '+FormatIdentifier(Q.Database.SQLDialect, aDBProps.ObjPrefix+'TRANSFER')+' WHERE SCHEMAID=%d AND STATUS=''%s'' AND GROUPID=%d AND DBID=%d AND STAMP_SENT IS NOT NULL AND STAMP_REC IS NOT NULL', [aSchemaId, aStatus, aGroupId, aTgtDBId]), Q.Transaction);
          if not VarIsNull(V2) then
            ResendS:= Format('OR (STAMP_REC IS NULL AND STAMP_SENT<%s)', [DT2IBDT(VarToDateTime(V2))]);
              { resend potentioly lost/corrupted packeges unreceived (older then last successfuly received package) }
        end;

        Q.SQL.Text:= Format('SELECT * FROM '+FormatIdentifier(Q.Database.SQLDialect, aDBProps.ObjPrefix+'TRANSFER')+' WHERE SCHEMAID=%d AND STATUS=''%s'' AND GROUPID=%d AND DBID=%d AND (STAMP_SENT IS NULL %s) ORDER BY TRANSFERID /*STAMP..not GMT*/', [aSchemaId, aStatus, aGroupId, aTgtDBId, ResendS]);
        SafeExecQuery(Q);
        if not Q.EOF then
        begin
          V[0]:= ParseStr(V[0], aDB, aDBProps.ObjPrefix);
          S:= Transfer.DllPrefix+V[0]+Transfer.DllVer+Transfer.DllExt;
          fDBLog.Log(fLogName, lchNull, Format(sLoadTransferLibrary, [string(V[0])]));
          LibHandle:= LoadLibrary(PChar(S));
          if LibHandle <> 0 then
            begin
              try
                Params:= ParseParams(V[1], aDB, aDBProps.ObjPrefix);
                H:= nil;
                try
                  fDBLog.Log(fLogName, lchNull, sOpening);
                  CheckTransferError(LibHandle, H, tTransferOpen(GetProcAddress(LibHandle, 'TransferOpen'))(aStatus='T', PChar(Params), H));
                  I:= 0;
                  while not Q.EOF and not ForceStop do
                  begin
                    S:= GetOfflinePackageName(aSchemaId, aStatus, aGroupId, aSrcDbId, aTgtDBId, Q.FieldByName('TRANSFERID').asInteger);
                    FileN:= aOfflineDir + S;
                    if Q.FieldByName('STAMP_SENT').isNull then
                      S2:= sSending
                    else
                      S2:= sResending;

                    fDBLog.Log(fLogName, lchNull, Format(S2, [S]));
                    try
                      St:= TFileStream.Create(FileN, fmOpenRead or fmShareDenyWrite);
                      try
                        if V[2]<>Null then
                          begin
                            V[2]:= ParseStr(V[2], aDB, aDBProps.ObjPrefix);
                            Enc:= V[2];
                          end
                        else
                          Enc:= '';
                        while Enc <> '' do
                        begin
                          J:= Pos(',', Enc+',');
                          S2:= Copy(Enc, 1, J-1);
                          Delete(Enc, 1, J);
                          if S2 <> '' then
                          begin
                            J:= Pos('/', S2+'/');
                            S3:= Encoder.DllPrefix+Copy(S2, 1, J-1)+Encoder.DllVer+Encoder.DllExt;
                            fDBLog.Log(fLogName, lchNull, Format(sEncoding, [S2]));
                            EncHandle:= LoadLibrary(PChar(S3));
                            if EncHandle <> 0 then
                              begin
                                try
                                  St2:= TMemoryStream.Create;
                                  try
                                    if tEncProcess(GetProcAddress(EncHandle, 'EncProcess'))(True, PChar(Copy(S2, J+1, Length(S2))), St, St2, PChar(Params), @Buff, SizeOf(BufF)) then
                                      begin
                                        FreeAndNil(St);
                                        St2.Position:= 0;
                                        St:= St2;
                                        St2:= nil;
                                      end
                                    else
                                      fDBLog.Log(fLogName, lchError, Format(sEncoderErr, [string(V[2]), string(PChar(@Buff))]));
                                  finally
                                    St2.Free;
                                  end;
                                finally
                                  FreeLibrary(EncHandle);
                                end;
                              end
                            else
                              fDBLog.Log(fLogName, lchError, Format(sCannotLoadEncoderLib, [string(V[2])]));
                          end;
                        end;
                        fDBLog.Log(fLogName, lchNull, sCopying);
                        CheckTransferError(LibHandle, H, tTransferSendPackage(GetProcAddress(LibHandle, 'TransferSendPackage'))(H, PChar(S), St));
                      finally
                        St.Free;
                      end;
                      Inc(I);
                      DBSQLExec(Q.Database, Format('UPDATE '+FormatIdentifier(Q.Database.SQLDialect, aDBProps.ObjPrefix+'TRANSFER')+' SET STAMP_SENT=%s,OFFTRANSFER=''%s'' WHERE SCHEMAID=%d AND STATUS=''%s'' AND GROUPID=%d AND DBID=%d AND TRANSFERID=%d', [DT2IBDT(Now2), string(V[0]), aSchemaId, aStatus, aGroupId, aTgtDBId, Q.FieldByName('TRANSFERID').asInteger]), Q.Transaction);
                      SafeCommit(Q.Transaction, True);
                    except
                      on E: Exception do
                      begin
                        if CheckIBConnectionError(E, Q.Database) then
                          raise;
                        fDBLog.Log(fLogName, lchError, E.Message);
                      end;
                    end;
                    SafeNext(Q);
                  end;
                  fReplLog.Log(fLogName, lchNull, Format(sTransferSent, [I, string(V[0])]));
                finally
                  if H <> nil then
                    tTransferClose(GetProcAddress(LibHandle, 'TransferClose'))(H);
                end;
              finally
                FreeLibrary(LibHandle);
              end;
            end
          else
            IBReplicatorError(Format(sCannotLoadTransferLib, [string(V[0])]), 1);
        end;
      finally
        SafeCommit(Q.Transaction);
      end;
    finally
      Q.Free;
    end;
  except
    on E: Exception do
      fDBLog.Log(fLogName, lchError, E.Message);
  end;
end;

function TIBReplicator.PrepareSourceOfflinePackageToFile;
var
  St: TMemoryStream;
  S: string;
resourcestring
  sPreparedS = 'Prepared package: "%s"';
begin
  St:= TMemoryStream.Create;  // I don't know file name
  try
    ForceDirectories(aOfflineDir);
    Result:= PrepareSourceOfflinePackage(aDB, aDBProps, aSchemaId, aGroupId, aSrcDbId, aTgtDBId, St, aIfReplRecord);
    if Result <> 0 then
    begin
      S:= GetOfflinePackageName(aSchemaId, 'S', aGroupId, aSrcDbId, aTgtDBId, Result);
      fDBLog.Log(fLogName, lchNull, Format(sPreparedS, [S]));
      St.SaveToFile(aOfflineDir + S);
    end;
  finally
    St.Free;
  end;
end;

function TIBReplicator.ProcessSourceOfflinePackageAndPrepareTargetOfflinePackageToFile;
var
  St: TMemoryStream;
  S: string;
resourcestring
  sPreparedT = 'Prepared package: "%s"';
begin
  St:= TMemoryStream.Create;  // I don't know file name
  try
    ForceDirectories(aOfflineDir);
    Result:= ProcessSourceOfflinePackageAndPrepareTargetOfflinePackage(aDB, aDBProps, aSchemaId, aGroupId, aSrcDBId, aTgtDBId, Src, St, aOfflineDir, aStampRec, aReplOptions, aStamp);
    if Result <> 0 then
    begin
      S:= GetOfflinePackageName(aSchemaId, 'T', aGroupId, aSrcDBId, aTgtDBId, Result);
      fDBLog.Log(fLogName, lchNull, Format(sPreparedT, [S]));
      St.SaveToFile(aOfflineDir + S);
    end;
  finally
    St.Free;
  end;
end;

procedure TIBReplicator.SourceOfflineBatch;
var
  Q: TIBSQL;
  SrcDBId: Integer;
  OffDir: string;
begin
{$IFDEF REGISTRATION}
  TReg_CheckRegistration(False);
{$ENDIF}
  Q:= TIBSQL.Create(nil);
  try
    Q.Database:= fConfigDatabase;
    Q.Transaction:= fConfigDatabaseTransaction;
    Q.SQL.Text:= 'SELECT SCHEMAID,GROUPID,DBID FROM '+FormatIdentifier(Q.Database.SQLDialect, fConfigDatabasePrefix+'SCHEMADB')+' WHERE GROUPID<>0 AND '+
      IntegerArrayToSQLCondition(aSchemaIds, 'SCHEMAID', '', ' AND ')+
      IntegerArrayToSQLCondition(aGroupIds, 'GROUPID', '', ' AND ')+
      IntegerArrayToSQLCondition(aTgtDBIds, 'DBID', '', ' AND ')+
                 'OFFTRANSFER IS NOT NULL';
    SafeStartTransaction(Q.Transaction);
    try
      SafeExecQuery(Q);
      if Q.EOF then
        fReplLog.Log(fLogName, lchNull, sNoOfflineSchema);
      while not Q.EOF and not ForceStop do
      begin
        SrcDbId:= GetSourceDBId(Q.FieldByName('SCHEMAID').asInteger);
        BuildImplicitEnvironment(['SCHEMAID','GROUPID','DBID','DB2ID','OFFDIR'], [IntToStr(Q.FieldByName('SCHEMAID').asInteger),IntToStr(Q.FieldByName('GROUPID').asInteger),IntToStr(SrcDBId),IntToStr(Q.FieldByName('DBID').asInteger), 'S']);
        OffDir:= {$IFDEF VER130}IncludeTrailingBackslash{$ELSE}IncludeTrailingPathDelimiter{$ENDIF}(ParseStr(aOfflineDir));

        if aReceiveData then
          ReceivePackages(aDB, aDBProps, Q.FieldByName('SCHEMAID').asInteger, 'T', Q.FieldByName('GROUPID').asInteger, SrcDbId, Q.FieldByName('DBID').asInteger, OffDir);
        if aProcessReceived then
          ProcessReceivedPackages(aDB, aDBProps, Q.FieldByName('SCHEMAID').asInteger, 'T', Q.FieldByName('GROUPID').asInteger, SrcDbId, Q.FieldByName('DBID').asInteger, OffDir, aReplOptions);
        if aProcess then
          PrepareSourceOfflinePackageToFile(aDB, aDBProps, Q.FieldByName('SCHEMAID').asInteger, Q.FieldByName('GROUPID').asInteger, SrcDbId, Q.FieldByName('DBID').asInteger, OffDir, True);
        if aSendData then
          SendPendingPackages(aDB, aDBProps, Q.FieldByName('SCHEMAID').asInteger, 'S', Q.FieldByName('GROUPID').asInteger, SrcDbId, Q.FieldByName('DBID').asInteger, OffDir, aResendData);
        SafeNext(Q);
      end;
    finally
      SafeCommit(Q.Transaction);
    end;
  finally
    Q.Free;
  end;
end;

procedure TIBReplicator.TargetOfflineBatch;
var
  Q: TIBSQL;
  SrcDBId: Integer;
  OffDir: string;
begin
{$IFDEF REGISTRATION}
  TReg_CheckRegistration(False);
{$ENDIF}
  Q:= TIBSQL.Create(nil);
  try
    Q.Database:= fConfigDatabase;
    Q.Transaction:= fConfigDatabaseTransaction;
    Q.SQL.Text:= 'SELECT SCHEMAID,GROUPID,DBID FROM '+FormatIdentifier(Q.Database.SQLDialect, fConfigDatabasePrefix+'SCHEMADB')+' WHERE GROUPID<>0 AND '+
      IntegerArrayToSQLCondition(aSchemaIds, 'SCHEMAID', '', ' AND ')+
      IntegerArrayToSQLCondition(aGroupIds, 'GROUPID', '', ' AND ')+
      IntegerArrayToSQLCondition(aTgtDBIds, 'DBID', '', ' AND ')+
                 'OFFTRANSFER IS NOT NULL';
    SafeStartTransaction(Q.Transaction);
    try
      SafeExecQuery(Q);
      if Q.EOF then
        fReplLog.Log(fLogName, lchNull, sNoOfflineSchema);
      while not Q.EOF and not ForceStop do
      begin
        SrcDbId:= GetSourceDBId(Q.FieldByName('SCHEMAID').asInteger);
        BuildImplicitEnvironment(['SCHEMAID','GROUPID','DBID','DB2ID','OFFDIR'], [IntToStr(Q.FieldByName('SCHEMAID').asInteger),IntToStr(Q.FieldByName('GROUPID').asInteger),IntToStr(SrcDBId),IntToStr(Q.FieldByName('DBID').asInteger),'T']);
        OffDir:= {$IFDEF VER130}IncludeTrailingBackslash{$ELSE}IncludeTrailingPathDelimiter{$ENDIF}(ParseStr(aOfflineDir));
        if aReceiveData then
          ReceivePackages(aDB, aDBProps, Q.FieldByName('SCHEMAID').asInteger, 'S', Q.FieldByName('GROUPID').asInteger, SrcDbId, Q.FieldByName('DBID').asInteger, OffDir);
        if aProcess then
          ProcessReceivedPackages(aDB, aDBProps, Q.FieldByName('SCHEMAID').asInteger, 'S', Q.FieldByName('GROUPID').asInteger, SrcDbId, Q.FieldByName('DBID').asInteger, OffDir, aReplOptions);
        if aSendData then
          SendPendingPackages(aDB, aDBProps, Q.FieldByName('SCHEMAID').asInteger, 'T', Q.FieldByName('GROUPID').asInteger, SrcDbId, Q.FieldByName('DBID').asInteger, OffDir, aResendData);
        SafeNext(Q);                                          
      end;
    finally
      SafeCommit(Q.Transaction);
    end;
  finally
    Q.Free;
  end;
end;

procedure TIBReplicator.DecodePackage;
var
  EncHandle: THandle;
  E: TOffEnvelope;
  St2: TStream;
  S2, Params: string;
  Buff: array[1..255] of Char;
resourcestring
  sDecoding = 'Decoding: %s/%s';
begin
  St:= Src;
  repeat
    St.Position:= 0;
    St.ReadBuffer(E.Magic, SizeOf(E.Magic));
    CheckMagic2(E.Magic);
    if E.Magic.Envelope = 1 then  // encoded
    begin
      St.Position:= 0;
      St.ReadBuffer(E, SizeOf(E));
      S2:= Encoder.DllPrefix+E.EncName+Encoder.DllVer+Encoder.DllExt;
      fDBLog.Log(fLogName, lchNull, Format(sDecoding, [E.EncName, E.SubType]));
      EncHandle:= LoadLibrary(PChar(S2));
      if EncHandle <> 0 then
        begin
          try
            St2:= TMemoryStream.Create;
            try
              S2:= E.SubType;
              if tEncProcess(GetProcAddress(EncHandle, 'EncProcess'))(False, PChar(S2), St, St2, PChar(Params), @Buff, SizeOf(BufF)) then
                begin
                  if St <> Src then
                    FreeAndNil(St);
                  St2.Position:= 0;
                  St:= St2;
                  St2:= nil;
                end
              else
                IBReplicatorError(Format(sEncoderErr, [E.EncName, string(PChar(@Buff))]), 1);
            finally
              St2.Free;
            end;
          finally
            FreeLibrary(EncHandle);
          end;
        end
      else
        IBReplicatorError(Format(sCannotLoadEncoderLib, [E.EncName]), 1);
    end;
  until E.Magic.Envelope = 0;
  St.Position:= 0;
end;

procedure TIBReplicator.ReceivePackages(aDB: TIBDatabase; const aDBProps: TReplDatabaseProperties; aSchemaId: Integer; aStatus: Char; aGroupId, aSrcDbId, aTgtDBId: Integer; const aOfflineDir: string);
var
  LibHandle: THandle;
  H: TTransferHandle;
  V: Variant;
  St, St2: TStream;
  I, J, N: Integer;
  He: TOffHeader;
  S, S2, Params: string;
  IBT: TIBTransaction;
  Fl: Boolean;
  DT: TDateTime;
resourcestring
  sChecking = 'Checking';
  sReceiving = 'Receiving package (%d/%d): %s';
  sPending = 'Pending %d package(s)';
  sSaving = 'Saving: %s';
  sDecoding = 'Decoding: %s/%s';
  sOpening = 'Session opening';
begin
  try
    BuildImplicitEnvironment(['SCHEMAID','GROUPID','DBID','DB2ID','OFFDIR'], [IntToStr(aSchemaId),IntToStr(aGroupId),IntToStr(aSrcDBId),IntToStr(aTgtDBId), aStatus]);
    V:= DBSQLRecord(fConfigDatabase, Format('SELECT OFFTRANSFER,OFFPARAMS FROM '+FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'SCHEMADB')+' WHERE SCHEMAID=%d AND OFFTRANSFER IS NOT NULL AND GROUPID=%d AND DBID=%d', [aSchemaId, aGroupId, aTgtDBId]));
    if VarIsEmpty(V) then
      IBReplicatorError(Format(sNoOfflineMethod, [aSchemaId, aTgtDBId]), 1);
    V[0]:= ParseStr(V[0], aDB, aDBProps.ObjPrefix);
    fDBLog.Log(fLogName, lchNull, Format(sLoadTransferLibrary, [string(V[0])]));
    S:= Transfer.DllPrefix+V[0]+Transfer.DllVer+Transfer.DllExt;
    LibHandle:= LoadLibrary(PChar(S));
    if LibHandle <> 0 then
      begin
        try
          Params:= ParseParams(V[1], aDB, aDBProps.ObjPrefix);
          H:= nil;
          try
            fDBLog.Log(fLogName, lchNull, sOpening);
            CheckTransferError(LibHandle, H, tTransferOpen(GetProcAddress(LibHandle, 'TransferOpen'))(aStatus='S', PChar(Params), H));
            J:= 0;
            fDBLog.Log(fLogName, lchNull, sChecking);
            S:= GetOfflinePackageName(aSchemaId, aStatus, aGroupId, aSrcDbId, aTgtDBId, 0);
            CheckTransferError(LibHandle, H, tTransferReceivePackageNames(GetProcAddress(LibHandle, 'TransferReceivePackageNames'))(H, PChar(S), N));
            fDBLog.Log(fLogName, lchNull, Format(sPending, [N]));
            ForceDirectories(aOfflineDir);
            for I:= 0 to N-1 do
            begin
              if ForceStop then
                Break;
              try
                SetLength(S, 255);
                CheckTransferError(LibHandle, H, tTransferGetReceivedPackageName(GetProcAddress(LibHandle, 'TransferGetReceivedPackageName'))(H, I, PChar(@S[1]), Length(S)));
                S:= StrPas(PChar(S));
                St:= TMemoryStream.Create;
                try

                  fDBLog.Log(fLogName, lchNull, Format(sReceiving, [I+1, N, S]));
                  CheckTransferError(LibHandle, H, tTransferReceivePackage(GetProcAddress(LibHandle, 'TransferReceivePackage'))(H, PChar(S), St));

                  try
                    DecodePackage(St, St2);  // st may be changed but it is always TMemoryStream
                  finally
                    if St2 <> St then
                    begin
                      St.Free;
                      St:= St2;
                    end;
                  end;
                  St.ReadBuffer(He, SizeOf(He));
                  CheckMagic2(He.Magic);
                  S2:= GetOfflinePackageName(He.SchemaId, He.Status, He.GroupId, He.SrcDbId, He.TgtDBId, He.TransferId);
                  if S <> S2 then
                    fDBLog.Log(fLogName, lchError, Format(sOfflinePackageMismatch, [S, S2]));
                  if (He.SchemaId <> aSchemaId) or (He.Status <> aStatus) or (He.GroupId <> aGroupId) or (He.SrcDbId <> aSrcDbId) or (He.TgtDBId <> aTgtDBId) then
                    IBReplicatorError(Format(sOfflineUnexpectedPackageContent, [S]), 1);

                  CheckCRC(He, St);
                  IBT:= TIBTransaction.Create(nil);
                  try
                    IBT.DefaultDatabase:= aDB;
                    IBT.Params.Text:= tranRW;
                    SafeStartTransaction(IBT);
                    try
                      Fl:= Integer(DBSQLRecord(aDB, Format('SELECT CAST(COUNT(*) AS INTEGER) FROM '+FormatIdentifier(aDB.SQLDialect, aDBProps.ObjPrefix+'TRANSFER')+' WHERE TRANSFERID=%d AND STATUS=''%s'' AND SCHEMAID=%d AND GROUPID=%d AND DBID=%d AND STAMP_REC IS NOT NULL', [He.TransferId, He.Status, He.SchemaId, He.GroupId, He.TgtDBId]), IBT)) = 0;
                      DT:= 0;
                      if Fl then
                      begin
                        fDBLog.Log(fLogName, lchNull, Format(sSaving, [S2]));
                        TMemoryStream(St).SaveToFile(aOfflineDir + S2);
                        DT:= Now2;
                      end;
                      if Integer(DBSQLRecord(aDB, Format('SELECT CAST(COUNT(*) AS INTEGER) FROM '+FormatIdentifier(aDB.SQLDialect, aDBProps.ObjPrefix+'TRANSFER')+' WHERE TRANSFERID=%d AND STATUS=''%s'' AND SCHEMAID=%d AND GROUPID=%d AND DBID=%d', [He.TransferId, He.Status, He.SchemaId, He.GroupId, He.TgtDBId]), IBT)) = 0 then
                        DBSQLExec(aDB, Format('INSERT INTO '+FormatIdentifier(aDB.SQLDialect, aDBProps.ObjPrefix+'TRANSFER')+'(TRANSFERID,STATUS,SCHEMAID,GROUPID,DBID,STAMP,STAMP_REC,OFFTRANSFER) VALUES (%d,''%s'',%d,%d,%d,%s,%s,''%s'')', [He.TransferId, He.Status, He.SchemaId, He.GroupId, He.TgtDBId, DT2IBDT(OffStamp2DT(He.Stamp)), DT2IBDT(DT), string(V[0])]), IBT)
                      else if Fl then
                        DBSQLExec(aDB, Format('UPDATE '+FormatIdentifier(aDB.SQLDialect, aDBProps.ObjPrefix+'TRANSFER')+' SET STAMP_REC=%s,OFFTRANSFER=''%s'' WHERE TRANSFERID=%d AND STATUS=''%s'' AND SCHEMAID=%d AND GROUPID=%d AND DBID=%d', [DT2IBDT(DT), string(V[0]), He.TransferId, He.Status, He.SchemaId, He.GroupId, He.TgtDBId]), IBT)
                      else  // to resend acknowledge set TRANSFERID_ACK to null every record that been acknowledge by this package
                        DBSQLExec(aDB, Format('UPDATE '+FormatIdentifier(aDB.SQLDialect, aDBProps.ObjPrefix+'TRANSFER')+' SET RESEND_FLAG=''Y'' WHERE SCHEMAID=%d AND STATUS=''%s'' AND TRANSFERID=%d AND GROUPID=%d AND DBID=%d', [He.SchemaId, He.Status, He.TransferId, He.GroupId, He.TgtDBId]), IBT);

                      CheckTransferError(LibHandle, H, tTransferDeletePackage(GetProcAddress(LibHandle, 'TransferDeletePackage'))(H, PChar(S)));
                    finally
                      SafeCommit(IBT);
                    end;
                  finally
                    IBT.Free;
                  end;
                  Inc(J);
                finally
                  St.Free;
                end;
              except
                on E: Exception do
                begin
                  if CheckIBConnectionError(E, aDB) then
                    raise;
                  fDBLog.Log(fLogName, lchError, E.Message);
                end;
              end;
            end;
            fReplLog.Log(fLogName, lchNull, Format(sTransferReceive, [J, string(V[0])]));
          finally
            if H <> nil then
              tTransferClose(GetProcAddress(LibHandle, 'TransferClose'))(H);
          end;
        finally
          FreeLibrary(LibHandle);
        end;
      end
    else
      IBReplicatorError(Format(sCannotLoadTransferLib, [string(V[0])]), 1);
  except
    on E: Exception do
      fDBLog.Log(fLogName, lchError, E.Message);
  end;
end;

procedure TIBReplicator.ProcessReceivedPackages(aDB: TIBDatabase; const aDBProps: TReplDatabaseProperties; aSchemaId: Integer; aStatus: Char; aGroupId, aSrcDbId, aTgtDBId: Integer; const aOfflineDir: string; aReplOptions: Word);
var
  Q: TIBSQL;
  St: TStream;
  S: string;
  J: Integer;
  DT: TDateTime;
  TransferId: Integer;
resourcestring
  sProcessing = 'Processing package: "%s"';
begin
  Q:= TIBSQL.Create(nil);
  try
    Q.Database:= aDB;
    Q.Transaction:= TIBTransaction.Create(Q);
    Q.Transaction.Params.Text:= tranRW;
    Q.Transaction.DefaultDatabase:= Q.Database;
    SafeStartTransaction(Q.Transaction);
    try
      Q.SQL.Text:= Format('SELECT * FROM '+FormatIdentifier(Q.Database.SQLDialect, aDBProps.ObjPrefix+'TRANSFER')+' WHERE SCHEMAID=%d AND STATUS=''%s'' AND GROUPID=%d AND DBID=%d AND STAMP_PROC IS NULL AND STAMP_REC IS NOT NULL ORDER BY TRANSFERID /*STAMP..not GMT*/', [aSchemaId, aStatus, aGroupId, aTgtDBId]);
      SafeExecQuery(Q);
      J:= 0;
      while not Q.EOF and not ForceStop do
      begin
        try
          S:= GetOfflinePackageName(aSchemaId, aStatus, aGroupId, aSrcDbId, aTgtDBId, Q.FieldByName('TRANSFERID').asInteger);
          fDBLog.Log(fLogName, lchNull, Format(sProcessing, [S]));
          S:= aOfflineDir+S;
          St:= TFileStream.Create(S, fmOpenRead or fmShareDenyWrite);
          try
            if aStatus = 'T' then
              begin
                ProcessTargetOfflinePackage(aDB, aDBProps, aSchemaId, aGroupId, aSrcDbId, aTgtDBId, Q.FieldByName('TRANSFERID').asInteger, St, aOfflineDir, aReplOptions);
                // warning: if is sent an acknowledge but STAMP_REC is empty, won't send anymore in future.  Therefore TRANSFERID_ACK=NULL switch off sending of change, even was acknowledged
                // CZ: pozor kdyby se odeslalo potvrzeni a jeste neni vyplnen STAMP_REC tak uz by se nikdy neposlalo, proto TRANSFERID_ACK=NULL vypnuti poslani zmeny i kdyby uz bylo potvrzeno
                DBSQLExec(Q.Database, Format('UPDATE '+FormatIdentifier(Q.Database.SQLDialect, aDBProps.ObjPrefix+'TRANSFER')+' SET TRANSFERID_ACK=NULL,STAMP_PROC= %s WHERE SCHEMAID=%d AND STATUS=''%s'' AND TRANSFERID=%d AND GROUPID=%d AND DBID=%d', [DT2IBDT(Now2), aSchemaId, aStatus, Q.FieldByName('TRANSFERID').asInteger, aGroupId, aTgtDbId]), Q.Transaction);
              end
            else
              begin
               // STAMP_PROC in S must be filled during processing because is sent in data
               // CZ: STAMP_PROC u S musim vyplinit pri zpracovani, protoze to posilam uz v datech
                TransferId:= ProcessSourceOfflinePackageAndPrepareTargetOfflinePackageToFile(aDB, aDBProps, aSchemaId, aGroupId, aSrcDbId, aTgtDBId, aOfflineDir, St, Q.FieldByName('STAMP_REC').asDateTime, aReplOptions, DT);
                DBSQLExec(Q.Database, Format('UPDATE '+FormatIdentifier(Q.Database.SQLDialect, aDBProps.ObjPrefix+'TRANSFER')+' SET STAMP_PROC=%s, TRANSFERID_ACK=%d WHERE SCHEMAID=%d AND STATUS=''%s'' AND TRANSFERID=%d AND GROUPID=%d AND DBID=%d', [DT2IBDT(DT), TransferId, aSchemaId, aStatus, Q.FieldByName('TRANSFERID').asInteger, aGroupId, aTgtDbId]), Q.Transaction);
              end;
            SafeCommit(Q.Transaction, True);
            Inc(J);
          finally
            St.Free;
          end;
          DeleteFile(S);  // Warning: if test installation at one computer when shared directory is the same for both source and destination databases
                          // pozor pokud se jedna testovaci provoz na jednom pocitaci kdy sdileny adresar je stejny pro zdrojovou i cilovou databazi
        except
          on E: Exception do
          begin
            if CheckIBConnectionError(E, Q.Database) then
              raise;
            fDBLog.Log(fLogName, lchError, E.Message);
          end;
        end;
        SafeNext(Q);
      end;
      fReplLog.Log(fLogName, lchNull, Format(sTransferProcessed, [J]));
    finally
      SafeCommit(Q.Transaction);
    end;
  finally
    Q.Free;
  end;
end;

procedure TIBReplicator.ExecSQLScript;
var
  aDB: TIBDatabase;
  Q, SnapshotQ: TIBSQL;
  SourceF: Boolean;
  S, SQLS, CommentS, TableN, FieldN, WhereS: string;
  DBProps: TReplDatabaseProperties;
  QuotesF, ApostF, CommentF, SQLF, CondDTF, CondSeqF: Boolean;
  C, C2, LastC: Char;
  SeqId, LastSeqId: Integer;

  function DecodeStr(S: string; aBin: Boolean): string;
  var
    I: Integer;
  begin
    if aBin then
      begin
        I:= 1;
        while I <= Length(S) do
          if S[I] < ' ' then
            Delete(S, I, 1)
          else
            Inc(I);
        Result:= Bin2Hex(S);
      end
    else
      begin
        S:= StringReplace(S, '*\_/', '*/', [rfReplaceAll]);
        Result:= StringReplace(S, '\\', '\', [rfReplaceAll]);;
      end;
  end;
begin
  aDB:= TIBDatabase.Create(nil);
  try
    SourceF:= aDbId = 0;
    if SourceF then
      SetDBParams_Repl(aDB, aSchemaId, 0, GetSourceDBId(aSchemaId), False, DBProps)
    else
      SetDBParams_Repl(aDB, aSchemaId, aGroupId, aDBId, False, DBProps);

    Q:= TIBSQL.Create(nil);
    try
      Q.Database:= aDB;
      Q.Transaction:= aDB.DefaultTransaction;

      SnapshotQ:= TIBSQL.Create(nil);
      try
        SnapshotQ.Transaction:= Q.Transaction;
        SnapshotQ.SQL.Text:= Format('UPDATE %s SET SEQID=:SEQID WHERE SCHEMAID=%d AND GROUPID=%d AND DBID=%d AND SEQID<:SEQID2', [FormatIdentifier(SnapshotQ.Database.SQLDialect, DBProps.ObjPrefix+'SNAPSHOT'), aSchemaId, aGroupId, aDBId]);

        QuotesF:= False; ApostF:= False; CommentF:= False; SQLF:= False;
        CondDTF:= (aFromDT = 0) and (aToDT = 0);
        CondSeqF:= (aFromSeqId = 0) and (aToSeqId = 0);
        SQLS:= ''; CommentS:= '';
        LastC:= #0; SeqId:= 0; LastSeqId:= 0;

        while aSQL.Position < aSQL.Size do
        begin
          aSQL.ReadBuffer(C, SizeOf(C));
          if (C = #13) and (LastC = #10) or
             (C = #10) and (LastC = #13) or
             (C < ' ') and not (C in [#13,#10]) then
            LastC:= #0        // omit
          else
          begin
            if (C = '/') and not QuotesF and not ApostF and not CommentF then
            begin
              if aSQL.Position < aSQL.Size then
              begin
                aSQL.ReadBuffer(C2, SizeOf(C2));
                if C2 = '*' then
                  begin
                    CommentF:= True;
                    CommentS:= '';
                   // don't change C to C2, sequence "/*/"
                    C:= #0;  // don't write to CommentS
                  end
                else
                  aSQL.Position:= aSQL.Position-1;
              end;
            end;
            if (C = '*') and CommentF then
            begin
              if aSQL.Position < aSQL.Size then
              begin
                aSQL.ReadBuffer(C2, SizeOf(C2));
                if C2 = '/' then
                  begin
                    CommentF:= False;

                    if not SQLF then  // process "not in SQL" comment
                    begin
                      S:= Trim(CommentS);
                      if Pos(prefSQLDT, S) = 1 then
                      begin
                        CondDTF:= ((aFromDT = 0) or (FormatSQLDT(aFromDT) <= S)) and
                                  ((aToDT = 0) or (FormatSQLDT(aToDT) >= S));
                      end
                      else if Pos(prefSQLSeqId, S) = 1 then
                      begin
                        Delete(S, 1, Length(prefSQLSeqId));
                        SeqId:= StrToIntDef(S, 0);
                        if SeqId > 0 then
                          CondSeqF:= ((aFromSeqId = 0) or (aFromSeqId <= SeqId)) and
                                     ((aToSeqId = 0) or (aToSeqId >= SeqId));
                      end
                      else if (Pos(prefSQLBLOB_Txt, S) = 1) or (Pos(prefSQLBLOB_Bin, S) = 1) then
                      begin  // process BLOB
                        if CondDTF and CondSeqF then
                          if Upcase((Trim(SQLS)+' ')[1]) in ['I', 'U'] then  // ignore previous DELETE or EXECUTE PROCEDURE
                          begin
                            DecodeBLOBMetaSQL(S, TableN, FieldN, WhereS, S);
                            if WhereS <> '' then
                            begin
                              Q.SQL.Clear;
                              Q.SQL.Text:= Format('UPDATE %s SET %s=:PARAM WHERE ', [TableN, FieldN]);
                              Q.SQL.Add(WhereS);
                              if not Q.Transaction.InTransaction then
                                SafeStartTransaction(Q.Transaction);
                              try
                                Q.Prepare;
                                Q.Params[0].asString:= S;
                                SafeExecQuery(Q);
                                SafeCommit(Q.Transaction, True);
                                Inc(aCntBlob);
                              except
                                on E: Exception do
                                begin
                                  if CheckIBConnectionError(E, Q.Database) then
                                    raise;
                                  SafeRollback(Q.Transaction, True);
                                  if E is EDatabaseError then  // null fields, contraints
                                    fDBLog.Log(fLogName, lchError, Format(sReplDatabaseError2, [E.Message, SeqId]))
                                  else
                                    raise;
                                end;
                              end;
                            end;
                          end;
                      end;
                    end;
                    // don't change C to C2, sequence "*/*"
                    C:= #0; // don't write to SQLS
                  end
                else
                  aSQL.Position:= aSQL.Position-1;
              end;
            end;

            if C in [#13,#10] then
              begin
                QuotesF:= False;
                ApostF:= False;
              end
            else if (C = '''') and not CommentF and not QuotesF then
              ApostF:= not ApostF
            else if (C = '"') and not CommentF and not ApostF then
              QuotesF:= not QuotesF
            else if ((C = ';') or (aSQL.Position = aSQL.Size)) and SQLF and not QuotesF and not ApostF and not CommentF then
              begin  // end of SQL command (or EOF)
                if SQLS <> '' then
                  Inc(aCntTotal);
                if (SQLS <> '') and CondDTF and CondSeqF then
                begin
                  Q.SQL.Text:= SQLS+C;
                  if not Q.Transaction.InTransaction then
                    SafeStartTransaction(Q.Transaction);
                  try
                    SafeExecQuery(Q);
                    SafeCommit(Q.Transaction, True);
                    Inc(aCntProc);
                    if LastSeqId = SeqId then  // invalidate already used SeqId
                      SeqId:= 0;
                    if (SeqId > 0) and aSnapshot then
                    begin
                      LastSeqId:= SeqId;
                      if not SnapshotQ.Prepared then
                        SnapshotQ.Prepare;
                      SnapshotQ.Params.ByName('SEQID').asInteger:= SeqId;
                      SnapshotQ.Params.ByName('SEQID2').asInteger:= SeqId;
                      try
                        SafeExecQuery(SnapshotQ);
                        SafeCommit(SnapshotQ.Transaction, True);
                      except
                        on E: Exception do
                        begin
                          if CheckIBConnectionError(E, SnapshotQ.Database) then
                            raise;
                          SafeRollback(SnapshotQ.Transaction, True);
                          fDBLog.Log(fLogName, lchError, Format(sSnapshotError, [E.Message]));
                        end;
                      end;
                    end;
                  except
                    on E: Exception do
                    begin
                      if CheckIBConnectionError(E, Q.Database) then
                        raise;
                      SafeRollback(Q.Transaction, True);
                      if E is EDatabaseError then  // null fields, contraints
                        fDBLog.Log(fLogName, lchError, Format(sReplDatabaseError2, [E.Message, SeqId]))
                      else
                        raise;
                    end;
                  end;
                end;
                SQLF:= False;
                C:= #0;
                SeqId:= 0;
              end;

            if C <> #0 then
            begin
              if CommentF then
                CommentS:= CommentS+C
              else
                begin
                  if not SQLF and (C > ' ') then
                  begin
                    SQLF:= True;
                    SQLS:= '';
                  end;
                  if SQLF then
                    SQLS:= SQLS+C;
                end;
            end;
            LastC:= C;
          end;
        end;
      finally
        SnapshotQ.Free;
      end;
    finally
      Q.Free;
    end;
  finally
    aDB.Free;
  end;
end;

procedure TIBReplicator.DoUpdateStatus(aTicks, aCnt: LongWord);
begin
  if Assigned(fOnUpdateStatus) then
    fOnUpdateStatus(Self, aTicks, aCnt);
end;

procedure TIBReplicator.ProcessMessages;
{$IFNDEF LINUX}
var
  Msg: TMsg;
{$ENDIF}
begin
  {$IFDEF LINUX}
  if MainThreadId = GetCurrentThreadId() then
  begin
  //  QApplication_processEvents(Handle);
    CheckSynchronize;
  end;
  {$ELSE}
  if MainThreadId = GetCurrentThreadId() then
  begin
    if PeekMessage(Msg, 0, 0, 0, PM_REMOVE) then
    begin
      if Msg.Message <> WM_QUIT then
        begin
          TranslateMessage(Msg);
          DispatchMessage(Msg);
        end
      else
        ManualStop:= True;
    end;
  end;
  {$ENDIF}
end;

procedure TIBReplicator.SetEnvironment(const Value: TStrings);
begin
  FEnvironment.Assign(Value);
end;

function TIBReplicator.ParseStr(const aPar: string; aDB: TIBDatabase; const aObjPrefix: string): string;

  function ParseStr2(S: string; var N: Integer): string;
  var
    I, J, SubS_Start, SubS_Len: Integer;
    MacroStr, FuncName, S2, S3: string;

  resourcestring
    sErrorParsing = 'Error parsing string "%s" near "%s"';
    sErrorParsingFunc = 'Unknown parse function  "%s" near "%s"';
    sErrorParsingInt = 'Bad integer conversion "%s" near "%s"';

    function FindEndOfPart(const S: string; ParamFlag: Boolean): Integer;
    var
      Lev: Integer;
      FuncNameFlag: Boolean;
      I: Integer;
    begin
      Lev:= 1; I:= 1; FuncNameFlag:= False;
      while (I <= Length(S)) and (Lev > 0) do
      begin
        if (S[I]='(') and FuncNameFlag then
          Inc(Lev);
        if FuncNameFlag and not (S[I] in ['A'..'Z','a'..'z','0'..'9','_']) then
          FuncNameFlag:= False
        else if (S[I]='$') and (I < Length(S)) and (S[I+1]='$') then
          Inc(I) // $$
        else if (S[I]='$') then
          FuncNameFlag:= True
        else if (S[I] = ')') then
          Dec(Lev)
        else if ParamFlag and (S[I] in [',']) and (Lev = 1) then
          Dec(Lev);  // return param
        Inc(I);
      end;
      if (Lev > 0) then
        IBReplicatorError(Format(sErrorParsing, [aPar, S]), 1);
      Result:= I;
    end;

    function GetNextParam(var S: string): string;
    var
      I: Integer;
    begin
      I:= FindEndOfPart(S+')', True);
      Result:= Copy(S, 1, I-2);
//      if (S[I-1] = ')') then
//        Dec(I);
      Delete(S, 1, I-1);
      Result:= ParseStr2(Result, N);
    end;
  begin
    Result:= '';
    while S <> '' do
    begin
      I:= Pos('$', S);
      if (I = 0) or (N <= 0) then
      begin
        Result:= Result+S;
        Break;
      end;
      if (I < Length(S)) and (S[I+1] = '$') then // $$
      begin
        Result:= Result+Copy(S, 1, I);
        Delete(S, 1, I-1+Length('$$'));
        Continue;
      end;
      J:= I+1;
      while (J <= Length(S)) and (S[J] in ['A'..'Z','a'..'z','0'..'9','_']) do
        Inc(J);
      if (J > Length(S)) or (S[J] <> '(') then // not $xxxx(
      begin
        Result:= Result+Copy(S, 1, J-1);
        Delete(S, 1, J-1);
        Continue;
      end;
      Result:= Result+Copy(S, 1, I-1);
      FuncName:= Copy(S, I+1, J-I-1);
      Delete(S, 1, J);

      Dec(N);
      I:= FindEndOfPart(S, False);

      (*Lev:= 1; I:= 1; FuncNameFlag:= False;
      while (I <= Length(S)) and (Lev > 0) do
      begin
        if (S[I]='(') and FuncNameFlag then
          Inc(Lev);
        if FuncNameFlag and not (S[I] in ['A'..'Z','a'..'z','0'..'9','_']) then
          FuncNameFlag:= False
        else if (S[I]='$') and (I < Length(S)) and (S[I+1]='$') then
          Inc(I) // $$
        else if (S[I]='$') then
          FuncNameFlag:= True
        else if (S[I] = ')') then
          Dec(Lev);
        Inc(I);
      end;
      if (Lev > 0) then
        IBReplicatorError(Format(sErrorParsing, [aPar, S]), 1); *)
      MacroStr:= Copy(S, 1, I-2);
      if (FuncName = '') then
        MacroStr:= ReadEnvironment(ParseStr2(MacroStr, N), '', aDB, aObjPrefix)
      else if SameText(FuncName, 'SUBSTR') then
        begin
          S2:= GetNextParam(MacroStr);
          S3:= Trim(GetNextParam(MacroStr));
          if (S3 = '') then
            SubS_Start:= 0
          else
            SubS_Start:= StrToIntDef(S3, -MaxInt);
          if (SubS_Start = -MaxInt) then
            IBReplicatorError(Format(sErrorParsingInt, [S3, S]), 1);

          S3:= Trim(GetNextParam(MacroStr));
          if (S3 = '') then
            SubS_Len:= MaxInt
          else
            SubS_Len:= StrToIntDef(S3, -MaxInt);
          if (SubS_Len = -MaxInt) then
            IBReplicatorError(Format(sErrorParsingInt, [S3, S]), 1);

          if (SubS_Start < 0) then
            SubS_Start:= Length(S2)+SubS_Start;
          if SubS_Start < 0 then
            SubS_Start:= 0;
          S2:= Copy(S2, SubS_Start+1, MaxInt);
          if (SubS_Len < 0) then
            SubS_Len:= Length(S2)+SubS_Len;
          if (SubS_Len < 0) then
            SubS_Len:= 0;
          MacroStr:= Copy(S2, 1, SubS_Len);
        end
      else
        IBReplicatorError(Format(sErrorParsingFunc, [FuncName, S]), 1);
      S:= MacroStr+Copy(S, I, MaxInt);
    (*

      begin

      end;

      I:= Pos('$(', S);
      if (I = 0) or (N <= 0) then
        I:= Length(S)+1;
      Result:= Result+Copy(S, 1, I-1);
      Delete(S, 1, I-1+Length('$('));
      if S <> '' then  // $(
      begin
        Dec(N);
        Lev:= 1; I:= 1;
        while (I <= Length(S)) and (Lev > 0) do
        begin
          if (S[I]='(') and (I > 1) and (S[I-1]='$') then
            Inc(Lev)
          else if (S[I] = ')') then
            Dec(Lev);
          Inc(I);
        end;
        if (Lev > 0) then
          IBReplicatorError(Format(sErrorParsing, [aPar, S]), 1);
        MacroName:= ParseStr2(Copy(S, 1, I-2), N);
        S:= ReadEnvironment(MacroName, '', aDB, aObjPrefix)+
          Copy(S, I, Length(S));
      end; *)
    end;
  end;

var
  N: Integer;
const
  MaxLevel = 50;  // to avoid infinite cycle or recursions
begin
  N:= MaxLevel;
  Result:= ParseStr2(aPar, N);
end;

function TIBReplicator.ParseParams(const aPar: string; aDB: TIBDatabase; const aObjPrefix: string): string;
var
  Sg: TStrings;
  S: string;
  I, J: Integer;
begin
  Sg:= TStringList.Create;
  try
    Sg.Text:= ParseStr(aPar, aDB, aObjPrefix);
    for I:= 0 to Sg.Count-1 do
    begin
      J:= Pos('=', Sg[I]);
      if J > 0 then
      begin
        S:= Copy(Sg[I], J+1, Length(Sg[I]));
        if IsScrambled(S) then
        begin
          Sg[I]:= Copy(Sg[I], 1, J)+ParseStr(ScramblePassword(S, False), aDB, aObjPrefix);
        end;
      end;
    end;
    Result:= Sg.Text;
  finally
    Sg.Free;
  end;
end;

class function TIBReplicator.ScramblePassword(aPass: string; aEncode: Boolean): string;
const
  AlphaRange = 2*(Ord('Z')-Ord('A')+1)+Ord('9')-Ord('0')+1;
  AlphaSet = [Ord('0')..Ord('9'), Ord('a')..Ord('z'),Ord('A')..Ord('Z')];
  function ScrC(C: Byte): Byte;
  const
    ScrArr: array[1..14] of Word = (Ord(' '), Ord('0'), Ord('9')+1, Ord('A'), Ord('S'), Ord('Z')+1, Ord('a'), Ord('j'), Ord('v'), Ord('z')+1, $80, $95, $CC, $100);
  var
    L, K: Integer;
  begin
    L:= 0;
    Result:= 0;
    for K:= Low(ScrArr) to High(ScrArr) do
    begin
      if C < ScrArr[K] then
      begin
        Result:= ScrArr[K]-1 - (Ord(C)-L);
        Break;
      end;
      L:= ScrArr[K];
    end;
  end;
  function AlphaToByte(C: Byte): Byte;
  begin
    if C in [Ord('0')..Ord('9')] then
      Result:= C - Ord('0')
    else if C in [Ord('A')..Ord('Z')] then
      Result:= C - Ord('A') + Ord('9')-Ord('0')+1
    else // if C in [Ord('a')..Ord('z')] then
      Result:= C - Ord('a') + Ord('9')-Ord('0')+1+Ord('Z')-Ord('A')+1;
  end;

  function ByteToAlpha(C: Byte): Byte;
  begin
    if C <= Ord('9')-Ord('0') then
      Result:= Ord('0')+C
    else
      begin
        Dec(C, Ord('9')-Ord('0')+1);
        if C <= Ord('Z')-Ord('A') then
          Result:= Ord('A')+C
        else
          begin
            Dec(C, Ord('Z')-Ord('A')+1);
            Result:= Ord('a')+C;
          end;
      end;
  end;
  function RotateC(C: Byte; aInc: Integer): Byte;
  begin
    if C in AlphaSet then
      Result:= ByteToAlpha((Integer(AlphaToByte(C))+AlphaRange+aInc mod AlphaRange) mod AlphaRange)
    else
      Result:= C;
  end;
const
  XorM = $00;  // do not xor not to extend beyond #20-#127
var
  I, N: Integer;
  C: Byte;
  Rand: Byte;  // 0..$3F, bits 0..2, order scrambling, bits 3..5 .. scramble in group
  PosFlags: set of Byte;
begin
  Result:= '';
  if IsScrambled(aPass) xor aEncode then
    begin
      if aEncode then
        begin
          if aPass = '' then
            Exit;
          {$IFDEF REGISTRATION_CHECK_PRO}
          TReg_CheckPro('Scramble password');
          {$ENDIF}

          Rand:= Random(AlphaRange); // max. $3F
          N:= 1;
          while aPass <> '' do
          begin
            N:= (N+(Rand and $7)-1) mod Length(aPass) +1;
            C:= Ord(aPass[N]);
            Delete(aPass, N, 1);

            C:= ScrC(C);    { break linear range }
            C:= RotateC(C, Rand*Length(aPass){spread same chars 'aaaa'}); { spread lower/upper/digits }
            C:= C xor XorM;
            Result:= Result+Chr(C);
          end;
          C:= ByteToAlpha(Rand);
          Insert(Chr(C), Result, 2);
          Result:= '~'+Result;
        end
      else
        begin
          if Length(aPass) < 3 then
            Exit;
          C:= Ord(aPass[3]);
          if not (C in AlphaSet) then
            Exit;
          Rand:= AlphaToByte(C);
          Delete(aPass, 1, 1);
          Delete(aPass, 2, 1);
          SetLength(Result, Length(aPass));
          PosFlags:= [];
          N:= 0;
          while aPass <> '' do
          begin
            I:= (Rand and $7) mod Length(Result) +1;
            while I > 0 do
            begin
              repeat
                N:= N mod Length(Result) +1;  // must find free position
              until not ((N-1) in PosFlags);
              Dec(I);
            end;
            PosFlags:= PosFlags+[N-1];
            C:= Ord(aPass[1]);
            Delete(aPass, 1, 1);

            C:= C xor XorM;
            C:= RotateC(C, -Rand*Length(aPass));
            C:= ScrC(C);

            Result[N]:= Chr(C);
          end;
        end;
    end
  else
    Result:= aPass;
end;

class function TIBReplicator.IsScrambled(const aPass: string): Boolean;
begin
  Result:= Pos('~', aPass) = 1;
end;

const
  // sorted list of reserved words
  ReservedWords: array[1..343] of string = (
    'ABSOLUTE',
    'ACTION',
    'ACTIVE',
    'ADD',
    'ADMIN',
    'AFTER',
    'ALL',
    'ALLOCATE',
    'ALTER',
    'AND',
    'ANY',
    'ARE',
    'AS',
    'ASC',
    'ASCENDING',
    'ASSERTION',
    'AT',
    'AUTHORIZATION',
    'AUTO',
    'AUTODDL',
    'AVG',
    'BASED',
    'BASENAME',
    'BASE_NAME',
    'BEFORE',
    'BEGIN',
    'BETWEEN',
    'BIT',
    'BIT_LENGTH',
    'BLOB',
    'BLOBEDIT',
    'BOTH',
    'BUFFER',
    'BY',
    'CACHE',
    'CASCADE',
    'CASCADED',
    'CASE',
    'CAST',
    'CATALOG',
    'CHAR',
    'CHARACTER',
    'CHAR_LENGTH',
    'CHARACTER_LENGTH',
    'CHECK',
    'CHECK_POINT_LEN',
    'CHECK_POINT_LENGTH',
    'CLOSE',
    'COALESCE',
    'COLLATE',
    'COLLATION',
    'COLUMN',
    'COMMIT',
    'COMMITTED',
    'COMPILETIME',
    'COMPUTED',
    'CONDITIONAL',
    'CONNECT',
    'CONNECTION',
    'CONSTRAINT',
    'CONSTRAINTS',
    'CONTAINING',
    'CONTINUE',
    'CONVERT',
    'CORRESPONDING',
    'COUNT',
    'CREATE',
    'CROSS',
    'CSTRING',
    'CURRENT',
    'CURRENT_DATE',
    'CURRENT_TIME',
    'CURRENT_TIMESTAMP',
    'CURRENT_USER',
    'DATABASE',
    'DATE',
    'DAY',
    'DB_KEY',
    'DEALLOCATE',
    'DEBUG',
    'DEC',
    'DECIMAL',
    'DECLARE',
    'DEFAULT',
    'DEFERRABLE',
    'DEFERRED',
    'DELETE',
    'DESC',
    'DESCENDING',
    'DESCRIBE',
    'DESCRIPTOR',
    'DIAGNOSTICS',
    'DISCONNECT',
    'DISPLAY',
    'DISTINCT',
    'DO',
    'DOMAIN',
    'DOUBLE',
    'DROP',
    'ECHO',
    'EDIT',
    'ELSE',
    'END',
    'END-EXEC',
    'ENTRY_POINT',
    'ESCAPE',
    'EVENT',
    'EXCEPT',
    'EXCEPTION',
    'EXEC',
    'EXECUTE',
    'EXISTS',
    'EXIT',
    'EXTERN',
    'EXTERNAL',
    'EXTRACT',
    'FALSE',
    'FETCH',
    'FILE',
    'FILTER',
    'FLOAT',
    'FOR',
    'FOREIGN',
    'FOUND',
    'FREE_IT',
    'FROM',
    'FULL',
    'FUNCTION',
    'GDSCODE',
    'GENERATOR',
    'GEN_ID',
    'GET',
    'GLOBAL',
    'GO',
    'GOTO',
    'GRANT',
    'GROUP',
    'GROUP_COMMIT_WAIT',
    'GROUP_COMMIT_WAIT_TIME',
    'HAVING',
    'HELP',
    'HOUR',
    'IDENTITY',
    'IF',
    'IMMEDIATE',
    'IN',
    'INACTIVE',
    'INDEX',
    'INDICATOR',
    'INIT',
    'INITIALLY',
    'INNER',
    'INPUT',
    'INPUT_TYPE',
    'INSENSITIVE',
    'INSERT',
    'INT',
    'INTEGER',
    'INTERSECT',
    'INTERVAL',
    'INTO',
    'IS',
    'ISOLATION',
    'ISQL',
    'JOIN',
    'KEY',
    'LANGUAGE',
    'LAST',
    'LC_MESSAGES',
    'LC_TYPE',
    'LEADING',
    'LEFT',
    'LENGTH',
    'LEV',
    'LEVEL',
    'LIKE',
    'LOCAL',
    'LOGFILE',
    'LOG_BUFFER_SIZE',
    'LOG_BUF_SIZE',
    'LONG',
    'LOWER',
    'MANUAL',
    'MATCH',
    'MAX',
    'MAXIMUM',
    'MAXIMUM_SEGMENT',
    'MAX_SEGMENT',
    'MERGE',
    'MESSAGE',
    'MIN',
    'MINIMUM',
    'MINUTE',
    'MODULE',
    'MODULE_NAME',
    'MONTH',
    'NAMES',
    'NATIONAL',
    'NATURAL',
    'NCHAR',
    'NEXT',
    'NO',
    'NOAUTO',
    'NOT',
    'NULL',
    'NULLIF',
    'NUM_LOG_BUFS',
    'NUM_LOG_BUFFERS',
    'NUMERIC',
    'OCTET_LENGTH',
    'OF',
    'ON',
    'ONLY',
    'OPEN',
    'OPTION',
    'OR',
    'ORDER',
    'OUTER',
    'OUTPUT',
    'OUTPUT_TYPE',
    'OVERFLOW',
    'OVERLAPS',
    'PAD',
    'PAGE',
    'PAGELENGTH',
    'PAGES',
    'PAGE_SIZE',
    'PARAMETER',
    'PARTIAL',
    'PASSWORD',
    'PLAN',
    'POSITION',
    'POST_EVENT',
    'PRECISION',
    'PREPARE',
    'PRESERVE',
    'PRIMARY',
    'PRIOR',
    'PRIVILEGES',
    'PROCEDURE',
    'PUBLIC',
    'QUIT',
    'RAW_PARTITIONS',
    'RDB$DB_KEY',
    'READ',
    'REAL',
    'RECORD_VERSION',
    'REFERENCES',
    'RELATIVE',
    'RELEASE',
    'RESERV',
    'RESERVING',
    'RESTRICT',
    'RETAIN',
    'RETURN',
    'RETURNING_VALUES',
    'RETURNS',
    'REVOKE',
    'RIGHT',
    'ROLE',
    'ROLLBACK',
    'ROWS',
    'RUNTIME',
    'SCHEMA',
    'SCROLL',
    'SECOND',
    'SECTION',
    'SELECT',
    'SESSION',
    'SESSION_USER',
    'SET',
    'SHADOW',
    'SHARED',
    'SHELL',
    'SHOW',
    'SINGULAR',
    'SIZE',
    'SMALLINT',
    'SNAPSHOT',
    'SOME',
    'SORT',
    'SPACE',
    'SQL',
    'SQLCODE',
    'SQLERROR',
    'SQLSTATE',
    'SQLWARNING',
    'STABILITY',
    'STARTING',
    'STARTS',
    'STATEMENT',
    'STATIC',
    'STATISTICS',
    'SUB_TYPE',
    'SUBSTRING',
    'SUM',
    'SUSPEND',
    'SYSTEM_USER',
    'TABLE',
    'TEMPORARY',
    'TERMINATOR',
    'THEN',
    'TIME',
    'TIMESTAMP',
    'TIMEZONE_HOUR',
    'TIMEZONE_MINUTE',
    'TO',
    'TRAILING',
    'TRANSACTION',
    'TRANSLATE',
    'TRANSLATION',
    'TRIGGER',
    'TRIM',
    'TRUE',
    'TYPE',
    'UNCOMMITTED',
    'UNION',
    'UNIQUE',
    'UNKNOWN',
    'UPDATE',
    'UPPER',
    'USAGE',
    'USER',
    'USING',
    'VALUE',
    'VALUES',
    'VARCHAR',
    'VARIABLE',
    'VARYING',
    'VERSION',
    'VIEW',
    'WAIT',
    'WEEKDAY',
    'WHEN',
    'WHENEVER',
    'WHERE',
    'WHILE',
    'WITH',
    'WORK',
    'WRITE',
    'YEAR',
    'YEARDAY',
    'ZONE'
  );

var
  ReservedWordsIdx: array['A'..Succ('Z')] of Word;

class procedure TIBReplicator.IndexReservedWords;
var
  C: Char;
  I: Word;
begin
  I:= Low(ReservedWords);
  for C:= 'A' to Succ('Z') do
  begin
    ReservedWordsIdx[C]:= I;
    while (I <= High(ReservedWords)) and (ReservedWords[I][1] = C) do
      Inc(I);
  end;
end;

class function TIBReplicator.IsReservedWord(const Value: string): Boolean;
var
  L, H, I: Word;
  C: Integer;
begin
  Result:= False;
  if Length(Value) < 2 then
    Exit;
  if not (Value[1] in ['A'..'Z']) then
    Exit;

  L:= ReservedWordsIdx[Value[1]];
  H:= ReservedWordsIdx[Succ(Value[1])];
  while L < H do
  begin
    I:= (L + H) div 2;
    C:= CompareStr(Value, ReservedWords[I]);
    if C < 0 then
      begin
        H:= I;
      end
    else if C > 0 then
      begin
        L:= I + 1;
        if L > High(ReservedWords) then
          Break;
      end
    else
      begin
        Result:= True;
        Break;
      end;
  end;
end;

class function TIBReplicator.FormatIdentifier(Dialect: Integer; Value: String): String; // = QuoteIdentifier() in x.03
begin
  if Dialect = 1 then
    begin
      Value := AnsiUpperCase(Trim(Value));
      { note we cannot quote reserved words in SQLDialect 1, therefore they are forbidden  }
    end
  else
    Value := '"' + StringReplace (TrimRight(Value), '"', '""', [rfReplaceAll]) + '"';
  Result := Value;
end;

class function TIBReplicator.FormatIdentifierValue(Dialect: Integer; Value: String): String; // = FormatIdentifierValue(QuoteIdentifier()) in x.03
begin
  if Dialect = 1 then
    Result := AnsiUpperCase(Trim(Value))
  else
    Result := TrimRight(Value);
end;

procedure TIBReplicator.SetConfigDatabase(const Value: TIBDatabase);
begin
  fConfigDatabase := Value;
  fConfigDatabaseTransaction.DefaultDatabase:= fConfigDatabase;
end;

class function TIBReplicator.FormatXMLValue(const V: Variant): UTF8String;
var
  I: Integer;
  S2: UTF8string;
begin
  Result:= '';
  case VarType(V) of
    varEmpty, varNull:;
    varDate:
      begin
        Result:= SysUtils.FormatDateTime('yyyy"-"mm"-"dd"T"hh":"nn":"ss', VarToDateTime(V));
      end;
    varString, varOleStr:
      begin
        S2:= AnsiToUtf8(V);
        S2:= StringReplace(S2, '&', '&amp;', [rfReplaceAll]);
        S2:= StringReplace(S2, '>', '&gt;', [rfReplaceAll]);
        S2:= StringReplace(S2, '<', '&lt;', [rfReplaceAll]);
        S2:= StringReplace(S2, '"', '&quot;', [rfReplaceAll]);
        Result:= '';
        for I:= 1 to Length(S2) do
        begin
          if Ord(S2[I]) in [Ord(' ')..255] then
            Result:= Result+S2[I]
          else
            Result:= Result+Format('&#x%.2x;', [Ord(S2[I])]);
        end;
      end
    else
      Result:= string(V);
  end;
end;

function TIBReplicator.GetXMLElement(const aTag: string; const aAttrNames: array of string;
  aAttrValues: array of Variant; const aContent: UTF8string; aContentCRLF: Boolean; aIndent: Integer): UTF8String;
var
  I: Integer;
begin
  Result:= StringOfChar(' ', 2*aIndent);
  Result:= Result+'<'+aTag;
  if Length(aAttrNames) > 0 then
  begin
    for I:= 0 to Length(aAttrNames)-1 do
    begin
      if aAttrNames[I] <> '' then
        Result:= Result+' '+aAttrNames[I]+'="'+FormatXMLValue(aAttrValues[I])+'"';
    end;
  end;
  if aContent <> '' then
    begin
      Result:= Result+'>';
      if aContentCRLF then
        Result:= Result+#13#10;
      Result:= Result+aContent;
      Result:= Result+'</'+aTag+'>'+#13#10;
    end
  else
    Result:= Result+'/>'#13#10;
end;

function TIBReplicator.GetXMLHeader(const aMasterTag,
  aDTD: string; aOptions: Byte): UTF8String;
begin
  Result:= '';
  if aOptions and xmloptHeader > 0 then
    Result:= Result+'<?xml version="1.0" encoding="utf-8"?>'#13#10;
  if aOptions and xmloptHeader > 1 then
    Result:= Result+Format('<!DOCTYPE %s SYSTEM "%s">'#13#10, [aMasterTag, aDTD]);
end;

function TIBReplicator.GetXMLValue(Src: TStream; const aFieldName: string; aUTF8: Boolean): UTF8string;
var
  RRV: TOffReplRecValue;
  StPos: LongInt;
  S: string;
begin
  Result:= '';
  StPos:= Src.Position;
  Src.ReadBuffer(RRV, SizeOf(RRV));
  Src.Position:= StPos;
  case RRV.DataType of
    'N': S:= 'null';
    'C', 'H': S:= 'string';
    'M': S:= 'memo';
    'I': S:= 'integer';
    'J': S:= 'int64';
    'B': S:= 'byte';
    'F': S:= 'float';
    'D': S:= 'date';
    'T': S:= 'time';
    'S': S:= 'datetime';
    'O': S:= 'binary';
    ' ':
      begin
        GetOffReplVariant(Src, aUTF8); // move stream position
        Exit;
      end;
  else
    IBReplicatorError(sBadOffPackage, 1);
  end;
  Result:= GetXMLElement('value', ['fieldname','datatype','fieldtype'],
                                  [aFieldName, S, RRV.FieldType], FormatXMLValue(GetOffReplVariant(Src, aUTF8)), False);
end;

function TIBReplicator.OfflinePackageToXML(aSrc: TStream; aOptions: Byte): UTF8String;
var
  RFS: TReplFieldDefs;
  function GetS(aName: TRelName): string;
  begin
    SetLength(Result, SizeOf(aName));
    Move(aName, Result[1], Length(Result));
    Result:= TrimRight(Result);
  end;
var
  B: Byte;
  I: Integer;
  H: TOffHeader;
  RD: TOffSrcRelDef;
  RDF: TOffSrcRelDefField;
  RL: TOffSrcReplLog;
  RAA: TOffSrcReplAckAck;
  RAAA: TOffTgtReplAckAckAck;
  RA: TOffTgtReplAck;
  RAC: TOffTgtReplAckRec;
  RRS: TOffReplResend;
  S: string;
  CRI: TReplCachedRelationItem;
  CFI: TReplCachedFieldItem;
  Src: TStream;
  LogTag, Ack1Tag, Ack2Tag, Ack3Tag, ResendTag, TagS, SeqS: UTF8string;

  function GetPKeyValues(const aPrimary, aNew: string; aFieldType: Byte): UTF8string;
  var
    S: string;
    I: Integer;
    TagS: UTF8String;
    Arr: TStringOpenArray;
  begin
    Result:= '';
    SetLength(Arr, 0);  // unwarning
    S:= GetOffReplString(Src, SizeOf(Word), True, RL.Options and optUTF8 <> 0);
    if S <> '' then
    begin
      Arr:= StringToArray(S, RL.Sep);
      TagS:= '';
      for I:= 0 to Length(RFS.Fields[aFieldType])-1 do
      begin
        if I < Length(Arr) then
        begin
          TagS:= TagS+GetXMLElement('value', ['fieldname','datatype'],
                                             [RFS.Fields[aFieldType,I].FieldName_Src, 'string'], FormatXMLValue(Arr[I]), False);
        end;
      end;
      Result:= GetXMLElement('values', ['type','state'], [aPrimary, aNew], TagS, True);
    end;
  end;
  function ReadValBuf(const aType, aNew: string; aFieldType: Byte): UTF8string;
  var
    I: Integer;
    RR: TOffSrcRelRec;
    TagS: UTF8string;
  begin
    Src.ReadBuffer(RR, SizeOf(RR));
    TagS:= '';
    for I:= 0 to RR.FieldCount-1 do
    begin
      if aFieldType = 0 then
        with RFS.FieldRefs[I] do
          TagS:= TagS+GetXMLValue(Src, RFS.Fields[FieldType, Idx].FieldName_Src, RL.Options and optUTF8 <> 0)
      else
        TagS:= TagS+GetXMLValue(Src, RFS.Fields[aFieldType, I].FieldName_Src, RL.Options and optUTF8 <> 0);
    end;
    Result:= GetXMLElement('values', ['type','state'], [aType, aNew], TagS, True);
  end;
  function ReadExtBuf: UTF8string;
  var
    I, J: Integer;
    RR: TOffSrcExternalRec;
    RRV: TOffSrcExternalValue;
    FName, S: string;
    TagS: UTF8string;
  begin
    Src.ReadBuffer(RR, SizeOf(RR));
    TagS:= '';
    S:= '';
    for I:= 0 to RR.FieldCount-1 do
    begin
      Src.ReadBuffer(RRV, SizeOf(RRV));
      FName:= GetOffReplString(Src, SizeOf(Byte), True, RL.Options and optUTF8 <> 0);
      if RRV.Options and optextData <> 0 then
      begin
        if aOptions and xmloptExternal <> 0 then
          begin
            for J:= 0 to RRV.Size-1 do
            begin
              Src.ReadBuffer(B, SizeOf(B));
              S:= S+Format('%.2x', [B]);
            end;
          end
        else
          Src.Seek(RRV.Size, soFromCurrent);  // skip data, do not export now

      end;
      with RFS.FieldRefs[RRV.Idx] do
        Tags:= Tags+GetXMLElement('external', ['fieldname','name','stamp','size'],
                                  [RFS.Fields[FieldType, Idx].FieldName_Src, FName, VarFromDateTime(OffStamp2DT(RRV.Stamp)),IntToStr(RRV.Size)], S, False);
    end;
    Result:= GetXMLElement('externals', [''], [''], TagS, True);
  end;
  function GetVer(aVer: Word): string;
  begin
    Result:= Format('%d.%d', [(aVer shr 12) * 10 + (aVer shr 8) mod 16, aVer shr 4 mod 16 * 10 + aVer mod 16]);
  end;
begin
  Result:= '';
  DecodePackage(aSrc, Src);
  try
    Src.ReadBuffer(H, SizeOf(H));
    CheckMagic2(H.Magic);
    CheckCRC(H, Src);
    if not (H.Status in ['S','T']) then
      IBReplicatorError(sBadOffPackage, 1);
    if H.Status = 'S' then
      begin
        ClearCachedValues(False);
        LogTag:= ''; Ack2Tag:= ''; ResendTag:= '';
        while (Src.Position < Src.Size) do
        begin
          Src.ReadBuffer(B, SizeOf(B));
          Src.Position:= Src.Position-SizeOf(B);
          case B of
            offrtRelationDef:
              begin
                Src.ReadBuffer(RD, SizeOf(RD));
                ReadCachedFields(H.SchemaId, H.GroupId, RD.RelationId, False, CRI);
                CRI.Fields.Clear;
                CRI.Name:= GetS(RD.Name);
                CRI.Options:= 0;
                for I:= 0 to RD.Count-1 do
                begin
                  Src.ReadBuffer(RDF, SizeOf(RDF));
                  CFI:= TReplCachedFieldItem.Create;
                  CFI.Id:= I;
                  CFI.FType:= RDF.FieldType;
                  CFI.Name:= RDF.Name;
                  CFI.Options:= 0;
                  CRI.Fields.Add(CFI);
                end;
              end;
            offrtReplLog:
              begin
                RFS:= TReplFieldDefs.Create;
                try
                  Src.ReadBuffer(RL, SizeOf(RL));
                  ReadCachedFields(H.SchemaId, H.GroupId, RD.RelationId, False, CRI);
                  RFS.BuildFieldDefs(CRI, 1, 1);

                  TagS:= '';
                  Tags:= Tags+GetPKeyValues('primary', 'old', 1);
                  Tags:= Tags+GetPKeyValues('primary', 'new', 1);
                  Tags:= Tags+GetPKeyValues('foreign', 'new', 2);

                  if RL.Options and optIsForeignRecord <> 0 then
                  begin
                    Tags:= Tags+ReadValBuf('foreign', 'new', 2);
                    Tags:= Tags+ReadValBuf('foreign', 'old', 2);
                  end;
                  if RL.Options and optIsRelRecord <> 0 then
                  begin
                    Tags:= Tags+ReadValBuf('table', 'new', 0);
                  end;
                  if RL.Options and optIsExternalRecord <> 0 then
                  begin
                    Tags:= Tags+ReadExtBuf;
                  end;

                  case RL.RepType of
                    'I': S:= 'insert';
                    'U': S:= 'update';
                    'D': S:= 'delete';
                  else
                    IBReplicatorError(sBadOffPackage, 1);
                  end;
                  LogTag:= LogTag+GetXMLElement('record', ['relationid','relationname','seqid','reptype','stamp'],
                                                          [RL.RelationId, RFS.RelationName_Src, Integer(RL.SeqId), S, OffStamp2DT(RL.Stamp)],
                                                          TagS, True);
                finally
                  RFS.Free;
                end;
              end;
            offrrtReplAckAck:
              begin
                Src.ReadBuffer(RAA, SizeOf(RAA));
                Ack2Tag:= Ack2Tag+GetXMLElement('acknowledge2', ['transferid','transferidack','stampsent','stamprec','stampproc'],
                                                                [RAA.TransferId, RAA.TransferIdAck, OffStamp2DT(RAA.StampSent), OffStamp2DT(RAA.StampRec), OffStamp2DT(RAA.StampProc)],
                                                                '', True);
              end;
            offrReplResend:
              begin
                Src.ReadBuffer(RRS, SizeOf(RRS));
                ResendTag:= ResendTag+GetXMLElement('resend', ['transferid','transferidack','stamprec','stampproc'],
                                                              [RRS.TransferId, RRS.TransferIdAck, OffStamp2DT(RRS.StampRec), OffStamp2DT(RRS.StampProc)],
                                                              '', True);
              end;
          else
            IBReplicatorError(sBadOffPackage, 1);
          end;
        end;
        Result:= '';
        if LogTag <> '' then
          Result:= Result+GetXMLElement('log', EmptyStringArray, EmptyVariantArray, LogTag, True);
        if Ack2Tag <> '' then
          Result:= Result+GetXMLElement('acknowledges2', EmptyStringArray, EmptyVariantArray, Ack2Tag, True);
        if ResendTag <> '' then
          Result:= Result+GetXMLElement('resends', EmptyStringArray, EmptyVariantArray, ResendTag, True);
        S:= 'source';
      end
    else
      begin
        Ack1Tag:= ''; Ack3Tag:= ''; ResendTag:= '';
        while Src.Position < Src.Size do
        begin
          Src.ReadBuffer(B, SizeOf(B));
          Src.Position:= Src.Position-SizeOf(B);
          case B of
            offrtReplAck:
              begin
                Src.ReadBuffer(RA, SizeOf(RA));
                SeqS:= '';
                for I:= 0 to RA.ProcCount-1 do
                begin
                  Src.ReadBuffer(RAC, SizeOf(RAC));
                  TagS:= '';
                  if RAC.Flag and ackflReasonStr <> 0 then
                  begin
                    TagS:= GetXMLElement('reason', EmptyStringArray, EmptyVariantArray, FormatXMLValue(GetOffReplString(Src, 2, True, True)), False);
                  end;
                  if RAC.Flag and ackflConflict <> 0 then
                    TagS:= TagS + GetXMLConflict(Src);
                  SeqS:= SeqS + GetXMLElement('seqid', ['seqid','action'], [RAC.SeqId, iif(RAC.Flag and ackflManual <> 0, 'manual', 'delete')], TagS, True);
                end;
                Ack1Tag:= Ack1Tag+GetXMLElement('acknowledge1', ['transferidack','stamprec','stampproc'],
                                                                [RA.TransferIdAck, OffStamp2DT(RA.StampRec), OffStamp2DT(RA.StampProc)], SeqS, True);

              end;
            offrrtReplAckAckAck:
              begin
                Src.ReadBuffer(RAAA, SizeOf(RAAA));
                Ack3Tag:= Ack3Tag+GetXMLElement('acknowledge3', ['transferidack','stampsent'],
                                                                [RAAA.TransferIdAck, OffStamp2DT(RAAA.StampSent)], '', True);
              end;
            offrReplResend:
              begin
                Src.ReadBuffer(RRS, SizeOf(RRS));
                ResendTag:= ResendTag+GetXMLElement('resend', ['transferid','transferidack','stamprec','stampproc'],
                                                              [RRS.TransferId, RRS.TransferIdAck, OffStamp2DT(RRS.StampRec), OffStamp2DT(RRS.StampProc)], '', True);
              end;
          end;
        end;
        Result:= '';
        if Ack1Tag <> '' then
          Result:= Result+GetXMLElement('acknowledges1', EmptyStringArray, EmptyVariantArray, Ack1Tag, True);
        if Ack3Tag <> '' then
          Result:= Result+GetXMLElement('acknowledges3', EmptyStringArray, EmptyVariantArray, Ack3Tag, True);
        if ResendTag <> '' then
          Result:= Result+GetXMLElement('resends', EmptyStringArray, EmptyVariantArray, ResendTag, True);
        S:= 'target';
      end;

    Result:= GetXMLElement('package', ['version','schemaid', 'transferid', 'status', 'srcdbid', 'tgtdbid', 'groupid', 'stamp'],
                                      [GetVer(H.Magic.Version), H.SchemaId, H.TransferId, S, H.SrcDBId, H.TgtDBId, H.GroupId, OffStamp2DT(H.Stamp)],
                                      Result, True);
    Result:= GetXMLHeader('package', 'offpackage.dtd', aOptions) + Result;
  finally
    if Src <> aSrc then
      Src.Free;
  end;
end;

function TIBReplicator.GetXMLConflict(Src: TStream): UTF8String;
var
  RACR: TOffTgtReplAckConflictRec;
  S, TgtRelName: string;
  SrcTag, TgtTag: UTF8string;
  I: Integer;
begin
  Src.ReadBuffer(RACR, SizeOf(RACR));
  SrcTag:= ''; TgtTag:= '';

  TgtRelName:= GetOffReplString(Src, 1, False, False);
  for I:= 0 to RACR.Count-1 do
  begin
    S:= GetOffReplString(Src, 1, False, False);
    SrcTag:= SrcTag+GetXMLValue(Src, S, True);
    S:= GetOffReplString(Src, 1, False, False);
    TgtTag:= TgtTag+GetXMLValue(Src, S, True);
  end;

  Result:= '';
  Result:= Result + GetXMLElement('sourcerecord', EmptyStringArray, EmptyVariantArray, SrcTag, True);
  Result:= Result + GetXMLElement('targetrecord', ['relationname'], [TgtRelName], TgtTag, True);

  case RACR.Flag of
    confoptUnknown: S:= 'unknown';
    confoptPrimaryKeyViolation: S:= 'primary_key';
    confoptRecordViolation: S:= 'record';
    confoptFieldViolation: S:= 'field';
  else
    IBReplicatorError(sBadOffPackage, 1);
  end;
  Result:= GetXMLElement('conflict', ['violation', 'stamp', 'tgtdbid'],
                                     [S, OffStamp2DT(RACR.Stamp), RACR.TgtDBId],
                                     Result, True);
end;

function TIBReplicator.ConflictToXML(Src: TStream; aOptions: Byte): UTF8String;
begin
  Result:= '';
  while (Src.Position < Src.Size) do
    Result:= Result+GetXMLConflict(Src);

  Result:= GetXMLElement('conflicts', EmptyStringArray, EmptyVariantArray, Result, True);
  Result:= GetXMLHeader('conflicts', 'conflicts.dtd', aOptions) + Result;
end;

procedure TIBReplicator.ClearRelationStatistics(aSchemaId, aGroupId, aRelationId: Integer);
var
  Q: TIBSQL;
  InTran: Boolean;
begin
  Q:= TIBSQL.Create(nil);
  try
    Q.Database:= fConfigDatabase;
    Q.Transaction:= fConfigDatabaseTransaction;
    Q.SQL.Text:= Format('UPDATE %s SET S_INSERT=0,S_DELETE=0,S_UPDATE=0,S_ERROR=0,S_CONFLICT=0,S_MSEC=0 WHERE (SCHEMAID=%d OR %d=0) AND (GROUPID=%d OR %d=0) AND (RELATIONID=%d OR %d=0)', [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'RELATIONS'), aSchemaId, aSchemaId, aGroupId, aGroupId, aRelationId, aRelationId]);
    InTran:= Q.Transaction.InTransaction;
    if not InTran then
      SafeStartTransaction(Q.Transaction);
    try
      SafeExecQuery(Q);
    finally
      SafeCommit(Q.Transaction, InTran);
    end;
  finally
    Q.Free;
  end;
end;

procedure TIBReplicator.ClearSchemaStatistics(aSchemaId: Integer);
var
  Q: TIBSQL;
  InTran: Boolean;
begin
  Q:= TIBSQL.Create(nil);
  try
    Q.Database:= fConfigDatabase;
    Q.Transaction:= fConfigDatabaseTransaction;
    Q.SQL.Text:= Format('UPDATE %s SET S_INSERT=0,S_DELETE=0,S_UPDATE=0,S_ERROR=0,S_CONFLICT=0,S_MSEC=0 WHERE SCHEMAID=%d OR %d=0', [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'SCHEMATA'), aSchemaId, aSchemaId]);
    InTran:= Q.Transaction.InTransaction;
    if not InTran then
      SafeStartTransaction(Q.Transaction);
    try
      SafeExecQuery(Q);
    finally
      SafeCommit(Q.Transaction, InTran);
    end;
  finally
    Q.Free;
  end;
end;

function TIBReplicator.ForceStop: Boolean;
begin
  Result:= ManualStop;
end;

procedure TIBReplicator.ExecGBAK(aDB: TIBDatabase; aParams: string);
var
  Tmp: string;
  Sg: TStrings;
  I: Integer;
  {$IFNDEF LINUX}
  StartupInfo: TStartupInfo;
  ProcessInformation: TProcessInformation;
  ExitCode: Cardinal;
  Attr: TSecurityAttributes;
  {$ENDIF}
resourcestring
  sExecuting = 'Execute: %s';
begin
  Tmp:= GetTempFileName();
  if FileExists(Tmp) then
    DeleteFile(Tmp);
  try
    aParams:= aParams+Format(' -Y "%s" -USER %s', [Tmp, aDB.Params.Values['USER_NAME']]);
    if IBUtilPath <> '' then
      aParams:= '"'+{$IFDEF VER130}IncludeTrailingBackslash{$ELSE}IncludeTrailingPathDelimiter{$ENDIF}(IBUtilPath)+'gbak" '+aParams
    else
      aParams:= 'gbak '+aParams;

    fDBLog.Log(fLogName, lchNull, Format(sExecuting, [aParams]));
    aParams:= aParams+Format(' -PASSWORD %s', [aDB.Params.Values['PASSWORD']]);  // do no log password

    {$IFNDEF LINUX}
    FillChar(StartupInfo, SizeOf(StartupInfo), 0);
    StartupInfo.CB:= SizeOf(StartupInfo);
    with StartupInfo do
    begin
      dwFlags:= startf_UseShowWindow;
      wShowWindow:= SW_HIDE;
      hStdInput:= INVALID_HANDLE_VALUE;
      hStdOutput:= INVALID_HANDLE_VALUE;
      hStdError:= INVALID_HANDLE_VALUE;
    end;
    Attr.nLength:= SizeOf(Attr);
    Attr.lpSecurityDescriptor:= nil;
    Attr.bInheritHandle:= True;

    if not CreateProcess(nil, PChar(aParams), @Attr, @Attr, True, Normal_Priority_Class, nil, nil, StartupInfo, ProcessInformation) then
      RaiseLastWin32Error;
    repeat
      if not GetExitCodeProcess(ProcessInformation.hProcess, ExitCode) then
        RaiseLastWin32Error;
    until ExitCode <> Still_Active;
    {$ELSE}
    
    if LibC.system(PChar(aParams)) < 0 then
      RaiseLastOSError;
    {$ENDIF}
    Sg:= TStringList.Create;
    try
      Sg.LoadFromFile(Tmp);
      for I:= 0 to Sg.Count-1 do
        fDBLog.Log(fLogName, lchNull, Sg[I]);
    finally
      Sg.Free;
    end;
  finally
    DeleteFile(Tmp);
  end;
end;

procedure TIBReplicator.BackupDatabase(aDB: TIBDatabase; const aDatabaseName, aBackupFileName: string; aOptions: TBackupOptions);
var
  lBackupService: TIBBackupService;
  DBFileName, S: string;
resourcestring
  sBackuping = 'Backuping "%s" to "%s"';
begin
  {$IFDEF REGISTRATION_CHECK_PRO}
  TReg_CheckPro('Database backup');
  {$ENDIF}
  lBackupService := TIBBackupService.Create(nil);
  try
    SetDBServiceParams(lBackupService, aDB, aDatabaseName, DBFileName);
    if lBackupService.Active = true then
    begin
      lBackupService.Options := aOptions;
      lBackupService.Verbose := fLogVerbose;
      lBackupService.DatabaseName:= DBFileName;
      lBackupService.BackupFile.Add(ParseStr(aBackupFileName));
      fDBLog.Log(fLogName, lchNull, Format(sBackuping, [aDB.DatabaseName, ParseStr(aBackupFileName)]));

      if lBackupService.Protocol = IBServices.Local then
        begin
          lBackupService.ServiceStart;

          while lBackupService.IsServiceRunning and not ForceStop do
          begin
            ProcessMessages;
            if not lBackupService.EOF then
              fDBLog.Log(fLogName, lchNull, lBackupService.GetNextLine());
          end;
          while not lBackupService.EOF do
            fDBLog.Log(fLogName, lchNull, lBackupService.GetNextLine());
        end
      else
        begin
          S:= '-BACKUP_DATABASE';
          if IgnoreChecksums in lBackupService.Options then
            S:= S+' -IGNORE';
          if IgnoreChecksums in lBackupService.Options then
            S:= S+' -LIMBO';
          if MetadataOnly in lBackupService.Options then
            S:= S+' -META_DATA';
          if NoGarbageCollection in lBackupService.Options then
            S:= S+' -GARBAGE_COLLECT';
          if OldMetadataDesc in lBackupService.Options then
            S:= S+' -OLD_DESCRIPTIONS';
          if NonTransportable in lBackupService.Options then
            S:= S+' -NT';
          if ConvertExtTables in lBackupService.Options then
            S:= S+' -CONVERT';
          if lBackupService.Verbose then
            S:= S+' -VERIFY';
          S:= S+Format(' "%s" "%s"', [aDB.DatabaseName, lBackupService.BackupFile[0]]);
          ExecGBAK(aDB, S);
        end;
    end;
  finally
    if lBackupService.Active then
      lBackupService.Detach();
    lBackupService.Free();
  end;
end;

procedure TIBReplicator.RestoreDatabase(aDB: TIBDatabase; const aDatabaseName, aBackupFileName: string; aOptions: TRestoreOptions; aPageSize: Integer);
var
  lRestoreService: TIBRestoreService;
  DBFileName, S: string;
resourcestring
  sRestoring = 'Restoring "%s" from "%s"';
begin
  {$IFDEF REGISTRATION_CHECK_PRO}
  TReg_CheckPro('Database restore');
  {$ENDIF}
  lRestoreService := TIBRestoreService.Create(nil);
  try
    SetDBServiceParams(lRestoreService, aDB, aDatabaseName, DBFileName);
    if lRestoreService.Active = true then
    begin
      lRestoreService.Verbose := fLogVerbose;
      lRestoreService.BackupFile.Add(ParseStr(aBackupFileName));
      lRestoreService.DatabaseName.Add(DBFileName);
      lRestoreService.Options:= aOptions;
      lRestoreService.PageSize := aPageSize;
      fDBLog.Log(fLogName, lchNull, Format(sRestoring, [aDB.DatabaseName, ParseStr(aBackupFileName)]));

      if lRestoreService.Protocol = IBServices.Local then
        begin
          lRestoreService.ServiceStart;
          while lRestoreService.IsServiceRunning and not ForceStop do
          begin
            ProcessMessages;
            if not lRestoreService.EOF then
              fDBLog.Log(fLogName, lchNUll, lRestoreService.GetNextLine());
          end;
          while not lRestoreService.EOF do
            fDBLog.Log(fLogName, lchNUll, lRestoreService.GetNextLine());
        end
      else
        begin
          if CreateNewDB in lRestoreService.Options then
            S:= ' -CREATE_DATABASE';
          if Replace in lRestoreService.Options then
            S:= ' -REPLACE_DATABASE';
          if DeactivateIndexes in lRestoreService.Options then
            S:= S+' -INACTIVE';
          if NoShadow in lRestoreService.Options then
            S:= S+' -KILL';
          if NoValidityCheck in lRestoreService.Options then
            S:= S+' -NO_VALIDITY';
          if OneRelationAtATime in lRestoreService.Options then
            S:= S+' -ONE_AT_A_TIME';
          if UseAllSpace in lRestoreService.Options then
            S:= S+' -USE_ALL_SPACE';
          if lRestoreService.Verbose then
            S:= S+' -VERIFY';
          S:= S+Format(' -PAGE_SIZE %d "%s" "%s"', [lRestoreService.PageSize, lRestoreService.BackupFile[0], aDB.DatabaseName]);
          ExecGBAK(aDB, S);
        end;
    end;
  finally
    if lRestoreService.Active then
      lRestoreService.Detach();
    lRestoreService.Free();
  end;
end;

procedure TIBReplicator.BackupDatabase(aDBId: Integer;
  const aBackupFileName: string; aOptions: TBackupOptions);
var
  DB: TIBDatabase;
  DBProps: TReplDatabaseProperties;
begin
  if GetDatabaseType(aDBId) <> dbtInterbase then
    IBReplicatorError(sDatabaseTypeRequired, 0);
  DB:= TIBDatabase.Create(nil);
  try
    if AssignDBParams(DB, aDBId, DBProps) then
    begin
      BackupDatabase(DB, DBProps.DBName, aBackupFileName, aOptions);
    end;
  finally
    DB.Free;
  end;
end;

procedure TIBReplicator.RestoreDatabase(aDBId: Integer;
  const aBackupFileName: string; aOptions: TRestoreOptions; aPageSize: Integer);
var
  DB: TIBDatabase;
  DBProps: TReplDatabaseProperties;
  F: Boolean;
begin
  if GetDatabaseType(aDBId) <> dbtInterbase then
    IBReplicatorError(sDatabaseTypeRequired, 0);
  DB:= TIBDatabase.Create(nil);
  try
    if AssignDBParams(DB, aDBId, DBProps) then
    begin
      { if compressing configdatabase or database which encapsulates config database, this test is not perfect }
      F:= fConfigDatabase.Connected and (fConfigDatabase.DatabaseName = DB.DatabaseName);
      if F then fConfigDatabase.Close;
      RestoreDatabase(DB, DBProps.DBName, aBackupFileName, aOptions, aPageSize);
      if F then fConfigDatabase.Open;
    end;
  finally
    DB.Free;
  end;
end;

function TIBReplicator.GetTempFileName: string;
{$IFNDEF LINUX}
var
  TmpDir: string;
  Buff: array[1..{$IFDEF LINUX}L_tmpnam{$ELSE}MAX_PATH{$ENDIF}] of Char;
  {$ENDIF}
begin
  {$IFDEF LINUX}
  Result:= StrPas(tempnam(_PATH_TMP, 'ibrpl'));
  {$ELSE}
  Windows.GetTempPath(SizeOf(Buff), @Buff);
  TmpDir:= IncludeTrailingBackSlash(StrPas(@Buff));
  Windows.GetTempFileName(PChar(TmpDir), 'ibreplicator.', 0, @Buff);
  Result:= StrPas(@Buff);
  {$ENDIF}
end;

procedure TIBReplicator.CloneSourceDatabase(aSchemaId, aGroupId,
  aDBId: Integer; const aMetadataOnly: Boolean);
var
  Q1, Q2: TIBSQL;
  TempFile, S: string;
  DBProps, TgtDBProps: TReplDatabaseProperties;
  SrcDBId, I: Integer;
  BackupOpt: TBackupOptions;
  DB1, DB2: TIBDatabase;
resourcestring
  sCloning = 'Cloning database DBId:%d for SchemaId:%d,GroupId:%d,DBId:%d';
  sCannotCloneEmpty = 'Cannot clone to empty database because OBJPREFIXis source and target databases are different';
  sCopyingREPLCONFIG = 'Copying datata to "%s"';
begin
  {$IFDEF REGISTRATION_CHECK_PRO}
  TReg_CheckPro('Database cloning');
  {$ENDIF}
  SrcDBId:= GetSourceDBId(aSchemaId);
  TempFile:= GetTempFileName;
  fDBLog.Log(fLogName, lchNull, Format(sCloning, [SrcDBId, aSchemaId, aGroupId, aDBId]));
  if GetDatabaseType(aDBId) <> dbtInterbase then
    IBReplicatorError(sDatabaseTypeRequired, 0);
  BackupOpt:= [ConvertExtTables];
  if aMetadataOnly then
  begin
    if Integer(DBSQLRecord(fConfigDatabase, Format('SELECT CAST(COUNT(*) AS INTEGER) FROM %s D1 INNER JOIN %s D2 ON D1.OBJPREFIX=D2.OBJPREFIX WHERE D1.DBID=%d AND D2.DBID=%d',
        [FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'DATABASES'), FormatIdentifier(fConfigDatabase.SQLDialect, fConfigDatabasePrefix+'DATABASES'), aDBId, GetSourceDBId(aSchemaId)]))) = 0 then
      IBReplicatorError(sCannotCloneEmpty, 0);
    Include(BackupOpt, MetadataOnly);
  end;
  BackupDatabase(SrcDBId, TempFile, BackupOpt);
  try
    RestoreDatabase(aDBId, TempFile, [Replace]);
  finally
    DeleteFile(TempFile);
  end;
  if aMetadataOnly then
  begin  // recreate REPL$CONFIG table to successfuly remove system objects
    DB1:= TIBDatabase.Create(nil);
    try
      SetDBParams(DB1, SrcDBId, DBProps); // SYSDBA preffered, repl user may not exist
      DB2:= TIBDatabase.Create(nil);
      try
        SetDBParams(DB2, aDBId, TgtDBProps);

        DBLogVerbose(Format(sCopyingREPLCONFIG, [DBProps.ObjPrefix+'CONFIG']));
        Q1:= TIBSQL.Create(nil);
        try
          Q1.Database:= DB1;
          Q1.Transaction:= DB1.DefaultTransaction;
          SafeStartTransaction(Q1.Transaction);
          try
            Q1.SQL.Text:= 'SELECT * FROM '+FormatIdentifier(Q1.Database.SQLDialect, DBProps.ObjPrefix+'CONFIG');
            SafeExecQuery(Q1);

            Q2:= TIBSQL.Create(nil);
            try
              Q2.Database:= DB2;
              Q2.Transaction:= DB2.DefaultTransaction;
              SafeStartTransaction(Q2.Transaction);
              try
                Q2.SQL.Text:= 'INSERT INTO '+FormatIdentifier(Q2.Database.SQLDialect, TgtDBProps.ObjPrefix+'CONFIG')+'(';
                S:= '';
                for I:= 0 to Q1.Current.Count-1 do
                begin
                  if S <> '' then
                  begin
                    S:= S+',';
                    Q2.SQL.Text:= Q2.SQL.Text+',';
                  end;
                  Q2.SQL.Text:= Q2.SQL.Text+FormatIdentifier(Q2.Database.SQLDialect, Q1.Fields[I].Name);
                  S:= S+':'+FormatIdentifier(Q2.Database.SQLDialect, Q1.Fields[I].Name);
                end;
                Q2.SQL.Text:=Q2.SQL.Text+') VALUES ('+S+')';
                Q2.Prepare;
                while not Q1.EOF do
                begin
                  for I:= 0 to Q1.Current.Count-1 do
                  begin
                    Q2.Params.ByName(Q1.Fields[I].Name).Value:= Q1.Fields[I].Value;
                  end;
                  SafeExecQuery(Q2);

                  SafeNext(Q1);
                end;
                SafeCommit(Q2.Transaction);
              except
                SafeRollback(Q2.Transaction);
                raise;
              end;
            finally
              Q2.Free;
            end;
          finally
            SafeCommit(Q1.Transaction);
          end;
        finally
          Q1.Free;
        end;

      finally
        DB2.Free;
      end;
    finally
      DB1.Free;
    end;
  end;
  DropSystemObjects(aDBId);
end;

procedure TIBReplicator.CompressDatabase(aDBId: Integer);
var
  TempFile: string;
resourcestring
  sCompressing = 'Compressing database DBId:%d';
begin
  {$IFDEF REGISTRATION_CHECK_PRO}
  TReg_CheckPro('Database compressing');
  {$ENDIF}
  TempFile:= GetTempFileName;
  fDBLog.Log(fLogName, lchNull, Format(sCompressing, [aDBId]));
  if GetDatabaseType(aDBId) <> dbtInterbase then
    IBReplicatorError(sDatabaseTypeRequired, 0);
  BackupDatabase(aDBId, TempFile, [ConvertExtTables]);
  try
    RestoreDatabase(aDBId, TempFile, [Replace]);
  finally
    DeleteFile(TempFile);
  end;
end;

{ TReplTargetDatabase }

procedure TReplTargetDatabase.WriteExtFiles(const aReplLogRec: TReplLogRecord; aIdx: Integer; aPath: string);
var
  I: Integer;
  {$IFNDEF LINUX}
  H: Integer;
  {$ENDIF}
  St: TStream;
begin
  I:= Pos(';', aPath);
  if (I > 0) then
    Delete(aPath, I, MaxInt);
  if aPath = '' then
    Exit;
  if DBProps.ExtFilePath <> '' then
    aPath:= {$IFDEF VER130}IncludeTrailingBackslash{$ELSE}IncludeTrailingPathDelimiter{$ENDIF}(DBProps.ExtFilePath)+aPath;
  aPath:= {$IFDEF VER130}IncludeTrailingBackslash{$ELSE}IncludeTrailingPathDelimiter{$ENDIF}(ExtractFilePath(Trim(fReplicator.ParseStr(aPath))));
  I:= 1;
  while I <= Length(aPath) do   // trim multi delimiters // /\ \/ \\\\ etc.
  begin
    if aPath[I] in ['\', '/'] then
      begin
        Inc(I);
        while (I <= Length(aPath)) and (aPath[I] in ['\', '/']) do
          Delete(aPath, I, 1);
      end
    else
      Inc(I);
  end;
  ForceDirectories(aPath);
  for I:= 0 to Length(aReplLogRec.Externals)-1 do
  begin
    with aReplLogRec.Externals[I] do
      if (Idx = aIdx) and (Data <> nil) then
      begin
        try
          St:= TFileStream.Create(aPath+FileName, fmCreate);
          try
            Data.Position:= DataPos;
            St.CopyFrom(Data, DataSize);
          finally
            St.Free;
          end;
          {$IFDEF LINUX}
          FileSetDate(aPath+FileName, DateTimeToFileDate(Stamp));
          {$ELSE}
          H:= FileOpen(aPath+FileName, fmOpenReadWrite or fmShareDenyNone);
          if H >= 0 then
            try
              FileSetDate(H, DateTimeToFileDate(Stamp));
            finally
              FileClose(H);
            end;
          {$ENDIF}
        except
          on E: Exception do
          begin
            fReplicator.fDBLog.Log(fReplicator.fLogName, lchError, E.Message);
          end;
        end;
      end;
  end;
end;

{ TReplTargetDatabase_IB }

destructor TReplTargetDatabase_IB.Destroy;
begin
  SnapshotQ.Free;
  Database.Free;
  inherited;
end;

function TReplTargetDatabase_IB.GetSQLDialect: Integer;
begin
  Result:= Database.SQLDialect;
end;

function TReplTargetDatabase_IB.InsertToTargetLog(
  const aReplLogRec: TReplLogRecord; aManual: Boolean;
  const aDescription: string; aConflict: TStream): Integer;
var
  SP: TIBStoredProc;
  F: Boolean;
begin
  Result:= 0;
  SP:= TIBStoredProc.Create(nil);
  try
    SP.Database:= Database;
    SP.Transaction:= SP.Database.DefaultTransaction;
    F:= SP.Transaction.InTransaction;
    if not F then
      fReplicator.SafeStartTransaction(SP.Transaction);
    try
      SP.StoredProcName:= DBProps.ObjPrefix + 'LOG_INSERT';
      SP.Prepare;
      SP.ParamByName('SCHEMAID').asInteger:= aReplLogRec.SchemaId;
      SP.ParamByName('GROUPID').asInteger:= aReplLogRec.GroupId;
      SP.ParamByName('SEQID').asInteger:= aReplLogRec.SeqId;
      SP.ParamByName('RELATIONID').asInteger:= aReplLogRec.RelationId;
      SP.ParamByName('DBMASK').asInteger:= DBMask;
      SP.ParamByName('REPLTYPE').asString:= aReplLogRec.IUD;
      SP.ParamByName('STAMP').AsDateTime:= aReplLogRec.Stamp;
      SP.ParamByName('OLDPKEY').asString:= ArrayToString(aReplLogRec.OldPKey, aReplLogRec.Separator);  // ReplaceString(Separator -> Separator ???
      SP.ParamByName('NEWPKEY').asString:= ArrayToString(aReplLogRec.NewPKey, aReplLogRec.Separator);
      fReplicator.SafeExecProc(SP);
      Result:= SP.ParamByName('RESULT').asInteger;
      if aManual then
      begin
        SP.StoredProcName:= DBProps.ObjPrefix + 'LOG_DELETE';
        SP.Prepare;
        SP.ParamByName('SCHEMAID').asInteger:= aReplLogRec.SchemaId;
        SP.ParamByName('SEQID').asInteger:= aReplLogRec.SeqId;
        SP.ParamByName('DBMASK').asInteger:= DBMask;
        SP.ParamByName('ACTIONTYPE').asInteger:= 2;
        fReplicator.FillLogDeleteDesc(SP, aDescription, aConflict);
        fReplicator.SafeExecProc(SP);
        Result:= SP.ParamByName('RESULT').asInteger;
      end;
      if not F then
        fReplicator.SafeCommit(SP.Transaction);
    except
      if not F then
        fReplicator.SafeRollback(SP.Transaction);
      raise;
    end;
  finally
    SP.Free;
  end;
end;

procedure TReplTargetDatabase_IB.ReplicateRecord(
  const aReplLogRec: TReplLogRecord; aReplOptions: Word;
  aConflict: TStream; var aCounter: Integer);
var
  TQ: TIBDataSet2;
  TSP: TIBStoredProc;
  RACR: TOffTgtReplAckConflictRec;
  SPFl: Boolean;

  function GetPKErr: string;
  begin
    if aReplLogRec.IUD = 'I' then
      Result:= ArrayToString(aReplLogRec.NewPKey, '|')
    else
      Result:= ArrayToString(aReplLogRec.OldPKey, '|');
  end;

  procedure AsgnPV(Q: TIBSQL; aParamName: string; const Val: string);
  var
    I: Integer;
  begin
    for I:= 0 to Q.Params.Count-1 do
      with Q.Params[i] do
        if Name = aParamName then
          if Val = '' then
            Value:= Null
          else
            case SQLType of
              SQL_TYPE_DATE,SQL_TYPE_TIME,SQL_TIMESTAMP:
                asDateTime:= IBDT2DT(Val)
              else
                asString:= Val;
            end;
  end;

  procedure AsgnSPV(Q: TIBStoredProc; aParamName: string; const Val: string);
  var
    I: Integer;
  begin
    for I:= 0 to Q.Params.Count-1 do
      with Q.Params[i] do
        if Name = aParamName then
          case DataType of
            ftDateTime, ftTime, ftDate:
              asDateTime:= IBDT2DT(Val)
            else
              asString:= Val;
        end;
  end;

  procedure AsgnPF(SrcDS: TReplLogField; Q: TIBSQL; const aTgtFld: string);
  var
    i: Integer;
  begin
    for i := 0 to Q.Params.Count - 1 do
    begin
{     if Qry.Params[i].name = 'IBX_INTERNAL_DBKEY' then // do not localize
      begin
        PIBDBKey(Qry.Params[i].AsPointer)^ := rdDBKey;
        continue;
      end;   }
      if Q.Params[i].Name = aTgtFld then
      begin
        if not SrcDS.IsNull and ((Q.Params[i].SQLType = SQL_BLOB) or SrcDS.IsBlob) then
          Q.Params[i].asString:= SrcDS.asString
        else
          Q.Params[i].Value:= SrcDS.asVariant;
//        Q.Params[i].Assign(SrcDS.FieldByName(aSrcFld));  // some gds32.dll access problem
        Break; // each of parameters only once
      end;
    end;
  end;

  procedure AsgnV(DS: TDataSet; const aFld: string; const Val: string);
  begin
    with DS.FieldByName(aFld) do
      if Val = '' then
        Value:= Null
      else if DataType in [ftDateTime, ftDate, ftTime] then
        asDateTime:= IBDT2DT(Val)
      else
        asString:= Val;
  end;

  procedure InsertFieldQ;
  var
    SP: TIBStoredProc;
    F: TReplLogField;

    procedure InsF(F: TReplLogField; aMode: Char; aFieldId: Integer);
    var
      K: Integer;
    begin
      for K:= 0 to SP.ParamCount-1 do
        SP.Params[K].Value:= Null;
      SP.ParamByName('SCHEMAID').asInteger:= aReplLogRec.SchemaId;
      SP.ParamByName('SEQID').asInteger:= aReplLogRec.SeqId;
      SP.ParamByName('MODE').asString:= aMode;
      SP.ParamByName('FIELDID').asInteger:= aFieldId;
      SP.ParamByName('DATATYPE').asString:= F.DataType_F;
      if not F.IsNull then
      begin
        if F.IsBlob then
          SP.ParamByName('DATA_'+F.DataType_F).asString:= F.asString
        else
          SP.ParamByName('DATA_'+F.DataType_F).asString:= F.asVariant;
      end;
      fReplicator.SafeExecProc(SP);
    end;
  var
    I: Integer;
  begin
    SP:= TIBStoredProc.Create(nil);
    try
      SP.Database:= Database;
      SP.Transaction:= SP.Database.DefaultTransaction;
      SP.StoredProcName:= DBProps.ObjPrefix+'FIELD_INSERT';
      SP.Prepare;

      for I:= 0 to aReplLogRec.Src_RecordCount-1 do
      begin
        F:= aReplLogRec.FieldQ_Locate('N', I);
        if F = nil then
          if not aReplLogRec.Src_IsEmpty and aReplLogRec.Src_FieldExists(I) then
            F:= aReplLogRec.Src_Record[I];
        if F <> nil then
          with aReplLogRec.FieldDefs, FieldRefs[I] do
            InsF(F, 'N', Fields[FieldType, Idx].FieldId);
      end;
      for I:= 0 to aReplLogRec.Src_RecordCount-1 do
      begin
        F:= aReplLogRec.FieldQ_Locate('O', I);
        if F <> nil then
          with aReplLogRec.FieldDefs, FieldRefs[I] do
            InsF(F, 'O', Fields[FieldType, Idx].FieldId);
      end;
    finally
      SP.Free;
    end;
  end;

  procedure InsertExtFileQ;
  var
    I: Integer;
    Q: TIBSQL;
    FR: TReplFieldRef;
  begin
    Q:= TIBSQL.Create(nil);
    try
      Q.Database:= Database;
      Q.Transaction:= Q.Database.DefaultTransaction;
      Q.SQL.Text:= Format('INSERT INTO %s(SCHEMAID,SEQID,FIELDID,NAME,STAMP,DATA) VALUES(:SchemaId,:SeqId,:FieldId,:Name,:Stamp,:Data)', [fReplicator.FormatIdentifier(Q.Database.SQLDialect, DBProps.ObjPrefix+'EXT_FILE'), DBProps.ObjPrefix]);

      for I:= 0 to Length(aReplLogRec.Externals)-1 do
      begin
        FR:= aReplLogRec.FieldDefs.FieldRefs[aReplLogRec.Externals[I].Idx];
        Q.ParamByName('SCHEMAID').asInteger:= aReplLogRec.SchemaId;
        Q.ParamByName('SEQID').asInteger:= aReplLogRec.SeqId;
        Q.ParamByName('FIELDID').asInteger:= aReplLogRec.FieldDefs.Fields[FR.FieldType, FR.Idx].FieldId;
        Q.ParamByName('NAME').asString:= aReplLogRec.Externals[I].FileName;
        Q.ParamByName('STAMP').asDateTime:= aReplLogRec.Externals[I].Stamp;
        if aReplLogRec.Externals[I].Data <> nil then
          Q.ParamByName('DATA').LoadFromStream(aReplLogRec.Externals[I].Data);
        fReplicator.SafeExecQuery(Q);
      end;
    finally
      Q.Free;
    end;
  end;

  function CmpFlds(aSrcF: TReplLogField; aTgtF: TField): Boolean;
  begin
    Result:= not ( (aTgtF.IsNull xor aSrcF.IsNull) or not aTgtF.IsNull and (aTgtF.asString <> aSrcF.asString) );
  end;

  procedure ConfIns(const aSrcFieldName: string; aSrcF: TReplLogField; aTgtF: TField);
  begin
    fReplicator.PutOffReplString(aConflict, aSrcFieldName, 1, False, False);
    fReplicator.PutOffReplRecValue(aConflict, aSrcF);
    fReplicator.PutOffReplString(aConflict, aTgtF.FieldName, 1, False, False);
    fReplicator.PutOffReplRecValue(aConflict, TIBDataSet2(aTgtF.DataSet).QSelect.FieldByName(aTgtF.FieldName));
  end;

  procedure CheckConfErr(const aErrS: string; aWhat: Integer);
  var
    I, J: Integer;
    Fl: Boolean;
    S: string;
  begin
    if RACR.Count = 0 then
      Exit;

    Fl:= False;
    S:= '';
    for J:= 2 to High(aReplLogRec.FieldDefs.Fields) do
    begin
      for I:= 0 to Length(aReplLogRec.FieldDefs.Fields[J])-1 do
      begin
        if aReplLogRec.FieldDefs.Fields[J, I].Tag and $FF = aWhat then
        begin
          Fl:= True;
          if S <> '' then
            S:= S+',';
          S:= S+TIBReplicator.FormatIdentifier(SQLDialect, aReplLogRec.FieldDefs.Fields[J, I].FieldName_Tgt);
        end;
      end;
    end;
    if Fl then
    begin
      aConflict.Position:=  0;
      RACR.Flag:= aWhat;
      aConflict.WriteBuffer(RACR, SizeOf(RACR));
      aConflict.Position:= aConflict.Size;
      IBReplicatorError(Format(aErrS, [aReplLogRec.SeqId, aReplLogRec.FieldDefs.RelationName_Tgt, ArrayToString(aReplLogRec.NewPKey, '|'), S]), 1);
    end;
  end;

var
  Q1: TIBSQL;
  I, J: Integer;
  SaveDecimalSeparator, SaveThousandSeparator, C: Char;
  S, SaveModifySQL, SaveInsertSQL: string;
  F: TReplLogField;
  SavePos: Longint;
  ManglingIUFl: Byte;
  AsgnExt: array of record Path: string; Idx: Integer; end;

  procedure AddAsgnExt(const FI: TReplFieldItem; const aPath: string);
  begin
    if aPath = '' then
      Exit;
    if FI.Options and fldoptExternalFile <> 0 then
    begin
      SetLength(AsgnExt, Length(AsgnExt)+1);
      with AsgnExt[Length(AsgnExt)-1] do
      begin
        Idx:= FI.Idx;
        Path:= aPath;
      end;
    end;
  end;
label
  ManglingIU;
resourcestring
  sManglingIU = 'Mangling operation "%s"->"%s" of record (#%d), target table "%s". PK: %s';
begin
  SaveDecimalSeparator:= DecimalSeparator;
  SaveThousandSeparator:= ThousandSeparator;
  DecimalSeparator:= '.';
  ThousandSeparator:= ',';
  SetLength(AsgnExt, 0);
  try
    SPFl:= aReplLogRec.FieldDefs.Type_Tgt = 'P';   // to stored procedure
    FillChar(RACR, SizeOf(RACR), 0);
    RACR.Stamp:= DT2OffStamp(fReplicator.Now2());
//      RACR.TgtDBId:=

    TQ:= nil; TSP:= nil;
    if not SPFl then
      TQ:= TIBDataSet2.Create(nil);
    try
      if TQ <> nil then
      begin
        TQ.Database:= Database;
        TQ.Transaction:= TQ.Database.DefaultTransaction;
      end;
      RACR.TgtDBId:= DBProps.DBId;
      if SPFl then
        TSP:= TIBStoredProc.Create(nil);
      try
        if TSP <> nil then
        begin
          TSP.Database:= Database;
          TSP.Transaction:= TSP.Database.DefaultTransaction;
        end;
        if TQ <> nil then
        begin
          TQ.InsertSQL.Text:= Format('INSERT INTO %s(', [TIBReplicator.FormatIdentifier(SQLDialect, aReplLogRec.FieldDefs.RelationName_Tgt)]);
          TQ.InsertSQL.Add(aReplLogRec.FieldDefs.InsertSQL_1);
          TQ.InsertSQL.Add(') VALUES (');
          TQ.InsertSQL.Add(aReplLogRec.FieldDefs.InsertSQL_2);
          TQ.InsertSQL.Add(')');
          TQ.ModifySQL.Text:= Format('UPDATE %s SET', [TIBReplicator.FormatIdentifier(SQLDialect, aReplLogRec.FieldDefs.RelationName_Tgt)]);
          TQ.ModifySQL.Add(aReplLogRec.FieldDefs.ModifySQL);
        end;
        if TSP <> nil then
        begin
          TAuxIBStoredProc(TSP).SelectSQL.Text:= aReplLogRec.CRI.StoredProcSQL;
          if TAuxIBStoredProc(TSP).SelectSQL.Text = '' then
            begin
              TSP.StoredProcName:= aReplLogRec.FieldDefs.RelationName_Tgt;
              aReplLogRec.CRI.StoredProcSQL:= TAuxIBStoredProc(TSP).SelectSQL.Text;
            end
          else
            begin
              TSP.Database:= nil;  // don't call GenerateSQL
              TSP.StoredProcName:= aReplLogRec.FieldDefs.RelationName_Tgt;
              TSP.Database:= Database;
            end;
        end;

        if not Database.DefaultTransaction.InTransaction then
          fReplicator.SafeStartTransaction(Database.DefaultTransaction);
        try
          if (aReplOptions and repoptReplicateLog = 0) or (aReplOptions and repoptTargetReplication <> 0) then
            begin
              try
                if TQ <> nil then
                begin
                  TQ.SelectSQL.Text:= 'SELECT * FROM '+TIBReplicator.FormatIdentifier(SQLDialect, aReplLogRec.FieldDefs.RelationName_Tgt);
                  TQ.SelectSQL.Add('WHERE '+aReplLogRec.FieldDefs.Where_Tgt);
                  TQ.ModifySQL.Add('WHERE '+aReplLogRec.FieldDefs.Where_Tgt);
                  TQ.DeleteSQL.Text:= Format('DELETE FROM %s WHERE %s', [TIBReplicator.FormatIdentifier(SQLDialect, aReplLogRec.FieldDefs.RelationName_Tgt), aReplLogRec.FieldDefs.Where_Tgt]);
                  SaveModifySQL:= TQ.ModifySQL.Text;
                  SaveInsertSQL:= TQ.InsertSQL.Text;
                  ManglingIUFl:= 0;
                ManglingIU:
                  TQ.RefreshSQL.Text:= TQ.SelectSQL.Text;
                  if aReplLogRec.IUD = 'U' then
                  begin
                    TQ.InsertSQL.Text:= '';
                    TQ.DeleteSQL.Text:= '';
                    if TQ.ModifySQL[1] = '' then
                      TQ.ModifySQL.Text:= '';
                  end;
                  if aReplLogRec.IUD = 'I' then
                  begin
                    TQ.ModifySQL.Text:= '';
                    TQ.DeleteSQL.Text:= '';
                    TQ.RefreshSQL.Text:= '';
                  end;
                  if aReplLogRec.IUD = 'D' then
                  begin
                    TQ.ModifySQL.Text:= '';
                    TQ.InsertSQL.Text:= '';
                    TQ.RefreshSQL.Text:= '';
                  end;
                  if aReplOptions and repoptExtConflictCheck <> 0 then
                  begin
                    {$IFDEF REGISTRATION_CHECK_PRO}
                    TReg_CheckPro('Extended conflict check');
                    {$ENDIF}
                    fReplicator.SafeOpen(TQ);   // no target select sql prepare, execute and fetch
                    if (aReplLogRec.IUD = 'I') and not TQ.IsEmpty or (aReplLogRec.IUD in ['U','D']) and TQ.IsEmpty then
                    begin
                      case aReplLogRec.IUD of
                        'I': S:= sReplConflictIns;
                        'U': S:= sReplConflictUpd;
                        'D': S:= sReplConflictDel;
                      end;
                      S:= Format(S, [aReplLogRec.SeqId, aReplLogRec.FieldDefs.RelationName_Tgt, GetPKErr]);

                      if (aReplLogRec.FieldDefs.RelationOptions and reloptInsertUpdateMangling <> 0) and (ManglingIUFl < 2) and (aReplLogRec.IUD in ['I', 'U']) then
                        begin
                          fReplicator.SafeClose(TQ);
                          Inc(ManglingIUFl);
                          fReplicator.fDBLog.Log(fReplicator.LogName, lchError, S);
                          TQ.ModifySQL.Text:= SaveModifySQL;
                          TQ.InsertSQL.Text:= SaveInsertSQL;
                          if aReplLogRec.IUD = 'I' then
                            begin
                              Inc(ManglingIUFl);  // skip pass 2
                              C:= 'U';
                            end
                          else
                            begin
                              if aReplLogRec.FieldDefs.Where_Tgt = aReplLogRec.FieldDefs.Where_Tgt_New then
                                Inc(ManglingIUFl);  // new and old pkey equals
                              if ManglingIUFl = 1 then
                                begin   // check if exist target record according NEWPKEY
                                  TQ.SelectSQL[1]:= 'WHERE '+aReplLogRec.FieldDefs.Where_Tgt_New;
                                  TQ.ModifySQL[2]:= 'WHERE '+aReplLogRec.FieldDefs.Where_Tgt_New;
                                  C:= 'U';
                                end
                              else
                                begin
                                  C:= 'I';
                                end;
                            end;
                          fReplicator.fDBLog.Log(fReplicator.LogName, lchNull, Format(sManglingIU, [aReplLogRec.IUD, C, aReplLogRec.SeqId, aReplLogRec.FieldDefs.RelationName_Tgt, GetPKErr]));
                          aReplLogRec.IUD:= C;
                          goto ManglingIU;
                        end
                      else
                        begin
                          RACR.Flag:= confoptPrimaryKeyViolation;
                          aConflict.WriteBuffer(RACR, SizeOf(RACR));
                          fReplicator.PutOffReplString(aConflict, aReplLogRec.FieldDefs.RelationName_Tgt, 1, False, False);
                          IBReplicatorError(S, 1);
                        end;
                    end;
                  end;
                end;
                try
                  if TQ <> nil then
                  begin
                    if aReplOptions and repoptExtConflictCheck <> 0 then
                      begin
                        aConflict.WriteBuffer(RACR, SizeOf(RACR));
                        fReplicator.PutOffReplString(aConflict, aReplLogRec.FieldDefs.RelationName_Tgt, 1, False, False);
                      // check conflicting non primary keys
                        if aReplLogRec.IUD = 'D' then
                          begin
                            for J:= 2 to High(aReplLogRec.FieldDefs.Fields) do
                            begin
                              for I:= 0 to Length(aReplLogRec.FieldDefs.Fields[J])-1 do
                              begin
                                if aReplLogRec.FieldDefs.Fields[J, I].Options and fldoptDoNotDeleteRecordWhenConflict <> 0 then
                                begin
                                  F:= aReplLogRec.FieldQ_Locate('O', aReplLogRec.FieldDefs.Fields[J, I].Idx);
                                  if F <> nil then
                                  begin // only from FieldQ, in SQ record is deleted
                                    if not CmpFlds(F, TQ.FieldByName(aReplLogRec.FieldDefs.Fields[J, I].FieldName_Tgt)) then
                                    begin
                                      ConfIns(aReplLogRec.FieldDefs.Fields[J, I].FieldName_Src, F, TQ.FieldByName(aReplLogRec.FieldDefs.Fields[J, I].FieldName_Tgt));
                                      Inc(RACR.Count);
                                      aReplLogRec.FieldDefs.Fields[J, I].Tag:= confoptRecordViolation;
                                    end;
                                  end;
                                end
                              end;
                            end;
                          end
                        else if aReplLogRec.IUD = 'U' then
                          begin
                            for J:= 2 to High(aReplLogRec.FieldDefs.Fields) do
                            begin
                              for I:= 0 to Length(aReplLogRec.FieldDefs.Fields[J])-1 do
                              begin
                                if aReplLogRec.FieldDefs.Fields[J, I].Options and (fldoptUpdateOnlyWhenChanged or fldoptTargetPriorityConflict or fldoptLogConflict or fldoptDoNotUpdateRecordWhenConflict) <> 0 then
                                begin
                                  F:= aReplLogRec.FieldQ_Locate('N', aReplLogRec.FieldDefs.Fields[J, I].Idx);
                                  if F <> nil then
                                  else if aReplLogRec.Src_IsEmpty or (aReplOptions and repoptTargetReplication <> 0) then
                                    F:= nil
                                  else
                                    F:= aReplLogRec.Src_Record[aReplLogRec.FieldDefs.Fields[J, I].Idx];
                                  if F <> nil then
                                  begin
                                    if CmpFlds(F, TQ.FieldByName(aReplLogRec.FieldDefs.Fields[J, I].FieldName_Tgt)) then
                                      aReplLogRec.FieldDefs.Fields[J, I].Tag:= $100  // field already updated, do not update optimization
                                    else
                                      begin
                                        SavePos:= aConflict.Position;
                                        ConfIns(aReplLogRec.FieldDefs.Fields[J, I].FieldName_Src, F, TQ.FieldByName(aReplLogRec.FieldDefs.Fields[J, I].FieldName_Tgt));
                                        F:= aReplLogRec.FieldQ_Locate('O', aReplLogRec.FieldDefs.Fields[J, I].Idx);
                                        if (F = nil) and (aReplLogRec.FieldDefs.Fields[J, I].Options and fldoptUpdateOnlyWhenChanged <> 0) then
                                          begin
                                            aReplLogRec.FieldDefs.Fields[J, I].Tag:= $100;  // field not changed, do not update
                                            aConflict.Size:= SavePos;
                                          end
                                        else if (F = nil) or not (CmpFlds(F, TQ.FieldByName(aReplLogRec.FieldDefs.Fields[J, I].FieldName_Tgt))) then
                                          begin
                                            if aReplLogRec.FieldDefs.Fields[J, I].Options and (fldoptTargetPriorityConflict or fldoptLogConflict or fldoptDoNotUpdateRecordWhenConflict) = fldoptTargetPriorityConflict then
                                              aReplLogRec.FieldDefs.Fields[J, I].Tag:= $100  // target priority
                                            else
                                              begin
                                                if aReplLogRec.FieldDefs.Fields[J, I].Options and fldoptDoNotUpdateRecordWhenConflict <> 0 then
                                                  aReplLogRec.FieldDefs.Fields[J, I].Tag:= confoptRecordViolation
                                                else if aReplLogRec.FieldDefs.Fields[J, I].Options and fldoptLogConflict <> 0 then
                                                  aReplLogRec.FieldDefs.Fields[J, I].Tag:= confoptFieldViolation or $100
                                              end;
                                            if (aReplLogRec.FieldDefs.Fields[J, I].Tag and $FF) <> 0 then
                                              begin
                                                if F <> nil then
                                                begin
                                                  aConflict.Size:= SavePos;
                                                  ConfIns(aReplLogRec.FieldDefs.Fields[J, I].FieldName_Src, F, TQ.FieldByName(aReplLogRec.FieldDefs.Fields[J, I].FieldName_Tgt));
                                                end;
                                                Inc(RACR.Count);
                                              end
                                            else
                                              aConflict.Size:= SavePos;
                                          end
                                        else
                                          aConflict.Size:= SavePos;
                                      end;
                                  end;
                                end
                              end;
                            end;
                          end;

                        CheckConfErr(sReplConflictRecord, confoptRecordViolation);

                        if aReplLogRec.IUD = 'D' then
                          begin
                            TQ.Delete;
                          end
                        else
                          begin
                            if aReplLogRec.IUD = 'I' then
                              TQ.Insert
                            else
                              TQ.Edit;
                            try
                              // assign mandatory primary field values
                              for I:= 0 to Length(aReplLogRec.FieldDefs.Fields[1])-1 do
                                if aReplLogRec.FieldDefs.Is_Updatable_Tgt(1, I, aReplLogRec.IUD) then
                                begin
                                  if I >= Length(aReplLogRec.NewPKey) then
                                    S:= ''
                                  else
                                    S:= aReplLogRec.NewPKey[I];
                                  AsgnV(TQ, aReplLogRec.FieldDefs.Fields[1, I].FieldName_Tgt, S);
                                  AddAsgnExt(aReplLogRec.FieldDefs.Fields[1, I], S);
                                end;

                              // assign non-primary field values, foreign value preference
                              for J:= 2 to High(aReplLogRec.FieldDefs.Fields) do
                              begin
                                for I:= 0 to Length(aReplLogRec.FieldDefs.Fields[J])-1 do
                                  if aReplLogRec.FieldDefs.Is_Updatable_Tgt(J, I, aReplLogRec.IUD) and (aReplLogRec.FieldDefs.Fields[J, I].Tag and $100 = 0) then
                                  begin
                                    F:= aReplLogRec.FieldQ_Locate('N', aReplLogRec.FieldDefs.Fields[J, I].Idx);
                                    if (F = nil) and not aReplLogRec.Src_IsEmpty then
                                      F:= aReplLogRec.Src_Record[aReplLogRec.FieldDefs.Fields[J, I].Idx];
                                    if F <> nil then   // error: if source record was deleted, leave current
                                    begin
                                      TQ.SetFieldAsVariant(TQ.FieldByName(aReplLogRec.FieldDefs.Fields[J, I].FieldName_Tgt), F.asVariant);
                                      AddAsgnExt(aReplLogRec.FieldDefs.Fields[J, I], F.asString);
                                    end;
                                  end;
                              end;

                              TQ.Post;
                            except
                              TQ.Cancel;
                              raise;
                            end;
                            CheckConfErr(sReplConflictRecord2, confoptFieldViolation);
                            aConflict.Size:= 0;
                          end;
                      end
                    else
                      begin
                        if aReplLogRec.IUD = 'D' then
                          fReplicator.SafeExecQuery(TQ.QDelete) // no parameters in delete query
                        else
                          begin
                            if aReplLogRec.IUD = 'I' then
                              Q1:= TQ.QInsert
                            else
                              Q1:= TQ.QModify;
                            Q1.Prepare;

                            // assign mandatory primary field values
                            for I:= 0 to Length(aReplLogRec.FieldDefs.Fields[1])-1 do
                              if aReplLogRec.FieldDefs.Is_Updatable_Tgt(1, I, aReplLogRec.IUD) then
                              begin
                                if I >= Length(aReplLogRec.NewPKey) then
                                  S:= ''
                                else
                                  S:= aReplLogRec.NewPKey[I];
                                AsgnPV(Q1, aReplLogRec.FieldDefs.Fields[1, I].FieldName_Tgt, S);
                                AddAsgnExt(aReplLogRec.FieldDefs.Fields[1, I], S);
                              end;

                            // assign non-primary field values, foreign value preference
                            for J:= 2 to High(aReplLogRec.FieldDefs.Fields) do
                            begin
                              for I:= 0 to Length(aReplLogRec.FieldDefs.Fields[J])-1 do
                                if aReplLogRec.FieldDefs.Is_Updatable_Tgt(J, I, aReplLogRec.IUD) then
                                begin
                                  F:= aReplLogRec.FieldQ_Locate('N', aReplLogRec.FieldDefs.Fields[J, I].Idx);
                                  if (F = nil) and not aReplLogRec.Src_IsEmpty then  // if not found in foreign key then get current value
                                    F:= aReplLogRec.Src_Record[aReplLogRec.FieldDefs.Fields[J, I].Idx];
                                  if F <> nil then
                                  begin
                                    AsgnPF(F, Q1, aReplLogRec.FieldDefs.Fields[J, I].FieldName_Tgt);
                                    AddAsgnExt(aReplLogRec.FieldDefs.Fields[J, I], F.asString);
                                  end;
                                end;
                            end;
                         // if field value does not exist in offline package then is assigned NULL value !!!
                            fReplicator.SafeExecQuery(Q1);
                          end;
                      end;
                  end;
                  if TSP <> nil then
                  begin
                    TSP.Prepare;
                    for I:= 0 to TSP.ParamCount-1 do
                      TSP.Params[I].Value:= Null;

                    AsgnSPV(TSP, 'VAR$OPER', aReplLogRec.IUD);  // mandatory parameter VAR$OPER

                    for I:= 0 to Length(aReplLogRec.FieldDefs.Fields[1])-1 do
                    begin
                      if (aReplLogRec.IUD = 'D') and (I < Length(aReplLogRec.OldPKey)) then
                        S:= aReplLogRec.OldPKey[I]
                      else if I < Length(aReplLogRec.NewPKey) then
                        S:= aReplLogRec.NewPKey[I]
                      else
                        S:= '';
                      AsgnSPV(TSP, aReplLogRec.FieldDefs.Fields[1, I].FieldName_Tgt, S);
                      AddAsgnExt(aReplLogRec.FieldDefs.Fields[1, I], S);
                    end;

                    for J:= 2 to High(aReplLogRec.FieldDefs.Fields) do
                    begin
                      for I:= 0 to Length(aReplLogRec.FieldDefs.Fields[J])-1 do
                      begin
                        F:= aReplLogRec.FieldQ_Locate('N', aReplLogRec.FieldDefs.Fields[J, I].Idx);
                        if (F = nil) and not aReplLogRec.Src_IsEmpty then
                          F:= aReplLogRec.Src_Record[aReplLogRec.FieldDefs.Fields[J, I].Idx];
                        with TSP.ParamByName(aReplLogRec.FieldDefs.Fields[J, I].FieldName_Tgt) do
                          if F <> nil then
                            begin
                              Value:= F.asVariant;
                              AddAsgnExt(aReplLogRec.FieldDefs.Fields[J, I], F.asString);
                            end
                          else
                            Value:= Null;
                      end;
                    end;

                    fReplicator.SafeExecProc(TSP);
                  end;
                  for I:= 0 to Length(AsgnExt)-1 do
                    with AsgnExt[I] do
                      WriteExtFiles(aReplLogRec, Idx, Path);
                  SetLength(AsgnExt, 0);
                  try
                    if not SnapshotQ.Prepared then
                      SnapshotQ.Prepare;
                    SnapshotQ.Params.ByName('SEQID').asInteger:= aReplLogRec.SeqId;
                    SnapshotQ.Params.ByName('SEQID2').asInteger:= SnapshotQ.Params.ByName('SEQID').asInteger;
                    fReplicator.SafeExecQuery(SnapshotQ);
                  except
                    on E: Exception do
                    begin
                      if CheckIBConnectionError(E, SnapshotQ.Database) then
                        raise;
                      fReplicator.fDBLog.Log(fReplicator.fLogName, lchError, Format(sSnapshotError, [E.Message]));
                    end;
                  end;
                except
                  on E: Exception do
                  begin
                    if CheckIBConnectionError(E, Database) then
                      raise;
                    if E is EDatabaseError then  // null fields, contraints
                      begin
                        IBReplicatorError(Format(sReplDatabaseError, [E.Message, aReplLogRec.SeqId, aReplLogRec.FieldDefs.RelationName_Tgt, GetPKErr]), 2);
                      end
                    else
                      raise;
                  end;
                end;
              except
                on E: Exception do
                begin
                  if E is EIBReplicatorError then
                  begin
                    fReplicator.SafeRollback(Database.DefaultTransaction, True);  // Interbase ver. 6 required
                    if aReplOptions and (repoptTargetReplication or repoptReportToTarget) = repoptReportToTarget then
                    begin
                      try
                        InsertToTargetLog(aReplLogRec, True, E.Message, aConflict);
                        InsertFieldQ();
                        InsertExtFileQ();
                        fReplicator.SafeCommit(Database.DefaultTransaction, True);
                      except
                      // error already logged in Safe* procedure
                      end;
                    end;
                  end;
                  raise;
                end;
              end;
            end
          else if aReplOptions and repoptTargetReplication = 0 then    // replicate REPL$LOG
            begin
              InsertToTargetLog(aReplLogRec, False, '', nil);
              InsertFieldQ();
              InsertExtFileQ();
            end;
          fReplicator.SafeCommit(Database.DefaultTransaction, True);
          Inc(aCounter);
        except
          fReplicator.SafeRollback(Database.DefaultTransaction, True);  // Interbase ver. 6 required
          raise;
        end;
      finally
        TSP.Free;
      end;
    finally
      TQ.Free;
    end;
  finally
    DecimalSeparator:= SaveDecimalSeparator;
    ThousandSeparator:= SaveThousandSeparator;
  end;
end;

function MakeInsSQL(const RFS: TReplFieldDefs; SQ: TDataSet; SQLDialect: Integer): string;
var
  S: string;
  I, J: Integer;
  F: TField;
begin
  S:= '';
  for I:= Low(RFS.Fields) to High(RFS.Fields) do
    for J:= 0 to Length(RFS.Fields[I])-1 do
    begin
      if RFS.Fields[I][J].Options and fldoptDoNotInsert = 0 then
      begin
        F:= SQ.FindField(RFS.Fields[I][J].FieldName_Src);
        if F <> nil then
        begin
          if S <> '' then
            S:= S+',';
          S:= S+FieldToSQLString(F);
        end;
      end;
    end;
  Result:= Format('INSERT INTO %s(%s) VALUES (%s);', [TIBReplicator.FormatIdentifier(SQLDialect, RFS.RelationName_Tgt), RFS.InsertSQL_1, S]);
  if S = '' then
    Result:= '/*'+Result+'*/';
end;

procedure TReplTargetDatabase_IB.SynchronizeTable(
  const aSyncRec: TSyncRecord; aSrcDB: TIBDatabase;
  var aCounters: TSyncRecordCounters);
var
  RFS: TReplFieldDefs;
  SQLS, WhereValues_Tgt, WhereLog_Tgt: string;
  CRI: TReplCachedRelationItem;
  SQ, TQ: TIBDataSet2;

  function MakeUpdSQL(SQ: TDataSet): string;
  var
    S: string;
    I, J: Integer;
  begin
    S:= '';
    for I:= Low(RFS.Fields) to High(RFS.Fields) do
      for J:= 0 to Length(RFS.Fields[I])-1 do
      begin
        with RFS.Fields[I][J] do
        begin
          if Tag and $1 <> 0 then
          begin
            if S <> '' then
              S:= S+',';
            S:= S+TIBReplicator.FormatIdentifier(SQLDialect, FieldName_Tgt)+'='+FieldToSQLString(SQ.FieldByName(FieldName_Src));
          end;
        end;
      end;
    Result:= Format('UPDATE %s SET %s WHERE %s;', [TIBReplicator.FormatIdentifier(SQLDialect, RFS.RelationName_Tgt), S, WhereValues_Tgt]);
    if S = '' then
      Result:= '/*'+Result+'*/';
  end;

  function AssignRec(aInsert: Boolean): Boolean;
  var
    I, J: Integer;
    F: TField;
  begin
    Result:= False;
    for I:= Low(RFS.Fields) to High(RFS.Fields) do
      for J:= 0 to Length(RFS.Fields[I])-1 do
        with RFS.Fields[I][J] do
          if (aInsert and (Options and fldoptDoNotInsert = 0)) or
             (not aInsert and (Options and fldoptDoNotUpdate = 0)) then
          begin
            F:= TQ.FindField(FieldName_Tgt);
            if F <> nil then
              with SQ.FieldByName(FieldName_Src) do
              begin
                if (F.IsNull xor IsNull) or (not IsNull and (asString <> F.AsString)) then
                begin
                  Tag:= Tag or $01;   // for MakeUpdSQL
                  F.DataSet.Edit;
                  Result:= True;
                  if not IsNull and IsBlob then
                    F.asString:= asString
                  else
                    TQ.SetFieldAsVariant(F, Value);
                end;
              end;
          end;
  end;

  procedure SetParams(aSetTQ: Boolean);
  var
    I: Integer;
    S: string;
  begin
    WhereValues_Tgt:= ''; WhereLog_Tgt:= '';
    for I:= 0 to Length(RFS.Fields[1])-1 do
      with RFS.Fields[1][I] do
      begin
        if aSetTQ then
          TQ.QSelect.Params.ByName('OLD_'+FieldName_Tgt).Value:= SQ.FieldByName(FieldName_Src).Value
        else
          SQ.QSelect.Params.ByName(FieldName_Src).Value:= TQ.FieldByName(FieldName_Tgt).Value;

        if WhereValues_Tgt <> '' then
        begin
          WhereValues_Tgt:= WhereValues_Tgt + ' AND ';
          WhereLog_Tgt:= WhereLog_Tgt + ',';
        end;
        if aSetTQ then
          S:= FieldToSQLString(SQ.FieldByName(FieldName_Src))
        else
          S:= FieldToSQLString(TQ.FieldByName(FieldName_Tgt));
        WhereValues_Tgt:= WhereValues_Tgt + TIBReplicator.FormatIdentifier(SQLDialect, FieldName_Tgt)+'='+S;
        WhereLog_Tgt:= WhereLog_Tgt + TIBReplicator.FormatIdentifier(SQLDialect, FieldName_Tgt)+'='+S;
      end;
    WhereLog_Tgt:= TIBReplicator.FormatIdentifier(SQLDialect, RFS.RelationName_Tgt)+':'+WhereLog_Tgt;
  end;

  procedure Log(Wh: Char);
  begin
    fReplicator.fReplLog.Log(fReplicator.fLogName, lchNull, Format('%s:%s', [Wh, WhereLog_Tgt]));
  end;
begin
  if aSyncRec.Action.Action and (saInsDel or saUpd) = 0 then
    Exit;
  if fReplicator.ReadCachedFields(aSyncRec.SchemaId, aSyncRec.GroupId, aSyncRec.RelationId, True, CRI) then { any definition ? }
  begin
    if CRI.TType = 'P' then   // to stored procedure
      IBReplicatorError(Format(sCannotSynchronizeToStoredProc, [CRI.Name]), 2);

    RFS:= TReplFieldDefs.Create;
    try
      SQ:= TIBDataSet2.Create(nil);
      try
        SQ.Database:= aSrcDB;
        SQ.Transaction:= SQ.Database.DefaultTransaction;
        if not SQ.Transaction.InTransaction then
          fReplicator.SafeStartTransaction(SQ.Transaction);
        TQ:= TIBDataSet2.Create(nil);
        try
          TQ.Database:= Database;
          TQ.Transaction:= TQ.Database.DefaultTransaction;
          if not TQ.Transaction.InTransaction then
            fReplicator.SafeStartTransaction(TQ.Transaction);
          try
            RFS.BuildFieldDefs(CRI, SQ.Database.SQLDialect, SQLDialect);
            RFS.GetWhereParams(SQ.Database.SQLDialect, SQLDialect, 'OLD_', RFS.Where_Src, RFS.Where_Tgt);

            SQ.SelectSQL.Text:= Format('SELECT * FROM %s T', [TIBReplicator.FormatIdentifier(SQ.Database.SQLDialect, RFS.RelationName_Src)]);
            TQ.SelectSQL.Text:= Format('SELECT * FROM %s T', [TIBReplicator.FormatIdentifier(SQLDialect, RFS.RelationName_Tgt)]);
            TQ.InsertSQL.Add(Format('INSERT INTO %s(', [TIBReplicator.FormatIdentifier(SQLDialect, RFS.RelationName_Tgt)]));
            TQ.InsertSQL.Add(RFS.InsertSQL_1);
            TQ.InsertSQL.Add(') VALUES (');
            TQ.InsertSQL.Add(RFS.InsertSQL_2);
            TQ.InsertSQL.Add(')');
            TQ.ModifySQL.Add(Format('UPDATE %s SET', [TIBReplicator.FormatIdentifier(SQLDialect, RFS.RelationName_Tgt)]));
            TQ.ModifySQL.Add(RFS.ModifySQL);    // updatable fields
            if RFS.Where_Src = '' then
              IBReplicatorError(Format(sNoPrimaryKey, [RFS.RelationName_Src]), 2);

            TQ.ModifySQL.Add('WHERE '+RFS.Where_Tgt);
            TQ.DeleteSQL.Text:= Format('DELETE FROM %s WHERE %s', [TIBReplicator.FormatIdentifier(SQLDialect, RFS.RelationName_Tgt), RFS.Where_Tgt]);
            TQ.RefreshSQL.Text:= TQ.SelectSQL.Text;
            TQ.RefreshSQL.Add('WHERE '+RFS.Where_Tgt);

            if TQ.ModifySQL[1] = '' then
              TQ.ModifySQL.Text:= '';
            if TQ.InsertSQL[1] = '' then
              TQ.InsertSQL.Text:= '';

            if aSyncRec.Action.Action and saSrcTgt <> 0 then
              begin
                if RFS.Where_Src = '' then  // no PK
                  Exit;
                if aSyncRec.Action.Where_Tgt <> '' then
                begin
                  {$IFDEF REGISTRATION_CHECK_PRO}
                  TReg_CheckPro('Conditional synchronization');
                  {$ENDIF}
                  TQ.SelectSQL.Add('WHERE '+aSyncRec.Action.Where_Tgt);
                end;
                SQ.SelectSQL.Add(Format('WHERE %s', [RFS.Where_Src]));
                if aSyncRec.Action.Where_Src <> '' then
                begin
                  {$IFDEF REGISTRATION_CHECK_PRO}
                  TReg_CheckPro('Conditional synchronization');
                  {$ENDIF}
                  SQ.SelectSQL.Add('AND ('+aSyncRec.Action.Where_Src+')');
                end;
              end
            else
              begin
                if RFS.Where_Tgt = '' then  // no PK
                  Exit;
                if aSyncRec.Action.Where_Src <> '' then
                begin
                  {$IFDEF REGISTRATION_CHECK_PRO}
                  TReg_CheckPro('Conditional synchronization');
                  {$ENDIF}
                  SQ.SelectSQL.Add('WHERE '+aSyncRec.Action.Where_Src);
                end;
                TQ.SelectSQL.Add(Format('WHERE %s', [RFS.Where_Tgt]));
                if aSyncRec.Action.Where_Tgt <> '' then
                begin
                  {$IFDEF REGISTRATION_CHECK_PRO}
                  TReg_CheckPro('Conditional synchronization');
                  {$ENDIF}
                  TQ.SelectSQL.Add('AND ('+aSyncRec.Action.Where_Tgt+')');
                end;
              end;

            if (aSyncRec.Action.Action and saInsDel = 0) or (aSyncRec.Action.Action and saRO <> 0) then
            begin
              SQ.DeleteSQL.Text:= '';
              SQ.InsertSQL.Text:= '';
            end;
            if (aSyncRec.Action.Action and saUpd = 0) or (aSyncRec.Action.Action and saRO <> 0) then
            begin
              SQ.ModifySQL.Text:= '';
            end;

            if aSyncRec.Action.Action and saSrcTgt = 0 then
              begin               // loop source table
                fReplicator.SafeOpen(SQ);
                while not SQ.EOF and not fReplicator.ForceStop do
                begin
                  RFS.ClearTags(0);
                  if not TQ.Prepared then
                    TQ.Prepare;
                  SetParams(True);
                  fReplicator.SafeOpen(TQ);
                  try
                    try
                      if TQ.IsEmpty and (aSyncRec.Action.Action and saInsDel <> 0) then
                        begin
                          if aSyncRec.Action.Action and saRO = 0 then
                            begin
                              AssignRec(True);
                              TQ.CheckBrowseMode;
                              Inc(aCounters.Inserted);
                              Log('I');
                            end
                          else
                            begin   { log SQL }
                              SQLS:= MakeInsSQL(RFS, SQ, SQLDialect);
                              fReplicator.fReplLog.Log(fReplicator.fLogName, lchNull, SQLS);
                            end;
                        end
                      else if not TQ.IsEmpty and (aSyncRec.Action.Action and saUpd <> 0) then
                        begin
                          if AssignRec(False) then
                          begin
                            if aSyncRec.Action.Action and saRO = 0 then
                              begin
                                TQ.CheckBrowseMode;
                                Inc(aCounters.Updated);
                                Log('U');
                              end
                            else
                              begin   { log SQL }
                                TQ.Cancel;
                                SQLS:= MakeUpdSQL(SQ);
                                fReplicator.fReplLog.Log(fReplicator.fLogName, lchNull, SQLS);
                              end;
                          end;
                        end;
                    except
                      on E: Exception do
                      begin
                        if TQ.State = dsEdit then
                          TQ.Cancel;
                        if E is EDatabaseError then  // null fields, contraints
                          begin
                            Inc(aCounters.Conflicts);
                            fReplicator.fDBLog.Log(fReplicator.fLogName, lchError, Format(sSyncDatabaseError, [E.Message, WhereLog_Tgt]));
                            if CheckIBConnectionError(E, TQ.Database) then
                              raise;
                          end
                        else
                          begin
                            Inc(aCounters.Errors);
                            raise;
                          end;
                      end;
                    end;
                  finally
                    fReplicator.SafeClose(TQ);
                  end;
                  fReplicator.SafeNext(SQ);
                  Inc(aCounters.Total);
                  fReplicator.ProcessMessages;
                end;
              end
            else
              begin             // loop target table
                fReplicator.SafeOpen(TQ);
                while not TQ.EOF and not fReplicator.ForceStop do
                begin
                  RFS.ClearTags(0);
                  SetParams(False);
                  fReplicator.SafeOpen(SQ);
                  try
                    try
                      if SQ.IsEmpty and (aSyncRec.Action.Action and saInsDel <> 0) then
                        begin
                          if aSyncRec.Action.Action and saRO = 0 then
                            begin
                              TQ.Delete;
                              Log('D');
                              Inc(aCounters.Deleted);
                            end
                          else
                            begin   { log SQL }
                              SQLS:= Format('DELETE FROM %s WHERE %s;', [TIBReplicator.FormatIdentifier(SQLDialect, RFS.RelationName_Tgt), WhereValues_Tgt]);
                              fReplicator.fReplLog.Log(fReplicator.fLogName, lchNull, SQLS);
                            end;
                          end
                      else if not SQ.IsEmpty and (aSyncRec.Action.Action and saUpd <> 0) then
                        begin
                          if AssignRec(False) then
                          begin
                            if aSyncRec.Action.Action and saRO = 0 then
                              begin
                                TQ.CheckBrowseMode;
                                Inc(aCounters.Updated);
                                Log('U');
                              end
                            else
                              begin   { log SQL }
                                TQ.Cancel;
                                SQLS:= MakeUpdSQL(SQ);
                                fReplicator.fReplLog.Log(fReplicator.fLogName, lchNull, SQLS);
                              end;
                          end;
                        end;
                    except
                      on E: Exception do
                      begin
                        if TQ.State = dsEdit then
                          TQ.Cancel;
                        if E is EDatabaseError then  // null fields, contraints
                          begin
                            Inc(aCounters.Conflicts);
                            fReplicator.fDBLog.Log(fReplicator.fLogName, lchError, Format(sSyncDatabaseError, [E.Message, WhereLog_Tgt]));
                            if CheckIBConnectionError(E, TQ.Database) then
                              raise;
                          end
                        else
                          begin
                            Inc(aCounters.Errors);
                            raise;
                          end;
                      end;
                    end;
                  finally
                    fReplicator.SafeClose(SQ);
                  end;
                  fReplicator.SafeNext(TQ);
                  Inc(aCounters.Total);
                  fReplicator.ProcessMessages;
                end;
              end;

            fReplicator.SafeCommit(TQ.Transaction, True);
          except
            fReplicator.SafeRollback(TQ.Transaction, True);  // Interbase ver. 6 required
            raise;
          end;
        finally
          TQ.Free;
        end;
      finally
        SQ.Free;
      end;
    finally
      RFS.Free;
    end;
  end;
end;

{ TReplTargetDatabase_Logger }

destructor TReplTargetDatabase_Logger.Destroy;
begin
  inherited;
  Logger.Free;
end;

function TReplTargetDatabase_Logger.GetSQLDialect: Integer;
begin
  Result:= fSQLDialect;
end;

procedure TReplTargetDatabase_Logger.ReplicateRecord(
  const aReplLogRec: TReplLogRecord; aReplOptions: Word;
  aConflict: TStream; var aCounter: Integer);
var
  SQLS, SQLS2: string;
  SPFl: Boolean;

  procedure PutSQLS(aFieldIdx: Integer; F1: TReplLogField; PFK: Boolean; PFKS: string);
  var
    NullFl: Boolean;
    Fld2: string;
  begin
    if not aReplLogRec.Src_FieldExists(aFieldIdx) then
    begin
      with aReplLogRec.FieldDefs, FieldRefs[aFieldIdx] do
        fReplicator.fDBLog.Log(fReplicator.fLogName, lchError, Format(sPutSQLFieldNotFound, [Fields[FieldType, Idx].FieldName_Src, aReplLogRec.FieldDefs.RelationName_Src]));
      Exit;
    end;

    with aReplLogRec.FieldDefs, FieldRefs[aFieldIdx] do
      Fld2:= Fields[FieldType, Idx].FieldName_Tgt;

    if F1.isBlob and (F1.asString <> '') then
      begin
        if SPFl then
        begin
          if SQLS2 <> '' then
            SQLS2:= SQLS2+',';
          SQLS2:=SQLS2+'/*'+TIBReplicator.FormatIdentifier(SQLDialect, Fld2)+'=*/NULL';
        end;
        Exit;   { cannot insert blob using normal SQL }
      end;
    if not PFK then
      begin
        NullFl:= F1.isNull;
        if not NullFl then
          case F1.DataType of
            ftDateTime:
              PFKS:= FormatDateTime('yyyy-mm-dd hh-nn-ss.zzz', F1.asDateTime);
            ftDate:
              PFKS:= FormatDateTime('yyyy-mm-dd', F1.asDateTime);
            ftTime:
              PFKS:= FormatDateTime('hh-nn-ss.zzz', F1.asDateTime);
          else
            PFKS:= F1.asString;
          end;
      end
    else
      NullFl:= PFKS = '';
    PFKS:= AddApostrophs(PFKS);
    if not NullFl then
      PFKS:= ''''+PFKS+''''
    else
      PFKS:= 'NULL';
    if SQLS2 <> '' then
    begin
      if not SPFl then
        SQLS:= SQLS+',';
      SQLS2:= SQLS2+',';
    end;
    if SPFl then
      SQLS2:= SQLS2+'/*'+TIBReplicator.FormatIdentifier(SQLDialect, Fld2)+'=*/'+PFKS
    else if aReplLogRec.IUD = 'I' then
      begin
        SQLS:= SQLS+TIBReplicator.FormatIdentifier(SQLDialect, Fld2);
        SQLS2:= SQLS2+PFKS;
      end
    else
      begin
        SQLS:= SQLS+TIBReplicator.FormatIdentifier(SQLDialect, Fld2)+'='+PFKS;
        SQLS2:= ','; { dummy }
      end;
  end;
var
  I, J: Integer;
  SaveDecimalSeparator, SaveThousandSeparator: Char;
  S: string;
  F: TReplLogField;
  AsgnExt: array of record Path: string; Idx: Integer; end;

  procedure AddAsgnExt(const FI: TReplFieldItem; const aPath: string);
  begin
    if aPath = '' then
      Exit;
    if FI.Options and fldoptExternalFile <> 0 then
    begin
      SetLength(AsgnExt, Length(AsgnExt)+1);
      with AsgnExt[Length(AsgnExt)-1] do
      begin
        Idx:= FI.Idx;
        Path:= aPath;
      end;
    end;
  end;
begin
  SaveDecimalSeparator:= DecimalSeparator;
  SaveThousandSeparator:= ThousandSeparator;
  DecimalSeparator:= '.';
  ThousandSeparator:= ',';
  SetLength(AsgnExt, 0);
  try
    SPFl:= aReplLogRec.FieldDefs.Type_Tgt = 'P';   // to stored procedure
    if aReplOptions and (repoptTargetReplication or repoptReplicateLog) = 0 then
    begin
      if (aReplLogRec.IUD = 'D') and not SPFl then
        SQLS:= Format('DELETE FROM %s WHERE %s;', [TIBReplicator.FormatIdentifier(SQLDialect, aReplLogRec.FieldDefs.RelationName_Tgt), aReplLogRec.FieldDefs.Where_Tgt])
      else
        begin
          SQLS2:= '';
          if SPFl then
            begin
              SQLS:= Format('EXECUTE PROCEDURE %s (', [TIBReplicator.FormatIdentifier(SQLDialect, aReplLogRec.FieldDefs.RelationName_Tgt)]);
              SQLS2:= ''''+aReplLogRec.IUD+'''';
            end
          else if aReplLogRec.IUD = 'I' then
            SQLS:= Format('INSERT INTO %s(', [TIBReplicator.FormatIdentifier(SQLDialect, aReplLogRec.FieldDefs.RelationName_Tgt)])
          else
            SQLS:= Format('UPDATE %s SET ', [TIBReplicator.FormatIdentifier(SQLDialect, aReplLogRec.FieldDefs.RelationName_Tgt)]);

          for I:= 0 to Length(aReplLogRec.FieldDefs.Fields[1])-1 do
            if aReplLogRec.FieldDefs.Is_Updatable_Tgt(1, I, aReplLogRec.IUD) then
            begin
              if I < Length(aReplLogRec.NewPKey) then
                S:= aReplLogRec.NewPKey[I]
              else
                S:= '';
              F:= aReplLogRec.Src_Record[aReplLogRec.FieldDefs.Fields[1, I].Idx];
              PutSQLS(aReplLogRec.FieldDefs.Fields[1, I].Idx, F, True, S);
              AddAsgnExt(aReplLogRec.FieldDefs.Fields[1, I], S);
            end;

          for J:= 2 to High(aReplLogRec.FieldDefs.Fields) do
          begin
            for I:= 0 to Length(aReplLogRec.FieldDefs.Fields[J])-1 do
              if aReplLogRec.FieldDefs.Is_Updatable_Tgt(J, I, aReplLogRec.IUD) then
              begin
                F:= aReplLogRec.FieldQ_Locate('N', aReplLogRec.FieldDefs.Fields[J, I].Idx);
                if (F = nil) and not aReplLogRec.Src_IsEmpty then
                  F:= aReplLogRec.Src_Record[aReplLogRec.FieldDefs.Fields[J, I].Idx];
                PutSQLS(aReplLogRec.FieldDefs.Fields[J, I].Idx, F, False, '');
                AddAsgnExt(aReplLogRec.FieldDefs.Fields[J, I], F.asString);
              end;
          end;

          if SPFl then
            SQLS:= SQLS+SQLS2+')'
          else if aReplLogRec.IUD = 'I' then
            SQLS:= SQLS+')'#13#10+'VALUES('+SQLS2+');'
          else
            SQLS:= SQLS+#13#10+'WHERE '+aReplLogRec.FieldDefs.Where_Tgt+';'
        end;

      if aCounter = 0 then
        S:= '/* '+FormatSQLDT(fReplicator.Now2()) + ' */'#13#10
      else
        S:= '';
      S:= S+'/* '+ FormatSQLSeqId(aReplLogRec.SeqId)+ ' */'#13#10;

      SQLS:= S+SQLS+#13#10;
      Logger.Log(fReplicator.fLogName, lchNull, SQLS);
      if aReplLogRec.IUD <> 'D' then
      begin
        for J:= 2 to High(aReplLogRec.FieldDefs.Fields) do
        begin
          for I:= 0 to Length(aReplLogRec.FieldDefs.Fields[J])-1 do
            if aReplLogRec.FieldDefs.Is_Updatable_Tgt(J, I, aReplLogRec.IUD) then
            begin
              F:= aReplLogRec.FieldQ_Locate('N', aReplLogRec.FieldDefs.Fields[J, I].Idx);
              if F = nil then
              else
                begin
                  if aReplLogRec.Src_FieldExists(aReplLogRec.FieldDefs.Fields[J, I].Idx) then
                    F:= aReplLogRec.Src_Record[aReplLogRec.FieldDefs.Fields[J, I].Idx];
                end;
              if (F <> nil) and F.IsBlob then
              begin
                S:= fReplicator.EncodeBLOBMetaSQL(TIBReplicator.FormatIdentifier(SQLDialect, aReplLogRec.FieldDefs.RelationName_Tgt), TIBReplicator.FormatIdentifier(SQLDialect, aReplLogRec.FieldDefs.Fields[J, I].FieldName_Tgt), aReplLogRec.FieldDefs.Where_Tgt, F.DataType <> ftMemo, F.asString);
                if S <> '' then
                  Logger.Log(fReplicator.fLogName, lchNull, S);
              end;
            end;
        end;
        for I:= 0 to Length(AsgnExt)-1 do
          with AsgnExt[I] do
            WriteExtFiles(aReplLogRec, Idx, Path);
        SetLength(AsgnExt, 0);
      end;
      Inc(aCounter);
    end;
  finally
    DecimalSeparator:= SaveDecimalSeparator;
    ThousandSeparator:= SaveThousandSeparator;
  end;
end;

procedure TReplTargetDatabase_Logger.SynchronizeTable(
  const aSyncRec: TSyncRecord; aSrcDB: TIBDatabase;
  var aCounters: TSyncRecordCounters);
var
  RFS: TReplFieldDefs;
  SQLS, S: string;
  CRI: TReplCachedRelationItem;
  SQ: TIBDataSet2;
  I: Integer;
begin
  if aSyncRec.Action.Action and (saInsDel or saUpd) = 0 then
    Exit;
  if fReplicator.ReadCachedFields(aSyncRec.SchemaId, aSyncRec.GroupId, aSyncRec.RelationId, True, CRI) then { any definition ? }
  begin
    if CRI.TType = 'P' then   // to stored procedure
      IBReplicatorError(Format(sCannotSynchronizeToStoredProc, [CRI.Name]), 2);

    RFS:= TReplFieldDefs.Create;
    try
      SQ:= TIBDataSet2.Create(nil);
      try
        SQ.Database:= aSrcDB;
        SQ.Transaction:= SQ.Database.DefaultTransaction;
        if not SQ.Transaction.InTransaction then
          fReplicator.SafeStartTransaction(SQ.Transaction);
        RFS.BuildFieldDefs(CRI, SQ.Database.SQLDialect, SQLDialect);
        if (aSyncRec.Action.Action and saSrcTgt = 0) and (aSyncRec.Action.Action and saInsDel <> 0) then
        begin
          S:= '';
          for I:= 0 to Length(RFS.Fields[1])-1 do
          begin   // order by primary key
            if S <> '' then
              S:= S+','
            else
              S:= 'ORDER BY ';
            S:= S+TIBReplicator.FormatIdentifier(SQ.Database.SQLDialect, RFS.Fields[1][I].FieldName_Src);
          end;
          if aSyncRec.Action.Where_Src <> '' then
            S:= 'WHERE '+aSyncRec.Action.Where_Src+' '+S;
          SQ.SelectSQL.Text:= Format('SELECT * FROM %s T %s;', [TIBReplicator.FormatIdentifier(SQ.Database.SQLDialect, RFS.RelationName_Src), S]);
          fReplicator.SafeOpen(SQ);

          while not SQ.EOF and not fReplicator.ForceStop do
          begin
            SQLS:= MakeInsSQL(RFS, SQ, SQLDialect);
            if aSyncRec.Action.Action and saRO <> 0 then
              fReplicator.fReplLog.Log(fReplicator.fLogName, lchNull, SQLS)
            else
              begin
                Logger.Log(fReplicator.fLogName, lchNull, SQLS+#13#10);
                Inc(aCounters.Inserted);
              end;
            fReplicator.SafeNext(SQ);
            Inc(aCounters.Total);
            fReplicator.ProcessMessages;
          end;
        end;
      finally
        SQ.Free;
      end;
    finally
      RFS.Free;
    end;
  end;
end;

{ TReplLogField }

function TReplLogField.GetDataType_F: Char;
var
  FT: TFieldType;
begin
  if IsNull then
    Result:= 'N'
  else
    begin
      FT:= DataType;
      case FT of
        ftSmallInt, ftBCD, ftInteger:
          Result:= 'I';
        ftDate, ftTime, ftDateTime:
          Result:= 'T';
        ftMemo:
          Result:= 'M';
        ftBlob:
          Result:= 'B';
        ftFloat:
          Result:= 'D';
      else  // ftString, ftLargeInt, ftUnknown
        Result:= 'S';
      end;
    end;
end;

function TReplLogField.IsBlob: Boolean;
begin
  Result:= DataType in [ftBlob, ftMemo];
end;

{ TReplLogField_IBSQL }

function TReplLogField_IBSQL.GetAsString: string;
begin
  if IBXSQLVAR <> nil then
    Result:= IBXSQLVAR.asString
  else
    Result:= '';
end;

function TReplLogField_IBSQL.GetAsVariant: Variant;
begin
  if IBXSQLVAR = nil then
    Result:= Unassigned
  else if IsBlob or (DataType = ftLargeInt) then
    Result:= asString
  else
    Result:= IBXSQLVAR.Value;
end;

function TReplLogField_IBSQL.IsNull: Boolean;
begin
  Result:= (IBXSQLVAR = nil) or IBXSQLVAR.IsNull;
end;

function TReplLogField_IBSQL.GetDataType: TFieldType;
begin
  Result:= ftUnknown;
  if IBXSQLVAR = nil then
    Exit;
  case IBXSQLVAR.SQLType of
    SQL_VARYING, SQL_TEXT:
      Result:= ftString;
    SQL_DOUBLE, SQL_FLOAT:
      Result:= ftFloat;
    SQL_SHORT:
      begin
        if (IBXSQLVAR.{$IFDEF IBREPL_SQLVAR}SQLVar{$ELSE}asXSQLVar{$ENDIF}.sqlscale = 0) then
          Result:= ftSmallInt
        else
          Result:= ftBCD;
      end;
    SQL_LONG:
      begin
        if (IBXSQLVAR.{$IFDEF IBREPL_SQLVAR}SQLVar{$ELSE}asXSQLVar{$ENDIF}.sqlscale = 0) then
          Result:= ftInteger
        else if (IBXSQLVAR.{$IFDEF IBREPL_SQLVAR}SQLVar{$ELSE}asXSQLVar{$ENDIF}.sqlscale >= (-4)) then
          Result:= ftBCD
        else
          Result:= ftFloat;
      end;
    SQL_INT64:
      begin
        if (IBXSQLVAR.{$IFDEF IBREPL_SQLVAR}SQLVar{$ELSE}asXSQLVar{$ENDIF}.sqlscale = 0) then
          Result:= ftLargeInt
        else if (IBXSQLVAR.{$IFDEF IBREPL_SQLVAR}SQLVar{$ELSE}asXSQLVar{$ENDIF}.sqlscale >= (-4)) then
          Result:= ftBCD
        else
          Result:= ftFloat;
      end;
    SQL_TIMESTAMP:
      Result:= ftDateTime;
    SQL_TYPE_TIME:
      Result:= ftTime;
    SQL_TYPE_DATE:
      Result:= ftDate;
    SQL_BLOB:
      begin
        if (IBXSQLVAR.{$IFDEF IBREPL_SQLVAR}SQLVar{$ELSE}asXSQLVar{$ENDIF}.sqlsubtype = 1) then
          Result:= ftMemo
        else
          Result:= ftBlob;
      end;
  end;
end;

function TReplLogField_IBSQL.GetAsDateTime: TDateTime;
begin
  Result:= IBXSQLVAR.asDateTime;
end;

function TReplLogField_IBSQL.GetAsFloat: Extended;
begin
  Result:= IBXSQLVAR.asFloat;
end;

function TReplLogField_IBSQL.GetAsInt64: Int64;
begin
  Result:= IBXSQLVAR.asInt64;
end;

function TReplLogField_IBSQL.GetAsInteger: Longint;
begin
  Result:= IBXSQLVAR.asInteger;
end;

{ TReplLogField_Offline }

function TReplLogField_Offline.GetAsDateTime: TDateTime;
var
  V: Variant;
begin
  V:= asVariant;
  if not VarIsNull(V) and not VarIsEmpty(V) then
    Result:= VarToDateTime(V)
  else
    Result:= 0;
end;

function TReplLogField_Offline.GetAsFloat: Extended;
var
  V: Variant;
begin
  V:= asVariant;
  if not VarIsNull(V) and not VarIsEmpty(V) then
    Result:= V
  else
    Result:= 0;
end;

function TReplLogField_Offline.GetAsInt64: Int64;
var
  RRV: TOffReplRecValue;
begin
  Result:= 0;
  if GetRRV(RRV) then
  begin
    fSQRecord^.Buffer.ReadBuffer(RRV, SizeOf(RRV));
    fSQRecord^.Buffer.ReadBuffer(Result, SizeOf(Result));
  end;
end;

function TReplLogField_Offline.GetAsInteger: Longint;
var
  V: Variant;
begin
  V:= asVariant;
  if not VarIsNull(V) and not VarIsEmpty(V) then
    Result:= V
  else
    Result:= 0;
end;

function TReplLogField_Offline.GetAsString: string;
var
  V: Variant;
begin
  Result:= '';
  V:= asVariant;
  if VarIsEmpty(V) or VarIsNull(V) then
    Exit;
  Result:= string(V);  // locale ThausandSeparator, ...
end;

function TReplLogField_Offline.GetAsVariant: Variant;
var
  RRV: TOffReplRecValue;
begin
  Result:= Unassigned;
  if not GetRRV(RRV) then
    Exit;
  Result:= TIBReplicator.GetOffReplVariant(fSQRecord^.Buffer, fSQRecord^.UTF8);
end;

function TReplLogField_Offline.GetDataType: TFieldType;
var
  RRV: TOffReplRecValue;
begin
  if GetRRV(RRV) then
    Byte(Result):=  RRV.FieldType
  else
    Result:= ftUnknown;
end;

function TReplLogField_Offline.GetRRV(
  var RRV: TOffReplRecValue): Boolean;
begin
  Result:= (fSQRecord <> nil) and (fSQRecord.Pos[fFieldIdx] >= 0);
  if Result then
  begin
    fSQRecord^.Buffer.Position:= fSQRecord^.Pos[fFieldIdx];
    fSQRecord^.Buffer.ReadBuffer(RRV, SizeOf(RRV));
    fSQRecord^.Buffer.Position:= fSQRecord^.Pos[fFieldIdx];
  end;
end;

function TReplLogField_Offline.IsNull: Boolean;
var
  RRV: TOffReplRecValue;
begin
  Result:= not GetRRV(RRV) or (RRV.DataType = 'N');
end;

{ TReplFieldDefs }

destructor TReplFieldDefs.Destroy;
var
  I: Integer;
begin
  inherited;
  for I:= Low(Fields) to High(Fields) do
    SetLength(Fields[I], 0);
  SetLength(FieldRefs, 0);
end;

procedure TReplFieldDefs.BuildFieldDefs(CRI: TReplCachedRelationItem;
  aSQLDialect_Src, aSQLDialect_Tgt: Integer);
var
  I, J: Integer;
  S: string;
  FT: Byte;
begin
  RelationName_Src:= CRI.Name;
  RelationName_Tgt:= CRI.TargetName;
  Type_Tgt:= CRI.TType;
  RelationOptions:= CRI.Options;
  for I:= 0 to CRI.Fields.Count-1 do
  begin
    FT:= TReplCachedFieldItem(CRI.Fields[I]).FType;
    J:= Length(Fields[FT]);
    SetLength(Fields[FT], J+1);
    SetLength(FieldRefs, Length(FieldRefs)+1);
    with FieldRefs[Length(FieldRefs)-1] do
    begin
      FieldType:= FT;
      Idx:= J;
    end;
    with Fields[FT][J] do
    begin
      Idx:= I;
      Options:= TReplCachedFieldItem(CRI.Fields[I]).Options;
      FieldName_Src:= TReplCachedFieldItem(CRI.Fields[I]).Name;
      FieldName_Tgt:= TReplCachedFieldItem(CRI.Fields[I]).TargetName;
      FieldId:= TReplCachedFieldItem(CRI.Fields[I]).Id;
      if (InsertSQL_1 <> '') and (Options and fldoptDoNotInsert = 0) then
      begin
        InsertSQL_1:= InsertSQL_1+',';
        InsertSQL_2:= InsertSQL_2+',';
      end;
      if (ModifySQL <> '') and (Options and fldoptDoNotUpdate = 0) then
        ModifySQL:= ModifySQL+',';
      S:= TIBReplicator.FormatIdentifier(aSQLDialect_Tgt, FieldName_Tgt);
      if Options and fldoptDoNotInsert = 0 then
      begin
        InsertSQL_1:= InsertSQL_1+S;
        InsertSQL_2:= InsertSQL_2+':'+S;
      end;
      if Options and fldoptDoNotUpdate = 0 then
        ModifySQL:= ModifySQL+S+'=:'+S;
    end;
  end;
end;

function TReplFieldDefs.Is_Updatable_Tgt(aFieldType: Byte; Idx: Integer; IUD: Char): Boolean;
begin
  Result:= (Type_Tgt = 'P') or (IUD <> 'U') or (Fields[aFieldType, Idx].Options and fldoptDoNotUpdate = 0) and (IUD <> 'I') or (Fields[aFieldType, Idx].Options and fldoptDoNotInsert = 0);
end;

procedure TReplFieldDefs.ClearTags(aTagMask: Integer);
var
  I, J: Integer;
begin
  for I:= Low(Fields) to High(Fields) do
  begin
    for J:= 0 to Length(Fields[I])-1 do
      with Fields[I][J] do
        Tag:= Tag and aTagMask;
  end;
end;

procedure TReplFieldDefs.GetWhereParams(aSQLDialect_Src,
  aSQLDialect_Tgt: Integer; const aPrefix_Tgt: string; var aWhere_Src,
  aWhere_Tgt: string);
var
  I: Integer;
begin
  aWhere_Src:= '';
  aWhere_Tgt:= '';
  for I:= 0 to Length(Fields[1])-1 do
  begin
    if aWhere_Src <> '' then
    begin
      aWhere_Src:= aWhere_Src + ' AND ';
      aWhere_Tgt:= aWhere_Tgt + ' AND ';
    end;
    with Fields[1][I] do
    begin
      aWhere_Src:= aWhere_Src + Format('%s=:%s', [TIBReplicator.FormatIdentifier(aSQLDialect_Src, FieldName_Src), TIBReplicator.FormatIdentifier(aSQLDialect_Src, FieldName_Src)]);
      aWhere_Tgt:= aWhere_Tgt + Format('%s=:%s', [TIBReplicator.FormatIdentifier(aSQLDialect_Tgt, FieldName_Tgt), TIBReplicator.FormatIdentifier(aSQLDialect_Tgt, aPrefix_Tgt+FieldName_Tgt)]);
    end;
  end;
end;

procedure TReplFieldDefs.GetWhereValues(aSQLDialect_Src,
  aSQLDialect_Tgt: Integer; IUD: Char; const aOldPKey,
  aNewPKey: TStringOpenArray; var aWhere_Src, aWhere_Tgt, aWhere_Tgt_New: string);
var
  I: Integer;
  OldS, NewS: string;
begin
  aWhere_Src:= '';
  aWhere_Tgt:= '';
  aWhere_Tgt_New:= '';
  for I:= 0 to Length(Fields[1])-1 do
  begin
    if aWhere_Src <> '' then
    begin
      aWhere_Src:= aWhere_Src + ' AND ';
      aWhere_Tgt:= aWhere_Tgt + ' AND ';
      aWhere_Tgt_New:= aWhere_Tgt_New + ' AND ';
    end;
    if I >= Length(aOldPKey) then
      OldS:= ''
    else
      OldS:= AddApostrophs(aOldPKey[I]);
    if I >= Length(aNewPKey) then
      NewS:= ''
    else
      NewS:= AddApostrophs(aNewPKey[I]);
    with Fields[1][I] do
    begin
      aWhere_Src:= aWhere_Src + Format('(%s=''%s'')', [TIBReplicator.FormatIdentifier(aSQLDialect_Src, FieldName_Src), iif(IUD in ['I','U'], NewS, OldS)]);
      aWhere_Tgt:= aWhere_Tgt + Format('(%s=''%s'')', [TIBReplicator.FormatIdentifier(aSQLDialect_Tgt, FieldName_Tgt), iif(IUD in ['I'],     NewS, OldS)]);
      aWhere_Tgt_New:= aWhere_Tgt_New + Format('(%s=''%s'')', [TIBReplicator.FormatIdentifier(aSQLDialect_Tgt, FieldName_Tgt), NewS]);
    end;
  end;
end;

{ TReplLogRecord }

constructor TReplLogRecord.Create;
begin
  inherited Create;
  fReplicator:= aReplicator;
  FieldDefs:= TReplFieldDefs.Create;
end;

destructor TReplLogRecord.Destroy;
var
  I: Integer;
begin
  inherited;
  FreeAndNil(FieldDefs);
  for I:= 0 to Src_RecordCount-1 do
    fSrc_Record[I].Free;  // not Src_Record[] !!!
  SetLength(fSrc_Record, 0);
  SetLength(OldPKey, 0);
  SetLength(NewPKey, 0);
  SetLength(NewFKey, 0);
  SetLength(Externals, 0);
end;

function TReplLogRecord.Src_RecordCount: Integer;
begin
  Result:= Length(fSrc_Record);
end;

function TReplLogRecord.Src_FieldExists;
begin
  Result:= fSrc_Record[Idx] <> nil;
end;

function TReplLogRecord.GetSrc_Record;
const
  sFieldNotFound = 'Field "%s" not found in source relation "%s"';
begin
  if not Src_FieldExists(Idx) then
    with FieldDefs, FieldDefs.FieldRefs[Idx] do
      IBReplicatorError(Format(sFieldNotFound, [Fields[FieldType, Idx].FieldName_Src, RelationName_Src]), 1);
  Result:= fSrc_Record[Idx];
end;

{ TReplLogRecord_Online }

constructor TReplLogRecord_Online.Create;
begin
  inherited;
  fFieldQReplField:= TReplLogField_IBSQL.Create;
end;

destructor TReplLogRecord_Online.Destroy;
begin
  FreeExternals;
  inherited;
  fFieldQReplField.Free;
end;

procedure TReplLogRecord_Online.AssignReplQ;
begin
  SeqId:= aReplQ.FieldByName('SEQID').asInteger;
  SchemaId:= aReplQ.FieldByName('SCHEMAID').asInteger;
  GroupId:= aReplQ.FieldByName('GROUPID').asInteger;
  RelationId:= aReplQ.FieldByName('RELATIONID').asInteger;
  IUD:= (aReplQ.FieldByName('REPLTYPE').asString+' ')[1];
  Separator:= (aReplQ.FieldByName('SEPARATOR').asString+' ')[1];
  Stamp:= aReplQ.FieldByName('STAMP').asDateTime;
  OldPKey:= StringToArray(aReplQ.FieldByName('OLDPKEY').asString, Separator);
  NewPKey:= StringToArray(aReplQ.FieldByName('NEWPKEY').asString, Separator);
end;

procedure TReplLogRecord_Online.AssignFieldQ;
begin
  fFieldQ:= aFieldQ;
  fFieldQIsEmpty:= aFieldQ.EOF;
end;

procedure TReplLogRecord_Online.FreeExternals;
var
  I: Integer;
begin
  for I:= 0 to Length(Externals)-1 do
    FreeAndNil(Externals[I].Data);
  SetLength(Externals, 0);
end;

procedure TReplLogRecord_Online.AssignSQ(SQ: TIBSQL);
var
  I, J: Integer;
  S: string;
begin
  for I:= 0 to Length(fSrc_Record)-1 do
    FreeAndNil(fSrc_Record[I]);
  SetLength(fSrc_Record, Length(FieldDefs.FieldRefs));
  fSrc_IsEmpty:= SQ.EOF;
  for I:= 0 to Length(FieldDefs.FieldRefs)-1 do
  begin
    with FieldDefs, FieldRefs[I] do
      S:= Fields[FieldType, Idx].FieldName_Src;
    J:= SQ.FieldIndex[S];
    if J >= 0 then
    begin
      fSrc_Record[I]:= TReplLogField_IBSQL.Create;
      TReplLogField_IBSQL(fSrc_Record[I]).IBXSQLVAR:= SQ.Fields[J];
    end;
  end;
end;

procedure TReplLogRecord_Online.AssignExtFileQ(ExtFileQ: TIBSQL);
var
  FldId, FldIdx, I: Integer;
begin
  FldId:= -1;
  FldIdx:= -1;
  while not ExtFileQ.EOF do
  begin
    if FldId <> ExtFileQ.FieldByName('FIELDID').asInteger then
    begin
      FldId:= ExtFileQ.FieldByName('FIELDID').asInteger;
      for I:= 0 to Length(FieldDefs.FieldRefs)-1 do
        with FieldDefs.FieldRefs[I] do
          if FieldDefs.Fields[FieldType, Idx].FieldId = FldId then
          begin
            FldIdx:= I;
            Break;
          end;
    end;
    if FldIdx >= 0 then
    begin
      SetLength(Externals, Length(Externals)+1);
      with Externals[Length(Externals)-1] do
      begin
        Idx:= FldIdx;
        DataPos:= 0;
        FileName:= ExtFileQ.FieldByName('NAME').asString;
        Stamp:= ExtFileQ.FieldByName('STAMP').asDateTime;
        try
          // copy it through memory streams because data are in different records (otherwise record location necessary when using Data - difficult implementation)
          Data:= TMemoryStream.Create();
          ExtFileQ.FieldByName('DATA').SaveToStream(Data);
          DataSize:= Data.Size;
        except
          on E: Exception do
          begin
            FreeAndNil(Data);
            fReplicator.fDBLog.Log(fReplicator.fLogName, lchError, E.Message);
          end;
        end;
      end;
    end;
    ExtFileQ.Next;
  end;
end;

procedure TReplLogRecord_Online.AssignExtFiles(const aDBProps: TReplDatabaseProperties);
  procedure PutExtFile(FldIdx: Integer; FName: string);
  var
    I: Integer;
    SR: TSearchRec;
    Found: Integer;
    Path: string;
  begin
    I:= Pos(';', FName);
    if I > 0 then
      Delete(FName, I, MaxInt);   // strip params
    if aDBProps.ExtFilePath <> '' then
      FName:= {$IFDEF VER130}IncludeTrailingBackslash{$ELSE}IncludeTrailingPathDelimiter{$ENDIF}(aDBProps.ExtFilePath)+Trim(FName);
    FName:= Trim(fReplicator.ParseStr(FName));
    Path:= {$IFDEF VER130}IncludeTrailingBackslash{$ELSE}IncludeTrailingPathDelimiter{$ENDIF}(ExtractFilePath(FName));

    Found:= FindFirst(FName, faAnyFile and not faDirectory, SR);
    if Found = 0 then
    begin
      try
        while Found = 0 do
        begin
          SetLength(Externals, Length(Externals)+1);
          with Externals[Length(Externals)-1] do
          begin
            Idx:= FldIdx;
            DataPos:= 0;
            DataSize:= SR.Size;
            FileName:= SR.Name;
            Stamp:= FileDateToDateTime(SR.Time);
            try
              Data:= TFileStream.Create(Path+SR.Name, fmOpenRead or fmShareDenyWrite);
            except
              on E: Exception do
              begin
                FreeAndNil(Data);
                fReplicator.fDBLog.Log(fReplicator.fLogName, lchError, E.Message);
              end;
            end;
          end;
          Found:= FindNext(SR);
        end;
      finally
        FindClose(SR);
      end;
    end;
  end;
var
  I: Integer;
  F: TReplLogField;
  S: string;
begin
  FreeExternals;

  for I:= 0 to Length(FieldDefs.FieldRefs)-1 do
  begin
    with FieldDefs.FieldRefs[I] do
    begin
      if FieldDefs.Fields[FieldType][Idx].Options and fldoptExternalFile = 0 then
        Continue;
    end;
    F:= FieldQ_Locate('N', I);
    if F = nil then
      F:= Src_Record[I];
    if (F = nil) or (F.asString = '') then
      Continue;
    S:= F.asString;
    if (IUD = 'U') then
      begin
        F:= FieldQ_Locate('O', I);
        if (F = nil) or (F.asString <> S) then
        begin
          PutExtFile(I, S);
        end;
      end
    else
      begin
        PutExtFile(I, S);
      end;
  end;
end;

function TReplLogRecord_Online.FieldQ_Locate(aNew: Char;
  aIdx: Integer): TReplLogField;
var
  I: Integer;
begin
  with FieldDefs, FieldRefs[aIdx] do
    I:= Fields[FieldType, Idx].FieldId;
  if fReplicator.LocateFieldQuery(fFieldQ, aNew, I) then
    begin
      fFieldQReplField.IBXSQLVAR:= fFieldQ.FieldByName('DATA_'+fFieldQ.FieldByName('DATATYPE').asString);
      Result:= fFieldQReplField;
    end
  else
    Result:= nil;
end;

function TReplLogRecord_Online.FieldQ_IsEmpty: Boolean;
begin
  Result:= fFieldQIsEmpty;
end;

function TReplLogRecord_Online.GetSrc_IsEmpty: Boolean;
begin
  Result:= fSrc_IsEmpty;
end;

{ TReplLogRecord_Offline }

constructor TReplLogRecord_Offline.Create(aReplicator: TIBReplicator);
begin
  inherited;
  fFieldQReplField:= TReplLogField_Offline.Create;
end;

destructor TReplLogRecord_Offline.Destroy;
begin
  inherited;
  SQRecord.Buffer.Free;
  FOldQRecord.Buffer.Free;
  FNewQRecord.Buffer.Free;
  fFieldQReplField.Free;
end;

procedure TReplLogRecord_Offline.Assign(const aHeader: TOffHeader;
  const aReplLog: TOffSrcReplLog);
var
  I: Integer;
begin
  SeqId:= aReplLog.SeqId;
  SchemaId:= aHeader.SchemaId;
  GroupId:= aHeader.GroupId;
  RelationId:= aReplLog.RelationId;
  IUD:= aReplLog.RepType;
  Separator:= aReplLog.Sep;
  Stamp:= OffStamp2DT(aReplLog.Stamp);
  SetLength(fSrc_Record, Length(FieldDefs.FieldRefs));
  for I:= 0 to Length(FieldDefs.FieldRefs)-1 do
  begin
    if (SQRecord.Buffer <> nil) and (SQRecord.Pos[I] >= 0) then
      begin
        fSrc_Record[I]:= TReplLogField_Offline.Create;
        with TReplLogField_Offline(fSrc_Record[I]) do
        begin
          fFieldIdx:= I;
          fSQRecord:= @Self.SQRecord;
        end;
      end
    else
      fSrc_Record[I]:= nil;
  end;
end;

function TReplLogRecord_Offline.FieldQ_Locate(aNew: Char;
  aIdx: Integer): TReplLogField;
var
  P: PReplOffRecordBuffer;
  RRV: TOffReplRecValue;
begin
  if aNew = 'N' then
    P:= @FNewQRecord
  else
    P:= @FOldQRecord;
  if (P^.Buffer <> nil) and (P^.Pos[aIdx] >= 0) then
    begin
      fFieldQReplField.fSQRecord:= P;
      fFieldQReplField.fFieldIdx:= aIdx;
      if TReplLogField_Offline(fFieldQReplField).GetRRV(RRV) and (RRV.DataType <> ' ') then
        Result:= fFieldQReplField
      else
        Result:= nil;
    end
  else
    Result:= nil;
end;

function TReplLogRecord_Offline.FieldQ_IsEmpty: Boolean;
begin
  Result:= (FOldQRecord.Buffer = nil) and (FNewQRecord.Buffer = nil);
end;

function TReplLogRecord_Offline.GetSrc_IsEmpty: Boolean;
begin
  Result:= SQRecord.Buffer = nil;
end;

{ TSortedClassList }

procedure TSortedClassList.Notify(Ptr: Pointer; Action: TListNotification);
begin
  if Action = lnDeleted then
  begin
    fLastFindId:= -1;  // invalidate
    if TSortedClassItem(Ptr).Id < fCurItem.Id then
      begin
        Dec(fCurIdx);
      end
    else if Ptr = fCurItem then
      begin
        if fCurIdx < Count then
          begin
            fCurItem:= Items[fCurIdx];
          end
        else if fCurIdx > 0 then
          begin
            Dec(fCurIdx);
            fCurItem:= Items[fCurIdx];
          end
        else
          begin
            fCurItem:= nil;
          end;
      end;
    TObject(Ptr).Free;
  end;
  inherited Notify(Ptr, Action);
end;

function TSortedClassList.Find(const aId: Integer): TSortedClassItem;
var
  L, H: Integer;
begin
  Result:= nil;
  if (aId < 0) or (fCurItem = nil) then
  begin
    Exit;
  end;
  if (aId = fCurItem.Id) then
    begin
      Result:= fCurItem;
    end
  else if (fLastFindId = aId) then
    begin
      Exit;  // no seek since last find
    end
  else
    begin
      fLastFindId:= aId;  // if not found seek to nearest record and remember id to optimize next Find (called from Add)
      L:= 0; H:= Count-1;
      repeat
        if aId < fCurItem.Id then
          begin
            H:= fCurIdx;
            L:= Max(L, fCurIdx-(fCurItem.Id - aId));
          end
        else // if aId > fCurItem.Id
          begin
            L:= fCurIdx + 1;
            if L >= Count then
            begin
              Break;
            end;
            H:= Min(H, fCurIdx+(aId - fCurItem.Id));
          end;
        fCurIdx:= (L + H) div 2;
        fCurItem:= Items[fCurIdx];
      until (fCurItem.Id = aId) or (L >= H);
      if (fCurItem.Id = aId) then
        Result:= fCurItem;
    end;
end;

procedure TSortedClassList.Add(aItem: TSortedClassItem);
resourcestring
  sDuplicateId = 'Duplicate Id (%d)';
begin
  if fCurItem = nil then
    fCurIdx:= 0
  else
    begin
      if Find(aItem.Id) <> nil then   // seek to nearest (insert) pos
        IBReplicatorError(Format(sDuplicateId, [aItem.Id]), 0);
      if (fCurIdx = Count-1) and (aItem.Id > fCurItem.Id) then  // exception if it's the last item
        Inc(fCurIdx);
    end;
  Insert(fCurIdx, aItem);
  fCurItem:= aItem;
  fLastFindId:= -1;
end;

{ TReplCachedSchemaItem }

constructor TReplCachedSchemaItem.Create;
begin
  inherited;
  Groups:= TSortedClassList.Create;
end;

destructor TReplCachedSchemaItem.Destroy;
begin
  Groups.Free;
  inherited;
end;

{ TReplCachedGroupItem }

constructor TReplCachedGroupItem.Create;
begin
  inherited;
  Relations:= TSortedClassList.Create;
end;

destructor TReplCachedGroupItem.Destroy;
begin
  Relations.Free;
  inherited;
end;

{ TReplCachedRelationItem }

constructor TReplCachedRelationItem.Create;
begin
  inherited;
  Fields:= TSortedClassList.Create;
end;

destructor TReplCachedRelationItem.Destroy;
begin
  Fields.Free;
  inherited;
end;

begin
  TIBReplicator.IndexReservedWords;
  SetLength(EmptyStringArray, 1);  // there is some problem in zero length dynamic arrays, oleaut32 error
  SetLength(EmptyVariantArray, 1);
end.


