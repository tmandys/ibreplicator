object ServerTaskSettingsDialog: TServerTaskSettingsDialog
  Left = 418
  Top = 210
  HelpContext = 21200
  BorderStyle = bsDialog
  Caption = 'Task settings'
  ClientHeight = 501
  ClientWidth = 374
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object Label7: TLabel
    Left = 0
    Top = 0
    Width = 374
    Height = 32
    Align = alTop
    Caption = 
      'Parameters indicated by blue color may override default settings' +
      ' from global settings dialog.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    WordWrap = True
  end
  object OKBtn: TButton
    Left = 40
    Top = 456
    Width = 92
    Height = 31
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
  object CancelBtn: TButton
    Left = 144
    Top = 456
    Width = 92
    Height = 31
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
  object HelpBtn: TButton
    Left = 248
    Top = 456
    Width = 92
    Height = 31
    Caption = '&Help'
    TabOrder = 3
    OnClick = HelpBtnClick
  end
  object PageControl2: TPageControl
    Left = 0
    Top = 32
    Width = 374
    Height = 409
    ActivePage = TabSheet2
    Align = alTop
    TabOrder = 0
    object TabSheet4: TTabSheet
      Caption = 'Common'
      ImageIndex = 3
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 68
        Height = 16
        Caption = 'Task name'
        FocusControl = TaskNameEdit
      end
      object Label21: TLabel
        Left = 16
        Top = 64
        Width = 57
        Height = 16
        Caption = 'Comment'
        FocusControl = Comment
      end
      object TaskNameEdit: TEdit
        Left = 16
        Top = 32
        Width = 321
        Height = 24
        TabOrder = 0
      end
      object Comment: TMemo
        Left = 16
        Top = 80
        Width = 321
        Height = 281
        TabOrder = 1
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'General'
      object Label6: TLabel
        Left = 16
        Top = 16
        Width = 32
        Height = 16
        Caption = 'Type'
      end
      object Label4: TLabel
        Left = 16
        Top = 64
        Width = 76
        Height = 16
        Caption = 'SchemaId(s)'
      end
      object Label9: TLabel
        Left = 176
        Top = 64
        Width = 63
        Height = 16
        Caption = 'GroupId(s)'
      end
      object Label11: TLabel
        Left = 176
        Top = 120
        Width = 65
        Height = 16
        Caption = 'TgtDBId(s)'
      end
      object Label17: TLabel
        Left = 176
        Top = 16
        Width = 58
        Height = 16
        Caption = 'Replicate'
        FocusControl = ReplOpt
      end
      object Label18: TLabel
        Left = 16
        Top = 120
        Width = 65
        Height = 16
        Caption = 'SrcDBId(s)'
      end
      object RType: TComboBox
        Left = 16
        Top = 32
        Width = 145
        Height = 24
        Style = csDropDownList
        ItemHeight = 16
        TabOrder = 0
        OnChange = RTypeChange
        Items.Strings = (
          'online'
          'offline - source'
          'offline - target'
          'target'
          'synchronization')
      end
      object SchemaId: TEdit
        Left = 16
        Top = 80
        Width = 121
        Height = 24
        TabOrder = 2
        OnKeyDown = SchemaIdKeyDown
      end
      object GroupId: TEdit
        Left = 176
        Top = 80
        Width = 129
        Height = 24
        TabOrder = 4
        OnKeyDown = SchemaIdKeyDown
      end
      object TgtDBId: TEdit
        Left = 176
        Top = 136
        Width = 129
        Height = 24
        TabOrder = 8
        OnKeyDown = SchemaIdKeyDown
      end
      object NowAsUTC: TCheckBox
        Left = 16
        Top = 168
        Width = 152
        Height = 21
        AllowGrayed = True
        Caption = 'Now as UTC'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        State = cbGrayed
        TabOrder = 9
      end
      object ExtConflict: TCheckBox
        Left = 16
        Top = 200
        Width = 152
        Height = 21
        Caption = 'Ext.conflict check'
        TabOrder = 10
      end
      object ReplOpt: TComboBox
        Left = 176
        Top = 32
        Width = 117
        Height = 24
        Style = csDropDownList
        ItemHeight = 16
        TabOrder = 1
        OnChange = RTypeChange
        Items.Strings = (
          'records'
          'replication log')
      end
      object RepToSource: TCheckBox
        Left = 176
        Top = 168
        Width = 152
        Height = 21
        Caption = 'Report to source'
        TabOrder = 11
      end
      object RepToTarget: TCheckBox
        Left = 176
        Top = 200
        Width = 152
        Height = 21
        Caption = 'Report to target'
        TabOrder = 12
      end
      object SrcDBId: TEdit
        Left = 16
        Top = 136
        Width = 121
        Height = 24
        TabOrder = 6
        OnKeyDown = SchemaIdKeyDown
      end
      object SchemaIdButton: TButton
        Left = 136
        Top = 80
        Width = 24
        Height = 24
        Caption = '...'
        TabOrder = 3
        TabStop = False
        OnClick = SchemaIdButtonClick
      end
      object GroupIdButton: TButton
        Left = 304
        Top = 80
        Width = 24
        Height = 24
        Caption = '...'
        TabOrder = 5
        TabStop = False
        OnClick = GroupIdButtonClick
      end
      object SrcDBIdButton: TButton
        Left = 136
        Top = 136
        Width = 24
        Height = 24
        Caption = '...'
        TabOrder = 7
        TabStop = False
        OnClick = SrcDBIdButtonClick
      end
      object TgtDBIdButton: TButton
        Left = 304
        Top = 136
        Width = 24
        Height = 24
        Caption = '...'
        TabOrder = 13
        TabStop = False
        OnClick = TgtDBIdButtonClick
      end
    end
    object TabSheet5: TTabSheet
      Caption = 'Offline'
      ImageIndex = 4
      object Label13: TLabel
        Left = 16
        Top = 16
        Width = 149
        Height = 16
        Caption = 'Offline package directory'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object OfflineDir: TEdit
        Left = 16
        Top = 32
        Width = 313
        Height = 24
        TabOrder = 0
      end
      object ReceiveData: TCheckBox
        Left = 16
        Top = 72
        Width = 149
        Height = 21
        Caption = 'Receive data'
        TabOrder = 1
      end
      object SendData: TCheckBox
        Left = 16
        Top = 96
        Width = 149
        Height = 21
        Caption = 'Send data'
        TabOrder = 2
      end
      object ResendData: TCheckBox
        Left = 16
        Top = 120
        Width = 149
        Height = 21
        Caption = 'Resend data'
        TabOrder = 3
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Log'
      ImageIndex = 1
      object Label19: TLabel
        Left = 16
        Top = 328
        Width = 60
        Height = 16
        Caption = 'Log name'
        FocusControl = LogName
      end
      object GroupBox1: TGroupBox
        Left = 16
        Top = 192
        Width = 337
        Height = 129
        Caption = 'Repl log'
        TabOrder = 1
        object Label2: TLabel
          Left = 16
          Top = 24
          Width = 167
          Height = 16
          Caption = 'File name ("NUL" = disable)'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label3: TLabel
          Left = 16
          Top = 72
          Width = 117
          Height = 16
          Caption = 'Max file size [Bytes]'
          FocusControl = ReplLogMaxSize
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label10: TLabel
          Left = 168
          Top = 72
          Width = 97
          Height = 16
          Caption = 'Rotate log count'
          FocusControl = ReplLogRotate
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object ReplLog: TEdit
          Left = 16
          Top = 40
          Width = 305
          Height = 24
          TabOrder = 0
        end
        object ReplLogMaxSize: TEdit
          Left = 16
          Top = 88
          Width = 113
          Height = 24
          TabOrder = 1
        end
        object ReplLogRotate: TEdit
          Left = 168
          Top = 88
          Width = 80
          Height = 24
          TabOrder = 2
        end
      end
      object GroupBox2: TGroupBox
        Left = 16
        Top = 8
        Width = 337
        Height = 177
        Caption = 'DB Log'
        TabOrder = 0
        object Label12: TLabel
          Left = 16
          Top = 24
          Width = 167
          Height = 16
          Caption = 'File name ("NUL" = disable)'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label14: TLabel
          Left = 16
          Top = 72
          Width = 119
          Height = 16
          Caption = 'Max log size [Bytes]'
          FocusControl = DBLogMaxSize
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label15: TLabel
          Left = 144
          Top = 72
          Width = 97
          Height = 16
          Caption = 'Rotate log count'
          FocusControl = DBLogRotate
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label22: TLabel
          Left = 250
          Top = 72
          Width = 79
          Height = 16
          Caption = 'Syslog name'
          FocusControl = DBSysLog
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object DBLog: TEdit
          Left = 16
          Top = 40
          Width = 305
          Height = 24
          TabOrder = 0
        end
        object DBLogMaxSize: TEdit
          Left = 16
          Top = 88
          Width = 113
          Height = 24
          TabOrder = 1
        end
        object DBLogRotate: TEdit
          Left = 144
          Top = 88
          Width = 80
          Height = 24
          TabOrder = 2
        end
        object LogErrSQLCmds: TCheckBox
          Left = 16
          Top = 120
          Width = 218
          Height = 21
          AllowGrayed = True
          Caption = 'Log error SQL commands'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          State = cbGrayed
          TabOrder = 4
        end
        object LogErrSQLParams: TCheckBox
          Left = 16
          Top = 144
          Width = 218
          Height = 21
          AllowGrayed = True
          Caption = 'Log error SQL params'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          State = cbGrayed
          TabOrder = 5
        end
        object DBSysLog: TEdit
          Left = 248
          Top = 88
          Width = 73
          Height = 24
          TabOrder = 3
        end
      end
      object LogName: TEdit
        Left = 16
        Top = 344
        Width = 329
        Height = 24
        TabOrder = 2
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Scheduler'
      ImageIndex = 2
      object Label5: TLabel
        Left = 16
        Top = 96
        Width = 43
        Height = 16
        Caption = 'Interval'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label8: TLabel
        Left = 16
        Top = 208
        Width = 97
        Height = 16
        Caption = 'Max.repl.runtime'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label16: TLabel
        Left = 16
        Top = 48
        Width = 247
        Height = 16
        Caption = 'Replicate when time matches cron pattern'
        FocusControl = Cron
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label20: TLabel
        Left = 16
        Top = 256
        Width = 160
        Height = 16
        Caption = 'Do not replicate if file exists'
        FocusControl = FileLock
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object TimerInterval: TEdit
        Left = 16
        Top = 112
        Width = 137
        Height = 24
        TabOrder = 2
      end
      object OnEvent: TCheckBox
        Left = 16
        Top = 150
        Width = 113
        Height = 21
        AllowGrayed = True
        Caption = 'Repl.on event'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        State = cbGrayed
        TabOrder = 3
      end
      object Event: TEdit
        Left = 16
        Top = 168
        Width = 329
        Height = 24
        TabOrder = 4
      end
      object MaxReplRunTime: TEdit
        Left = 16
        Top = 224
        Width = 137
        Height = 24
        TabOrder = 5
      end
      object Disable: TCheckBox
        Left = 16
        Top = 16
        Width = 178
        Height = 21
        Caption = 'Disable timer && event'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object Cron: TEdit
        Left = 16
        Top = 64
        Width = 329
        Height = 24
        TabOrder = 1
      end
      object FileLock: TEdit
        Left = 16
        Top = 272
        Width = 329
        Height = 24
        TabOrder = 6
      end
    end
  end
end
