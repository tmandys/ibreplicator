(* IB Replication Suite
 * Copyright (C) 2002,2003 by Tomas Mandys-MandySoft
 *)

unit dg_SchemaRelationsSyncActions;

interface

uses
  {$IFDEF LINUX}
  Libc, QGraphics, QForms, QControls, QStdCtrls, QButtons, QExtCtrls,
  {$ELSE}
  Windows, Graphics, Forms, Controls, StdCtrls, Buttons, ExtCtrls,
  {$ENDIF}
  SysUtils, Classes, IBReplicator;

type
  TSchemaRelationsSyncActionsDialog = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    DatabaseType: TRadioGroup;
    Panel1: TPanel;
    ListBox1: TListBox;
    Button1: TButton;
    Button2: TButton;
    HelpBtn: TButton;
    Notebook2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Loop: TComboBox;
    Update: TCheckBox;
    Insert: TCheckBox;
    Delete: TCheckBox;
    ComboBox1: TComboBox;
    Notebook1: TPanel;
    Label3: TLabel;
    ComboBox2: TComboBox;
    Label4: TLabel;
    Where1_Src: TComboBox;
    Label5: TLabel;
    Where2_Src: TComboBox;
    Label6: TLabel;
    Where1_Tgt: TComboBox;
    Panel2: TPanel;
    Label7: TLabel;
    Condition: TMemo;
    procedure DatabaseTypeClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure LoopChange(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure HelpBtnClick(Sender: TObject);
    procedure ConditionExit(Sender: TObject);
  private
    fActions: TSyncActions;
    fLock: Byte;
    function GetSyncActions: string;
    procedure SetSyncActions(const Value: string);
    procedure AdjustListBox;
    procedure AdjustCond;
    procedure AdjustControls;
    { Private declarations }
  public
    property SyncActions: string read GetSyncActions write SetSyncActions;
    { Public declarations }
  end;

var
  SchemaRelationsSyncActionsDialog: TSchemaRelationsSyncActionsDialog;
implementation
uses
  dm_IBRepl, Math;

{$IFDEF LINUX}
{$R *.xfm}
{$ELSE}
{$R *.dfm}
{$ENDIF}

{ TTSchemaRelationsSyncActionsDialog }

procedure TSchemaRelationsSyncActionsDialog.AdjustListBox;
var
  I, J: Integer;
  S, S2: string;
resourcestring
  sLoopSource = 'Loop source';
  sLoopTarget = 'Loop target';
  sInsert = 'INSERT';
  sDelete = 'DELETE';
  sUpdate = 'UPDATE';
  sWhere = 'WHERE';
  sDirect = 'Direct';
  sDiffLog = 'Differ.log';
  sDatabaseLog = 'to database log';
  sDBLog = 'to DB log';

begin
  with ListBox1.Items do
  begin
    while Count > Length(fActions.Acts) do
      Delete(Count-1);
    while Count < Length(fActions.Acts) do
      Add('');
    for I:= 0 to Count-1 do
    begin
      J:= fActions.Acts[I].Action;
      S:= '$'+IntToHex(J, 1)+': ';
      if DatabaseType.ItemIndex = dbtInterbase then
        begin
          if J and saSrcTgt = 0 then
            S2:= sLoopSource
          else
            S2:= sLoopTarget;
          S:= S+Copy(S2+StringOfChar(' ', 13), 1, 13);
          S2:= '';
          if (Trim(fActions.Acts[I].Where_Src) <> '') or (Trim(fActions.Acts[I].Where_Tgt) <> '') then
            S2:= S2+sWhere+' ';
          if J and saUpd <> 0 then
            S2:= S2+sUpdate+' ';
          if J and saInsDel <> 0 then
            if J and saSrcTgt = 0 then
              S2:= S2+sInsert
            else
              S2:= S2+sDelete;
          if J and (saUpd or saInsDel) = 0 then
            S2:= '?';
          S:= S+Copy(S2+StringOfChar(' ', 20), 1, 20);
          if J and saRO = 0 then
            S:= S+sDirect
          else
            S:= S+sDiffLog;
        end
      else
        begin
          if J and saRO = 0 then
            S2:= sDatabaseLog
          else
            S2:= sDBLog;
          S:= S+Copy(S2+StringOfChar(' ', 20), 1, 20);

          S2:= '';
          if Trim(fActions.Acts[I].Where_Src) <> '' then
            S2:= S2+sWhere+' ';
          S:= S+Copy(S2+StringOfChar(' ', 7), 1, 7);
        end;
      ListBox1.Items[I]:= S;
    end;
  end;
//  Button1.Enabled:= ListBox1.Items.Count < 4;
  Button2.Enabled:= ListBox1.ItemIndex >= 0;
end;

function TSchemaRelationsSyncActionsDialog.GetSyncActions: string;
begin
  Result:= TIBReplicator.EncodeSyncActions(fActions);
end;

procedure TSchemaRelationsSyncActionsDialog.SetSyncActions(
  const Value: string);
  var
  Act: TSyncActions;
begin
  fActions:= TIBReplicator.DecodeSyncActions(Value, False);

  SetLength(Act.Acts, 0);
  Act.Cond:= fActions.Cond;
  Condition.Lines.Text:= TIBReplicator.EncodeSyncActions(Act);

  AdjustListBox;
  AdjustCond;
end;

procedure TSchemaRelationsSyncActionsDialog.DatabaseTypeClick(
  Sender: TObject);
begin
  AdjustListBox;
  Notebook1.Visible:= DatabaseType.ItemIndex = 1;  // TNotebook not supported in Kylix
  Notebook2.Visible:= DatabaseType.ItemIndex = 0;
  AdjustControls;
end;

procedure TSchemaRelationsSyncActionsDialog.FormShow(Sender: TObject);
begin
{$IFDEF LINUX}
  DatabaseType.ItemIndex:= 0;
{$ENDIF}
  DatabaseTypeClick(nil);
  ListBox1Click(nil);
end;

procedure TSchemaRelationsSyncActionsDialog.AdjustControls;
var
  J: Integer;
begin
  Inc(fLock);
  try
    Notebook1.Enabled:= ListBox1.ItemIndex >= 0;
    Notebook2.Enabled:= ListBox1.ItemIndex >= 0;
    if ListBox1.ItemIndex >= 0 then
    begin
      J:= fActions.Acts[ListBox1.ItemIndex].Action;
      Loop.ItemIndex:= Byte(J and saSrcTgt <> 0);
      ComboBox1.ItemIndex:= Byte(J and saRo <> 0);
      ComboBox2.ItemIndex:= ComboBox1.ItemIndex;
      Update.Checked:= J and saUpd <> 0;
      Insert.Checked:= J and saInsDel <> 0;
      Delete.Checked:= J and saInsDel <> 0;
      Where1_Src.Text:= fActions.Acts[ListBox1.ItemIndex].Where_Src;
      Where1_Tgt.Text:= fActions.Acts[ListBox1.ItemIndex].Where_Tgt;
      Where2_Src.Text:= Where1_Src.Text;
      LoopChange(nil);
    end;
  finally
    Dec(fLock);
  end;
end;

procedure TSchemaRelationsSyncActionsDialog.LoopChange(Sender: TObject);
var
  J: Integer;
begin
  if fLock = 0 then
  begin
    J:= 0;
    if DatabaseType.ItemIndex = dbtInterbase then
      begin
        if Loop.ItemIndex > 0 then
          J:= J or saSrcTgt;
        if Update.Checked then
          J:= J or saUpd;
        if Insert.Enabled and Insert.Checked then
          J:= J or saInsDel;
        if Delete.Enabled and Delete.Checked then
          J:= J or saInsDel;
        if ComboBox1.ItemIndex > 0 then
          J:= J or saRO;
        fActions.Acts[ListBox1.ItemIndex].Where_Src:= Where1_Src.Text;
        fActions.Acts[ListBox1.ItemIndex].Where_Tgt:= Where1_Tgt.Text;
      end
    else
      begin
        J:= J or saUpd;
        if ComboBox2.ItemIndex > 0 then
          J:= J or saRO;
        fActions.Acts[ListBox1.ItemIndex].Where_Src:= Where2_Src.Text;
      end;
    fActions.Acts[ListBox1.ItemIndex].Action:= J;
  end;
  AdjustListBox;
  Insert.Enabled:= Loop.ItemIndex = 0;
  Delete.Enabled:= not Insert.Enabled;
end;

procedure TSchemaRelationsSyncActionsDialog.ListBox1Click(Sender: TObject);
begin
//  Button2.Enabled:= ListBox1.ItemIndex >= 0;
  AdjustControls;
end;

procedure TSchemaRelationsSyncActionsDialog.Button1Click(Sender: TObject);
var
  I, J: Integer;
begin
  SetLength(fActions.Acts, Length(fActions.Acts)+1);
  J:= ListBox1.ItemIndex;
  if J >= 0 then
  begin
    for I:= Length(fActions.Acts)-1 downto J+1 do
      fActions.Acts[I]:= fActions.Acts[I-1];
    Inc(J);
  end;
  AdjustListBox;
  if J > 0 then
    ListBox1.ItemIndex:= J;
  AdjustControls;
end;

procedure TSchemaRelationsSyncActionsDialog.Button2Click(Sender: TObject);
var
  I, J: Integer;
begin
  J:= ListBox1.ItemIndex;
  for I:= J to Length(fActions.Acts)-2 do
    fActions.Acts[I]:= fActions.Acts[I+1];
  SetLength(fActions.Acts, Length(fActions.Acts)-1);
  AdjustListBox;
  if ListBox1.Items.Count > 0 then
    ListBox1.ItemIndex:= Min(J, ListBox1.Items.Count-1);
  AdjustControls;
end;

procedure TSchemaRelationsSyncActionsDialog.HelpBtnClick(Sender: TObject);
begin
  Application.{$IFDEF LINUX}ContextHelp{$ELSE}HelpContext{$ENDIF}(HelpContext);
end;

procedure TSchemaRelationsSyncActionsDialog.AdjustCond;
var
  I: Integer;
begin
  Where1_Src.Items.Clear;
  for I:= 0 to Length(fActions.Cond)-1 do
  begin
    Where1_Src.Items.Add(fActions.Cond[I].Alias);
  end;
  Where2_Src.Items.Text:= Where1_Src.Items.Text;
  Where1_Tgt.Items.Text:= Where1_Src.Items.Text;
end;

procedure TSchemaRelationsSyncActionsDialog.ConditionExit(Sender: TObject);
var
  Act: TSyncActions;
begin
  Act:= TIBReplicator.DecodeSyncActions(Condition.Lines.Text, False);
  fActions.Cond:= Act.Cond;
  AdjustCond;
end;

end.
