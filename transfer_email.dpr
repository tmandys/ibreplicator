(* transfer_email - Transfer library for IB Replicator
 * Copyright (C) 2003  Tomas Mandys-MandySoft
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA  02111-1307  USA
 *)

library transfer_email;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

// Indy version
{...$DEFINE INDY100} // This is defined in {$I IdCompilerDefines.inc} but it requires search path

uses
  {$IFDEF LINUX}
  Libc,
  {$ELSE}
  Windows,
  FileCtrl,
  {$ENDIF}
  SysUtils,
  Classes,
  IdSMTP,
  IdPOP3,
  IdMessage,
  IdEmailAddress,
  {$IFDEF INDY100}
  IdAttachment,
  IdAttachmentFile,
  {$ENDIF}
  Transfer in 'Transfer.pas';

{$R *.res}

{$IFDEF LINUX}
{$ENDIF}

const
  ToolName = 'email';

{$DEFINE IMP_TYPES}
{$I Transfer.inc}

{$DEFINE IMP_CLASS}
{$I Transfer.inc}

{$I ToolUtils.inc}

const
  PackageExtension = '.rpg';

type
  TEmailTransferData = class(TTransferData)
    Params: TStringList;
    ReceivedNames: TStringList;
    TargetSide: Boolean;
    Pop3: TIdPop3;
    constructor Create;
    destructor Destroy; override;
  end;

constructor TEmailTransferData.Create;
begin
  inherited;
  Params:= TStringList.Create;
end;

destructor TEmailTransferData.Destroy;
begin
  Params.Free;
  ReceivedNames.Free;
  if Pop3 <> nil then
    Pop3.Disconnect;
  Pop3.Free;
  inherited;
end;

resourcestring
  sPackageNotFoundI = 'Package #%d not found';
  sPackageNotFoundS = 'Package "%s" not found';
  sCannotRetrievePackageS = 'Cannot retrieve package "%s"';
  sAttachmentNotFoundS = 'Attachment "%s" not found';

function TransferOpen(aTargetSide: Boolean; aParams: PChar; var H: TEmailTransferData): Integer; stdcall;
begin
  H:= TEmailTransferData.Create;
  try
    Result:= 0;
    H.Params.Text:= StrPas(aParams);
    H.TargetSide:= aTargetSide;
  except
    on E: Exception do
    begin
      Result:= errErrorS;
      H.BlockedError:= Result;
      H.LastErrorS:= E.Message;
    end;
  end;
end;

function TransferClose(H: TEmailTransferData): Integer; stdcall;
begin
  Result:= H.CheckHandle;
  if Result <> 0 then
    Exit;
  H.ClearError;
  H.Free;
end;

function TransferSendPackage(H: TEmailTransferData; Name: PChar; Src: TStream): Integer; stdcall;
var
  SMTP: TIdSMTP;
  Msg: TIdMessage;
  EI: TIdEMailAddressItem;

  Att: {$IFDEF INDY100}TIdAttachmentFile{$ELSE}TIdAttachment{$ENDIF};
  TmpFileName: string;
begin
  Result:= H.CheckHandle;
  if Result <> 0 then
    Exit;
  H.ClearError;
  try
    SMTP:= TIdSMTP.Create(nil);
    try
      SMTP.Host:= GetParString(H.Params, 'SMTP.Host', SMTP.Host, Byte(H.TargetSide));
      SMTP.Port:= GetParInteger(H.Params, 'SMTP.Port', SMTP.Port, Byte(H.TargetSide));

      {$IFDEF INDY100}
      SMTP.AuthType:= TIdSMTPAuthenticationType(GetParInteger(H.Params, 'SMTP.AuthType', Byte(SMTP.AuthType), Byte(H.TargetSide)));
      {$ELSE}
      SMTP.{$IFDEF INDY100}AuthType{$ELSE}AuthenticationType{$ENDIF}:= TAuthenticationType(GetParInteger(H.Params, 'SMTP.AuthType', Byte(SMTP.AuthenticationType), Byte(H.TargetSide)));
      {$ENDIF}
      SMTP.UserName:= GetParString(H.Params, 'SMTP.UserName', SMTP.UserName, Byte(H.TargetSide));
      SMTP.Password:= GetParString(H.Params, 'SMTP.Password', SMTP.Password, Byte(H.TargetSide));
      SMTP.ReadTimeout:= GetParInteger(H.Params, 'SMTP.ReadTimeout', 20000{ms}{SMTP.ReadTimeout}, Byte(H.TargetSide));
      SMTP.MailAgent:= 'IBRepl mail agent';
      SMTP.Connect;
      try
        Msg:= TIdMessage.Create(nil);
        try
          Msg.Subject:= NamePrefix+StrPas(Name)+PackageExtension;
          Msg.Sender.Address:= GetParString(H.Params, 'Address', '', Byte(H.TargetSide));
          Msg.UseNowForDate:= True;
          EI:= Msg.Recipients.Add;
          EI.Address:= GetParString(H.Params, 'Address', '', Byte(not H.TargetSide));
          TmpFileName:= SaveToTempFile('', Src, H.Params);
          Att:= {$IFNDEF INDY100}TIdAttachment{$ELSE}TIdAttachmentFile{$ENDIF}.Create(Msg.MessageParts, TmpFileName);
          {$IFDEF LINUX}
          Att.DeleteTempFile:= True;
          {$ELSE}
            {$IFDEF INDY100}
          Att.FileIsTempFile:= True;
            {$ELSE}
          Att.DeleteTempFile:= True;   // in Indy Rev 1.7
            {$ENDIF}
          {$ENDIF}
          Att.FileName:= Msg.Subject;
          SMTP.Send(Msg);
        finally
          Msg.Free;
        end;
      finally
        SMTP.Disconnect;
      end;
    finally
      SMTP.Free;
    end;
  except
    on E: Exception do
    begin
      H.LastErrorS:= E.Message;
      Result:= errErrorS;
    end;
  end;
end;

function TransferReceivePackageNames(H: TEmailTransferData; NameMask: PChar; var N: Integer): Integer; stdcall;
var
  S: string;
  I, J, K: Integer;
  Msg: TIdMessage;
begin
  Result:= H.CheckHandle;
  if Result <> 0 then
    Exit;
  H.ClearError;
  try
    if H.ReceivedNames = nil then
      H.ReceivedNames:= TStringList.Create;
    H.ReceivedNames.Clear;
    H.Pop3.Free;
    H.Pop3:= TIdPop3.Create(nil);
    H.Pop3.Host:= GetParString(H.Params, 'POP3.Host', H.Pop3.Host, Byte(H.TargetSide));
    H.Pop3.Port:= GetParInteger(H.Params, 'POP3.Port', H.Pop3.Port, Byte(H.TargetSide));
    {$IFDEF INDY100}
    H.Pop3.AuthType:= TIdPop3AuthenticationType(GetParInteger(H.Params, 'POP3.APOP', Integer(H.Pop3.AuthType), Byte(H.TargetSide)));
    {$ELSE}
    H.Pop3.APOP:= GetParBool(H.Params, 'POP3.APOP', H.Pop3.APOP, Byte(H.TargetSide));
    {$ENDIF}
    H.Pop3.UserName:= GetParString(H.Params, 'POP3.UserName', H.Pop3.UserName, Byte(H.TargetSide));
    H.Pop3.Password:= GetParString(H.Params, 'POP3.Password', H.Pop3.Password, Byte(H.TargetSide));
    H.Pop3.ReadTimeout:= GetParInteger(H.Params, 'POP3.ReadTimeout', 20000{ms}{H.Pop3.ReadTimeout}, Byte(H.TargetSide));
    H.Pop3.Connect;
    H.Pop3.KeepAlive;
    J:= H.Pop3.CheckMessages;
    for I:= 1 to J do
    begin
      Msg:= TIdMessage.Create(nil);
      try
        if H.Pop3.RetrieveHeader(I, Msg) then
          begin
            S:= Msg.Subject;
            K:= Pos(NamePrefix, S);
            if K > 0 then
              Delete(S, 1, K-1+Length(NamePrefix))
            else
              S:= '';  // bad subject
            Delete(S, Pos('.', S+'.'), Length(S));
            H.ReceivedNames.Add(S);
          end
        else
          H.ReceivedNames.Add('');
      finally
        Msg.Free;
      end;
    end;
    N:= H.ReceivedNames.Count;
    K:= 0;
    for I:= 0 to N-1 do
    begin
      S:= H.ReceivedNames[I];
      if Pos(NameMask, S) = 1 then
        begin
          J:= H.ReceivedNames.IndexOf(S);  // first occurance
          if J = I then
            begin
              H.ReceivedNames.Objects[I]:= Pointer(K);
              Inc(K);
            end
          else           // duplicate message
            H.ReceivedNames.Objects[I]:= H.ReceivedNames.Objects[J];
        end
      else
        H.ReceivedNames.Objects[I]:= Pointer(-1);
    end;
    N:= K;
  except
    on E: Exception do
    begin
      H.LastErrorS:= E.Message;
      Result:= errErrorS;
    end;
  end;
end;

function TransferGetReceivedPackageName(H: TEmailTransferData; I: Integer; Name: PChar; BufferLength: Integer): Integer; stdcall;
var
  J: Integer;
  F: Boolean;
begin
  Result:= H.CheckHandle;
  if Result <> 0 then
    Exit;
  H.ClearError;
  try
    if H.ReceivedNames = nil then
      H.ProjError(sNamesNotReceived);
    F:= False;
    for J:= 0 to H.ReceivedNames.Count-1 do
      if H.ReceivedNames.Objects[J] = Pointer(I) then
      begin
        StrPLCopy(Name, H.ReceivedNames[J], BufferLength);
        F:= True;
        Break;
      end;
    if not F then
      ProjError(Format(sPackageNotFoundI, [I]));
  except
    on E: Exception do
    begin
      H.LastErrorS:= E.Message;
      Result:= errErrorS;
    end;
  end;
end;

function TransferReceivePackage(H: TEmailTransferData; Name: PChar; Tgt: TStream): Integer; stdcall;
var
  S: string;
  I, J: Integer;
  F: Boolean;
  Msg: TIdMessage;
begin
  Result:= H.CheckHandle;
  if Result <> 0 then
    Exit;
  H.ClearError;
  try
    F:= False;
    for I:= 0 to H.ReceivedNames.Count-1 do
      if H.ReceivedNames[I] = StrPas(Name) then
      begin
        Msg:= TIdMessage.Create(nil);
        try
          if H.Pop3.Retrieve(I+1, Msg) then
            begin
              for J:= 0 to Msg.MessageParts.Count-1 do
              begin
                if Msg.MessageParts.Items[J] is TIdAttachment then
                begin
                  S:= TIdAttachment(Msg.MessageParts.Items[J]).Filename;
                  if Pos(StrPas(Name), S) > 0 then
                  begin
                    S:= GetTempDir(H.Params)+S;
                    TIdAttachment(Msg.MessageParts.Items[J]).SaveToFile(S);
                    LoadFromTempFile(S, Tgt);
                    DeleteFile(S);
                    F:= True;
                    Break;
                  end;
                end;
              end;
              if not F then
                ProjError(Format(sAttachmentNotFoundS, [StrPas(Name)]));
            end
          else
            ProjError(Format(sCannotRetrievePackageS, [StrPas(Name)]));
        finally
          Msg.Free;
        end;

        Break;
      end;
    if not F then
      ProjError(Format(sPackageNotFoundS, [StrPas(Name)]));

  except
    on E: Exception do
    begin
      H.LastErrorS:= E.Message;
      Result:= errErrorS;
    end;
  end;
end;

function TransferDeletePackage(H: TEmailTransferData; Name: PChar): Integer; stdcall;
var
  I: Integer;
begin
  Result:= H.CheckHandle;
  if Result <> 0 then
    Exit;
  H.ClearError;
  try
    for I:= 0 to H.ReceivedNames.Count-1 do
      if H.ReceivedNames[I] = StrPas(Name) then
        H.Pop3.Delete(I+1);
  except
    on E: Exception do
    begin
      H.LastErrorS:= E.Message;
      Result:= errErrorS;
    end;
  end;
end;

function TransferError(H: TEmailTransferData; ErrS: PChar; BufferLength: Integer): Integer; stdcall;
begin
  Result:= H.TransferError(ErrS, BufferLength);
end;


{$DEFINE EXP_PROCS}
{$I Transfer.inc}

begin
  TransferLibInit;
end.

