(* IB Replication Suite
 * Copyright (C) 2002,2003 by Tomas Mandys-MandySoft
 *)

unit dg_ServerTaskSettings;

interface

uses
  {$IFDEF LINUX}
  Libc, QGraphics, QControls, QForms, QDialogs, QStdCtrls, QComCtrls,
  {$ELSE}
  Windows, Messages, Graphics, Controls, Forms, Dialogs, StdCtrls, ComCtrls,
  {$ENDIF}
  SysUtils, Classes, Connect, IBReplicator;

type
  TServerTaskSettingsDialog = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    HelpBtn: TButton;
    PageControl2: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    Label6: TLabel;
    Label4: TLabel;
    Label9: TLabel;
    Label11: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    RType: TComboBox;
    SchemaId: TEdit;
    GroupId: TEdit;
    TgtDBId: TEdit;
    NowAsUTC: TCheckBox;
    ExtConflict: TCheckBox;
    ReplOpt: TComboBox;
    RepToSource: TCheckBox;
    RepToTarget: TCheckBox;
    SrcDBId: TEdit;
    Label5: TLabel;
    TimerInterval: TEdit;
    OnEvent: TCheckBox;
    Event: TEdit;
    Label8: TLabel;
    MaxReplRunTime: TEdit;
    TabSheet4: TTabSheet;
    Label1: TLabel;
    TabSheet5: TTabSheet;
    Label13: TLabel;
    OfflineDir: TEdit;
    ReceiveData: TCheckBox;
    SendData: TCheckBox;
    ResendData: TCheckBox;
    Disable: TCheckBox;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    ReplLog: TEdit;
    Label3: TLabel;
    ReplLogMaxSize: TEdit;
    Label10: TLabel;
    ReplLogRotate: TEdit;
    GroupBox2: TGroupBox;
    Label12: TLabel;
    DBLog: TEdit;
    Label14: TLabel;
    DBLogMaxSize: TEdit;
    Label15: TLabel;
    DBLogRotate: TEdit;
    LogErrSQLCmds: TCheckBox;
    LogErrSQLParams: TCheckBox;
    TaskNameEdit: TEdit;
    Label7: TLabel;
    Label19: TLabel;
    LogName: TEdit;
    Label16: TLabel;
    Cron: TEdit;
    Label20: TLabel;
    FileLock: TEdit;
    SchemaIdButton: TButton;
    GroupIdButton: TButton;
    SrcDBIdButton: TButton;
    TgtDBIdButton: TButton;
    Label21: TLabel;
    Comment: TMemo;
    Label22: TLabel;
    DBSysLog: TEdit;
    procedure RTypeChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure HelpBtnClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SchemaIdButtonClick(Sender: TObject);
    procedure GroupIdButtonClick(Sender: TObject);
    procedure SrcDBIdButtonClick(Sender: TObject);
    procedure TgtDBIdButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SchemaIdKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    fLoaded: Boolean;
    function SectionName: string;
    procedure ReadFromIni;
    procedure WriteToIni;
    procedure LookupDlg(aEdit: TEdit; aMultiSelect: Boolean; const aSelectSQL: string; const aCaptions: array of string);
  public
    TaskName: string;
  end;

var
  ServerTaskSettingsDialog: TServerTaskSettingsDialog;

implementation
uses
  AuxProj, Math, dm_IBRepl, dg_Lookup;

{$IFDEF LINUX}
{$R *.xfm}
{$ELSE}
{$R *.dfm}
{$ENDIF}

procedure TServerTaskSettingsDialog.RTypeChange(Sender: TObject);
begin
  with RType as TComboBox do
  begin
    SrcDBId.Enabled:= not (ItemIndex in [3,4]);
    OnEvent.Enabled:= ItemIndex in [0,1,3];
    Event.Enabled:= ItemIndex in [0,1];
    OfflineDir.Enabled:= ItemIndex in [1,2];
    ReceiveData.Enabled:= ItemIndex in [1,2];
    SendData.Enabled:= ItemIndex in [1,2];
    ResendData.Enabled:= ItemIndex in [1,2];
    ReplOpt.Enabled:= ItemIndex in [0,2];
    ExtConflict.Enabled:= (ItemIndex in [0,2,3]) and ((ReplOpt.ItemIndex = 0) or not ReplOpt.Enabled);
    RepToSource.Enabled:= (ItemIndex in [0,1,2,3]) and ((ReplOpt.ItemIndex = 0) or not ReplOpt.Enabled);
    RepToTarget.Enabled:= (ItemIndex in [0,2]) and ((ReplOpt.ItemIndex = 0) or not ReplOpt.Enabled);

    SchemaIdButton.Enabled:= SchemaId.Enabled;
    GroupIdButton.Enabled:= GroupId.Enabled;
    SrcDBIdButton.Enabled:= SrcDBId.Enabled;
    TgtDBIdButton.Enabled:= TgtDBId.Enabled;
  end;
end;

procedure TServerTaskSettingsDialog.FormShow(Sender: TObject);
begin
//  if TaskName <> '' then
  ReadFromIni;
  if TaskNameEdit.Text = '' then
    PageControl2.ActivePageIndex:= 0
  else
    PageControl2.ActivePageIndex:= 1;
  RTypeChange(nil);
  fLoaded:= True;
end;

procedure TServerTaskSettingsDialog.HelpBtnClick(Sender: TObject);
begin
  Application.{$IFDEF LINUX}ContextHelp{$ELSE}HelpContext{$ENDIF}(HelpContext);
end;

procedure TServerTaskSettingsDialog.FormClose(Sender: TObject;
  var Action: TCloseAction);
var
  Sg: TStrings;
resourcestring
  sTaskNameEmpty = 'Task name is empty';
  sTaskNameExists = 'Task name already exists';
begin
  if ModalResult = mrOK then
  begin
    if Trim(TaskNameEdit.Text) = '' then
      raise Exception.Create(sTaskNameEmpty);
    Sg:= TStringList.Create;
    try
      Ini.ReadSections(Sg);
      if not SameText(Trim(TaskNameEdit.Text), TaskName) then
        if Sg.IndexOf(TIBReplicatorTask.GetSectionName(TaskNameEdit.Text)) >=0 then
          raise Exception.Create(sTaskNameExists);
      WriteToIni;
    finally
      Sg.Free;
    end;
  end;
end;

function TServerTaskSettingsDialog.SectionName: string;
begin
  Result:= TIBReplicatorTask.GetSectionName(TaskName);
end;

type
  TEntity = record
    Str: string;
    Ent: string;
  end;

const
  Entities: array[1..5] of TEntity =
    ((Str: '\'; Ent: '\\'),
     (Str: #13; Ent: '\r'),
     (Str: #10; Ent: '\n'),
     (Str: #12; Ent: '\f'),
     (Str: #9; Ent: '\t')
     );

function DecStr(const S: string): string;
var
  I: Integer;
begin
  Result:= StringReplace(S, Entities[Low(Entities)].Ent, #1, [rfReplaceAll]);
  for I:= Low(Entities)+1 to High(Entities) do
  begin
    Result:= StringReplace(Result, Entities[I].Ent, Entities[I].Str, [rfReplaceAll]);
  end;
  Result:= StringReplace(Result, #1, Entities[Low(Entities)].Str, [rfReplaceAll]);
end;

function EncStr(const S: string): string;
var
  I: Integer;
begin
  Result:= S;
  for I:= Low(Entities) to High(Entities) do
  begin
    Result:= StringReplace(Result, Entities[I].Str, Entities[I].Ent, [rfReplaceAll]);
  end;
  for I:= 1 to Length(Result) do
    if Result[I] < ' ' then
      Result[I]:= ' ';
end;

procedure TServerTaskSettingsDialog.ReadFromIni;
  procedure IniReadString(const aName: string; aEdit: TEdit);
  begin
    aEdit.Text:= Trim(Ini.ReadString(SectionName, aName, aEdit.Text));
  end;
  procedure IniReadBool(const aName: string; CB: TCheckBox);
  begin
    if Ini.ValueExists(SectionName, aName) then
      CB.Checked:= Ini.ReadBool(SectionName, aName, False)
    else
      if CB.AllowGrayed then
        CB.State:= cbGrayed
      else
        CB.Checked:= False;
  end;

var
  W: Word;
begin
  TaskNameEdit.Text:= TaskName;
  Comment.Text:= DecStr(Ini.ReadString(SectionName, 'Comment', ''));
  RType.ItemIndex:= Ini.ReadInteger(SectionName, 'ReplType', Max(0, RType.ItemIndex));
  IniReadString('SchemaId', SchemaId);
  IniReadString('GroupId', GroupId);
  IniReadString('SrcDBId', SrcDBId);
  IniReadString('TgtDBId', TgtDBId);

  IniReadBool('Disabled', Disable);

  IniReadString('Scheduler.Cron', Cron);
  IniReadString('Scheduler.FileLock', FileLock);
  IniReadString('Scheduler.Interval', TimerInterval);
  IniReadBool('Scheduler.ReplOnEvent', OnEvent);
  IniReadString('Scheduler.Event', Event);
  IniReadString('Scheduler.MaxRunTime', MaxReplRuntime);

  IniReadBool('NowAsUTC', NowAsUTC);
  IniReadString('OfflineDir', OfflineDir);

  IniReadBool('SendData', SendData);
  IniReadBool('ResendData', ResendData);
  IniReadBool('ReceiveData', ReceiveData);
  W:= Ini.ReadInteger(SectionName, 'ReplOptions', 0);
  ReplOpt.ItemIndex:= W and $0001;
  ExtConflict.Checked:= W and repoptExtConflictCheck <> 0;
  RepToSource.Checked:= W and repoptReportToSource <> 0;
  RepToTarget.Checked:= W and repoptReportToTarget <> 0;

  IniReadString('ReplLog.FileName', ReplLog);
  IniReadString('ReplLog.MaxSize', ReplLogMaxSize);
  IniReadString('ReplLog.RotateCount', ReplLogRotate);
  IniReadString('DBLog.FileName', DBLog);
  IniReadString('DBLog.MaxSize', DBLogMaxSize);
  IniReadString('DBLog.RotateCount', DBLogRotate);
  IniReadString('DBLog.SysLogName', DBSysLog);
  IniReadBool('DBLog.ErrSQLCmds', LogErrSQLCmds);
  IniReadBool('DBLog.ErrSQLParams', LogErrSQLParams);
  IniReadString('Log.Name', LogName);
end;

procedure TServerTaskSettingsDialog.WriteToIni;
  procedure IniWriteString(const aName: string; aEdit: TEdit);
  begin
    if Trim(aEdit.Text) <> '' then
      Ini.WriteString(SectionName, aName, Trim(aEdit.Text));
  end;

  procedure IniWriteBool(const aName: string; CB: TCheckBox);
  begin
    if CB.State <> cbGrayed then
      Ini.WriteBool(SectionName, aName, CB.Checked);
  end;
var
  W: Word;
begin
  if TaskName <> '' then
    Ini.EraseSection(SectionName);
  TaskName:= TaskNameEdit.Text;
  Ini.EraseSection(SectionName);
  Ini.WriteString(SectionName, 'Comment', EncStr(Comment.Text));
  Ini.WriteInteger(SectionName, 'ReplType', RType.ItemIndex);
  IniWriteString('SchemaId', SchemaId);
  IniWriteString('GroupId', GroupId);
  IniWriteString('SrcDBId', SrcDBId);
  IniWriteString('TgtDBId', TgtDBId);

  IniWriteBool('Disabled', Disable);

  IniWriteString('Scheduler.Cron', Cron);
  IniWriteString('Scheduler.FileLock', FileLock);
  IniWriteString('Scheduler.Interval', TimerInterval);
  IniWriteBool('Scheduler.ReplOnEvent', OnEvent);
  IniWriteString('Scheduler.Event', Event);
  IniWriteString('Scheduler.MaxRunTime', MaxReplRunTime);

  IniWriteString('OfflineDir', OfflineDir);
  IniWriteBool('SendData', SendData);
  IniWriteBool('ResendData', ResendData);
  IniWriteBool('ReceiveData', ReceiveData);
  IniWriteBool('NowAsUTC', NowAsUTC);
  W:= ReplOpt.ItemIndex;
  if ExtConflict.Checked then
    W:= W or repoptExtConflictCheck;
  if RepToSource.Checked then
    W:= W or repoptReportToSource;
  if RepToTarget.Checked then
    W:= W or repoptReportToTarget;
  Ini.WriteInteger(SectionName, 'ReplOptions', W);

  IniWriteString('ReplLog.FileName', ReplLog);
  IniWriteString('ReplLog.MaxSize', ReplLogMaxSize);
  IniWriteString('ReplLog.RotateCount', ReplLogRotate);
  IniWriteString('DBLog.FileName', DBLog);
  IniWriteString('DBLog.MaxSize', DBLogMaxSize);
  IniWriteString('DBLog.RotateCount', DBLogRotate);
  IniWriteString('DBLog.SysLogName', DBSysLog);
  IniWriteBool('DBLog.ErrSQLCmds', LogErrSQLCmds);
  IniWriteBool('DBLog.ErrSQLParams', LogErrSQLParams);
  IniWriteString('Log.Name', LogName);
  Ini.UpdateFile;
end;

procedure TServerTaskSettingsDialog.LookupDlg;
begin
  with TLookupDlg.Create(nil) do
  try
    AssignSQL(aMultiSelect, aSelectSQL, aCaptions);
    Selected:= aEdit.Text;
    if ShowModal = mrOK then
      aEdit.Text:= Selected;
  finally
    Release;
  end;
end;

resourcestring
  sSchemaId = 'SchemaId';
  sGroupId = 'GroupId';
  sDBId = 'DBId';
  sDBName = 'DB';
  sSchemaName = 'Schema';
procedure TServerTaskSettingsDialog.SchemaIdButtonClick(Sender: TObject);

begin
  LookupDlg(SchemaId, RType.ItemIndex <> 4, Format('SELECT S.SCHEMAID AS ID,S.NAME AS NAME_0 FROM /*PREFIX*/SCHEMATA S ORDER BY S.SCHEMAID', []), [sSchemaId, sSchemaName]);
end;

procedure TServerTaskSettingsDialog.GroupIdButtonClick(Sender: TObject);
resourcestring
  sDBCount = 'DB count';
begin
  LookupDlg(GroupId, True, Format('SELECT SD.GROUPID AS ID,S.SCHEMAID AS NAME_0,S.NAME AS NAME_1, COUNT(*) AS NAME_2 FROM /*PREFIX*/SCHEMADB SD INNER JOIN /*PREFIX*/SCHEMATA S INNER JOIN /*PREFIX*/DATABASES D ON SD.DBID=D.DBID ON SD.SCHEMAID=S.SCHEMAID %s '+
                                   'WHERE SD.GROUPID<>0 GROUP BY SD.GROUPID,S.SCHEMAID,S.NAME ORDER BY S.SCHEMAID',
                                   [IntegerArrayToSQLCondition(StringArrayToIntegerArray(StringToArray(SchemaId.Text, ',')), 'S.SCHEMAID', ' AND ')]),
                                   [sGroupId, sSchemaId, sSchemaName, sDBCount]);
end;

procedure TServerTaskSettingsDialog.SrcDBIdButtonClick(Sender: TObject);
begin
  LookupDlg(SrcDBId, False, Format('SELECT D.DBID AS ID,D.NAME AS NAME_0,S.SCHEMAID AS NAME_1,S.NAME AS NAME_2 FROM /*PREFIX*/SCHEMADB SD INNER JOIN /*PREFIX*/SCHEMATA S INNER JOIN /*PREFIX*/DATABASES D ON SD.DBID=D.DBID ON SD.SCHEMAID=S.SCHEMAID '+
                                   'WHERE SD.GROUPID=0 %s ORDER BY S.SCHEMAID,D.DBID',
                                   [IntegerArrayToSQLCondition(StringArrayToIntegerArray(StringToArray(SchemaId.Text, ',')), 'S.SCHEMAID', ' AND ')]),
                                   [sDBId, sDBName, sSchemaId, sSchemaName]);
end;

procedure TServerTaskSettingsDialog.TgtDBIdButtonClick(Sender: TObject);
begin
  LookupDlg(TgtDBId, True, Format('SELECT D.DBID AS ID,D.NAME AS NAME_0,S.SCHEMAID AS NAME_1,S.NAME AS NAME_2,SD.GROUPID AS NAME_3 FROM /*PREFIX*/SCHEMADB SD INNER JOIN /*PREFIX*/SCHEMATA S INNER JOIN /*PREFIX*/DATABASES D ON SD.DBID=D.DBID ON SD.SCHEMAID=S.SCHEMAID '+
                                   'WHERE SD.GROUPID<>0 %s %s ORDER BY S.SCHEMAID,D.DBID',
                                   [IntegerArrayToSQLCondition(StringArrayToIntegerArray(StringToArray(SchemaId.Text, ',')), 'S.SCHEMAID', ' AND '),
                                    IntegerArrayToSQLCondition(StringArrayToIntegerArray(StringToArray(GroupId.Text, ',')), 'SD.GROUPID', ' AND ')]),
                                   [sDBId, sDBName, sSchemaId, sSchemaName, sGroupId]);
end;

procedure TServerTaskSettingsDialog.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  for I:= 0 to PageControl2.PageCount-1 do
    PageControl2.Pages[I].HelpContext:= HelpContext+1+I;
end;

procedure TServerTaskSettingsDialog.SchemaIdKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Shift = [ssAlt]) and ((Key = VK_DOWN) or (Key = VK_UP)) then
    TButton(FindComponent(TComponent(Sender).Name+'Button')).Click;
end;

end.
