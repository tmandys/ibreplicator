inherited SchemaSourceDialog: TSchemaSourceDialog
  HelpContext = 31220
  Caption = 'Source database properties'
  ClientHeight = 465
  ClientWidth = 491
  OnCreate = FormCreate
  PixelsPerInch = 120
  TextHeight = 16
  inherited Panel1: TPanel
    Left = 383
    Height = 465
  end
  inherited Panel2: TPanel
    Width = 383
    Height = 465
    object Label1: TLabel
      Left = 20
      Top = 20
      Width = 63
      Height = 16
      Caption = 'Schema&ID'
      FocusControl = DBEdit1
    end
    object Label5: TLabel
      Left = 20
      Top = 138
      Width = 58
      Height = 16
      Caption = 'Repl &user'
      FocusControl = DBEdit4
    end
    object Label6: TLabel
      Left = 20
      Top = 197
      Width = 91
      Height = 16
      Caption = 'Repl &password'
      FocusControl = DBEdit5
    end
    object Label7: TLabel
      Left = 20
      Top = 256
      Width = 55
      Height = 16
      Caption = 'Repl &role'
      FocusControl = DBEdit6
    end
    object Label9: TLabel
      Left = 230
      Top = 138
      Width = 60
      Height = 16
      Caption = '&Separator'
      FocusControl = DBEdit7
    end
    object Label10: TLabel
      Left = 20
      Top = 315
      Width = 57
      Height = 16
      Caption = 'Co&mment'
      FocusControl = DBMemo2
    end
    object Label2: TLabel
      Left = 20
      Top = 79
      Width = 104
      Height = 16
      Caption = 'S&ource database'
      FocusControl = DBLookupComboBox1
    end
    object DBEdit1: TDBEdit
      Left = 20
      Top = 39
      Width = 70
      Height = 24
      TabStop = False
      DataField = 'SCHEMAID'
      DataSource = DataSource1
      ParentColor = True
      ReadOnly = True
      TabOrder = 0
    end
    object DBMemo2: TDBMemo
      Left = 20
      Top = 335
      Width = 357
      Height = 109
      DataField = 'COMMENT'
      DataSource = DataSource1
      ScrollBars = ssVertical
      TabOrder = 8
    end
    object DBEdit4: TDBEdit
      Left = 20
      Top = 158
      Width = 157
      Height = 24
      CharCase = ecUpperCase
      DataField = 'REPLUSER'
      DataSource = DataSource1
      TabOrder = 2
    end
    object DBEdit5: TDBEdit
      Left = 20
      Top = 217
      Width = 157
      Height = 24
      DataField = 'REPLPSW'
      DataSource = DataSource1
      TabOrder = 3
    end
    object DBEdit6: TDBEdit
      Left = 20
      Top = 276
      Width = 157
      Height = 24
      DataField = 'REPLROLE'
      DataSource = DataSource1
      TabOrder = 5
    end
    object DBEdit7: TDBEdit
      Left = 230
      Top = 158
      Width = 139
      Height = 24
      DataField = 'SEPARATOR'
      DataSource = DataSource1
      TabOrder = 6
    end
    object DBCheckBox3: TDBCheckBox
      Left = 230
      Top = 217
      Width = 131
      Height = 21
      Caption = '&Disabled'
      DataField = 'DISABLED'
      DataSource = DataSource1
      TabOrder = 7
      ValueChecked = 'Y'
      ValueUnchecked = 'N'
    end
    object DBLookupComboBox1: TDBLookupComboBox
      Left = 20
      Top = 98
      Width = 349
      Height = 24
      DataField = 'DBID'
      DataSource = DataSource1
      DropDownRows = 10
      KeyField = 'DBID'
      ListField = 'NAME'
      ListSource = DataSource2
      TabOrder = 1
      OnDropDown = DBLookupComboBox1DropDown
    end
    object Button2: TButton
      Left = 179
      Top = 218
      Width = 17
      Height = 24
      Caption = '*'
      TabOrder = 4
      OnClick = Button2Click
    end
  end
  inherited Table: TIBDataSet
    AfterOpen = TableAfterOpen
    SelectSQL.Strings = (
      'SELECT * FROM'
      'SCHEMADB'
      '')
    Left = 93
    Top = 16
  end
  inherited DataSource1: TDataSource
    OnDataChange = DataSource1DataChange
    Left = 112
    Top = 32
  end
  object IBQuery1: TIBQuery
    Database = IBReplDataModule.IBCfgDB
    Transaction = IBReplDataModule.IBCfgTransaction
    BeforeOpen = IBQuery1BeforeOpen
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'SELECT * FROM '
      'DATABASES'
      'ORDER BY NAME')
    Left = 168
    Top = 16
  end
  object DataSource2: TDataSource
    DataSet = IBQuery1
    Left = 184
    Top = 32
  end
end
