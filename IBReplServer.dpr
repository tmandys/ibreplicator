program IBReplServer;

uses
  {$IFDEF LINUX}
  QForms,
  {$ELSE}
  {$IFDEF VER130}
  DbgFixD5,
  {$ENDIF}
  Forms,
  {$ENDIF}
  fm_Server in 'fm_Server.pas' {IBReplicationServerForm},
  dm_IBRepl in 'dm_IBRepl.pas' {IBReplDataModule: TDataModule},
  dg_About in 'dg_About.pas' {AboutDlg},
  dg_ServerSettings in 'dg_ServerSettings.pas' {TServerSettingsDialog},
  dg_ServerTaskSettings in 'dg_ServerTaskSettings.pas' {ServerTaskSettingsDialog},
  dg_Lookup in 'dg_Lookup.pas' {LookupDlg},
  dm_IBRepl_Server in 'dm_IBRepl_Server.pas';

{$R *.res}

begin
  Application.Initialize;
  {$IFNDEF LINUX}
  Application.HelpFile := 'ibrepl2.hlp';
  {$ENDIF}
  DBIniSection2:= '.Server';
  Application.CreateForm(TIBReplicationServerForm, IBReplicationServerForm);
  Application.Run;
end.
