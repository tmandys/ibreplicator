unit IBReplVerRec;

interface
uses
  SysUtils, Classes, DB, IBCustomDataSet, IBSQL, IBReplicator, IBHeader;

type
  TReplVerRecField = record
    FieldName: string;
    FieldId: Integer;
    FieldType: Byte;
  end;
  TReplVerRecFields = array of TReplVerRecField;

  TIBSQLVersionRecord = class(TIBSQL)
    fConfigDatabase: TIBDatabase;
    fConfigDatabaseTransaction: TIBTransaction;
    fConfigDatabasePrefix: string;
    fObjPrefix: string;

    fSchemaId: Integer;
    fRelationId: Integer;
    fGroupId: Integer;

    fRelationName: string;
    fVerRecFields: TReplVerRecFields;
    fCurPrimaryKey: string;
    procedure SetConfigDatabase(const Value: TIBDatabase);
    procedure SetObjPrefix(const Value: string);
    procedure SetRelationId(const Value: Integer);
    procedure SetSchemaId(const Value: Integer);
  protected
    procedure InternalPrepare; override;
    procedure InternalClose; override;
//    procedure InternalFirst; override;
//    procedure InternalLast; override;
//    procedure InternalInitFieldDefs; override;
//    procedure InternalInitRecord(Buffer: PChar); override;
    function GetCanModify: Boolean; override;
  published
    property ConfigDatabase: TIBDatabase read fConfigDatabase write SetConfigDatabase;
    property ConfigDatabasePrefix: string read fConfigDatabasePrefix write fConfigDatabasePrefix;
    property SchemaId: Integer read fSchemaId write SetSchemaId;
    property RelationId: Integer read fRelationId write SetRelationId;
    property ObjPrefix: string read fObjPrefix write SetObjPrefix;

    property Active;
    property AutoCalcFields;
    property AfterClose;
    property AfterEdit;
    property AfterOpen;
    property AfterRefresh;
    property AfterScroll;
    property BeforeClose;
    property BeforeOpen;
    property BeforeRefresh;
    property BeforeScroll;
    property OnCalcFields;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;

//    procedure Revert(const aFields: string); overload;
//    procedure Revert(const aFields: TStringOpenArray); overload;
//    property PrimaryKeyCount: Integer read GetPrimaryKeyCount;
//    property PrimaryKeyFields[Index: Integer]: string read GetPrimaryKeyField;
//    property KeyValues[Index: Integer]: Variant read GetKeyValues write SetKeyValues;
//    property FieldValidFrom[Index: Integer]: Integer read GetValidFrom;
//    property FieldValidTo[Index: Integer]: Integer read GetValidFrom;
    property RelationName: string read fRelationName;
//    property SeqId: Integer read GetSeqId;
  end;
  end;

  TIBReplicatorVersionRecord = class(TIBCustomDataSet)
  private
    fConfigDatabase: TIBDatabase;
    fConfigDatabaseTransaction: TIBTransaction;
    fConfigDatabasePrefix: string;
    fObjPrefix: string;

    fSchemaId: Integer;
    fRelationId: Integer;
    fGroupId: Integer;
    fVerRecQuery: TIBSQL;

    fRelationName: string;
    fVerRecFields: TReplVerRecFields;
    fCurPrimaryKey: string;
    procedure SetVerRecFields;
    procedure SetConfigDatabase(const Value: TIBDatabase);
    procedure SetObjPrefix(const Value: string);
    procedure SetRelationId(const Value: Integer);
    procedure SetSchemaId(const Value: Integer);
  protected
    procedure InternalPrepare; override;
    procedure InternalClose; override;
//    procedure InternalFirst; override;
//    procedure InternalLast; override;
//    procedure InternalInitFieldDefs; override;
//    procedure InternalInitRecord(Buffer: PChar); override;
    function GetCanModify: Boolean; override;
  published
    property ConfigDatabase: TIBDatabase read fConfigDatabase write SetConfigDatabase;
    property ConfigDatabasePrefix: string read fConfigDatabasePrefix write fConfigDatabasePrefix;
    property SchemaId: Integer read fSchemaId write SetSchemaId;
    property RelationId: Integer read fRelationId write SetRelationId;
    property ObjPrefix: string read fObjPrefix write SetObjPrefix;

    property Active;
    property AutoCalcFields;
    property AfterClose;
    property AfterEdit;
    property AfterOpen;
    property AfterRefresh;
    property AfterScroll;
    property BeforeClose;
    property BeforeOpen;
    property BeforeRefresh;
    property BeforeScroll;
    property OnCalcFields;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;

//    procedure Revert(const aFields: string); overload;
//    procedure Revert(const aFields: TStringOpenArray); overload;
//    property PrimaryKeyCount: Integer read GetPrimaryKeyCount;
//    property PrimaryKeyFields[Index: Integer]: string read GetPrimaryKeyField;
//    property KeyValues[Index: Integer]: Variant read GetKeyValues write SetKeyValues;
//    property FieldValidFrom[Index: Integer]: Integer read GetValidFrom;
//    property FieldValidTo[Index: Integer]: Integer read GetValidFrom;
    property RelationName: string read fRelationName;
//    property SeqId: Integer read GetSeqId;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('InterBase', [TIBReplicatorVersionRecord]);
end;

constructor TIBReplicatorVersionRecord.Create(aOwner: TComponent); override;
begin
  inherited;
end;

destructor TIBReplicatorVersionRecord.Destroy; override;
begin
end;

procedure TIBReplicatorVersionRecord.InternalPrepare;
var
  Q1: TIBSQL;
  S: string;
  I: Integer;
resourcestring
  sNoConfigDatabase = 'Config database not assigned';
  sSchemaNotDefined = 'Relation id not defined in record version schema';
  sNoPrimaryKey = 'No primary key';
begin
  SetLength(fVerRecFields, 0);
  if fConfigDatabase = nil then
    IBReplicatorError(sConfigDatabaseNotDefined, 0);
  F:= fConfigDatabase.Connected;
  if not F then
    fConfigDatabase.Open;
  try
    fRelationName:= '';
    Q1:= TIBSQL.Create(nil);
    try
      Q1.Database:= fConfigDatabase;
      Q1.Transaction:= fConfigDatabaseTransaction;
      Q1.Transaction.StartTransaction;
      try
        Q1.SQL.Add('SELECT R.RELATIONNAME,R.TARGETNAME AS TARGETTNAME,R.TARGETTYPE,F.FIELDID,F.FIELDNAME,F.TARGETNAME,F.FIELDTYPE,F.TARGETNAME AS TARGETFNAME,F.OPTIONS FROM '+FormatIdentifier(Q1.Database.SQLDialect, fConfigDatabasePrefix+'RELATIONS')+' R INNER JOIN '+FormatIdentifier(Q1.Database.SQLDialect, fConfigDatabasePrefix+'SCHEMA')+' S INNER JOIN '+FormatIdentifier(Q1.Database.SQLDialect, fConfigDatabasePrefix+'FIELDS')+' F ON R.SCHEMAID=F.SCHEMAID AND R.GROUPID=F.GROUPID AND R.RELATIONID=F.RELATIONID ON R.SCHEMAID=S.SCHEMAID');
        Q1.SQL.Add(Format('WHERE R.SCHEMAID=%d AND R.RELATIONID=%d AND R.GROUPID=%d AND S.SCHEMATYPE=%d ORDER BY F.FIELDID', [fSchemaId, fRelationId, fGroupId, schtVersion]));
        Q1.ExecQuery;
        if Q1.EOF then
          IBReplicatorError(sSchemaNotDefined, 0);
        fRelationName:= Q1.FieldByName('RELATIONNAME').asString;
        while not Q1.EOF do
        begin
          SetLength(fVerRecFields, Length(fVerRecFields)+1);
          with fVerRecFields[Length(fVerRecFields]-1] do
          begin
            FieldId:= Q1.FieldByName('FIELDID').asInteger;
            FieldName:= Q1.FieldByName('FIELDNAME').asString;
            FieldType:= Q1.FieldByName('FIELDTYPE').asInteger;
          end;
          Q1.Next;
        end;
      finally
        Q1.Transaction.Commit;
      end;
    finally
      Q1.Free;
    end;
    S:= '';
    for I:= 0 to Length(fVerRecFields)-1 do
    begin
      if fVerRecFields[I].FieldType = 1 then
      begin
        if S <> '' then
          S:= S+' AND ';
        S:= S + FormatIdentifier(fDatabase.SQLDialect, fVerRecFields[I].FieldName) + '=:'+FormatIdentifier(fDatabase.SQLDialect, fVerRecFields[I].FieldName);
      end;
    end;
    if S = '' then
      IBReplicator(sNoPrimaryKey, 0);
    QSelect.SQL.Text:= Format('SELECT * FROM %s WHERE '+S, FormatIdentifier(Database.SQLDialect, fRelationName]);  // call InternalUnprepare
  finally
    if not F then
      fConfigDatabase.Close;
  end;
  inherited;
end;

procedure TIBReplicatorVersionRecord.InternalPrepare;
begin
  if not InternalPrepared then
    Exit;
  inherited;
  fRelationName:= '';
  SetLength(fVerRecFields, 0);
end;

procedure TIBReplicatorVersionRecord.InternalOpen;
var
  Q1: TIBSQL;
  S: string;
  I: Integer;
  F: TIBSQLVAR;
begin
  inherited;
  for I:= 0 to Length(fVerRecFields)-1 do
  begin
    if fVerRecFields[I].FieldType = 1 then
    begin
      F:= QSelect.FieldByName(fVerRecFields[I].FieldName);
      if F:SQLType = SQL_TIME
      if S <> '' then
        S:= S+' AND ';
      S:= S + FormatIdentifier(fDatabase.SQLDialect, fVerRecFields[I].FieldName) + '=:'+FormatIdentifier(fDatabase.SQLDialect, fVerRecFields[I].FieldName);
    end;
  end;
end;

procedure TIBReplicatorVersionRecord.InternalClose;
begin
  inherited;
  fRelationName:= '';
end;

function TIBReplicatorVersionRecord.GetCanModify: Boolean;
begin
  Result:= False;
end;

procedure TIBReplicatorVersionRecord.SetConfigDatabase(
  const Value: TIBDatabase);
begin
  CheckInactive;
  if fConfigDatabase <> Value then
  begin
    fConfigDatabase:= Value;
    Unprepare;
  end;
end;

procedure TIBReplicatorVersionRecord.SetSchemaId(const Value: Integer);
begin
  CheckInactive;
  if fSchemaId <> Value then
  begin
    fSchemaId:= Value;
    Unprepare;
  end;
end;

procedure TIBReplicatorVersionRecord.SetRelationId(const Value: Integer);
begin
  CheckInactive;
  if fRelationId <> Value then
  begin
    fRelationId:= Value;
    Unprepare;
  end;
end;

procedure TIBReplicatorVersionRecord.SetObjPrefix(const Value: string);
begin
  CheckInactive;
  fObjPrefix := Value;
end;

end.