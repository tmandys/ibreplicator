unit dg_ManSettings;

interface

uses
  {$IFDEF LINUX}
  Libc, QGraphics, QControls, QForms, QDialogs, QStdCtrls, QComCtrls, QExtCtrls,
  {$ELSE}
  Windows, Messages, Graphics, Controls, Forms, Dialogs, StdCtrls, ComCtrls, ExtCtrls,
  {$ENDIF}
  SysUtils, Classes, dg_ServerSettings;

type
  TManSettingsDialog = class(TServerSettingsDialog)
    KeyLength: TEdit;
    Label110: TLabel;
    Label19: TLabel;
    IBUtilPath: TEdit;
  private
    { Private declarations }
  protected
    procedure ReadFromIni; override;
    procedure WriteToIni; override;
  public
  end;

var
  ManSettingsDialog: TManSettingsDialog;

implementation
uses
  AuxProj, dm_IBRepl;

{$IFDEF LINUX}
{$R *.xfm}
{$ELSE}
{$R *.dfm}
{$ENDIF}

{ TServerSettingsDialog1 }

procedure TManSettingsDialog.ReadFromIni;
begin
  inherited;
  KeyLength.Text:= IntToStr(Ini.ReadInteger(DBIniSection+DBIniSection2, 'KeyLength', 255));
  IBUtilPath.Text:= Ini.ReadString(DBIniSection, 'IBUtilPath', '');
end;

procedure TManSettingsDialog.WriteToIni;
begin
  inherited;
  Ini.WriteString(DBIniSection+DBIniSection2, 'KeyLength', KeyLength.Text);
  Ini.WriteString(DBIniSection, 'IBUtilPath', IBUtilPath.Text);
end;

end.
