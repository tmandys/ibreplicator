(* IB Replication Suite
 * Copyright (C) 2002,2003 by Tomas Mandys-MandySoft
 *)

unit ft_Form;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, IniFiles;

type
  TAppForm = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    function GetLayoutIniSection: string;
    { Private declarations }
  public
    procedure SetFormSettings;
    procedure GetFormSettings;
    function GetSettIni: TIniFile;
  end;

var
  AppForm: TAppForm;

implementation
uses
  AuxProj, CmdLine, dm_IBRepl;

{$IFDEF LINUX}
{$R *.xfm}
{$ELSE}
{$R *.dfm}
{$ENDIF}

procedure TAppForm.FormCreate(Sender: TObject);
begin
  inherited;
  if GetKeyState(VK_SHIFT) and $80 = 0 then
    SetFormSettings;
end;

procedure TAppForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  if GetKeyState(VK_SHIFT) and $80 <> 0 then
    GetFormSettings;
end;

procedure TAppForm.SetFormSettings;
var
  Ini: TIniFile;
  Placement: TWindowPlacement;
  WS: TWindowState;
begin
  Ini:= GetSettIni;
  if Ini <> nil then
  try
    WS:= TWindowState(Ini.ReadInteger(GetLayoutIniSection, 'Form.WindowState', Integer(WindowState)));
    if WS <> wsNormal then
      begin
        Placement.length := SizeOf(TWindowPlacement);
        GetWindowPlacement(Handle, @Placement);
        Placement.rcNormalPosition.Left:= Ini.ReadInteger(GetLayoutIniSection, 'Form.Left', Placement.rcNormalPosition.Left);
        Placement.rcNormalPosition.Top:= Ini.ReadInteger(GetLayoutIniSection, 'Form.Top', Placement.rcNormalPosition.Top);
        Placement.rcNormalPosition.Right:= Placement.rcNormalPosition.Left + Ini.ReadInteger(GetLayoutIniSection, 'Form.Width', Placement.rcNormalPosition.Right-Placement.rcNormalPosition.Left);
        Placement.rcNormalPosition.Bottom:= Placement.rcNormalPosition.Top + Ini.ReadInteger(GetLayoutIniSection, 'Form.Height', Placement.rcNormalPosition.Bottom-Placement.rcNormalPosition.Top);
        SetWindowPlacement(Handle, @Placement);
      end
    else
      begin
        Left:= Ini.ReadInteger(GetLayoutIniSection, 'Form.Left', Left);
        Top:= Ini.ReadInteger(GetLayoutIniSection, 'Form.Top', Top);
        Width:= Ini.ReadInteger(GetLayoutIniSection, 'Form.Width', Width);
        Height:= Ini.ReadInteger(GetLayoutIniSection, 'Form.Height', Height);
      end;
    WindowState:= WS;
  finally
    Ini.Free;
  end;
end;

procedure TAppForm.GetFormSettings;
var
  Ini: TIniFile;
  Placement: TWindowPlacement;
begin
  Ini:= GetSettIni;
  if Ini <> nil then
  try
    if WindowState <> wsNormal then
      begin
        Placement.length := SizeOf(TWindowPlacement);
        GetWindowPlacement(Handle, @Placement);
        Ini.WriteInteger(GetLayoutIniSection, 'Form.Left', Placement.rcNormalPosition.Left);
        Ini.WriteInteger(GetLayoutIniSection, 'Form.Top', Placement.rcNormalPosition.Top);
        Ini.WriteInteger(GetLayoutIniSection, 'Form.Width', Placement.rcNormalPosition.Right-Placement.rcNormalPosition.Left);
        Ini.WriteInteger(GetLayoutIniSection, 'Form.Height', Placement.rcNormalPosition.Bottom-Placement.rcNormalPosition.Top);
      end
    else
      begin
        Ini.WriteInteger(GetLayoutIniSection, 'Form.Left', Left);
        Ini.WriteInteger(GetLayoutIniSection, 'Form.Top', Top);
        Ini.WriteInteger(GetLayoutIniSection, 'Form.Width', Width);
        Ini.WriteInteger(GetLayoutIniSection, 'Form.Height', Height);
      end;
    Ini.WriteInteger(GetLayoutIniSection, 'Form.WindowState', Integer(WindowState));
  finally
    Ini.Free;
  end;
end;

function TAppForm.GetSettIni: TIniFile;
var
  S: string;
begin
  Result:= nil;
  if IsThereCmd('NOSET', clUpcase) then
    Exit;
  S:= '';
  GetCmdString('SI:', clUpcase, S);
  if S = '' then
  S:= Ini.ReadString('Global', 'Ini.Layout', GetProgramPath+'IBREPL.DSK');
  if S <> '' then
    Result:= TIniFile.Create(S);
end;

function TAppForm.GetLayoutIniSection: string;
begin
  Result:= Format('%s-%dx%d', [ExtractFileName(ParamStr(0)), Screen.Width, Screen.Height]);
end;

end.
