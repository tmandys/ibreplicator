(* IB Replication Suite
 * Copyright (C) 2002,2003 by Tomas Mandys-MandySoft
 *)

unit dg_SchemaRelationsOptions;

interface

uses
  {$IFDEF LINUX}
  Libc, QGraphics, QForms, QControls, QStdCtrls, QButtons, QExtCtrls,
  {$ELSE}
  Windows, Graphics, Forms, Controls, StdCtrls, Buttons, ExtCtrls,
  {$ENDIF}
  SysUtils, Classes;

type
  TSchemaRelationsOptionsDialog = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    HelpBtn: TButton;
    Label1: TLabel;
    Options_1: TCheckBox;
    Options_0: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure OptionsChange(Sender: TObject);
    procedure HelpBtnClick(Sender: TObject);
  private
    function GetOptions: Integer;
    procedure SetOptions(const Value: Integer);
    procedure AdjustControls;
    { Private declarations }
  public
    TargetType: Char;
    property Options: Integer read GetOptions write SetOptions;
    { Public declarations }
  end;

var
  SchemaRelationsOptionsDialog: TSchemaRelationsOptionsDialog;

implementation
uses
  IBReplicator;

{$IFDEF LINUX}
{$R *.xfm}
{$ELSE}
{$R *.dfm}
{$ENDIF}

{ TSchemaFieldsConflictOptionsDialog }

function TSchemaRelationsOptionsDialog.GetOptions: Integer;
var
  I: Integer;
  C: TComponent;
begin
  Result:= 0;
  for I:= 0 to 15 do
  begin
    C:= FindComponent(Format('Options_%d', [I]));
    if (C <> nil) and TCheckBox(C).Checked then
      Result:= Result or (1 shl I);
  end;
end;

procedure TSchemaRelationsOptionsDialog.SetOptions(
  const Value: Integer);
var
  I: Integer;
  C: TComponent;
begin
  for I:= 0 to 15 do
  begin
    C:= FindComponent(Format('Options_%d', [I]));
    if C <> nil then
      TCheckBox(C).Checked:= Value and (1 shl I) <> 0;
  end;
  AdjustControls;
end;

procedure TSchemaRelationsOptionsDialog.FormShow(Sender: TObject);
begin
  AdjustControls;
end;

procedure TSchemaRelationsOptionsDialog.AdjustControls;
begin
  Options_1.Enabled:= TargetType = 'T';
end;

procedure TSchemaRelationsOptionsDialog.OptionsChange(Sender: TObject);
begin
  AdjustControls;
end;

procedure TSchemaRelationsOptionsDialog.HelpBtnClick(Sender: TObject);
begin
  Application.{$IFDEF LINUX}ContextHelp{$ELSE}HelpContext{$ENDIF}(HelpContext);
end;

end.


