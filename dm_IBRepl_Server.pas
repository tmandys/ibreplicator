unit dm_IBRepl_Server;

interface
uses
  Windows, SysUtils, dm_IBRepl, Connect, IBReplicator, Classes, SyncObjs, Terminal;

type
  TIBReplDataModule2 = class(TIBReplDataModule)
  private
    fLogCriticalSection: TCriticalSection;
    procedure OnException(Sender: TComponent; E: Exception; const aOriginalMessage, aName: string);
    procedure OnExceptionReplLog(Sender: TComponent; E: Exception; const aName: string; aChannel: Byte; const aOriginalMessage: string);
    procedure OnExceptionDBLog(Sender: TComponent; E: Exception; const aName: string; aChannel: Byte; const aOriginalMessage: string);
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure AssignLoggers(aIBReplicator: TIBReplicator); override;
  end;

  TFileTermLogger2 = class(TFileSysLogger)  // the same as TFileLogger
  private
    FTerminal: TTerminal;
    FTermChar: Boolean;
  protected
    procedure OpenConn; override;
    procedure DoLog(const aText: string); override;
  published
    property Terminal: TTerminal read FTerminal write FTerminal;
    property TermChar: Boolean read FTermChar write FTermChar;
  end;

implementation
uses
  fm_Server;

{ TFileTermLogger2 }

procedure TFileTermLogger2.DoLog;
begin
  if FTerminal <> nil then
  begin
    if FTermChar or (lfAutoCR in LogFlags) then
      FTerminal.LogTerm(aText)
    else
      FTerminal.Log(aText);
  end;
  inherited;
end;

procedure TFileTermLogger2.OpenConn;
begin
  try
    inherited;
  except
    on E: Exception do    // handle open file error exception, allow log to terminal
      DoOnException(E, '', lchError, 'TFileTermLogger.OpenConn');
  end;
end;

{ TIBReplDataModule2 }

constructor TIBReplDataModule2.Create(aOwner: TComponent);
begin
  fLogCriticalSection:= TCriticalSection.Create;
  inherited;
end;

destructor TIBReplDataModule2.Destroy;
begin
  inherited;
  fLogCriticalSection.Free;
end;

procedure TIBReplDataModule2.AssignLoggers(aIBReplicator: TIBReplicator);
begin
  with aIBReplicator do
  begin
    DBLog:= TFileTermLogger2.Create(aIBReplicator);
    TFileTermLogger2(DBLog).Terminal:= IBReplicationServerForm.DBLog;
    DBLog.OnException:= OnExceptionDBLog;
    DBLog.CriticalSection2:= fLogCriticalSection;
    ReplLog:= TFileTermLogger2.Create(aIBReplicator);
    TFileTermLogger2(ReplLog).Terminal:= IBReplicationServerForm.ReplLog;
    ReplLog.OnException:= OnExceptionReplLog;
    ReplLog.CriticalSection2:= fLogCriticalSection;
  end;
end;

procedure TIBReplDataModule2.OnException(Sender: TComponent; E: Exception; const aOriginalMessage, aName: string);
var
  S: string;
begin
  if not (csDestroying in ComponentState) then
  begin
    if Sender = nil then
      S:= '=nil'
    else if Sender is TComponent then
      S:= '='''+TComponent(Sender).Name+''''
    else
      S:= ' is '+Sender.ClassName;
    TFileTermLogger2(Sender).Terminal.Log(Format('%s.OnException(Sender%s; E.Message=''%s''; Message: ''%s'')', [aName, S, E.Message, aOriginalMessage]));  // Sender.Name might not be assigned
  end;
end;


procedure TIBReplDataModule2.OnExceptionReplLog(Sender: TComponent; E: Exception; const aName: string; aChannel: Byte; const aOriginalMessage: string);
begin
  OnException(Sender, E, aOriginalMessage, 'ReplLog');
end;

procedure TIBReplDataModule2.OnExceptionDBLog(Sender: TComponent; E: Exception; const aName: string; aChannel: Byte; const aOriginalMessage: string);
begin
  OnException(Sender, E, aOriginalMessage, 'DBLog');
end;

end.

