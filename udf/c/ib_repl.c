/* ib_repl.c - UDF library for IB Replicator
 * Copyright (C) 2003  Tomas Mandys-MandySoft
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA  02111-1307  USA
 */
 

#include <sys/types.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

// Interbase header files
#include <ibase.h>

#include "ib_util.h"

#if defined __BORLANDC__ && defined __WIN32__
#define EXPORT _export
#else
#define EXPORT ISC_EXPORT
#endif

#if !defined ISC_DOUBLE
#define ISC_DOUBLE double
#endif


//#define BLOBCALLBACK

// not defined in gcc lib
#define min(__a,__b)    (((__a) < (__b)) ? (__a) : (__b)) 

/* =========================================
     Aux. function definition
   ========================================= */

void blob_to_buff2(BLOBCALLBACK b, void ISC_FAR * buff, ISC_ULONG maxlen)
{
  unsigned char ISC_FAR * buff2;
  
  ISC_USHORT actual_length;

  memset(buff, 0, maxlen);
  maxlen = min(maxlen, (ISC_ULONG) b->blob_total_length);

  if (maxlen == 0 || !b || !b->blob_handle || b->blob_max_segment == 0)
    return; 

  buff2 = buff;

  while ((*b->blob_get_segment) (b->blob_handle, buff2, (ISC_USHORT) min((ISC_ULONG) b->blob_max_segment, maxlen), &actual_length)) {
    buff2+=actual_length;
    maxlen-= actual_length;
    if (maxlen == 0) 
      break;
  }
}

void ISC_FAR * blob_to_buff(BLOBCALLBACK b)
{
  void ISC_FAR * buff = malloc(b->blob_total_length);
  if (buff) 
    blob_to_buff2(b, buff, b->blob_total_length);
  return (buff);
}

#define BUFFER_SIZE 127L

void buff_to_blob(unsigned char ISC_FAR * buff, ISC_ULONG len, BLOBCALLBACK b)
{
  if (!b || !b->blob_handle) return;
  while (len > 0) {
    (*b->blob_put_segment) (b->blob_handle, buff, (ISC_USHORT) min(BUFFER_SIZE, len));
    buff += BUFFER_SIZE;
	    if (len > BUFFER_SIZE)
      len -= BUFFER_SIZE;
    else
      len = 0;
  }
}

#undef BUFFER_SIZE

void buff_rtrim(unsigned char ISC_FAR * buff, ISC_ULONG ISC_FAR * n)
{
  while (*n > 1) {
    if (buff[*n-1] != 0)
      break;
    (*n)--;
  }
}

static ISC_DATE ib_util_nday (struct tm ISC_FAR * times)
{
/**************************************
 *
 *	n d a y
 *
 **************************************
 *
 * Functional description
 *	Convert a calendar date to a numeric day
 *	(the number of days since the base date).
 *
 **************************************/
short	day, month, year;
ISC_LONG	c, ya;

day = times->tm_mday;
month = times->tm_mon + 1;
year = times->tm_year + 1900;

if (month > 2)
    month -= 3;
else
    {
    month += 9;
    year -= 1;
    }

c = year / 100;
ya = year - 100 * c;

return (ISC_DATE) (((ISC_INT64) 146097 * c) / 4 + 
    (1461 * ya) / 4 + 
    (153 * month + 2) / 5 + 
    day + 1721119 - 2400001);
}

static void ib_util_isc_encode_timestamp (void ISC_FAR * times_arg, ISC_TIMESTAMP ISC_FAR * date)
{
/**************************************
 *
 *	i s c _ e n c o d e _ t i m e s t a m p
 *
 **************************************
 *
 * Functional description
 *	Convert from UNIX time structure to internal timestamp format.
 *
 *	Note: the date arguement is really an ISC_TIMESTAMP -- however the
 *	definition of ISC_TIMESTAMP is not available from all the source
 *	modules that need to use isc_encode_timestamp
 *
 **************************************/
struct tm ISC_FAR * times;

times = (struct tm ISC_FAR *) times_arg;

date->timestamp_date = ib_util_nday (times);
date->timestamp_time = 
	((times->tm_hour * 60 + times->tm_min) * 60 + 
		times->tm_sec) * ISC_TIME_SECONDS_PRECISION;
}

/* ===============================================
     Export function definition
   =============================================== */

ISC_TIMESTAMP ISC_FAR * EXPORT NowUTC(void)
{
  time_t t = time(NULL);
  struct tm ISC_FAR *utc = gmtime(&t);
  
  ISC_TIMESTAMP ISC_FAR *utc_buff = ib_util_malloc(sizeof(ISC_TIMESTAMP));
  if (utc_buff)
    ib_util_isc_encode_timestamp(utc, utc_buff);
 
  return (utc_buff);
}

ISC_LONG EXPORT Cmp_Blobs(BLOBCALLBACK b1, BLOBCALLBACK b2)
{
  void ISC_FAR * buff1;
  void ISC_FAR * buff2;
  ISC_LONG res = 0;

  if (b1->blob_handle && !b2->blob_handle || !b1->blob_handle && b2->blob_handle)
    return (0);
  if (!b1->blob_handle && !b2->blob_handle)
    return (1);
  if (b1->blob_total_length != b2->blob_total_length)
    return (0);
  if (b1->blob_total_length == 0)
    return (1);

  buff1 = blob_to_buff(b1);
  if (!buff1) goto ret2;
  buff2 = blob_to_buff(b2);
  if (!buff2) goto ret1;
  res = (memcmp(buff1, buff2, b1->blob_total_length) == 0) ? 1:0; 

  free(buff1);
ret1:
  free(buff2);
ret2:
  return (res);
}
 
void EXPORT Integer_To_Blob(ISC_ULONG ISC_FAR * val, BLOBCALLBACK b1)
{
  ISC_ULONG n = sizeof(ISC_ULONG);

  if (val == NULL) 
    return;
  buff_rtrim((unsigned char ISC_FAR *) val, &n);
  buff_to_blob((unsigned char ISC_FAR *) val, n, b1);  
}

void EXPORT Double_To_Blob(ISC_DOUBLE ISC_FAR * val, BLOBCALLBACK b1)
{
  ISC_ULONG n = sizeof(ISC_DOUBLE);

  if (val == NULL) 
    return;
  buff_rtrim((unsigned char ISC_FAR *) val, &n);
  buff_to_blob((unsigned char ISC_FAR *) val, n, b1);  
}

void EXPORT TimeStamp_To_Blob(ISC_TIMESTAMP ISC_FAR * val, BLOBCALLBACK b1)
{
  ISC_ULONG n = sizeof(ISC_TIMESTAMP);

  if (val == NULL) 
    return;
  buff_rtrim((unsigned char ISC_FAR *) val, &n);
  buff_to_blob((unsigned char ISC_FAR *) val, n, b1);  
}


void EXPORT String_To_Blob(unsigned char ISC_FAR * val, BLOBCALLBACK b1)
{
  ISC_ULONG n = 0;
  if (val == NULL) 
    return;
  while (*(val+n) != 0) 
    n++;
  buff_to_blob((unsigned char ISC_FAR *) val, n+1, b1);  
}

void EXPORT Concat_Blobs(BLOBCALLBACK b1, BLOBCALLBACK b2, BLOBCALLBACK b3)
{
  if (b1->blob_handle) {
    if (b1->blob_total_length > 0) {
      unsigned char ISC_FAR * buff = blob_to_buff(b1);
      if (!buff) return;
      buff_to_blob(buff, b1->blob_total_length, b3);
      free(buff);
    }  
  }
  if (b2->blob_handle) {
    if (b2->blob_total_length > 0) {
      unsigned char ISC_FAR * buff = blob_to_buff(b2);
      if (!buff) return;
      buff_to_blob(buff, b2->blob_total_length, b3);
      free(buff);
    }  
  }
}

ISC_ULONG EXPORT Blob_To_Integer(BLOBCALLBACK b1)
{
  ISC_ULONG res;
  blob_to_buff2(b1, &res, sizeof(ISC_ULONG));
  return (res);
}

ISC_DOUBLE EXPORT Blob_To_Double(BLOBCALLBACK b1)
{
  ISC_DOUBLE res;
  blob_to_buff2(b1, &res, sizeof(ISC_DOUBLE));
  return (res);
}

ISC_TIMESTAMP ISC_FAR * EXPORT Blob_To_TimeStamp(BLOBCALLBACK b1)
{
  ISC_TIMESTAMP ISC_FAR * res = ib_util_malloc(sizeof(ISC_TIMESTAMP));
  if (res)
    blob_to_buff2(b1, res, sizeof(ISC_TIMESTAMP));
  return (res);
}

unsigned char ISC_FAR * EXPORT Blob_To_String(BLOBCALLBACK b1)
{
  unsigned char ISC_FAR * res = ib_util_malloc(b1->blob_total_length+1);
  if (res) 
    blob_to_buff2(b1, res, b1->blob_total_length+1);
  return (res);
}

ISC_ULONG EXPORT Blob_Length(BLOBCALLBACK b1)
{
  return (b1->blob_total_length);
}

ISC_ULONG EXPORT String_Length(unsigned char ISC_FAR * val)
{
  ISC_ULONG n = 0;
  if (val) {
    while (val[n])
      n++;
  }
  return n;
}

unsigned char ISC_FAR * EXPORT String_Copy(char ISC_FAR * val, ISC_ULONG ISC_FAR * m, ISC_ULONG ISC_FAR *len)
{
  unsigned char ISC_FAR * res;
  ISC_ULONG n;
  
  res = NULL;
  n = strlen(val);
  if (n > 0) {
    if (*m <= n && *m >= 1 && *len >= 1) {

      if (*m + *len -1 <= n)
        n = *len;
      else
        n+= 1 - *m;
      
      res = ib_util_malloc (n + 1);
      if (res) {
        memcpy(res, val + *m - 1, n); 
        res[n] = 0;
      }
    }
  } 
    
  return res;
}

ISC_ULONG EXPORT Memo_Line_Count(BLOBCALLBACK b1)
{
  ISC_LONG i, res;
  char fl;
  unsigned char ISC_FAR * buff;
  if (b1->blob_total_length == 0 || !b1->blob_handle)
    return 0;
  buff = malloc(b1->blob_total_length);
  if (!buff) return 0;
  blob_to_buff2(b1, buff, b1->blob_total_length);
  
  i = 0; res = 0; fl = 0;
  while (i < b1->blob_total_length) {
    if (buff[i] == 10) {
      res++;
      fl = 0;
    }
    switch (buff[i]) {
      case 10:
      case 13:
        break;
      default:
        fl = 1;
        break;
    }     
    i++;  
  }
  if (fl) {
    res++;
  }
  free(buff);
  return (res);
}


unsigned char ISC_FAR * EXPORT Memo_Get_Line(BLOBCALLBACK b1, ISC_ULONG ISC_FAR * n)
{
  ISC_LONG i, l, p1;
  unsigned char ISC_FAR * res;
  unsigned char ISC_FAR * buff = malloc(b1->blob_total_length);
  if (!buff) return 0;
  blob_to_buff2(b1, buff, b1->blob_total_length);
  
  i = 0;
  while ((*n > 0) && (i < b1->blob_total_length)) {
    if (buff[i] == 10) {
      (*n)--;
    }
    i++;
  }

  while ((i < b1->blob_total_length) && (buff[i] == 13)) {
    i++;
  }
  
  p1 = i; l = 0;
  while ((i < b1->blob_total_length) && (buff[i] != 10) && (buff[i] != 13) ) {
    l++;
    i++;
  }

  res = ib_util_malloc(l+1);
  if (!res) goto ret;
  memcpy (res, buff+p1, l);
  res[l] = 0;
ret:
  free(buff);
  return (res);
}


ISC_ULONG EXPORT Bit_And(ISC_ULONG ISC_FAR * a, ISC_ULONG ISC_FAR * b)
{
	return (*a & *b);
}

ISC_ULONG EXPORT Bit_Or(ISC_ULONG ISC_FAR * a, ISC_ULONG ISC_FAR * b)
{
	return (*a | *b);
}

ISC_ULONG EXPORT Bit_Not(ISC_ULONG ISC_FAR * a)
{
	return (~ *a);
}


ISC_ULONG EXPORT Bit_Xor(ISC_ULONG ISC_FAR * a, ISC_ULONG ISC_FAR * b)
{
	return (*a ^ *b);
}

