# Borland C 5.5 make file

ROOT=		e:\proj\ibrepl


# edit the IBASE variable to point to the Interbase/Firebird directory
#IBASE=		D:\Program files\Interbase
#IBASE_INC=      $(IBASE)\Sdk\include

IBASE=		D:\Program files\Firebird_2_0
IBASE_INC=      $(IBASE)\include

UDFPATH=        $(IBASE)\UDF

# edit the TOOLPATH variable to point to the Borland C++ directory
TOOLPATH=	d:\program files\bc

INCLUDE=	-I"$(TOOLPATH)\include" -I"$(IBASE_INC)" -I"$(TOOLPATH)\include"
LIB=		-L"$(TOOLPATH)\lib"
#LIBS=		$(IBASE)\lib\gds32.lib

COMMON_FLAGS=	-c -v -w+ -a4 -tWM -DWIN32 $(INCLUDE) 
CFLAGS=		$(COMMON_FLAGS) -tWC
LIB_CFLAGS=	$(COMMON_FLAGS) -tWCDE
LINK=		ILINK32.EXE

!include $(TOOLPATH)\bin\builtins.mak

.path.c=$(ROOT)\udf\c

FUNCLIB_FILES=	ib_repl.c

APPBUILD=1

# BCC cannot find uuid.lib in its \lib directory, copy to proj dir

ib_repl.dll: ib_repl.obj
# build a small argument file and use it
 	@echo -----------------------------------------------------------
        @echo BCC cannot find uuid.lib in its \lib directory, copying to 
        @echo proj dir
	@echo -----------------------------------------------------------
	@echo "$(TOOLPATH)\lib\c0d32.obj"+ > link.arg
	@echo $? >> link.arg
	@echo $@ >> link.arg
	@echo /x /Tpd >> link.arg 
	@echo "$(TOOLPATH)\lib\import32.lib"+ >> link.arg
	@echo "$(TOOLPATH)\lib\cw32mt.lib" >> link.arg
	@echo ib_repl.bcc.def >> link.arg
	@echo ..\delphi\ib_repl.res >> link.arg
       	$(LINK)  @link.arg -r
 	@echo -----------------------------------------------------------
	@echo You need to copy $@ to the interbase lib directory
	@echo in order for the server to load it. Trying ...
	@echo -----------------------------------------------------------
        @copy $@ "$(UDFPATH)\"

ib_repl.obj: ib_repl.c
	$(CC) $(CFLAGS) ib_repl.c 

