@rem make file for "Microsoft Visual C++ Toolkit 2003"
@rem first set environment using "Microsoft Visual C++ Toolkit 2003\vcvars32.bat"
@rem LINK : fatal error LNK1158: cannot run 'cvtres.exe'
@rem cvtres.exe is in WINDOWS\Microsoft.NET\Framework\v2.0.50727\ directory, add to PATH
@rem libc.lib .. single threaded
@rem libcmt.lib .. multi threaded, supeserver 2.5 is running as multithreaded
@rem NOTE: currently still crashes at superserver 2.5 Win32

set FIREBIRD_PATH=d:\Program Files\Firebird_2_0
set ROOT=e:\proj\ibrepl
@rem Use /MD (mscvrt.lib), /ML (libc.lib), /MT  (libcmt.lib), note mscvrt.lib is not contained in "MSVC Toolkit" but in MSVStudio
cl /TC /I"%FIREBIRD_PATH%\include"  ib_repl.c /LD /MT /link /DEF:ib_repl.msv.def "%FIREBIRD_PATH%\lib\ib_util_ms.lib" /VERSION:2.1 ..\delphi\ib_repl.res /MACHINE:X86

