unit IBUDFUtils;

interface

uses
  {$IFNDEF LINUX}
  Windows,
  {$ELSE}
  libc,
  {$ENDIF}
  SysUtils, Classes,
  {$IFDEF FPC}
  ibase40
  {$ELSE}
  IBHeader, IBErrorCodes, IBExternals, IB
  {$ENDIF};


function BlobToStream(const aBlob: TBlob; aStream: TStream): Integer;
function StreamToBlob(const aBlob: TBlob; aStream: TStream): Integer;

function ib_util_malloc(Size: Integer): Pointer; cdecl; external {$IFDEF LINUX}'ib_util.so'{$ELSE}'ib_util.dll'{$ENDIF};

implementation

function BlobToStream(const aBlob: TBlob; aStream: TStream): Integer;
var
  Buff: PChar;
  L: Integer;
begin
  Result:= 0;
  try
    if aBlob.MaxSegmentLength = 0 then
      Exit;
    GetMem(Buff, aBlob.MaxSegmentLength);
    try
      L:= aBlob.MaxSegmentLength+aBlob.TotalSize;  // dummy, but necessary, D5 raises unbelivable error if aBlob not used (optimization??)
      while aBlob.GetSegment(aBlob.BlobHandle, Buff, aBlob.MaxSegmentLength, L) <> 0 do
      begin
        aStream.WriteBuffer(Buff^, L);
        Inc(Result, L);
      end;
    finally
      FreeMem(Buff);
    end;
  except
    on E: Exception do
    begin
    {$IFNDEF LINUX}
      Messagebox(0, PChar(E.Message), 'IBUDFUtils.BlobToStream', MB_OK);
    {$ENDIF}
    end;
  end;
end;

function StreamToBlob(const aBlob: TBlob; aStream: TStream): Integer;
var
  Buff: PChar;
  L: Longint;
  I: SmallInt;
const
  BufferSize = 127;  // max smallint
begin
  Result:= 0;
  try
    GetMem(Buff, BufferSize);
    try
      L:= aStream.Size-aStream.Position;
      while L > 0 do
      begin
        if L > BufferSize then
          I:= BufferSize
        else
          I:= L;
        aStream.ReadBuffer(Buff^, I);
        aBlob.PutSegment(aBlob.BlobHandle, Buff, I);
        Dec(L, I);
        Inc(Result, I);
      end;
    finally
      FreeMem(Buff);
    end;
  except
    on E: Exception do
    begin
    {$IFNDEF LINUX}
      Messagebox(0, PChar(E.Message), 'IBUDFUtils.StreamToBlob', MB_OK);
    {$ENDIF}
    end;
  end;
end;

end.


