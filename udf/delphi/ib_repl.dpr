library ib_repl;

{.$MODE delphi} // FPC directive
uses
  {$IFNDEF LINUX}
  Windows,
  {$ELSE}
  Libc,
  {$ENDIF}
  {$IFDEF FPC}
  ibase60,
  ibase40,
  {$ELSE}
  IBIntf,
  IBExternals,
  IBHeader,
  IBUtils,
  {$ENDIF}
  IBUDFUtils,
  Connect,
  SysUtils,
  Classes;

{$IFNDEF LINUX}
{$IFNDEF FPC}
{$R *.RES}
{$ENDIF}
{$ELSE}
{$ENDIF}


function NowUTC(): PISC_TIMESTAMP; cdecl; export;
var
  TM: TCTimeStructure;
  DT: TDateTime;
  Y, M, D, Ho, Mi, Se, S100: Word;
begin
  DT:= Connect.NowUTC;
  DecodeDate(DT, Y, M, D);
  DecodeTime(DT, Ho, Mi, Se, S100);
  FillChar(TM, SizeOf(TM), 0);
  TM.tm_year:= Y-1900;
  TM.tm_mon:= M-1;
  TM.tm_mday:= D;
  TM.tm_hour:= Ho;
  TM.tm_min:= Mi;
  TM.tm_sec:= Se;
  TM.tm_isdst:= 0;
  Result:= ib_util_malloc(SizeOf(Result^));
  if (Result <> nil) then {$IFNDEF FPC}{$IFNDEF VER130}GetGDSLibrary.{$ENDIF}{$ENDIF}isc_encode_timestamp(@TM, Result);
end;

function Cmp_Blobs(var B1, B2: TBlob): Integer; cdecl; export;
var
  St1, St2: TMemoryStream;
begin
  Result:= 0;
  try
    if (B1.BlobHandle = nil) xor (B2.BlobHandle = nil) then
      Exit;
    if (B1.BlobHandle = nil){and (B2.BlobHandle = nil)} then
    begin
      Result:= 1;
      Exit;
    end;
    if B1.TotalSize <> B2.TotalSize then
      Exit;
    St1:= TMemoryStream.Create;
    try
      BlobToStream(B1, St1);
      St2:= TMemoryStream.Create;
      try
        BlobToStream(B2, St2);
        if CompareMem(St1.Memory, St2.Memory, St1.Size) then
          Result:= 1;
      finally
        St2.Free;
      end;
    finally
      St1.Free;
    end;
  except
  {$IFNDEF LINUX}
    on E: Exception do
    begin
      Messagebox(0, PChar(E.Message), 'ib_repl.Cmp_Blobs', MB_OK);
    end;
  {$ENDIF}
  end;
end;

procedure Stream_RTrim(St: TStream; aChar: Byte; aMinLen: Longint);
var
  B: Byte;
begin
  St.Seek(0, soFromEnd);
  while St.Position > aMinLen do   // RTrim #0
  begin
    St.Seek(-1, soFromCurrent);
    St.ReadBuffer(B, SizeOf(B));
    if (B <> aChar) then
      Break;
    St.Seek(-1, soFromCurrent);
  end;
  St.Size:= St.Position;
end;

procedure Integer_To_Blob(Val: PULong{or Long}; var B1: TBlob); cdecl; export;
var
  St: TMemoryStream;
begin
  if Val = nil then
    Exit;
  St:= TMemoryStream.Create;
  try
    St.WriteBuffer(Val^, SizeOf(Val^));
    Stream_RTrim(St, 0, 1);
    St.Position:= 0;
    StreamToBlob(B1, St);
  finally
    St.Free;
  end;
end;

procedure Double_To_Blob(Val: PDouble; var B1: TBlob); cdecl; export;
var
  St: TMemoryStream;
begin
  if Val = nil then
    Exit;
  St:= TMemoryStream.Create;
  try
    St.WriteBuffer(Val^, SizeOf(Val^));
    Stream_RTrim(St, 0, 1);
    St.Position:= 0;
    StreamToBlob(B1, St);
  finally
    St.Free;
  end;
end;

procedure TimeStamp_To_Blob(Val: PISC_TIMESTAMP; var B1: TBlob); cdecl; export;
var
  St: TMemoryStream;
begin
  if Val = nil then
    Exit;
  St:= TMemoryStream.Create;
  try
    St.WriteBuffer(Val^, SizeOf(Val^));
    Stream_RTrim(St, 0, 1);
    St.Position:= 0;
    StreamToBlob(B1, St);
  finally
    St.Free;
  end;
end;

procedure String_To_Blob(Val: PChar; var B1: TBlob); cdecl; export;
var
  St: TMemoryStream;
  PC: PChar;
begin
  if Val = nil then
    Exit;
  St:= TMemoryStream.Create;
  try
    PC:= Val-1;
    repeat
      Inc(PC);
      St.WriteBuffer(PC^, SizeOf(PC^));  // write with terminating #0 to distinguish NULL from empty string ('')
    until PC^ = #0;
    St.Position:= 0;
    StreamToBlob(B1, St);
  finally
    St.Free;
  end;
end;

procedure Concat_Blobs(var B1, B2, B3: TBlob); cdecl; export;
var
  St: TMemoryStream;
begin
  St:= TMemoryStream.Create;
  try
    if B1.BlobHandle <> nil then
      BlobToStream(B1, St);
    if B2.BlobHandle <> nil then
      BlobToStream(B2, St);
    St.Position:= 0;
    StreamToBlob(B3, St);
  finally
    St.Free;
  end;
end;

function Blob_To_Integer(var B1: TBlob): Long; cdecl; export;
var
  St: TMemoryStream;
begin
  St:= TMemoryStream.Create;
  try
    BlobToStream(B1, St);
    St.Position:= 0;
    FillChar(Result, SizeOf(Result), 0);
    St.ReadBuffer(Result, Min(SizeOf(Result), St.Size));
  finally
    St.Free;
  end;
end;

function Blob_To_Double(var B1: TBlob): Double; cdecl; export;
var
  St: TMemoryStream;
begin
  St:= TMemoryStream.Create;
  try
    BlobToStream(B1, St);
    St.Position:= 0;
    FillChar(Result, SizeOf(Result), 0);
    St.ReadBuffer(Result, Min(SizeOf(Result), St.Size));
  finally
    St.Free;
  end;
end;

function Blob_To_TimeStamp(var B1: TBlob): PISC_TIMESTAMP; cdecl; export;
var
  St: TMemoryStream;
begin
  St:= TMemoryStream.Create;
  try
    BlobToStream(B1, St);
    St.Position:= 0;
    Result:= ib_util_malloc(SizeOf(Result^));
    if (Result = nil) then Exit;
    FillChar(Result^, SizeOf(Result^), 0);
    St.ReadBuffer(Result^, Min(SizeOf(Result^), St.Size));
  finally
    St.Free;
  end;
end;

function Blob_To_String(var B1: TBlob): PChar; cdecl; export;
var
  St: TMemoryStream;
begin
  St:= TMemoryStream.Create;
  try
    BlobToStream(B1, St);
    St.Position:= 0;
    Result:= ib_util_malloc(St.Size+1);
    if (Result = nil) then Exit;
    St.ReadBuffer(Result^, St.Size);
    Result[St.Size]:= #0;
  finally
    St.Free;
  end;
end;

function Blob_Length(var B1: TBlob): Long; cdecl; export;
begin
  Result:= B1.TotalSize;
end;

function String_Length(Val: PChar): Long; cdecl; export;
begin
  Result:= StrLen(Val);
end;

function String_Copy(Val: PChar; M, Len: PULong): PChar; cdecl; export;
var
  N: ULong;
begin
  Result:= nil;
  N:= StrLen(Val);
  if (N > 0) then
  begin
    if (M^ <= n) and (M^ >= 1) and (Len^ >= 1) then
    begin
      if (M^+Len^-1 <= N) then
        N:= Len^
      else
        Inc(N, 1-M^);
      Result:= ib_util_malloc(N + 1);
      if (Result = nil) then Exit;
      Move(Val[M^-1], Result^, N);
      Result[N]:= #0;
    end;
  end;
end;

function Memo_Line_Count(var B1: TBlob): Long; cdecl; export;
var
  St: TMemoryStream;
  C: Char;
  Fl: Boolean;
begin
  St:= TMemoryStream.Create;
  try
    BlobToStream(B1, St);
    St.Position:= 0;
    Result:= 0;
    Fl:= False;
    while St.Position < St.Size do
    begin
      St.ReadBuffer(C, SizeOf(C));
      if (C = #10) then
      begin
        Inc(Result);
        Fl:= False;
      end;
      if not (C in [#10,#13]) then
        Fl:= True;
    end;
    if Fl then
      Inc(Result);
  finally
    St.Free;
  end
end;

function Memo_Get_Line(var B1: TBlob; var N: Long): PChar; cdecl; export;
var
  St: TMemoryStream;
  P1, L: Integer;
  C: Char;
begin
  St:= TMemoryStream.Create;
  try
    BlobToStream(B1, St);
    St.Position:= 0;

    while (N > 0) and (St.Position < St.Size) do
    begin
      St.ReadBuffer(C, SizeOf(C));
      if C = #10 then
        Dec(N);
    end;

    while St.Position < St.Size do
    begin
      St.ReadBuffer(C, SizeOf(C));
      if C <> #13 then
        Break;
    end;
    P1:= St.Position-1;
    L:= 0;
    while not (C in [#10,#13]) and (St.Position < St.Size) do
    begin
      Inc(L);
      St.ReadBuffer(C, SizeOf(C));
    end;
    Result:= ib_util_malloc(L+1);
    if (Result = nil) then Exit;
    St.Position:= P1;
    St.ReadBuffer(Result^, L+1);
    Result[L]:= #0;
  finally
    St.Free;
  end;
end;


function Bit_And(V1, V2: PULong{or long}): ULong; cdecl; export;
begin
  Result:= 0;
  if (V1 = nil) or (V2 = nil) then
    Exit;
  Result:= V1^ and V2^;
end;

function Bit_Or(V1, V2: PULong{or long}): ULong; cdecl; export;
begin
  Result:= 0;
  if (V1 = nil) or (V2 = nil) then
    Exit;
  Result:= V1^ or V2^;
end;

function Bit_Xor(V1, V2: PULong{or long}): ULong; cdecl; export;
begin
  Result:= 0;
  if (V1 = nil) or (V2 = nil) then
    Exit;
  Result:= V1^ xor V2^;
end;

function Bit_Not(V1: PULong{or long}): ULong; cdecl; export;
begin
  Result:= 0;
  if V1 = nil then
    Exit;
  Result:= not V1^;
end;

exports
  NowUTC,
  Cmp_Blobs,
  Integer_To_Blob,
  Double_To_Blob,
  TimeStamp_To_Blob,
  String_To_Blob,
  Blob_To_Integer,
  Blob_To_Double,
  Blob_To_TimeStamp,
  Blob_To_String,
  Blob_Length,
  String_Length,
  String_Copy,
  Concat_Blobs,
  Memo_Line_Count,
  Memo_Get_Line,

  Bit_And,
  Bit_Or,
  Bit_Not,
  Bit_Xor;

begin
  {$IFNDEF FPC}
  {$IFNDEF VER130}GetGDSLibrary.{$ENDIF}LoadIBLibrary;
  {$ENDIF}
end.
