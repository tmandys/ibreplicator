declare external function REPL$NOWUTC returns TimeStamp free_it entry_point "NowUTC" module_name "ib_Repl";
declare external function REPL$CMP_BLOBS blob,blob returns integer by value entry_point "Cmp_Blobs" module_name "ib_repl";

/*	Note: This string params are NOT limited to
 *	receiving and returning only 80 characters,
 *	rather, it can use as long as 32767
 * 	characters which is the limit on an
 *	INTERBASE character string.
 */

declare external function REPL$INTEGER_TO_BLOB integer,blob returns parameter 2 entry_point "Integer_To_Blob" module_name "ib_repl";
declare external function REPL$DOUBLE_TO_BLOB double precision,blob returns parameter 2 entry_point "Double_To_Blob" module_name "ib_repl";
declare external function REPL$TIMESTAMP_TO_BLOB timestamp,blob returns parameter 2 entry_point "TimeStamp_To_Blob" module_name "ib_repl";
declare external function REPL$STRING_TO_BLOB CSTRING(80),blob returns parameter 2 entry_point "String_To_Blob" module_name "ib_repl";
declare external function REPL$BLOB_TO_INTEGER blob returns integer by value entry_point "Blob_To_Integer" module_name "ib_repl";
declare external function REPL$BLOB_TO_DOUBLE blob returns double precision by value entry_point "Blob_To_Double" module_name "ib_repl";
declare external function REPL$BLOB_TO_TIMESTAMP blob returns TimeStamp free_it entry_point "Blob_To_TimeStamp" module_name "ib_repl";
declare external function REPL$BLOB_TO_STRING blob returns CSTRING(80) free_it entry_point "Blob_To_String" module_name "ib_repl";
declare external function REPL$BLOB_LENGTH blob returns integer by value entry_point "Blob_Length" module_name "ib_repl";
declare external function REPL$CONCAT_BLOBS blob,blob,blob returns parameter 3 entry_point "Concat_Blobs" module_name "ib_repl";
declare external function REPL$STRING_LENGTH CSTRING(80) returns integer by value entry_point "String_Length" module_name "ib_repl";
declare external function REPL$STRING_COPY CSTRING(80),integer,integer returns CSTRING(80) free_it entry_point "String_Copy" module_name "ib_repl";
declare external function REPL$MEMO_LINE_COUNT blob returns integer by value entry_point "Memo_Line_Count" module_name "ib_repl";
declare external function REPL$MEMO_GET_LINE blob,integer returns CSTRING(80) free_it entry_point "Memo_Get_Line" module_name "ib_repl";
declare external function REPL$BIT_AND integer,integer returns integer by value entry_point "Bit_And" module_name "ib_repl";
declare external function REPL$BIT_OR integer,integer returns integer by value entry_point "Bit_Or" module_name "ib_repl";
declare external function REPL$BIT_XOR integer,integer returns integer by value entry_point "Bit_Xor" module_name "ib_repl";
declare external function REPL$BIT_NOT integer returns integer by value entry_point "Bit_Not" module_name "ib_repl";

