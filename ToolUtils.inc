(* toolutils - Transfer and encoder library tools for IB Replicator
 * Copyright (C) 2003  Tomas Mandys-MandySoft
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA  02111-1307  USA
 *)

resourcestring
  sLastError = 'Error: "%s" Code: %.x'#13#10'%s';
  sTargetFileNotCreated = 'Target file has not been not created';
  sNullInstance = 'Null instance';

procedure AdjustPath(var S: string);
begin
  if (S <> '') and not (S[Length(S)] in ['\',':','/']) then
    S:= S+{$IFDEF LINUX}'/'{$ELSE}'\'{$ENDIF};
end;

function GetParString(Params: TStrings; const aPar, DefVal: string; aTgtSide: Integer = -1): string;
var
  S: string;
begin
  Result:= Params.Values[ToolName+'.'+aPar];
  if aTgtSide >= 0 then
  begin
    S:= Params.Values[ToolName+'.'+aPar+'.'+IntToStr(aTgtSide)];
    if S <> '' then
      Result:= S;
  end;
  if Result = '' then
    Result:= DefVal;
end;

function GetParInteger(Params: TStrings; const aPar: string; DefVal: Integer; aTgtSide: Integer = -1): Integer;
begin
  Result:= StrToIntDef(GetParString(Params, aPar, IntToStr(DefVal), aTgtSide), DefVal);
end;

function GetParBool(Params: TStrings; const aPar: string; DefVal: Boolean; aTgtSide: Integer = -1): Boolean;
begin
  Result:= StrToIntDef(GetParString(Params, aPar, IntToStr(Byte(DefVal)), aTgtSide), 0) <> 0;
end;

procedure ProjError(S: string);
begin
  raise Exception.Create(S);
end;

procedure CheckInstance(St: TStream);
begin
  if St = nil then
    ProjError(sNullInstance);
end;

procedure ProjLastError(const S, S2: string);
begin
  ProjError(Format(sLastError, [S, GetLastError, S2]));
end;


function GetTempDir(Params: TStrings): string;
{$IFNDEF LINUX}
var
  Buff: array[1..MAX_PATH] of Char;
{$ENDIF}
begin
  Result:= Params.Values[ToolName+'.tempdir'];
  if Result = '' then
  begin
    {$IFDEF LINUX}
    Result:= _PATH_TMP;
    {$ELSE}
    Windows.GetTempPath(SizeOf(Buff), @Buff);
    Result:= StrPas(@Buff);
    {$ENDIF}
  end;
  AdjustPath(Result);
end;

function GetTempFileName(SubType: string; Params: TStrings): string;
var
  TmpDir: string;
  {$IFNDEF LINUX}
  Buff: array[1..{$IFDEF LINUX}L_tmpnam{$ELSE}MAX_PATH{$ENDIF}] of Char;
  {$ENDIF}
begin
  TmpDir:= GetTempDir(Params);
  SubType:= SubType;
  {$IFDEF LINUX}
  Result:= StrPas(tempnam(PChar(TmpDir), 'ibrpl'));
  {$ELSE}
  if Windows.GetTempFileName(PChar(TmpDir), PChar(ToolName+SubType), 0, @Buff) = 0 then
    ProjLastError('CreateTempFileName', '');
  Result:= StrPas(@Buff){default extemsion overrided by TMP};
  {$ENDIF}
end;

function SaveToTempFile(const SubType: string; Src: TStream; Params: TStrings): string;
var
  St: TStream;
begin
  Result:= GetTempFileName(SubType, Params);
  St:= TFileStream.Create(Result, fmCreate);
  try
    St.CopyFrom(Src, Src.Size-Src.Position);
  finally
    St.Free;
  end;
end;

procedure LoadFromTempFile(FName: string; Tgt: TStream);
var
  St: TStream;
begin
  if not FileExists(FName) then
    ProjError(sTargetFileNotCreated);
  St:= TFileStream.Create(FName, fmOpenRead or fmShareDenyWrite);
  try
    if St.Size = 0 then
      ProjError(sTargetFileNotCreated);
    Tgt.CopyFrom(St, St.Size);
  finally
    St.Free;
  end;
end;


