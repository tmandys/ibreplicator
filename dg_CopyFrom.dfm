object CopyFromDlg: TCopyFromDlg
  Left = 227
  Top = 108
  HelpContext = 30300
  BorderStyle = bsDialog
  Caption = 'Copy from'
  ClientHeight = 140
  ClientWidth = 318
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 120
  TextHeight = 16
  object Label1: TLabel
    Left = 20
    Top = 20
    Width = 63
    Height = 16
    Caption = '&SchemaID'
    FocusControl = SchemaId
  end
  object Label2: TLabel
    Left = 118
    Top = 20
    Width = 50
    Height = 16
    Caption = '&GroupID'
    FocusControl = GroupId
  end
  object Label3: TLabel
    Left = 217
    Top = 20
    Width = 63
    Height = 16
    Caption = '&RelationID'
    FocusControl = RelationId
  end
  object OKBtn: TButton
    Left = 20
    Top = 89
    Width = 92
    Height = 30
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 3
  end
  object CancelBtn: TButton
    Left = 118
    Top = 89
    Width = 92
    Height = 30
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 4
  end
  object SchemaId: TEdit
    Left = 20
    Top = 39
    Width = 80
    Height = 24
    TabOrder = 0
  end
  object GroupId: TEdit
    Left = 118
    Top = 39
    Width = 80
    Height = 24
    TabOrder = 1
  end
  object RelationId: TEdit
    Left = 217
    Top = 39
    Width = 80
    Height = 24
    TabOrder = 2
  end
  object HelpBtn: TButton
    Left = 217
    Top = 89
    Width = 92
    Height = 30
    Caption = '&Help'
    TabOrder = 5
    OnClick = HelpBtnClick
  end
end
