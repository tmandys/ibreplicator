object SortDlg: TSortDlg
  Left = 363
  Top = 348
  HelpContext = 30400
  BorderStyle = bsDialog
  Caption = 'Order by'
  ClientHeight = 262
  ClientWidth = 296
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 120
  TextHeight = 16
  object OKBtn: TButton
    Left = 192
    Top = 7
    Width = 93
    Height = 31
    Anchors = [akTop, akRight]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
  object Button2: TButton
    Left = 192
    Top = 48
    Width = 93
    Height = 31
    Anchors = [akTop, akRight]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
  object ListBox: TListView
    Left = 8
    Top = 8
    Width = 176
    Height = 241
    Anchors = [akLeft, akTop, akRight, akBottom]
    Checkboxes = True
    Columns = <
      item
        Caption = 'Fields'
        Width = 150
      end>
    ColumnClick = False
    GridLines = True
    HideSelection = False
    ReadOnly = True
    RowSelect = True
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    ViewStyle = vsReport
    OnCompare = ListBoxCompare
    OnKeyDown = ListBoxKeyDown
  end
  object MoveUp: TButton
    Left = 192
    Top = 176
    Width = 93
    Height = 31
    Anchors = [akTop, akRight]
    Caption = 'Move up'
    TabOrder = 4
    OnClick = MoveUpClick
  end
  object MoveDown: TButton
    Left = 192
    Top = 215
    Width = 93
    Height = 31
    Anchors = [akTop, akRight]
    Caption = 'Move down'
    TabOrder = 5
    OnClick = MoveDownClick
  end
  object HelpBtn: TButton
    Left = 192
    Top = 88
    Width = 93
    Height = 30
    Anchors = [akTop, akRight]
    Caption = '&Help'
    TabOrder = 2
    OnClick = HelpBtnClick
  end
end
