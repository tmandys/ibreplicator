object SchemaFieldsConflictOptionsDialog: TSchemaFieldsConflictOptionsDialog
  Left = 374
  Top = 223
  HelpContext = 31620
  BorderStyle = bsDialog
  Caption = 'Field conflict options'
  ClientHeight = 457
  ClientWidth = 480
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object Label1: TLabel
    Left = 8
    Top = 64
    Width = 465
    Height = 80
    Caption = 
      'Field conflict occurs if target field value does not match old (' +
      'or new) source field value. It means probably that target record' +
      ' was changed since last replication or synchronization. It'#39's not' +
      ' valuable for primary key checking, they must match to find corr' +
      'esponding record. Extending conflict checking must be enabled in' +
      ' replication server.'
    WordWrap = True
  end
  object OKBtn: TButton
    Left = 69
    Top = 412
    Width = 92
    Height = 30
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 4
  end
  object CancelBtn: TButton
    Left = 197
    Top = 412
    Width = 92
    Height = 30
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 5
  end
  object HelpBtn: TButton
    Left = 325
    Top = 412
    Width = 92
    Height = 30
    Caption = '&Help'
    TabOrder = 6
    OnClick = HelpBtnClick
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 152
    Width = 465
    Height = 113
    Caption = 'UPDATE conflicts'
    TabOrder = 2
    object Options_3: TCheckBox
      Left = 10
      Top = 45
      Width = 444
      Height = 21
      Caption = 'leave target field value'
      TabOrder = 1
      OnClick = OptionsChange
    end
    object Options_4: TCheckBox
      Left = 10
      Top = 65
      Width = 444
      Height = 21
      Caption = 'do record update and report field conflict'
      TabOrder = 2
      OnClick = OptionsChange
    end
    object Options_5: TCheckBox
      Left = 10
      Top = 84
      Width = 444
      Height = 21
      Caption = 'do not record update and report conflict'
      TabOrder = 3
      OnClick = OptionsChange
    end
    object Options_2: TCheckBox
      Left = 10
      Top = 24
      Width = 444
      Height = 21
      Caption = 'update only when source field changed (for type 2 only)'
      TabOrder = 0
      OnClick = OptionsChange
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 272
    Width = 465
    Height = 49
    Caption = 'DELETE conflicts'
    TabOrder = 3
    object Options_8: TCheckBox
      Left = 10
      Top = 20
      Width = 444
      Height = 21
      Caption = 'do not delete record'
      TabOrder = 0
      OnClick = OptionsChange
    end
  end
  object Options_1: TCheckBox
    Left = 18
    Top = 37
    Width = 444
    Height = 21
    Caption = 'do not update field'
    TabOrder = 1
    OnClick = OptionsChange
  end
  object Options_0: TCheckBox
    Left = 18
    Top = 13
    Width = 444
    Height = 21
    Caption = 'do not assign for insert'
    TabOrder = 0
  end
  object GroupBox3: TGroupBox
    Left = 7
    Top = 328
    Width = 465
    Height = 57
    Caption = 'External file'
    TabOrder = 7
    object Options_9: TCheckBox
      Left = 10
      Top = 20
      Width = 444
      Height = 21
      Caption = 'field value is path and name to external file'
      TabOrder = 0
      OnClick = OptionsChange
    end
  end
end
