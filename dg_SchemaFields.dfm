inherited SchemaFieldsDialog: TSchemaFieldsDialog
  HelpContext = 31610
  Caption = 'Field properties'
  ClientHeight = 317
  ClientWidth = 354
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  inherited Panel1: TPanel
    Left = 246
    Height = 317
  end
  inherited Panel2: TPanel
    Width = 246
    Height = 317
    object Label1: TLabel
      Left = 20
      Top = 20
      Width = 32
      Height = 16
      Caption = '&Index'
      FocusControl = DBEdit1
    end
    object Label2: TLabel
      Left = 20
      Top = 79
      Width = 71
      Height = 16
      Caption = '&Source field'
      FocusControl = DBEdit2
    end
    object Label3: TLabel
      Left = 20
      Top = 138
      Width = 68
      Height = 16
      Caption = '&Target field'
      FocusControl = DBEdit3
    end
    object Label4: TLabel
      Left = 20
      Top = 197
      Width = 59
      Height = 16
      Caption = '&Field type'
      FocusControl = RxDBComboBox1
    end
    object Label7: TLabel
      Left = 18
      Top = 253
      Width = 90
      Height = 16
      Caption = '&Conflict options'
      FocusControl = DBEdit4
    end
    object DBEdit1: TDBEdit
      Left = 20
      Top = 39
      Width = 70
      Height = 24
      DataField = 'FIELDID'
      DataSource = DataSource1
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Left = 20
      Top = 98
      Width = 208
      Height = 24
      DataField = 'FIELDNAME'
      DataSource = DataSource1
      TabOrder = 1
    end
    object RxDBComboBox1: TRxDBComboBox
      Left = 20
      Top = 217
      Width = 208
      Height = 24
      Style = csDropDownList
      DataField = 'FIELDTYPE'
      DataSource = DataSource1
      EnableValues = True
      ItemHeight = 16
      Items.Strings = (
        '1..primary field'
        '2..foreign/required field'
        '3..common field')
      TabOrder = 3
      Values.Strings = (
        '1'
        '2'
        '3')
    end
    object DBEdit3: TDBEdit
      Left = 20
      Top = 158
      Width = 208
      Height = 24
      DataField = 'TARGETNAME'
      DataSource = DataSource1
      TabOrder = 2
    end
    object DBEdit4: TDBEdit
      Left = 18
      Top = 273
      Width = 111
      Height = 24
      DataField = 'OPTIONS'
      DataSource = DataSource1
      TabOrder = 4
      OnKeyDown = DBEdit4KeyDown
    end
    object Button1: TButton
      Left = 128
      Top = 275
      Width = 21
      Height = 23
      Caption = '...'
      TabOrder = 5
      TabStop = False
      OnClick = Button1Click
    end
  end
  inherited Table: TIBDataSet
    Left = 93
    Top = 16
    object TableSCHEMAID: TIntegerField
      FieldName = 'SCHEMAID'
      Required = True
    end
    object TableGROUPID: TIntegerField
      FieldName = 'GROUPID'
      Required = True
    end
    object TableRELATIONID: TIntegerField
      FieldName = 'RELATIONID'
      Required = True
    end
    object TableFIELDID: TIntegerField
      FieldName = 'FIELDID'
      Required = True
    end
    object TableFIELDNAME: TIBStringField
      FieldName = 'FIELDNAME'
      OnChange = TableFIELDNAMEChange
      Size = 100
    end
    object TableFIELDTYPE: TIntegerField
      FieldName = 'FIELDTYPE'
      Required = True
    end
    object TableTARGETNAME: TIBStringField
      FieldName = 'TARGETNAME'
      Size = 100
    end
    object TableOPTIONS: TIntegerField
      FieldName = 'OPTIONS'
      Required = True
    end
  end
  inherited DataSource1: TDataSource
    OnDataChange = DataSource1DataChange
    Left = 112
    Top = 32
  end
end
