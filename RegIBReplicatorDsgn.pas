(* RegIBReplicatorDsn - IBReplicator editors
 * Copyright (c) 2004 by Mandys Tomas-MandySoft

 Do not add this unit to any package distribution, delete it after compilation 
 *)

{ URL: http://www.2p.cz }

unit RegIBReplicatorDsgn;

interface
uses
  Classes, SysUtils
  {$IFDEF CLR}
    D8 not supported
    , Borland.Vcl.Design.DesignEditors
    , Borland.Vcl.Design.DesignIntf
  {$ELSE}
    {$IFDEF VER130}
    , Dsgnintf
    {$ELSE}
      {$IFDEF VER120}
    , Dsgnintf
      {$ELSE}
    , DesignIntf, DesignEditors
      {$ENDIF}
    {$ENDIF}
  {$ENDIF}
  ;

type
  TIBReplicatorComponentEditor = class(TDefaultEditor)
  public
    procedure ExecuteVerb(Index: Integer); override;
    function GetVerb(Index: Integer): string; override;
    function GetVerbCount: Integer; override;
  end;

procedure Register;

implementation

uses
  {$IFDEF VER120}FileCtrl, {$ENDIF}{$IFDEF VER130}FileCtrl, {$ENDIF}
  {$IFDEF LINUX}
    Libc,
  {$ELSE}
    Windows,
  {$ENDIF}
  {$IFDEF REGISTRATION}RegIBReplicatorDlg, {$ENDIF}IBReplicator;

{ TIBReplicatorComponentEditor }

procedure TIBReplicatorComponentEditor.ExecuteVerb(Index: Integer);
begin
  case Index of
    0:
      begin
        if csDesigning in Component.ComponentState then
        begin
          {$IFDEF REGISTRATION}
          IsLibrary:= True; // do not exit delphi IDE
          _CheckRegistration(True);
          {$ENDIF}
        end;
      end;
  end;
end;

function TIBReplicatorComponentEditor.GetVerb(Index: Integer): string;
resourcestring
  sRegister = 'Register...';
begin
  case Index of
    0: Result:= sRegister;
  end;
end;

function TIBReplicatorComponentEditor.GetVerbCount: Integer;
begin
  Result := {$IFDEF REGISTRATION}1{$ELSE}0{$ENDIF};
end;

procedure Register;
begin
  RegisterComponentEditor(TIBReplicator, TIBReplicatorComponentEditor);
end;

end.

