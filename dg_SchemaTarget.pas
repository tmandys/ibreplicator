(* IB Replication Suite
 * Copyright (C) 2002,2003 by Tomas Mandys-MandySoft
 *)

unit dg_SchemaTarget;

interface

uses
  {$IFDEF LINUX}
  Libc, QGraphics, QForms, QControls, QStdCtrls, QButtons, QExtCtrls, QMask, QDBCtrls, QRxDBComb, QDialogs,
  {$ELSE}
  Windows, Messages, Graphics, Forms, Controls, StdCtrls, Buttons, ExtCtrls, Mask, DBCtrls, RxDBComb, DBLookup, Dialogs,
  {$ENDIF}
  SysUtils, Classes, dg_SchemaTemplate, Db, IBCustomDataSet, IBQuery, IBSQL;

type
  TSchemaTargetDialog = class(TSchemaTemplateDialog)
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label10: TLabel;
    DBMemo2: TDBMemo;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    IBQuery1: TIBQuery;
    DataSource2: TDataSource;
    DBCheckBox3: TDBCheckBox;
    Label2: TLabel;
    DBLookupComboBox1: TDBLookupComboBox;
    Label3: TLabel;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Label8: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label9: TLabel;
    GroupId: TDBEdit;
    DBComboBox1: TDBComboBox;
    DBMemo1: TDBMemo;
    DBComboBox2: TDBComboBox;
    DBComboBox3: TDBComboBox;
    TableSCHEMAID: TIntegerField;
    TableGROUPID: TIntegerField;
    TableDBID: TIntegerField;
    TableDBMASK: TIntegerField;
    TableDISABLED: TIBStringField;
    TableREPLUSER: TIBStringField;
    TableREPLROLE: TIBStringField;
    TableREPLPSW: TIBStringField;
    TableSEPARATOR: TIntegerField;
    TableCOMMENT: TMemoField;
    TableOFFTRANSFER: TIBStringField;
    TableOFFPARAMS: TMemoField;
    TableOFFENCODER: TIBStringField;
    TableOFFDBID: TIntegerField;
    RxDBComboBox1: TRxDBComboBox;
    Button2: TButton;
    Label13: TLabel;
    DBEdit7: TDBEdit;
    Button1: TButton;
    procedure TableNewRecord(DataSet: TDataSet);
    procedure TableBeforeOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure IBQuery1BeforeOpen(DataSet: TDataSet);
    procedure TableBeforePost(DataSet: TDataSet);
    procedure TableDBMASKGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure TableDBMASKSetText(Sender: TField; const Text: String);
    procedure DBLookupComboBox2DropDown(Sender: TObject);
    procedure IBQuery1AfterOpen(DataSet: TDataSet);
    procedure Button2Click(Sender: TObject);
    procedure DataSource1DataChange(Sender: TObject; Field: TField);
    procedure TableAfterOpen(DataSet: TDataSet);
    procedure Button1Click(Sender: TObject);
    procedure DBLookupComboBox1DropDown(Sender: TObject);
  private
    ScrFlag: Byte;
    procedure OnSetTextPassword(Sender: TField; const Text: String);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SchemaTargetDialog: TSchemaTargetDialog;

implementation

uses dm_IBRepl, IBReplicator{$IFNDEF VER130}, Variants{$ENDIF};

{$IFDEF LINUX}
{$R *.xfm}
{$ELSE}
{$R *.dfm}
{$ENDIF}

resourcestring
  sMaskNotUnique = 'DBMASK is not unique in group';

procedure TSchemaTargetDialog.TableNewRecord(DataSet: TDataSet);
var
  Q: TIBSQL;
  I, M: Integer;
begin
  inherited;
  with DataSet do
  begin
    FieldByName('DBID').Value:= Null;
    FieldByName('DBMASK').asInteger:= 1;
    Q:= TIBSQL.Create(nil);
    try
      Q.Database:= Table.Database;
      Q.Transaction:= Table.Transaction;

      if not GroupId.ReadOnly then
      begin
        Q.SQL.Text:= Format('SELECT MAX(GROUPID) FROM %s WHERE SCHEMAID=%d', [TIBReplicator.FormatIdentifier(Q.Database.SQLDialect, IBReplDataModule.IBReplicator.ConfigDatabasePrefix+'SCHEMADB'), FieldByName('SCHEMAID').asInteger]);
        Q.ExecQuery;
        with FieldByName('GROUPID') do
          if Q.EOF then
            asInteger:= 1
          else
            asInteger:= Q.Fields[0].asInteger+1;
        Q.Close;
      end;
      Q.SQL.Text:= Format('SELECT DBMASK FROM %s WHERE SCHEMAID=%d AND GROUPID=%d', [TIBReplicator.FormatIdentifier(Q.Database.SQLDialect, IBReplDataModule.IBReplicator.ConfigDatabasePrefix+'SCHEMADB'), FieldByName('SCHEMAID').asInteger, FieldByName('GROUPID').asInteger]);
      Q.ExecQuery;
      M:= 0;
      while not Q.EOF do
      begin
        M:= M or Q.FieldByName('DBMASK').asInteger;
        Q.Next;
      end;

      for I:= 0 to 31 do
      begin
        if (1 shl I) and M = 0 then
        begin
          FieldByName('DBMASK').asInteger:= 1 shl I;
          Break;
        end;
      end;
    finally
      Q.Free;
    end;
    FieldByName('REPLUSER').asString:= 'REPL';
    FieldByName('SEPARATOR').asInteger:= 5;
    FieldByName('DISABLED').asString:= 'N';
  end;
end;

procedure TSchemaTargetDialog.TableBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  IBQuery1.Transaction:= Table.Transaction;
  IBQuery1.Open;
end;

procedure TSchemaTargetDialog.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  inherited;
  BatchReindexingForbidden:= True;
  for I:= 0 to 31 do
    DBComboBox3.Items.Add(Format('$%.8x', [1 shl I]));
end;

procedure TSchemaTargetDialog.IBQuery1BeforeOpen(DataSet: TDataSet);
begin
  inherited;
  with DataSet as TIBQuery do
    SQL[1]:= TIBReplicator.FormatIdentifier(Database.SQLDialect, IBReplDataModule.IBReplicator.ConfigDatabasePrefix+TrimRight(SQL[1]));
end;

procedure TSchemaTargetDialog.TableBeforePost(DataSet: TDataSet);
var
  Q: TIBSQL;
begin
  Q:= TIBSQL.Create(nil);
  try
    Q.Database:= Table.Database;
    Q.Transaction:= Table.Transaction;
    Q.SQL.Text:= Format('SELECT COUNT(*) FROM %s WHERE SCHEMAID=%d AND GROUPID=%d AND DBID<>%d AND DBMASK=%d', [TIBReplicator.FormatIdentifier(Q.Database.SQLDialect, IBReplDataModule.IBReplicator.ConfigDatabasePrefix+'SCHEMADB'), DataSet.FieldByName('SCHEMAID').asInteger, DataSet.FieldByName('GROUPID').asInteger, LastCountPrimaryValue, DataSet.FieldByName('DBMASK').asInteger]);
    Q.ExecQuery;
    if Q.Fields[0].asInteger > 0 then
      IBReplicatorError(sMaskNotUnique, 2);
  finally
    Q.Free;
  end;

  inherited;
end;

procedure TSchemaTargetDialog.TableDBMASKGetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  inherited;
  Text:= Format('$%.8x', [Sender.asInteger]);
end;

procedure TSchemaTargetDialog.TableDBMASKSetText(Sender: TField;
  const Text: String);
begin
  inherited;
  Sender.asInteger:= StrToInt(Text);
end;

procedure TSchemaTargetDialog.DBLookupComboBox2DropDown(Sender: TObject);
begin
  inherited;
  with sender as TDBLookupComboBox do
  begin
  end;
end;

procedure TSchemaTargetDialog.IBQuery1AfterOpen(DataSet: TDataSet);
resourcestring
  sNoDatabase = '<no database>';
begin
  inherited;
  with DataSet do
  begin
    First;
    RxDBComboBox1.Values.Clear;
    RxDBComboBox1.Items.Clear;
    RxDBComboBox1.Values.Add('');
    RxDBComboBox1.Items.Add(sNoDatabase);

    while not EOF do
    begin
      RxDBComboBox1.Values.Add(FieldByName('DBID').asString);
      RxDBComboBox1.Items.Add(FieldByName('NAME').asString);
      Next;
    end;
  end;
end;

procedure TSchemaTargetDialog.Button2Click(Sender: TObject);
begin
  Inc(ScrFlag);
  try
    Table.Edit;
    Table.FieldByName('REPLPSW').asString:= TIBReplicator.ScramblePassword(Table.FieldByName('REPLPSW').asString, True);
  finally
    Dec(ScrFlag);
  end;
end;

procedure TSchemaTargetDialog.DataSource1DataChange(Sender: TObject;
  Field: TField);
var
  Fl: Boolean;
begin
  inherited;
  Fl:= IBQuery1.Lookup('DBID', Table.FieldByName('DBID').asInteger, 'DBTYPE') = dbtInterbase;

// user/password must be defined for alternate database
  DBEdit7.Enabled:= Fl;
  RxDBComboBox1.Enabled:= not Fl;
  Button2.Enabled:= not TIBReplicator.IsScrambled(Table.FieldByName('REPLPSW').asString);
end;

procedure TSchemaTargetDialog.TableAfterOpen(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('REPLPSW').OnSetText:= OnSetTextPassword;
end;

procedure TSchemaTargetDialog.OnSetTextPassword(Sender: TField;
  const Text: String);
begin
  if (ScrFlag = 0) and TIBReplicator.IsScrambled(Text) then
    Sender.AsString:= ''
  else
    Sender.AsString:= Text;
end;

procedure TSchemaTargetDialog.Button1Click(Sender: TObject);
var
  I: Integer;
  S: string;
resourcestring
  sScrambleNoText = 'First select text to scramble. Part of line right to equation sign';
  sScrambleBadChar = 'Selected text contains unallowed char in interval [#0..#32]. Do not select text in multiple lines.';
  sScrambleConf = 'Encode text "%s" ?';
begin
  S:= DBMemo1.SelText;
  if S = '' then
    MessageDlg(sScrambleNoText, mtInformation, [mbOk], 0)
  else
    begin
      for I:= 1 to Length(S) do
        if S[I] < ' ' then
        begin
          MessageDlg(sScrambleBadChar, mtError, [mbOk], 0);
          Exit;
        end;
      if MessageDlg(Format(sScrambleConf, [S]), mtConfirmation, [mbYes, mbNo], 0) = mrYes then
      begin
        Table.Edit;
        DBMemo1.SelText:= TIBReplicator.ScramblePassword(S, True);
      end;
    end;
end;

procedure TSchemaTargetDialog.DBLookupComboBox1DropDown(Sender: TObject);
begin
  inherited;
  with IBQuery1 do
    if Tag = 0 then
    begin
      FetchAll;
      Tag:= 1;
    end;
end;

end.
