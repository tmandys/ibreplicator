(* IB Replication Suite
 * Copyright (C) 2002,2003 by Tomas Mandys-MandySoft
 *)

unit dg_SchemaDatabases;

interface

uses
  {$IFDEF LINUX}
  Libc, QGraphics, QForms, QControls, QStdCtrls, QButtons, QExtCtrls, QDialogs, QMask, QDBCtrls, QRxDBComb,
  {$ELSE}
  Windows, Messages, Graphics, Forms, Controls, StdCtrls, Buttons, ExtCtrls, Dialogs, Mask, DBCtrls, RxDBComb, 
  {$ENDIF}
  SysUtils, Classes, dg_SchemaTemplate, Db, IBCustomDataSet, IBQuery, IBSQL;


type
  TSchemaDatabasesDialog = class(TSchemaTemplateDialog)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    DBComboBox1: TDBComboBox;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    DBMemo2: TDBMemo;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBLookupComboBox1: TDBLookupComboBox;
    DBEdit7: TDBEdit;
    IBQuery1: TIBQuery;
    DataSource2: TDataSource;
    IBQuery1RDBCHARACTER_SET_NAME: TStringField;
    IBQuery1RDBCHARACTER_SET_NAME2: TStringField;
    Label11: TLabel;
    RxDBComboBox1: TRxDBComboBox;
    Label12: TLabel;
    DBMemo1: TDBMemo;
    Button2: TButton;
    Label13: TLabel;
    Label14: TLabel;
    DBEdit8: TDBEdit;
    procedure TableNewRecord(DataSet: TDataSet);
    procedure TableBeforeOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure TableBeforePost(DataSet: TDataSet);
    procedure IBQuery1CalcFields(DataSet: TDataSet);
    procedure DataSource1DataChange(Sender: TObject; Field: TField);
    procedure Button2Click(Sender: TObject);
    procedure TableAfterOpen(DataSet: TDataSet);
    procedure DBLookupComboBox1DropDown(Sender: TObject);
  private
    ScrFlag: Byte;
    procedure OnSetTextPassword(Sender: TField; const Text: String);
    procedure OnSetTextFileName(Sender: TField; const Text: string);
  public
    { Public declarations }
  end;

var
  SchemaDatabasesDialog: TSchemaDatabasesDialog;

implementation

uses dm_IBRepl, IBReplicator, IBServices;

{$IFDEF LINUX}
{$R *.xfm}
{$ELSE}
{$R *.dfm}
{$ENDIF}

resourcestring
  sCannotDeleteDatabase = 'Database is used in schema';

procedure TSchemaDatabasesDialog.TableNewRecord(DataSet: TDataSet);
begin
  inherited;
  with DataSet do
  begin
    FieldByName('SQLDIALECT').asInteger:= 3;
    FieldByName('DBTYPE').asInteger:= 0;
    FieldByName('CHARSET').asString:= 'NONE';
    FieldByName('ADMINUSER').asString:= 'SYSDBA';
    FieldByName('OBJPREFIX').asString:= 'REPL$';
  end;
end;

procedure TSchemaDatabasesDialog.TableBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  IBQuery1.Transaction:= Table.Transaction;
  IBQuery1.Open;
end;

procedure TSchemaDatabasesDialog.FormCreate(Sender: TObject);
begin
  inherited;
  BatchReindexingForbidden:= True;
end;

procedure TSchemaDatabasesDialog.TableBeforePost(DataSet: TDataSet);
var
  Q: TIBSQL;
begin
  if LastCountPrimaryValue <> DataSet.FieldByName('DBID').asInteger then
  begin
    Q:= TIBSQL.Create(nil);
    try
      Q.Database:= Table.Database;
      Q.Transaction:= Table.Transaction;
      Q.SQL.Text:= Format('SELECT COUNT(*) FROM %s WHERE DBID=%d', [TIBReplicator.FormatIdentifier(Q.Database.SQLDialect, IBReplDataModule.IBReplicator.ConfigDatabasePrefix+'SCHEMADB'), LastCountPrimaryValue]);
      Q.ExecQuery;
      if Q.Fields[0].asInteger > 0 then
        IBReplicatorError(sCannotDeleteDatabase, 2);
    finally
      Q.Free;
    end;
  end;
  inherited;
end;

procedure TSchemaDatabasesDialog.IBQuery1CalcFields(DataSet: TDataSet);
begin
  inherited;
  with DataSet do
    FieldByName('RDB$CHARACTER_SET_NAME2').asString:= Trim(FieldByName('RDB$CHARACTER_SET_NAME').asString);  // CAST na VARCHAR nestaci
end;

procedure TSchemaDatabasesDialog.DataSource1DataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  DBEdit4.Enabled:= Table.FieldByName('DBTYPE').asInteger = dbtInterbase;
  Label13.Visible:= DBEdit4.Enabled;
  DBEdit5.Enabled:= DBEdit4.Enabled;
  DBEdit6.Enabled:= DBEdit4.Enabled;
  DBMemo2.Enabled:= DBEdit4.Enabled;
  Button2.Enabled:= not TIBReplicator.IsScrambled(Table.FieldByName('ADMINPSW').asString) and DBEdit5.Enabled;
end;

procedure TSchemaDatabasesDialog.Button2Click(Sender: TObject);
begin
  Inc(ScrFlag);
  try
    Table.Edit;
    Table.FieldByName('ADMINPSW').asString:= TIBReplicator.ScramblePassword(Table.FieldByName('ADMINPSW').asString, True);
  finally
    Dec(ScrFlag);
  end;
end;

procedure TSchemaDatabasesDialog.TableAfterOpen(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('ADMINPSW').OnSetText:= OnSetTextPassword;
  DataSet.FieldByName('FILENAME').OnSetText:= OnSetTextFileName;
end;

procedure TSchemaDatabasesDialog.OnSetTextPassword(Sender: TField;
  const Text: String);
begin
  if (ScrFlag = 0) and TIBReplicator.IsScrambled(Text) then
    Sender.AsString:= ''
  else
    Sender.AsString:= Text;
end;

procedure TSchemaDatabasesDialog.OnSetTextFileName(Sender: TField; const Text: string);
var
  S1, S2: string;
resourcestring
  sDatabaseMultithreading = 'You have probably entered database name as Local server database. This is not compatible to multi-threading. Use "localhost:%s" prefix to enable it.';
begin
  Sender.asString:= Text;
  if (Sender.DataSet.FieldByName('DBTYPE').asInteger = dbtInterbase) and (TIBReplicator.GetDBProtocol(IBReplDataModule.IBReplicator.ParseStr(Text), S1, S2) = IBServices.Local) and (S2 <> '') then
  begin
    TIBReplicator.GetDBProtocol(Text, S1, S2);  // no macro expansion
    S1:= Format(sDatabaseMultithreading, [S2]);
    if MessageDlg(Format(sDatabaseMultithreading, [S2]), mtWarning, [mbYes, mbNo], 0) = mrYes then
      Sender.asString:= 'localhost:'+S2;
  end;
end;

procedure TSchemaDatabasesDialog.DBLookupComboBox1DropDown(
  Sender: TObject);
begin
  inherited;
  with IBQuery1 do
    if Tag = 0 then
    begin
      FetchAll;
      Tag:= 1;
    end;
end;

end.
