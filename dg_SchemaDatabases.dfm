inherited SchemaDatabasesDialog: TSchemaDatabasesDialog
  HelpContext = 31110
  Caption = 'Database properties'
  ClientHeight = 625
  ClientWidth = 489
  OnCreate = FormCreate
  PixelsPerInch = 120
  TextHeight = 16
  inherited Panel1: TPanel
    Left = 381
    Height = 625
  end
  inherited Panel2: TPanel
    Width = 381
    Height = 625
    object Label1: TLabel
      Left = 20
      Top = 20
      Width = 32
      Height = 16
      Caption = 'DB&ID'
      FocusControl = DBEdit1
    end
    object Label2: TLabel
      Left = 20
      Top = 71
      Width = 37
      Height = 16
      Caption = '&Name'
      FocusControl = DBEdit2
    end
    object Label3: TLabel
      Left = 20
      Top = 122
      Width = 59
      Height = 16
      Caption = '&File name'
      FocusControl = DBEdit3
    end
    object Label4: TLabel
      Left = 202
      Top = 221
      Width = 69
      Height = 16
      Caption = 'SQL &dialect'
      FocusControl = DBComboBox1
    end
    object Label5: TLabel
      Left = 20
      Top = 221
      Width = 67
      Height = 16
      Caption = 'Admin &user'
      FocusControl = DBEdit4
    end
    object Label6: TLabel
      Left = 20
      Top = 272
      Width = 100
      Height = 16
      Caption = 'Admin &password'
      FocusControl = DBEdit5
    end
    object Label7: TLabel
      Left = 20
      Top = 323
      Width = 64
      Height = 16
      Caption = 'Admin &role'
      FocusControl = DBEdit6
    end
    object Label8: TLabel
      Left = 198
      Top = 272
      Width = 49
      Height = 16
      Caption = '&Char set'
      FocusControl = DBLookupComboBox1
    end
    object Label9: TLabel
      Left = 198
      Top = 323
      Width = 74
      Height = 16
      Caption = 'Object prefi&x'
      FocusControl = DBEdit7
    end
    object Label10: TLabel
      Left = 20
      Top = 374
      Width = 80
      Height = 16
      Caption = 'Custom fi&elds'
      FocusControl = DBMemo2
    end
    object Label11: TLabel
      Left = 141
      Top = 20
      Width = 32
      Height = 16
      Caption = '&Type'
      FocusControl = RxDBComboBox1
    end
    object Label12: TLabel
      Left = 20
      Top = 486
      Width = 57
      Height = 16
      Caption = 'Co&mment'
      FocusControl = DBMemo1
    end
    object Label13: TLabel
      Left = 184
      Top = 123
      Width = 177
      Height = 16
      Alignment = taRightJustify
      Caption = 'Syntax:  "server:path/db.gdb"'
    end
    object Label14: TLabel
      Left = 20
      Top = 170
      Width = 97
      Height = 16
      Caption = 'E&xternal file path'
      FocusControl = DBEdit8
    end
    object DBEdit1: TDBEdit
      Left = 20
      Top = 39
      Width = 70
      Height = 24
      DataField = 'DBID'
      DataSource = DataSource1
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Left = 20
      Top = 90
      Width = 341
      Height = 24
      DataField = 'NAME'
      DataSource = DataSource1
      TabOrder = 2
    end
    object DBEdit3: TDBEdit
      Left = 20
      Top = 142
      Width = 341
      Height = 24
      DataField = 'FILENAME'
      DataSource = DataSource1
      TabOrder = 3
    end
    object DBComboBox1: TDBComboBox
      Left = 200
      Top = 241
      Width = 161
      Height = 24
      Style = csDropDownList
      DataField = 'SQLDIALECT'
      DataSource = DataSource1
      ItemHeight = 16
      Items.Strings = (
        '1'
        '2'
        '3')
      TabOrder = 9
    end
    object DBMemo2: TDBMemo
      Left = 20
      Top = 394
      Width = 349
      Height = 87
      DataField = 'CUSTOMFIELDS'
      DataSource = DataSource1
      ScrollBars = ssVertical
      TabOrder = 12
      WordWrap = False
    end
    object DBEdit4: TDBEdit
      Left = 20
      Top = 241
      Width = 165
      Height = 24
      DataField = 'ADMINUSER'
      DataSource = DataSource1
      TabOrder = 5
    end
    object DBEdit5: TDBEdit
      Left = 20
      Top = 292
      Width = 149
      Height = 24
      DataField = 'ADMINPSW'
      DataSource = DataSource1
      TabOrder = 6
    end
    object DBEdit6: TDBEdit
      Left = 20
      Top = 343
      Width = 165
      Height = 24
      DataField = 'ADMINROLE'
      DataSource = DataSource1
      TabOrder = 8
    end
    object DBLookupComboBox1: TDBLookupComboBox
      Left = 198
      Top = 292
      Width = 163
      Height = 24
      DataField = 'CHARSET'
      DataSource = DataSource1
      DropDownRows = 10
      KeyField = 'RDB$CHARACTER_SET_NAME2'
      ListField = 'RDB$CHARACTER_SET_NAME2'
      ListSource = DataSource2
      TabOrder = 10
      OnDropDown = DBLookupComboBox1DropDown
    end
    object DBEdit7: TDBEdit
      Left = 198
      Top = 343
      Width = 163
      Height = 24
      DataField = 'OBJPREFIX'
      DataSource = DataSource1
      TabOrder = 11
    end
    object RxDBComboBox1: TRxDBComboBox
      Left = 140
      Top = 39
      Width = 221
      Height = 24
      Style = csDropDownList
      DataField = 'DBTYPE'
      DataSource = DataSource1
      EnableValues = True
      ItemHeight = 16
      Items.Strings = (
        '0..database'
        '1..text file')
      TabOrder = 1
      Values.Strings = (
        '0'
        '1')
    end
    object DBMemo1: TDBMemo
      Left = 20
      Top = 506
      Width = 349
      Height = 109
      DataField = 'COMMENT'
      DataSource = DataSource1
      ScrollBars = ssVertical
      TabOrder = 13
    end
    object Button2: TButton
      Left = 171
      Top = 293
      Width = 17
      Height = 24
      Caption = '*'
      TabOrder = 7
      OnClick = Button2Click
    end
    object DBEdit8: TDBEdit
      Left = 20
      Top = 190
      Width = 341
      Height = 24
      DataField = 'EXTFILEPATH'
      DataSource = DataSource1
      TabOrder = 4
    end
  end
  inherited Table: TIBDataSet
    AfterOpen = TableAfterOpen
    SelectSQL.Strings = (
      'SELECT * FROM'
      'DATABASES'
      '')
    Left = 93
    Top = 16
  end
  inherited DataSource1: TDataSource
    OnDataChange = DataSource1DataChange
    Left = 112
    Top = 32
  end
  object IBQuery1: TIBQuery
    Database = IBReplDataModule.IBCfgDB
    Transaction = IBReplDataModule.IBCfgTransaction
    OnCalcFields = IBQuery1CalcFields
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      
        'SELECT CAST(RDB$CHARACTER_SET_NAME AS VARCHAR(20)) AS RDB$CHARAC' +
        'TER_SET_NAME'
      'FROM RDB$CHARACTER_SETS')
    Left = 168
    Top = 16
    object IBQuery1RDBCHARACTER_SET_NAME: TStringField
      FieldName = 'RDB$CHARACTER_SET_NAME'
    end
    object IBQuery1RDBCHARACTER_SET_NAME2: TStringField
      FieldKind = fkCalculated
      FieldName = 'RDB$CHARACTER_SET_NAME2'
      Calculated = True
    end
  end
  object DataSource2: TDataSource
    DataSet = IBQuery1
    Left = 184
    Top = 32
  end
end
