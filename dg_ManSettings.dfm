inherited ManSettingsDialog: TManSettingsDialog
  HelpContext = 32000
  PixelsPerInch = 120
  TextHeight = 16
  inherited PageControl1: TPageControl
    ActivePage = TabSheet4
    inherited TabSheet2: TTabSheet
      inherited Label12: TLabel
        Visible = False
      end
      inherited Label17: TLabel
        Visible = False
      end
      inherited Label11: TLabel
        Visible = False
      end
      inherited ShowOnSysTray: TCheckBox
        Visible = False
      end
      inherited OnlyOneInstance: TCheckBox
        Visible = False
      end
      inherited InstanceId: TEdit
        Visible = False
      end
      inherited RunMinimized: TCheckBox
        Visible = False
      end
      inherited MultiThreading: TCheckBox
        Visible = False
      end
      inherited MaxReplThreads: TEdit
        Visible = False
      end
    end
    inherited TabSheet3: TTabSheet
      inherited GroupBox1: TGroupBox
        Visible = False
      end
      inherited LogVerbose: TCheckBox
        Caption = 'Verbose log'
        Visible = True
      end
    end
    inherited TabSheet4: TTabSheet
      inherited Label13: TLabel
        Visible = False
      end
      object Label110: TLabel [1]
        Left = 8
        Top = 88
        Width = 89
        Height = 16
        Caption = 'Max.key length'
        FocusControl = KeyLength
      end
      object Label19: TLabel [2]
        Left = 8
        Top = 136
        Width = 249
        Height = 16
        Caption = 'Interbase/Firebird utility directory (gbak,...)'
        FocusControl = IBUtilPath
      end
      inherited OfflineDir: TEdit
        Visible = False
      end
      object KeyLength: TEdit
        Left = 8
        Top = 104
        Width = 108
        Height = 24
        TabOrder = 2
      end
      object IBUtilPath: TEdit
        Left = 8
        Top = 152
        Width = 385
        Height = 24
        TabOrder = 3
      end
    end
    inherited TabSheet5: TTabSheet
      TabVisible = False
      inherited Label5: TLabel
        Visible = False
      end
    end
  end
end
