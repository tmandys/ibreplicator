OK-lepsi logovani pripadnych chyb v IBREPLMAN
OK-vyzkouset jak se bude chovat na case sensitivy and space in name
OK-prefix v jmenech tabulek v konfiguracni databazi-umozni to slouceni databazi pri offline replikaci
NO-pomocna storova procedura, ktera by umoznila misto repl log pouzivat vlastni zjistovani zmenenych zaznamu (param, dt posledni replikace, schemaid,groupid,) dalsi procedura pro zruseni zaznamu (vymazani nebo presun do manualniho logu)
OK-REPL$ - moznost definovat prefix

OK -vylepsit offline synchronizaci - plug-in pro send/receive mailu ?, drop off box
OK -moznost replikovat z incrementalniho logu
-where vyber pro jednotlive databaze
OK -parametr proved replikaci a zkonci + provedeni vice replikaci - batch, exit code parametr
OK -repl$transfer schemaid,groupid,stamp,stampack,status(send/received),id_repllog - lastrepllogid.,stampsent,stamprec,id_potvrzeni_sent,id_potvrzeni_rec

CREATE TABLE "REPL$TRANSFER" (
  "TRANSFERID" NOT NULL,
  "STATUS" CHAR(1) NOT NULL, /* S..sent, R..received */

  "SCHEMAID" INTEGER NOT NULL,
  "GROUPID" INTEGER NOT NULL,
  "DBID" INTEGER NOT NULL,
  "STAMP" TIMESTAMP,
  "STAMP_SENT" TIMESTAMP,
  "STAMP_REC" TIMESTAMP,
  "TRANSFERID_ACK_SENT" INTEGER,
  "TRANSFERID_ACK_REC" INTEGER,
  "FLAG_ACK" CHAR(1),
  "RELCOUNT" INTEGER,
  "RECCOUNT" INTEGER,
  "PROCCOUNT" INTEGER,
  "LOGID_FROM" INTEGER,
  "LOGID_TO" INTEGER,
  PRIMARY KEY (SCHEMAID,TRANSFERID,STATUS)
);


replman
-ObjPrefix: t�mto prefixem budou prefixov�ny v�echny tabulky a gener�tory vytvo�en� v konfigura�n� datab�zi. Pomoc� r�zn�ch prefix� je tak mo�no
pou��t jednu interbase datab�zi-soubor pro v�ce konfigura�n�ch datab�z� nebo um�stit replikovanou a konfigura�n� datab�zi do jednoho souboru.
-logov�n� chyb v SQL v�etn� SQL p��kazu
-vylep�en� generov�n�/refreshov�n� n�zv� pol� a odtran�n� chyby p�i generov�n� replikovan�ch pol� (RDB$UPDATE_FLAG)
-SCHEMADB.OFFTRANSFER,SCHEMADB.OFFPARAMS,OFFDBID .. prefix umo��uje p�eps�n�m metody p�ej�t na jinou pouze p�eps�n�m n�zvu (parametry se kombinuj�)
-nepou��vat id v�ce ne� p�tim�stn� aby bylo spr�vn� sortov�n� offline bal��k�
-SEPARATOR pozor p�i p�evodu p�es unicode na jinou str�nku
-RELATION.CONDITION pomocn� podm�nka vyhodnocovan� v triggeru, VAR$xxxx parametry, VAR$COND > 0, mo�no declarovat vlastn� prom�nn� DECLARE VARIABLE
-parsovac� {#IF DELETE,!DELETE, ALL#} {#CXO#} {#CXN#}
-Drop triggers
-stored procedure: VAR$OPER, pokud se m� zapsat do manu�ln�ho lohu mo�no vyvolat EXCEPTION, p�i exportu do textov�ho filu se zapisuje podle pozice
ve FIELDS EXECUTE PROCEDURE xx (VAR$OPER, ...), nelze pou��t p�i synchronizaci, pozor: pokud obsahuje BLOB a je vol�na exception zm�n� se chyba
z isc_except na isc_bad_segstr_id - chyba v TIBSQL.ExecQuery, kdy isc_dsql_prepare zru�� BLOBy, exception je nutno volat d��ve ne� se pou�ije BLOBov� pole v procedu�e
NoTgtSelect nem� v tomto p��pad� v�znam
-FIELDS.UPDATEDISABLED umo��uje vypnout pole pro update

replserver
OBJPREFIX - parametr, kter� ur�uje prefix pro v�echny objekty vlo�en� replika�n�m managerem do replikovan� datab�ze. Default je "REPL$".
-mo�no nastavit v konfiguraci na jak� Interbase event m� server reagovat, defaultn� "REPL$REPLICATENOW"
-spu�t�n� s paramtrem /SYNC provede synchronizaci a ukon�� server
-spu�t�n� s parametrem /DISABLED nebude po startu prov�d�t replikace na timer nebo event
STAMP in REPL$LOG - NOW pozor na zm�nu �asu
-offline replication - to overcome different char sets there are strings encoded using UNICODE
-pozor: NOW x NOWGMT
-REPL parameter
-systray feature
-multiinstance
-encoders


-supported SQL dialect 1 and 3
-unsupported when Commit/RollbackRetaining not supported


Nov� verze  Professional
----------

-n obousm�rn� replikace (spou�t�n� z jednoho m�sta protism�rn� replikace), jak offlinov� - stamp v bal��ku kam a� m��e doj�t?
-x ibreplc instalace jako service (du�ln� interface)
-n konflikt manager
-x UDF na NOWUTC pro logov�n� do repllog  - UDF knihovna
-x vyhozen� eventu v ibrepl serveru, pokud se nepou��v� (mo�n� je, mo�n� nen�), kdo v�
-? replikace zm�n struktury
-x instal�tor sch�mat
- instal�tor replik�toru
-n replik�tor konfigurac�
-x typ filu ned�lat jen podle p��pony  |TXT, |GDB  umo�n� zm�nit p��ponu, nov� pole MODE
-x prohl�e� bal��k� -> package to xml
-x scheduler b��c� jako service (z�mek nap�. nereplikuj kdy� n�jak� file exist)
- db versioning
-n glob�ln� zapnut� UID pro relaci?
-x k�dov�n� string� v bal��ku v UTF-8 m�sto Unicode - aby se u�et�ilo m�sto
-x REPL$LOG_MEMO/BINARY do kter�ho se budou ukl�dat bindata
-x upgrade mo�nost
-x �ifrov�n� hesla
-x $(param) pro datab�zov� parametry + u�ivatele 1) spec. schemaid,groupid,dbid 2a)command line 2b) parametry serveru 3) parametry cfg 4) IBREPL_environment 5) environment
-x vytv��en� skeleton� procedur (nevytvo�� pr�va, mus� se ud�lat create system objects)
-x replman - pozn�mka kde bude d�vod chyby ADD_STRING_TO_BLOB
-x REPL$CONFIG field FLAG if <> 0 pak vypnuty eventy
-x d�vod chyby v repl man
-x custom fields to repl$ pro datab�zi / ;#/ koment��e, odd�lova� |, relation|field|fieldtype|dummy
-x conflict to XML, package to xml
-x rotace log�
-x podm�n�n� synchronizace - pro zasynchronizav�n� padl�ch datab�z�?

- vyhozena AuxStr z dm_IBRepl
- nowutc - nutno zm�nit recreate system objects (trigger LOG_SEQID)
- p�eps�no jako komponenta

-SCHEMATA added schematype
-DATABASES added DBTYPE
-config.disabled nove pole, repl$man.description nove pole
-x localhost u multithreadingu!!! Jeff Overcash IBX newsgroup
-zruseno SCHEMADB.SYNCDISABLED
-zvysena rychlost replikaci a zejmena synchronizace
-DecimalSeparator, ThousandSeparator jsou m�n�ny - pozor na multi-thread
-http://synedit.sourceforge.net
-podm�n�n� synchronizace (tag T pro identifikaci tabulky v podm�nce)
-maxruntime
-pou��vat c:/xxx proto�e je kompatabiln� s unixem a ne naopak
-cloning
-backup/restore
-rychlej�� synchronizace zalo�en� na set��d�n� dvou tabulek

rozd�l Pro x non-pro
--------------------
�ifrov�n� hesla
repl server s v�ce thready
conditional replication/synchronization ???
field options
replik�tor b��c� jako service a command line
versioning (star�� ne�??)
nezobrazov�n� reg.dialogu
clonning, backup, restore
zapnut� v�ce task� v serveru ???

11/2006
-------
- prepsat fCachedList (problem v linuxu - neradi to dobre, ted hack), load_replication schema
  do openarray struktur nebo do TList. Je to vice mene staticky, v ibreplman by se dala napsat
  malinka TxxTable (tam je treba update/delete/insert se zmenovym logem pri commitu). DONE 2.20
- lowecase nazbu programu, ted je neco mixed a v linuxu je to hnusny)
- daemon pro linux, komunikace pomoci socketu
- zobecneni databaze - zacit od replikacni databaze
- URL se vsemi parametry jako nazev db

3/2007
------
- test with Windows Vista
- rewrite help in doxygen style
- test with Interbase 2007
- .NET support
- XML2pkg
- Lazarus and linux better compatability
- performance problem reported by Stephan Vidouse, improved in 2.20

Seznam konkurent�
-----------------
http://www.ib-aid.com/links.html
http://www.ibphoenix.com/main.nfs?a=ibphoenix&s=1085138136:422717&page=ibp_replicator
http://www.meta.com.au/index.php?option=com_docman&Itemid=39&task=view_category&catid=15&order=dmdate_published&ascdesc=DESC

Server 120$, 10x 1000$  - kde b�� server
Replicant 100$/10x800$ - kam se replikuje
lite pro laptopy - $50/20x700$

