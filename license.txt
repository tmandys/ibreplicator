Copyright (c) 2001-2004 by Tomas Mandys-MandySoft

==== General

This license applies to everything in the "Interbase/Firebird Replication Suite"
package, except where otherwise noted.

This software is provided 'as-is', without any express or implied warranty.
In no event will the authors be held liable for any damages arising from the use
of this software.

Permission is granted to anyone to use this software for any purpose, including
commercial applications, and to alter it and redistribute it freely, subject to
the following restrictions:

1. The origin of this software must not be misrepresented; you must not claim
   that you wrote the original software.

2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

==== Executables

o  \*.exe,*.dll

There are some restrictions in some executables. First of all you must register
and activate software at each machine where is running - it means that registration
and activation is valid only for one machine. There are two levels
of activation - first level (Standard version) unlock major features, is processed 
online and it is for-free, second level (Professional version) that enables all 
features is commercial service.

==== SDK

o  \SDK\*.*

All files included in source form are distributed under GNU Library GPL,
see L-LESSERGPL.TXT. It means they are distributed as open source and
you can modify them as you need.

Files included only in compiled form - namely IBReplicator.* - are subject of
registration/activation and may be used only for software that is running
at computer where Delphi/Kylix/C Builder are running. To use for distributable
software based on the library you must buy commercial SDK deploy licence.

Note that you can buy full sources of the Suite. The sources not included
in free distribution you cannot pass to 3rd parties.
