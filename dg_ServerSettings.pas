(* IB Replication Suite
 * Copyright (C) 2002,2003 by Tomas Mandys-MandySoft
 *)

unit dg_ServerSettings;

interface

uses
  {$IFDEF LINUX}
  Libc, QGraphics, QControls, QForms, QDialogs, QStdCtrls, QComCtrls, QExtCtrls,
  {$ELSE}
  Windows, Messages, Graphics, Controls, Forms, Dialogs, StdCtrls, ComCtrls, ExtCtrls,
  {$ENDIF}
  SysUtils, Connect, Classes;

type
  TServerSettingsDialog = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    HelpBtn: TButton;
    Button1: TButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label10: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Database: TEdit;
    UserName: TEdit;
    Password: TEdit;
    ObjPrefix: TEdit;
    CharSet: TComboBox;
    Button2: TButton;
    TabSheet4: TTabSheet;
    Label14: TLabel;
    MaxLines: TEdit;
    ShowOnSysTray: TCheckBox;
    OnlyOneInstance: TCheckBox;
    Label12: TLabel;
    InstanceId: TEdit;
    RunMinimized: TCheckBox;
    NowAsUTC: TCheckBox;
    GroupBox1: TGroupBox;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    ReplLog: TEdit;
    ReplLogMaxSize: TEdit;
    ReplLogRotate: TEdit;
    GroupBox4: TGroupBox;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    DBLog: TEdit;
    DBLogMaxSize: TEdit;
    DBLogRotate: TEdit;
    LogErrSQLCmds: TCheckBox;
    LogErrSQLParams: TCheckBox;
    Label4: TLabel;
    Label13: TLabel;
    OfflineDir: TEdit;
    Label6: TLabel;
    MultiThreading: TCheckBox;
    Label17: TLabel;
    SQLDialect: TComboBox;
    TabSheet5: TTabSheet;
    DisableAll: TCheckBox;
    Label7: TLabel;
    Cron: TEdit;
    Label9: TLabel;
    FileLock: TEdit;
    Label5: TLabel;
    Label8: TLabel;
    TimerInterval: TEdit;
    OnEvent: TCheckBox;
    Event: TEdit;
    MaxReplRunTime: TEdit;
    LogVerbose: TCheckBox;
    IniFile: TEdit;
    Bevel1: TBevel;
    Label11: TLabel;
    MaxReplThreads: TEdit;
    Label18: TLabel;
    DBSysLog: TEdit;
    procedure FormShow(Sender: TObject);
    procedure OnlyOneInstanceClick(Sender: TObject);
    procedure HelpBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure PasswordChange(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure MultiThreadingClick(Sender: TObject);
    procedure DatabaseExit(Sender: TObject);
    procedure DatabaseChange(Sender: TObject);
  private
    ScrFlag: Byte;
    fDBChanged: Boolean;
    fLoaded: Boolean;
  protected
    procedure ReadFromIni; virtual;
    procedure WriteToIni; virtual;
  public
    Environment: TStrings;
    { Public declarations }
  end;

var
  ServerSettingsDialog: TServerSettingsDialog;

implementation
uses
  AuxProj, dm_IBRepl, dg_Environment, IBReplicator, IBServices;

{$IFDEF LINUX}
{$R *.xfm}
{$ELSE}
{$R *.dfm}
{$ENDIF}

procedure TServerSettingsDialog.FormShow(Sender: TObject);
begin
  ReadFromIni;
  fLoaded:= True;
  PageControl1.ActivePageIndex:= 0;
  OnlyOneInstanceClick(nil);
  Inc(ScrFlag, 2);
  try
    PasswordChange(nil);
  finally
    Dec(ScrFlag);
  end;
end;

procedure TServerSettingsDialog.OnlyOneInstanceClick(Sender: TObject);
begin
  InstanceId.Enabled:= OnlyOneInstance.Checked;
end;

procedure TServerSettingsDialog.HelpBtnClick(Sender: TObject);
begin
  Application.{$IFDEF LINUX}ContextHelp{$ELSE}HelpContext{$ENDIF}(HelpContext);
end;

procedure TServerSettingsDialog.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  Environment:= TStringList.Create;
  
  for I:= 0 to PageControl1.PageCount-1 do
    PageControl1.Pages[I].HelpContext:= HelpContext+1+I;
end;

procedure TServerSettingsDialog.FormDestroy(Sender: TObject);
begin
  Environment.Free;
end;

procedure TServerSettingsDialog.Button1Click(Sender: TObject);
resourcestring
  sCtxIni = 'INI';
begin
  with TEnvironmentDialog.Create(nil) do
  try
    Context:= sCtxIni;
    Keys.Lines.Assign(Environment);
    if ShowModal = mrOk then
      Environment.Assign(Keys.Lines);
  finally
    Release;
  end;
end;

procedure TServerSettingsDialog.PasswordChange(Sender: TObject);
begin
  Button2.Enabled:= not TIBReplicator.IsScrambled(Password.Text);
  if ScrFlag <> 1 then   // enable initialization before OnShow
    Exit;
  Inc(ScrFlag);
  try
    if (Password.Text <> '') and TIBReplicator.IsScrambled(Password.Text) then
      Password.Text:= '';
  finally
    Dec(ScrFlag);
  end;
end;

procedure TServerSettingsDialog.Button2Click(Sender: TObject);
begin
  Inc(ScrFlag);
  try
    Password.Text:= TIBReplicator.ScramblePassword(Password.Text, True);
  finally
    Dec(ScrFlag);
  end;
end;

procedure TServerSettingsDialog.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if ModalResult = mrOK then
  begin
    if fDBChanged then
      DatabaseExit(nil);
    WriteToIni;
  end;
end;

procedure TServerSettingsDialog.ReadFromIni;
begin
  IniFile.Text:= Ini.FileName;
  Database.Text:= Ini.ReadString(DBIniSection+'.ConfigDatabase', 'DatabaseName', '');
  UserName.Text:= Ini.ReadString(DBIniSection+'.ConfigDatabase', 'User', '');
  Password.Text:= Ini.ReadString(DBIniSection+'.ConfigDatabase', 'Password', '');
  CharSet.Text:= Ini.ReadString(DBIniSection+'.ConfigDatabase', 'CharSet', 'NONE');
  SQLDialect.Text:= IntToStr(Ini.ReadInteger(DBIniSection+'.ConfigDatabase', 'SQLDialect', 3));
  ObjPrefix.Text:= Ini.ReadString(DBIniSection+'.ConfigDatabase', 'ObjPrefix', '');

  Cron.Text:= Ini.ReadString(DBIniSection, 'Scheduler.Cron', '');
  FileLock.Text:= Ini.ReadString(DBIniSection, 'Scheduler.FileLock', '');
  TimerInterval.Text:= Ini.ReadString(DBIniSection, 'Scheduler.Interval', '');
  OnEvent.Checked:= Ini.ReadBool(DBIniSection, 'Scheduler.ReplOnEvent', False);
  Event.Text:= Ini.ReadString(DBIniSection, 'Scheduler.Event', 'REPL$REPLICATENOW');
  MaxReplRunTime.Text:= Ini.ReadString(DBIniSection, 'Scheduler.MaxRuntime', '');

  NowAsUTC.Checked:= Ini.ReadBool(DBIniSection, 'NowAsUTC', False);
  OfflineDir.Text:= Ini.ReadString(DBIniSection, 'OfflineDir', '');
  DisableAll.Checked:= Ini.ReadBool(DBIniSection, 'DisableAll', False);

  MultiThreading.Checked:= Ini.ReadBool(DBIniSection, 'MultiThreading', MultiThreading.Checked);
  MaxReplThreads.Text:= Ini.ReadString(DBIniSection, 'MaxReplThreads', '');

  ReplLog.Text:= Ini.ReadString(DBIniSection, 'ReplLog.FileName', devNull);
  ReplLogMaxSize.Text:= Ini.ReadString(DBIniSection, 'ReplLog.MaxSize', '');
  ReplLogRotate.Text:= Ini.ReadString(DBIniSection, 'ReplLog.RotateCount', '');
  DBLog.Text:= Ini.ReadString(DBIniSection, 'DBLog.FileName', devNull);
  DBLogMaxSize.Text:= Ini.ReadString(DBIniSection, 'DBLog.MaxSize', '');
  DBLogRotate.Text:= Ini.ReadString(DBIniSection, 'DBLog.RotateCount', '');
  DBSysLog.Text:= Ini.ReadString(DBIniSection, 'DBLog.SysLogName', '');
  LogErrSQLCmds.Checked:= Ini.ReadBool(DBIniSection, 'DBLog.ErrSQLCmds', False);
  LogErrSQLParams.Checked:= Ini.ReadBool(DBIniSection, 'DBLog.ErrSQLParams', False);
  LogVerbose.Checked:= Ini.ReadBool(DBIniSection, 'Log.Verbose', False);

  ShowOnSysTray.Checked:= Ini.ReadBool(DBIniSection+DBIniSection2, 'ShowOnSysTray', False);
  RunMinimized.Checked:= Ini.ReadBool(DBIniSection+DBIniSection2, 'RunMinimized', False);
  OnlyOneInstance.Checked:= Ini.ReadBool(DBIniSection+DBIniSection2, 'OnlyOneInstance', False);
  InstanceId.Text:= Ini.ReadString(DBIniSection+DBIniSection2, 'InstanceId', '20');
  MaxLines.Text:= Ini.ReadString(DBINISection+DBIniSection2, 'MaxLogLines', '');

  Ini.ReadSectionValues(DBIniSection+'.Environment', Environment);
  MultiThreadingClick(MultiThreading);
end;

procedure TServerSettingsDialog.WriteToIni;
var
  I: Integer;

  procedure IniWriteString(const aSection, aName: string; aEdit: TEdit); overload;
  begin
    if aEdit.Visible then
      Ini.WriteString(aSection, aName, aEdit.Text);
  end;
  procedure IniWriteString(const aSection, aName: string; aEdit: TComboBox); overload;
  begin
    if aEdit.Visible then
      Ini.WriteString(aSection, aName, aEdit.Text);
  end;
  procedure IniWriteBool(const aSection, aName: string; aEdit: TCheckBox);
  begin
    if aEdit.Visible then
      Ini.WriteBool(aSection, aName, aEdit.Checked);
  end;
begin
  IniWriteString(DBIniSection+'.ConfigDatabase', 'DatabaseName', Database);
  IniWriteString(DBIniSection+'.ConfigDatabase', 'User', UserName);
  IniWriteString(DBIniSection+'.ConfigDatabase', 'Password', Password);
  IniWriteString(DBIniSection+'.ConfigDatabase', 'CharSet', CharSet);
  IniWriteString(DBIniSection+'.ConfigDatabase', 'SQLDialect', SQLDialect);
  IniWriteString(DBIniSection+'.ConfigDatabase', 'ObjPrefix', ObjPrefix);

  IniWriteString(DBIniSection, 'Scheduler.Cron', Cron);
  IniWriteString(DBIniSection, 'Scheduler.FileLock', FileLock);
  IniWriteString(DBIniSection, 'Scheduler.Interval', TimerInterval);
  IniWriteBool(DBIniSection, 'Scheduler.ReplOnEvent', OnEvent);
  IniWriteString(DBIniSection, 'Scheduler.Event', Event);
  IniWriteString(DBIniSection, 'Scheduler.MaxRunTime', MaxReplRunTime);

  IniWriteBool(DBIniSection, 'NowAsUTC', NowAsUTC);
  IniWriteString(DBIniSection, 'OfflineDir', OfflineDir);
  IniWriteBool(DBIniSection, 'MultiThreading', MultiThreading);
  IniWriteString(DBIniSection, 'MaxReplThreads', MaxReplThreads);
  IniWriteBool(DBIniSection, 'DisableAll', DisableAll);

  IniWriteString(DBIniSection, 'ReplLog.FileName', ReplLog);
  IniWriteString(DBIniSection, 'ReplLog.MaxSize', ReplLogMaxSize);
  IniWriteString(DBIniSection, 'ReplLog.RotateCount', ReplLogRotate);
  IniWriteString(DBIniSection, 'DBLog.SysLogName', DBSysLog);
  IniWriteString(DBIniSection, 'DBLog.FileName', DBLog);
  IniWriteString(DBIniSection, 'DBLog.MaxSize', DBLogMaxSize);
  IniWriteString(DBIniSection, 'DBLog.RotateCount', DBLogRotate);
  IniWriteBool(DBIniSection, 'DBLog.ErrSQLCmds', LogErrSQLCmds);
  IniWriteBool(DBIniSection, 'DBLog.ErrSQLParams', LogErrSQLParams);
  Ini.WriteBool(DBIniSection, 'Log.Verbose', LogVerbose.Checked);

  IniWriteBool(DBIniSection+DBIniSection2, 'ShowOnSysTray', ShowOnSysTray);
  IniWriteBool(DBIniSection+DBIniSection2, 'RunMinimized', RunMinimized);
  IniWriteBool(DBIniSection+DBIniSection2, 'OnlyOneInstance', OnlyOneInstance);
  IniWriteString(DBIniSection+DBIniSection2, 'InstanceId', InstanceId);
  IniWriteString(DBINISection+DBIniSection2, 'MaxLogLines', MaxLines);

  Ini.EraseSection(DBIniSection+'.Environment');
  for I:= 0 to Environment.Count-1 do
    if Environment.Names[I] <> '' then
      Ini.WriteString(DBIniSection+'.Environment', Environment.Names[I], Environment.Values[Environment.Names[I]]);
  Ini.UpdateFile;
end;

procedure TServerSettingsDialog.MultiThreadingClick(Sender: TObject);
resourcestring
  sMultiThreadWarning = 'Interbase Express (IBX) causes machine crash when ANY local server database - database name without server prefix - is used in ANY non-VCL thread. This may cause data lost and a serious problem.'#13#10+
                        'Double check that none of used databases (configuration database, all source/target databases) are defined as local database.';
begin
  if TCheckBox(Sender).Checked and fLoaded then
  begin
    if MessageDlg(sMultiThreadWarning, mtWarning, [mbOk, mbCancel], 0) <> mrOK then
    begin
      TCheckBox(Sender).Checked:= False;
    end;
  end;
  MaxReplThreads.Enabled:= TCheckBox(Sender).Checked;
end;

procedure TServerSettingsDialog.DatabaseExit(Sender: TObject);
var
  S1, S2: string;
resourcestring
  sDatabaseMultithreading = 'You have probably entered configuration database as Local server database. This is not compatible to multi-threading. Use "localhost:%s" prefix to enable it. ';
  sDatabaseMultithreading2 = 'Multi-threading checkbox has been unchecked.';
begin
  if fDBChanged then
  begin
    if (TIBReplicator.GetDBProtocol(IBReplDataModule.IBReplicator.ParseStr(Database.Text), S1, S2) = IBServices.Local) and (S2 <> '') then
    begin
      TIBReplicator.GetDBProtocol(Database.Text, S1, S2);  // no macro expansion
      S1:= Format(sDatabaseMultithreading, [S2]);
      if MultiThreading.Visible and MultiThreading.Checked then
      begin
        S1:= S1+sDatabaseMultithreading2;
        MultiThreading.Checked:= False;
      end;
      if MessageDlg(S1, mtWarning, [mbYes, mbNo], 0) = mrYes then
        Database.Text:= 'localhost:'+S2;
    end;
    fDBChanged:= False;
  end;
end;

procedure TServerSettingsDialog.DatabaseChange(Sender: TObject);
begin
  if fLoaded then
    fDBChanged:= True;
end;

end.
