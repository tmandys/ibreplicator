(* IBDataSet2 - TDataSet auxiliary library
 * Copyright (C) 1999-2003  Tomas Mandys-MandySoft
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA  02111-1307  USA
 *)

{ URL: http://www.2p.cz }

unit IBDataSet2;

{ IBDataSet2.htx }

interface
uses
  SysUtils, Classes, IBDatabase, Db, IBSQL, IBCustomDataSet, IB, IBHeader, IBExternals{$IFNDEF LINUX}, Windows{$ENDIF};

type
  TIBDataSet2 = class(TIBDataSet)
  public
    procedure PrepareBlobs;  // obsolete
    property QSelect;
    property QDelete;
    property QModify;
    property QInsert;
    procedure SetFieldAsVariant(Field: TField; const Val: Variant);
  protected
    procedure InternalInitFieldDefs; override;
  end;

procedure Register;

implementation
{$IFDEF LINUX}
uses
  Types, Variants;
{$ELSE}

{$IFDEF VER140}
uses
  Variants;
{$ENDIF}

{$IFDEF VER150}
uses
  Variants;
{$ENDIF}

{$IFDEF VER170}
uses
  Variants;
{$ENDIF}

{$IFDEF VER180}
uses
  Variants;
{$ENDIF}
{$ENDIF}

type
  TAuxIBCustomDataSet = class({$IFDEF VER180}TWideDataSet{$ELSE}TDataset{$ENDIF})
  private
  {$HINTS OFF}
  {$IFDEF LINUX}
    {$IFDEF VER140}
    FNeedsRefresh: Boolean;
    FForcedRefresh: Boolean;
    FIBLoaded: Boolean;
    FBase: TIBBase;
    FBlobCacheOffset: Integer;
    FBlobStreamList: TList;
    FBufferChunks: Integer;
    FBufferCache,
    FOldBufferCache: PChar;
    FBufferChunkSize,
    FCacheSize,
    FOldCacheSize: Integer;
    FFilterBuffer: PChar;
    FBPos,
    FOBPos,
    FBEnd,
    FOBEnd: DWord;
    FCachedUpdates: Boolean;
    FCalcFieldsOffset: Integer;
    FCurrentRecord: Long;
    FDeletedRecords: Long;
    FModelBuffer,
    FOldBuffer, FTempBuffer: PChar;
    FOpen: Boolean;
    FInternalPrepared: Boolean;
    FQDelete,
    FQInsert,
    FQRefresh,
    FQSelect,
    FQModify: TIBSQL;
    FRecordBufferSize: Integer;
    FRecordCount: Integer;
    FRecordSize: Integer;
    FUniDirectional: Boolean;
    FUpdateMode: TUpdateMode;
    FUpdateObject: TIBDataSetUpdateObject;
    FParamCheck: Boolean;
    FUpdatesPending: Boolean;
    FUpdateRecordTypes: TIBUpdateRecordTypes;
    FMappedFieldPosition: array of Integer;
    {$ENDIF}
  {$ELSE}
    {$IFDEF VER130}
    FNeedsRefresh: Boolean;
    FForcedRefresh: Boolean;
    FIBLoaded: Boolean;
    FBase: TIBBase;
    FBlobCacheOffset: Integer;
    FBlobStreamList: TList;
    FBufferChunks: Integer;
    FBufferCache,
    FOldBufferCache: PChar;
    FBufferChunkSize,
    FCacheSize,
    FOldCacheSize: Integer;
    FFilterBuffer: PChar;
    FBPos,
    FOBPos,
    FBEnd,
    FOBEnd: DWord;
    FCachedUpdates: Boolean;
    FCalcFieldsOffset: Integer;
    FCurrentRecord: Long;
    FDeletedRecords: Long;
    FModelBuffer,
    FOldBuffer: PChar;
    FOpen: Boolean;
    FInternalPrepared: Boolean;
    FQDelete,
    FQInsert,
    FQRefresh,
    FQSelect,
    FQModify: TIBSQL;
    FRecordBufferSize: Integer;
    FRecordCount: Integer;
    FRecordSize: Integer;
    FUniDirectional: Boolean;
    FUpdateMode: TUpdateMode;
    FUpdateObject: TIBDataSetUpdateObject;
    FParamCheck: Boolean;
    FUpdatesPending: Boolean;
    FUpdateRecordTypes: TIBUpdateRecordTypes;
    FMappedFieldPosition: array of Integer;  // I need this private field
    {$ELSE}
      {$IFDEF VER140}
    FNeedsRefresh: Boolean;
    FForcedRefresh: Boolean;
    FIBLoaded: Boolean;
    FBase: TIBBase;
    FBlobCacheOffset: Integer;
    FBlobStreamList: TList;
    FBufferChunks: Integer;
    FBufferCache,
    FOldBufferCache: PChar;
    FBufferChunkSize,
    FCacheSize,
    FOldCacheSize: Integer;
    FFilterBuffer: PChar;
    FBPos,
    FOBPos,
    FBEnd,
    FOBEnd: DWord;
    FCachedUpdates: Boolean;
    FCalcFieldsOffset: Integer;
    FCurrentRecord: Long;
    FDeletedRecords: Long;
    FModelBuffer,
    FOldBuffer, FTempBuffer: PChar;
    FOpen: Boolean;
    FInternalPrepared: Boolean;
    FQDelete,
    FQInsert,
    FQRefresh,
    FQSelect,
    FQModify: TIBSQL;
    FRecordBufferSize: Integer;
    FRecordCount: Integer;
    FRecordSize: Integer;
    FUniDirectional: Boolean;
    FUpdateMode: TUpdateMode;
    FUpdateObject: TIBDataSetUpdateObject;
    FParamCheck: Boolean;
    FUpdatesPending: Boolean;
    FUpdateRecordTypes: TIBUpdateRecordTypes;
    FMappedFieldPosition: array of Integer;
      {$ELSE}
        {$IFDEF VER150}
    FNeedsRefresh: Boolean;
    FForcedRefresh: Boolean;
    FIBLoaded: Boolean;
    FBase: TIBBase;
    FBlobCacheOffset: Integer;
    FBlobStreamList: TList;
    FBufferChunks: Integer;
    FBufferCache,
    FOldBufferCache: PChar;
    FBufferChunkSize,
    FCacheSize,
    FOldCacheSize: Integer;
    FFilterBuffer: PChar;
    FBPos,
    FOBPos,
    FBEnd,
    FOBEnd: DWord;
    FCachedUpdates: Boolean;
    FCalcFieldsOffset: Integer;
    FCurrentRecord: Long;
    FDeletedRecords: Long;
    FModelBuffer,
    FOldBuffer, FTempBuffer: PChar;
    FOpen: Boolean;
    FInternalPrepared: Boolean;
    FQDelete,
    FQInsert,
    FQRefresh,
    FQSelect,
    FQModify: TIBSQL;
    FRecordBufferSize: Integer;
    FRecordCount: Integer;
    FRecordSize: Integer;
    FUniDirectional: Boolean;
    FUpdateMode: TUpdateMode;
    FUpdateObject: TIBDataSetUpdateObject;
    FParamCheck: Boolean;
    FUpdatesPending: Boolean;
    FUpdateRecordTypes: TIBUpdateRecordTypes;
    FMappedFieldPosition: array of Integer;
        {$ELSE}
           {$IFDEF VER170}
    FNeedsRefresh: Boolean;
    FForcedRefresh: Boolean;
    FIBLoaded: Boolean;
    FBase: TIBBase;
    FBlobCacheOffset: Integer;
    FBlobStreamList: TList;
    FBufferChunks: Integer;
    FBufferCache,
    FOldBufferCache: PChar;
    FBufferChunkSize,
    FCacheSize,
    FOldCacheSize: Integer;
    FFilterBuffer: PChar;
    FBPos,
    FOBPos,
    FBEnd,
    FOBEnd: DWord;
    FCachedUpdates: Boolean;
    FCalcFieldsOffset: Integer;
    FCurrentRecord: Long;
    FDeletedRecords: Long;
    FModelBuffer,
    FOldBuffer, FTempBuffer: PChar;
    FOpen: Boolean;
    FInternalPrepared: Boolean;
    FQDelete,
    FQInsert,
    FQRefresh,
    FQSelect,
    FQModify: TIBSQL;
    FRecordBufferSize: Integer;
    FRecordCount: Integer;
    FRecordSize: Integer;
    FUniDirectional: Boolean;
    FUpdateMode: TUpdateMode;
    FUpdateObject: TIBDataSetUpdateObject;
    FParamCheck: Boolean;
    FUpdatesPending: Boolean;
    FUpdateRecordTypes: TIBUpdateRecordTypes;
    FMappedFieldPosition: array of Integer;
           {$ELSE}
             {$IFDEF VER180}
    FNeedsRefresh: Boolean;
    FForcedRefresh: Boolean;
    FIBLoaded: Boolean;
    FBase: TIBBase;
    FBlobCacheOffset: Integer;
    FBlobStreamList: TList;
    FBufferChunks: Integer;
    FBufferCache,
    FOldBufferCache: PChar;
    FBufferChunkSize,
    FCacheSize,
    FOldCacheSize: Integer;
    FFilterBuffer: PChar;
    FBPos,
    FOBPos,
    FBEnd,
    FOBEnd: DWord;
    FCachedUpdates: Boolean;
    FCalcFieldsOffset: Integer;
    FCurrentRecord: Long;
    FDeletedRecords: Long;
    FModelBuffer,
    FOldBuffer, FTempBuffer: PChar;
    FOpen: Boolean;
    FInternalPrepared: Boolean;
    FQDelete,
    FQInsert,
    FQRefresh,
    FQSelect,
    FQModify: TIBSQL;
    FRecordBufferSize: Integer;
    FRecordCount: Integer;
    FRecordSize: Integer;
    FUniDirectional: Boolean;
    FUpdateMode: TUpdateMode;
    FUpdateObject: TIBDataSetUpdateObject;
    FParamCheck: Boolean;
    FUpdatesPending: Boolean;
    FUpdateRecordTypes: TIBUpdateRecordTypes;
    FMappedFieldPosition: array of Integer;
             {$ELSE}
    WARNING Copy appropriate private section of TIBCustomDataset from VCL source IBCustomDataSet
             {$ENDIF}
           {$ENDIF}
        {$ENDIF}
      {$ENDIF}
    {$ENDIF}
  {$ENDIF}
  end;
{$HINTS ON}

{$IFDEF LINUX}
procedure TIBDataSet2.InternalInitFieldDefs;
var
  FieldType: TFieldType;
  FieldSize: Word;
  FieldNullable : Boolean;
  i, FieldPosition, FieldPrecision, FieldIndex: Integer;
  FieldAliasName: string;
  RelationName, FieldName: string;

begin
  if not InternalPrepared then
  begin
    InternalPrepare;
    exit;
  end;
  TAuxIBCustomDataSet(Self).FNeedsRefresh := False;
  try
    FieldDefs.BeginUpdate;
    FieldDefs.Clear;
    FieldIndex := 0;
    if (Length(TAuxIBCustomDataSet(Self).FMappedFieldPosition) < TAuxIBCustomDataSet(Self).FQSelect.Current.Count) then
      SetLength(TAuxIBCustomDataSet(Self).FMappedFieldPosition, TAuxIBCustomDataSet(Self).FQSelect.Current.Count);
    for i := 0 to TAuxIBCustomDataSet(Self).FQSelect.Current.Count - 1 do
      with TAuxIBCustomDataSet(Self).FQSelect.Current[i].Data do
      begin
        { Get the field name }
        FieldAliasName := aliasname;
        RelationName := relname;
        FieldName := sqlname;
        FieldSize := 0;
        FieldPrecision := 0;
        FieldNullable := TAuxIBCustomDataSet(Self).FQSelect.Current[i].IsNullable;
        case sqltype and not 1 of
          { All VARCHAR's must be converted to strings before recording
           their values }
          SQL_VARYING, SQL_TEXT:
          begin
            FieldSize := sqllen;
            FieldType := ftString;
          end;
          { All Doubles/Floats should be cast to doubles }
          SQL_DOUBLE, SQL_FLOAT:
            FieldType := ftFloat;
          SQL_SHORT:
          begin
            if (sqlscale = 0) then
              FieldType := ftSmallInt
            else
            begin
              FieldType := ftBCD;
              FieldPrecision := 4;
              FieldSize := -sqlscale;
            end;
          end;
          SQL_LONG:
          begin
            if (sqlscale = 0) then
              FieldType := ftInteger
            else
              if (sqlscale >= (-4)) then
              begin
                FieldType := ftBCD;
                FieldPrecision := 9;
                FieldSize := -sqlscale;
              end
              else
                FieldType := ftFloat
              end;
          SQL_INT64:
          begin
            if (sqlscale = 0) then
              FieldType := ftLargeInt
            else
              if (sqlscale >= (-4)) then
              begin
                FieldType := ftBCD;
                FieldPrecision := 18;
                FieldSize := -sqlscale;
              end
              else
                FieldType := ftFloat
              end;
          SQL_TIMESTAMP: FieldType := ftDateTime;
          SQL_TYPE_TIME: FieldType := ftTime;
          SQL_TYPE_DATE: FieldType := ftDate;
          SQL_BLOB:
          begin
            FieldSize := sizeof (TISC_QUAD);
            if (sqlsubtype = 1) then
              FieldType := ftmemo
            else
              FieldType := ftBlob;
          end;
          SQL_ARRAY:
          begin
            FieldSize := sizeof (TISC_QUAD);
            FieldType := ftUnknown;
          end;
          SQL_BOOLEAN:
            FieldType := ftBoolean;
          else
            FieldType := ftUnknown;
        end;
        FieldPosition := i + 1;
        if (FieldType <> ftUnknown) and (FieldAliasName <> 'IBX_INTERNAL_DBKEY') then {do not localize}
        begin
          TAuxIBCustomDataSet(Self).FMappedFieldPosition[FieldIndex] := FieldPosition;
          Inc(FieldIndex);
          with FieldDefs.AddFieldDef do
          begin
            Name := FieldAliasName;
            FieldNo := FieldPosition;
            DataType := FieldType;
            Size := FieldSize;
            Precision := FieldPrecision;
            Required := not FieldNullable;
            InternalCalcField := False;
            if ((SQLType and not 1) = SQL_TEXT) then
              Attributes := Attributes + [faFixed];
          end;
        end;
      end;
  finally
    FieldDefs.EndUpdate;
  end;
end;
{$ELSE}
procedure TIBDataSet2.InternalInitFieldDefs;  // original component calls prepare, execute and fetch for each field and it decreases performance
var
  FieldType: TFieldType;
  FieldSize: Word;
  i, FieldPosition, FieldPrecision: Integer;
  FieldAliasName: string;
  RelationName, FieldName: string;
  FieldIndex: Integer;

begin
  if not InternalPrepared then
  begin
    InternalPrepare;
    exit;
  end;
  FieldDefs.BeginUpdate;
  try
    FieldDefs.Clear;
    FieldIndex := 0;              
    if (Length(TAuxIBCustomDataSet(Self).FMappedFieldPosition) < QSelect.Current.Count) then
      SetLength(TAuxIBCustomDataSet(Self).FMappedFieldPosition, QSelect.Current.Count);
    for i := 0 to QSelect.Current.Count - 1 do
      with QSelect.Current[i].Data{$IFDEF VER130}^{$ENDIF} do
      begin
        { Get the field name }
        {$IFDEF VER130}
        SetString(FieldAliasName, aliasname, aliasname_length);
        SetString(RelationName, relname, relname_length);
        SetString(FieldName, sqlname, sqlname_length);
        {$ELSE}
        FieldAliasName := aliasname;
        RelationName := relname;
        FieldName := sqlname;
        {$ENDIF}
        FieldSize := 0;
        FieldPrecision := 0;
        case sqltype and not 1 of
          { All VARCHAR's must be converted to strings before recording
           their values }
          SQL_VARYING, SQL_TEXT:
          begin
            FieldSize := sqllen;
            FieldType := ftString;
          end;
          { All Doubles/Floats should be cast to doubles }
          SQL_DOUBLE, SQL_FLOAT:
            FieldType := ftFloat;
          SQL_SHORT:
          begin
            if (sqlscale = 0) then
              FieldType := ftSmallInt
            else begin
              FieldType := ftBCD;
              FieldPrecision := 4;
            end;
          end;
          SQL_LONG:
          begin
            if (sqlscale = 0) then
              FieldType := ftInteger
            else if (sqlscale >= (-4)) then
            begin
              FieldType := ftBCD;
              FieldPrecision := 9;
            end
            else
              FieldType := ftFloat;
            end;
          SQL_INT64:
          begin
            if (sqlscale = 0) then
              FieldType := ftLargeInt
            else if (sqlscale >= (-4)) then
            begin
              FieldType := ftBCD;
              FieldPrecision := 18;
            end
            else
              FieldType := ftFloat;
            end;
          SQL_TIMESTAMP: FieldType := ftDateTime;
          SQL_TYPE_TIME: FieldType := ftTime;
          SQL_TYPE_DATE: FieldType := ftDate;
          SQL_BLOB:
          begin
            FieldSize := sizeof (TISC_QUAD);
            if (sqlsubtype = 1) then
              FieldType := ftmemo
            else
              FieldType := ftBlob;
          end;
          SQL_ARRAY:
          begin
            FieldSize := sizeof (TISC_QUAD);
            FieldType := ftUnknown;
          end;
          else
            FieldType := ftUnknown;
        end;
        FieldPosition := i + 1;
        if (FieldType <> ftUnknown) and (FieldAliasName <> 'IBX_INTERNAL_DBKEY') then {do not localize}
        begin
          TAuxIBCustomDataSet(Self).FMappedFieldPosition[FieldIndex] := FieldPosition;
          Inc(FieldIndex);
          with FieldDefs.AddFieldDef do
          begin
            Name := string( FieldAliasName );
            FieldNo := FieldPosition;
            DataType := FieldType;
            Size := FieldSize;
            Precision := FieldPrecision;
            Required := False;
            InternalCalcField := False;
          end;
        end;
      end;
  finally
    FieldDefs.EndUpdate;
  end;
end;
{$ENDIF}

procedure TIBDataSet2.SetFieldAsVariant(Field: TField; const Val: Variant);
var
  Buff: PChar;
begin
  Field.AsVariant:= Val;
  if Field is TIBStringField then    // fix TIBCustomDataSet.InternalSetFieldData, empty string is changed to Null
  begin
    if Field.IsNull and not VarIsNull(Val) and (Field.FieldNo >= 0) then
    begin
      Buff:= GetActiveBuf;
      PRecordData(Buff)^.rdFields[TAuxIBCustomDataSet(Field.DataSet).FMappedFieldPosition[Field.FieldNo - 1]].fdIsNull:= False;
    end;
  end;
end;

procedure TIBDataSet2.PrepareBlobs;
var
  i, j, k: Integer;
  pbd: PBlobDataArray;
  Buff: Pointer;
begin
  Buff:= GetActiveBuf;
  pbd := PBlobDataArray(PChar(Buff) + TAuxIBCustomDataSet(Self).FBlobCacheOffset);
  j := 0;
  for i := 0 to FieldCount - 1 do
    if Fields[i].IsBlob then
    begin
      Fields[i].asString; // read blob resource
      k := TAuxIBCustomDataSet(Self).FMappedFieldPosition[Fields[i].FieldNo -1];
      if pbd^[j] <> nil then
      begin
        pbd^[j].Finalize;
        PISC_QUAD(
          PChar(Buff) + PRecordData(Buff)^.rdFields[k].fdDataOfs)^ :=
          pbd^[j].BlobID;
        PRecordData(Buff)^.rdFields[k].fdIsNull := pbd^[j].Size = 0;
      end;
      Inc(j);
    end;
end;

procedure Register;
begin
  RegisterComponents('Interbase', [TIBDataSet2]);
end;

end.
