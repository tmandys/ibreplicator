program IBReplPackage2XML;
{$APPTYPE CONSOLE}
uses
  SysUtils,
  dm_IBRepl,
  CmdLine,
  {$IFDEF LINUX}
  Libc,
  Types,
  Variants,
  {$ELSE}
  Windows,
  {$IFDEF VER150}
  Variants,
  {$ENDIF}
  {$IFDEF VER140}
  Variants,
  {$ENDIF}
  {$IFDEF VER130}
  SystemD6,
  {$ENDIF}
  {$IFDEF VER170}
  Variants,
  {$ENDIF}
  {$ENDIF}
  Classes,
  IBReplicator,
  Connect,
  dm_IBReplC;

{$R IBReplServer.res}

resourcestring
  sAbout2= 'Tool exporting offline package to XML';
  sHelp1 = 'Usage: ibreplpackage2xml <package> [/X:[<export>]] [/L:<level>] [/E:<par>=<val>] [/F] [/H] [/?]'#13#10+
          'Description:'#13#10+
          '  <package> package file name'#13#10+
          '  <export>  write to export file instead to stdout, if not used use package.xml'#13#10+
          '  <level>   0..no XML header output, 1{default}..no DOCTYPE, 2..complete header'#13#10+
          '  <par>=<val> set environment parameter(s)'#13#10+
          '  /F        include external files as hex'#13#10+
          ''#13#10+
          'WARNING:'#13#10+
          'Long names containing spaces put as quoted parameter';

  sNoPackage= 'Missing name of package';
  sExporting = 'Exporting to "%s"';
var
  St: TStream;
  fReplicator: TIBReplicator;
  fPackage, fExport: string;
  Res: UTF8string;
  Opt: Integer;
begin
  WriteProgName;
  Writeln2(sAbout2);
  WriteLn2('');
  if IsThereCmd('H', clUpcase) or IsThereCmd('?', clUpcase) then
  begin
    Writeln2(sHelp1);
    Exit;
  end;

  try
    fReplicator:= TIBReplicator.Create(nil);
    try
      GetCmdString('', 0, fPackage);
      if fPackage = '' then
        IBReplicatorError(sNoPackage, 2);

      St:= TFileStream.Create(fPackage, fmOpenRead or fmShareDenyWrite);
      try
        Opt:= StrToIntDef(TIBReplDataModuleC.GetParString('L', ''), 1) and xmloptHeader;
        if IsThereCmd('F', clUpcase) then
          Opt:= Opt or xmloptExternal;
        Res:= fReplicator.OfflinePackageToXML(St, Opt);
      finally
        St.Free;
      end;
      if IsThereCmd('X:', clUpcase) then
        begin
          fExport:= TIBReplDataModuleC.GetParString('X', '');
          if fExport = '' then
            fExport:= ChangeFileExt(fPackage, '.xml');
          WriteLn(CP(Format(sExporting, [fExport])));
          St:= TFileStream.Create(fExport, fmCreate);
          try
            StringToStream(Res, St);
          finally
            St.Free;
          end;
        end
      else
        if not IsThereCmd('SILENT', clUpcase) then
          Write(Res)    // do not convert UTF-8 -> OEM
        else
          Write2(Res);
    finally
      fReplicator.Free;
    end;
  except
    on E: Exception do
    begin
      Writeln2(E.Message);
      ExitCode:= 1;
    end;
  end;
end.

