(* transfer_ftp - Transfer library for IB Replicator
 * Copyright (C) 2003  Tomas Mandys-MandySoft
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA  02111-1307  USA
 *)

library transfer_ftp;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  {$IFDEF LINUX}
  Libc,
  {$ELSE}
  Windows,
  FileCtrl,
  {$ENDIF}
  SysUtils,
  Classes,
  IdFTPCommon,
  IdFTP,
  IdException,
  Transfer in 'Transfer.pas';

{$R *.res}

{$IFDEF LINUX}
{$ENDIF}

const
  ToolName = 'ftp';

// Indy version
{..$DEFINE INDY100} // This is defined in {$I IdCompilerDefines.inc} but it requires search path

{$DEFINE IMP_TYPES}
{$I Transfer.inc}

{$DEFINE IMP_CLASS}
{$I Transfer.inc}

{$I ToolUtils.inc}

const
  PackageExtension = '.rpg';

type
  TFTPTransferData = class(TTransferData)
    Params: TStringList;
    ReceivedNames: TStringList;
    FTP: TIdFTP;
    TargetSide: Boolean;
    RemoteDir: string;
    constructor Create;
    destructor Destroy; override;
  end;

constructor TFTPTransferData.Create;
begin
  inherited;
  Params:= TStringList.Create;
  FTP:= TIdFTP.Create(nil);
end;

destructor TFTPTransferData.Destroy;
begin
  Params.Free;
  ReceivedNames.Free;
  if FTP <> nil then
    FTP.Disconnect;
  FTP.Free;
  inherited;
end;

function TransferOpen(aTargetSide: Boolean; aParams: PChar; var H: TFTPTransferData): Integer; stdcall;
begin
  H:= TFTPTransferData.Create;
  try
    Result:= 0;
    H.Params.Text:= StrPas(aParams);
    H.TargetSide:= aTargetSide;
    H.FTP.Host:= GetParString(H.Params, 'Host', H.FTP.Host, Byte(H.TargetSide));
    H.FTP.Port:= GetParInteger(H.Params, 'Port', H.FTP.Port, Byte(H.TargetSide));

    H.FTP.Passive:= GetParBool(H.Params, 'Passive', H.FTP.Passive, Byte(H.TargetSide));
    H.FTP.UserName:= GetParString(H.Params, 'UserName', H.FTP.UserName, Byte(H.TargetSide));
    H.FTP.Password:= GetParString(H.Params, 'Password', H.FTP.Password, Byte(H.TargetSide));
    {..$IFNDEF INDY100}
    H.FTP.ReadTimeout:= GetParInteger(H.Params, 'ReadTimeout', 20000{ms}{H.FTP.ReadTimeout}, Byte(H.TargetSide));
    {..$ENDIF}

    H.FTP.ProxySettings.Host:= GetParString(H.Params, 'Proxy.Host', H.FTP.ProxySettings.Host, Byte(H.TargetSide));
    H.FTP.ProxySettings.Port:= GetParInteger(H.Params, 'Proxy.Port', H.FTP.ProxySettings.Port, Byte(H.TargetSide));
    H.FTP.ProxySettings.UserName:= GetParString(H.Params, 'Proxy.UserName', H.FTP.ProxySettings.UserName, Byte(H.TargetSide));
    H.FTP.ProxySettings.Password:= GetParString(H.Params, 'Proxy.Password', H.FTP.ProxySettings.Password, Byte(H.TargetSide));
    H.FTP.ProxySettings.ProxyType:= TIdFtpProxyType(GetParInteger(H.Params, 'Proxy.Type', Integer(H.FTP.ProxySettings.ProxyType), Byte(H.TargetSide)));
    H.FTP.Connect;
    H.RemoteDir:= GetParString(H.Params, 'DropOffDir', '', Byte(H.TargetSide));
    H.FTP.ChangeDir(H.RemoteDir);
    H.FTP.Noop;
  except
    on E: Exception do
    begin
      Result:= errErrorS;
      H.BlockedError:= Result;
      H.LastErrorS:= E.Message;
    end;
  end;
end;

function TransferClose(H: TFTPTransferData): Integer; stdcall;
begin
  Result:= H.CheckHandle;
  if Result <> 0 then
    Exit;
  H.ClearError;
  H.Free;
end;

function TransferSendPackage(H: TFTPTransferData; Name: PChar; Src: TStream): Integer; stdcall;
begin
  Result:= H.CheckHandle;
  if Result <> 0 then
    Exit;
  H.ClearError;
  try
    H.FTP.TransferType:= ftBinary;
    H.FTP.Put(Src, NamePrefix+StrPas(Name)+PackageExtension, False);
  except
    on E: Exception do
    begin
      H.LastErrorS:= E.Message;
      Result:= errErrorS;
    end;
  end;
end;

function TransferReceivePackageNames(H: TFTPTransferData; NameMask: PChar; var N: Integer): Integer; stdcall;
var
  S: string;
  I: Integer;
begin
  Result:= H.CheckHandle;
  if Result <> 0 then
    Exit;
  H.ClearError;
  try
    if H.ReceivedNames = nil then
      H.ReceivedNames:= TStringList.Create;
    H.ReceivedNames.Clear;
    H.FTP.TransferType:= ftASCII;
    try
      H.FTP.List(H.ReceivedNames, NamePrefix+NameMask+'*'+PackageExtension, True{False raises error on some FTP server});
    except
    {$IFNDEF INDY100}
      on E: EIdProtocolReplyError do
        if E.ReplyErrorCode <> 550 then  // no such file or directory
          raise;
      else
    {$ENDIF}
        raise;
      end;

    for I:= 0 to H.ReceivedNames.Count-1 do
    begin
      H.ReceivedNames[I]:= H.FTP.DirectoryListing.Items[I].FileName;  { must be parsed because List aDetails parametr is true }
      S:= H.ReceivedNames[I];
      Delete(S, 1, Length(NamePrefix));
      Delete(S, Pos('.', S+'.'), Length(S));
      H.ReceivedNames[I]:= S;
    end;
    N:= H.ReceivedNames.Count;
    
  except
    on E: Exception do
    begin
      H.LastErrorS:= E.Message;
      Result:= errErrorS;
    end;
  end;
end;

function TransferGetReceivedPackageName(H: TFTPTransferData; I: Integer; Name: PChar; BufferLength: Integer): Integer; stdcall;
begin
  Result:= H.CheckHandle;
  if Result <> 0 then
    Exit;
  H.ClearError;
  try
    if H.ReceivedNames = nil then
      H.ProjError(sNamesNotReceived);
    StrPLCopy(Name, H.ReceivedNames[I], BufferLength);
  except
    on E: Exception do
    begin
      H.LastErrorS:= E.Message;
      Result:= errErrorS;
    end;
  end;
end;

function TransferReceivePackage(H: TFTPTransferData; Name: PChar; Tgt: TStream): Integer; stdcall;
begin
  Result:= H.CheckHandle;
  if Result <> 0 then
    Exit;
  H.ClearError;
  try
    H.FTP.TransferType:= ftBinary;
    H.FTP.Get(NamePrefix+StrPas(Name)+PackageExtension, Tgt, False);
  except
    on E: Exception do
    begin
      H.LastErrorS:= E.Message;
      Result:= errErrorS;
    end;
  end;
end;

function TransferDeletePackage(H: TFTPTransferData; Name: PChar): Integer; stdcall;
begin
  Result:= H.CheckHandle;
  if Result <> 0 then
    Exit;
  H.ClearError;
  try
    H.FTP.Delete(NamePrefix+StrPas(Name)+PackageExtension);
  except
    on E: Exception do
    begin
      H.LastErrorS:= E.Message;
      Result:= errErrorS;
    end;
  end;
end;

function TransferError(H: TFTPTransferData; ErrS: PChar; BufferLength: Integer): Integer; stdcall;
begin
  Result:= H.TransferError(ErrS, BufferLength);
end;


{$DEFINE EXP_PROCS}
{$I Transfer.inc}

begin
  TransferLibInit;
end.

