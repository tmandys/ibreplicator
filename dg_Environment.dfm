object EnvironmentDialog: TEnvironmentDialog
  Left = 289
  Top = 199
  HelpContext = 23000
  BorderStyle = bsDialog
  Caption = 'Environment [%s]'
  ClientHeight = 257
  ClientWidth = 460
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 30
    Height = 16
    Caption = '&Keys'
    FocusControl = Keys
  end
  object OKBtn: TButton
    Left = 361
    Top = 25
    Width = 93
    Height = 32
    Anchors = [akTop, akRight]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
  object CancelBtn: TButton
    Left = 361
    Top = 70
    Width = 93
    Height = 32
    Anchors = [akTop, akRight]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
  object HelpBtn: TButton
    Left = 361
    Top = 125
    Width = 92
    Height = 32
    Anchors = [akTop, akRight]
    Caption = '&Help'
    TabOrder = 3
    OnClick = HelpBtnClick
  end
  object Keys: TMemo
    Left = 8
    Top = 24
    Width = 345
    Height = 222
    Anchors = [akLeft, akTop, akRight, akBottom]
    ScrollBars = ssVertical
    TabOrder = 0
    WordWrap = False
  end
end
