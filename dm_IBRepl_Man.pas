unit dm_IBRepl_Man;

interface
uses
  SysUtils, dm_IBRepl, Connect, Classes, LogTerm, IBReplicator;

type
  TIBReplDataModule2 = class(TIBReplDataModule)
  private
    procedure OnException(Sender: TComponent; E: Exception; const aName: string; aChannel: Byte; const aOriginalMessage: string);
  public
    procedure AssignLoggers(aIBReplicator: TIBReplicator); override;
  end;


implementation
uses
  fm_SchemaEditor;

{ TIBReplDataModule2 }

procedure TIBReplDataModule2.AssignLoggers(aIBReplicator: TIBReplicator);
begin
  with aIBReplicator do
  begin
    DBLog:= TFileTermLogger.Create(aIBReplicator);
    TFileTermLogger(DBLog).Terminal:= SchemaEditor.DBLog;
    DBLog.OnException:= OnException;
    ReplLog:= DBLog;
  end;
end;

procedure TIBReplDataModule2.OnException(Sender: TComponent; E: Exception; const aName: string; aChannel: Byte; const aOriginalMessage: string);
begin
  TFileTermLogger(Sender).Terminal.Log(E.Message);
end;

end.
 
