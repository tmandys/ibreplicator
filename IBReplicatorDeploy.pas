(* Deployment library
 * Copyright (c) 2003 by Mandys Tomas-MandySoft
 *)

unit IBReplicatorDeploy;
interface

const
  RegDeployDelimiter = '#';
  RegDeployUnitName = 'IBReplicatorDeploy';
var
  RegDeployKey: string;  // multiple keys delimited by RegDeployDelimiter

implementation
end.
