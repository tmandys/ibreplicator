(* IB Replication Suite
 * Copyright (C) 2002,2003 by Tomas Mandys-MandySoft
 *)

unit dg_About;

interface

uses
  {$IFDEF LINUX}
  Libc, QGraphics, QForms, QControls, QStdCtrls, QButtons, QExtCtrls,
  Controls, StdCtrls, ExtCtrls,
  {$ELSE}
  Windows, Graphics, Forms, Controls, StdCtrls, Buttons, ExtCtrls, ShellAPI,
  {$ENDIF}
  SysUtils, Classes;

type
  TAboutDlg = class(TForm)
    OKBtn: TButton;
    Bevel1: TBevel;
    Icon: TImage;
    ProductName: TLabel;
    FileDescription: TLabel;
    LegalCopyright: TLabel;
    Version: TLabel;
    HomePage: TLabel;
    procedure FormShow(Sender: TObject);
    procedure HomePageClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AboutDlg: TAboutDlg;

implementation
uses
  AuxProj, dm_IBRepl;

{$IFDEF LINUX}
{.$R *.xfm}   // not used
{$ELSE}
{$R *.dfm}
{$ENDIF}

procedure TAboutDlg.FormShow(Sender: TObject);
type
  TTranslation = packed record
    wLanguage: Word;
    wCodePage: Word;
  end;
var
  I: Integer;
  FileName, S: string;
  InfoSize, Wnd: DWORD;
  VerBuf: Pointer;
//  FI: PVSFixedFileInfo;
  pC: PChar;
  VerSize: Cardinal;
  Translation: ^TTranslation;
  function GetVal(const aName: string): string;
  var
    S: string;
  begin
    Result:= '';
    S:= Format('\StringFileInfo\%.04x%.04x\%s', [Translation.wLanguage, Translation.wCodePage, aName]);
    if VerQueryValue(VerBuf, PChar(S), Pointer(pC), VerSize) then
      Result:= StrPas(pC);
  end;
resourcestring
  sVersion = 'Version %s (Build %s)';
begin
  // due potential cpi change here in runtime
  ProductName.Font.Style:= [fsBold];
  HomePage.Font.Style:= [fsUnderline];
  HomePage.Font.Color:= clNavy;

  Icon.Picture.Icon.Handle:= Application.Icon.Handle;
  FileName := Application.ExeName;
  InfoSize := GetFileVersionInfoSize(PChar(FileName), Wnd);
  if InfoSize <> 0 then
  begin
    GetMem(VerBuf, InfoSize);
    try
      if GetFileVersionInfo(PChar(FileName), Wnd, InfoSize, VerBuf) then
      begin
        if VerQueryValue(VerBuf, '\VarFileInfo\Translation', Pointer(Translation), VerSize) then
        begin
          for I:= 0 to ComponentCount-1 do
          begin
            if Components[I] is TLabel then
              with Components[I] as TLabel do
              begin
                Caption:= GetVal(Name);
              end;
          end;
          S:= GetVal('FileVersion');
          if Pos(GetVal('ProductVersion')+'.', S) = 1 then
            Delete(S, 1, Length(GetVal('ProductVersion')+'.'));
          Version.Caption:= Format(sVersion, [GetVal('ProductVersion'), S]);
        end;
      end;
    finally
      FreeMem(VerBuf);
    end;
  end;
end;

procedure TAboutDlg.HomePageClick(Sender: TObject);
begin
  CheckShellExecute(ShellExecute(0, 'open', PChar((Sender as TLabel).Caption), '', '', SW_SHOWNORMAL));
end;

end.
