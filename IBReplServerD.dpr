program IBReplServerD;
{$APPTYPE CONSOLE}
uses
  ComObj,
  Classes,
  Windows,
  SysUtils,
  AuxProj,
  CmdLine,
{$IFDEF REGISTRATION}
  Registry,
  IBReplicatorDeploy,
{$ENDIF}
  Math,
  Connect,
  IBReplicator,
  IBDatabase,
  SyncObjs,
  dm_IBRepl in 'dm_IBRepl.pas' {IBReplDataModule: TDataModule},
  dm_IBReplC in 'dm_IBReplC.pas' {IBReplDataModuleC: TDataModule};

{$R IBReplServer.res}

type
  TMappingRecord = record
    Terminate: Boolean;
    Pause: Boolean;
    Reset: Byte;
    ProcessId: pid_t;
    Started: TDateTime;
    Status: array[0..4096] of Char;
  end;

var
  MappingRecord: TMappingRecord;
  FileMapping: THandle;
  ServiceStatus: TServiceStatus;
  ServiceStatusHandle: SERVICE_STATUS_HANDLE;
  SvcName: string;
  SvcType: string;
  IsDaemon: Boolean;
  SvcCriticalSection: TCriticalSection;
  PidFile: string;

type
  TIBReplDataModule2 = class(TIBReplDataModuleC)
  private
  protected
    procedure IBCfgDBBeforeConnect(Sender: TObject);
    procedure UpdateStatus; override;
  public
    Terminate: Boolean;
    function Status: string;
    constructor Create(aOwner: TComponent); override;
    procedure ResetStatus;
    procedure ProcessFileMap;
  end;

procedure _RaiseLastWin32Error(aFunc: string);
begin
  try
    RaiseLastWin32Error;
  except
    on E: Exception do
    begin
      E.Message:= aFunc+':  '+E.Message;
      raise;
    end;
  end;
end;

{ TIBReplDataModule2 }

constructor TIBReplDataModule2.Create;
begin
  inherited;
  IBCfgDB.BeforeConnect:= IBCfgDBBeforeConnect; // no config db params override
end;

procedure TIBReplDataModule2.ResetStatus;
begin
{ dummy }
end;

procedure TIBReplDataModule2.UpdateStatus;
begin
  inherited;
  if not MultiThreading then
  begin
    try
      ProcessFileMap;
    except
    end;
  end;
end;

function TIBReplDataModule2.Status: string;
var
  IBR: TIBReplicatorTask;
  I: Integer;
  function LStr(const S: string; N: Integer): string;
  begin
    Result:= Copy(S+StringOfChar(' ', N), 1, N);
  end;
const
  OnlS: array[0..2*4+1] of string = ('ONL-R', 'ONL-L', 'OFF-S', 'OFF-S', 'OFF-T-R', 'OFF-T-L', 'ONL-T', 'ONL-T', 'SYN', 'SYN');
  DisS: array[Boolean] of string = ('', 'DIS');
  TimS: array[0..3] of string = ('', 'T', 'E', 'T+E');
  RunS: array[Boolean] of string = ('', 'RUN');
begin
  Result:= '';
  Result:= Result+Format('Name: %s'#13#10, [SvcType]);
  Result:= Result+Format('NT service: %d'#13#10, [Byte(IsDaemon)]);
  Result:= Result+Format('Disable all: %d'#13#10, [Byte(DisableAll)]);
  Result:= Result+#13#10;

  for I:= 0 to Tasks.Count-1 do
  begin
    IBR:= Pointer(Tasks[I]);
    Result:= Result+
             Format('%.2d ', [I]) +
             LStr(IBR.TaskName, 15)+ ' '+
             LStr(DisS[IBR.Disabled], 3)+ ' '+
             LStr(OnlS[2*IBR.ReplType+Byte(IBR.ReplOptions and repoptReplicateLog<>0)], 7)+ ' '+
             LStr(TimS[Byte((IBR.ReplType in [0,1,3]) and IBR.DBEvent.Registered) shl 1 + Byte(IBR.Timer.Interval > 0 {enabled is toggled when replicatingt})], 3)+' '+
             LStr(RunS[IBR.IsRunning], 3)+ ' '+
             Format('DBId:%d/', [IBR.DBId]);
    if IBR.DBId = 0 then
      Result:= Result+'?'
    else
      Result:= Result+'"'+IBR.ReplDatabaseName+'" ';
    Result:= Result+'SchId:'+ArrayToString(IntegerArrayToStringArray(IBR.SchemaIds), ',')+' '+
                    'GrId:'+ArrayToString(IntegerArrayToStringArray(IBR.GroupIds), ',')+' '+
                    'SDbId:'+ArrayToString(IntegerArrayToStringArray(IBR.SrcDBIds), ',')+' '+
                    'TDbId:'+ArrayToString(IntegerArrayToStringArray(IBR.TgtDBIds), ',')+' '+
                    'Sec:'+IntToStr(IBR.UpdateStatusTicks div 1000)+' '+
                    'Cnt:'+IntToStr(IBR.UpdataStatusCount)+' '+
                    'Rate:';
    if IBR.UpdateStatusTicks = 0 then
      Result:= Result+'0'
    else
      Result:= Result+FloatToStrF(IBR.UpdataStatusCount / (IBR.UpdateStatusTicks / 1000 / 60), ffGeneral, 2, 0);

    if IBR.LastStarted <> 0 then
      Result:= Result+' '+DateTimeToStr(IBR.LastStarted);
    Result:= Result+#13#10;
  end;
  Result:= Result+#13#10;
end;

procedure TIBReplDataModule2.IBCfgDBBeforeConnect(Sender: TObject);
begin
  TIBReplDataModule(Self).IBCfgDBBeforeConnect(Sender); // skip config db params override
  TIBDatabase(Sender).LoginPrompt:= False;
end;

procedure Daemonize();
var
  S: TStream;
  Pid: TPid_T;
  P: Integer;
  GID, UID: Integer;
  PwEntry: TPasswd;
  GrEntry: TGroup;
resourcestring
  sChrootError = 'chroot("%s") error';
  sChdirError = 'chdir("%s") error';
  sPasswdError = 'Cannot get /etc/passwd entry for user "%s"';
  sGroupError = 'Cannot get /etc/group entry for group "%s"';
  sSetuidError = 'setuid(%d) error';
  sSetgidError = 'setuid(%d) error';
  sForkError = 'fork() error';
  sUnableReopenStdin = 'Unable to replace %s with /dev/null: %s';
begin
  P:= -1;
  SetBuf(stdout, 0);
  SetBuf(stderr, 0);
  S:= Ini.ReadString(DBIniSection, 'Chroot', '');
  S:= TIBReplDataModuleC.GetParString('t', S);
  if (S <> '') and (chroot(S) < 0) then
    raise Exception.CreateFmt(sChrootError, [S]);    

  S:= Ini.ReadString(DBIniSection, 'WorkingDir', '');
  S:= TIBReplDataModuleC.GetParString('w', S);
  if (S <> '') and (chdir(S) < 0) then
    raise Exception.CreateFmt(sChdirError, [S]);    

  S:= Ini.ReadString(DBIniSection, 'User', '');
  S:= TIBReplDataModuleC.GetParString('u', S);
  UID:= -1; GID:= -1;
  if (S <> '') then
  begin
    UID:= StrToIntDef(S, -1);
    if UID < 0 then
    begin
      PwEntry:= getpwnam(S);
      if (PwEntry <> nil) then
        raise Exception.CreateFmt(sPasswdError, [S]);    
      UID:= PwEntry.pw_uid;
      GUI:= PwEntry.pw_gid;
    end;
  end;

  S:= Ini.ReadString(DBIniSection, 'Group', '');
  S:= TIBReplDataModuleC.GetParString('g', S);
  if (S <> '') then
  begin
    GID:= StrToIntDef(S, -1);
    if GID < 0 then
    begin
      GrEntry:= getgrnam(S);
      if (GrEntry <> nil) then
        raise Exception.CreateFmt(sGroupError, [S]);    
      GUI:= GrEntry.gr_gid;
    end;
  end;
  if (GID > 0) and (setgid(GID)<0) then
    raise Exception.CreateFmt(sSetgidError, [GID]);
  if (UID > 0) and (setuid(UID)<0) then
    raise Exception.CreateFmt(sSetuidError, [UID]);

  Pid:= fork();
  if Pid < 0 then
    raise Exception.CreateFmt(sForkError);    
  if Pid = 0 then  // parent process
    Halt;

  IsDaemon:= True;

  if PidFile <> '' then
  begin
  end;

  if freopen('/dev/null', 'r', stdin) = 0 then
    raise Exception.CreateFmt(sUnableReopenStdin, ['stdin', strerror(errno)]);
end;

procedure TIBReplDataModule2.ProcessFileMap;
var
  MappingPtr: ^TMappingRecord;
  LastState: DWORD;
  Pause: Boolean;
begin
  StrLCopy(@MappingRecord.Status, PChar(Status), SizeOf(MappingRecord.Status));

  SvcCriticalSection.Enter;
  try
    MappingPtr:= MapViewOfFile(FileMapping, FILE_MAP_WRITE, 0, 0, SizeOf(TMappingRecord));
    if MappingPtr = nil then
      _RaiseLastWin32Error('MapViewOfFile');
    try
      MappingPtr^.Status:= MappingRecord.Status;
    finally
      UnmapViewOfFile(MappingPtr);
    end;
  finally
    SvcCriticalSection.Leave;
  end;

  MappingPtr:= MapViewOfFile(FileMapping, FILE_MAP_READ, 0, 0, SizeOf(TMappingRecord));
  if MappingPtr = nil then
    _RaiseLastWin32Error('MapViewOfFile');
  try
    MappingRecord.Terminate:= MappingPtr^.Terminate;
    Pause:= MappingPtr^.Pause; // multi-thread
    if not MappingPtr^.Terminate and (IBReplDataModule.Enabled{non-Pause} = Pause) then
    begin
      IBReplDataModule.Lock;
      try
        IBReplDataModule.Enabled{Pause}:= not Pause;
        try
          IBReplDataModule.StopAll;
          IBReplDataModule.ReadFromIni(False, False);
        except
          on E: Exception do
            Writeln2(E.Message);
        end;
      finally
        IBReplDataModule.Unlock;
      end;
    end;
    SvcCriticalSection.Enter;
    try
      if not MappingPtr^.Terminate and (IBReplDataModule.Enabled <> MappingPtr^.Pause) and (Pause = MappingPtr^.Pause {not pending}) then
      begin
        LastState:= ServiceStatus.dwCurrentState;
        if IBReplDataModule.Enabled then
          ServiceStatus.dwCurrentState:= SERVICE_RUNNING
        else
          ServiceStatus.dwCurrentState:= SERVICE_PAUSED;
        if LastState <> ServiceStatus.dwCurrentState then
        begin
          SetServiceStatus (ServiceStatusHandle, ServiceStatus);
        end;
      end;
      if MappingPtr^.Reset <> MappingRecord.Reset then
      begin
        MappingRecord.Reset:= MappingPtr^.Reset;
        ResetStatus;
      end;
    finally
      SvcCriticalSection.Leave;
    end;

  finally
    UnmapViewOfFile(MappingPtr);
  end;
end;

function StartService: Boolean;
var
  Mgr, Svc: Integer;
  UserName, ServiceStartName: string;
  Config: Pointer;
  Size: DWord;
begin
  Result := False;
  Mgr := OpenSCManager(SvcMachine, nil, SC_MANAGER_ALL_ACCESS);
  if Mgr <> 0 then
  begin
    Svc := OpenService(Mgr, PChar(SvcName), SERVICE_ALL_ACCESS);
    Result := Svc <> 0;
    if Result then
    begin
      QueryServiceConfig(Svc, nil, 0, Size);
      Config := AllocMem(Size);
      try
        QueryServiceConfig(Svc, Config, Size, Size);
        ServiceStartName := PQueryServiceConfig(Config)^.lpServiceStartName;
        if CompareText(ServiceStartName, 'LocalSystem') = 0 then
          ServiceStartName := 'SYSTEM';
      finally
        Dispose(Config);
      end;
      CloseServiceHandle(Svc);
    end;
    CloseServiceHandle(Mgr);
  end;
  if Result then
  begin
    SetLength(UserName, Size);
    GetUserName(PChar(UserName), Size);
    Size := 256;
    SetLength(UserName, StrLen(PChar(UserName)));
    Result := CompareText(UserName, ServiceStartName) = 0;
  end;
end;

procedure DoService;
var
  Msg: TMsg;
  MappingPtr: ^TMappingRecord;
resourcestring
  sStarted = 'Started "%s"';
  sStopped = 'Stopped by remote';
  sTerminating = 'Terminated be itself';
begin
  if InitProc <> nil then
    TProcedure(InitProc);   // not called in Console?, InitComObj of ComObj must be called!!!

  DBIniSection2:= '.Server';
  IBReplDataModule:= TIBReplDataModule2.Create(nil);
  try
    try
      MappingRecord.Pause:= IsThereCmd('PAUSE', clUpcase);
      IBReplDataModule.Enabled:= not MappingRecord.Pause;
      if not MappingRecord.Pause then
        IBReplDataModule.ReadFromIni(True, True);
      IBReplDataModule.IBReplicator.ReplLog.Log('', lchNull, Format(sStarted, [SvcType]));

      SvcCriticalSection.Enter;
      try
        MappingPtr:= MapViewOfFile(FileMapping, FILE_MAP_WRITE, 0, 0, SizeOf(TMappingRecord));
        if MappingPtr = nil then
          _RaiseLastWin32Error('MapViewOfFile');
        try
          MappingPtr^:= MappingRecord;
        finally
          UnmapViewOfFile(MappingPtr);
        end;
      finally
        SvcCriticalSection.Leave;
      end;

      repeat
        // pred volanim timeru aby byl status a processid zapsany pred potencialni akci (dlouhy connect)
        TIBReplDataModule2(IBReplDataModule).ProcessFileMap;
        Sleep(100);

        if PeekMessage(Msg, 0, 0, 0, PM_REMOVE) then
        begin
          MappingRecord.Terminate:= MappingRecord.Terminate or (Msg.Message = WM_QUIT);
          if not MappingRecord.Terminate then
          begin
            TranslateMessage(Msg);
            DispatchMessage(Msg);
          end;
        end;
      until MappingRecord.Terminate or TIBReplDataModule2(IBReplDataModule).Terminate;

      if TIBReplDataModule2(IBReplDataModule).Terminate then
        IBReplDataModule.IBReplicator.ReplLog.Log(SvcType, lchNull, sTerminating)
      else
        IBReplDataModule.IBReplicator.ReplLog.Log(SvcType, lchNull, sStopped);
    except
      on E: Exception do
      begin
        IBReplDataModule.IBReplicator.DBLog.Log(SvcType, lchError, E.Message);
        ExitCode:= 1;
      end;
    end;
  finally
    IBReplDataModule.Free;
  end;
end;

procedure ServiceCtrlHandler(CtrlCode: DWord); stdcall;
var
  MappingPtr: ^TMappingRecord;
begin
  SvcCriticalSection.Enter;
  try
    MappingPtr:= MapViewOfFile(FileMapping, FILE_MAP_WRITE, 0, 0, SizeOf(TMappingRecord));
    try
      if MappingPtr = nil then
        _RaiseLastWin32Error('MapViewOfFile');
      case CtrlCode of
        SERVICE_CONTROL_PAUSE:
          begin
            MappingPtr^.Pause:= True;
            ServiceStatus.dwCurrentState:= SERVICE_PAUSE_PENDING;
          end;
        SERVICE_CONTROL_CONTINUE:
          begin
            MappingPtr^.Pause:= False;
            ServiceStatus.dwCurrentState:= SERVICE_CONTINUE_PENDING;
          end;
        SERVICE_CONTROL_STOP:
          begin
            ServiceStatus.dwWin32ExitCode:= 0;
            ServiceStatus.dwCurrentState:= SERVICE_STOP_PENDING;
            MappingPtr^.Terminate:= True;
          end;
        SERVICE_CONTROL_INTERROGATE:
          begin  // just return service status
          end;
        else
      end;
      SetServiceStatus (ServiceStatusHandle, ServiceStatus);  // do not call _RaiseLastWin32Error because is called also from console
    finally
      UnmapViewOfFile(MappingPtr);
    end;
  finally
    SvcCriticalSection.Leave;
  end;
end;

procedure ServiceMain(Argc: DWord; Argv: PLPSTR); stdcall;
begin
  IsDaemon:= True;
  FillChar(ServiceStatus, SizeOf(ServiceStatus), 0);
  ServiceStatus.dwServiceType:= SERVICE_WIN32;
  ServiceStatus.dwCurrentState:= SERVICE_START_PENDING;
  ServiceStatus.dwControlsAccepted:= SERVICE_ACCEPT_STOP or SERVICE_ACCEPT_PAUSE_CONTINUE;

  ServiceStatusHandle:= RegisterServiceCtrlHandler(PChar(SvcName), @ServiceCtrlHandler);

  if ServiceStatusHandle = 0 then
    _RaiseLastWin32Error('RegisterServiceCtrlHandler');

  if not SetServiceStatus (ServiceStatusHandle, ServiceStatus) then
    _RaiseLastWin32Error('SetServiceStatus');

// initialization
// Initialization complete - report running status.
  ServiceStatus.dwCurrentState:= SERVICE_RUNNING;
  ServiceStatus.dwCheckPoint:= 0;
  ServiceStatus.dwWaitHint:= 0;

  if not SetServiceStatus (ServiceStatusHandle, ServiceStatus) then
    _RaiseLastWin32Error('SetServiceStatus');

  DoService;
  ServiceStatus.dwCurrentState:= SERVICE_STOPPED;
  if not SetServiceStatus (ServiceStatusHandle,  ServiceStatus) then
    _RaiseLastWin32Error('SetServiceStatus');
end;

procedure WriteSIDInfo(aSID: PSID);
var
  Buff1, Buff2: array[1..512] of Char;
  N1, N2: DWORD;
  Use: SID_NAME_USE;
const
  UseStr: array[sidTypeUser..sidTypeUnknown] of string = ('User', 'Group', 'Domain', 'Alias', 'WellKnownGroup', 'DeletedAccount', 'Invalid', 'Unknown');
resourcestring
  sSid = 'Sid: %s Owner:"%s" Group:"%s"';
begin
  if not LookupAccountSid(SvcMachine, aSid, @Buff1, N1, @Buff2, N2, Use) then
    _RaiseLastWin32Error('LookupAccountSid');
  Writeln2(Format(sSid, [UseStr[Use], @Buff1, @Buff2]));
end;

type
  PPSID = ^PSID;
  PPSECURITY_DESCRIPTOR = ^PSECURITY_DESCRIPTOR;

  SetEntriesInAclProc = function(cCountOfExplicitEntries: ULONG; pListOfExplicitEntries: PEXPLICIT_ACCESS_;
         OldAcl: PACL; var NewAcl: ACL): DWORD; stdcall;
  GetNamedSecurityInfoProc = function (pObjectName: PAnsiChar; ObjectType: SE_OBJECT_TYPE;
         SecurityInfo: SECURITY_INFORMATION; ppsidOwner, ppsidGroup: PPSID; ppDacl, ppSacl: PACL;
         var ppSecurityDescriptor: PSECURITY_DESCRIPTOR): DWORD; stdcall;
  GetSecurityInfoProc = function (handle: THandle; ObjectType: SE_OBJECT_TYPE;
         SecurityInfo: SECURITY_INFORMATION; ppsidOwner, ppsidGroup: PPSID; ppDacl, ppSacl: PACL;
         var ppSecurityDescriptor: PPSECURITY_DESCRIPTOR): DWORD; stdcall;

var
  S, FifoPath: string;
  H, FileH, LibH: THandle;
  ServiceTableEntry: array[0..1] of TServiceTableEntry;
  MappingPtr: ^TMappingRecord;
  SucurityAttr: TSecurityAttributes;
  SecurityDescriptor: TSecurityDescriptor;
  SecurityDescriptorPtr: PSecurityDescriptor;
//  Access: EXPLICIT_ACCESS;
  Dacl, Sacl: ACL;
  SidOwner, SidGroup: PSID;
  P: Pointer;
resourcestring
  sAbout2 = 'IBReplication server running as daemon or console application';
  sHelp1 = 'Usage: ibreplserversvc [<name>] [/STATUS] [/START] [/PAUSE] [/CONTINUE] [/STOP] [/KILL] [/RESET] [/I:<inifile>] [/U:<user>] [/G:<group>] [/F:<file>] [/W:<work_dir>] [/R:<chroot>] [/SILENT] [/O:<stdout>] [/FI:<fifo>] [/PF:<pidfile>] [/H] [/?]';
  sHelp10 = 'Description:'#13#10+
           '   <name>     name of service, enable running more ibrepl servers, default: "default"'#13#10+
           '   /D         daemonize'#13#10+
           '   /START     start processing, default switch, /START /PAUSE start service in pause'#13#10+
           '   /PAUSE     pause processing'#13#10+
           '   /CONTINUE  continue processing'#13#10+
           '   /STOP      stop process'#13#10+
           '   /KILL      kill process'#13#10+
           '   /STATUS    get status'#13#10+
           '   /RESET     reset status'#13#10+
           '   <user>     user ... service account "DomainName\Username", default: "LocalSystem"'#13#10+
           '   <group>    group'#13#10+
           '   <file>     shared status file'#13#10+
           '   <inifile>  configuration file, default is IBREPL.INI in program directory'#13#10+
           '   /SILENT    no stdout output'#13#10+
           '   <stdout>   stdout file'#13#10+
           '   <fifo>     FIFO path, Default: "/tmp/ibrepl"'#13#10+
           '   <pidfile>  pidfile Default: "/var/run/ibrepl.pid"'#13#10+
           '   /? /H      print this help'#13#10;
  sHelp60= 'WARNING:'#13#10+
           'Long names containing spaces put as quoted parameter'#13#10+
           ''#13#10;

  sHelp70= 'Example:'#13#10+
           '  ibrteplserverd my'#13#10+
           '  ibrteplserverd my /STOP'#13#10+
           '  ibrteplserverd my /STATUS'#13#10;

  sMasterNotRunning = 'Program not running';
  sErrorCreatingMapping = 'Error creating mapping';
  sProcessId = 'ProcessId: $%x';
  sAlreadyRunning = 'Cannot start, instance already running';
  sInstalling = 'Installing service "%s"';
  sUninstalling = 'Uninstalling service "%s"';
  sPaused = 'PAUSED';
  sSvcType = 'Instance name: %s';
  sCannotLoadLibrary = 'Cannot load library "%s"';

{$IFDEF REGISTRATION}
{$I RegClass.inc}
{$I RegIBReplicator.inc}
{$I RegCheckReg.inc}

{$IFNDEF SDK}
{$I RegIBReplicatorCheckPro.inc}
{$DEFINE REGISTRATION_CHECK_PRO}
{$ENDIF}
{$ENDIF}

const
  FifoMode = S_IRUSR or S_IWUSR or S_IRGRP or S_IWGRP;
var
  FifoStream: file;
  FifoRead, FifoWrite: Integer;
begin
  PidFile:= '';
  WriteProgName;
  WriteLn2(sAbout2);
  WriteLn2('');
  if IsThereCmd('H', clUpcase) or IsThereCmd('?', clUpcase) then
  begin
    Writeln2(sHelp1);
    WriteLn2('');
    Writeln2(sHelp10);
    Writeln2(sHelp60);
    Writeln2(sHelp70);
    Exit;
  end;

  try
    {$IFDEF REGISTRATION_CHECK_PRO}
    TReg_CheckPro('Daemon replicator');
    {$ENDIF}

    PidFile:= Ini.ReadString(DBIniSection, 'PidFile', '');
    PidFile:= TIBReplDataModuleC.GetParString('PF', PidFile);

    FifoPath:= Ini.ReadString(DBIniSection, 'FifoPath', '');
    FifoPath:= TIBReplDataModuleC.GetParString('FI', FifoPath);

    S:= 'default';
    GetCmdString('', 0, S);
    SvcType:= S;
    WriteLn2(Format(sSvcType, [SvcType]));


    if IsThereCmd('START', clUpcase) then
      begin
        if PidFile <> '' then
        begin
          if FileExists(PidFile) then
            raise Exception.CreateFmt('Pid file already exists "%s"', PidFile);
        end;
        if FileExists(FifoPath) then
        begin
          if (unlink(FifoPath) < 0) then
            raise Exception.CreateFmt('Cannot delete old FIFO "%s": %s', FifoPath, strerror(errno));
        end;
      end
    else
      begin
        if (FilePath = '') or not FileExists(FifoPath) then
          raise Exception.CreateFmt('FIFO does not exist "%s"', FifoPath);

      end;

    FifoRead:= 0;
    if FifoPath <> '' then
    begin
      if FileExists(FifoPath) then
        begin
          if not IsThereCmd('START', clUpcase) then
            begin

            end
          else
            begin
            end;
        end
      else
        begin
          n:= stat(FifoPath, FileStat);
          if n = 0 then
            begin
              if (unlink(FifoPath) < 0) then
                raise Exception.CreateFmt('Cannot delete old FIFO "%s": %s', FifoPath, strerror(errno));
            end
          if (n < 0) and (errno <> ENOENT) then
            raise Exception.CreateFmt('FIFO stat failed "%s": %s', FifoPath, strerror(errno));
          if mkfifo(FifoPath, FifoMode) < 0 then
            raise Exception.CreateFmt('Cannot create FIFO "%s", mode: %d, %s', FifoPath, FifoMode, strerror(errno));
          if chmod(FifoPath, FifoMode) < 0 then
            raise Exception.CreateFmt('Cannot chmod FIFO "%s", mode:%d, %s', FifoPath, FifoMode, strerror(errno));

          FifoRead:= Open(FifoPath, O_RDONLY, 0);
          if FifoRead < 0 then
            raise Exception.CreateFmt('Cannot open FIFO "%s": %s', FifoPath, strerror(errno));

          FifoStream:= fdopen(FifoRead, 'r');
          if FifoStream = nil then
            raise Exception.CreateFmt('fdopen failed: %s', strerror(errno));

          signal(SIG_PIPE, SIG_IGN);
        end;
    end;

    FillChar(MappingRecord, SizeOf(MappingRecord), 0);
    if Ini.ReadBool(DBIniSection, 'NowAsUTC', False) then
      MappingRecord.Started:= NowUTC()
    else
      MappingRecord.Started:= Now();

    FileH:= INVALID_HANDLE_VALUE;  // = $FFFFFFFF
    SvcCriticalSection:= TCriticalSection.Create;
    FileMapping:= OpenFileMapping(FILE_MAP_WRITE, False, PChar('IBReplServer_'+UpperCase(SvcType)));
    if (FileMapping = 0) and not (GetLastError() in [ERROR_FILE_NOT_FOUND, ERROR_INVALID_NAME]) then
      _RaiseLastWin32Error('OpenFileMapping');
    try
      if FileMapping <> 0 then
        begin
          CloseHandle(FileMapping);  // open handle have security problem accessing master's filemapping, MapViewOfFile returns nil
          FileMapping:= CreateFileMapping(INVALID_HANDLE_VALUE, nil, PAGE_READWRITE, 0, SizeOf(MappingRecord), PChar('IBReplServer_'+UpperCase(SvcType)));
          if IsThereCmd('STATUS', clUpcase) then
            begin
              MappingPtr:= MapViewOfFile(FileMapping, FILE_MAP_READ, 0, 0, SizeOf(TMappingRecord));
              if MappingPtr = nil then
                _RaiseLastWin32Error('MapViewOfFile');
              try
                Writeln2(Format(sProcessId, [MappingPtr^.ProcessId]));
                Writeln2(MappingPtr^.Status);
                if MappingPtr^.Pause then
                  Writeln2(sPaused);
              finally
                UnmapViewOfFile(MappingPtr);
              end;
            end
          else if IsThereCmd('STOP', clUpcase) then
            begin
              ServiceCtrlHandler(SERVICE_CONTROL_STOP);
            end
          else if IsThereCmd('PAUSE', clUpcase) and not IsThereCmd('START', clUpcase) then
            begin
              ServiceCtrlHandler(SERVICE_CONTROL_PAUSE);
            end
          else if IsThereCmd('CONTINUE', clUpcase) then
            begin
              ServiceCtrlHandler(SERVICE_CONTROL_CONTINUE);
            end
          else if IsThereCmd('RESET', clUpcase) then
            begin
              MappingPtr:= MapViewOfFile(FileMapping, FILE_MAP_WRITE, 0, 0, SizeOf(TMappingRecord));
              if MappingPtr = nil then
                _RaiseLastWin32Error('MapViewOfFile');
              try
                Inc(MappingPtr^.Reset);
              finally
                UnmapViewOfFile(MappingPtr);
              end;
            end
          else if IsThereCmd('KILL', clUpcase) then
            begin
              MappingPtr:= MapViewOfFile(FileMapping, FILE_MAP_READ, 0, 0, SizeOf(TMappingRecord));
              if MappingPtr = nil then
                _RaiseLastWin32Error('MapViewOfFile');
              try
                H:= OpenProcess(PROCESS_ALL_ACCESS, True, MappingPtr^.ProcessId);
                try
                  if not TerminateProcess(H, 0) then
                    _RaiseLastWin32Error('OpenProcess');
                finally
                  CloseHandle(H);
                end;
              finally
                UnmapViewOfFile(MappingPtr);
              end;
            end
          else
            raise Exception.Create(sAlreadyRunning);
        end
      else
        begin
          if IsThereCmd('STATUS', clUpcase) or IsThereCmd('STOP', clUpcase) or IsThereCmd('KILL', clUpcase) or IsThereCmd('RESET', clUpcase) or (IsThereCmd('PAUSE', clUpcase) and not IsThereCmd('START', clUpcase)) or IsThereCmd('CONTINUE', clUpcase) or IsThereCmd('SECURITY', clUpcase)  then
            begin
              raise Exception.Create(sMasterNotRunning);
            end;
          MappingRecord.ProcessId:= GetCurrentProcessId();

          if IsThereCmd('INSTALL', clUpcase) then
            begin
              Writeln2(Format(sInstalling, [SvcName]));
              InstallService(True);
            end
          else if IsThereCmd('UNINSTALL', clUpcase) then
            begin
              Writeln2(Format(sUninstalling, [SvcName]));
              InstallService(False)
            end
          else
            begin
              SucurityAttr.nLength:= SizeOf(SucurityAttr);
              SucurityAttr.bInheritHandle:= True;
              SucurityAttr.lpSecurityDescriptor:= @SecurityDescriptor;
              InitializeSecurityDescriptor(SucurityAttr.lpSecurityDescriptor, SECURITY_DESCRIPTOR_REVISION);
(*
              SetSecurityDescriptorControl(SucurityAttr.lpSecurityDescriptor, SE_DACL_AUTO_INHERIT_REQ or SE_SACL_AUTO_INHERIT_REQ, SE_DACL_AUTO_INHERIT_REQ or SE_SACL_AUTO_INHERIT_REQ);
              InitializeAcl(Dacl, SizeOf(Dacl), 1{ACL_REVISION});

              Access.grfAccessPermissions:= SECTION_ALL_ACCESS;
              Access.grfAccessMode:= GRANT_ACCESS;
              Access.grfInheritance:= NO_INHERITANCE;
              Access.Trustee.MultipleTrusteeOperation:= NO_MULTIPLE_TRUSTEE;
              // change these informations to grant access to a group or other user
              Access.Trustee.TrusteeForm:= TRUSTEE_IS_NAME;
              Access.Trustee.TrusteeType:= TRUSTEE_IS_USER;
              Access.Trustee.ptstrName:= 'CURRENT_USER';  // 'CREATOR OWNER'

              LibH:= LoadLibrary('ADVAPI32.DLL');
              if LibH = 0 then
                _RaiseLastWin32Error('LoadLibrary');
              try
                P:= GetProcAddress(LibH, 'SetEntriesInAclA');
                if P <> nil then
                  SetEntriesInAclProc(P)(1, @Access, nil, Dacl);
              finally
                FreeLibrary(LibH);
              end;
*)
              SetSecurityDescriptorDacl(SucurityAttr.lpSecurityDescriptor, True, nil {@Dacl{ nil = all access }, False);

              S:= '';
              GetCmdString('F:', clUpcase or clValueCase, S);
              if S <> '' then
              begin
                FileH:= CreateFile(PChar(S), GENERIC_WRITE or GENERIC_READ, FILE_SHARE_READ or FILE_SHARE_WRITE{?}, @SucurityAttr, CREATE_ALWAYS, FILE_ATTRIBUTE_TEMPORARY or FILE_FLAG_DELETE_ON_CLOSE, 0);
                { file is inaccessable in WinXP, not enough rights, even after mapping is closed, probably created file is impossible to share }
                if FileH = INVALID_HANDLE_VALUE then
                  _RaiseLastWin32Error('CreateFile');
              end;
              FileMapping:= CreateFileMapping(FileH, @SucurityAttr, PAGE_READWRITE, 0, SizeOf(MappingRecord), PChar('IBReplServer_'+UpperCase(SvcType)));
              if FileMapping = 0 then
                _RaiseLastWin32Error('CreateFileMapping');
              if StartService then
                begin
                  FillChar(ServiceTableEntry, SizeOf(ServiceTableEntry), 0);
                  ServiceTableEntry[0].lpServiceName:= PChar(SvcName);
                  ServiceTableEntry[0].lpServiceProc:= @ServiceMain;
                  if not StartServiceCtrlDispatcher(ServiceTableEntry[0]) then
                    _RaiseLastWin32Error('StartServiceCtrlDispatcher');
                end
              else
                DoService;
            end;
        end;
    finally
      if FileMapping <> 0 then
        CloseHandle(FileMapping);
      if FileH <> INVALID_HANDLE_VALUE then
        CloseHandle(FileH);
      SvcCriticalSection.Free;
    end;
  except
    on E: Exception do
    begin
      Writeln2(E.Message);
      ExitCode:= 1;
    end;
  end;
end.

