(* IB Replication Suite
 * Copyright (C) 2002,2003 by Tomas Mandys-MandySoft
 *)

unit dg_SchemaTemplate;

interface

uses
  {$IFDEF LINUX}
  Libc, QGraphics, QForms, QControls, QStdCtrls, QButtons, QExtCtrls,
  {$ELSE}
  Windows, Messages, Graphics, Forms, Controls, StdCtrls, Buttons, ExtCtrls,
  {$ENDIF}
  SysUtils, Classes, Db, IBCustomDataSet, IBSQL;

type
  TSchemaTemplateDialog = class(TForm)
    Panel1: TPanel;
    OKBtn: TButton;
    CancelBtn: TButton;
    Panel2: TPanel;
    Table: TIBDataSet;
    DataSource1: TDataSource;
    HelpBtn: TButton;
    procedure TableBeforeOpen(DataSet: TDataSet);
    procedure TableNewRecord(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TableBeforeEdit(DataSet: TDataSet);
    procedure TableBeforeInsert(DataSet: TDataSet);
    procedure TableBeforePost(DataSet: TDataSet);
    procedure HelpBtnClick(Sender: TObject);
    procedure DataSource1UpdateData(Sender: TObject);
  private
    fMasterTable: TIBDataSet;
    fListTable: TIBDataSet;
    fInsert: Boolean;
    fNonEmptyBlobs: set of Byte;
    { Private declarations }
  protected
    PrimaryFields: string;
    CountPrimaryField: string;
    LastCountPrimaryValue: Integer;
    DoNotGenCountId: Boolean;
    procedure ChangeDependant(aFrom, aTo{-MaxInt...delete}: Integer); virtual;
  public
    BatchReindexingForbidden: Boolean;
    function EditRecord(aListTable: TIBDataSet; aLocateField: string = ''): Boolean;
    function InsertRecord(aListTable, aMasterTable: TIBDataSet; aLocateField: string = ''): Boolean;
    { Public declarations }
  end;

var
  SchemaTemplateDialog: TSchemaTemplateDialog;

implementation
uses
  fm_SchemaEditor, dm_IBRepl, IBReplicator;

{$IFDEF LINUX}
{$R *.xfm}
{$ELSE}
{$R *.dfm}
{$ENDIF}

resourcestring
 sTargetIDAlreadyExist = 'Target index (ID) already exists';

function TSchemaTemplateDialog.EditRecord;
var
  I: Integer;
begin
  fListTable:= aListTable;
  Table.Transaction:= fListTable.Transaction;
  Table.Open;
  fNonEmptyBlobs:= [];
  for I:= 0 to Table.FieldCount-1 do
    if Table.Fields[I].IsBlob and (Table.Fields[I].asString <> '') then
      Include(fNonEmptyBlobs, I);
  Result:= ShowModal = mrOk;
  if Result then
  begin
    fListTable.Close;
    fListTable.Open;
    if aLocateField = '' then
      aLocateField:= CountPrimaryField;
    fListTable.Locate(aLocateField, Table.FieldByName(aLocateField).asInteger, []);
  end;
end;

function TSchemaTemplateDialog.InsertRecord;
begin
  fListTable:= aListTable;
  fMasterTable:= aMasterTable;
  fInsert:= True;
  Table.Transaction:= fListTable.Transaction;
  Table.Open;
  Table.Insert;
  Result:= ShowModal = mrOk;
  if Result then
  begin
    fListTable.Close;
    fListTable.Open;
    if aLocateField = '' then
      aLocateField:= CountPrimaryField;
    fListTable.Locate(aLocateField, Table.FieldByName(aLocateField).asInteger, []);
  end;
end;

procedure TSchemaTemplateDialog.TableBeforeOpen(DataSet: TDataSet);
var
  Q1: TIBSQL;
  WhereS1, WhereS, TableN, FieldN: string;
begin
  TableN:= IBReplDataModule.IBReplicator.ConfigDatabasePrefix+TrimRight(Table.SelectSQL[1]);
  Q1:= TIBSQL.Create(nil);
  try
    Q1.Database:= Table.Database;
    Q1.Transaction:= Table.Transaction;
    Q1.SQL.Add('SELECT DISTINCT RF.RDB$FIELD_NAME');
    Q1.SQL.Add('FROM RDB$RELATION_FIELDS RF INNER JOIN RDB$FIELDS F ON RF.RDB$FIELD_SOURCE=F.RDB$FIELD_NAME');
    Q1.SQL.Add(Format('WHERE RF.RDB$RELATION_NAME=''%s'' AND F.RDB$COMPUTED_SOURCE IS NULL', [TIBReplicator.FormatIdentifierValue(Q1.Database.SQLDialect, TableN)]));
    Q1.SQL.Add('ORDER BY RF.RDB$FIELD_POSITION;');
    Q1.ExecQuery;
    Table.InsertSQL.Text:= Format('INSERT INTO %s(', [TIBReplicator.FormatIdentifier(Table.Database.SQLDialect, TableN)]);
    Table.InsertSQL.Add('');
    Table.InsertSQL.Add(') VALUES (');
    Table.InsertSQL.Add('');
    Table.InsertSQL.Add(')');
    Table.ModifySQL.Text:= Format('UPDATE %s SET ', [TIBReplicator.FormatIdentifier(Table.Database.SQLDialect, TableN)]);
    Table.ModifySQL.Add('');

    Table.ModifySQL.Add('');
    while not Q1.EOF do
    begin
      if Table.InsertSQL[1] <> '' then
      begin
        Table.InsertSQL[1]:= Table.InsertSQL[1]+',';
        Table.InsertSQL[3]:= Table.InsertSQL[3]+',';
        Table.ModifySQL[1]:= Table.ModifySQL[1]+',';
      end;
      FieldN:= TrimRight(Q1.Fields[0].asString);
      Table.InsertSQL[1]:= Table.InsertSQL[1]+TIBReplicator.FormatIdentifier(Table.Database.SQLDialect, FieldN);
      Table.InsertSQL[3]:= Table.InsertSQL[3]+':'+TIBReplicator.FormatIdentifier(Table.Database.SQLDialect, FieldN);
      Table.ModifySQL[1]:= Table.ModifySQL[1]+TIBReplicator.FormatIdentifier(Table.Database.SQLDialect, FieldN)+'=:'+TIBReplicator.FormatIdentifier(Table.Database.SQLDialect, FieldN);
      Q1.Next;                                              
    end;
    Q1.Close;
    Q1.SQL.Clear;
    Q1.SQL.Add('SELECT DISTINCT ISG.RDB$FIELD_NAME');
    Q1.SQL.Add('FROM RDB$RELATION_CONSTRAINTS RLC,RDB$INDICES ID,RDB$INDEX_SEGMENTS ISG');
    Q1.SQL.Add('WHERE RLC.RDB$CONSTRAINT_TYPE=''PRIMARY KEY''');
    Q1.SQL.Add(Format('AND RLC.RDB$INDEX_NAME=ID.RDB$INDEX_NAME AND ID.RDB$INDEX_NAME=ISG.RDB$INDEX_NAME AND ID.RDB$RELATION_NAME=''%s''', [TIBReplicator.FormatIdentifierValue(Q1.Database.SQLDialect, TableN)]));
    Q1.SQL.Add('ORDER BY ISG.RDB$FIELD_POSITION;');
    Q1.ExecQuery;
    WhereS:= '';
    PrimaryFields:= '';
    while not Q1.EOF do
    begin
      if WhereS <> '' then
        begin
          WhereS:= WhereS+' AND ';
          WhereS1:= WhereS1+' AND ';
        end
      else
        begin
          WhereS:= WhereS+' WHERE ';
          WhereS1:= WhereS1+' WHERE ';
        end;
      FieldN:= TrimRight(Q1.Fields[0].asString);
      WhereS:= WhereS + TIBReplicator.FormatIdentifier(Q1.Database.SQLDialect, FieldN) +'=:'+ TIBReplicator.FormatIdentifier(Q1.Database.SQLDialect, 'NEW_'+FieldN);
      if not fInsert then
        WhereS1:= WhereS1 + Format('%s=%d', [TIBReplicator.FormatIdentifier(Q1.Database.SQLDialect, FieldN), fListTable.FieldByName(FieldN).asInteger]);
      CountPrimaryField:= FieldN;
      PrimaryFields:= PrimaryFields+FieldN+';';
      Q1.Next;
    end;
  finally
    Q1.Free;
  end;
  Table.SelectSQL[1]:= TIBReplicator.FormatIdentifier(Table.Database.SQLDialect, TableN);
  Table.RefreshSQL.Text:= Table.SelectSQL.Text+' '+WhereS;
  if not fInsert then
  begin
    Table.SelectSQL[2]:= WhereS1;
    Table.ModifySQL[2]:= WhereS;
  end;
end;

procedure TSchemaTemplateDialog.TableNewRecord(DataSet: TDataSet);
var
  Q: TIBSQL;
  I: Integer;
  S: string;
begin
  Q:= TIBSQL.Create(nil);
  try
    Q.Database:= Table.Database;
    Q.Transaction:= Table.Transaction;
    Q.SQL.Text:= Format('SELECT MAX(%s) FROM %s', [TIBReplicator.FormatIdentifier(Q.Database.SQLDialect, CountPrimaryField), Table.SelectSQL[1]]);
    Q.SQL.Add('');
    I:= 1;
    repeat
      S:= ExtractFieldName(PrimaryFields, I);
      if S <> '' then
      begin
        if (S <> CountPrimaryField) and (fMasterTable <> nil) then
        begin
          DataSet.FieldByName(S).Value:= fMasterTable.FieldByName(S).Value;
          if Q.SQL[1] <> '' then
            Q.SQL[1]:= Q.SQL[1]+' AND '
          else
            Q.SQL[1]:= 'WHERE ';
          Q.SQL[1]:= Q.SQL[1] + Format('%s=%d', [TIBReplicator.FormatIdentifier(Q.Database.SQLDialect, S), fMasterTable.FieldByName(S).asInteger]);
        end;
      end;
    until I > Length(PrimaryFields);
    if (CountPrimaryField <> '') and not DoNotGenCountId then
    begin
      Q.ExecQuery;
      with DataSet.FieldByName(CountPrimaryField) do
      begin
        if Q.EOF then
          asInteger:= 1
        else
          asInteger:= Q.Fields[0].asInteger+1;
        LastCountPrimaryValue:= asInteger;
      end;
    end;
  finally
    Q.Free;
  end;
end;

procedure TSchemaTemplateDialog.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  {$IFDEF LINUX}
  Action:= caHide;
  {$ELSE}
  Action:= caFree;
  {$ENDIF}
  try
    if ModalResult = mrCancel then
      begin
        Table.Cancel;
      end else
    if ModalResult = mrOk then
      begin
        Table.CheckBrowseMode;
      end;
  except
    Action:= caNone;
    raise;
  end;
end;

procedure TSchemaTemplateDialog.TableBeforeEdit(DataSet: TDataSet);
begin
  LastCountPrimaryValue:= DataSet.FieldByName(CountPrimaryField).asInteger;
end;

procedure TSchemaTemplateDialog.TableBeforeInsert(DataSet: TDataSet);
begin
  LastCountPrimaryValue:= MaxInt;
end;

procedure TSchemaTemplateDialog.TableBeforePost(DataSet: TDataSet);
var
  Q: TIBSQL;
  I: Integer;
  S: string;
begin
  Q:= TIBSQL.Create(nil);
  try
    Q.Database:= Table.Database;
    Q.Transaction:= Table.Transaction;
    if DataSet.FieldByName(CountPrimaryField).asInteger <> LastCountPrimaryValue then
    begin
      Q.SQL.Text:= Format('SELECT COUNT(*) FROM %s WHERE %s=%d', [Table.SelectSQL[1], TIBReplicator.FormatIdentifier(Q.Database.SQLDialect, CountPrimaryField), DataSet.FieldByName(CountPrimaryField).asInteger]);
      Q.SQL.Add('');
      Q.SQL.Add('');
      I:= 1;
      repeat
        S:= ExtractFieldName(PrimaryFields, I);
        if S <> '' then
        begin
          if S <> CountPrimaryField then
            Q.SQL[2]:= Q.SQL[2] + Format(' AND %s=%d', [TIBReplicator.FormatIdentifier(Q.Database.SQLDialect, S), DataSet.FieldByName(S).asInteger]);
        end;
      until I > Length(PrimaryFields);
      Q.ExecQuery;

      if Q.Fields[0].asInteger > 0 then
        begin
          if BatchReindexingForbidden then
            IBReplicatorError(sTargetIDAlreadyExist, 2);
          Q.Close;
          Q.SQL[0]:= Format('DELETE FROM %s WHERE %s=%d', [Table.SelectSQL[1], TIBReplicator.FormatIdentifier(Q.Database.SQLDialect, CountPrimaryField), MaxInt]);
          Q.ExecQuery;
          ChangeDependant(MaxInt, -MaxInt);

          Q.SQL[0]:= Format('UPDATE %s SET ', [Table.SelectSQL[1]]);
          Q.SQL[1]:= Format('%s=%d WHERE %s=%d', [TIBReplicator.FormatIdentifier(Q.Database.SQLDialect, CountPrimaryField), MaxInt, TIBReplicator.FormatIdentifier(Q.Database.SQLDialect, CountPrimaryField), LastCountPrimaryValue]);
          Q.ExecQuery;
          ChangeDependant(LastCountPrimaryValue, MaxInt);

          if DataSet.FieldByName(CountPrimaryField).asInteger > LastCountPrimaryValue then
            for I:= LastCountPrimaryValue+1 to DataSet.FieldByName(CountPrimaryField).asInteger do
            begin
              Q.SQL[1]:= Format('%s=%s-1 WHERE %s=%d', [TIBReplicator.FormatIdentifier(Q.Database.SQLDialect, CountPrimaryField), TIBReplicator.FormatIdentifier(Q.Database.SQLDialect, CountPrimaryField), TIBReplicator.FormatIdentifier(Q.Database.SQLDialect, CountPrimaryField), I]);
              Q.ExecQuery;
              ChangeDependant(I, I-1);
            end
          else
            for I:= LastCountPrimaryValue-1 downto DataSet.FieldByName(CountPrimaryField).asInteger do
            begin
              Q.SQL[1]:= Format('%s=%s+1 WHERE %s=%d', [TIBReplicator.FormatIdentifier(Q.Database.SQLDialect, CountPrimaryField), TIBReplicator.FormatIdentifier(Q.Database.SQLDialect, CountPrimaryField), TIBReplicator.FormatIdentifier(Q.Database.SQLDialect, CountPrimaryField), I]);
              Q.ExecQuery;
              ChangeDependant(I, I+1);
            end;

          Q.SQL[1]:= Format('%s=%d WHERE %s=%d', [TIBReplicator.FormatIdentifier(Q.Database.SQLDialect, CountPrimaryField), DataSet.FieldByName(CountPrimaryField).asInteger, TIBReplicator.FormatIdentifier(Q.Database.SQLDialect, CountPrimaryField), MaxInt]);
          Q.ExecQuery;
          ChangeDependant(MaxInt, DataSet.FieldByName(CountPrimaryField).asInteger);

          Q.SQL[0]:= Format('DELETE FROM %s WHERE %s=%d', [Table.SelectSQL[1], TIBReplicator.FormatIdentifier(Q.Database.SQLDialect, CountPrimaryField), MaxInt]);
          Q.SQL[1]:= '';
          Q.ExecQuery;
          ChangeDependant(MaxInt, -MaxInt);
        end
      else
        begin
          Q.Close;
          Q.SQL[0]:= Format('UPDATE %s SET ', [Table.SelectSQL[1]]);
          Q.SQL[1]:= Format('%s=%d WHERE %s=%d', [TIBReplicator.FormatIdentifier(Q.Database.SQLDialect, CountPrimaryField), DataSet.FieldByName(CountPrimaryField).asInteger, TIBReplicator.FormatIdentifier(Q.Database.SQLDialect, CountPrimaryField), LastCountPrimaryValue]);
          Q.ExecQuery;
          ChangeDependant(LastCountPrimaryValue, DataSet.FieldByName(CountPrimaryField).asInteger);
        end;
    end;
  finally
    Q.Free;
  end;
end;

procedure TSchemaTemplateDialog.ChangeDependant(aFrom, aTo: Integer);
begin

end;

procedure TSchemaTemplateDialog.HelpBtnClick(Sender: TObject);
begin
  Application.{$IFDEF LINUX}ContextHelp{$ELSE}HelpContext{$ENDIF}(HelpContext);
end;

procedure TSchemaTemplateDialog.DataSource1UpdateData(Sender: TObject);
var
  I: Integer;
begin
  with Sender as TDataSource do
    for I:= 0 to DataSet.FieldCount-1 do   // fixes mistake in VCL, setting BLOB value to Null does not set Modified to true
    begin
      if DataSet.Fields[I].IsBlob and (DataSet.Fields[I].asString = '') and (I in fNonEmptyBlobs) then
      begin
        DataSet.Fields[I].asString:= ' ';
        DataSet.Fields[I].asString:= '';
        Break;
      end;
    end;
end;

end.
