(* dm_IBRepl - replication datamodule
 * Copyright (C) 2002,2003,2004 by Tomas Mandys-MandySoft
 *)

unit dm_IBRepl;

interface

uses
  {$IFDEF LINUX}
  Libc, Types, Qt, {$IFNDEF CONSOLE}QForms, QExtCtrls, QTypes, {$ENDIF}
  {$ELSE}
  Windows, Messages, Forms, ExtCtrls, EventLog,
  {$ENDIF}
  SysUtils, Classes, IBReplicator, Connect, IniFiles, IBDatabase, Db, Contnrs,
  {$IFDEF VER130}IBEvents6, {$ELSE}IBEvents, {$ENDIF}
  IBSQL, Cron, IBCustomDataSet;

  {$IFDEF LINUX}
  {$IFDEF CONSOLE}
type
  TTimer = class;

  TTimerThread = class(TThread)
  private
    fOwner: TTimer;
    fSignaled: Boolean;
  protected
    procedure Execute; override;
  public
  end;

  // implement own TTimer because QExtCtrls requires X Server
  TTimer = class(TComponent)
  private
    fThread: TTimerThread;
    FInterval: Cardinal;
    FOnTimer: TNotifyEvent;
    FEnabled: Boolean;
    procedure SetEnabled(Value: Boolean);
    procedure SetInterval(Value: Cardinal);
  protected
    procedure Timer; virtual;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property Enabled: Boolean read FEnabled write SetEnabled default True;
    property Interval: Cardinal read FInterval write SetInterval default 1000;
    property OnTimer: TNotifyEvent read FOnTimer write FOnTimer;
  end;
  {$ENDIF}
  {$ENDIF}

{$IFNDEF VER130}
type
  TIBEvents6 = TIBEvents;
{$ENDIF}

type
  TIBReplicatorTask = class;
  TIBReplDataModule = class;

  TIBReplicatorThread = class(TThread)
  private
    fOwner: TIBReplicatorTask;
  protected
    procedure Execute; override;
  end;

  TIBReplicatorTask = class(TIBReplicator)
  private
    fDataModule: TIBReplDataModule;
    fThread: TIBReplicatorThread;
    fDBEvent: TIBEvents6;
    FlagReplDB: Byte;
    fLockFlag: Byte;
    fInterval: TDateTime;
    fFileLock: string;
    fCron: TCron;
    procedure OnReplDBBeforeDisconnect(Sender: TObject);
    procedure OnDBEventAlert(Sender: TObject; EventName: String; EventCount: Integer; var CancelAlerts: Boolean);
    procedure UpdateStatusLine(Sender: TObject; T, aCnt: LongWord);
    procedure OnTerminate(Sender: TObject);
    procedure AssignReplDB(aIBReplicator: TIBReplicator; aDB: TIBDatabase);
    procedure OnReplDBAfterDisconnect(Sender: TObject);
    function FmtT(const S: string): string;
  protected
    function ForceStop: Boolean; override;
  public
    TaskName: string;
    Index: Integer;
    ReplDB: TIBDatabase;
    ReplDatabaseName: string;
    Disabled: Boolean;
    ReplOnEvent: Boolean;
    ReplEventName: string;
    LastStarted, LastFinished, MaxReplRunTime: TDateTime;

    ReplType: Integer;
    DBId: Integer;
    SchemaIds, GroupIds, SrcDBIds, TgtDBIds: TIntegerOpenArray;
    SchemaId, GroupId, SrcDbId, TgtDBId: Integer;
    DBProps: TReplDatabaseProperties;
    OfflineDir: string;
    SendData: Boolean;
    ResendData: Boolean;
    ReceiveData: Boolean;
    ReplOptions: Word;

    UpdateStatusTicks, UpdataStatusCount: LongWord;

    property Cron: TCron read fCron;
    property DBEvent: TIBEvents6 read fDBEvent;
    property Interval: TDateTime read fInterval;
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;

    procedure ReadFromIni(aName: string);
    function CanReplicate(aManual: Boolean): Boolean;
    procedure DoReplicate;
    function IsRunning: Boolean;
    procedure Lock;
    procedure Unlock;

    procedure OpenReplDB;
    procedure CloseReplDB;

    function SectionName: string;
    class function GetSectionName(const aTaskName: string): string;
  end;

  TIBReplDataModule = class(TDataModule)
    IBCfgDB: TIBDatabase;
    IBCfgTransaction: TIBTransaction;
    IBReplicator: TIBReplicator;
    IniTimer: TTimer;
    ReplExtFileQuery: TIBDataSet;
    IBDatabase1: TIBDatabase;
    IBTransaction1: TIBTransaction;
    ScheduleTimer: TTimer;
    procedure IBCfgDBAfterConnect(Sender: TObject);
    procedure IBCfgDBBeforeConnect(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure IniTimerTimer(Sender: TObject);
    procedure ScheduleTimerTimer(Sender: TObject);
    procedure IBCfgDBAfterDisconnect(Sender: TObject);
  private
    fOnRereadIni: TNotifyEvent;
    fTasks: TObjectList;
    fLockFlag: Byte;
    fReconnectingFlag: Boolean;
    function GetEnabled: Boolean;
    procedure SetEnabled(aEnable: Boolean);
  protected
    procedure ReadIBReplicatorFromIni(aIBReplicator: TIBReplicator; const aIniSection: string); virtual;
    procedure UpdateStatus; virtual;
  public
    DisableAll: Boolean;
    UpgradeFlag: Byte;
    MultiThreading: Boolean;
    MaxReplThreads: Integer;
    IniCheck: Boolean;
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    property Tasks: TObjectList read fTasks;
    procedure ReadFromIni(aReadTasks, aRefreshCfgDB: Boolean);
    procedure StopAll;
    procedure Lock;
    procedure Unlock;
    function RunningCount: Integer;
    procedure AssignLoggers(aIBReplicator: TIBReplicator); virtual;
    procedure InsertTask(P: TIBReplicatorTask; I: Integer = -1);
    property LockFlag: Byte read fLockFlag;
    property Enabled: Boolean read GetEnabled write SetEnabled;

    property OnRereadIni: TNotifyEvent read fOnRereadIni write fOnRereadIni;
  end;

  TFileSysLogger = class(TFileLogger)
  private
    fSourceName: string;
  protected
    procedure DoLog2(const aName: string; aChannel: Byte; const aText: string);  override;
  public
    property SourceName: string read fSourceName write fSourceName;
  end;


var
  DBIniSection: string;
  DBIniSection2: string;  // local subsection identifier
  DebugFileName: string;

resourcestring
  sHomePage = 'http://www.2p.cz/en/interbase_replicator/';
  sSupport = 'http://www.2p.cz/en/interbase_replicator/forum';

function DecodeInterval(S: string): TDateTime;
procedure WriteToFile(const aFileName, S: string);
procedure Debug(const S: string; const Args: array of const); overload;
procedure Debug(const S: string); overload;
procedure WriteToSysLog(const aIdent: string; aChannel: Byte; aMsg: string);

var
  IBReplDataModule: TIBReplDataModule;

implementation
uses
  CmdLine, AuxProj, IB, SyncObjs
  {$IFDEF REGISTRATION}, IBReplicatorDeploy
    {$IFNDEF LINUX}, Registry{$ENDIF}
    {$IFDEF VER120}, FileCtrl{$ENDIF}{$IFDEF VER130}, FileCtrl{$ENDIF}
  {$ENDIF}
  ;

{$R *.dfm}

{$IFDEF REGISTRATION}
{$I RegIBReplicator.inc}
{$I RegClass.inc}
{$I RegCheckReg.inc}

{$I RegIBReplicatorCheckPro.inc}
{$ENDIF}

var
  WriteCriticalSection: TCriticalSection;

procedure WriteToFile(const aFileName, S: string);
var
{$IFDEF LINUX}
  H: PIOFile;
{$ELSE}
  D: DWORD;
  H: THandle;
{$ENDIF}
begin
  if S = '' then
    Exit;
  WriteCriticalSection.Enter;
  try
    if aFileName = '' then
      Write(S)
    else
      begin
        try
        {$IFDEF LINUX}
          H:= fopen(PChar(aFileName),  'a');
          try
            if H <> nil then
              fwrite(@S[1], Length(S), 1, H);
          finally
            if H <> nil then
              fclose(H);
          end;
        {$ELSE}
          H:= CreateFile(PChar(aFileName), GENERIC_READ or GENERIC_WRITE, FILE_SHARE_READ or FILE_SHARE_WRITE {or FILE_SHARE_DELETE...NTOnly}, nil, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
          try
            if H <> INVALID_HANDLE_VALUE then
            begin
              SetFilePointer(H, 0, nil, FILE_END);
              WriteFile(H, S[1], Length(S), D, nil);
            end;
          finally
            CloseHandle(H);
          end;
        {$ENDIF}
        except
        end;
      end;
  finally
    WriteCriticalSection.Leave;
  end;
end;

procedure Debug(const S: string);
begin
  Debug(S, []);
end;

procedure Debug(const S: string; const Args: array of const);
begin
  if DebugFileName <> '' then
    try
      WriteToFile(DebugFileName, DateTimemsToStr(Now)+') '+Format(S, Args)+#13#10);
    except  // if bad Args does not correspond to S
    end;
end;

function IniReadBool(const aSection, aName: string; aDef: Boolean): Boolean;
begin
  Result:= Ini.ReadBool(aSection, aName, Ini.ReadBool(DBIniSection, aName, aDef));
end;

function IniReadString(const aSection, aName: string; const aDef: string): string;
begin
  Result:= Ini.ReadString(aSection, aName, Ini.ReadString(DBIniSection, aName, aDef));
end;

function IniReadInteger(const aSection, aName: string; const aDef: Integer): Integer;
begin
  Result:= Ini.ReadInteger(aSection, aName, Ini.ReadInteger(DBIniSection, aName, aDef));
end;

function DecodeInterval(S: string): TDateTime;
  procedure GetS(var Res: TDateTime; const aTag: string; aDT: TDateTime);
  var
    I, J: Integer;
  begin
    I:= Pos(aTag, S+'s'{default is sec});
    if I > 0 then
    begin
      J:= I-1;
      while (J >= 1) and (S[J] in ['0'..'9']) do
        Dec(J);
      Res:= Res + StrToIntDef(Copy(S, J+1, I-J-1), 0)*aDT;
    end;
  end;
begin
  Result:= 0;
  GetS(Result, 'd', 1);
  GetS(Result, 'h', 1/24);
  GetS(Result, 'm', 1/24/60);
  GetS(Result, 's', 1/24/60/60);
end;

procedure WriteToSysLog(const aIdent: string; aChannel: Byte; aMsg: string);
var
{$IFNDEF LINUX}
   EL: TEventLog;
{$ENDIF}
   W1: Word;
begin
{$IFDEF LINUX}
  WriteCriticalSection.Enter;
  try
    openlog(PChar(aIdent), LOG_PID or LOG_NDELAY, LOG_USER);
    try
      if aChannel = lchNull then
        W1:= LOG_NOTICE
      else if aChannel = lchError then
        W1:= LOG_ERR
      else
        W1:= 0;

      if W1 <> 0 then
        syslog(W1, PChar(aMsg));
    finally
      closelog();
    end;
  finally
    WriteCriticalSection.Leave;
  end;
{$ELSE}
  EL:= TEventLog.Create(nil);
  try
    EL.SourceName:= aIdent;
    EL.AutoOpen:= True;
    if aChannel = lchNull then
      W1:= EVENTLOG_INFORMATION_TYPE
    else if aChannel = lchError then
      W1:= EVENTLOG_ERROR_TYPE
    else
      W1:= 0;
    if W1 <> 0 then
      EL.Log(W1, 0, aMsg);
  finally
    EL.Free;
  end;
{$ENDIF}
end;

{ TFileSysLogger }

procedure TFileSysLogger.DoLog2;
begin
  inherited;
  try
    if fSourceName <> '' then
      WriteToSysLog(fSourceName, aChannel, aText);
  except
    on E: Exception do
      DoOnException(E, '', lchError, 'WriteToSyslog');
  end;
end;

{ TIBReplDataModule }

constructor TIBReplDataModule.Create(aOwner: TComponent);
var
  S: string;
begin
  inherited;
  fTasks:= TObjectList.Create;
  fTasks.OwnsObjects:= True;
  IBCfgDB.BeforeConnect:= IBCfgDBBeforeConnect;   // is public, may be opened in Create
  S:= '';
  GetCmdString('IC:', clUpcase, S);
  IniCheck:= StrToIntDef(S, 1) > 0;
end;

destructor TIBReplDataModule.Destroy;
begin
  Lock;
  while fLockFlag > 1 do   // wait for IniTimerTimer finish
    Sleep(10);
  inherited;
  fTasks.Free;
end;

procedure TIBReplDataModule.IBCfgDBAfterConnect(Sender: TObject);
resourcestring
  sVerUpgradable = 'Config database is not up to date. Upgrade it.';
  sVerNonupgradable = 'Config database cannot be upgraded';
  sCfgConnected = 'Cfg connected';
var
  U: TDatabaseVersion;
begin
  IBReplicator.DBLog.Log('', lchNull, sCfgConnected);
  if UpgradeFlag = 0 then
  begin
    U:= IBReplicator.CheckConfigDatabaseVersion;
    if U <> verCurrent then
      TIBDatabase(Sender).Close;
    case U of
      verUpgradable:
        IBReplicatorError(sVerUpgradable, 1);
      verNonUpgradable:
        IBReplicatorError(sVerNonUpgradable, 1);
    end;
  end;
end;

procedure TIBReplDataModule.IBCfgDBAfterDisconnect(Sender: TObject);
resourcestring
  sCfgDisconnected = 'Cfg disconnected';
begin
  if not (csDestroying in ComponentState) then
    IBReplicator.DBLog.Log('', lchNull, sCfgDisconnected);
end;

procedure TIBReplDataModule.IBCfgDBBeforeConnect(Sender: TObject);
begin
  if IsThereCmd('TRACESQL', clUpcase) then
    TIBDatabase(Sender).TraceFlags:= [tfQPrepare,tfQExecute,tfQFetch,tfError,tfStmt,tfConnect,tfTransact,tfBlob,tfService,tfMisc]
  else
    TIBDatabase(Sender).TraceFlags:= [];
  TIBDatabase(Sender).DatabaseName:= IBReplicator.ParseStr(Ini.ReadString(DBIniSection+'.ConfigDatabase', 'DatabaseName', ''));
  CheckDatabaseName(TIBDatabase(Sender).DatabaseName, 'CfgDB');
  TIBDatabase(Sender).Params.Values['user_name']:= IBReplicator.ParseStr(Ini.ReadString(DBIniSection+'.ConfigDatabase', 'User', ''));
  TIBDatabase(Sender).Params.Values['password']:= IBReplicator.ScramblePassword(IBReplicator.ParseStr(IBReplicator.ScramblePassword(Ini.ReadString(DBIniSection+'.ConfigDatabase', 'Password', ''), False)), False);
  TIBDatabase(Sender).Params.Values['lc_ctype']:= IBReplicator.ParseStr(Ini.ReadString(DBIniSection+'.ConfigDatabase', 'CharSet', 'NONE'));
  TIBDatabase(Sender).SQLDialect:= Ini.ReadInteger(DBIniSection+'.ConfigDatabase', 'SQLDialect', 3);

  TIBDatabase(Sender).LoginPrompt:= (TIBDatabase(Sender).Params.Values['user_name'] = '') or (TIBDatabase(Sender).Params.Values['password'] = '');
end;

procedure TIBReplDataModule.DataModuleCreate(Sender: TObject);
begin
  IBReplicator.TraceSQL:= IsThereCmd('TRACESQL', clUpcase);
end;

procedure TIBReplDataModule.DataModuleDestroy(Sender: TObject);
begin
  StopAll;
end;

procedure TIBReplDataModule.ReadFromIni;
var
  Sg: TStrings;
  I, J: Integer;
  P: TIBReplicatorTask;
resourcestring
  sCfgDB = '"config database:"';
begin
  StopAll;
  if aRefreshCfgDB then
    IBReplicator.ConfigDatabase.Close;
  DisableAll:= Ini.ReadBool(DBIniSection, 'DisableAll', False);
  MultiThreading:= Ini.ReadBool(DBIniSection, 'MultiThreading', False);
  MaxReplThreads:= Ini.ReadInteger(DBIniSection, 'MaxReplThreads', 0);

  IBReplicator.BuildImplicitEnvironment(['TASK'], ['main'], False);
  ReadIBReplicatorFromIni(IBReplicator, DBIniSection);
  try
    if aReadTasks then
      begin
        try
          IBReplicator.ConfigDatabase.Open;
        except
          on E: Exception do
            IBReplicator.DBLog.Log('', lchError, sCfgDB+E.Message);
        end;
        if not IBReplicator.ConfigDatabase.Connected then Abort;

        fTasks.Clear;
        Sg:= TStringList.Create;
        try
          Ini.ReadSections(Sg);
          for I:= 0 to Sg.Count-1 do
            if Pos(TIBReplicatorTask.GetSectionName(''), Sg[I]) = 1 then
            begin
              P:= TIBReplicatorTask.Create(nil);
              InsertTask(P);
              P.ReadFromIni(Copy(Sg[I], Length(TIBReplicatorTask.GetSectionName(''))+1, Length(Sg[I])));
              for J:= 0 to Tasks.Count-2 do
                if TIBReplicatorTask(Tasks[J]).Index > P.Index then
                begin
                  Tasks.Move(Tasks.Count-1, J);
                  Break;
                end;
            end;
        finally
          Sg.Free;
        end;
        if Assigned(fOnRereadIni) then
          fOnRereadIni(Self);
      end
    else
      begin
        if Tasks.Count > 0 then
        begin
          try
            IBReplicator.ConfigDatabase.Open;
          except
            on E: Exception do
              IBReplicator.DBLog.Log('', lchError, sCfgDB+E.Message);
          end;
        end;
        for I:= 0 to Tasks.Count-1 do
          with TIBReplicatorTask(Tasks[I]) do
            ReadFromIni(TaskName);
      end;
  except
    on E: Exception do
      IBReplicator.DBLog.Log('', lchError, sCfgDB+E.Message);
  end;
end;

procedure TIBReplDataModule.ReadIBReplicatorFromIni(aIBReplicator: TIBReplicator; const aIniSection: string);
  procedure SetFL(FL: TFileLogger);
  begin
    FL.AutoOpen:= True;
    FL.LogFlags:= [lfStamp, lfAutoCR];
    FL.MaxLineLength:= 0;
  end;
var
  S: string;
  I: Integer;
begin
  if aIBReplicator.ReplLog <> aIBReplicator.DBLog then
    aIBReplicator.DBLog.Free;
  aIBReplicator.DBLog:= nil;
  aIBReplicator.ReplLog.Free; aIBReplicator.ReplLog:= nil;
  AssignLoggers(aIBReplicator);
  SetFL(TFileLogger(aIBReplicator.ReplLog));
  SetFL(TFileLogger(aIBReplicator.DBLog));
  with TFileLogger(aIBReplicator.DBLog) do
    LogFlags:= LogFlags+[lfAggregate];
  Ini.ReadSectionValues(DBIniSection+'.Environment', aIBReplicator.Environment);
  aIBReplicator.IBUtilPath:= Ini.ReadString(DBIniSection, 'IBUtilPath', '');

  S:= '';
  CmdLine.WhereCmd:= 0;
  while GetCmdString('E:', clUpcase or clValueCase or clNext, S) do
  begin
    I:= Pos('=', S);
    if I > 1 then
    begin
      aIBReplicator.Environment.Values[Copy(S, 1, I-1)]:= Copy(S, I+1, Length(S));
    end;
  end;
  aIBReplicator.ConfigDatabasePrefix:= IniReadString(DBIniSection+'.ConfigDatabase', 'ObjPrefix', '');
  aIBReplicator.NowAsUTC:= IniReadBool(aIniSection, 'NowAsUTC', False);
  aIBReplicator.LogErrSQLCmds:= IniReadBool(aIniSection, 'DBLog.ErrSQLCmds', False);
  aIBReplicator.LogErrSQLParams:= IniReadBool(aIniSection, 'DBLog.ErrSQLParams', False);
  aIBReplicator.LogVerbose:= IniReadBool(aIniSection, 'Log.Verbose', False);
  aIBReplicator.LogName:= IniReadString(aIniSection, 'Log.Name', '');
  TFileLogger(aIBReplicator.DBLog).LogFile:= aIBReplicator.ParseStr(IniReadString(aIniSection, 'DBLog.FileName', devNull));
  TFileLogger(aIBReplicator.DBLog).MaxFileSize:= IniReadInteger(aIniSection, 'DBLog.MaxSize', 0);
  TFileLogger(aIBReplicator.DBLog).RotateCount:= IniReadInteger(aIniSection, 'DBLog.RotateCount', 0);

  if aIBReplicator.DBLog is TFileSysLogger then
    TFileSysLogger(aIBReplicator.DBLog).SourceName:= aIBReplicator.ParseStr(IniReadString(aIniSection, 'DBLog.SysLogName', ''));
  if aIBReplicator.ReplLog <> aIBReplicator.DBLog then
  begin
    TFileLogger(aIBReplicator.ReplLog).LogFile:= aIBReplicator.ParseStr(IniReadString(aIniSection, 'ReplLog.FileName', devNull));
    TFileLogger(aIBReplicator.ReplLog).MaxFileSize:= IniReadInteger(aIniSection, 'ReplLog.MaxSize', 0);
    TFileLogger(aIBReplicator.ReplLog).RotateCount:= IniReadInteger(aIniSection, 'ReplLog.RotateCount', 0);
    if aIBReplicator.ReplLog is TFileSysLogger then  // hidden, cannot set in GUI
      TFileSysLogger(aIBReplicator.ReplLog).SourceName:= aIBReplicator.ParseStr(IniReadString(aIniSection, 'ReplLog.SysLogName', ''));
  end;
  if aIBReplicator.LogName <> '' then
  begin
    TFileLogger(aIBReplicator.ReplLog).LogFlags:= TFileLogger(aIBReplicator.ReplLog).LogFlags + [lfInsertName];
    if aIBReplicator.ReplLog <> aIBReplicator.DBLog then
      TFileLogger(aIBReplicator.DBLog).LogFlags:= TFileLogger(aIBReplicator.DBLog).LogFlags + [lfInsertName];
  end;
end;

{ TIBReplicatorTask }

constructor TIBReplicatorTask.Create(aOwner: TComponent);
begin
  inherited;
  ReplDB:= TIBDatabase.Create(Self);
  ReplDB.DefaultTransaction:= TIBTransaction.Create(ReplDB);
  ReplDB.DefaultTransaction.Params.Text:= tranRW;
  ReplDB.LoginPrompt:= False;
  ReplDB.DefaultTransaction.DefaultDatabase:= ReplDB;
  ReplDB.BeforeDisconnect:= OnReplDBBeforeDisconnect;
  ReplDB.AfterDisconnect:= OnReplDBAfterDisconnect;
  fDBEvent:= TIBEvents6.Create(Self);
  fDBEvent.AutoRegister:= False;
  fDBEvent.Database:= ReplDB;
  fDBEvent.OnEventAlert:= OnDBEventAlert;
  fCron:= TCron.Create;
  fCron.ExtendedSyntax:= True;
end;

destructor TIBReplicatorTask.Destroy;
begin
  if fThread <> nil then
  begin
    ManualStop:= True;
    fThread.Terminate;
    if fThread.Suspended then
      fThread.Resume;
    fThread.WaitFor;
  end;
  while FlagReplDB > 0 do
    CloseReplDB;               // force close
  fCron.Free;
  inherited;
end;

function TIBReplicatorTask.CanReplicate(aManual: Boolean): Boolean;
begin
  Result:= False;
  if ((Disabled or fDataModule.DisableAll or not fDataModule.Enabled) and not aManual) or fDataModule.fReconnectingFlag or (fLockFlag <> 0) or (fDataModule.fLockFlag <> 0) or IsRunning or (csDestroying in ComponentState) then
    Exit;
  Result:= True;
end;

procedure TIBReplicatorTask.DoReplicate;
var
  N: Integer;
begin
  N:= fDataModule.RunningCount;
  if (not fDataModule.MultiThreading and (N > 0)) or
     (fDataModule.MultiThreading and (fDataModule.MaxReplThreads <> 0) and (N >= fDataModule.MaxReplThreads)) then
    Exit;
  ManualStop:= False;
  LastStarted:= Now2();
  if ForceStop then
    Exit;
  {$IFDEF REGISTRATION}
  try
    TReg_CheckRegistration(False);
  except
    on E: TReg_RegistrationException do
      begin
        ReplLog.Log(LogName, lchError, FmtT(E.Message));
        raise;
      end
    else
      raise;
  end;
  {$ENDIF}
  fThread:= TIBReplicatorThread.Create(True);
  fThread.FreeOnTerminate:= True;
  fThread.fOwner:= Self;
  if fDataModule.MultiThreading then
    begin
      {$IFDEF REGISTRATION}
      TReg_CheckPro('Multi-threading');
      {$ENDIF}
      fThread.OnTerminate:= OnTerminate;
      fThread.Resume;
    end
  else
    begin
      fThread.Execute;
      try
        fThread.Terminate;   // do not process Execute
        if fThread.Suspended then
          fThread.Resume;      // execute and finish thread by regular way to avoid memory leak
      except                   // sometimes thread is already resumed. Sometimes Resume raises error even Suspended is False
      end;
      fThread:= nil;       // avoid OnTerminate because sometimes seems Resume does not work correctly
    end;
end;

function TIBReplicatorTask.IsRunning: Boolean;
begin
  Result:= fThread <> nil;
end;

procedure TIBReplicatorTask.OnTerminate(Sender: TObject);
begin
  fThread:= nil;
end;

function TIBReplicatorTask.SectionName: string;
begin
  Result:= GetSectionName(TaskName);
end;

class function TIBReplicatorTask.GetSectionName(const aTaskName: string): string;
begin
  Result:= DBIniSection+'.Task.'+Trim(aTaskName);
end;

procedure TIBReplicatorTask.ReadFromIni(aName: string);
var
  S, S2: string;
  Q: TIBSQL;
  {$IFDEF REGISTRATION}
  I: Integer;
resourcestring
  sMultiTaskFeature = 'Task disabled. Automatic multi-tasking';
  {$ENDIF}

  function ReadArr(aName: string): TIntegerOpenArray;
  var
    S: string;
  begin
    try
      S:= Ini.ReadString(SectionName, aName, '');
      Result:= StringArrayToIntegerArray(StringToArray(S, ','));
    except
      SetLength(Result, 0);
    end;
  end;
  function Arr2Str(const Arr: TIntegerOpenArray): string;
  begin
    if (Length(Arr) = 0) or (Length(Arr) = 1) and (Arr[0]=0) then
      Result:= '*'
    else
      Result:= ArrayToString(IntegerArrayToStringArray(Arr), ',')
  end;

resourcestring
  sSchemaNotFound = 'Database #%d is not contained in required schema/group';
  sEmptyReplEventName = 'Empty "replicate on event" name, replicate on event is disabled';
begin
  ClearCachedValues;
  TaskName:= aName;
  Index:= Ini.ReadInteger(SectionName, 'Index', 0);

  ReplType:= Ini.ReadInteger(SectionName, 'ReplType', 0);

  fCron.Pattern:= ParseStr(IniReadString(SectionName, 'Scheduler.Cron', ''));
  fFileLock:= ParseStr(IniReadString(SectionName, 'Scheduler.FileLock', ''));
  MaxReplRunTime:= DecodeInterval(IniReadString(SectionName, 'Scheduler.MaxRunTime', ''));
  OfflineDir:= Format(IniReadString(SectionName, 'OfflineDir', ''), [ReplType]);

  fInterval:= DecodeInterval(IniReadString(SectionName,'Scheduler.Interval', ''));

  Disabled:= Ini.ReadBool(SectionName, 'Disabled', False);
  {$IFDEF REGISTRATION}
  if not Disabled then
  begin
    try
      for I:= 0 to fDataModule.Tasks.Count-1 do
      begin
        if (fDataModule.Tasks[I] <> Self) then
        begin
          if not TIBReplicatorTask(fDataModule.Tasks[I]).Disabled then
            TReg_CheckPro(sMultiTaskFeature);  // allow only one enabled task in non-pro version
        end;
      end;
    except
      on E: Exception do
      begin
        Disabled:= True;
        ReplLog.Log(LogName, lchError, FmtT(E.Message));
      end;
    end;
  end;
  {$ENDIF}


  SendData:= Ini.ReadBool(SectionName, 'SendData', True);
  ResendData:= Ini.ReadBool(SectionName, 'ResendData', True);
  ReceiveData:= Ini.ReadBool(SectionName, 'ReceiveData', True);
  ReplOptions:= Ini.ReadInteger(SectionName, 'ReplOptions', 0);
  ReplEventName:= Trim(IniReadString(SectionName, 'Scheduler.Event', 'REPL$REPLICATENOW'));
  ReplOnEvent:= IniReadBool(SectionName, 'Scheduler.ReplOnEvent', False);
  if ReplOnEvent and (ReplEventName = '') then
  begin
    ReplOnEvent:= False;
    DBLog.Log(LogName, lchNull, FmtT(sEmptyReplEventName));
  end;

  DBId:= 0; SchemaId:= 0; GroupId:= 0; SrcDBId:= 0; TgtDBId:= 0;
  SchemaIds:= ReadArr('SchemaId');
  GroupIds:= ReadArr('GroupId');
  SrcDBIds:= ReadArr('SrcDBId');
  TgtDBIds:= ReadArr('TgtDBId');

  BuildImplicitEnvironment(['TASK'], [TaskName], False);
  fDataModule.ReadIBReplicatorFromIni(Self, SectionName);

  case ReplType of
    0, 1:
      begin
        if Length(SrcDBIds) > 0 then
        begin
          SrcDBId:= SrcDBIds[0];
          SetLength(SrcDBIds, 1);
        end;
        if (SrcDBId = 0) and (Length(SchemaIds) > 0) and fDataModule.IBReplicator.ConfigDatabase.Connected then
        begin
          try
            SrcDbId:= fDataModule.IBReplicator.GetSourceDBId(SchemaIds[0]);
          except
          end;
        end;
        DBId:= SrcDBId;
      end;
    2:
      begin
        if (SrcDBId = 0) and (Length(SchemaIds) > 0) and fDataModule.IBReplicator.ConfigDatabase.Connected then
        begin
          try
            SrcDbId:= fDataModule.IBReplicator.GetSourceDBId(SchemaIds[0]);
          except
          end;
        end;
        if Length(TgtDBIds) > 0 then
        begin
          DBId:= TgtDBIds[0];
          SetLength(TgtDBIds, 1);
        end;
      end;
    3:
      begin
        if Length(TgtDBIds) > 0 then
        begin
          DBId:= TgtDBIds[0];
          SetLength(TgtDBIds, 1);
        end;
      end;
    4:
      begin
        SetLength(SrcDBIds, 0);
        if (Length(SchemaIds) > 0) and fDataModule.IBReplicator.ConfigDatabase.Connected then
        begin
          try
            DbId:= fDataModule.IBReplicator.GetSourceDBId(SchemaIds[0]);
          except
          end;
        end;
      end;
  end;


  if Length(GroupIds) > 0 then
    GroupId:= GroupIds[0];
  if Length(SchemaIds) > 0 then
    if (DBId <> 0) then
    begin
      if fDataModule.IBReplicator.ConfigDatabase.Connected then
      begin
        Q:= TIBSQL.Create(nil);
        try
          Q.Database:= fDataModule.IBReplicator.ConfigDatabase;
          Q.Transaction:= Q.Database.DefaultTransaction;
          if ReplType in [0..1,4] then
            S:= '='
          else
            begin
              S:= '<>';
              GroupId:= 0;
            end;
          Q.SQL.Text:= Format('SELECT SCHEMAID,GROUPID FROM %s WHERE DBID=%d AND GROUPID%s0', [FormatIdentifier(Q.Database.SQLDialect, fDataModule.IBReplicator.ConfigDatabasePrefix+'SCHEMADB'), DBId, S]);
          Q.Transaction.StartTransaction;
          try
            S:= ','+ArrayToString(IntegerArrayToStringArray(SchemaIds), ',')+',';
            S2:= ','+ArrayToString(IntegerArrayToStringArray(GroupIds), ',')+',';
            Q.ExecQuery;
            while not Q.EOF do
            begin
              if Pos(','+Q.Fields[0].asString+',', S) > 0 then
              begin
                if (ReplType in [0,1,4]) then
                begin
                  SchemaId:= Q.Fields[0].asInteger;
                  Break;
                end;
                if (ReplType in [2,3]) and (Length(GroupIds) = 0) or (Pos(','+Q.Fields[1].asString+',', S2) > 0) then
                begin
                  SchemaId:= Q.Fields[0].asInteger;
                  GroupId:= Q.Fields[1].asInteger;
                  Break;
                end;
              end;
              Q.Next;
            end;
          finally
            Q.Transaction.Commit;
          end;
        finally
          Q.Free;
        end;
      end;
      if (SchemaId = 0) or (ReplType in [2,3]) and (GroupId = 0) then
        DBLog.Log(LogName, lchNull, FmtT(Format(sSchemaNotFound, [DBId])));
    end
  else
    begin
      SchemaId:= SchemaIds[0];
    end;

  if (Length(GroupIds) = 0) and (GroupId > 0) then
  begin
    SetLength(GroupIds, 1);
    GroupIds[0]:= GroupId;
  end;
  ReplDatabaseName:= '';

  if DBId <> 0 then
  try
    ReplDatabaseName:= fDataModule.IBReplicator.GetDatabaseName(DBId);
  except
  end;

  while FlagReplDB > 0 do
  begin
    CloseReplDB;
  end;

  TraceSQL:= IsThereCmd('TRACESQL', clUpcase);

  if fDataModule.Enabled then
    try
      if ReplType <> 4 then
      begin
        if ReplOnEvent and not Disabled and not fDataModule.DisableAll then
          begin
            OpenReplDB;
            fDBEvent.Registered:= False;
            fDBEvent.Events.Clear;
            if ReplEventName  <> '' then // obsolete
            begin
              fDBEvent.Events.Add(ReplEventName);
              fDBEvent.Registered:= True;
            end;
          end
        else
          begin
            if fDBEvent.Registered then
            begin
              fDBEvent.Registered:= False;
              fDBEvent.Events.Clear;
            end;
          end;
      end;
{      if (fTimer.Interval > 0) and not fCron.IsEmpty and not Disabled and not fDataModule.DisableAll and fDataModule.INITimer.Enabled and not ReplDB.Connected then
        OpenReplDB;  }   // 2004-10-06 IB Client License optiomization
    except
      on E: Exception do
        DBLog.Log(LogName, lchError, FmtT(E.Message));
    end;
end;

procedure TIBReplicatorTask.OnDBEventAlert(Sender: TObject;
  EventName: String; EventCount: Integer; var CancelAlerts: Boolean);
begin
  if CanReplicate(False) then
    DoReplicate;
end;

procedure TIBReplicatorTask.OnReplDBBeforeDisconnect(Sender: TObject);
begin
  ManualStop:= True;
  if fThread <> nil then
  begin
    fThread.Terminate;
    if fThread.Suspended then
      fThread.Resume;
  end;
  try
    fDBEvent.UnRegisterEvents;
  except
  end;
end;

procedure TIBReplicatorTask.OpenReplDB;
resourcestring
  sOpenning = 'Source database connecting';
begin
  if FlagReplDB = 0 then
  begin
    DBLog.Log(LogName, lchNull, FmtT(sOpenning));
    AssignReplDB(fDataModule.IBReplicator, ReplDB);
  end;
  Inc(FlagReplDB);
end;

procedure TIBReplicatorTask.CloseReplDB;
begin
  if FlagReplDB > 0 then
    Dec(FlagReplDB);
  if FlagReplDB = 0 then
    begin
      try
        ReplDB.Close;
      except
        ReplDB.ForceClose;
      end;
    end
  else
    if not ReplDB.Connected then
      FlagReplDB:= 0;
end;

procedure TIBReplicatorTask.AssignReplDB;
resourcestring
  sConnectingAsAdmin = 'Connecting under admin account';
  sCannotConnectDB = 'Cannot connect to replication database';
begin
  if (SchemaId = 0) or (ReplType in [2,3]) and (GroupId = 0) then
    begin
      DBLog.Log(LogName, lchNull, FmtT(sConnectingAsAdmin));
      aIBReplicator.SetDBParams(aDB, DBId, DBProps) // log as SYSDBA, unknown SchemaId
    end
  else
    if ReplType in [0, 1, 4] then
      aIBReplicator.SetDBParams_Repl(aDB, SchemaId, 0, DBId, False, DBProps)
    else
      aIBReplicator.SetDBParams_Repl(aDB, SchemaId, GroupId, DBId, True, DBProps);
  if not aDB.Connected then
    IBReplicatorError(sCannotConnectDB, 0);
end;

procedure TIBReplicatorTask.UpdateStatusLine(Sender: TObject; T,
  aCnt: LongWord);
begin
  UpdateStatusTicks:= T;
  UpdataStatusCount:= aCnt;
  fDataModule.UpdateStatus;
end;

procedure TIBReplicatorTask.Lock;
begin
  Inc(fLockFlag);
end;

procedure TIBReplicatorTask.Unlock;
begin
  if fLockFlag > 0 then
    Dec(fLockFlag);
end;

procedure TIBReplicatorTask.OnReplDBAfterDisconnect(Sender: TObject);
resourcestring
  sDisconnected = 'Disconnected "%s"';
begin
  DBLog.Log(LogName, lchNull, FmtT(Format(sDisconnected, [ReplDatabaseName])));
end;

function TIBReplicatorTask.ForceStop: Boolean;
resourcestring
  sFileLockAcquired = 'File lock "%s" acquired';
  sMaxRunTimeElapsed = 'Max.runtime elapsed';
begin
  Result:= inherited ForceStop;
  if not Result and (fFileLock <> '') and FileExists(fFileLock) then
  begin
    DBLog.Log(LogName, lchNull, FmtT(Format(sFileLockAcquired, [fFileLock])));
    Result:= True;
  end;
  if not Result and (MaxReplRunTime > 0) and (Now2() - LastStarted > MaxReplRunTime) then
  begin
    DBLog.Log(LogName, lchNull, FmtT(sMaxRunTimeElapsed));
    Result:= True;
  end;
end;

function TIBReplicatorTask.FmtT(const S: string): string;
begin
  if (Self <> nil) and (TaskName <> '') then
    Result:= '"'+TaskName+'":'+S
  else
    Result:= S;
end;

{ TIBReplicatorThread }

procedure TIBReplicatorThread.Execute;
var
  W: Word;
  SaveLogFlags: TLogFormatFlags;
  DB: TIBDatabase;
  S: string;
  CreateDB1, CreateDB2: Boolean;

  function Fmt(const Arr: TIntegerOpenArray): string;
  begin
    if Length(Arr) = 0 then
      Result:= '*'
    else
      Result:= ArrayToString(IntegerArrayToStringArray(Arr), ',');
  end;
resourcestring
  sCfgConnected = 'Cfg connected';
  sCfgDisconnected = 'Cfg disconnected';
  sExec = 'Executing: ';
  sRepl = 'Replication: ';
  sSynch= 'Synchronization: ';
  sSchema = 'SchemaId: %s, GroupId: %s, DBId: %s/%s';
  sRepl0 = 'online: ';
  sRepl1 = 'offline-src: ';
  sRepl2 = 'offline-tgt: ';
  sRepl3 = 'online-tgt: ';
  sBadReplicationTypeSpecified = 'No valid replication type specified';
begin
  if Terminated then
    Exit;
  with fOwner do
  try
 // special db clones must be created, seems IBDatabase is not multithreaded !!!
    if not (ReplType in [0..4]) then
      raise Exception.Create(sBadReplicationTypeSpecified);

    CreateDB1:= fDataModule.MultiThreading;

    if CreateDB1 then
      ConfigDatabase:= TIBDatabase.Create(nil)
    else
      ConfigDatabase:= fDataModule.IBReplicator.ConfigDatabase;
    try

      if CreateDB1 then
      begin
        ConfigDatabase.DefaultTransaction:= TIBTransaction.Create(ConfigDatabase);
        ConfigDatabase.DefaultTransaction.Params.Text:= tranRW;
        ConfigDatabase.LoginPrompt:= False;
        ConfigDatabase.BeforeConnect:= fDataModule.IBReplicator.ConfigDatabase.BeforeConnect;
      end;

      ConfigDatabase.Open;
      try
        if CreateDB1 then
          DBLog.Log(LogName, lchNull, FmtT(sCfgConnected));  // AfterConnect calls VCL tag replicator


        CreateDB2:= CreateDB1; // or not ReplDB.Connected;   // 2004-10-06 IB Client License optimization

        if CreateDB2 then
          DB:= TIBDatabase.Create(nil)
        else
          begin
            DB:= ReplDB;    // main thread
            if ReplType <> 4 then
              OpenReplDB;
          end;
        try
          if CreateDB2 then
          begin
            DB.DefaultTransaction:= TIBTransaction.Create(DB);
            DB.DefaultTransaction.Params.Text:= tranRW;
            DB.LoginPrompt:= False;
            DB.DefaultTransaction.DefaultDatabase:= DB;
            if ReplType <> 4 then
            begin
              AssignReplDB(fOwner, DB);
              DB.Open;
            end;
          end;
          try
            OnUpdateStatus:= UpdateStatusLine;
            try
              case ReplType of
                0: S:= sRepl0;
                1: S:= sRepl1;
                2: S:= sRepl2;
                3: S:= sRepl3;
              else
                S:= '';
              end;
              if ReplType = 4 then
                S:= sSynch + S
              else
                S:= sRepl + S;

              S:= S+Format(sSchema, [Fmt(SchemaIds), Fmt(GroupIds), Fmt(SrcDBIds), Fmt(TgtDBIds)]);
              S:= sExec+S;
              ReplLog.Log(LogName, lchNull, FmtT(S));
              case ReplType of
                0, 3:
                  begin
                    if ReplType = 0 then
                      W:= ReplOptions and not repoptTargetReplication
                    else
                      W:= (ReplOptions or repoptTargetReplication) and not repoptReplicateLog;
                    ReplicateOnline(DB, DBProps, SchemaIds, GroupIds, TgtDBIds, W);
                  end;
                1:
                  begin
                    SourceOfflineBatch(DB, DBProps, SchemaIds, GroupIds, TgtDBIds, OfflineDir, ReceiveData, True, True, SendData, ResendData, ReplOptions and repoptReportToSource);
                  end;
                2:
                  begin
                    TargetOfflineBatch(DB, DBProps, SchemaIds, GroupIds, TgtDBIds, OfflineDir, ReceiveData, True, SendData, ResendData, ReplOptions);
                  end;
                4:
                  begin
                    SaveLogFlags:= TFileLogger(ReplLog).LogFlags;
                    TFileLogger(ReplLog).LogFlags:= TFileLogger(ReplLog).LogFlags-[lfStamp, lfAggregate];
                    try
                      Synchronize(SchemaId, GroupIds, TgtDBIds);
                    finally
                      TFileLogger(ReplLog).LogFlags:= SaveLogFlags;
                    end;
                  end;
              end;
            finally
              OnUpdateStatus:= nil;
            end;
          finally
            if CreateDB2 then
              DB.Close
            else
              if ReplType <> 4 then
                CloseReplDB;   // main thread
          end;
        finally
          if CreateDB2 then
            DB.Free;
        end;
      finally
        if CreateDB1 then
        begin
          ConfigDatabase.Close;
          DBLog.Log(LogName, lchNull, FmtT(sCfgDisconnected));
        end;
      end;
    finally
      LastFinished:= Now2;
      if CreateDB1 then
        ConfigDatabase.Free;
      ConfigDatabase:= nil;
    end;
  except
    on E: Exception do
      DBLog.Log(LogName, lchError, FmtT(E.Message));
  end;
end;

procedure TIBReplDataModule.StopAll;
var
  I: Integer;
resourcestring
  sStopping = 'Stopping all tasks';
begin
  if RunningCount > 0 then
    IBReplicator.DBLog.Log('', lchNull, sStopping);
  for I:= fTasks.Count-1 downto 0 do
    TIBReplicator(fTasks[I]).ManualStop:= True;
end;

function TIBReplDataModule.RunningCount: Integer;
var
  I: Integer;
begin
  Result:= 0;
  for I:= 0 to fTasks.Count-1 do
  begin
    if TIBReplicatorTask(fTasks[I]).IsRunning then
      Inc(Result);
  end;
end;

procedure TIBReplDataModule.IniTimerTimer(Sender: TObject);
var
  I: Integer;
resourcestring
  sIniConnecting = 'INI file change, connecting..';
  sIniDisconnecting = 'INI file change, disconnecting..';
  sIniReconnecting = 'INI file change, disconnecting and connecting..';
begin
  if (fLockFlag > 0) or not IniCheck then
    Exit;
  for I:= 0 to fTasks.Count-1 do
    if TIBReplicatorTask(Tasks[I]).FLockFlag > 0 then
      Exit;
  Lock;  // do not set IniTimer.Enabled:= False; // it checked in ReadFromIni
  try
    Ini.UpdateFile;
    if IBReplicator.ConfigDatabase.Connected then
      IBReplicator.ConfigDatabase.TestConnected;

    if IBReplicator.ConfigDatabase.Connected then
      begin
        if IBReplicator.ParseStr(Ini.ReadString(DBIniSection+'.ConfigDatabase', 'DatabaseName','')) <> '' then
          begin
            if (IBReplicator.ConfigDatabase.DatabaseName <> IBReplicator.ParseStr(Ini.ReadString(DBIniSection+'.ConfigDatabase', 'DatabaseName', ''))) or
               (IBReplicator.ConfigDatabase.Params.Values['user_name'] <> IBReplicator.ParseStr(Ini.ReadString(DBIniSection+'.ConfigDatabase', 'User', ''))) then
            begin
              IBReplicator.DBLog.Log('', lchNull, sIniReconnecting);
              if RunningCount > 0 then
                begin
                  if not fReconnectingFlag then
                    StopAll;
                  fReconnectingFlag:= True;
                end
              else
                begin
                  ReadFromIni(True, True);
                  fReconnectingFlag:= False;
                end;
            end;
          end
        else
          begin
            if RunningCount > 0 then
              begin
                if not fReconnectingFlag then
                  StopAll;
                fReconnectingFlag:= True;
              end
            else
              begin
                IBReplicator.DBLog.Log('', lchNull, sIniDisconnecting);
                fTasks.Clear;
                try
                  IBReplicator.ConfigDatabase.Close;
                except
                  IBReplicator.ConfigDatabase.ForceClose;
                end;
                fReconnectingFlag:= False;
                if Assigned(fOnRereadIni) then
                  fOnRereadIni(Self);
              end;
          end;
      end
    else
      if IBReplicator.ParseStr(Ini.ReadString(DBIniSection+'.ConfigDatabase', 'DatabaseName', '')) <> '' then
      begin
        ReadFromIni(True, True);
        IBReplicator.DBLog.Log('', lchNull, sIniConnecting);
      end;
  finally
    Unlock;
  end;
end;

procedure TIBReplDataModule.ScheduleTimerTimer(Sender: TObject);
var
  T: TIBReplicatorTask;
  TT: array of TIBReplicatorTask;
  I, J, K: Integer;
begin
  if (fLockFlag > 0) then
    Exit;
//  Lock;   it's called in VCL thread so it's not necessary. If locked then CanReplicate returns False
  try
    for I:= 0 to fTasks.Count-1 do
    begin
      T:= TIBReplicatorTask(fTasks[I]);
      if T.CanReplicate(False) and not T.Cron.IsEmpty and (T.Now2() > T.LastFinished+T.fInterval) and T.Cron.IsMatch(T.Now2) then
      begin
        J:= 0;
        while J < Length(TT) do
        begin
          if TT[J].LastFinished > T.LastFinished then
            Break;
          Inc(J);
        end;
        SetLength(TT, Length(TT)+1);
        for K:= Length(TT)-2 downto J do
          TT[K+1]:= TT[K];
        TT[J]:= T;
      end;
    end;

    for I:= 0 to Length(TT)-1 do
    begin
      TT[I].DoReplicate;
    end;
  finally
//    Unlock;
  end;
end;

procedure TIBReplDataModule.AssignLoggers(aIBReplicator: TIBReplicator);
begin
  with aIBReplicator do
  begin
    DBLog:= TFileLogger.Create(aIBReplicator);
    ReplLog:= TFileLogger.Create(aIBReplicator);
  end;
end;

procedure TIBReplDataModule.InsertTask;
begin
  P.fDataModule:= Self;
  P.ConfigDatabasePrefix:= IBReplicator.ConfigDatabasePrefix;
  if I = -1 then
    fTasks.Add(P)
  else
    fTasks.Insert(I, P);
end;

procedure TIBReplDataModule.Lock;
begin
  Inc(fLockFlag);
end;

procedure TIBReplDataModule.Unlock;
begin
  if fLockFlag > 0 then
    Dec(fLockFlag);
end;

procedure TIBReplDataModule.UpdateStatus;
begin
end;

function TIBReplDataModule.GetEnabled: Boolean;
begin
  Result:= IniTimer.Enabled;
end;

procedure TIBReplDataModule.SetEnabled(aEnable: Boolean);
begin
  IniTimer.Enabled:= aEnable;
  ScheduleTimer.Enabled:= aEnable;
end;

{$IFDEF LINUX}
{$IFDEF CONSOLE}

{ TTimerThread }

procedure TTimerThread.Execute;
var
  Tick: LongWord;
  function GetTickCount: LongWord;
  var
    tms: TTimes;
  begin
    Result:= times(tms)*1000 div CLK_TCK{tick->ms};
  end;
begin
  while not Terminated do
  begin
    fSignaled:= False;
    if fOwner.Enabled and (fOwner.Interval > 0) and not (csDesigning in fOwner.ComponentState) then
    begin
      Tick:= GetTickCount;
      repeat
        if LongWord(Abs(GetTickCount-Tick)) >= fOwner.Interval then
        begin
          if fOwner.Enabled and not fSignaled and not Terminated then
            Synchronize(fOwner.Timer);
          Break;
        end;
        Sleep(1);
      until fSignaled or Terminated;
    end;
    Sleep(1);
  end;
end;

{ TTimer }

constructor TTimer.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  fThread:= TTimerThread.Create(True);
  fThread.fOwner:= Self;
  fInterval := 1000;
end;

destructor TTimer.Destroy;
begin
  fThread.Terminate;
  fThread.Resume;
  fThread.WaitFor;
  inherited;
end;

procedure TTimer.SetEnabled(Value: Boolean);
begin
  if Value <> FEnabled then
  begin
    if not (csDesigning in ComponentState) then
      if Value then
        fThread.fSignaled:= True;  { starts the timer going again }
    FEnabled := Value;
    if not (csDesigning in ComponentState) then
      if Value then
        fThread.Resume
      else
        fThread.Suspend;
  end;
end;

procedure TTimer.SetInterval(Value: Cardinal);
begin
  if Value <> FInterval then
  begin
    FInterval := Value;
    if Enabled and not (csDesigning in ComponentState) then
    begin
      fEnabled:= False;
      fThread.fSignaled:= True;  { starts the timer going again }
      fEnabled:= True;
    end;
  end;
end;

procedure TTimer.Timer;
begin
  if Assigned(FOnTimer) then
  try
    FOnTimer(Self);
  except
    //Application.HandleException(Self);
  end;
end;
{$ENDIF}
{$ENDIF}

initialization
  WriteCriticalSection:= TCriticalSection.Create;
  DBIniSection:= 'IBReplicator';
  AssignIniFileName({$IFDEF LINUX}'/etc/ibrepl.conf'{$ELSE}GetProgramPath+'IBREPL.ini'{$ENDIF}, False);

//  DebugFileName:= ChangeFileExt(ParamStr(0), '.dbg');
  GetCmdString('DBG:', clUpcase or clValueCase, DebugFileName);
finalization
  WriteCriticalSection.Free;
end.
