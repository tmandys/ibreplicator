unit dg_Sort;

interface

uses
  {$IFDEF LINUX}
  Libc, QGraphics, QControls, QForms, QDialogs, QStdCtrls, QComCtrls, QExtCtrls, Qt,
  {$ELSE}
  Windows, Messages, Graphics, Controls, Forms, Dialogs, StdCtrls, ComCtrls, ExtCtrls,
  {$ENDIF}
  SysUtils, Connect, Classes;

type
  TSortDlg = class(TForm)
    OKBtn: TButton;
    Button2: TButton;
    ListBox: TListView;
    MoveUp: TButton;
    MoveDown: TButton;
    HelpBtn: TButton;
    procedure ListBoxDblClick(Sender: TObject);
    procedure MoveUpClick(Sender: TObject);
    procedure MoveDownClick(Sender: TObject);
    procedure ListBoxCompare(Sender: TObject; Item1, Item2: TListItem;
      Data: Integer; var Compare: Integer);
    procedure ListBoxKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure HelpBtnClick(Sender: TObject);
  private
    function GetSorting: string;
    procedure SetSorting(const Value: string);
    procedure CustomSort;
    { Private declarations }
  public
    property Sorting: string read GetSorting write SetSorting;
    procedure AssignFields(const aFields: string);
  end;

var
  SortDlg: TSortDlg;

implementation
uses
  dm_IBRepl, IBReplicator;

{$IFDEF LINUX}
{$R *.xfm}
{$ELSE}
{$R *.dfm}
{$ENDIF}

procedure TSortDlg.ListBoxDblClick(Sender: TObject);
begin
  if ListBox.Selected <> nil then
    if ListBox.Checkboxes then
      ListBox.Selected.Checked:= not ListBox.Selected.Checked
    else
      OkBtn.Click;
end;


procedure TSortDlg.AssignFields(const aFields: string);
var
  LI: TListItem;
  I: Integer;
  Sg: TStringOpenArray;
begin
  Sg:= StringToArray(aFields, ',');
  for I:= 0 to Length(Sg)-1 do
  begin
    LI:= ListBox.Items.Add;
    LI.Caption:= Sg[I];
    LI.Data:= Pointer(I shl 8 + I);
  end;
  CustomSort();
end;

function TSortDlg.GetSorting: string;
var
  I: Integer;
  Sg: TStringOpenArray;
  S: string;
begin
  SetLength(Sg, ListBox.Items.Count);
  for I:= 0 to ListBox.Items.Count-1 do
  begin
    if ListBox.Items[I].Checked then
      S:= '*'
    else
      S:= '';
    Sg[Integer(ListBox.Items[I].Data) shr 8]:= S+IntToStr(Integer(ListBox.Items[I].Data) and $FF);
  end;
  Result:= ArrayToString(Sg, ',');
end;

procedure TSortDlg.SetSorting(const Value: string);
var
  I, J, K, L, N, M: Integer;
  Sg: TStringOpenArray;
  S: string;
  Fl: Boolean;
begin
  M:= 0;
  Sg:= StringToArray(Value, ',');
  for I:= 0 to Length(Sg)-1 do
  begin
    S:= Trim(Sg[I]);
    Fl:= Pos('*', S) = 1;
    if Fl then
      Delete(S, 1, 1);
    N:= StrToIntDef(S, -1);
    if (N >= 0) and (N < ListBox.Items.Count) then
    begin
      for J:= 0 to ListBox.Items.Count-1 do
        if N = Integer(ListBox.Items[J].Data) and $FF then
        begin
          K:= Integer(ListBox.Items[J].Data) shr 8;
          if K <> M then
          begin
            for L:= 0 to ListBox.Items.Count-1 do
            begin
              if M > K then
                begin
                  if (Integer(ListBox.Items[L].Data) shr 8 > K) and (Integer(ListBox.Items[L].Data) shr 8 <= M) then
                    ListBox.Items[L].Data:= Pointer(Integer(ListBox.Items[L].Data)-$100);
                end
              else
                begin
                  if (Integer(ListBox.Items[L].Data) shr 8 < K) and (Integer(ListBox.Items[L].Data) shr 8 >= M) then
                    ListBox.Items[L].Data:= Pointer(Integer(ListBox.Items[L].Data)+$100);
                end;
            end;
            ListBox.Items[J].Data:= Pointer(M shl 8 + Integer(ListBox.Items[J].Data) and $FF);
          end;
          ListBox.Items[J].Checked:= Fl;
          Inc(M);

          Break;
        end;
    end;
  end;
  CustomSort();
end;

procedure TSortDlg.MoveUpClick(Sender: TObject);
begin
  if (ListBox.Selected <> nil) and (ListBox.Selected.Index > 0) then
  begin
    ListBox.Items[ListBox.Selected.Index].Data:= Pointer(Integer(ListBox.Items[ListBox.Selected.Index].Data)-$100);
    ListBox.Items[ListBox.Selected.Index-1].Data:= Pointer(Integer(ListBox.Items[ListBox.Selected.Index-1].Data)+$100);
    CustomSort();
  end;
end;

procedure TSortDlg.MoveDownClick(Sender: TObject);
begin
  if (ListBox.Selected <> nil) and (ListBox.Selected.Index < ListBox.Items.Count-1) then
  begin
    ListBox.Items[ListBox.Selected.Index].Data:= Pointer(Integer(ListBox.Items[ListBox.Selected.Index].Data)+$100);
    ListBox.Items[ListBox.Selected.Index+1].Data:= Pointer(Integer(ListBox.Items[ListBox.Selected.Index+1].Data)-$100);
    CustomSort();
  end;
end;

procedure TSortDlg.ListBoxCompare(Sender: TObject; Item1, Item2: TListItem;
  Data: Integer; var Compare: Integer);
begin
  if Integer(Item1.Data) < Integer(Item2.Data) then
    Compare:= -1
  else if Integer(Item1.Data) > Integer(Item2.Data) then
    Compare:= 1
  else
    Compare:= 0;
end;

procedure TSortDlg.ListBoxKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Shift = [ssCtrl]) and (Key = {$IFDEF LINUX}Key_Down{$ELSE}VK_DOWN{$ENDIF}) and MoveDown.Enabled then
  begin
    MoveDown.Click;
    Key:= 0;
  end;
  if (Shift = [ssCtrl]) and (Key = {$IFDEF LINUX}Key_Up{$ELSE}VK_UP{$ENDIF}) and MoveUp.Enabled then
  begin
    MoveUp.Click;
    Key:= 0;
  end;
end;

procedure TSortDlg.HelpBtnClick(Sender: TObject);
begin
  Application.{$IFDEF LINUX}ContextHelp{$ELSE}HelpContext{$ENDIF}(HelpContext);
end;

procedure TSortDlg.CustomSort;
var
  I, J: Integer;
  S: string;
begin
  for I:= 0 to ListBox.Items.Count-1 do
  begin
    S:= ListBox.Items[I].Caption;    // Kylix does not support CustomSort
    J:= Pos('. ', S);
    if J > 0 then
      Delete(S, 1 ,J+1);
    S:= Format('%.2d. ', [Integer(ListBox.Items[I].Data) shr 8 +1])+S;
    if S[1] = '0' then
      S[1]:= ' ';
    ListBox.Items[I].Caption:= S;
  end;
{$IFDEF LINUX}
//  ListBox.AlphaSort;   // does not work anyway and Sort:= True/False raises exception, MoveUp/Down feature disabled, a bug in Qt/TCustomListView
{$ELSE}
  ListBox.CustomSort(nil, 0);
{$ENDIF}
end;

end.
