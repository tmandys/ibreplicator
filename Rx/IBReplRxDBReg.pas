unit IBReplRxDBReg;

interface

uses Classes;

{ Register data aware custom controls and components }

procedure Register;

implementation

uses
  {$IFDEF LINUX}
  QRxDBComb
  {$ELSE}
  RxDBComb
  {$ENDIF}
  ;

procedure Register;
begin
  RegisterComponents('RX DBAware', [TRxDBComboBox]);
end;

end.
