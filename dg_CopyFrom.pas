(* IB Replication Suite
 * Copyright (C) 2002,2003 by Tomas Mandys-MandySoft
 *)

unit dg_CopyFrom;

interface

uses
  {$IFDEF LINUX}
  Libc, QGraphics, QForms, QControls, QStdCtrls, QButtons, 
  {$ELSE}
  Windows, Graphics, Forms, Controls, StdCtrls, Buttons, ExtCtrls,
  {$ENDIF}
  SysUtils, Classes;

type
  TCopyFromDlg = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    SchemaId: TEdit;
    GroupId: TEdit;
    RelationId: TEdit;
    HelpBtn: TButton;
    procedure HelpBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CopyFromDlg: TCopyFromDlg;

implementation

{$IFDEF LINUX}
{$R *.xfm}
{$ELSE}
{$R *.dfm}
{$ENDIF}

procedure TCopyFromDlg.HelpBtnClick(Sender: TObject);
begin
  Application.{$IFDEF LINUX}ContextHelp{$ELSE}HelpContext{$ENDIF}(HelpContext);
end;

end.
