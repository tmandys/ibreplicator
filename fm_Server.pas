(* IB Replication Suite
 * Copyright (C) 2002,2003 by Tomas Mandys-MandySoft
 *)

unit fm_Server;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, Terminal, ExtCtrls, ToolWin, ActnList, ImgList, Menus,
  IniFiles, IB, Connect, Db, IBDatabase, IBSQL, MultiInst, ft_Form,
  IBCustomDataSet, DBCtrls, Grids, DBGrids, IBQuery, SysTray, IBReplicator, ShellAPI,
  dm_IBRepl, SynEdit, SynMemo, SynEditHighlighter, IBDataSet2,
  SynHighlighterXML;

type
  TIBReplicationServerForm = class(TAppForm)
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Setup1: TMenuItem;
    Edit1: TMenuItem;
    Clear1: TMenuItem;
    ImageList1: TImageList;
    ActionList1: TActionList;
    SetupAction: TAction;
    ExitAction: TAction;
    ClearAction: TAction;
    ToolBar1: TToolBar;
    ToolButton8: TToolButton;
    ToolButton7: TToolButton;
    ToolButton6: TToolButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    ReplLog: TTerminal;
    TabSheet2: TTabSheet;
    Panel2: TPanel;
    DBLog: TTerminal;
    ReplicateAction: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    Process1: TMenuItem;
    UpdateTimer: TTimer;
    MultiInstance1: TMultiInstance;
    StatusBar: TStatusBar;
    StatAction: TAction;
    Reccount1: TMenuItem;
    ToolButton3: TToolButton;
    TabSheet4: TTabSheet;
    Panel3: TPanel;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    ToolButton4: TToolButton;
    DBNavigator1: TDBNavigator;
    IBTransaction1: TIBTransaction;
    ReplManQuery: TIBDataSet;
    DisableAction: TAction;
    Disablereplication1: TMenuItem;
    AboutAction: TAction;
    HomePageAction: TAction;
    Help1: TMenuItem;
    N1: TMenuItem;
    About1: TMenuItem;
    Homepage1: TMenuItem;
    SysTray1: TSysTray;
    SysTrayPopupMenu: TPopupMenu;
    Disabletimerevent1: TMenuItem;
    Exit1: TMenuItem;
    ReplicateMenuItem: TMenuItem;
    Setup2: TMenuItem;
    N2: TMenuItem;
    StopAction: TAction;
    Stop1: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    Stop2: TMenuItem;
    RestoreAction: TAction;
    N5: TMenuItem;
    Restore1: TMenuItem;
    TabSheet5: TTabSheet;
    Panel4: TPanel;
    DBGrid2: TDBGrid;
    TransferQuery: TIBDataSet;
    IBTransaction2: TIBTransaction;
    DataSource2: TDataSource;
    TransferQueryTRANSFERID: TIntegerField;
    TransferQuerySTATUS: TIBStringField;
    TransferQuerySCHEMAID: TIntegerField;
    TransferQueryGROUPID: TIntegerField;
    TransferQueryDBID: TIntegerField;
    TransferQuerySTAMP: TDateTimeField;
    TransferQuerySTAMP_SENT: TDateTimeField;
    TransferQuerySTAMP_REC: TDateTimeField;
    TransferQuerySTAMP_PROC: TDateTimeField;
    TransferQueryTRANSFERID_ACK: TIntegerField;
    ClearCacheAction: TAction;
    Clearcachedsettings1: TMenuItem;
    HelpContentsAction: TAction;
    Contents1: TMenuItem;
    N6: TMenuItem;
    ReplLogQuery: TIBDataSet;
    DataSource3: TDataSource;
    TabSheet3: TTabSheet;
    DBGrid3: TDBGrid;
    Splitter1: TSplitter;
    ReplFieldQuery: TIBDataSet;
    DataSource4: TDataSource;
    Splitter2: TSplitter;
    PageControl2: TPageControl;
    TabSheet6: TTabSheet;
    TabSheet7: TTabSheet;
    DBMemo1: TDBMemo;
    DBGrid5: TDBGrid;
    ReplFieldQuerySEQID: TIntegerField;
    ReplFieldQueryMODE: TIBStringField;
    ReplFieldQueryFIELDID: TIntegerField;
    ReplFieldQueryDATATYPE: TIBStringField;
    ReplFieldQueryDATA_N: TIntegerField;
    ReplFieldQueryDATA_B: TBlobField;
    ReplFieldQueryDATA_I: TIntegerField;
    ReplFieldQueryDATA_D: TFloatField;
    ReplFieldQueryDATA_T: TDateTimeField;
    ReplFieldQueryDATA_S: TIBStringField;
    TabSheet8: TTabSheet;
    NewTaskAction: TAction;
    EditTaskAction: TAction;
    DeleteTaskAction: TAction;
    New1: TMenuItem;
    Edit2: TMenuItem;
    Delete1: TMenuItem;
    N7: TMenuItem;
    Run1: TMenuItem;
    TabSheet0: TTabSheet;
    TaskList: TListView;
    StopAllAction: TAction;
    ImageList2: TImageList;
    Stopall1: TMenuItem;
    ToolButton5: TToolButton;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    MoveTaskUpAction: TAction;
    MoveTaskDownAction: TAction;
    N8: TMenuItem;
    Moveup1: TMenuItem;
    Movedown1: TMenuItem;
    CurName: TLabel;
    ToolButton13: TToolButton;
    SynXMLSyn1: TSynXMLSyn;
    ConflictXML: TSynMemo;
    DisableINICheckingAction: TAction;
    DisableINIchecking1: TMenuItem;
    ReplExtFileQuery: TIBDataSet;
    DataSource5: TDataSource;
    ReplExtFileQuerySCHEMAID: TIntegerField;
    ReplExtFileQuerySEQID: TIntegerField;
    ReplExtFileQueryFIELDID: TIntegerField;
    ReplExtFileQueryNAME: TIBStringField;
    ReplExtFileQuerySTAMP: TDateTimeField;
    ReplExtFileQueryDATA: TBlobField;
    TabSheet9: TTabSheet;
    DBGrid6: TDBGrid;
    PageControl3: TPageControl;
    TabSheet10: TTabSheet;
    DBGrid7: TDBGrid;
    TabSheet11: TTabSheet;
    DBGrid8: TDBGrid;
    SupportAction: TAction;
    Supportforum1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure ClearActionExecute(Sender: TObject);
    procedure SetupActionExecute(Sender: TObject);
    procedure ReplicateActionExecute(Sender: TObject);
    procedure ReplicateActionIExecute(Sender: TObject);
    procedure UpdateTimerTimer(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure StatActionExecute(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure DBNavigator1Click(Sender: TObject; Button: TNavigateBtn);
    procedure ReplManQueryAfterOpen(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure DisableActionExecute(Sender: TObject);
    procedure AboutActionExecute(Sender: TObject);
    procedure HomePageActionExecute(Sender: TObject);
    procedure SupportActionExecute(Sender: TObject);
    procedure MultiInstance1ForceClose(Sender: TObject);
    procedure MultiInstance1GetAppId(Sender: TObject; var fAppId: Integer);
    procedure StopActionExecute(Sender: TObject);
    procedure RestoreActionExecute(Sender: TObject);
    procedure SysTray1DblClick(Sender: TObject);
    procedure ExitActionExecute(Sender: TObject);
    procedure ClearCacheActionExecute(Sender: TObject);
    procedure HelpContentsActionExecute(Sender: TObject);
    procedure ReplFieldQueryBeforeOpen(DataSet: TDataSet);
    procedure ReplExtFileQueryBeforeOpen(DataSet: TDataSet);
    procedure ReplFieldQueryDATA_NGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure NewTaskActionExecute(Sender: TObject);
    procedure EditTaskActionExecute(Sender: TObject);
    procedure DeleteTaskActionExecute(Sender: TObject);
    procedure StopAllActionExecute(Sender: TObject);
    procedure TaskListSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure TaskListDblClick(Sender: TObject);
    procedure MoveTaskUpActionExecute(Sender: TObject);
    procedure DataSource1DataChange(Sender: TObject; Field: TField);
    procedure DisableINICheckingActionExecute(Sender: TObject);
  private
    CurIBReplicator: TIBReplicatorTask;
    fUpdateLock: Byte;
    procedure StatusLineOnHint(Sender: TObject);
    procedure RereadIni;
    procedure OnException(Sender: TObject; E: Exception);
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    procedure UpdateTaskItem(I: Integer; aFull: Boolean);
    procedure OnRereadTaskList(Sender: TObject);
    procedure RefreshReplicateMenuItems;
    procedure WriteIndicesToIni;
  public
    { Public declarations }
  end;

var
  IBReplicationServerForm: TIBReplicationServerForm;

implementation
uses
  {$IFDEF VER150}Variants,{$ENDIF}{$IFDEF VER140}Variants,{$ENDIF}
  dg_ServerSettings, dg_ServerTaskSettings, CmdLine, dg_About, Math, FileCtrl, Transfer,
  AuxProj, dm_IBRepl_Server
  {$IFDEF REGISTRATION}, IBReplicatorDeploy{$IFNDEF LINUX}, Registry{$ENDIF}{$ENDIF};

{$IFNDEF LINUX}
{$R *.dfm}
{$ELSE}
{$R *.xfm}   // causes problem in D7 if mentioned above *.dfm
{$ENDIF}

{$IFDEF REGISTRATION}
{$I RegClass.inc}
{$I RegIBReplicator.inc}
{$I RegCheckReg.inc}
{$I RegIBReplicatorCheckPro.inc}
{$ENDIF}

procedure TIBReplicationServerForm.FormCreate(Sender: TObject);
resourcestring
  sProfessional = ' Professional';
{$IFDEF REGISTRATION}
  sStandard = ' Standard [ purchase Professional ]';
  sProfessional2 = ' [%d day(s) remaining]';
var
  ProF: Integer;
{$ENDIF}
begin
  inherited;
  DecimalSeparator:= '.';    // multi-thread replication
  ThousandSeparator:= ',';
  IBReplicationServerForm:= Self;
  Application.CreateForm(TIBReplDataModule2, IBReplDataModule);  // I need TTerminals!!!
  IBReplDataModule.OnRereadIni:= OnRereadTaskList;

  Application.OnHint:= StatusLineOnHint;
  Application.OnException:= OnException;
  {$IFDEF REGISTRATION}                 
  try
    TReg_CheckRegistration(False);
  except
    on E: Exception do
      MessageDlg(E.Message, mtError, [mbOk], 0);
  end;
  try
    ProF:= TReg_CheckPro('');
    if ProF > 0 then
      begin
        Caption:= Caption+sProfessional;
        if ProF < MaxInt then
          Caption:= Caption + Format(sProfessional2, [ProF]);
      end
    else
      Caption:= Caption+sStandard;
  except
  end;
  {$ELSE}
    Caption:= Caption+sProfessional;
  {$ENDIF}

  IBReplDataModule.AssignLoggers(IBReplDataModule.IBReplicator);

  UpdateTimer.Enabled:= True;
  SysTray1.Visible:= Ini.ReadBool(DBIniSection+DBIniSection2, 'ShowOnSysTray', False);
  if IsThereCmd('MIN', clUpCase) or Ini.ReadBool(DBIniSection+DBIniSection2, 'RunMinimized', False) then
  begin
    if SysTray1.Visible then
      Application.ShowMainForm:= False
    else
      WindowState:= wsMinimized;
  end;
  PageControl1.ActivePageIndex:= 0;
  PageControl2.ActivePageIndex:= 0;
  PageControl3.ActivePageIndex:= 0;
  DisableAction.Checked:= IsThereCmd('DISABLED', clUpcase);
  DisableINICheckingAction.Checked:= not IBReplDataModule.IniCheck;
  DisableINICheckingAction.Enabled:= not DisableAction.Checked;
  try
    RereadIni;
    IBReplDataModule.Enabled:= not DisableAction.Checked;
    IBReplDataModule.ReadFromIni(True, True);
  except
    Application.HandleException(nil);
  end;
end;

procedure TIBReplicationServerForm.FormShow(Sender: TObject);
begin
//
end;

procedure TIBReplicationServerForm.FormDestroy(Sender: TObject);
begin
  UpdateTimer.Enabled:= False;
  Application.HelpCommand(HELP_QUIT,0);
end;

procedure TIBReplicationServerForm.UpdateTimerTimer(Sender: TObject);
var
  I: Integer;
  P: TIBReplicatorTask;
  Fl: Boolean;

  procedure AssignDB(Q: TIBDataSet; aDB: TIBDatabase);
  begin
    Q.Close;
    Q.Database:= aDB;
    Q.Transaction.DefaultDatabase:= aDB;
  end;
begin
  if (csDestroying in ComponentState) or (fUpdateLock > 0) then
    Exit;
  P:= nil;
  if (TaskList.Selected <> nil) then
  begin
    I:= TaskList.Selected.Index;
    if (I >= 0) and (I < IBReplDataModule.Tasks.Count) then
      P:= Pointer(IBReplDataModule.Tasks[I]);
  end;
  if P <> CurIBReplicator then
  begin
    if PageControl1.ActivePageIndex in [3,4,5] then
      PageControl1.ActivePageIndex:= 0; // close tables

    Fl:= False;   // check if CurIBReplicator still exist in task list
    for I:= 0 to IBReplDataModule.Tasks.Count -1 do
    begin
      if IBReplDataModule.Tasks[I] = CurIBReplicator then
      begin
        Fl:= True;
        Break;
      end;
    end;

    if Fl and (CurIBReplicator <> nil) then
    begin
      AssignDB(ReplLogQuery, CurIBReplicator.ReplDB);
      AssignDB(ReplManQuery, CurIBReplicator.ReplDB);
      AssignDB(ReplFieldQuery, CurIBReplicator.ReplDB);
      AssignDB(ReplExtFileQuery, CurIBReplicator.ReplDB);
      if ReplLogQuery.Transaction.InTransaction then
        ReplLogQuery.Transaction.Commit;
      AssignDB(TransferQuery, CurIBReplicator.ReplDB);
      if TransferQuery.Transaction.InTransaction then
        TransferQuery.Transaction.Commit;
    end;
    CurIBReplicator:= P;
    if CurIBReplicator <> nil then
      begin
        AssignDB(ReplLogQuery, CurIBReplicator.ReplDB);
        AssignDB(ReplManQuery, CurIBReplicator.ReplDB);
        AssignDB(ReplFieldQuery, CurIBReplicator.ReplDB);
        AssignDB(ReplExtFileQuery, CurIBReplicator.ReplDB);
        AssignDB(TransferQuery, CurIBReplicator.ReplDB);
        CurName.Caption:= CurIBReplicator.TaskName;
      end
    else
      CurName.Caption:= '';
  end;

  ClearAction.Enabled:= PageControl1.ActivePageIndex in [1..2];
  NewTaskAction.Enabled:= (PageControl1.ActivePageIndex = 0);
  EditTaskAction.Enabled:= (CurIBReplicator <> nil) {and not CurIBReplicator.IsRunning} and (PageControl1.ActivePageIndex = 0);
  DeleteTaskAction.Enabled:= (CurIBReplicator <> nil) and not CurIBReplicator.IsRunning and (PageControl1.ActivePageIndex = 0);
  MoveTaskUpAction.Enabled:= (TaskList.Items.Count > 1) and (IBReplDataModule.RunningCount = 0) and (PageControl1.ActivePageIndex = 0) and (TaskList.Selected <> nil) and (TaskList.Selected.Index > 0);
  MoveTaskDownAction.Enabled:= (TaskList.Items.Count > 1) and (IBReplDataModule.RunningCount = 0) and (PageControl1.ActivePageIndex = 0) and (TaskList.Selected <> nil) and (TaskList.Selected.Index < TaskList.Items.Count-1);

  ReplicateAction.Enabled:= (CurIBReplicator <> nil) and not CurIBReplicator.IsRunning;
  StopAction.Enabled:= (CurIBReplicator <> nil) and CurIBReplicator.IsRunning;
  StatAction.Enabled:= (CurIBReplicator <> nil) and (CurIBReplicator.ReplType in [0,1,2,3]);
  ClearCacheAction.Enabled:= (CurIBReplicator <> nil) and (CurIBReplicator.ReplType in [0,3]) and not CurIBReplicator.IsRunning;
  StopAllAction.Enabled:= IBReplDataModule.RunningCount > 0;
//  DisableAction.Checked:= IBReplDataModule.DisableAll;  

//  if (CurIBReplicator = nil) or (CurIBReplicator.ReplType in [0,1,2,3]) and (PageControl1.ActivePageIndex in [3,4,5]) then
//    PageControl1.ActivePageIndex:= 0;   // table is closed
  TabSheet3.TabVisible:= (CurIBReplicator <> nil) and (CurIBReplicator.ReplType in [0,1,2,3]);
  TabSheet4.TabVisible:= TabSheet3.TabVisible;
  TabSheet5.TabVisible:= (CurIBReplicator <> nil) and (CurIBReplicator.ReplType in [1,2]);

  StatusLineOnHint(nil);

  for I:= 0 to TaskList.Items.Count-1 do
  begin
    UpdateTaskItem(I, False);

  end;
  for I:= 0 to TaskList.Items.Count-1 do
  begin
    if ReplicateMenuItem.Count > I then
      if TIBReplicatorTask(IBReplDataModule.Tasks[I]).IsRunning then
        ReplicateMenuItem.Items[I].ImageIndex:= StopAction.ImageIndex
      else
        ReplicateMenuItem.Items[I].ImageIndex:= ReplicateAction.ImageIndex;
  end;

  if IBReplDataModule.Tasks.Count = 0 then
    TabSheet0.ImageIndex:= -1
  else
    TabSheet0.ImageIndex:= Byte(not (IBReplDataModule.DisableAll));
end;

procedure TIBReplicationServerForm.UpdateTaskItem;
const
  DisIdx = 0;
  TypeIdx = 4;
  ClockIdx = 11;
var
  B: Byte;
  S: string;
  LI: TListItem;
  IBR: TIBReplicatorTask;
  TR: TIBTransaction;
  procedure AssignI(I, Idx: Integer);
  begin
    if LI.SubItemImages[I] <> Idx then
      LI.SubItemImages[I]:= Idx;
  end;
  procedure AssignS(I: Integer; const S: string);
  begin
    if LI.SubItems[I] <> S then
      LI.SubItems[I]:= S;
  end;

begin
  LI:= TaskList.Items[I];
  while LI.SubItems.Count <= 14 do
    LI.SubItems.Add('');
  IBR:= Pointer(IBReplDataModule.Tasks[I]);
  if LI.Caption <> IBR.TaskName then
    LI.Caption:= IBR.TaskName;
  if LI.ImageIndex <> Byte(not IBR.Disabled) then
    LI.ImageIndex:= Byte(not IBR.Disabled);
  case IBR.ReplType of
    0:
      if IBR.ReplOptions and repoptReplicateLog <> 0 then
        AssignI(0, TypeIdx+3)
      else
        AssignI(0, TypeIdx+1);
    1:
      AssignI(0, TypeIdx+2);
    2:
      if IBR.ReplOptions and repoptReplicateLog <> 0 then
        AssignI(0, TypeIdx+6)
      else
        AssignI(0, TypeIdx+4);
    3:
      AssignI(0, TypeIdx+5);
    4:
      AssignI(0, TypeIdx);
  end;

  B:= Byte((IBR.ReplType in [0,1,3]) and IBR.ReplOnEvent) shl 1 + Byte(not IBR.Cron.IsEmpty);
  if B > 0 then
    AssignI(1, ClockIdx+B-1)
  else
    AssignI(1, -1);

  if IBR.IsRunning then
    AssignI(2, 14)
  else
    AssignI(2, -1);

  AssignS(3, IntToStr(IBR.DBId));
  S:= '';
  if IBR.DBId > 0 then
  begin
    try
      S:= IBR.ReplDatabaseName;
    except
    end;
  end;
  AssignS(4, S);
  AssignS(5, ArrayToString(IntegerArrayToStringArray(IBR.SchemaIds), ','));
  AssignS(6, ArrayToString(IntegerArrayToStringArray(IBR.GroupIds), ','));
  AssignS(7, ArrayToString(IntegerArrayToStringArray(IBR.SrcDBIds), ','));
  AssignS(8, ArrayToString(IntegerArrayToStringArray(IBR.TgtDBIds), ','));
  AssignS(9, IntToStr(IBR.UpdateStatusTicks div 1000));
  AssignS(10, IntToStr(IBR.UpdataStatusCount));
  if IBR.UpdateStatusTicks = 0 then
    S:= '0'
  else
    S:= FloatToStrF(IBR.UpdataStatusCount / (IBR.UpdateStatusTicks / 1000 / 60), ffGeneral, 2, 0);
  AssignS(11, S);
  if IBR.LastStarted = 0 then
    S:= ''
  else
    S:= DateTimeToStr(IBR.LastStarted);
  AssignS(12, S);
  if aFull then
  begin
    IBR.OpenReplDB;
    try
      TR:= TIBTransaction.Create(nil);
      try
        TR.DefaultDatabase:= IBR.ReplDB;
        TR.Params.Text:= tranRW;
        TR.StartTransaction;
        try
          AssignS(13, IntToStr(Integer(IBR.DBSQLRecord(IBR.ReplDB, 'SELECT COUNT(*) FROM '+TIBReplicator.FormatIdentifier(IBR.ReplDB.SQLDialect, IBR.DBProps.ObjPrefix+'LOG'), TR))));
          AssignS(14, IntToStr(Integer(IBR.DBSQLRecord(IBR.ReplDB, 'SELECT COUNT(*) FROM '+TIBReplicator.FormatIdentifier(IBR.ReplDB.SQLDialect, IBR.DBProps.ObjPrefix+'MAN'), TR))));
        finally
          TR.Rollback;
        end;
      finally
        TR.Free;
      end;
    finally
      IBR.CloseReplDB;
    end;
  end;
end;

procedure TIBReplicationServerForm.StatusLineOnHint;
var
  S: string;
begin
  S:= GetLongHint(Application.Hint);
  if S = '' then
    if IBReplDataModule.IBReplicator.ConfigDatabase.Connected then
      S:= IBReplDataModule.IBReplicator.ConfigDatabase.DatabaseName;
  StatusBar.Panels[0].Text:= S;
end;

procedure TIBReplicationServerForm.RereadIni;
begin
  ReplLog.MaxLines:= Ini.ReadInteger(DBIniSection+DBIniSection2, 'MaxLogLines', ReplLog.MaxLines);
  DBLog.MaxLines:= ReplLog.MaxLines;

  Application.Title:= Caption;
  SysTray1.Hint:= Application.Title;

  UpdateTimerTimer(nil);
end;

procedure TIBReplicationServerForm.ClearActionExecute(Sender: TObject);
begin
  if PageControl1.ActivePageIndex = 1 then
    ReplLog.Lines.Clear
  else
    DBLog.Lines.Clear;
end;

procedure TIBReplicationServerForm.ReplicateActionExecute(Sender: TObject);
begin
  if CurIBReplicator.CanReplicate(True) then
    CurIBReplicator.DoReplicate;
end;

procedure TIBReplicationServerForm.ReplicateActionIExecute(Sender: TObject);
var
  MI: TMenuItem;
  I: Integer;
begin
  MI:= TMenuItem(Sender);
  I:= MI.Parent.IndexOf(MI);
  if I >= 0 then
  begin
    with TIBReplicatorTask(IBReplDataModule.Tasks[I]) do
      if IsRunning then
        ManualStop:= True
      else
        begin
          if CurIBReplicator.CanReplicate(True) then
            CurIBReplicator.DoReplicate;
        end;
  end;
end;

procedure TIBReplicationServerForm.OnException(Sender: TObject;
  E: Exception);
var
  S: string;
begin
  if not (csDestroying in ComponentState) then
  begin
    if Sender = nil then
      S:= '=nil'
    else if Sender is TComponent then
      S:= '='''+TComponent(Sender).Name+''''
    else
      S:= ' is '+Sender.ClassName;
    IBReplDataModule.IBReplicator.DBLog.Log('', lchError, Format('TIBReplicationServerForm.OnException(Sender%s; E.Message:''%s'')', [S, E.Message]));
  end;
end;

procedure TIBReplicationServerForm.StatActionExecute(Sender: TObject);
begin
  UpdateTaskItem(TaskList.Selected.Index, True);
end;

procedure TIBReplicationServerForm.PageControl1Change(Sender: TObject);
var
  S: string;
begin
  if PageControl1.ActivePageIndex = 3 then
    begin
      DBNavigator1.VisibleButtons:= DBNavigator1.VisibleButtons + [nbDelete]-[nbInsert];
      DBNavigator1.DataSource:= DataSource3;
      CurIBReplicator.OpenReplDB;
      try
        if not ReplLogQuery.Transaction.InTransaction then
        begin
          ReplLogQuery.Transaction.Params.Text:= tranRW;
          ReplLogQuery.Transaction.StartTransaction;
        end;
        ReplLogQuery.SelectSQL[1]:= TIBReplicator.FormatIdentifier(ReplLogQuery.Database.SQLDialect, CurIBReplicator.DBProps.ObjPrefix+'LOG')+' M INNER JOIN '+TIBReplicator.FormatIdentifier(ReplLogQuery.Database.SQLDialect, CurIBReplicator.DBProps.ObjPrefix+'CONFIG')+' S';
        S:= IntegerArrayToSQLCondition(CurIBReplicator.SchemaIds, 'M.SCHEMAID');
        if CurIBReplicator.GroupId <> 0 then
        begin
          if S <> '' then
            S:= S + ' AND ';
          S:= S + IntegerArrayToSQLCondition(CurIBReplicator.GroupIds, 'M.GROUPID');
        end;
        if S <> '' then
          S:= 'WHERE '+S;
//          if CurIBReplicator.TgtDBID > 0 then
//            ReplLogQuery.SelectSQL[3]:= ReplLogQuery.SelectSQL[3] + Format(' AND M.DBID=%d', [CurIBReplicator.TgtDBID]);   // DBMASK
        ReplLogQuery.SelectSQL[3]:= S;
        ReplLogQuery.DeleteSQL[1]:= TIBReplicator.FormatIdentifier(ReplLogQuery.Database.SQLDialect, CurIBReplicator.DBProps.ObjPrefix+'LOG_DELETE');
        ReplLogQuery.RefreshSQL[1]:= TIBReplicator.FormatIdentifier(ReplLogQuery.Database.SQLDialect, CurIBReplicator.DBProps.ObjPrefix+'LOG');
        ReplLogQuery.Open;
        ReplFieldQuery.Close;
        ReplFieldQuery.DataSource:= DataSource3;
        ReplFieldQuery.Open;
        ReplExtFileQuery.Close;
        ReplExtFileQuery.DataSource:= DataSource3;
        ReplExtFileQuery.Open;
      except
        on E: Exception do
        begin
          CurIBReplicator.CloseReplDB;
          CurIBReplicator.DBLog.Log(CurIBReplicator.LogName, lchError, E.Message);
        end;
      end;
    end
  else if ReplLogQuery.Active or ReplManQuery.Active then
    begin
      ReplLogQuery.Close;
      ReplLogQuery.Transaction.CommitRetaining;
      if PageControl1.ActivePageIndex <> 4 then
        CurIBReplicator.CloseReplDB;
    end;
  if PageControl1.ActivePageIndex = 4 then
    begin
      DBNavigator1.VisibleButtons:= DBNavigator1.VisibleButtons + [nbInsert,nbDelete];
      DBNavigator1.DataSource:= DataSource1;
      CurIBReplicator.OpenReplDB;
      try
        if not ReplManQuery.Transaction.InTransaction then
        begin
          ReplManQuery.Transaction.Params.Text:= tranRW;
          ReplManQuery.Transaction.StartTransaction;
        end;
        ReplManQuery.SelectSQL[1]:= TIBReplicator.FormatIdentifier(ReplManQuery.Database.SQLDialect, CurIBReplicator.DBProps.ObjPrefix+'MAN')+' M INNER JOIN '+TIBReplicator.FormatIdentifier(ReplManQuery.Database.SQLDialect, CurIBReplicator.DBProps.ObjPrefix+'CONFIG')+' S';
        S:= IntegerArrayToSQLCondition(CurIBReplicator.SchemaIds, 'M.SCHEMAID');
        if CurIBReplicator.GroupId <> 0 then
        begin
          if S <> '' then
            S:= S + ' AND ';
          S:= S + IntegerArrayToSQLCondition(CurIBReplicator.GroupIds, 'M.GROUPID');
        end;
//          if CurIBReplicator.TgtDBID > 0 then
//            ReplManQuery.SelectSQL[3]:= ReplManQuery.SelectSQL[3] + Format(' AND M.DBID=%d', [CurIBReplicator.TgtDBID]);   // DBMASK
        if S <> '' then
          S:= 'WHERE '+S;
        ReplManQuery.SelectSQL[3]:= S;
        ReplManQuery.DeleteSQL[1]:= TIBReplicator.FormatIdentifier(ReplManQuery.Database.SQLDialect, CurIBReplicator.DBProps.ObjPrefix+'MAN_DELETE');
        ReplManQuery.InsertSQL[1]:= TIBReplicator.FormatIdentifier(ReplManQuery.Database.SQLDialect, CurIBReplicator.DBProps.ObjPrefix+'MAN_DELETE');
//        ReplManQuery.InsertSQL[4]:= TIBReplicator.FormatIdentifier(ReplManQuery.Database.SQLDialect, CurIBReplicator.DBProps.ObjPrefix+'MAN');
        ReplManQuery.RefreshSQL[1]:= TIBReplicator.FormatIdentifier(ReplManQuery.Database.SQLDialect, CurIBReplicator.DBProps.ObjPrefix+'MAN');
        ReplManQuery.Open;
        ReplFieldQuery.Close;
        ReplFieldQuery.DataSource:= DataSource1;
        ReplFieldQuery.Open;
        ReplExtFileQuery.Close;
        ReplExtFileQuery.DataSource:= DataSource1;
        ReplExtFileQuery.Open;
      except
        on E: Exception do
        begin
          CurIBReplicator.CloseReplDB;
          CurIBReplicator.DBLog.Log(CurIBReplicator.LogName, lchError, E.Message);
        end;
      end;
    end
  else if ReplLogQuery.Active or ReplManQuery.Active then
    begin
      ReplManQuery.Close;
      ReplManQuery.Transaction.CommitRetaining;
      if PageControl1.ActivePageIndex <> 3 then
        CurIBReplicator.CloseReplDB;
    end;
  if PageControl1.ActivePageIndex = 5 then
    begin
      DBNavigator1.VisibleButtons:= DBNavigator1.VisibleButtons - [nbInsert,nbDelete];
      DBNavigator1.DataSource:= DataSource2;
      CurIBReplicator.OpenReplDB;
      try
        if not TransferQuery.Transaction.InTransaction then
        begin
          TransferQuery.Transaction.Params.Text:= tranRW;
          TransferQuery.Transaction.StartTransaction;
        end;
        TransferQuery.SelectSQL[1]:= TIBReplicator.FormatIdentifier(TransferQuery.Database.SQLDialect, CurIBReplicator.DBProps.ObjPrefix+'TRANSFER');
        S:= IntegerArrayToSQLCondition(CurIBReplicator.SchemaIds, 'SCHEMAID');
        if CurIBReplicator.GroupId <> 0 then
        begin
          if S <> '' then
            S:= S + ' AND ';
          S:= S + IntegerArrayToSQLCondition(CurIBReplicator.GroupIds, 'GROUPID');
        end;
        if CurIBReplicator.TgtDBId <> 0 then
        begin
          if S <> '' then
            S:= S + ' AND ';
          S:= S + IntegerArrayToSQLCondition(CurIBReplicator.TgtDBIds, 'DBID');
        end;
        if S <> '' then
          S:= 'WHERE '+S;
        TransferQuery.SelectSQL[2]:= S;
        TransferQuery.ModifySQL[1]:= TransferQuery.SelectSQL[1];
        TransferQuery.RefreshSQL[1]:= TransferQuery.SelectSQL[1];
        TransferQuery.Open;
      except
        on E: Exception do
        begin
          CurIBReplicator.CloseReplDB;
          CurIBReplicator.DBLog.Log(CurIBReplicator.LogName, lchError, E.Message);
        end;
      end;
    end
  else if TransferQuery.Active then
    begin
      TransferQuery.Close;
      TransferQuery.Transaction.Commit;
      CurIBReplicator.CloseReplDB;
    end;
end;

procedure TIBReplicationServerForm.DBNavigator1Click(Sender: TObject;
  Button: TNavigateBtn);
var
  Q: TIBDataSet2;
  F: Boolean;
begin
  Q:= Pointer(DBNavigator1.DataSource.DataSet);
  if Button = nbPost then
  begin
    Q.Transaction.CommitRetaining;
    Abort;
  end else if Button = nbCancel then
  begin
    Q.Transaction.RollbackRetaining;
    Abort;
  end else if Button = nbRefresh then
  begin
    F:= ReplFieldQuery.Active;
    Q.Close;
    Q.Transaction.Commit;
    Q.Transaction.StartTransaction;
    Q.Open;
    ReplFieldQuery.Active:= F;
    ReplExtFileQuery.Active:= F;
    Abort;
  end else if Button = nbInsert then
  begin
    Q.CheckBrowseMode; // cancel insert
    Q.QInsert.Params.ByName('SEQID').asInteger:= Q.FieldByName('SEQID').asInteger;
    Q.QInsert.Params.ByName('SCHEMAID').asInteger:= Q.FieldByName('SCHEMAID').asInteger;
    Q.QInsert.ExecQuery;
    Q.Close;
    Q.Open;
    Abort;
  end;
end;

procedure TIBReplicationServerForm.ReplManQueryAfterOpen(
  DataSet: TDataSet);
var
  I: Integer;
begin
  DataSet.Fields[0].DisplayWidth:= 20;
  for I:= 0 to DataSet.FieldCount-1 do
  begin
    if DataSet.Fields[I].IsBlob then
      DataSet.Fields[I].Visible:= False;
    if DataSet.Fields[I].DisplayWidth > 60 then
      DataSet.Fields[I].DisplayWidth:= 60;
  end;
end;

procedure TIBReplicationServerForm.DisableActionExecute(Sender: TObject);
begin
  DisableAction.Checked:= not DisableAction.Checked;
  IBReplDataModule.Enabled:= not DisableAction.Checked;
  IBReplDataModule.ReadFromIni(False, False);
  DisableINICheckingAction.Enabled:= not DisableAction.Checked;
end;

procedure TIBReplicationServerForm.AboutActionExecute(Sender: TObject);
begin
  with TAboutDlg.Create(nil) do
  try
    ShowModal;
  finally  
    Release;
  end;
end;

procedure TIBReplicationServerForm.HomePageActionExecute(Sender: TObject);
begin
  CheckShellExecute(ShellExecute(0, 'open', PChar(sHomePage), '', '', SW_SHOWNORMAL));
end;

procedure TIBReplicationServerForm.SupportActionExecute(Sender: TObject);
begin
  CheckShellExecute(ShellExecute(0, 'open', PChar(sSupport), '', '', SW_SHOWNORMAL));
end;

procedure TIBReplicationServerForm.MultiInstance1ForceClose(
  Sender: TObject);
begin
  if not Ini.ReadBool(DBIniSection+DBIniSection2, 'OnlyOneInstance', False) then
    Abort;
end;

procedure TIBReplicationServerForm.MultiInstance1GetAppId(Sender: TObject;
  var fAppId: Integer);
begin
  fAppId:= Ini.ReadInteger(DBIniSection+DBIniSection2, 'InstanceId', fAppId);
end;

procedure TIBReplicationServerForm.StopActionExecute(Sender: TObject);
begin
  CurIBReplicator.ManualStop:= True;
end;

procedure TIBReplicationServerForm.RestoreActionExecute(Sender: TObject);
begin
  Application.Restore;
  Show;
  BringToFront;
end;

procedure TIBReplicationServerForm.SysTray1DblClick(Sender: TObject);
begin
  RestoreAction.Execute;
end;

procedure TIBReplicationServerForm.ExitActionExecute(Sender: TObject);
begin
  Close;
end;

procedure TIBReplicationServerForm.WMSysCommand(
  var Message: TWMSysCommand);
begin
  with Message do
  begin
    if Ini.ReadBool(DBIniSection+DBIniSection2, 'ShowOnSysTray', False) then
      if (CmdType and $FFF0 = SC_MINIMIZE) then
      begin
        Application.ShowMainForm:= False;
        Hide;
        Exit;
      end;
    inherited;
  end;
end;

procedure TIBReplicationServerForm.ClearCacheActionExecute(
  Sender: TObject);
begin
  CurIBReplicator.ClearCachedValues;
end;

procedure TIBReplicationServerForm.HelpContentsActionExecute(
  Sender: TObject);
begin
  Application.HelpCommand(HELP_CONTENTS, 0);
end;

procedure TIBReplicationServerForm.ReplFieldQueryBeforeOpen(
  DataSet: TDataSet);
begin
  with TIBDataSet(DataSet) do
    SelectSQL[1]:= TIBReplicator.FormatIdentifier(Database.SQLDialect, CurIBReplicator.DBProps.ObjPrefix+'FIELD2');
end;

procedure TIBReplicationServerForm.ReplExtFileQueryBeforeOpen(
  DataSet: TDataSet);
begin
  with TIBDataSet(DataSet) do
    SelectSQL[1]:= TIBReplicator.FormatIdentifier(Database.SQLDialect, CurIBReplicator.DBProps.ObjPrefix+'EXT_FILE');
end;

procedure TIBReplicationServerForm.ReplFieldQueryDATA_NGetText(
  Sender: TField; var Text: String; DisplayText: Boolean);
var
  F: TField;
begin
  F:= Sender.DataSet.FindField('DATA_'+Sender.DataSet.FieldByName('DATATYPE').asString);
  if (F <> nil) and (F <> Sender) then
    Text:= F.asString;
end;

procedure TIBReplicationServerForm.RefreshReplicateMenuItems;
var
  I: Integer;
  MI: TMenuItem;
begin
  ReplicateMenuItem.Clear;
  for I:= 0 to IBReplDataModule.Tasks.Count-1 do
  begin
    MI:= TMenuItem.Create(Self);
    MI.Caption:= TIBReplicatorTask(IBReplDataModule.Tasks[I]).TaskName;
    MI.OnClick:= ReplicateActionIExecute;
    ReplicateMenuItem.Add(MI);
  end;
  ReplicateMenuItem.Enabled:= ReplicateMenuItem.Count > 0;
end;

procedure TIBReplicationServerForm.OnRereadTaskList(Sender: TObject);
var
  SaveN, I: Integer;
  P: TListItem;
begin
  if TaskList.Selected = nil then
    SaveN:= -1
  else
    SaveN:= TaskList.Selected.Index;

  Inc(fUpdateLock);
  try
    TaskList.Items.Clear;
    for I:= 0 to IBReplDataModule.Tasks.Count-1 do
    begin
      TaskList.Items.Add;

      UpdateTaskItem(I, False);
    end;
    RefreshReplicateMenuItems;
  finally
    Dec(fUpdateLock);
  end;

  if TaskList.Items.Count > 0 then
    begin
      P:= TaskList.Items[Min(SaveN, TaskList.Items.Count-1)];
      TaskList.Selected:= P;
    end
  else
    UpdateTimerTimer(nil);
end;

procedure TIBReplicationServerForm.NewTaskActionExecute(Sender: TObject);
var
  LI: TListItem;
  I: Integer;
  P: TIBReplicatorTask;
begin
  with TServerTaskSettingsDialog.Create(nil) do
  try
    TaskName:= '';
    if TaskList.Selected <> nil then
      I:= TaskList.Selected.Index
    else
      I:= TaskList.Items.Count;
    if ShowModal = mrOK then
    begin
      LI:= TaskList.Items.Insert(I);
      P:= TIBReplicatorTask.Create(nil);
      IBReplDataModule.InsertTask(P, LI.Index);
      P.ReadFromIni(TaskName);
      TaskList.Selected:= LI;
      RefreshReplicateMenuItems;
      UpdateTaskItem(LI.Index, False);
      WriteIndicesToIni;
    end;
  finally
    Release;
  end;
end;

procedure TIBReplicationServerForm.EditTaskActionExecute(Sender: TObject);
var
  LI: TListItem;
  P: TIBReplicatorTask;
begin
  with TServerTaskSettingsDialog.Create(nil) do
  try
    LI:= TaskList.Selected;
    P:= TIBReplicatorTask(IBReplDataModule.Tasks[LI.Index]);
    P.Lock;
    try
      TaskName:= P.TaskName;
      OKBtn.Enabled:= not P.IsRunning;
      if ShowModal = mrOK then
      begin
        P.ReadFromIni(TaskName);
        UpdateTaskItem(LI.Index, False);
        WriteIndicesToIni;
        RefreshReplicateMenuItems;
      end;
    finally
      P.Unlock;
    end;
  finally
    Release;
  end;
end;

procedure TIBReplicationServerForm.DeleteTaskActionExecute(
  Sender: TObject);
var
  LI: TListItem;
  P: TIBReplicatorTask;
resourcestring
  sDeleteTask = 'Delete task "%s" ?';
begin
  LI:= TaskList.Selected;
  P:= Pointer(IBReplDataModule.Tasks[LI.Index]);
  P.Lock;
  try
    if P.IsRunning or (MessageDlg(Format(sDeleteTask, [LI.Caption]), mtConfirmation, [mbYes, mbNo], 0) <> mrYes) then
      Abort;
    TaskList.Selected:= nil;
    P:= Pointer(IBReplDataModule.Tasks[LI.Index]);
    Ini.EraseSection(P.SectionName);
  finally
    P.Unlock;
  end;
  IBReplDataModule.Tasks.Delete(LI.Index);
  TaskList.Items.Delete(LI.Index);
  RefreshReplicateMenuItems;
  WriteIndicesToIni;
end;

procedure TIBReplicationServerForm.StopAllActionExecute(Sender: TObject);
begin
  IBReplDataModule.StopAll;
end;

procedure TIBReplicationServerForm.SetupActionExecute(Sender: TObject);
begin
  with TServerSettingsDialog.Create(nil) do
  try
    IBReplDataModule.Lock;
    try
      OKBtn.Enabled:= IBReplDataModule.RunningCount = 0;
      if ShowModal = mrOk then
      begin
        SysTray1.Visible:= ShowOnSysTray.Checked;
        if not SysTray1.Visible then
          RestoreAction.Execute;
        RereadIni;
        IBReplDataModule.ReadFromIni(False, True);
      end;
    finally
      IBReplDataModule.Unlock;
    end;
  finally
    Release;
  end;
end;

procedure TIBReplicationServerForm.TaskListSelectItem(Sender: TObject;
  Item: TListItem; Selected: Boolean);
begin
  UpdateTimerTimer(nil);
end;

procedure TIBReplicationServerForm.TaskListDblClick(Sender: TObject);
begin
  if EditTaskAction.Enabled then
    EditTaskAction.Execute
  else
    NewTaskAction.Execute;
end;

procedure TIBReplicationServerForm.MoveTaskUpActionExecute(
  Sender: TObject);
var
  I, J: Integer;
  LI: TListItem;
begin
  IBReplDataModule.Lock;
  try
    I:= TComponent(Sender).Tag;
    if (IBReplDataModule.RunningCount = 0) and (TaskList.Selected <> nil) and (I <> 0) then
    begin
      J:= TaskList.Selected.Index;
      IBReplDataModule.Tasks.Move(J, J+I);
      Inc(fUpdateLock);
      try
        if I > 0 then
          begin
            LI:= TaskList.Items.Insert(J+I+1);
            LI.Assign(TaskList.Items[J]);
            TaskList.Items.Delete(J);
          end
        else 
          begin
            LI:= TaskList.Items.Insert(J+I);
            LI.Assign(TaskList.Items[J+1]);
            TaskList.Items.Delete(J+1);
          end;
      finally
        Dec(fUpdateLock);
      end;
      TaskList.Selected:= LI;
      TaskList.ItemFocused:= LI;
      RefreshReplicateMenuItems;
      WriteIndicesToIni;
    end;
  finally
    IBReplDataModule.Unlock;
  end;
end;

procedure TIBReplicationServerForm.WriteIndicesToIni;
var
  I: Integer;
begin
  for I:= 0 to IBReplDataModule.Tasks.Count-1 do
  begin
    Ini.WriteInteger(TIBReplicatorTask(IBReplDataModule.Tasks[I]).SectionName, 'Index', I);
  end;
end;

procedure TIBReplicationServerForm.DataSource1DataChange(Sender: TObject;
  Field: TField);
var
  St: TStream;
  Xml: {Utf8}String;
begin
  St:= TStringStream.Create(ReplManQuery.FieldByName('CONFLICT').AsString);
  try
    try
      Xml:= '';
      if St.Size > 0 then
        Xml:= IBReplDataModule.IBReplicator.ConflictToXML(St, 0);
    except
      on E: Exception do
        Xml:= E.Message;
    end;
    ConflictXML.Lines.Text:= XML;
  finally
    St.Free;
  end;
end;

procedure TIBReplicationServerForm.DisableINICheckingActionExecute(
  Sender: TObject);
begin
  DisableINICheckingAction.Checked:= not DisableINICheckingAction.Checked;
  IBReplDataModule.IniCheck:= not DisableINICheckingAction.Checked;
end;

end.
