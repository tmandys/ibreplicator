const
  Reg_ApplicationKey = $CAA5;
  Reg_ApplicationName = 'Interbase Replication Suite 2.x';
  {$IFDEF SDK}
  Reg_ApplicationId = 1;
  Reg_Prefix = 'SDK';
  {$ELSE}
  Reg_ApplicationId = 0;
  Reg_Prefix = '';
  {$ENDIF}
//  Level: Word = 0;          // 0..trial, 1..full, apod. nastavi se reg.cislem
  Reg_Expiration: Word = 21;      // doba expirace ve dnech
  Reg_SecretKey = '{5F6364CE-DA1C-449D-A460-CBAD6D5CAA5}';

  Reg_Email = 'ibrepl@2p.cz';
  Reg_Homepage = 'http://www.2p.cz/en/interbase_replicator/';
  Reg_OnlineRegistration = 'http://www.2p.cz/en/interbase_replicator/registration?webform%%5Busername%%5D=%s&webform%%5Bfirm%%5D=%s&webform%%5Bregcode%%5D=%s';
  Reg_OnlineRegistration2 = ''; // deploy key
  Reg_DisableOffline = True;


resourcestring
  {$IFDEF SDK}
  Reg_sstWhatToDo = 'Please register in IDE form designer - right click on TIBReplicator component and execute command "Register..."';
  {$ELSE}
  Reg_sstWhatToDo = 'Please register using IBReplication Manager "Help/Registration..." command';
  {$ENDIF}
  Reg_sstNonRegistered = 'Software "%s" is not registered.';
  Reg_sstNonActivated = 'Without activation you may use software for %d days.'#13#10'Software is used %d days.';
  Reg_sstElapsed = 'Trial period elapsed - you cannot use software "%s" without activation. Right click on TIBReplicator component in IDE form designer and execute command "Register..."';
  Reg_sstViolation = 'Registration "%s" violation. Right click at TIBReplicator component in IDE form designer and execute command "Register..."';

function Reg_KeyFileName: string;
var
{$IFDEF CLR}
  B: StringBuilder;
{$ELSE}
  B: array[0..MAX_PATH] of Char;
{$ENDIF}
begin
  {$IFDEF CLR}
  B:= StringBuilder.Create;
  GetWindowsDirectory(B, B.Capacity);

  {$ELSE}
    {$IFDEF LINUX}
  // home dir, no access rights problem
  StrPCopy(B, GetEnvironmentVariable('HOME'));  // creat does not know tilde, B:= '~' does not work
    {$ELSE}
  GetWindowsDirectory(B, SizeOf(B));
    {$ENDIF}
  {$ENDIF}
  Result:= {$IFDEF VER120}IncludeTrailingBackSlash{$ELSE}{$IFDEF VER130}IncludeTrailingBackSlash{$ELSE}IncludeTrailingPathDelimiter{$ENDIF}{$ENDIF}(B{$IFDEF CLR}.ToString{$ENDIF})+{$IFDEF LINUX}'.'+{$ENDIF}'ibreplicator2.key';   
end;

