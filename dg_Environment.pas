unit dg_Environment;

interface

uses
  {$IFDEF LINUX}
  Libc, QGraphics, QForms, QControls, QStdCtrls, QButtons, QExtCtrls,
  {$ELSE}
  Windows, Graphics, Forms, Controls, StdCtrls, Buttons, ExtCtrls,
  {$ENDIF}
  SysUtils, Classes;


type
  TEnvironmentDialog = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    HelpBtn: TButton;
    Label1: TLabel;
    Keys: TMemo;
    procedure FormShow(Sender: TObject);
    procedure HelpBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    Context: string;
    { Public declarations }
  end;

var
  EnvironmentDialog: TEnvironmentDialog;

implementation

{$IFDEF LINUX}
{$R *.xfm}
{$ELSE}
{$R *.dfm}
{$ENDIF}

procedure TEnvironmentDialog.FormShow(Sender: TObject);
begin
  Caption:= Format(Caption, [Context]);
end;

procedure TEnvironmentDialog.HelpBtnClick(Sender: TObject);
begin
  Application.{$IFDEF LINUX}ContextHelp{$ELSE}HelpContext{$ENDIF}(HelpContext);
end;

end.
