(* IB Replication Suite
 * Copyright (C) 2002,2003 by Tomas Mandys-MandySoft
 *)

unit dm_IBReplC;

interface
uses
  {$IFDEF LINUX}
  Libc,
  {$ELSE}
  Windows,
  {$ENDIF}
  SysUtils, dm_IBRepl, Connect, Classes, IBReplicator, IBDatabase, SyncObjs;

type
  TFileLogger2 = class(TFileSysLogger)
  private
    FWriteText: Boolean;
  protected
    procedure DoLog(const aText: string); override;
  public
  end;

type
  TIBReplDataModuleC = class(TIBReplDataModule)
  private
    fLogCriticalSection: TCriticalSection;
    procedure IBCfgDBBeforeConnect(Sender: TObject);
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;

    class function GetParString(const aPar, Val: string): string;
    class function GetParInteger(const aPar: string; Val: Integer): Integer;
    procedure AssignLoggers(aIBReplicator: TIBReplicator); override;
    procedure ReadIBReplicatorFromIni(aIBReplicator: TIBReplicator; const aIniSection: string); override;
  end;


function CP(const S: string): string;
procedure WriteProgName;

procedure WriteLn2(const S: string);
procedure Write2(const S: string);

implementation
uses
  CmdLine, Math, AuxProj;

function CP(const S: string): string;
{$IFNDEF LINUX}
var
  Buff: array[1..2048] of Char;
{$ENDIF}
begin
  {$IFDEF LINUX}
  Result:= S;
  {$ELSE}
  FillChar(Buff, SizeOf(Buff), 0);
  CharToOEMBuff(PChar(S), @Buff, Min(SizeOf(Buff)-1, Length(S)));
  Result:= StrPas(@Buff);
  {$ENDIF}
end;

{$IFDEF LINUX}
procedure WriteProgName;
resourcestring
  sVersion = 'Interbase Replication Suite, version %s (Build %s), copyright (c) 2004 by MandySoft';
begin
  Writeln2(Format(sVersion, [ProductReleaseMajor, ProductReleaseMinor]));
end;
{$ELSE}
procedure WriteProgName;
type
  TTranslation = packed record
    wLanguage: Word;
    wCodePage: Word;
  end;
var
  FileName: string;
  InfoSize, Wnd: DWORD;
  VerBuf: Pointer;
//  FI: PVSFixedFileInfo;
  pC: PChar;
  VerSize: Cardinal;
  Translation: ^TTranslation;
  S: string;

  function GetVal(aName: string): string;
  var
    S: string;
  begin
    S:= Format('\StringFileInfo\%.04x%.04x\%s', [Translation.wLanguage, Translation.wCodePage, aName]);
    if VerQueryValue(VerBuf, PChar(S), Pointer(pC), VerSize) then
      Result:= StrPas(pC)
    else
      Result:= '';
  end;
resourcestring
  sVersion = '%s, version %s (Build %s), %s';
begin
  FileName := ParamStr(0);
  InfoSize := GetFileVersionInfoSize(PChar(FileName), Wnd);
  if InfoSize <> 0 then
  begin
    GetMem(VerBuf, InfoSize);
    try
      if GetFileVersionInfo(PChar(FileName), Wnd, InfoSize, VerBuf) then
        if VerQueryValue(VerBuf, '\VarFileInfo\Translation', Pointer(Translation), VerSize) then
        begin
          S:= GetVal('FileVersion');
          if Pos(GetVal('ProductVersion')+'.', S) = 1 then
            Delete(S, 1, Length(GetVal('ProductVersion')+'.'));

          WriteLn2(Format(sVersion, [GetVal('ProductName'), GetVal('ProductVersion'), S, GetVal('LegalCopyright')]));
//          WriteLn2(Format('%s', [GetVal('HomePage')]));
          WriteLn2('');
        end;

    finally
      FreeMem(VerBuf);
    end;
  end;
end;
{$ENDIF}

procedure WriteLn2(const S: string);
begin
  Write2(S+#13#10);
end;

procedure Write2(const S: string);
var
  S2: string;
begin
  if S = '' then
    Exit;
  if not IsThereCmd('SILENT', clUpcase) then
    WriteToFile('', CP(S));

  S2:= TIBReplDataModuleC.GetParString('O', '');
  if S2<>'' then
    WriteToFile(S2, S);
end;

procedure TFileLogger2.DoLog;
begin
  if FWriteText then
  begin
    if lfAutoCR in LogFlags then
      Write2(aText)
    else
      Writeln2(aText);
  end;
  inherited;
end;

constructor TIBReplDataModuleC.Create;
begin
  fLogCriticalSection:= TCriticalSection.Create;
  inherited;
  IBCfgDB.BeforeConnect:= IBCfgDBBeforeConnect;
end;

destructor TIBReplDataModuleC.Destroy;
begin
  inherited;
  fLogCriticalSection.Free;
end;


class function TIBReplDataModuleC.GetParString(const aPar, Val: string): string;
begin
  Result:= Val;
  GetCmdString(aPar+':', clUpcase or clValueCase, Result);
end;

class function TIBReplDataModuleC.GetParInteger(const aPar: string; Val: Integer): Integer;
var
  S: string;
begin
  Result:= Val;
  S:= IntToStr(Result);
  GetCmdString(aPar+':', clUpcase, S);
  Result:= StrToIntDef(S, Result);
end;

procedure TIBReplDataModuleC.IBCfgDBBeforeConnect(Sender: TObject);
begin
  inherited;
  TIBDatabase(Sender).DatabaseName:= IBReplicator.ParseStr(GetParString('D', TIBDatabase(Sender).DatabaseName));
  TIBDatabase(Sender).Params.Values['user_name']:= IBReplicator.ParseStr(GetParString('U', TIBDatabase(Sender).Params.Values['user_name']));
  TIBDatabase(Sender).Params.Values['password']:= IBReplicator.ScramblePassword(IBReplicator.ParseStr(IBReplicator.ScramblePassword(GetParString('P', TIBDatabase(Sender).Params.Values['password']), False)), False);
  TIBDatabase(Sender).SQLDialect:= GetParInteger('Q', TIBDatabase(Sender).SQLDialect);
  IBReplicator.ConfigDatabasePrefix:= GetParString('F', IBReplicator.ConfigDatabasePrefix);
  TIBDatabase(Sender).LoginPrompt:= False;
end;

procedure TIBReplDataModuleC.AssignLoggers(aIBReplicator: TIBReplicator);
var
  I: Integer;
  F: TFileLogger2;
begin
  I:= GetParInteger('V', $FF);
  F:= TFileLogger2.Create(aIBReplicator);
  F.fWriteText:= I and $02 <> 0;
  F.CriticalSection2:= fLogCriticalSection;
  aIBReplicator.DBLog:= F;

  F:= TFileLogger2.Create(aIBReplicator);
  F.fWriteText:= I and $01 <> 0;
  F.CriticalSection2:= fLogCriticalSection;
  aIBReplicator.ReplLog:= F;
end;

procedure TIBReplDataModuleC.ReadIBReplicatorFromIni(
  aIBReplicator: TIBReplicator; const aIniSection: string);
var
  S: string;
  I: Integer;
begin
  inherited;
  if aIBReplicator = IBReplicator then
  begin
    S:= GetParString('L', '');   // allow global override
    if S <> '' then
    begin
      S:= IBReplicator.ParseStr(S);
      (aIBReplicator.DBLog as TFileLogger).LogFile:= S;
      (aIBReplicator.ReplLog as TFileLogger).LogFile:= S;
    end;
    S:= GetParString('SL', '');
    if S <> '' then
    begin
      if aIBReplicator is TIBReplicatorTask then
        aIBReplicator.BuildImplicitEnvironment(['TASK'], [TIBReplicatorTask(aIBReplicator).TaskName], False);
      if aIBReplicator.DBLog is TFileSysLogger then
        TFileSysLogger(aIBReplicator.DBLog).SourceName:= aIBReplicator.ParseStr(S);
    end;
    I:= GetParInteger('LO', -1);
    if I >= 0 then
    begin
      aIBReplicator.LogErrSQLCmds:= I and 1 <> 0;
      aIBReplicator.LogErrSQLParams:= I and 2 <> 0;
    end;
  end;
end;


end.
