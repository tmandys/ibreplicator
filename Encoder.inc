(* encoder - Encoder library tools for IB Replicator
 * Copyright (C) 2003  Tomas Mandys-MandySoft
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA  02111-1307  USA
 *)

// no string parameters and exception raising posiible during DLL function

{$IFDEF IMP_CLASS}
{$UNDEF IMP_CLASS}
const
  DllName = DllPrefix+ToolName;


resourcestring
  sTransferPackageNotFound = 'Transfer package "%s" not found';

procedure FillEnvelope(var E: TOffEnvelope; SubType: string);
begin
  FillChar(E, SizeOf(E), 0);
  FillMagic(E.Magic);
  E.Magic.Envelope:= 1;
  E.EncName:= ToolName;
  E.SubType:= SubType;
end;

{$ENDIF}

{$IFDEF IMP_TYPES}
{$UNDEF IMP_TYPES}

const
  DllPrefix = 'enc_';
  DllVer = {$IFNDEF LINUX}
              {$IFDEF VER130}''{$ELSE} // D5, backward compatibility
              {$IFDEF VER140}'.140'{$ELSE} // D6
              {$IFDEF VER150}'.150'{$ELSE} // D7
              {$IFDEF VER160}'.160'{$ELSE} // D8
              {$IFDEF VER170}'.170'{$ELSE} // D2005
              {$IFDEF VER180}'.180'{$ELSE} // D2006
              ''
              {$ENDIF}{$ENDIF}{$ENDIF}{$ENDIF}{$ENDIF}{$ENDIF}
           {$ELSE}
              ''
           {$ENDIF};
  DllExt = {$IFDEF LINUX}'.so'{$ELSE}'.dll'{$ENDIF};

type   { pro dynamicke volani }
  tEncProcess = function (Encode: Boolean; EncType: PChar; Src, Tgt: TStream; Params: PChar; ErrS: PChar; BufferLength: Integer): Boolean; stdcall;

type
  TOffEnvelope = packed record
    Magic: TOffMagic;
    EncName: string[10];
    SubType: string[10];
  end;


{$ENDIF}

{$IFDEF IMP_PROCS}
{$UNDEF IMP_PROCS}
{ budou asi dynamicky loadovatelny tak je tady nanic }
function EncProcess(Encode: Boolean; EncType: PChar; Src, Tgt: TStream; Params: PChar; ErrS: PChar; BufferLength: Integer): Boolean; stdcall;

implementation

function  EncProcess; external DllName 4;

{$ENDIF}

{$IFDEF EXP_PROCS}
{$UNDEF EXP_PROCS}


exports
  EncProcess index 4;

{$ENDIF}
