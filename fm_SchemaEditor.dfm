object SchemaEditor: TSchemaEditor
  Left = 243
  Top = 127
  Width = 915
  Height = 662
  HelpContext = 30000
  VertScrollBar.Range = 48
  ActiveControl = PageControl1
  AutoScroll = False
  Caption = 'Interbase Replication Manager'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = 14
  Font.Name = 'MS Sans Serif'
  Font.Pitch = fpVariable
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = True
  Position = poDefaultPosOnly
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 120
  TextHeight = 13
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 907
    Height = 27
    AutoSize = True
    ButtonHeight = 23
    Caption = 'ToolBar1'
    EdgeBorders = [ebTop, ebBottom]
    Flat = True
    Images = ImageList1
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    object ToolButton8: TToolButton
      Left = 0
      Top = 0
      Action = CloseDatabaseAction
    end
    object ToolButton9: TToolButton
      Left = 23
      Top = 0
      Width = 8
      Caption = 'ToolButton9'
      ImageIndex = 23
      Style = tbsSeparator
    end
    object Prefix_Insert_ToolButton: TToolButton
      Left = 31
      Top = 0
      Action = DatabasesInsertAction
    end
    object Prefix_Edit_ToolButton: TToolButton
      Left = 54
      Top = 0
      Action = DatabasesEditAction
    end
    object Prefix_Delete_ToolButton: TToolButton
      Left = 77
      Top = 0
      Action = DatabasesDeleteAction
    end
    object Prefix_Sort_ToolButton: TToolButton
      Left = 100
      Top = 0
      Action = DatabasesSortAction
    end
    object ToolButton16: TToolButton
      Left = 123
      Top = 0
      Width = 8
      Caption = 'ToolButton16'
      ImageIndex = 29
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 131
      Top = 0
      Action = SchemaSourceAction
    end
    object ToolButton15: TToolButton
      Left = 154
      Top = 0
      Width = 8
      Caption = 'ToolButton15'
      ImageIndex = 29
      Style = tbsSeparator
    end
    object ToolButton14: TToolButton
      Left = 162
      Top = 0
      Action = GroupGenerateAction
    end
    object Prefix_CopyFrom_ToolButton: TToolButton
      Left = 185
      Top = 0
      Action = RelationsCopyFromAction
    end
    object ToolButton20: TToolButton
      Left = 208
      Top = 0
      Width = 8
      Caption = 'ToolButton20'
      ImageIndex = 23
      Style = tbsSeparator
    end
    object Prefix_CreateObjects_ToolButton: TToolButton
      Left = 216
      Top = 0
      Action = DatabasesCreateObjectsAction
    end
    object Prefix_CreateServerObjects_ToolButton: TToolButton
      Left = 239
      Top = 0
      Action = DatabasesCreateServerObjectsAction
    end
    object ToolButton17: TToolButton
      Left = 262
      Top = 0
      Width = 8
      Caption = 'ToolButton17'
      ImageIndex = 23
      Style = tbsSeparator
    end
    object ToolButton5: TToolButton
      Left = 270
      Top = 0
      Action = CommitAction
    end
    object ToolButton6: TToolButton
      Left = 293
      Top = 0
      Action = RollbackAction
    end
    object ToolButton2: TToolButton
      Left = 316
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 1
      Style = tbsSeparator
    end
    object ToolButton1: TToolButton
      Left = 324
      Top = 0
      Action = ClearLogAction
    end
    object StatToolButton: TToolButton
      Left = 347
      Top = 0
      Action = StatClearSchemaAction
    end
    object ToolButton4: TToolButton
      Left = 370
      Top = 0
      Width = 8
      Caption = 'ToolButton4'
      ImageIndex = 0
      Style = tbsSeparator
    end
    object ToolButton3: TToolButton
      Left = 378
      Top = 0
      Action = ConfigAction
    end
    object ToolButton7: TToolButton
      Left = 401
      Top = 0
      Width = 8
      Caption = 'ToolButton7'
      ImageIndex = 22
      Style = tbsSeparator
    end
    object DBNavigator1: TDBNavigator
      Left = 409
      Top = 0
      Width = 100
      Height = 23
      VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbRefresh]
      Flat = True
      TabOrder = 0
    end
    object ToolButton18: TToolButton
      Left = 509
      Top = 0
      Width = 8
      Caption = 'ToolButton18'
      ImageIndex = 23
      Style = tbsSeparator
    end
    object ToolButton19: TToolButton
      Left = 517
      Top = 0
      Action = HelpContentsAction
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 27
    Width = 907
    Height = 554
    ActivePage = TabSheet2
    Align = alClient
    TabOrder = 0
    OnChange = PageControl1Change
    object TabSheet1: TTabSheet
      HelpContext = 30100
      Caption = 'Log'
      object DBLog: TTerminal
        Left = 0
        Top = 0
        Width = 899
        Height = 528
        MaxLines = 100
        Align = alClient
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object TabSheet2: TTabSheet
      HelpContext = 31000
      Caption = 'Schema editor'
      ImageIndex = 1
      object Splitter3: TSplitter
        Left = 561
        Top = 0
        Width = 3
        Height = 526
        Cursor = crHSplit
      end
      object Splitter1: TSplitter
        Left = 209
        Top = 0
        Width = 3
        Height = 526
        Cursor = crHSplit
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 209
        Height = 526
        Align = alLeft
        BevelOuter = bvNone
        Caption = ' '
        TabOrder = 0
        object Splitter2: TSplitter
          Left = 0
          Top = 161
          Width = 209
          Height = 3
          Cursor = crVSplit
          Align = alTop
        end
        object Splitter4: TSplitter
          Left = 0
          Top = 305
          Width = 209
          Height = 3
          Cursor = crVSplit
          Align = alTop
        end
        object Splitter5: TSplitter
          Left = 0
          Top = 401
          Width = 209
          Height = 3
          Cursor = crVSplit
          Align = alTop
        end
        object Label1: TLabel
          Left = 0
          Top = 0
          Width = 209
          Height = 13
          Align = alTop
          Caption = '&Databases'
          FocusControl = Databases_DBGrid
        end
        object Label2: TLabel
          Left = 0
          Top = 164
          Width = 209
          Height = 13
          Align = alTop
          Caption = '&Schema'
          FocusControl = Schema_DBGrid2
        end
        object Label3: TLabel
          Left = 0
          Top = 308
          Width = 209
          Height = 13
          Align = alTop
          Caption = '&Groups'
          FocusControl = SchemaDB1_DBGrid1
        end
        object Label4: TLabel
          Left = 0
          Top = 404
          Width = 209
          Height = 13
          Align = alTop
          Caption = 'Databases &in group'
          FocusControl = SchemaDB2_DBGrid1
        end
        object Databases_DBGrid: TDBGrid
          Left = 0
          Top = 13
          Width = 209
          Height = 148
          HelpContext = 31100
          Align = alTop
          DataSource = DataSource1
          Options = [dgEditing, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit]
          PopupMenu = PopupMenu1
          ReadOnly = True
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = 14
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Pitch = fpVariable
          TitleFont.Style = []
          OnDrawColumnCell = Databases_DBGridDrawColumnCell
          OnDblClick = Databases_DBGridDblClick
          OnEnter = Databases_DBGridEnter
          OnExit = Databases_DBGridExit
          OnKeyDown = Databases_DBGridKeyDown
          OnKeyPress = Fields_DBGridKeyPress
        end
        object Schema_DBGrid2: TDBGrid
          Left = 0
          Top = 177
          Width = 209
          Height = 128
          HelpContext = 31200
          Align = alTop
          DataSource = DataSource2
          Options = [dgEditing, dgColumnResize, dgColLines, dgRowLines, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          PopupMenu = PopupMenu2
          ReadOnly = True
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = 14
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Pitch = fpVariable
          TitleFont.Style = []
          OnDrawColumnCell = Schema_DBGrid2DrawColumnCell
          OnDblClick = Schema_DBGrid2DblClick
          OnEnter = Databases_DBGridEnter
          OnExit = Databases_DBGridExit
          OnKeyDown = Databases_DBGridKeyDown
          OnKeyPress = Fields_DBGridKeyPress
        end
        object SchemaDB2_DBGrid1: TDBGrid
          Left = 0
          Top = 417
          Width = 209
          Height = 109
          HelpContext = 31400
          Align = alClient
          DataSource = DataSource4
          Options = [dgEditing, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit]
          PopupMenu = PopupMenu3
          ReadOnly = True
          TabOrder = 3
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = 14
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Pitch = fpVariable
          TitleFont.Style = []
          OnDrawColumnCell = SchemaDB2_DBGrid1DrawColumnCell
          OnDblClick = SchemaDB2_DBGrid1DblClick
          OnEnter = Databases_DBGridEnter
          OnExit = Databases_DBGridExit
          OnKeyDown = Databases_DBGridKeyDown
          OnKeyPress = Fields_DBGridKeyPress
        end
        object SchemaDB1_DBGrid1: TDBGrid
          Left = 0
          Top = 321
          Width = 209
          Height = 80
          HelpContext = 31300
          Align = alTop
          DataSource = DataSource3
          Options = [dgEditing, dgColumnResize, dgColLines, dgRowLines, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          PopupMenu = PopupMenu6
          ReadOnly = True
          TabOrder = 2
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = 14
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Pitch = fpVariable
          TitleFont.Style = []
          OnEnter = Databases_DBGridEnter
          OnExit = Databases_DBGridExit
          OnKeyDown = Databases_DBGridKeyDown
          OnKeyPress = Fields_DBGridKeyPress
        end
      end
      object Panel2: TPanel
        Left = 212
        Top = 0
        Width = 349
        Height = 526
        Align = alLeft
        BevelOuter = bvNone
        Caption = ' '
        TabOrder = 1
        object Label5: TLabel
          Left = 0
          Top = 0
          Width = 349
          Height = 13
          Align = alTop
          Caption = '&Relations'
          FocusControl = Relations_DBGrid3
        end
        object Relations_DBGrid3: TDBGrid
          Left = 0
          Top = 13
          Width = 349
          Height = 513
          HelpContext = 31500
          Align = alClient
          DataSource = DataSource5
          Options = [dgEditing, dgColumnResize, dgColLines, dgRowLines, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
          PopupMenu = PopupMenu4
          ReadOnly = True
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = 14
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Pitch = fpVariable
          TitleFont.Style = []
          OnDrawColumnCell = Relations_DBGrid3DrawColumnCell
          OnDblClick = Relations_DBGrid3DblClick
          OnEnter = Databases_DBGridEnter
          OnExit = Databases_DBGridExit
          OnKeyDown = Databases_DBGridKeyDown
          OnKeyPress = Fields_DBGridKeyPress
        end
      end
      object Panel3: TPanel
        Left = 564
        Top = 0
        Width = 335
        Height = 526
        Align = alClient
        BevelOuter = bvNone
        Caption = ' '
        TabOrder = 2
        object Label6: TLabel
          Left = 0
          Top = 0
          Width = 335
          Height = 13
          Align = alTop
          Caption = '&Fields'
          FocusControl = Fields_DBGrid4
        end
        object Fields_DBGrid4: TDBGrid
          Left = 0
          Top = 13
          Width = 335
          Height = 513
          HelpContext = 31600
          Align = alClient
          DataSource = DataSource6
          Options = [dgEditing, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit]
          PopupMenu = PopupMenu5
          ReadOnly = True
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = 14
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Pitch = fpVariable
          TitleFont.Style = []
          OnDrawColumnCell = Fields_DBGrid4DrawColumnCell
          OnDblClick = Fields_DBGrid4DblClick
          OnEnter = Databases_DBGridEnter
          OnExit = Databases_DBGridExit
          OnKeyDown = Databases_DBGridKeyDown
          OnKeyPress = Fields_DBGridKeyPress
        end
      end
    end
    object TabSheet3: TTabSheet
      HelpContext = 30200
      Caption = 'Statistics'
      ImageIndex = 2
      object Splitter6: TSplitter
        Left = 0
        Top = 250
        Width = 899
        Height = 3
        Cursor = crVSplit
        Align = alTop
      end
      object Splitter7: TSplitter
        Left = 0
        Top = 161
        Width = 899
        Height = 3
        Cursor = crVSplit
        Align = alTop
      end
      object Statistics_Relation: TDBGrid
        Left = 0
        Top = 253
        Width = 899
        Height = 275
        Align = alClient
        DataSource = DataSource9
        Options = [dgEditing, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        PopupMenu = PopupMenu9
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = 14
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Pitch = fpVariable
        TitleFont.Style = []
        OnEnter = Databases_DBGridEnter
        OnExit = Databases_DBGridExit
        OnKeyDown = Databases_DBGridKeyDown
        OnKeyPress = Fields_DBGridKeyPress
      end
      object Statistics_Group: TDBGrid
        Left = 0
        Top = 164
        Width = 899
        Height = 86
        Align = alTop
        DataSource = DataSource8
        Options = [dgEditing, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        PopupMenu = PopupMenu8
        ReadOnly = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = 14
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Pitch = fpVariable
        TitleFont.Style = []
        OnEnter = Databases_DBGridEnter
        OnExit = Databases_DBGridExit
        OnKeyDown = Databases_DBGridKeyDown
        OnKeyPress = Fields_DBGridKeyPress
      end
      object Statistics_Schema: TDBGrid
        Left = 0
        Top = 0
        Width = 899
        Height = 161
        Align = alTop
        DataSource = DataSource7
        Options = [dgEditing, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
        PopupMenu = PopupMenu7
        ReadOnly = True
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = 14
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Pitch = fpVariable
        TitleFont.Style = []
        OnEnter = Databases_DBGridEnter
        OnExit = Databases_DBGridExit
        OnKeyDown = Databases_DBGridKeyDown
        OnKeyPress = Fields_DBGridKeyPress
      end
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 581
    Width = 907
    Height = 23
    Panels = <
      item
        Width = 250
      end>
    SimplePanel = False
  end
  object MainMenu1: TMainMenu
    Images = ImageList1
    Left = 480
    Top = 56
    object Database1: TMenuItem
      Caption = 'Config.database'
      object Commit1: TMenuItem
        Action = CommitAction
      end
      object Rollback1: TMenuItem
        Action = RollbackAction
      end
      object N7: TMenuItem
        Caption = '-'
      end
      object Opendatabase1: TMenuItem
        Action = OpenDatabaseAction
      end
      object Closedatabase1: TMenuItem
        Action = CloseDatabaseAction
      end
      object Createdatabase1: TMenuItem
        Action = CreateDatabaseAction
      end
      object Upgradedatabase1: TMenuItem
        Action = UpgradeDatabaseAction
      end
      object Environment1: TMenuItem
        Action = ConfigEnvironmentAction
      end
      object N16: TMenuItem
        Caption = '-'
      end
      object Backup2: TMenuItem
        Action = BackupDatabaseAction
      end
      object Restore2: TMenuItem
        Action = RestoreDatabaseAction
      end
      object N8: TMenuItem
        Caption = '-'
      end
      object Settings1: TMenuItem
        Action = ConfigAction
      end
      object Clearlog1: TMenuItem
        Action = ClearLogAction
      end
    end
    object DatabaseItem1: TMenuItem
      Caption = 'Database'
      HelpContext = 31100
      object Insert3: TMenuItem
        Action = DatabasesInsertAction
      end
      object Edit3: TMenuItem
        Action = DatabasesEditAction
      end
      object Delete3: TMenuItem
        Action = DatabasesDeleteAction
      end
      object N12: TMenuItem
        Caption = '-'
      end
      object Environment2: TMenuItem
        Action = DatabasesEnvironmentAction
      end
      object Upgradedatabase2: TMenuItem
        Action = DatabasesUpgradeAction
      end
      object N13: TMenuItem
        Caption = '-'
      end
      object Createsystemobjects3: TMenuItem
        Action = DatabasesCreateObjectsAction
      end
      object Dropsystemobjects3: TMenuItem
        Action = DatabasesDropObjectsAction
      end
      object Droptriggers3: TMenuItem
        Action = DatabasesDropTriggersAction
      end
      object N14: TMenuItem
        Caption = '-'
      end
      object Addserveruser1: TMenuItem
        Action = DatabasesCreateServerObjectsAction
      end
      object Dropsystemobjects4: TMenuItem
        Action = DatabasesDropServerObjectsAction
      end
      object N15: TMenuItem
        Caption = '-'
      end
      object Backup1: TMenuItem
        Action = DatabasesBackupAction
      end
      object Restore1: TMenuItem
        Action = DatabasesRestoreAction
      end
      object N19: TMenuItem
        Caption = '-'
      end
      object Sort1: TMenuItem
        Action = DatabasesSortAction
      end
    end
    object SchemaItem1: TMenuItem
      Caption = 'Schema'
      HelpContext = 31200
      object Insert4: TMenuItem
        Action = SchemaInsertAction
      end
      object Edit4: TMenuItem
        Action = SchemaEditAction
      end
      object Delete4: TMenuItem
        Action = SchemaDeleteAction
      end
      object N5: TMenuItem
        Caption = '-'
      end
      object Sourcedatabase1: TMenuItem
        Action = SchemaSourceAction
      end
      object N20: TMenuItem
        Caption = '-'
      end
      object Sort2: TMenuItem
        Action = SchemaSortAction
      end
    end
    object Group1Item1: TMenuItem
      Caption = 'Group (S)'
      HelpContext = 31300
      object Insert6: TMenuItem
        Action = Group1InsertAction
      end
      object N6: TMenuItem
        Caption = '-'
      end
      object GeneratePKFKanddatafields1: TMenuItem
        Action = GroupGenerateAction
      end
      object Removeorphanedfields1: TMenuItem
        Action = RemoveOrphanedAction
      end
      object Validate1: TMenuItem
        Action = Group1ValidateAction
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object Createsystemobjects2: TMenuItem
        Action = GroupCreateObjectsAction
      end
      object Dropsystemobjects2: TMenuItem
        Action = GroupDropObjectsAction
      end
      object Droptriggers2: TMenuItem
        Action = GroupDropTriggersAction
      end
      object Cleansourcedata1: TMenuItem
        Action = GroupCleanData
      end
      object N10: TMenuItem
        Caption = '-'
      end
      object Createserverobjects1: TMenuItem
        Action = GroupCreateServerObjectsAction
      end
      object Dropserverobjects1: TMenuItem
        Action = GroupDropServerObjectsAction
      end
    end
    object GroupItem1: TMenuItem
      Caption = 'Group (T)'
      HelpContext = 31400
      object Insert5: TMenuItem
        Action = GroupInsertAction
      end
      object Edit5: TMenuItem
        Action = GroupEditAction
      end
      object Delete5: TMenuItem
        Action = GroupDeleteAction
      end
      object N23: TMenuItem
        Caption = '-'
      end
      object Validate2: TMenuItem
        Action = Group2ValidateAction
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Createsystemobjects1: TMenuItem
        Action = Group2CreateObjectsAction
      end
      object Dropsystemobjects1: TMenuItem
        Action = Group2DropObjectsAction
      end
      object Droptriggers1: TMenuItem
        Action = Group2DropTriggersAction
        Visible = False
      end
      object Cleansourcedata2: TMenuItem
        Action = Group2CleanData
      end
      object Createstoredprocedures1: TMenuItem
        Action = Group2CreateStoredProcAction
      end
      object N11: TMenuItem
        Caption = '-'
      end
      object Createserverobjects2: TMenuItem
        Action = Group2CreateServerObjectsAction
      end
      object Dropserverobjects2: TMenuItem
        Action = Group2DropServerObjectsAction
      end
      object N17: TMenuItem
        Caption = '-'
      end
      object Clone1: TMenuItem
        Action = Group2CloneSourceDatabaseAction
      end
      object Cloneempty1: TMenuItem
        Action = Group2CloneEmptySourceDatabaseAction
      end
      object N22: TMenuItem
        Caption = '-'
      end
      object Orderby1: TMenuItem
        Action = Group2SortAction
      end
    end
    object RelationItem1: TMenuItem
      Caption = 'Relation'
      HelpContext = 31500
      object Insert2: TMenuItem
        Action = RelationsInsertAction
      end
      object Edit2: TMenuItem
        Action = RelationsEditAction
      end
      object RelationsDeleteAction2: TMenuItem
        Action = RelationsDeleteAction
      end
      object N4: TMenuItem
        Caption = '-'
      end
      object Copyfromschema1: TMenuItem
        Action = RelationsCopyFromAction
      end
      object Sort3: TMenuItem
        Action = RelationsSortAction
      end
    end
    object FieldItem1: TMenuItem
      Caption = 'Field'
      HelpContext = 31600
      object Insert1: TMenuItem
        Action = FieldsInsertAction
      end
      object Edit1: TMenuItem
        Action = FieldsEditAction
      end
      object Delete1: TMenuItem
        Action = FieldsDeleteAction
      end
      object N3: TMenuItem
        Caption = '-'
      end
      object Copyfromrelation1: TMenuItem
        Action = FieldsCopyFromAction
      end
      object Sort4: TMenuItem
        Action = FieldsSortAction
      end
    end
    object Statistics1: TMenuItem
      Caption = 'Statistics'
      HelpContext = 30200
      object Clearschemastatistics1: TMenuItem
        Action = StatClearSchemaAction
      end
      object Cleargroupstatistics1: TMenuItem
        Action = StatClearGroupAction
      end
      object Clearrelationstatistics1: TMenuItem
        Action = StatClearRelationAction
      end
      object N21: TMenuItem
        Caption = '-'
      end
      object Schemastatisticsorder1: TMenuItem
        Action = StatSortSchemaAction
      end
      object Relationstatisticsorder1: TMenuItem
        Action = StatSortRelationAction
      end
    end
    object Help1: TMenuItem
      Caption = 'Help'
      object Content1: TMenuItem
        Action = HelpContentsAction
      end
      object N9: TMenuItem
        Caption = '-'
      end
      object Homepage1: TMenuItem
        Action = HomePageAction
      end
      object Supportforum1: TMenuItem
        Action = SupportAction
      end
      object Register1: TMenuItem
        Action = RegisterAction
      end
      object N18: TMenuItem
        Caption = '-'
      end
      object About1: TMenuItem
        Action = AboutAction
      end
    end
  end
  object ActionList1: TActionList
    Images = ImageList1
    Left = 512
    Top = 56
    object Group1InsertAction: TAction
      Tag = 2
      Category = 'SchemaDB1'
      Caption = 'Insert...'
      HelpContext = 31300
      Hint = 
        'Create new group (insert 1st target database for selected schema' +
        ')'
      ImageIndex = 18
      ShortCut = 45
      OnExecute = Group1InsertActionExecute
    end
    object GroupGenerateAction: TAction
      Tag = 1
      Category = 'SchemaDB1'
      Caption = 'Generate PK,FK and data fields'
      HelpContext = 31300
      Hint = 
        'Read relations from source database (only new data will be merge' +
        'd)'
      ImageIndex = 26
      OnExecute = GroupGenerateActionExecute
    end
    object FieldsInsertAction: TAction
      Tag = 2
      Category = 'Fields'
      Caption = 'Insert...'
      HelpContext = 31600
      Hint = 'Insert new field definition'
      ImageIndex = 18
      ShortCut = 45
      OnExecute = FieldsInsertActionExecute
    end
    object FieldsEditAction: TAction
      Tag = 1
      Category = 'Fields'
      Caption = 'Edit...'
      HelpContext = 31600
      Hint = 'Edit field definition'
      ImageIndex = 17
      ShortCut = 13
      OnExecute = FieldsEditActionExecute
    end
    object RemoveOrphanedAction: TAction
      Tag = 1
      Category = 'SchemaDB1'
      Caption = 'Remove orphaned fields...'
      HelpContext = 31300
      Hint = 'Remove orphaned relations and fields'
      OnExecute = RemoveOrphanedActionExecute
    end
    object GroupInsertAction: TAction
      Tag = 2
      Category = 'SchemaDB2'
      Caption = 'Insert...'
      HelpContext = 31400
      Hint = 'Insert new target database to selected group'
      ImageIndex = 18
      ShortCut = 45
      OnExecute = GroupInsertActionExecute
    end
    object GroupEditAction: TAction
      Tag = 1
      Category = 'SchemaDB2'
      Caption = 'Edit...'
      HelpContext = 31400
      Hint = 'Edit target database definition'
      ImageIndex = 17
      ShortCut = 13
      OnExecute = GroupEditActionExecute
    end
    object SchemaInsertAction: TAction
      Tag = 2
      Category = 'Schema'
      Caption = 'Insert...'
      HelpContext = 31200
      Hint = 'Insert new schema definition'
      ImageIndex = 18
      ShortCut = 45
      OnExecute = SchemaInsertActionExecute
    end
    object SchemaEditAction: TAction
      Tag = 1
      Category = 'Schema'
      Caption = 'Edit...'
      HelpContext = 31200
      Hint = 'Edit schema definition'
      ImageIndex = 17
      ShortCut = 13
      OnExecute = SchemaEditActionExecute
    end
    object CommitAction: TAction
      Caption = 'Commit'
      Enabled = False
      HelpContext = 31000
      Hint = 'Commit changes to database'
      ImageIndex = 32
      ShortCut = 16467
      OnExecute = CommitActionExecute
    end
    object RollbackAction: TAction
      Caption = 'Rollback'
      Enabled = False
      HelpContext = 31000
      Hint = 'Rollback (discard) changes'
      ImageIndex = 33
      OnExecute = RollbackActionExecute
    end
    object RelationsInsertAction: TAction
      Tag = 2
      Category = 'Relations'
      Caption = 'Insert...'
      HelpContext = 31500
      Hint = 'Insert new relation definition'
      ImageIndex = 18
      ShortCut = 45
      OnExecute = RelationsInsertActionExecute
    end
    object RelationsEditAction: TAction
      Tag = 1
      Category = 'Relations'
      Caption = 'Edit...'
      HelpContext = 31500
      Hint = 'Edit relation definition'
      ImageIndex = 17
      ShortCut = 13
      OnExecute = RelationsEditActionExecute
    end
    object RelationsDeleteAction: TAction
      Tag = 1
      Category = 'Relations'
      Caption = 'Delete...'
      HelpContext = 31500
      Hint = 'Delete relation definition'
      ImageIndex = 19
      ShortCut = 16430
      OnExecute = RelationsDeleteActionExecute
    end
    object SchemaDeleteAction: TAction
      Tag = 1
      Category = 'Schema'
      Caption = 'Delete...'
      HelpContext = 31200
      Hint = 'Delete schema definition'
      ImageIndex = 19
      ShortCut = 16430
      OnExecute = SchemaDeleteActionExecute
    end
    object FieldsDeleteAction: TAction
      Tag = 1
      Category = 'Fields'
      Caption = 'Delete...'
      HelpContext = 31600
      Hint = 'Delete field definition'
      ImageIndex = 19
      ShortCut = 16430
      OnExecute = FieldsDeleteActionExecute
    end
    object GroupDeleteAction: TAction
      Tag = 1
      Category = 'SchemaDB2'
      Caption = 'Delete...'
      HelpContext = 31400
      Hint = 
        'Delete target database definition or delete group if last databa' +
        'se remaining'
      ImageIndex = 19
      ShortCut = 16430
      OnExecute = GroupDeleteActionExecute
    end
    object DatabasesInsertAction: TAction
      Tag = 2
      Category = 'Databases'
      Caption = 'Insert...'
      HelpContext = 31100
      Hint = 'Insert new database definition'
      ImageIndex = 18
      ShortCut = 45
      OnExecute = DatabasesInsertActionExecute
    end
    object DatabasesDeleteAction: TAction
      Tag = 1
      Category = 'Databases'
      Caption = 'Delete...'
      HelpContext = 31100
      Hint = 'Delete database definition'
      ImageIndex = 19
      ShortCut = 16430
      OnExecute = DatabasesDeleteActionExecute
    end
    object DatabasesEditAction: TAction
      Tag = 1
      Category = 'Databases'
      Caption = 'Edit...'
      HelpContext = 31100
      Hint = 'Edit database definition'
      ImageIndex = 17
      ShortCut = 13
      OnExecute = DatabasesEditActionExecute
    end
    object GroupDropObjectsAction: TAction
      Tag = 1
      Category = 'SchemaDB1'
      Caption = 'Drop system objects'
      HelpContext = 31300
      Hint = 'Drop system objects in source database'
      OnExecute = GroupDropObjectsActionExecute
    end
    object GroupCreateObjectsAction: TAction
      Tag = 1
      Category = 'SchemaDB1'
      Caption = 'Create system objects'
      HelpContext = 31300
      Hint = 'Create system objects in source database'
      ImageIndex = 30
      OnExecute = GroupCreateObjectsActionExecute
    end
    object GroupDropTriggersAction: TAction
      Tag = 1
      Category = 'SchemaDB1'
      Caption = 'Drop triggers'
      HelpContext = 31300
      Hint = 'Drop system triggers in source database'
      OnExecute = GroupDropTriggersActionExecute
    end
    object Group2DropObjectsAction: TAction
      Tag = 1
      Category = 'SchemaDB2'
      Caption = 'Drop system objects'
      HelpContext = 31400
      Hint = 'Drop system objects in target database'
      OnExecute = Group2DropObjectsActionExecute
    end
    object Group2CreateObjectsAction: TAction
      Tag = 1
      Category = 'SchemaDB2'
      Caption = 'Create system objects'
      HelpContext = 31400
      Hint = 'Create system objects in target database'
      ImageIndex = 30
      OnExecute = Group2CreateObjectsActionExecute
    end
    object Group2DropTriggersAction: TAction
      Tag = 1
      Category = 'SchemaDB2'
      Caption = 'Drop triggers'
      HelpContext = 31400
      Hint = 'Drop triggers from target database'
      OnExecute = Group2DropTriggersActionExecute
    end
    object FieldsCopyFromAction: TAction
      Tag = 2
      Category = 'Fields'
      Caption = 'Copy from relation...'
      HelpContext = 31600
      Hint = 
        'Copy field definitions from other relation (only new (source) fi' +
        'elds will be merged)'
      ImageIndex = 31
      OnExecute = FieldsCopyFromActionExecute
    end
    object RelationsCopyFromAction: TAction
      Tag = 2
      Category = 'Relations'
      Caption = 'Copy from schema...'
      HelpContext = 31500
      Hint = 
        'Copy relation definitions from other group/schema (only new (sou' +
        'rce) relations will be merged)'
      ImageIndex = 31
      OnExecute = RelationsCopyFromActionExecute
    end
    object SchemaSourceAction: TAction
      Tag = 1
      Category = 'Schema'
      Caption = 'Source database...'
      HelpContext = 31200
      Hint = 'Edit source database configuration (for selected schema)'
      ImageIndex = 28
      ShortCut = 8205
      OnExecute = SchemaSourceActionExecute
    end
    object AboutAction: TAction
      Caption = 'About...'
      Hint = 'Show about dialog'
      OnExecute = AboutActionExecute
    end
    object OpenDatabaseAction: TAction
      Caption = 'Open database'
      HelpContext = 31000
      Hint = 'Open configuration database'
      OnExecute = OpenDatabaseActionExecute
    end
    object CloseDatabaseAction: TAction
      Caption = 'Close database'
      Enabled = False
      HelpContext = 31000
      Hint = 'Close configuration database'
      ImageIndex = 24
      OnExecute = CloseDatabaseActionExecute
    end
    object CreateDatabaseAction: TAction
      Caption = 'Create database'
      HelpContext = 31000
      Hint = 'Create configuration database'
      OnExecute = CreateDatabaseActionExecute
    end
    object UpgradeDatabaseAction: TAction
      Caption = 'Upgrade database'
      HelpContext = 31000
      Hint = 'Upgrade configuration database'
      OnExecute = UpgradeDatabaseActionExecute
    end
    object ConfigAction: TAction
      Caption = 'Settings...'
      HelpContext = 32000
      Hint = 'Show settings dialog'
      ImageIndex = 16
      OnExecute = ConfigActionExecute
    end
    object StatClearSchemaAction: TAction
      Tag = 1
      Category = 'Statistics'
      Caption = 'Clear schema statistics'
      HelpContext = 30200
      Hint = 'Clear statistics for selected schema'
      ImageIndex = 27
      OnExecute = StatClearSchemaActionExecute
    end
    object StatClearGroupAction: TAction
      Tag = 1
      Category = 'Statistics'
      Caption = 'Clear group statistics'
      HelpContext = 30200
      Hint = 'Clear statistics for all relations in selected group'
      ImageIndex = 27
      OnExecute = StatClearGroupActionExecute
    end
    object StatClearRelationAction: TAction
      Tag = 1
      Category = 'Statistics'
      Caption = 'Clear relation statistics'
      HelpContext = 30200
      Hint = 'Clear statistics for selected relation'
      ImageIndex = 27
      OnExecute = StatClearRelationActionExecute
    end
    object ClearLogAction: TAction
      Caption = 'Clear log'
      HelpContext = 30100
      Hint = 'Clear replication log window'
      ImageIndex = 8
      OnExecute = ClearLogActionExecute
    end
    object HelpContentsAction: TAction
      Caption = 'Contents'
      Hint = 'Help contents'
      ImageIndex = 22
      OnExecute = HelpContentsActionExecute
    end
    object GroupCreateServerObjectsAction: TAction
      Tag = 1
      Category = 'SchemaDB1'
      Caption = 'Add server users'
      HelpContext = 31300
      Hint = 'Create server objects (users) in source database'
      ImageIndex = 29
      OnExecute = GroupCreateServerObjectsActionExecute
    end
    object GroupDropServerObjectsAction: TAction
      Tag = 1
      Category = 'SchemaDB1'
      Caption = 'Remove server users'
      HelpContext = 31300
      Hint = 'Drop server objects (users) from source database'
      OnExecute = GroupDropServerObjectsActionExecute
    end
    object Group2CreateServerObjectsAction: TAction
      Tag = 1
      Category = 'SchemaDB2'
      Caption = 'Add server user'
      HelpContext = 31400
      Hint = 'Create server objects (users) in target database'
      ImageIndex = 29
      OnExecute = Group2CreateServerObjectsActionExecute
    end
    object Group2DropServerObjectsAction: TAction
      Tag = 1
      Category = 'SchemaDB2'
      Caption = 'Remove server user'
      HelpContext = 31400
      Hint = 'Drop server objects (users) from target database'
      OnExecute = Group2DropServerObjectsActionExecute
    end
    object Group2CreateStoredProcAction: TAction
      Tag = 1
      Category = 'SchemaDB2'
      Caption = 'Create stored procedures'
      HelpContext = 31400
      Hint = 
        'Create stored procedure skeletons for '#39'P'#39' field in target databa' +
        'se'
      OnExecute = Group2CreateStoredProcActionExecute
    end
    object DatabasesUpgradeAction: TAction
      Tag = 1
      Category = 'Databases'
      Caption = 'Upgrade database'
      HelpContext = 31100
      Hint = 'Upgrade database'
      OnExecute = DatabasesUpgradeActionExecute
    end
    object ConfigEnvironmentAction: TAction
      Caption = 'Environment...'
      Enabled = False
      HelpContext = 23000
      Hint = 'Setup configuration database environment values'
      OnExecute = ConfigEnvironmentActionExecute
    end
    object DatabasesEnvironmentAction: TAction
      Tag = 1
      Category = 'Databases'
      Caption = 'Environment...'
      HelpContext = 31100
      Hint = 'Setup replication database environment values'
      OnExecute = DatabasesEnvironmentActionExecute
    end
    object DatabasesCreateObjectsAction: TAction
      Tag = 1
      Category = 'Databases'
      Caption = 'Create system objects'
      HelpContext = 31100
      Hint = 'Create system objects in database'
      ImageIndex = 30
      OnExecute = DatabasesCreateObjectsActionExecute
    end
    object DatabasesDropObjectsAction: TAction
      Tag = 1
      Category = 'Databases'
      Caption = 'Drop system objects'
      HelpContext = 31100
      Hint = 'Drop system objects in database'
      OnExecute = DatabasesDropObjectsActionExecute
    end
    object DatabasesDropTriggersAction: TAction
      Tag = 1
      Category = 'Databases'
      Caption = 'Drop triggers'
      HelpContext = 31100
      Hint = 'Drop system triggers in database'
      OnExecute = DatabasesDropTriggersActionExecute
    end
    object DatabasesCreateServerObjectsAction: TAction
      Tag = 1
      Category = 'Databases'
      Caption = 'Add server user'
      HelpContext = 31100
      Hint = 'Create server objects (users) in database'
      ImageIndex = 29
      OnExecute = DatabasesCreateServerObjectsActionExecute
    end
    object DatabasesDropServerObjectsAction: TAction
      Tag = 1
      Category = 'Databases'
      Caption = 'Remove server user'
      HelpContext = 31100
      Hint = 'Drop server objects (users) from database'
      OnExecute = DatabasesDropServerObjectsActionExecute
    end
    object DatabasesBackupAction: TAction
      Tag = 1
      Category = 'Databases'
      Caption = 'Backup...'
      HelpContext = 31100
      Hint = 'Backup database to transportable backup format'
      OnExecute = DatabasesBackupActionExecute
    end
    object DatabasesRestoreAction: TAction
      Tag = 1
      Category = 'Databases'
      Caption = 'Restore...'
      HelpContext = 31100
      Hint = 'Restore database from backup'
      OnExecute = DatabasesRestoreActionExecute
    end
    object BackupDatabaseAction: TAction
      Caption = 'Backup...'
      Enabled = False
      HelpContext = 31000
      OnExecute = BackupDatabaseActionExecute
    end
    object RestoreDatabaseAction: TAction
      Caption = 'Restore...'
      HelpContext = 31000
      OnExecute = RestoreDatabaseActionExecute
    end
    object Group2CloneSourceDatabaseAction: TAction
      Tag = 1
      Category = 'SchemaDB2'
      Caption = 'Clone from source...'
      HelpContext = 31400
      Hint = 'Clone source database and delete source replication data'
      OnExecute = Group2CloneSourceDatabaseActionExecute
    end
    object Group2CloneEmptySourceDatabaseAction: TAction
      Tag = 1
      Category = 'SchemaDB2'
      Caption = 'Clone empty from source...'
      HelpContext = 31400
      Hint = 'Clone source database structure but do not clone data'
      OnExecute = Group2CloneEmptySourceDatabaseActionExecute
    end
    object Group2CleanData: TAction
      Tag = 1
      Category = 'SchemaDB2'
      Caption = 'Clean source data...'
      HelpContext = 31400
      Hint = 
        'Wipe replication data in source database concerning to target da' +
        'tabase'
      OnExecute = Group2CleanDataExecute
    end
    object GroupCleanData: TAction
      Tag = 1
      Category = 'SchemaDB1'
      Caption = 'Clean source data...'
      HelpContext = 31300
      Hint = 'Wipe replication data in source database concerning to group'
      OnExecute = GroupCleanDataExecute
    end
    object RegisterAction: TAction
      Caption = 'Registration...'
      Hint = 'Register IBReplicator'
      OnExecute = RegisterActionExecute
    end
    object HomePageAction: TAction
      Caption = 'Home page'
      Hint = 'Goto IBReplicator homepage'
      OnExecute = HomePageActionExecute
    end
    object DatabasesSortAction: TAction
      Category = 'Databases'
      Caption = 'Order by...'
      HelpContext = 30400
      Hint = 'Set order of records in grid'
      ImageIndex = 34
      ShortCut = 16463
      OnExecute = DatabasesSortActionExecute
    end
    object SchemaSortAction: TAction
      Category = 'Schema'
      Caption = 'Order by...'
      HelpContext = 30400
      Hint = 'Set order of records in grid'
      ImageIndex = 34
      ShortCut = 16463
      OnExecute = SchemaSortActionExecute
    end
    object RelationsSortAction: TAction
      Category = 'Relations'
      Caption = 'Order by...'
      HelpContext = 30400
      Hint = 'Set order of records in grid'
      ImageIndex = 34
      ShortCut = 16463
      OnExecute = RelationsSortActionExecute
    end
    object FieldsSortAction: TAction
      Category = 'Fields'
      Caption = 'Order by...'
      HelpContext = 30400
      Hint = 'Set order of records in grid'
      ImageIndex = 34
      ShortCut = 16463
      OnExecute = FieldsSortActionExecute
    end
    object StatSortSchemaAction: TAction
      Category = 'Statistics'
      Caption = 'Schema statistics order...'
      HelpContext = 30400
      Hint = 'Set order of records in grid'
      ImageIndex = 34
      ShortCut = 16463
      OnExecute = StatSortSchemaActionExecute
    end
    object StatSortRelationAction: TAction
      Category = 'Statistics'
      Caption = 'Relation statistics order...'
      HelpContext = 30400
      Hint = 'Set order of records in grid'
      ImageIndex = 34
      ShortCut = 16463
      OnExecute = StatSortRelationActionExecute
    end
    object Group2SortAction: TAction
      Category = 'SchemaDB2'
      Caption = 'Order by...'
      HelpContext = 31400
      Hint = 'Set order of records in grid'
      ImageIndex = 34
      ShortCut = 16463
      OnExecute = Group2SortActionExecute
    end
    object SupportAction: TAction
      Caption = 'Support forum'
      Hint = 'Goto IBReplicator support forum'
      OnExecute = SupportActionExecute
    end
    object Group1ValidateAction: TAction
      Tag = 1
      Category = 'SchemaDB1'
      Caption = 'Validity report'
      HelpContext = 31300
      Hint = 'Validate relations and fields. Result is printed in log'
      OnExecute = Group1ValidateActionExecute
    end
    object Group2ValidateAction: TAction
      Tag = 1
      Category = 'SchemaDB2'
      Caption = 'Validity report'
      HelpContext = 31400
      Hint = 'Validate target relations and fields. Result is printed in log'
      OnExecute = Group2ValidateActionExecute
    end
  end
  object IBTransaction: TIBTransaction
    Active = False
    DefaultDatabase = IBReplDataModule.IBCfgDB
    DefaultAction = TARollback
    Params.Strings = (
      'read_committed'
      'rec_version'
      'nowait')
    AutoStopAction = saNone
    Left = 332
    Top = 53
  end
  object DatabasesQuery: TIBDataSet
    Tag = 1
    Database = IBReplDataModule.IBCfgDB
    Transaction = IBTransaction
    BeforeOpen = DatabasesQueryBeforeOpen
    BufferChunks = 32
    CachedUpdates = False
    DeleteSQL.Strings = (
      'DELETE FROM /*$*/"DATABASES" WHERE '
      'DBID=:DBID')
    SelectSQL.Strings = (
      'SELECT * FROM'
      '/*$*/"DATABASES" D'
      '/*WHERE*//*PRIMARY*/'
      '/*ORDER BY*/')
    Left = 260
    Top = 53
    object DatabasesQueryDBID: TIntegerField
      DisplayWidth = 3
      FieldName = 'DBID'
      Required = True
    end
    object DatabasesQueryNAME: TIBStringField
      DisplayWidth = 20
      FieldName = 'NAME'
      Size = 100
    end
    object DatabasesQueryDBTYPE: TIntegerField
      DisplayWidth = 2
      FieldName = 'DBTYPE'
      Required = True
    end
    object DatabasesQueryFILENAME: TIBStringField
      FieldName = 'FILENAME'
      Visible = False
      Size = 254
    end
    object DatabasesQueryADMINUSER: TIBStringField
      FieldName = 'ADMINUSER'
      Visible = False
      Size = 50
    end
    object DatabasesQueryADMINPSW: TIBStringField
      FieldName = 'ADMINPSW'
      Visible = False
      Size = 50
    end
    object DatabasesQueryADMINROLE: TIBStringField
      FieldName = 'ADMINROLE'
      Visible = False
      Size = 50
    end
    object DatabasesQueryCHARSET: TIBStringField
      FieldName = 'CHARSET'
      Required = True
      Visible = False
    end
    object DatabasesQuerySQLDIALECT: TIntegerField
      FieldName = 'SQLDIALECT'
      Visible = False
    end
    object DatabasesQueryCOMMENT: TMemoField
      FieldName = 'COMMENT'
      Visible = False
      BlobType = ftMemo
      Size = 8
    end
    object DatabasesQueryOBJPREFIX: TIBStringField
      FieldName = 'OBJPREFIX'
      Required = True
      Visible = False
      Size = 6
    end
  end
  object SchemaQuery: TIBDataSet
    Tag = 2
    Database = IBReplDataModule.IBCfgDB
    Transaction = IBTransaction
    BeforeDelete = SchemaQueryBeforeDelete
    BeforeOpen = DatabasesQueryBeforeOpen
    OnCalcFields = SchemaQueryCalcFields
    BufferChunks = 32
    CachedUpdates = False
    DeleteSQL.Strings = (
      'DELETE FROM /*$*/"SCHEMATA" S WHERE '
      'S.SCHEMAID=:SCHEMAID'
      '')
    SelectSQL.Strings = (
      
        'SELECT S.*, D.DBID,D.NAME AS SRCNAME,SD.SCHEMAID AS SRCSCHID, 0 ' +
        'AS GROUPID,SD.REPLUSER,SD.DISABLED FROM'
      
        '/*$*/"SCHEMATA" S LEFT JOIN /*$*/"SCHEMADB" SD LEFT JOIN /*$*/"D' +
        'ATABASES" D ON SD.DBID=D.DBID ON S.SCHEMAID=SD.SCHEMAID AND SD.G' +
        'ROUPID=0'
      '/*WHERE*//*PRIMARY*/'
      '/*ORDER BY*/')
    Left = 260
    Top = 85
    object SchemaQuerySCHEMAID: TIntegerField
      DisplayWidth = 3
      FieldName = 'SCHEMAID'
      Required = True
    end
    object SchemaQueryNAME: TIBStringField
      DisplayWidth = 18
      FieldName = 'NAME'
      Required = True
      Size = 100
    end
    object SchemaQuerySCHEMATYPE: TIntegerField
      DisplayWidth = 2
      FieldName = 'SCHEMATYPE'
      Required = True
    end
    object SchemaQuerySRCNAME: TIBStringField
      DisplayWidth = 20
      FieldName = 'SRCNAME'
      Size = 100
    end
    object SchemaQueryREPLUSER: TIBStringField
      DisplayWidth = 10
      FieldName = 'REPLUSER'
      Size = 31
    end
    object SchemaQueryS_KEEP: TIBStringField
      FieldName = 'S_KEEP'
      Required = True
      Visible = False
      Size = 1
    end
    object SchemaQueryDISABLED: TIBStringField
      DisplayWidth = 2
      FieldName = 'DISABLED'
      Size = 1
    end
    object SchemaQueryS_INSERT: TIntegerField
      FieldName = 'S_INSERT'
      Required = True
      Visible = False
    end
    object SchemaQueryS_UPDATE: TIntegerField
      FieldName = 'S_UPDATE'
      Required = True
      Visible = False
    end
    object SchemaQueryS_DELETE: TIntegerField
      FieldName = 'S_DELETE'
      Required = True
      Visible = False
    end
    object SchemaQueryS_CONFLICT: TIntegerField
      FieldName = 'S_CONFLICT'
      Required = True
      Visible = False
    end
    object SchemaQueryS_ERROR: TIntegerField
      FieldName = 'S_ERROR'
      Required = True
      Visible = False
    end
    object SchemaQueryS_MSEC: TIntegerField
      FieldName = 'S_MSEC'
      Required = True
      Visible = False
    end
    object SchemaQueryCOMMENT: TMemoField
      FieldName = 'COMMENT'
      Visible = False
      BlobType = ftMemo
      Size = 8
    end
    object SchemaQuerySRCSCHID: TIntegerField
      FieldName = 'SRCSCHID'
      Visible = False
    end
    object SchemaQueryGROUPID: TIntegerField
      FieldName = 'GROUPID'
      Visible = False
    end
    object SchemaQueryDBID: TIntegerField
      FieldName = 'DBID'
      Visible = False
    end
    object SchemaQueryGROUPID1: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'GROUPID1'
      Visible = False
      Calculated = True
    end
    object SchemaQueryGROUPID2: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'GROUPID2'
      Visible = False
      Calculated = True
    end
  end
  object SchemaDB1Query: TIBDataSet
    Database = IBReplDataModule.IBCfgDB
    Transaction = IBTransaction
    BeforeDelete = SchemaDB1QueryBeforeDelete
    BeforeOpen = DatabasesQueryBeforeOpen
    BufferChunks = 32
    CachedUpdates = False
    DeleteSQL.Strings = (
      'DELETE FROM /*$*/"SCHEMADB" SD WHERE '
      'SD.SCHEMAID=:SCHEMAID AND SD.GROUPID=0'
      '')
    SelectSQL.Strings = (
      
        'SELECT SCHEMAID,GROUPID,CAST(COUNT(*) AS INTEGER) AS COUNT2,0 AS' +
        ' DBID FROM'
      '/*$*/"SCHEMADB" SD'
      
        'WHERE SCHEMAID=:SCHEMAID AND (GROUPID>=:GROUPID1 AND GROUPID<=:G' +
        'ROUPID2)'
      'GROUP BY SCHEMAID,GROUPID'
      'ORDER BY SCHEMAID,GROUPID')
    DataSource = DataSource2
    Left = 276
    Top = 117
    object SchemaDB1QueryGROUPID: TIntegerField
      DisplayWidth = 3
      FieldName = 'GROUPID'
    end
    object SchemaDB1QueryCOUNT: TIntegerField
      DisplayWidth = 25
      FieldName = 'COUNT2'
      OnGetText = SchemaDB1QueryCOUNTGetText
    end
    object SchemaDB1QuerySCHEMAID: TIntegerField
      FieldName = 'SCHEMAID'
      Visible = False
    end
    object SchemaDB1QueryDBID: TIntegerField
      FieldName = 'DBID'
      Visible = False
    end
  end
  object SchemaDB2Query: TIBDataSet
    Tag = 3
    Database = IBReplDataModule.IBCfgDB
    Transaction = IBTransaction
    BeforeOpen = DatabasesQueryBeforeOpen
    BufferChunks = 32
    CachedUpdates = False
    DeleteSQL.Strings = (
      'DELETE FROM /*$*/"SCHEMADB" SD WHERE '
      'SD.SCHEMAID=:SCHEMAID AND SD.GROUPID=:GROUPID AND SD.DBID=:DBID'
      ''
      '')
    SelectSQL.Strings = (
      'SELECT SD.*,D.NAME,D.DBTYPE FROM'
      
        '/*$*/"SCHEMADB" SD LEFT JOIN /*$*/"DATABASES" D ON SD.DBID=D.DBI' +
        'D'
      'WHERE SD.SCHEMAID=:SCHEMAID AND SD.GROUPID=:GROUPID /*PRIMARY*/'
      '/*ORDER BY*/')
    DataSource = DataSource3
    Left = 288
    Top = 149
    object SchemaDB2QueryDBID: TIntegerField
      DisplayWidth = 3
      FieldName = 'DBID'
      Required = True
    end
    object SchemaDB2QueryNAME: TIBStringField
      DisplayWidth = 20
      FieldName = 'NAME'
      Size = 100
    end
    object SchemaDB2QuerySCHEMAID: TIntegerField
      FieldName = 'SCHEMAID'
      Required = True
      Visible = False
    end
    object SchemaDB2QueryGROUPID: TIntegerField
      FieldName = 'GROUPID'
      Required = True
      Visible = False
    end
    object SchemaDB2QueryDBMASK: TIntegerField
      FieldName = 'DBMASK'
      Required = True
      Visible = False
    end
    object SchemaDB2QueryDISABLED: TIBStringField
      DisplayWidth = 2
      FieldName = 'DISABLED'
      Size = 1
    end
    object SchemaDB2QueryDBTYPE: TIntegerField
      DisplayWidth = 2
      FieldName = 'DBTYPE'
      Required = True
    end
    object SchemaDB2QueryREPLUSER: TIBStringField
      DisplayWidth = 10
      FieldName = 'REPLUSER'
      Size = 31
    end
    object SchemaDB2QueryREPLROLE: TIBStringField
      FieldName = 'REPLROLE'
      Visible = False
      Size = 50
    end
    object SchemaDB2QueryREPLPSW: TIBStringField
      FieldName = 'REPLPSW'
      Visible = False
      Size = 30
    end
    object SchemaDB2QuerySEPARATOR: TIntegerField
      FieldName = 'SEPARATOR'
      Required = True
      Visible = False
    end
    object SchemaDB2QueryCOMMENT: TMemoField
      FieldName = 'COMMENT'
      Visible = False
      BlobType = ftMemo
      Size = 8
    end
    object SchemaDB2QueryOFFTRANSFER: TIBStringField
      FieldName = 'OFFTRANSFER'
      Visible = False
      Size = 10
    end
    object SchemaDB2QueryOFFPARAMS: TMemoField
      FieldName = 'OFFPARAMS'
      Visible = False
      BlobType = ftMemo
      Size = 8
    end
    object SchemaDB2QueryOFFENCODER: TIBStringField
      FieldName = 'OFFENCODER'
      Visible = False
      Size = 50
    end
    object SchemaDB2QueryOFFDBID: TIntegerField
      FieldName = 'OFFDBID'
      Visible = False
    end
  end
  object RelationsQuery: TIBDataSet
    Tag = 4
    Database = IBReplDataModule.IBCfgDB
    Transaction = IBTransaction
    BeforeDelete = RelationsQueryBeforeDelete
    BeforeOpen = DatabasesQueryBeforeOpen
    BufferChunks = 32
    CachedUpdates = False
    DeleteSQL.Strings = (
      'DELETE FROM /*$*/"RELATIONS" WHERE '
      
        'SCHEMAID=:SCHEMAID AND GROUPID=:GROUPID AND RELATIONID=:RELATION' +
        'ID'
      '')
    SelectSQL.Strings = (
      'SELECT * FROM'
      '/*$*/"RELATIONS"'
      'WHERE SCHEMAID=:SCHEMAID AND GROUPID=:GROUPID /*PRIMARY*/'
      '/*ORDER BY*/')
    DataSource = DataSource3
    Left = 292
    Top = 181
    object RelationsQueryRELATIONID: TIntegerField
      DisplayWidth = 3
      FieldName = 'RELATIONID'
      Required = True
    end
    object RelationsQueryRELATIONNAME: TIBStringField
      DisplayWidth = 20
      FieldName = 'RELATIONNAME'
      Size = 100
    end
    object RelationsQueryTARGETNAME: TIBStringField
      DisplayWidth = 20
      FieldName = 'TARGETNAME'
      Size = 100
    end
    object RelationsQueryTARGETTYPE: TIBStringField
      DisplayWidth = 2
      FieldName = 'TARGETTYPE'
      Required = True
      Size = 1
    end
    object RelationsQueryDISABLED: TIBStringField
      DisplayWidth = 2
      FieldName = 'DISABLED'
      Size = 1
    end
    object RelationsQuerySCHEMAID: TIntegerField
      FieldName = 'SCHEMAID'
      Required = True
      Visible = False
    end
    object RelationsQueryGROUPID: TIntegerField
      FieldName = 'GROUPID'
      Required = True
      Visible = False
    end
    object RelationsQueryOPTIONS: TIntegerField
      FieldName = 'OPTIONS'
      Visible = False
    end
    object RelationsQueryS_KEEP: TIBStringField
      FieldName = 'S_KEEP'
      Required = True
      Visible = False
      Size = 1
    end
    object RelationsQueryS_INSERT: TIntegerField
      FieldName = 'S_INSERT'
      Required = True
      Visible = False
    end
    object RelationsQueryS_UPDATE: TIntegerField
      FieldName = 'S_UPDATE'
      Required = True
      Visible = False
    end
    object RelationsQueryS_DELETE: TIntegerField
      FieldName = 'S_DELETE'
      Required = True
      Visible = False
    end
    object RelationsQueryS_CONFLICT: TIntegerField
      FieldName = 'S_CONFLICT'
      Required = True
      Visible = False
    end
    object RelationsQueryS_ERROR: TIntegerField
      FieldName = 'S_ERROR'
      Required = True
      Visible = False
    end
    object RelationsQueryS_MSEC: TIntegerField
      FieldName = 'S_MSEC'
      Required = True
      Visible = False
    end
    object RelationsQuerySYNCORDER: TIntegerField
      FieldName = 'SYNCORDER'
      Visible = False
    end
    object RelationsQuerySYNCACTIONS: TMemoField
      DisplayWidth = 2
      FieldName = 'SYNCACTIONS'
      BlobType = ftMemo
    end
    object RelationsQueryCONDITION: TMemoField
      FieldName = 'CONDITION'
      Visible = False
      BlobType = ftMemo
      Size = 8
    end
  end
  object DataSource1: TDataSource
    DataSet = DatabasesQuery
    OnDataChange = DataSource1DataChange
    Left = 292
    Top = 53
  end
  object DataSource2: TDataSource
    DataSet = SchemaQuery
    OnDataChange = DataSource2DataChange
    Left = 292
    Top = 85
  end
  object DataSource3: TDataSource
    DataSet = SchemaDB1Query
    OnDataChange = DataSource1DataChange
    Left = 308
    Top = 117
  end
  object DataSource4: TDataSource
    DataSet = SchemaDB2Query
    OnDataChange = DataSource1DataChange
    Left = 324
    Top = 149
  end
  object DataSource5: TDataSource
    DataSet = RelationsQuery
    OnDataChange = DataSource1DataChange
    Left = 324
    Top = 181
  end
  object FieldsQuery: TIBDataSet
    Tag = 5
    Database = IBReplDataModule.IBCfgDB
    Transaction = IBTransaction
    BeforeOpen = DatabasesQueryBeforeOpen
    BufferChunks = 32
    CachedUpdates = False
    DeleteSQL.Strings = (
      'DELETE FROM /*$*/"FIELDS" WHERE '
      
        'SCHEMAID=:SCHEMAID AND GROUPID=:GROUPID AND RELATIONID=:RELATION' +
        'ID AND FIELDID=:FIELDID'
      ''
      '')
    SelectSQL.Strings = (
      'SELECT * FROM'
      '/*$*/"FIELDS"'
      
        'WHERE SCHEMAID=:SCHEMAID AND GROUPID=:GROUPID AND RELATIONID=:RE' +
        'LATIONID /*PRIMARY*/'
      '/*ORDER BY*/')
    DataSource = DataSource5
    Left = 308
    Top = 213
    object FieldsQueryFIELDID: TIntegerField
      DisplayWidth = 3
      FieldName = 'FIELDID'
      Required = True
    end
    object FieldsQueryFIELDNAME: TIBStringField
      DisplayWidth = 20
      FieldName = 'FIELDNAME'
      Size = 100
    end
    object FieldsQueryFIELDTYPE: TIntegerField
      DisplayWidth = 2
      FieldName = 'FIELDTYPE'
      Required = True
    end
    object FieldsQueryTARGETNAME: TIBStringField
      DisplayWidth = 20
      FieldName = 'TARGETNAME'
      Size = 100
    end
    object FieldsQuerySCHEMAID: TIntegerField
      FieldName = 'SCHEMAID'
      Required = True
      Visible = False
    end
    object FieldsQueryGROUPID: TIntegerField
      FieldName = 'GROUPID'
      Required = True
      Visible = False
    end
    object FieldsQueryRELATIONID: TIntegerField
      FieldName = 'RELATIONID'
      Required = True
      Visible = False
    end
    object FieldsQueryOPTIONS: TIntegerField
      DisplayWidth = 2
      FieldName = 'OPTIONS'
      Required = True
      OnGetText = FieldsQueryOPTIONSGetText
    end
  end
  object DataSource6: TDataSource
    DataSet = FieldsQuery
    OnDataChange = DataSource1DataChange
    Left = 340
    Top = 213
  end
  object PopupMenu1: TPopupMenu
    Images = ImageList1
    OnPopup = PopupMenu1Popup
    Left = 4
    Top = 45
    object Dummy1: TMenuItem
      Caption = 'Dummy'
    end
  end
  object PopupMenu2: TPopupMenu
    Images = ImageList1
    OnPopup = PopupMenu2Popup
    Left = 8
    Top = 213
    object Dummy2: TMenuItem
      Caption = 'Dummy'
    end
  end
  object PopupMenu3: TPopupMenu
    Images = ImageList1
    OnPopup = PopupMenu3Popup
    Left = 8
    Top = 453
    object Dummy4: TMenuItem
      Caption = 'Dummy'
    end
  end
  object PopupMenu4: TPopupMenu
    Images = ImageList1
    OnPopup = PopupMenu4Popup
    Left = 212
    Top = 45
    object Dummy5: TMenuItem
      Caption = 'Dummy'
    end
  end
  object PopupMenu5: TPopupMenu
    Images = ImageList1
    OnPopup = PopupMenu5Popup
    Left = 564
    Top = 45
    object Dummy6: TMenuItem
      Caption = 'Dummy'
    end
  end
  object PopupMenu6: TPopupMenu
    Images = ImageList1
    OnPopup = PopupMenu6Popup
    Left = 8
    Top = 357
    object Dummy3: TMenuItem
      Caption = 'Dummy'
    end
  end
  object StatSchemaQuery: TIBDataSet
    Tag = 6
    Database = IBReplDataModule.IBCfgDB
    Transaction = IBTransaction
    BeforeOpen = DatabasesQueryBeforeOpen
    BufferChunks = 32
    CachedUpdates = False
    DeleteSQL.Strings = (
      '/*'
      'SCHEMAID=:SCHEMAID'
      'for beforeopen event*/')
    SelectSQL.Strings = (
      'SELECT S.* FROM'
      '/*$*/"SCHEMATA" S'
      'WHERE S.SCHEMATYPE=0  /*PRIMARY*/'
      '/*ORDER BY*/')
    ModifySQL.Strings = (
      'UPDATE /*$*/"SCHEMATA" SET SCHEMAID=SCHEMAID '
      'WHERE SCHEMAID=0 AND SCHEMAID=-1  /* dummy to enable refresh */')
    Left = 416
    Top = 96
    object StatSchemaQuerySCHEMAID: TIntegerField
      DisplayLabel = 'SchemaID'
      DisplayWidth = 5
      FieldName = 'SCHEMAID'
      Required = True
    end
    object StatSchemaQueryNAME: TIBStringField
      DisplayLabel = 'Schema'
      DisplayWidth = 20
      FieldName = 'NAME'
      Required = True
      Size = 100
    end
    object StatSchemaQueryS_KEEP: TIBStringField
      FieldName = 'S_KEEP'
      Required = True
      Visible = False
      Size = 1
    end
    object StatSchemaQueryS_INSERT: TIntegerField
      DisplayLabel = 'INSERT #'
      FieldName = 'S_INSERT'
      Required = True
    end
    object StatSchemaQueryS_UPDATE: TIntegerField
      DisplayLabel = 'UPDATE #'
      FieldName = 'S_UPDATE'
      Required = True
    end
    object StatSchemaQueryS_DELETE: TIntegerField
      DisplayLabel = 'DELETE #'
      FieldName = 'S_DELETE'
      Required = True
    end
    object StatSchemaQueryS_CONFLICT: TIntegerField
      DisplayLabel = 'Conflict #'
      FieldName = 'S_CONFLICT'
      Required = True
    end
    object StatSchemaQueryS_ERROR: TIntegerField
      DisplayLabel = 'Error #'
      FieldName = 'S_ERROR'
      Required = True
    end
    object StatSchemaQueryS_MSEC: TIntegerField
      DisplayLabel = 'Time [msec]'
      FieldName = 'S_MSEC'
      Required = True
    end
  end
  object DataSource7: TDataSource
    DataSet = StatSchemaQuery
    OnDataChange = DataSource1DataChange
    Left = 448
    Top = 96
  end
  object StatSchemaDB1Query: TIBDataSet
    Database = IBReplDataModule.IBCfgDB
    Transaction = IBTransaction
    BeforeOpen = DatabasesQueryBeforeOpen
    BufferChunks = 32
    CachedUpdates = False
    DeleteSQL.Strings = (
      '/*'
      'SCHEMAID=:SCHEMAID AND GROUPID=:GROUPID'
      'for beforeopen event */')
    SelectSQL.Strings = (
      'SELECT SCHEMAID,GROUPID,CAST(COUNT(*) AS INTEGER) AS COUNT2 FROM'
      '/*$*/"SCHEMADB" SD'
      'WHERE SCHEMAID=:SCHEMAID AND GROUPID<>0 /*PRIMARY*/'
      'GROUP BY SCHEMAID,GROUPID'
      'ORDER BY SCHEMAID,GROUPID')
    ModifySQL.Strings = (
      'UPDATE /*$*/"SCHEMADB" SET SCHEMAID=SCHEMAID '
      'WHERE SCHEMAID=0 AND SCHEMAID=-1  /* dummy to enable refresh */')
    DataSource = DataSource7
    Left = 432
    Top = 128
    object StatSchemaDB1QueryGROUPID: TIntegerField
      DisplayLabel = 'GroupID'
      DisplayWidth = 5
      FieldName = 'GROUPID'
    end
    object StatSchemaDB1QueryCOUNT: TIntegerField
      DisplayLabel = 'Target DB'
      DisplayWidth = 25
      FieldName = 'COUNT2'
      OnGetText = SchemaDB1QueryCOUNTGetText
    end
    object StatSchemaDB1QuerySCHEMAID: TIntegerField
      FieldName = 'SCHEMAID'
      Visible = False
    end
  end
  object DataSource8: TDataSource
    DataSet = StatSchemaDB1Query
    OnDataChange = DataSource1DataChange
    Left = 464
    Top = 128
  end
  object StatRelationsQuery: TIBDataSet
    Tag = 7
    Database = IBReplDataModule.IBCfgDB
    Transaction = IBTransaction
    BeforeOpen = DatabasesQueryBeforeOpen
    BufferChunks = 32
    CachedUpdates = False
    DeleteSQL.Strings = (
      '/*'
      
        'SCHEMAID=:SCHEMAID AND GROUPID=:GROUPID AND RELATIONID=:RELATION' +
        'ID'
      'for beforeopen event */')
    SelectSQL.Strings = (
      'SELECT * FROM'
      '/*$*/"RELATIONS"'
      'WHERE SCHEMAID=:SCHEMAID AND GROUPID=:GROUPID /*PRIMARY*/'
      '/*ORDER BY*/')
    ModifySQL.Strings = (
      'UPDATE /*$*/"RELATIONS" SET SCHEMAID=SCHEMAID '
      'WHERE SCHEMAID=0 AND SCHEMAID=-1  /* dummy to enable refresh */')
    DataSource = DataSource8
    Left = 444
    Top = 165
    object StatRelationsQueryRELATIONID: TIntegerField
      DisplayLabel = 'RelationID'
      DisplayWidth = 3
      FieldName = 'RELATIONID'
      Required = True
    end
    object StatRelationsQueryRELATIONNAME: TIBStringField
      DisplayLabel = 'Relation name'
      DisplayWidth = 25
      FieldName = 'RELATIONNAME'
      Size = 100
    end
    object StatRelationsQueryTARGETNAME: TIBStringField
      DisplayLabel = 'Target name'
      DisplayWidth = 25
      FieldName = 'TARGETNAME'
      Size = 100
    end
    object StatRelationsQueryTARGETTYPE: TIBStringField
      DisplayLabel = 'Type'
      FieldName = 'TARGETTYPE'
      Required = True
      Size = 1
    end
    object StatRelationsQueryDISABLED: TIBStringField
      FieldName = 'DISABLED'
      Visible = False
      Size = 1
    end
    object StatRelationsQuerySCHEMAID: TIntegerField
      FieldName = 'SCHEMAID'
      Required = True
      Visible = False
    end
    object StatRelationsQueryGROUPID: TIntegerField
      FieldName = 'GROUPID'
      Required = True
      Visible = False
    end
    object StatRelationsQueryS_KEEP: TIBStringField
      FieldName = 'S_KEEP'
      Required = True
      Visible = False
      Size = 1
    end
    object StatRelationsQueryS_INSERT: TIntegerField
      DisplayLabel = 'INSERT #'
      FieldName = 'S_INSERT'
      Required = True
    end
    object StatRelationsQueryS_UPDATE: TIntegerField
      DisplayLabel = 'UPDATE #'
      FieldName = 'S_UPDATE'
      Required = True
    end
    object StatRelationsQueryS_DELETE: TIntegerField
      DisplayLabel = 'DELETE #'
      FieldName = 'S_DELETE'
      Required = True
    end
    object StatRelationsQueryS_CONFLICT: TIntegerField
      DisplayLabel = 'Conflict #'
      FieldName = 'S_CONFLICT'
      Required = True
    end
    object StatRelationsQueryS_ERROR: TIntegerField
      DisplayLabel = 'Error #'
      FieldName = 'S_ERROR'
      Required = True
    end
    object StatRelationsQueryS_MSEC: TIntegerField
      DisplayLabel = 'Time [msec]'
      FieldName = 'S_MSEC'
      Required = True
    end
  end
  object DataSource9: TDataSource
    DataSet = StatRelationsQuery
    OnDataChange = DataSource1DataChange
    Left = 476
    Top = 165
  end
  object PopupMenu7: TPopupMenu
    Images = ImageList1
    Left = 64
    Top = 48
    object Clearschemastatistics2: TMenuItem
      Action = StatClearSchemaAction
    end
    object Upgradedatabase3: TMenuItem
      Action = StatSortSchemaAction
    end
  end
  object PopupMenu8: TPopupMenu
    Images = ImageList1
    Left = 68
    Top = 213
    object Cleargroupstatistics2: TMenuItem
      Action = StatClearGroupAction
    end
  end
  object PopupMenu9: TPopupMenu
    Images = ImageList1
    Left = 68
    Top = 357
    object Clearrelationstatistics2: TMenuItem
      Action = StatClearRelationAction
    end
    object Relationstatisticsorder2: TMenuItem
      Action = StatSortRelationAction
    end
  end
  object ImageList1: TImageList
    BlendColor = clWhite
    Left = 432
    Top = 32
    Bitmap = {
      494C010123002700040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      000000000000360000002800000040000000A0000000010020000000000000A0
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      000000000000000000000000FF00000000000000000000000000000000008484
      8400000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000000000000000000000000000000000000000848484000000
      0000000000000000000084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000840000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      840084000000000000000000000000000000000000000000FF00000000000000
      0000000000000000FF000000FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400840000000000000000000000000000000000000084000000840000008400
      0000840000008400000000000000000000000000000000000000000000008400
      0000840000008400000084000000840000000000000000000000000000000000
      000084000000848484000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000000000000000000000000000000000000000000000000000000000008400
      0000840000008400000000000000000000000000000000000000000000008400
      0000840000008400000000000000000000000000000000000000000000000000
      0000000000008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000000000000000000000000000000000000000000000000000840000000000
      0000840000008400000000000000000000000000000000000000000000008400
      0000840000000000000084000000000000000000000000000000000000000000
      00000000000084000000000000000000000000000000FF000000FF000000FF00
      000000000000FF000000FF000000FF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400840000000000000000000000000000008400000084000000000000000000
      0000000000008400000000000000000000000000000000000000000000008400
      0000000000000000000000000000840000000000000000000000000000000000
      00008400000084848400000000000000000000000000C6C6C600FF0000000000
      00000000000000000000FF000000C6C6C6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848484008400000084000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008400000084000000840000008400
      0000848484000000000000000000000000000000000000000000FF000000FF00
      0000FF000000FF000000FF000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C6C600FF00
      000000000000FF000000C6C6C600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF00
      0000FF000000FF00000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C6C6
      C600FF000000C6C6C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FF0000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6C6C6008484840000FF000000FF
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF000000000000FFFF0000000000000000000000000000FFFF000000
      000000FFFF0000000000000000000000000084848400C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600848484000000000000000000000000000000
      0000000000000000000000000000000000008484840000000000848484000000
      0000848484000000000084848400000000008484840000000000000000008484
      840000000000848484000000000084848400848484008484840000FF000000FF
      0000000000000000000000000000848484000000000084848400000000008484
      84000000000084848400000000008484840000000000000000000000000000FF
      FF000000000000FFFF00000000000000000000000000000000000000000000FF
      FF000000000000FFFF0000000000000000008484840000000000000000000000
      0000000000000000000000000000848484008484840000000000000000000000
      00000000000000000000000000000000000000000000FFFF000000000000FFFF
      0000000000000000000000000000FFFF000000000000FFFF000000000000FFFF
      000000000000FFFF0000000000000000000000FF000000FF000000FF000000FF
      000000FF000000FF0000000000000000000000FFFF0000000000000000000000
      000000FFFF000000000000FFFF0000000000000000000000000000FFFF000000
      000000FFFF000000000000FFFF000000000000FFFF0000000000000000000000
      000000FFFF000000000000000000000000008484840084848400848484008484
      8400848484008484840084848400000000008484840000000000000000000000
      0000000000000000000000000000000000008484840000000000FFFF00000000
      0000FFFF000000000000FFFF000000000000FFFF000000000000000000000000
      0000FFFF000000000000FFFF00008484840000FF000000FF000000FF000000FF
      000000FF000000FF00000000000000FFFF000000000000FFFF000000000000FF
      FF000000000000FFFF00000000008484840000000000000000000000000000FF
      FF0000000000000000000000000000FFFF000000000000FFFF00000000000000
      00000000000000FFFF00000000000000000084848400C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600848484000000000084000000840000000000
      00000000000000000000000000000000000000000000FFFF000000000000FFFF
      0000000000000000000000000000FFFF0000000000008400000084000000FFFF
      000000000000FFFF00000000000000000000848484008484840000FF000000FF
      00000000000000000000000000000000000000FFFF0000000000000000000000
      000000FFFF000000000000FFFF0000000000000000000000000000FFFF000000
      00000000000000000000000000000000000000FFFF000000000000FFFF000000
      0000000000000000000000000000000000008484840000000000000000000000
      0000000000000000000000000000848484008484840084000000848400008400
      0000000000000000000000000000000000008484840000000000FFFF00000000
      0000FFFF000000000000FFFF000000000000FFFF000084000000848400008400
      0000FFFF000000000000FFFF000084848400C6C6C6008484840000FF000000FF
      000000000000848484000000000000FFFF000000000000FFFF000000000000FF
      FF000000000000FFFF0000000000848484000000000000000000000000000000
      00000000000000FFFF0000000000000000000000000000FFFF000000000000FF
      FF000000000000FFFF0000000000000000008484840084848400848484008484
      840084848400848484008484840000000000848484008400000000FFFF008484
      00008400000000000000000000000000000000000000FFFF000000000000FFFF
      0000000000000000000000000000FFFF0000000000008400000000FFFF008484
      000084000000FFFF000000000000000000000000000000000000848484008484
      84000000000000000000000000000000000000FFFF0000000000000000000000
      000000FFFF000000000000FFFF0000000000000000000000000000FFFF000000
      000000FFFF000000000000FFFF0000000000000000000000000000FFFF000000
      000000FFFF0000000000000000000000000084848400C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C6008484840000000000000000008400000000FF
      FF00848400008400000000000000000000008484840000000000FFFF00000000
      0000FFFF000000000000FFFF000000000000FFFF0000000000008400000000FF
      FF008484000084000000FFFF000084848400000000008484840000000000C6C6
      C60000000000000000000000000000FFFF000000000000FFFF000000000000FF
      FF000000000000FFFF00000000000000000000000000000000000000000000FF
      FF000000000000FFFF000000000000FFFF0000000000000000000000000000FF
      FF000000000000FFFF00000000000000000084848400C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600848484008400000084000000840000008400
      000000FFFF0084840000840000000000000000000000FFFF000000000000FFFF
      0000000000000000000000000000FFFF00008400000084000000840000008400
      000000FFFF008484000084000000000000000000000000000000C6C6C6000000
      0000C6C6C6000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000084848400C6C6C600C6C6C600C6C6
      C6000000FF000000FF00C6C6C600848484008400000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0084840000840000000000000000000000FFFF00000000
      0000FFFF000000000000FFFF0000000000008400000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF008484000084000000000000008484840000000000C6C6
      C600000000000000000000848400008484000084840000848400008484000084
      84000084840000848400008484000000000000000000000000000000000000FF
      FF000000000000FFFF000000000000FFFF000000000000FFFF000000000000FF
      FF000000000000000000000000000000000084848400C6C6C600C6C6C600C6C6
      C6000000FF000000FF00C6C6C600848484008400000000FFFF0000FFFF008400
      0000840000008400000084000000840000000000000000000000000000000000
      0000000000000000000000000000000000008400000000FFFF0000FFFF008400
      0000840000008400000084000000840000000000000000000000C6C6C6000000
      0000C6C6C6000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF000000
      000000FFFF000000000000FFFF000000000000FFFF000000000000FFFF000000
      000000FFFF0000000000000000000000000084848400C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C60084848400840000008400000000FFFF008484
      00008400000000000000000000000000000000000000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000840000008400000000FFFF008484
      000084000000FFFF0000FFFF000000000000000000008484840000000000C6C6
      C60000000000C6C6C60000000000C6C6C60000000000C6C6C600000000008484
      84000000000000000000000000000000000000000000000000000000000000FF
      FF000000000000FFFF000000000000FFFF000000000000FFFF000000000000FF
      FF00000000000000000000000000000000000000000084848400000000000000
      000000000000000000000000000084848400848484008400000000FFFF0000FF
      FF00848400008400000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008400000000FFFF0000FF
      FF00848400008400000000000000000000000000000000000000848484000000
      0000848484000000000084848400000000008484840000000000848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000084848400C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C60084848400000000008400000000FF
      FF0000FFFF008484000084000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000008400000000FF
      FF0000FFFF008484000084000000000000000000000084848400C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C6008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400848484008484840084848400848484008484840000000000840000008400
      0000840000008400000084000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000840000008400
      0000840000008400000084000000840000000000000000000000848484000000
      0000848484000000000084848400000000008484840000000000848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848484000000
      0000848484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000008484000000000000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400848484000000
      0000848484008484840000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000008484840000FF
      000000FF00000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000008484840000FF
      000000FF00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000008484000000000000FFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000008484840000000000848484000000
      0000848484000000000084848400000000000000000000000000000000000000
      00000000000000000000000000000000000000000000848484008484840000FF
      000000FF00000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000848484008484840000FF
      000000FF00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000084
      84000000000000FFFF0000FFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000848484008484840084848400000000000000
      0000000000008484840084848400848484008484840000000000848484000000
      0000848484000000000084848400000000008484840000FF000000FF000000FF
      000000FF000000FF000000FF0000000000008484840000000000848484000000
      0000848484000000000084848400000000008484840000FF000000FF000000FF
      000000FF000000FF000000FF0000000000008484840000000000848484000000
      0000848484000000000084848400000000008484840000000000848484000000
      000000FFFF0000FFFF0000FFFF000000000000000000000000000000000000FF
      FF00C6C6C60000FFFF00C6C6C600000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFF000000000000FFFF
      0000000000000000000000000000FFFF00008484840000FF000000FF000000FF
      000000FF000000FF000000FF0000000000000000000000FFFF000000000000FF
      FF0000000000000000000000000000FFFF008484840000FF000000FF000000FF
      000000FF000000FF000000FF0000000000000000000000FFFF000000000000FF
      FF0000000000000000000000000000FFFF00000000000000000084848400CED6
      D6000000000000FFFF000000000000FFFF00000000000000000000FFFF00C6C6
      C60000FFFF00C6C6C60000FFFF00848484008484840084848400000000000000
      0000000000008484840084848400848484008484840000000000FFFF00000000
      0000FFFF000000000000FFFF000000000000FFFF0000848484008484840000FF
      000000FF0000000000000000000084848400848484000000000000FFFF000000
      000000FFFF000000000000FFFF000000000000FFFF00848484008484840000FF
      000000FF0000000000000000000084848400848484000000000000FFFF000000
      000000FFFF000000000000FFFF0000000000000084000000FF0000000000CED6
      D600000000000000000000FFFF00000000000000000000000000C6C6C60000FF
      FF00C6C6C60000FFFF00C6C6C60000FFFF008484840000000000848484000000
      00008484840000000000848484000000000000000000FFFF000000000000FFFF
      0000000000000000000000000000FFFF000000000000FFFF00008484840000FF
      000000FF00000000000000000000000000000000000000FFFF000000000000FF
      FF0000000000000000000000000000FFFF000000000000FFFF008484840000FF
      000000FF00000000000000000000000000000000000000FFFF000000000000FF
      FF00000000000000000000000000000000000000FF000000FF000000FF000000
      0000CED6D600000000000000000000000000000000000000000000FFFF00C6C6
      C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF0084848400848484000000
      0000848484008484840000000000000000008484840000000000FFFF00000000
      0000FFFF000000000000FFFF000000000000FFFF000000000000000000008484
      84008484840000000000FFFF000084848400848484000000000000FFFF000000
      000000FFFF000000000000FFFF000000000000FFFF0000000000000000008484
      8400848484000000000000FFFF0084848400848484000000000000FFFF000000
      000000FFFF000000000000FFFF00000000000000FF000000FF000000FF000000
      FF00000000000000000000FFFF00848484000000000000000000C6C6C60000FF
      FF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00848484000000
      00008484840000000000000000000000000000000000FFFF000000000000FFFF
      0000000000000000000000000000FFFF000000000000FFFF000000000000FFFF
      000000000000FFFF000000000000000000000000000000FFFF000000000000FF
      FF0000000000000000000000000000FFFF000000000000FFFF000000000000FF
      FF000000000000FFFF0000000000000000000000000000FFFF000000000000FF
      FF0000000000000000000000000000FFFF00000000000000FF000000FF000000
      FF000000000000FFFF000000000000000000000000000000000000FFFF00C6C6
      C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF000000
      0000000000000000000000000000000000008484840000000000FFFF00000000
      0000FFFF000000000000FFFF000000000000FFFF000000000000000000000000
      0000FFFF000000000000FFFF000084848400848484000000000000FFFF000000
      000000FFFF000000000000FFFF000000000000FFFF0000000000000000000000
      000000FFFF000000000000FFFF0084848400848484000000000000FFFF000000
      000000FFFF000000000000FFFF000000000000FFFF00000000000000FF000000
      000000FFFF000000000000FFFF00848484000000000000000000C6C6C60000FF
      FF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C6000000
      00000000000000000000000000000000000000000000FFFF000000000000FFFF
      0000000000000000000000000000FFFF000000000000FFFF000000000000FFFF
      000000000000FFFF000000000000000000000000000000FFFF000000000000FF
      FF0000000000000000000000000000FFFF000000000000FFFF000000000000FF
      FF000000000000FFFF0000000000000000000000000000FFFF000000000000FF
      FF0000000000000000000000000000FFFF000000000000FFFF000000000000FF
      FF000000000000FFFF000000000000000000000000000000000000FFFF000000
      000000000000000000000000000000000000000000000000000000FFFF000000
      0000000000000000000000000000000000000000000000000000FFFF00000000
      0000FFFF000000000000FFFF000000000000FFFF000000000000000000000000
      0000FFFF000000000000FFFF000000000000000000000000000000FFFF000000
      000000FFFF000000000000FFFF000000000000FFFF0000000000000000000000
      000000FFFF000000000000FFFF0000000000000000000000000000FFFF000000
      000000FFFF000000000000FFFF000000000000FFFF0000000000000000000000
      000000FFFF000000000000FFFF000000000000000000000000000000000000FF
      FF000000000000FFFF000000000000FFFF000000000000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF000000
      000000FFFF000000000000FFFF000000000000FFFF000000000000FFFF000000
      00000000000000000000000000000000000000000000FF000000FF000000FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF00
      0000FF000000FF000000FF000000000000000000000000848400008484000084
      8400008484000084840000848400008484000084840000848400008484000084
      8400008484000084840000848400000000000000000000848400008484000084
      8400008484000084840000848400008484000084840000848400008484000084
      84000084840000848400008484000000000000000000000000000000000000FF
      FF000000000000FFFF000000000000FFFF000000000000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848484008484840084848400848484008484840084848400848484008484
      8400848484000000000000000000000000000000000000000000000000000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000840084008400840084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000840000008400000084000000840000008400000084000000840000008400
      00008484840000000000000000000000000000000000000000000000FF000000
      FF000000FF000000FF0000000000848484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000084008400840084000000000000000000C6C6C600848484000000
      000000000000000000000000000000000000000000000000000000FFFF00C6C6
      C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000848484008400000084848400840000008484840084000000848484008400
      000084848400000000000000000000000000000000000000FF000000FF000000
      FF00000000000000FF0000000000848484000000000000FFFF000000000000FF
      FF000000000000FFFF0000000000000000000000000000000000000000008400
      84008400840000000000000000000000000000000000C6C6C600C6C6C6008484
      8400000000000000000000000000000000000000000000FFFF00C6C6C60000FF
      FF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400840000008484840084000000848484008400000084848400840000008400
      000084848400000000000000000000000000000000000000FF00000000000000
      00000000000000000000000000008484840000FFFF000000000000FFFF000000
      000000FFFF000000000000FFFF00000000008484840084008400840084000000
      0000000000000000000000000000840084008400840000000000C6C6C600C6C6
      C6008484840000000000000000000000000000000000C6C6C60000FFFF00C6C6
      C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C600000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000848484008400000084848400840000008484840084000000848484008400
      000084848400000000000000000000000000000000000000FF00000000000000
      0000848484000000000000000000848484000000000000FFFF000000000000FF
      FF000000000000FFFF0000000000000000008484840084008400000000000000
      000000000000840084008400840084008400840084008400840000000000C6C6
      C600C6C6C6008484840000000000000000000000000000FFFF00C6C6C60000FF
      FF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400840000008484840084000000848484008400000084848400840000008400
      0000848484000000000000000000000000000000000000000000000000000000
      00008484840000000000C6C6C6008484840000FFFF000000000000FFFF000000
      000000FFFF000000000000FFFF00000000008484840000000000000000008400
      840084008400840084000084840000FFFF008400840084008400840084000000
      0000C6C6C600C6C6C600848484000000000000000000C6C6C60000FFFF00C6C6
      C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C600000000000000
      0000000000000000000000000000000000000000000000000000000000008400
      0000848484008400000084848400840000008484840084000000848484008400
      0000848484000000000000000000000000000000000000000000000000000000
      000084848400C6C6C60000000000848484000000000000FFFF000000000000FF
      FF000000000000FFFF0000000000000000008484840084008400840084008400
      8400840084008400840084008400008484008400840084008400840084008400
      840000000000C6C6C60000000000000000000000000000FFFF00C6C6C60000FF
      FF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400840000008484840084000000848484008400000084848400840000008400
      0000848484000000000000000000000000000000000000000000000000000000
      00008484840000000000C6C6C6008484840000FFFF000000000000FFFF000000
      0000000000000000000000000000000000000000000084008400000000008400
      84008400840084008400840084008400840000FFFF0000FFFF00840084008400
      84008400840000000000000000000000000000000000C6C6C60000FFFF00C6C6
      C60000FFFF00C6C6C60000FFFF00C6C6C60000FFFF00C6C6C6008484840000FF
      000000FF00000000000000000000000000000000000000000000000000008400
      0000848484008400000084848400840000008484840084000000848484008400
      0000848484000000000000000000000000000000000000000000000000000000
      000084848400C6C6C60000000000848484000000000000FFFF000000000000FF
      FF0000000000C6C6C60000000000000000000000000000000000840084000000
      000084008400840084008400840084008400840084000084840000FFFF0000FF
      FF00840084008400840000000000000000000000000000FFFF00000000000000
      00000000000000000000000000000000000000000000848484008484840000FF
      000000FF00000000000000000000000000000000000000000000000000008484
      8400840000008484840084000000848484008400000084848400840000008400
      0000848484000000000000000000000000000000000000000000000000000000
      00008484840000000000C6C6C6008484840000FFFF000000000000FFFF000000
      0000000000000000000000000000000000000000000000000000000000008400
      840000000000840084008400840084008400008484008400840000FFFF0000FF
      FF0084008400840084008400840000000000000000000000000000FFFF000000
      000000FFFF000000000000FFFF00000000008484840000FF000000FF000000FF
      000000FF000000FF000000FF0000000000000000000000000000000000008400
      0000848484008400000084848400840000008484840084000000848484008400
      0000000000000000000000000000000000000000000000000000000000000000
      000084848400C6C6C60000000000848484008484840084848400848484008484
      8400848484000000000000000000000000000000000000000000000000000000
      00008400840000000000840084008400840000FFFF0000FFFF0000FFFF008400
      8400840084008400840000000000000000000000000000FFFF000000000000FF
      FF000000000000FFFF000000000000FFFF008484840000FF000000FF000000FF
      000000FF000000FF000000FF0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008484840000000000C6C6C60000000000C6C6C60000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008400840000000000840084008400840084008400840084008400
      840000000000000000000000000000000000000000000000000000FFFF000000
      000000FFFF000000000000FFFF000000000000FFFF00848484008484840000FF
      000000FF00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000084848400C6C6C60000000000C6C6C6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084008400000000008400840084008400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000008484840000FF
      000000FF00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848484008484840084848400848484008484840084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000840084008400840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400848484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008484840000000000C6C6
      C60000000000C6C6C60000000000C6C6C60000000000C6C6C60000000000C6C6
      C60000000000C6C6C6000000000000000000000000000000000000FFFF000000
      000000FFFF000000000000FFFF0000000000000000000000000000FFFF000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400C6C6C6000000
      0000C6C6C60000000000C6C6C60000000000C6C6C60000000000C6C6C6000000
      0000C6C6C60000000000C6C6C6000000000000000000000000000000000000FF
      FF000000000000FFFF00000000000000000000000000000000000000000000FF
      FF000000000000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008484840000000000C6C6
      C600000000000000000000000000C6C6C60000000000C6C6C60000000000C6C6
      C60000000000C6C6C6000000000000000000000000000000000000FFFF000000
      000000FFFF0000000000000000000000000000FFFF0000000000000000000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400C6C6C6000000
      000000000000000000000000000000000000C6C6C60000000000C6C6C6000000
      0000C6C6C60000000000C6C6C6000000000000000000000000000000000000FF
      FF000000000000FFFF000000000000FFFF000000000000FFFF00000000000000
      00000000000000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008484840000000000C6C6
      C60000000000C6C6C600000000000000000000000000C6C6C60000000000C6C6
      C60000000000C6C6C6000000000000000000000000000000000000FFFF000000
      000000FFFF000000000000FFFF000000000000FFFF000000000000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400C6C6C6000000
      0000C6C6C60000000000C6C6C60000000000C6C6C60000000000C6C6C6000000
      0000C6C6C60000000000C6C6C6000000000000000000000000000000000000FF
      FF0000000000000000000000000000FFFF000000000000FFFF000000000000FF
      FF000000000000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008484840000000000C6C6
      C600000000000000000000000000C6C6C60000000000C6C6C60000000000C6C6
      C60000000000C6C6C6000000000000000000000000000000000000FFFF000000
      00000000000000000000000000000000000000FFFF000000000000FFFF000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400C6C6C6000000
      000000000000000000000000000000000000C6C6C60000000000C6C6C6000000
      0000C6C6C60000000000C6C6C6000000000000000000000000000000000000FF
      FF000000000000FFFF0000000000000000000000000000FFFF000000000000FF
      FF000000000000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008484840000000000C6C6
      C60000000000C6C6C600000000000000000000000000C6C6C60000000000C6C6
      C60000000000C6C6C6000000000000000000000000000000000000FFFF000000
      000000FFFF000000000000FFFF0000000000000000000000000000FFFF000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400C6C6C6000000
      0000C6C6C60000000000C6C6C60000000000C6C6C60000000000C6C6C6000000
      0000C6C6C60000000000C6C6C6000000000000000000000000000000000000FF
      FF000000000000FFFF000000000000FFFF000000000000FFFF000000000000FF
      FF000000000000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF000000000000FFFF000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008484840000000000FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF00
      000000000000FF000000000000000000000000000000000000000000000000FF
      FF000000000000FFFF000000000000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400848484008484840084848400848484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848484008484
      8400848484008484840000000000000000000000000084848400848484008484
      8400848484000000000000000000000000000000000000000000000000000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000840000008484840000000000000000008400000084000000840000008400
      00008484840000000000000000000000000000000000000000000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      0000848484000000000084848400000000008484840000000000848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FF0000C6C6C600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000848484008400
      0000840000008484840000000000000000008400000084848400840000008400
      000084848400000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000008484
      84000000000084848400C6C6C600C6C6C600C6C6C600C6C6C600000000008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FF0000C6C6C6000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400840000008484
      8400840000008484840000000000000000008484840084000000848484008400
      0000848484000000000000000000000000000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000000000000000848484000000
      000084848400C6C6C60000FF000000FF000000FFFF0000FF0000C6C6C6000000
      0000848484000000000000000000000000000000000000000000000000000000
      00000000000000FF0000C6C6C60000FF0000C6C6C60000000000000000000000
      0000000000000000000000000000000000000000000084000000848484008400
      0000840000008484840000000000000000008400000084848400840000008400
      0000848484000000000000000000000000000000FF0000000000000000000000
      FF000000FF00000000000000FF000000FF000000000000000000000000000000
      FF00000000000000FF000000FF000000FF000000000000000000000000008484
      8400C6C6C60000FF000000FFFF0000FF000000FF000000FF000000FFFF00C6C6
      C600000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000FF0000C6C6C60000FF0000C6C6C600000000000000
      0000000000000000000000000000000000000000000084848400840000008484
      8400840000008484840000000000000000008484840084000000848484008400
      0000848484000000000000000000000000000000FF000000FF000000FF000000
      00000000FF00000000000000FF000000FF00000000000000FF00000000000000
      FF00000000000000FF000000FF000000FF00000000000000000084848400C6C6
      C60000FFFF0000FF000000FF000000FF000000FFFF0000FF000000FF000000FF
      0000C6C6C6000000000000000000000000000000000000000000000000000000
      00000000000000FF0000C6C6C60000FF0000C6C6C60000FF0000C6C6C6000000
      0000000000000000000000000000000000000000000084000000848484008400
      0000840000008484840000000000000000008400000084848400840000008400
      0000848484000000000000000000000000000000FF000000FF00000000000000
      00000000FF00000000000000FF000000FF00000000000000FF00000000000000
      FF0000000000000000000000FF000000FF00000000000000000000000000C6C6
      C60000FF000000FF000000FFFF0000FF000000FF000000FF000000FFFF0000FF
      0000C6C6C6008484840000000000000000000000000000000000000000000000
      0000000000000000000000FF0000C6C6C60000FF0000C6C6C60000FF0000C6C6
      C600000000000000000000000000000000000000000084848400840000008484
      8400840000008484840000000000000000008484840084000000848484008400
      0000848484000000000000000000000000000000FF00000000000000FF000000
      FF000000FF00000000000000FF000000FF00000000000000FF00000000000000
      FF00000000000000FF00000000000000FF00000000000000000084848400C6C6
      C60000FFFF0000FF000000FF000000FF000000FFFF0000FF000000FF000000FF
      0000C6C6C6000000000000000000000000000000000000000000000000000000
      00000000000000FF0000C6C6C60000FF0000C6C6C60000FF0000C6C6C60000FF
      0000000000000000000000000000000000000000000084000000848484008400
      0000840000008484840000000000000000008400000084848400840000008400
      0000848484000000000000000000000000000000FF00000000000000FF000000
      FF000000FF00000000000000FF000000FF00000000000000FF00000000000000
      FF00000000000000FF00000000000000FF00000000000000000000000000C6C6
      C60000FF000000FF000000FFFF0000FF000000FF000000FF000000FFFF0000FF
      0000C6C6C6008484840000000000000000000000000000000000000000000000
      0000000000000000000000FF0000C6C6C60000FF0000C6C6C60000FF00000000
      0000000000000000000000000000000000000000000084848400840000008484
      8400840000008484840000000000000000008484840084000000848484008400
      0000848484000000000000000000000000000000FF0000000000000000000000
      FF000000000000000000000000000000FF000000000000000000000000000000
      FF0000000000000000000000FF000000FF000000000000000000848484000000
      0000C6C6C60000FF000000FF000000FF000000FFFF0000FF000000FF0000C6C6
      C600848484000000000000000000000000000000000000000000000000000000
      00000000000000FF0000C6C6C60000FF0000C6C6C60000FF0000000000000000
      0000000000000000000000000000000000000000000084000000848484008400
      0000840000008484840000000000000000008400000084848400840000008400
      0000848484000000000000000000000000000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000008484
      840000000000C6C6C60000FFFF0000FF000000FF000000FF0000C6C6C6008484
      8400000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000FF0000C6C6C60000FF000000000000000000000000
      0000000000000000000000000000000000000000000084848400840000008484
      8400840000008484840000000000000000008484840084000000848484008400
      000084848400000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000848484000000
      00008484840000000000C6C6C600C6C6C600C6C6C600C6C6C600848484000000
      0000848484000000000000000000000000000000000000000000000000000000
      00000000000000FF0000C6C6C60000FF00000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000848484008400
      0000840000008484840000000000000000008400000084848400840000008400
      00008484840000000000000000000000000000000000000000000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF0000000000000000000000000000000000000000008484
      8400000000008484840000848400008484000084840000848400000000008484
      8400000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000FF0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400840000008484
      8400840000008484840000000000000000008484840084000000848484008400
      0000848484000000000000000000000000000000000000000000000000000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000000000000000000000000000000000000000848484000000
      0000848484000084840000848400008484000084840000848400008484000000
      0000848484000000000000000000000000000000000000000000000000000000
      00000000000000FF000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084000000848484008400
      0000840000000000000000000000000000008400000084848400840000008400
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000008484
      8400008484000084840000848400008484000084840000848400008484000084
      8400000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000084
      84000000000000FFFF0000FFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000000
      000000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000084848400C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C60000000000008484000000000000FF
      FF0000FFFF0000FFFF000000000000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF0000000000000000000000000000000000000000008484840000000000C6C6
      C60000000000C6C6C6000000000000000000848484000000000000FFFF0000FF
      FF0000FFFF000000000000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000FF00000000000000000000000000000000000000FF000000
      FF000000000000000000000000000000000000000000000000000000FF000000
      FF000000FF000000000000000000000000000000000084848400000000000000
      0000C6C6C60000000000000000000000000084848400C6C6C6000000000000FF
      FF000000000000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000FF000000000000000000000000000000FF000000FF000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      FF000000FF000000FF000000000000000000000000008484840000000000C6C6
      C6000000000000000000000084000000FF0000000000C6C6C600000000000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF00000000000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      00000000FF000000FF0000000000000000000000000084848400000000000000
      000084000000000000000000FF000000FF000000FF0000000000C6C6C6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF000000FF00000000000000
      00000000FF000000FF000000FF00000000000000000000000000000000000000
      0000000000000000FF000000FF0000000000000000008484840000000000C6C6
      C60000000000000000000000FF000000FF000000FF000000FF00000000000000
      0000C6C6C6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF000000FF00000000000000
      0000000000000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000FF000000FF00000000000000000084848400000000000000
      0000C6C6C60000000000000000000000FF000000FF000000FF00000000000000
      0000C6C6C6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF000000FF00000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      0000000000000000FF000000FF0000000000000000008484840000000000C6C6
      C60000000000C6C6C60000000000000000000000FF000000000000000000C6C6
      C600C6C6C6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF000000FF00000000000000
      00000000000000000000000000000000FF000000FF000000FF00000000000000
      0000000000000000FF000000FF00000000000000000084848400000000000000
      0000C6C6C6000000000084000000000000000000000084000000840000000000
      0000C6C6C6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF000000FF00000000000000
      0000000000000000000000000000000000000000FF000000FF000000FF000000
      0000000000000000FF000000FF0000000000000000008484840000000000C6C6
      C60000000000C6C6C60000000000C6C6C60000000000C6C6C60000000000C6C6
      C600C6C6C6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFF0000FFFF
      0000FFFF00000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF00000000000000FF0000000000000000000000000084848400000000000000
      0000840000008400000084000000840000008400000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFF0000FFFF
      0000FFFF00000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF000000000000000000000000000000000000000000000000000000FF000000
      FF000000FF000000FF000000000000000000000000008484840000000000C6C6
      C60000000000C6C6C60000000000C6C6C60000000000C6C6C600C6C6C6000000
      00008484840000000000000000000000000000000000000000000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000FFFF0000FFFF
      0000FFFF00000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      FF000000FF000000000000000000000000000000000084848400000000000000
      0000000000000000000000000000000000000000000000000000C6C6C6008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF00000000000000000000000000000000000000000084848400848484008484
      8400848484008484840084848400848484008484840084848400848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008484840084848400848484008484840084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000848484000000FF008484840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C6C600C6C6C600848484008484840084848400000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400848484000000000000000000000000000000000000000000848484008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF000000000000FFFF000000FF000000FF000000FF0000FFFF000000000000FF
      FF0000000000000000000000000000000000000000000000000000000000C6C6
      C600C6C6C6008484840000000000000000000000000084848400848484008484
      8400000000000000000000000000000000000000000000000000848484000000
      0000000000000000000084848400848484008484840000000000000000000000
      0000848484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF000000
      000000FFFF0000000000848484000000FF00848484000000000000FFFF000000
      000000FFFF000000000000000000000000000000000000000000C6C6C600C6C6
      C600000000000000000000FFFF000000000000FFFF0000000000000000008484
      8400848484000000000000000000000000000000000084848400000000000000
      0000848484008484840000000000000000000000000084848400848484000000
      0000000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C6C600000000000000000000FFFF000000000000FF
      FF000000000000FFFF000000000000FFFF000000000000FFFF000000000000FF
      FF000000000000FFFF0000000000000000000000000000000000C6C6C6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848484000000000000000000000000000000000084848400000000008484
      8400000000000000000084848400848484008484840000000000000000008484
      8400000000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF000000
      000000FFFF000000000000FFFF000000FF0000FFFF000000000000FFFF000000
      000000FFFF0000000000000000000000000000000000C6C6C600848484000000
      0000000000000000000000FFFF00008484000084840000000000000000000000
      0000848484008484840000000000000000008484840000000000000000008484
      8400000000008484840000000000000000000000000084848400000000008484
      8400000000000000000084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF000000000000FF
      FF000000000000FFFF00000000000000FF008484840000FFFF000000000000FF
      FF000000000000FFFF00000000000000000000000000C6C6C6000000000000FF
      FF000000000000FFFF000084840000FFFF0000848400008484000000000000FF
      FF00000000008484840000000000000000008484840000000000848484000000
      0000848484000000000000000000000000000000000000000000848484000000
      0000848484000000000084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF000000000000FFFF000000
      000000FFFF000000000000FFFF000000FF000000FF000000000000FFFF000000
      000000FFFF000000000000FFFF00000000000000000000000000000000000000
      00000000000000FFFF0000FFFF0000FFFF0000FFFF0000848400000000000000
      0000000000008484840000000000000000008484840000000000848484000000
      0000848484000000000000000000000000000000000000000000848484000000
      00008484840000000000848484000000000000000000FF000000FF000000FF00
      0000FF000000FF00000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF000000000000FF
      FF000000000000FFFF000000000000FFFF000000FF000000FF000000000000FF
      FF000000000000FFFF00000000000000000000000000000000000000000000FF
      FF00000000000000000000FFFF0000FFFF000084840000FFFF000000000000FF
      FF0000000000C6C6C60000000000000000008484840000000000848484000000
      0000848484000000000000000000000000000000000000000000848484000000
      0000848484000000000084848400000000000000000000000000FF000000FF00
      0000FF0000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF000000000000FFFF000000
      0000848484008484840000FFFF0000000000848484000000FF000000FF000000
      000000FFFF000000000000FFFF00000000000000000000000000848484000000
      00000000000000000000000000000000000000FFFF0000000000000000000000
      000084848400C6C6C60000000000000000008484840000000000000000008484
      8400000000008484840000000000000000000000000084848400000000008484
      840000000000000000008484840000000000000000000000000000000000FF00
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF000000000000FF
      FF000000FF000000FF000000000000FFFF00848484000000FF000000FF0000FF
      FF000000000000FFFF0000000000000000000000000000000000C6C6C6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6C6C6000000000000000000000000000000000084848400000000008484
      8400000000000000000084848400848484008484840000000000000000008484
      8400000000008484840000000000000000000000000000000000000000000000
      000000000000000000008484840000FFFF008484840000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF000000
      00000000FF000000FF008484840000000000848484000000FF000000FF000000
      000000FFFF00000000000000000000000000000000000000000000000000C6C6
      C600000000000000000000FFFF000000000000FFFF000000000000000000C6C6
      C600C6C6C6000000000000000000000000000000000084848400000000000000
      0000848484008484840000000000000000000000000084848400848484000000
      000000000000848484000000000000000000000000000000FF000000FF000000
      FF00000000000000000000FFFF0000FFFF0000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000FFFF000000000000FF
      FF00000000000000FF000000FF000000FF000000FF000000FF000000000000FF
      FF000000000000FFFF0000000000000000000000000000000000000000000000
      0000C6C6C6008484840000000000000000000000000084848400C6C6C600C6C6
      C600000000000000000000000000000000000000000000000000848484000000
      0000000000000000000084848400848484008484840000000000000000000000
      000084848400000000000000000000000000000000000000FF000000FF000000
      FF0000000000000000008484840000FFFF008484840000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF000000
      000000FFFF00000000000000FF000000FF000000FF000000000000FFFF000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6C6C600C6C6C600000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400848484000000000000000000000000000000000000000000848484008484
      840000000000000000000000000000000000000000000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF000000000000FFFF000000000000FFFF000000000000FFFF000000000000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008484840084848400848484008484840084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF000000000000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      840000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      84000000840000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000000000000000000000000000000000000000000000000000000000000
      000000000000C6C6C600C6C6C600848484008484840084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C6C600C6C6C600848484008484840084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      8400000084000000840000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      8400000084000000840000008400000084000000840000008400000084000000
      840000000000000000000000000000000000000000000000000000000000C6C6
      C600C6C6C6008484840000000000000000000000000084848400848484008484
      840000000000000000000000000000000000000000000000000000000000C6C6
      C600C6C6C6008484840000000000000000000000000084848400848484008484
      8400000000000000000000000000000000000000000000000000000000000000
      840000008400000084000000000000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000000000000000000000000000000000000000000000000C6C6C600C6C6
      C60000000000000000000000FF00000000000000FF0000000000000000008484
      8400848484000000000000000000000000000000000000000000C6C6C600C6C6
      C600000000000000000000FF00000000000000FF000000000000000000008484
      8400848484000000000000000000000000000000000000000000000000000000
      840000008400000084000000000000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000000000000000000000000000000000000000000000000C6C6C6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848484000000000000000000000000000000000000000000C6C6C6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848484000000000000000000000000000000000000000000000000000000
      840000008400000084000000000000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      8400000084000000840000008400000084000000840000008400000084000000
      84000000000000000000000000000000000000000000C6C6C600848484000000
      000000000000000000000000FF00000084000000840000000000000000000000
      00008484840084848400000000000000000000000000C6C6C600848484000000
      0000000000000000000000FF0000008400000084000000000000000000000000
      0000848484008484840000000000000000000000000000000000000000000000
      840000008400000084000000000000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      840000008400000084000000840000008400000084000000840000FFFF000000
      84000000000000000000000000000000000000000000C6C6C600000000000000
      FF00000000000000FF00000084000000FF000000840000008400000000000000
      FF000000000084848400000000000000000000000000C6C6C6000000000000FF
      00000000000000FF00000084000000FF000000840000008400000000000000FF
      0000000000008484840000000000000000000000000000000000000000000000
      840000008400000084000000000000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF0000008400000000000000
      0000000000008484840000000000000000000000000000000000000000000000
      00000000000000FF000000FF000000FF000000FF000000840000000000000000
      0000000000008484840000000000000000000000000000000000000000000000
      840000008400000084000000000000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000000000000000000000000000000000000000000000000000000000000
      FF0000000000000000000000FF000000FF00000084000000FF00000000000000
      FF0000000000C6C6C600000000000000000000000000000000000000000000FF
      0000000000000000000000FF000000FF00000084000000FF00000000000000FF
      000000000000C6C6C60000000000000000000000000000000000000000000000
      8400000084000000840000FFFF000000000000FFFF0000FFFF0000FFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      840000008400FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000084000000
      8400000000000000000000000000000000000000000000000000848484000000
      0000000000000000000000000000000000000000FF0000000000000000000000
      000084848400C6C6C60000000000000000000000000000000000848484000000
      00000000000000000000000000000000000000FF000000000000000000000000
      000084848400C6C6C60000000000000000000000000000000000000000000000
      840000008400000084000000000000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      840000008400FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000084000000
      8400000000000000000000000000000000000000000000000000C6C6C6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6C6C6000000000000000000000000000000000000000000C6C6C6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6C6C6000000000000000000000000000000000000000000000000000000
      840000008400000084000000000000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      840000008400FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000084000000
      840000000000000000000000000000000000000000000000000000000000C6C6
      C60000000000000000000000FF00000000000000FF000000000000000000C6C6
      C600C6C6C600000000000000000000000000000000000000000000000000C6C6
      C600000000000000000000FF00000000000000FF00000000000000000000C6C6
      C600C6C6C6000000000000000000000000000000000000000000000000000000
      8400FFFF0000000084000000000000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000000000000000000000000000000000000000000000000000000000000
      0000C6C6C6008484840000000000000000000000000084848400C6C6C600C6C6
      C600000000000000000000000000000000000000000000000000000000000000
      0000C6C6C6008484840000000000000000000000000084848400C6C6C600C6C6
      C600000000000000000000000000000000000000000000000000000000000000
      8400FFFF0000FFFF00000000000000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6C6C600C6C6C600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6C6C600C6C6C600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000A00000000100010000000000000500000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFFFFFFFFFF0000FFFFFFFF81F70000
      FFFFFFFF9DE30000FFFFFFFFCFE30000FFFFFFFFE7C10000FFFFFFFFF3C10000
      F3FFFFE7B9F70000E783E0F381F70000EFE3E3FBFFF70000EFD3E5FB88F70000
      E73BEEF39CF70000F0FFFF07C1F70000FFFFFFFFC9F70000FFFFFFFFE3F70000
      FFFFFFFFE3F70000FFFFFFFFF7F70000FFFFFFFFFFFFCFFFE00780FFFFFF07FF
      D553007F00000000AA297E3F2A8A01549415003F5154008AAA89001F2A8A0354
      91457E0F5104028AA0A900072A828B54845500035140AA8AAA2900012A009000
      800500005100A8008AA9000000009000955500070000A8AFCAAB3E030000800F
      E0078001FFC1800FFFFFC040FFC0800FFFD7FFE3FFE3FFF0FF93FFC3FFC3FFE0
      FF55FF81FF81FFC0E038000000000000C1FF2A002A002A008038510051005008
      80552A822A822A04801351445144500080172A8A2A8A2A02800F515451545104
      800F2A8A2A8A2A8A800F5154515451548A8F000000000000954F000000000000
      CA9F000000000000E03FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3FFFE3FFFFF
      F007E3FFF81FC07FE007C200E18F803FE0078AAA8607001FE007BE541803001F
      E007B0AA2001001FE007F4540000001FE007F2AA00010003E007F450A0010003
      E007F2A1D0010001E007F453E8001500E00FF207F4002A00FFFFF52FFA039501
      FFFFF29FFD0FC043FFFFF03FFE3FFFE7FFFFFFFFFFFFFFFF80008001E003EFFD
      AAAA9555EFFBC7FF9154AA29EFFBC3FBA0AA9415EFFBE3F79454AA89EFFBF1E7
      AA2A9145EFFBF8CF9154A0A9EFFBFC1FA0AA8455EFFBFE3F9454AA29EFFBFC1F
      AA2A9515EFC3F8CF9554AAA9EFD7E1E780008055EFCFC3F3A00AAA01E01FC7FD
      8000C0FFFFFFFFFFFFFFFFFFFFFFFFFFF00FF81FFFFFC387E007F00FF9FF8307
      C003E007F8FF83078001C003FC7F830700008001F83F830764E88001FC1F8307
      14A88001F80F830734AC8001FC07830744AA8001F80F830744AA8001FC1F8307
      6EEC8001F83F830700008001FC7F830780018001F8FF8307C0038001FDFF8307
      E0078001FBFF870FF00F8001FFFFFFFFFFC08003FFFFFFFF8000BFFBFFC1F83F
      8000B77BFFC1E00FAA00B77BFFC1CFC7B401A01BFFC187E3A823BBBBFFC1A3F3
      B013BBBBFFFF31F9A803B00BFFF738F9B413BDDBFFE33C79AA23BDDBFFC13E39
      B513BFFB83803F19AAA3BFC383E39F8BB043BFDB83E38FC3AA97801783E3C7E7
      BFCFBFCF8387E00F801F801FFFFFF83FFFFFFFFFFE7FFFFFF83FF83FFE9FFC7F
      E00FE7CFFDE7E82FC007DC77FDF9D4578543B39BFB3CAAAB8823AC6BFBDED457
      10116BAD00F6AA2B000157D5003E5455501157D5000DAA2B440157D500325115
      53116BAD003EA20B8823AC6B003ED117A543B39B003EA82BD007DC77001DD457
      E70FE7CF0023EAAFF83FF83F003FFD7FC007C007FFFFFFFFC007C007F83FF83F
      C007C007E00FE00FC007C007C007C007C007C00785438543C007C00788238823
      C007C00710111011C007C00700010001C007C00750115011C007C00744014401
      C007C00753115311C007C00788238823C007C007A543A543C007C007D007D007
      C007C007E70FE70FC007C007F83FF83F00000000000000000000000000000000
      000000000000}
  end
  object ImageList2: TImageList
    BlendColor = clWhite
    BkColor = clWhite
    Left = 472
    Top = 32
    Bitmap = {
      494C010111001300040010001000FFFFFF00FF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000005000000001002000000000000050
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF008484840000000000848484000000000084848400000000008484
      8400000000008484840000000000848484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFF0000FFFFFF00FFFF0000FFFFFF0000000000FFFF
      FF00FFFF0000FFFFFF00FFFF0000000000000000000000000000008484000084
      8400008484000084840000848400008484000084840000848400008484000084
      840000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0084848400FFFFFF00FFFF0000FFFFFF00FFFF000000000000FFFF
      0000FFFFFF00FFFF0000FFFFFF00848484000000000000FFFF00000000000084
      8400008484000084840000848400008484000084840000848400008484000084
      84000084840000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00848484000000000084848400FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFF0000FFFFFF00FFFF0000FFFFFF0000000000FFFF
      FF00FFFF0000FFFFFF00FFFF00000000000000000000FFFFFF0000FFFF000000
      0000008484000084840000848400008484000084840000848400008484000084
      8400008484000084840000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008484
      840000000000848484000000000000FFFF000000000084848400000000008484
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0084848400000000008484
      84000000000084848400FFFFFF00FFFF0000FFFFFF00FFFF000000000000FFFF
      0000FFFFFF00FFFF0000FFFFFF00848484000000000000FFFF00848484000000
      0000008484000084840000848400008484000084840000848400008484000084
      840000848400008484000084840000000000FFFFFF0084848400000000000000
      00000000000084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00848484000000000084848400FFFFFF00FFFFFF00FFFFFF00C6C6C6000000
      000000FFFF00848484000000000000FFFF00000000008484840000FFFF000000
      0000C6C6C600FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000084848400FFFF
      FF00C6C6C60000000000FFFF0000FFFFFF00FFFF0000FFFFFF0000000000FFFF
      FF00FFFF0000FFFFFF00FFFF00000000000000000000FFFFFF0084848400FFFF
      FF00000000000000000000000000000000000084840000848400008484000084
      840000848400008484000084840000000000848484000000000000FFFF0000FF
      FF0000FFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000FFFF0000000000FFFFFF00FFFFFF00FFFFFF00000000008484
      84008484840000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00848484008484
      840000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0084848400FFFFFF00C6C6
      C600FFFFFF0000000000FFFFFF00FFFF0000FFFFFF00FFFF000000000000FFFF
      0000FFFFFF00FFFF0000FFFFFF00000000000000000000FFFF0084848400FFFF
      0000FFFFFF00FFFF0000FFFFFF00848484000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF0000FFFF000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      00000000000000FFFF000000000084848400FFFFFF00FFFFFF00848484000000
      00000000000000FFFF0084848400000000008484840000FFFF00000000000000
      000084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C6C6C600FFFF
      FF00C6C6C6000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF0084848400FFFF
      FF00FFFF0000FFFFFF00FFFF000084848400FFFFFF00FFFF0000FFFFFF00FFFF
      000084848400FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF0000FFFF000000
      000000FFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF00FFFF
      FF0000FFFF00FFFFFF0000FFFF0000000000FFFFFF00FFFFFF000000000000FF
      FF0000FFFF0000FFFF0000000000FFFFFF000000000000FFFF0000FFFF0000FF
      FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0084848400FFFFFF00C6C6
      C600FFFFFF0000000000FF000000FF000000FF000000FF000000FF000000FF00
      0000FF000000FF000000FF000000000000000000000000FFFF0084848400FFFF
      0000FFFFFF00FFFF0000FFFFFF0084848400FFFF0000FFFFFF00FFFF0000FFFF
      FF0084848400FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF0000FFFF000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      000000000000000000000000000084848400FFFFFF00FFFFFF00848484000000
      00000000000000FFFF0084848400000000008484840000FFFF00000000000000
      000084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C6C6C600FFFF
      FF00C6C6C6000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF000000000084848400FFFF
      FF00FFFF0000FFFFFF00FFFF000084848400FFFFFF00FFFF0000FFFFFF00FFFF
      000084848400FFFFFF00FFFFFF00FFFFFF008484840000000000FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008484
      84008484840000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00848484008484
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0084848400FFFFFF00C6C6
      C600FFFFFF00C6C6C60000000000C6C6C600FFFFFF00C6C6C600FFFFFF008484
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0084848400FFFF
      0000FFFFFF00FFFF0000FFFFFF0084848400FFFF0000FFFFFF00FFFF0000FFFF
      FF0084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF0084848400000000000000
      00000000000084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      000000FFFF00848484000000000000FFFF00000000008484840000FFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000848484000000
      0000848484000000000084848400000000008484840000000000848484000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      840084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008484
      840000000000848484000000000000FFFF000000000084848400000000008484
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0084848400C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C6008484
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0084848400FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF00
      000084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00848484000000000084848400FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000848484000000
      0000848484000000000084848400000000008484840000000000848484000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      840084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008484840000000000000000000000
      0000000000000000000084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF0084848400FF00
      000000840000008400000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF008484840000000000848484000000
      0000848484000000000084848400000000008484840000000000000000008484
      840000000000848484000000000084848400FFFFFF00FFFFFF00000000000000
      0000FFFFFF00FFFFFF00FFFFFF0000840000FF000000FFFFFF0000840000FFFF
      000000840000FF0000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0084848400C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C60000000000FF000000C6C6C600FFFF
      0000C6C6C60000000000FFFFFF00FFFFFF0000000000FFFF0000FFFFFF00FFFF
      0000FFFFFF0000000000FFFFFF00FFFF0000FFFFFF00FFFF000000000000FFFF
      0000FFFFFF00FFFF0000FFFFFF00000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF0000000000FF000000FFFFFF00848484008484
      840084848400FFFF00000084000000000000FFFFFF00FFFFFF00FFFFFF000000
      0000FFFF0000C6C6C600FFFF0000C6C6C600FFFF0000C6C6C600FFFF00000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0084848400FFFF0000C6C6
      C600FFFF0000C6C6C600FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000C6C6
      C600FFFF000000000000FFFFFF00FFFFFF0084848400FFFFFF00FFFF0000FFFF
      FF00FFFF000000000000FFFF0000FFFFFF00FFFF0000FFFFFF0000000000FFFF
      FF00FFFF0000FFFFFF00FFFF00008484840000000000FFFFFF00000000000000
      0000FFFFFF00FFFFFF00FFFFFF0000840000FF000000FFFFFF00848484008484
      8400FF000000000000000000000000000000FFFFFF00FFFFFF0000000000FFFF
      0000C6C6C600FFFF0000C6C6C600FFFF0000C6C6C600FFFF0000C6C6C600FFFF
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0084848400FFFF0000FFFF
      0000C6C6C600FFFF000000000000FF00000000000000FF00000000000000FFFF
      0000C6C6C60000000000FFFFFF00FFFFFF0000000000FFFF0000FFFFFF00FFFF
      0000FFFFFF0000000000FFFFFF00FFFF0000FFFFFF00FFFF000000000000FFFF
      0000FFFFFF00FFFF0000FFFFFF000000000000000000FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FF000000FFFFFF00848484008484
      840000840000848484000084000000000000FFFFFF00FFFFFF0000000000C6C6
      C600FFFF0000C6C6C600FFFF0000C6C6C600FFFF0000C6C6C600FFFF0000C6C6
      C60000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0084848400FFFF0000C6C6
      C600FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000C6C6C600FFFF0000C6C6
      C600FFFF000000000000FFFFFF00FFFFFF0084848400FFFFFF00FFFF0000FFFF
      FF00FFFF000000000000FFFF0000FFFFFF00FFFF0000FFFFFF0000000000FFFF
      FF00FFFF0000FFFFFF00FFFF00008484840000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000840000FF000000FFFFFF00008400008484
      840084848400FF0000000000000000000000FFFFFF00FFFFFF0000000000FFFF
      0000C6C6C600FFFF0000C6C6C600FFFF0000C6C6C600FFFF0000C6C6C600FFFF
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0084848400FFFF0000FFFF
      000000000000FFFF0000FF00000000000000FF00000000000000C6C6C6000000
      0000C6C6C60000000000FFFFFF00FFFFFF0000000000FFFF0000FFFFFF00FFFF
      0000FFFFFF0000000000FFFFFF00FFFF0000FFFFFF00FFFF000000000000FFFF
      0000FFFFFF00FFFF0000FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FF00000084848400848484008484
      840084848400848484000084000000000000FFFFFF00FFFFFF0000000000C6C6
      C600FFFF0000C6C6C600FFFF0000C6C6C600FFFF0000C6C6C600FFFF0000C6C6
      C60000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0084848400FFFF0000C6C6
      C600FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000C6C6
      C600FFFF000000000000FFFFFF00FFFFFF0084848400FFFFFF00FFFF0000FFFF
      FF00FFFF000000000000FFFF0000FFFFFF00FFFF0000FFFFFF0000000000FFFF
      FF00FFFF0000FFFFFF00FFFF00008484840000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000084000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF008484840000000000FFFFFF00FFFFFF0000000000FFFF
      0000C6C6C600FFFF0000C6C6C600FFFF0000C6C6C600FFFF0000C6C6C600FFFF
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0084848400FFFF0000FFFF
      0000C6C6C600FFFF000000000000FF00000000000000FF000000C6C6C600FFFF
      0000C6C6C60000000000FFFFFF00FFFFFF0000000000FFFF0000FFFFFF00FFFF
      0000FFFFFF0000000000FFFFFF00FFFF0000FFFFFF00FFFF000000000000FFFF
      0000FFFFFF00FFFF0000FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008484840000000000000000000000
      0000000000000000000084848400FFFFFF00FFFFFF00FFFFFF0000000000C6C6
      C600FFFF0000C6C6C600FFFF0000C6C6C600FFFF0000C6C6C600FFFF0000C6C6
      C60000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0084848400FFFF0000C6C6
      C600FFFF0000C6C6C600FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000C6C6
      C600FFFF000000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFF0000FFFF
      FF00FFFF000000000000FFFF0000FFFFFF00FFFF0000FFFFFF0000000000FFFF
      FF00FFFF0000FFFFFF00FFFF000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      000000000000000000000000000000000000000000000000000000000000FFFF
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0084848400FFFF0000FFFF
      0000C6C6C600FFFF000000000000FFFF000000000000FF00000000000000FFFF
      0000C6C6C60000000000FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00000000000000000000000000FFFFFF00000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000FFFF0000FFFFFF00FFFF0000FFFFFF00FFFF0000FFFFFF00FFFF00000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0084848400FFFF0000C6C6
      C600FFFF0000C6C6C600FFFF0000C6C6C600FFFF0000C6C6C600FFFF0000C6C6
      C600FFFF000000000000FFFFFF00FFFFFF0000000000FF000000FF000000FF00
      0000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF00
      0000FF000000FF000000FF00000000000000FFFFFF00FFFFFF0000000000FFFF
      FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      0000FFFFFF00FFFF0000FFFFFF00FFFF0000FFFFFF00FFFF0000FFFFFF00FFFF
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0084848400FFFF0000FFFF
      000000000000FF00000000000000FF00000000000000FFFF0000000000000000
      00000000000000000000FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFF0000FFFFFF00FFFF0000FFFFFF00FFFF0000FFFFFF00FFFF00000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0084848400FFFF0000C6C6
      C600FFFF0000C6C6C600FFFF0000C6C6C600FFFF0000C6C6C600C6C6C600FFFF
      000084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0084848400FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000C6C6C6008484
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000FFFF
      FF00000000000000000000000000FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0084848400848484008484
      840084848400848484008484840084848400848484008484840084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000008400FFFFFF00FFFFFF0000008400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000840000FFFFFF00FFFFFF0000840000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000008400FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000840000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000840000008400FFFFFF00FFFFFF000000840000008400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000084000000840000FFFFFF00FFFFFF000084000000840000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000840000008400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000084000000840000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000084000000FF0000008400FFFFFF00FFFFFF00000084000000FF000000
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000084000000FF000000840000FFFFFF00FFFFFF000084000000FF00000084
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000084000000FF0000008400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000084000000FF000000840000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      84000000FF000000FF00000084000000840000008400000084000000FF000000
      FF0000008400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000084
      000000FF000000FF00000084000000840000008400000084000000FF000000FF
      000000840000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000008400000084000000
      840000008400000084000000840000008400000084000000FF000000FF000000
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000840000008400000084
      0000008400000084000000840000008400000084000000FF000000FF00000084
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000084000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF0000008400FFFFFF00FFFFFF00FFFFFF00FFFFFF000084000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF000000840000FFFFFF00FFFFFF00FFFFFF00000084000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF0000008400FFFFFF00FFFFFF00FFFFFF00FFFFFF000084000000FF000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000840000FFFFFF00FFFFFF00FFFFFF00FFFFFF00000084000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF0000008400FFFFFF00FFFFFF000084000000FF000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF000000840000FFFFFF00FFFFFF00000084000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF0000008400FFFFFF00FFFFFF00FFFFFF000084000000FF000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF000000840000FFFFFF00FFFFFF00000084000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000084000084000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000840000FFFFFF00000084000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF0000008400FFFFFF00FFFFFF000084000000FF000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF000000840000FFFFFF00000084000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000084000084000000FF000000FF000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF000000FF000000840000FFFFFF00000084000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF0000008400FFFFFF00FFFFFF000084000000FF000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF000000840000FFFFFF00FFFFFF00000084000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF0000008400FFFFFF00FFFFFF000084000000FF000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF000000840000FFFFFF00FFFFFF00000084000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF0000008400FFFFFF00FFFFFF00FFFFFF000084000000FF000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF000000840000FFFFFF00FFFFFF00FFFFFF00FFFFFF00000084000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF0000008400FFFFFF00FFFFFF00FFFFFF00FFFFFF000084000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000FF000000840000FFFFFF00FFFFFF00FFFFFF00000084000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF0000008400FFFFFF00FFFFFF00FFFFFF00FFFFFF000084000000FF000000FF
      000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF
      000000840000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      84000000FF000000FF00000084000000840000008400000084000000FF000000
      FF0000008400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000084
      000000FF000000FF00000084000000840000008400000084000000FF000000FF
      000000840000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000008400000084000000
      840000008400000084000000840000008400000084000000FF000000FF000000
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000840000008400000084
      0000008400000084000000840000008400000084000000FF000000FF00000084
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000084000000FF0000008400FFFFFF00FFFFFF00000084000000FF000000
      8400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000084000000FF000000840000FFFFFF00FFFFFF000084000000FF00000084
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000084000000FF0000008400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000084000000FF000000840000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000840000008400FFFFFF00FFFFFF000000840000008400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000084000000840000FFFFFF00FFFFFF000084000000840000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000840000008400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000084000000840000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000008400FFFFFF00FFFFFF0000008400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000840000FFFFFF00FFFFFF0000840000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000008400FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000840000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF008484840084848400848484008484840084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000000000C6C6C600C6C6C6008484840084848400848484000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000000000C6C6C600C6C6C6008484840084848400848484000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000000000C6C6C600C6C6C6008484840084848400848484000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF008484840084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008484
      840084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000C6C6C600C6C6C60084848400000000000000000000000000848484008484
      84008484840000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000C6C6C600C6C6C60084848400000000000000000000000000848484008484
      84008484840000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000C6C6C600C6C6C60084848400000000000000000000000000848484008484
      84008484840000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008484
      8400FFFFFF00FFFFFF00FFFFFF00848484008484840084848400FFFFFF00FFFF
      FF00FFFFFF0084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000C6C6
      C600C6C6C60000000000FFFFFF000000FF00FFFFFF000000FF00FFFFFF000000
      0000848484008484840000000000FFFFFF00FFFFFF00FFFFFF0000000000C6C6
      C600C6C6C60000000000FFFFFF0000FF0000FFFFFF0000FF0000FFFFFF000000
      0000848484008484840000000000FFFFFF00FFFFFF00FFFFFF0000000000C6C6
      C600C6C6C60000000000FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF000000
      0000848484008484840000000000FFFFFF00FFFFFF00FFFFFF0084848400FFFF
      FF00FFFFFF008484840084848400FFFFFF00FFFFFF00FFFFFF00848484008484
      8400FFFFFF00FFFFFF0084848400FFFFFF00FFFFFF00FFFFFF0000000000C6C6
      C60000000000FFFFFF000000000000000000000000000000000000000000FFFF
      FF00000000008484840000000000FFFFFF00FFFFFF00FFFFFF0000000000C6C6
      C60000000000FFFFFF000000000000000000000000000000000000000000FFFF
      FF00000000008484840000000000FFFFFF00FFFFFF00FFFFFF0000000000C6C6
      C60000000000FFFFFF000000000000000000000000000000000000000000FFFF
      FF00000000008484840000000000FFFFFF00FFFFFF00FFFFFF0084848400FFFF
      FF0084848400FFFFFF00FFFFFF00848484008484840084848400FFFFFF00FFFF
      FF0084848400FFFFFF0084848400FFFFFF00FFFFFF0000000000C6C6C6008484
      8400FFFFFF0000000000000000000000FF000000840000008400000000000000
      0000FFFFFF00848484008484840000000000FFFFFF0000000000C6C6C6008484
      8400FFFFFF00000000000000000000FF00000084000000840000000000000000
      0000FFFFFF00848484008484840000000000FFFFFF0000000000C6C6C6008484
      8400FFFFFF00000000000000000000FFFF000084840000848400000000000000
      0000FFFFFF00848484008484840000000000FFFFFF0084848400FFFFFF00FFFF
      FF0084848400FFFFFF0084848400FFFFFF00FFFFFF00FFFFFF0084848400FFFF
      FF0084848400FFFFFF00FFFFFF0084848400FFFFFF0000000000C6C6C6000000
      00000000FF00000000000000FF00000084000000FF0000008400000084000000
      00000000FF00000000008484840000000000FFFFFF0000000000C6C6C6000000
      000000FF00000000000000FF00000084000000FF000000840000008400000000
      000000FF0000000000008484840000000000FFFFFF0000000000C6C6C6000000
      000000FFFF000000000000FFFF000084840000FFFF0000848400008484000000
      000000FFFF00000000008484840000000000FFFFFF0084848400FFFFFF008484
      8400FFFFFF0084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008484
      8400FFFFFF0084848400FFFFFF0084848400FFFFFF0000000000FFFFFF000000
      0000FFFFFF00000000000000FF000000FF000000FF000000FF00000084000000
      0000FFFFFF00000000008484840000000000FFFFFF0000000000FFFFFF000000
      0000FFFFFF000000000000FF000000FF000000FF000000FF0000008400000000
      0000FFFFFF00000000008484840000000000FFFFFF0000000000FFFFFF000000
      0000FFFFFF000000000000FFFF0000FFFF0000FFFF0000FFFF00008484000000
      0000FFFFFF00000000008484840000000000FFFFFF0084848400FFFFFF008484
      8400FFFFFF0084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008484
      8400FFFFFF0084848400FFFFFF0084848400FFFFFF0000000000FFFFFF000000
      00000000FF0000000000FFFFFF000000FF000000FF00000084000000FF000000
      00000000FF0000000000C6C6C60000000000FFFFFF0000000000FFFFFF000000
      000000FF000000000000FFFFFF0000FF000000FF00000084000000FF00000000
      000000FF000000000000C6C6C60000000000FFFFFF0000000000FFFFFF000000
      000000FFFF0000000000FFFFFF0000FFFF0000FFFF000084840000FFFF000000
      000000FFFF0000000000C6C6C60000000000FFFFFF0084848400FFFFFF008484
      8400FFFFFF0084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008484
      8400FFFFFF0084848400FFFFFF0084848400FFFFFF0000000000FFFFFF008484
      8400FFFFFF000000000000000000FFFFFF00FFFFFF000000FF00000000000000
      0000FFFFFF0084848400C6C6C60000000000FFFFFF0000000000FFFFFF008484
      8400FFFFFF000000000000000000FFFFFF00FFFFFF0000FF0000000000000000
      0000FFFFFF0084848400C6C6C60000000000FFFFFF0000000000FFFFFF008484
      8400FFFFFF000000000000000000FFFFFF00FFFFFF0000FFFF00000000000000
      0000FFFFFF0084848400C6C6C60000000000FFFFFF0084848400FFFFFF00FFFF
      FF0084848400FFFFFF0084848400FFFFFF00FFFFFF00FFFFFF0084848400FFFF
      FF0084848400FFFFFF00FFFFFF0084848400FFFFFF00FFFFFF0000000000C6C6
      C60000000000FFFFFF000000000000000000000000000000000000000000FFFF
      FF0000000000C6C6C60000000000FFFFFF00FFFFFF00FFFFFF0000000000C6C6
      C60000000000FFFFFF000000000000000000000000000000000000000000FFFF
      FF0000000000C6C6C60000000000FFFFFF00FFFFFF00FFFFFF0000000000C6C6
      C60000000000FFFFFF000000000000000000000000000000000000000000FFFF
      FF0000000000C6C6C60000000000FFFFFF00FFFFFF00FFFFFF0084848400FFFF
      FF0084848400FFFFFF00FFFFFF00848484008484840084848400FFFFFF00FFFF
      FF0084848400FFFFFF0084848400FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00C6C6C60000000000FFFFFF000000FF00FFFFFF000000FF00FFFFFF000000
      0000C6C6C600C6C6C60000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00C6C6C60000000000FFFFFF0000FF0000FFFFFF0000FF0000FFFFFF000000
      0000C6C6C600C6C6C60000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00C6C6C60000000000FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF000000
      0000C6C6C600C6C6C60000000000FFFFFF00FFFFFF00FFFFFF0084848400FFFF
      FF00FFFFFF008484840084848400FFFFFF00FFFFFF00FFFFFF00848484008484
      8400FFFFFF00FFFFFF0084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00C6C6C6008484840000000000000000000000000084848400C6C6
      C600C6C6C60000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00C6C6C6008484840000000000000000000000000084848400C6C6
      C600C6C6C60000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000FFFFFF00C6C6C6008484840000000000000000000000000084848400C6C6
      C600C6C6C60000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008484
      8400FFFFFF00FFFFFF00FFFFFF00848484008484840084848400FFFFFF00FFFF
      FF00FFFFFF0084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000000000FFFFFF00FFFFFF00FFFFFF00C6C6C600C6C6C6000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000000000FFFFFF00FFFFFF00FFFFFF00C6C6C600C6C6C6000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000000000FFFFFF00FFFFFF00FFFFFF00C6C6C600C6C6C6000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF008484840084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008484
      840084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF008484840084848400848484008484840084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00424D3E000000000000003E000000
      2800000040000000500000000100010000000000800200000000000000000000
      000000000000000000000000FFFFFF00FFFF000000000000FFFF000000000000
      FFFF000000000000FFFF000000000000FFFF000000000000FFFF000000000000
      FFFF000000000000FFFF000000000000FFFF000000000000FFFF000000000000
      FFFF000000000000FFFF000000000000FFFF000000000000FFFF000000000000
      FFFF000000000000FFFF000000000000FFFFFFFFFFFFFFFFF800000FFFFFFFFF
      F9540007FFFFFFFFFA8A0003FFFFFC7FF9544001FFFFE00F828A000083F1C007
      9154500003F1C007AA8A0A004000C007900054A742B4C107A8000A574000C007
      900094A73BFFE00FA8AFCA5783FFE00F800FC007FFFFE00F800FC007FFFFFC7F
      800FC007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF01FFFFFFFFFFFFDE40FFFF8003
      0000CE40F01F80032A8A0640E00F800351544E40C00780032A8A5E40C0078003
      51547E40C00780032A8A7E00C007800351547E7CC00780032A8A7F01C0078003
      5154FEFFC00780030000111FC54780030000D57FCAA780030000157FE54F8007
      FFFF757FF01F800FFFFF117FFFFF801FFFFFFFFFFFFFFFFFFDBFFDBFFF7FFF7F
      F99FF99FFF3FFF3FF18FF18FFF1FFF1FE007E007800F800FC003C00380078007
      8001800180038003000000008001800100000000800180018001800180038003
      C003C00380078007E007E007800F800FF18FF18FFF1FFF1FF99FF99FFF3FFF3F
      FDBFFDBFFF7FFF7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC1FFC1FFC1FFC1F
      F007F007F007F3E7E003E003E003EE3BC2A1C2A1C2A1D9CDC411C411C411D635
      880888088808B5D6800080008000ABEAA808A808A808ABEAA200A200A200ABEA
      A988A988A988B5D6C411C411C411D635D2A1D2A1D2A1D9CDE803E803E803EE3B
      F387F387F387F3E7FC1FFC1FFC1FFC1F00000000000000000000000000000000
      000000000000}
  end
  object BackupOpenDialog: TOpenDialog
    DefaultExt = '.gbk'
    Filter = 'Database backup (*.gbk)|*.gbk|All files (*.*)|*.*'
    Title = 'Select database backup to restore'
    Left = 584
    Top = 91
  end
  object BackupSaveDialog: TSaveDialog
    DefaultExt = '.gbk'
    Filter = 'Database backup (*.gbk)|*.gbk|All files (*.*)|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'Select database backup file'
    Left = 584
    Top = 123
  end
  object IncSearchTimer: TTimer
    Enabled = False
    Interval = 3000
    OnTimer = IncSearchTimerTimer
    Left = 648
    Top = 48
  end
end
