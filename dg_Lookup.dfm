object LookupDlg: TLookupDlg
  Left = 363
  Top = 348
  BorderStyle = bsDialog
  Caption = 'Select item(s)'
  ClientHeight = 273
  ClientWidth = 585
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 120
  TextHeight = 16
  object OKBtn: TButton
    Left = 481
    Top = 7
    Width = 93
    Height = 31
    Anchors = [akTop, akRight]
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object Button2: TButton
    Left = 481
    Top = 56
    Width = 93
    Height = 31
    Anchors = [akTop, akRight]
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object ListBox: TListView
    Left = 8
    Top = 8
    Width = 465
    Height = 257
    Anchors = [akLeft, akTop, akRight, akBottom]
    Columns = <>
    ColumnClick = False
    GridLines = True
    ReadOnly = True
    RowSelect = True
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    ViewStyle = vsReport
    OnDblClick = ListBoxDblClick
  end
end
