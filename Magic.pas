(* magic - magic library for IB Replicator
 * Copyright (C) 2003  Tomas Mandys-MandySoft
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA  02111-1307  USA
 *)

unit Magic;

interface

type
  TOffMagic = packed record
    Magic: array[1..4] of Char;
    Version: Word;
    Envelope: Byte;
  end;

const
  offVersion = $0099;
  offVersionCRC = $0100;
  offVersionV2 = $0101;

procedure FillMagic(var M: TOffMagic; aVer: Word = offVersion);
function CheckMagic(var M: TOffMagic): Boolean;

implementation

const
  offMagic = 'IBRP';

procedure FillMagic(var M: TOffMagic; aVer: Word = offVersion);
begin
  Move(offMagic[1], M.Magic, SizeOf(M.Magic));
  M.Version:= aVer;
end;

function CheckMagic(var M: TOffMagic): Boolean;
var
  S: string;
begin
  SetLength(S, SizeOf(M.Magic));
  Move(M.Magic, S[1], SizeOf(M.Magic));
  Result:= (S = offMagic) and (M.Envelope <= 1);
end;

end.


