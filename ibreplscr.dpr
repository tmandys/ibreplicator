program ibreplscr;
{$APPTYPE CONSOLE}
uses
  SysUtils,
  dm_IBRepl,
  CmdLine,
  {$IFDEF LINUX}
  Libc,
  Types,
  Variants,
  {$ELSE}
  Windows,
  {$IFDEF VER180}
  Variants,
  {$ENDIF}
  {$IFDEF VER170}
  Variants,
  {$ENDIF}
  {$IFDEF VER160}
  Variants,
  {$ENDIF}
  {$IFDEF VER150}
  Variants,
  {$ENDIF}
  {$IFDEF VER140}
  Variants,
  {$ENDIF}
  {$ENDIF}
  Connect,
  Classes,
  IBDatabase,
  IBReplicator,
  dm_IBReplC in 'dm_IBReplC.pas';

{$R IBReplServer.res}

type
  TIBReplDataModule2 = class(TIBReplDataModuleC)
  private
    fSchemaId: Integer;
    fGroupId: Integer;
    fTgtDBId: Integer;
    fScript: string;
    fFromDT, fToDT: TDateTime;
    fFromSeqId, fToSeqId: Integer;
    fUseSnapshot: Boolean;
  public
    constructor Create(aOwner: TComponent); override;
  end;


resourcestring
  sAbout2= 'Process SQL script made by repl.server and writes to source/target database';
  sHelp1 = 'Usage: ibreplscr <script> /D:<cfgdb> /U:<usr> /P:<psw> [/Q:<dial>] [/F:<pfx>]'#13#10+
           '       /S:<schemaid> /G:<groupid> [/B:<dbid>] [/L:<log>] [/NS]'#13#10+
           '       [/D1:<fromDT>] [/D2:<toDT>] [/Q1:<fromSeqId>] [/Q2:<toSeqId]'#13#10+
           '       [/I:<ini>] [/V:<verb>] [/E:<par>=<val>] [/TRACESQL] [/H] [/?]';
  sHelp2 = 'Description:'#13#10+
           '   <script>    SQL script file name'#13#10+
           '   <cfgdb>     configuration database file name'#13#10+
           '   <usr>       login name'#13#10+
           '   <psw>       password'#13#10+
           '   <dial>      SQL dialect'#13#10+
           '   <pfx>       prefix of objects in configuration database'#13#10+
           '   <log>       log file name, default: '+devNull+#13#10+
           '   <ini>       ini file, default: IBREPL.INI in program dir'#13#10+
           '   <verb>      verbose bit 0..repllog, 1..dblog, default: 255'#13#10+
           '   <par>=<val> set environment parameter(s)'#13#10+
           '   /TRACESQL   enable SQL tracing using sqlmonitor'#13#10+
           '   /NS         do not write SEQID to snapshot table'#13#10+
           '   <schemaid>  SchemaId to process'#13#10+
           '   <groupid>   GroupId to process'#13#10+
           '   <dbid>      0 (default) .. process to source database'#13#10+
           '               >0 .. process to target database'#13#10+
           ''#13#10;

  sHelp3 = '  Condition (if no condition all process records in script):'#13#10+
           '   <fromDT>    min stamp YYYYMMDD[hhmmss], default: 0'#13#10+
           '   <toDT>      max stamp YYYYMMDD[hhmmss], default: 0'#13#10+
           '   <fromSeqId> min SEQID, default: SNAPSHOT.SEQID+1'#13#10+
           '   <toSeqId>   max SEQID, default: 0'#13#10+
           ''#13#10+
           'WARNING:'#13#10+
           'Long names containing spaces put as quoted parameter, fex. "/F:REP $"'#13#10;

  sNoScript= 'Missing name of script';
  sBadDT = 'Bad datetime parameter "%s", use datetime in "YYYYMMDD[hhmmss]" format';
  sNoCondition = 'Processing all records (no filter condition)';
  sConditionDT = 'Stamp condition: %s - %s';
  sConditionSeqId = 'SEQID condition: %s - %s';

  sAppTitleS = 'Processing SQL script to source database, SchemaId: %d, GroupId: %d';
  sAppTitleT = 'Processing SQL script to target database, SchemaId: %d, GroupId: %d, DBId: %d';
  sProcessed = 'Total commands %d, processed %d, processed BLOBs %d';

constructor TIBReplDataModule2.Create;
  procedure GetDT(aPar: string; var aDT: TDateTime);
  var
    S: string;
  begin
    S:= '';
    GetCmdString(aPar+':', clUpcase, S);
    if S <> '' then
    begin
      try
        aDT:= EncodeDate(StrToInt(Copy(S, 1, 4)), StrToInt(Copy(S, 5, 2)), StrToInt(Copy(S, 7, 2)));
        if Length(S) > 8 then
          aDT:= aDT + EncodeTime(StrToInt(Copy(S, 9, 2)), StrToInt(Copy(S, 11, 2)), StrToInt(Copy(S, 13, 2)), 0);
      except
        IBReplicatorError(Format(sBadDT, [S]), 2);
      end;
    end;
  end;
var
  S: string;
  DBProps: TReplDatabaseProperties;
  V: Variant;
  DB: TIBDatabase;
begin
  inherited;
  ReadFromIni(False, False);
  GetCmdString('', 0, fScript);
  if fScript = '' then
    IBReplicatorError(sNoScript, 2);
  fScript:= IBReplicator.ParseStr(fScript);

  fSchemaId:= GetParInteger('S', 0);
  fGroupId:= GetParInteger('G', 0);
  fTgtDbId:= GetParInteger('B', 0);

  GetDT('D1', fFromDT);
  GetDT('D2', fToDT);
  S:= '';
  GetCmdString('Q1:', clUpcase, S);
  if S = '' then
    begin
      IBReplicator.ConfigDatabase.Open;
      DB:= TIBDatabase.Create(nil);
      try
        if fTgtDbId = 0 then
          IBReplicator.SetDBParams_Repl(DB, fSchemaId, 0, IBReplicator.GetSourceDBId(fSchemaId), False, DBProps)
        else
          IBReplicator.SetDBParams_Repl(DB, fSchemaId, fGroupId, fTgtDBId, False, DBProps);
        V:= IBReplicator.DBSQLRecord(DB, Format('SELECT SEQID FROM %s WHERE SCHEMAID=%d AND GROUPID=%d AND DBID=%d', [TIBReplicator.FormatIdentifier(DB.SQLDialect, DBProps.ObjPrefix+'SNAPSHOT'), fSchemaID, fGroupId, fTgtDBId]));
        if V <> Null then
          fFromSeqId:= Integer(V)+1;
      finally
        DB.Free;
      end;
    end
  else
    fFromSeqId:= GetParInteger('Q1', fFromSeqId);
  fToSeqId:= GetParInteger('Q2', fToSeqId);

  fUseSnapshot:= not IsThereCmd('NS', clUpcase);
  IBReplicator.ConfigDatabase.Open;
end;

function FmtDT(aDT: TDateTime): string;
begin
  if aDT = 0 then
    Result:= '*'
  else
    Result:= DateTimeToStr(aDT);
end;

function FmtInt(I: Integer): string;
begin
  if I = 0 then
    Result:= '*'
  else
    Result:= IntToStr(I);
end;

var
  St: TStream;
  CntT, CntR, CntB: Integer;
begin
  WriteProgName;
  Writeln2(sAbout2);
  WriteLn2('');
  if IsThereCmd('H', clUpcase) or IsThereCmd('?', clUpcase) then
  begin
    Writeln2(sHelp1);
    WriteLn2('');
    Writeln2(sHelp2+sHelp3);
    Exit;
  end;

  try
    IBReplDataModule:= TIBReplDataModule2.Create(nil);
    try
      try
        with IBReplDataModule as TIBReplDataModule2 do
        begin
          if fTgtDBId = 0 then
            IBReplicator.ReplLog.Log('', lchNull, Format(sAppTitleS, [fSchemaId, fGroupId]))
          else
            IBReplicator.ReplLog.Log('', lchNull, Format(sAppTitleT, [fSchemaId, fGroupId, fTgtDBId]));

          if (fFromDT = 0) and (fToDT = 0) and (fFromSeqId = 0) and (fToSeqId = 0) then
            IBReplicator.ReplLog.Log('', lchNull, sNoCondition);
          if (fFromDT <> 0) or (fToDT <>0) then
            IBReplicator.ReplLog.Log('', lchNull, Format(sConditionDT, [FmtDT(fFromDT), FmtDT(fToDT)]));
          if (fFromSeqId <> 0) or (fToSeqId <> 0) then
            IBReplicator.ReplLog.Log('', lchNull, Format(sConditionDT, [FmtInt(fFromSeqId), FmtInt(fToSeqId)]));

          St:= TFileStream.Create(fScript, fmOpenRead or fmShareDenyWrite);
          try
            CntT:= 0; CntR:= 0; CntB:= 0;
            IBReplicator.ExecSQLScript(fSchemaId, fGroupId, fTgtDBId, fFromSeqId, fToSeqId, fUseSnapshot, fFromDT, fToDT, St, CntT, CntR, CntB);
            IBReplicator.ReplLog.Log('', lchNull, Format(sProcessed, [CntT, CntR, CntB]));

          finally
            St.Free;
          end;
        end;
      except
        on E: Exception do
        begin
          IBReplDataModule.IBReplicator.DBLog.Log('', lchError, E.Message);
          ExitCode:= 1;
        end;
      end;
    finally
      IBReplDataModule.Free;
    end;
  except
    on E: Exception do
    begin
      Writeln2(E.Message);
      ExitCode:= 1;
    end;
  end;
end.
