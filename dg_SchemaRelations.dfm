inherited SchemaRelationsDialog: TSchemaRelationsDialog
  HelpContext = 31510
  Caption = 'Relation properties'
  ClientHeight = 630
  ClientWidth = 604
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  inherited Panel1: TPanel
    Left = 496
    Height = 630
    object Button1: TButton
      Left = 8
      Top = 212
      Width = 92
      Height = 31
      HelpContext = 31520
      Caption = 'Action editor..'
      TabOrder = 3
      OnClick = Button1Click
    end
  end
  inherited Panel2: TPanel
    Width = 496
    Height = 630
    object Label1: TLabel
      Left = 20
      Top = 20
      Width = 32
      Height = 16
      Caption = '&Index'
      FocusControl = DBEdit1
    end
    object Label2: TLabel
      Left = 20
      Top = 79
      Width = 90
      Height = 16
      Caption = '&Source relation'
      FocusControl = DBEdit2
    end
    object Label3: TLabel
      Left = 246
      Top = 79
      Width = 87
      Height = 16
      Caption = '&Target relation'
      FocusControl = DBEdit3
    end
    object Label4: TLabel
      Left = 20
      Top = 130
      Width = 69
      Height = 16
      Caption = 'T&arget type'
      FocusControl = RxDBComboBox1
    end
    object Label5: TLabel
      Left = 20
      Top = 489
      Width = 57
      Height = 16
      Caption = 'Co&mment'
      FocusControl = DBMemo2
    end
    object Label6: TLabel
      Left = 20
      Top = 320
      Width = 56
      Height = 16
      Caption = '&Condition'
      FocusControl = DBMemo1
    end
    object Label7: TLabel
      Left = 20
      Top = 213
      Width = 76
      Height = 16
      Caption = 'Sync.acti&ons'
    end
    object Label8: TLabel
      Left = 159
      Top = 133
      Width = 65
      Height = 16
      Caption = 'S&ync.order'
      FocusControl = DBEdit5
    end
    object Label9: TLabel
      Left = 245
      Top = 130
      Width = 46
      Height = 16
      Caption = '&Options'
      FocusControl = DBEdit4
    end
    object DBEdit1: TDBEdit
      Left = 20
      Top = 39
      Width = 70
      Height = 24
      DataField = 'RELATIONID'
      DataSource = DataSource1
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Left = 20
      Top = 98
      Width = 208
      Height = 24
      DataField = 'RELATIONNAME'
      DataSource = DataSource1
      TabOrder = 1
    end
    object RxDBComboBox1: TRxDBComboBox
      Left = 20
      Top = 150
      Width = 133
      Height = 24
      Style = csDropDownList
      DataField = 'TARGETTYPE'
      DataSource = DataSource1
      EnableValues = True
      ItemHeight = 16
      Items.Strings = (
        'T..table'
        'P..stored procedure')
      TabOrder = 3
      Values.Strings = (
        'T'
        'P')
    end
    object DBEdit3: TDBEdit
      Left = 246
      Top = 98
      Width = 208
      Height = 24
      DataField = 'TARGETNAME'
      DataSource = DataSource1
      TabOrder = 2
    end
    object DBCheckBox2: TDBCheckBox
      Left = 244
      Top = 181
      Width = 208
      Height = 21
      Caption = '&Keep statistic'
      DataField = 'S_KEEP'
      DataSource = DataSource1
      TabOrder = 8
      ValueChecked = 'Y'
      ValueUnchecked = 'N'
    end
    object DBCheckBox3: TDBCheckBox
      Left = 22
      Top = 182
      Width = 208
      Height = 20
      Caption = '&Disabled'
      DataField = 'DISABLED'
      DataSource = DataSource1
      TabOrder = 7
      ValueChecked = 'Y'
      ValueUnchecked = 'N'
    end
    object DBMemo1: TDBMemo
      Left = 20
      Top = 340
      Width = 453
      Height = 141
      DataField = 'CONDITION'
      DataSource = DataSource1
      ScrollBars = ssVertical
      TabOrder = 10
      WordWrap = False
    end
    object DBMemo2: TDBMemo
      Left = 20
      Top = 509
      Width = 453
      Height = 109
      DataField = 'COMMENT'
      DataSource = DataSource1
      ScrollBars = ssVertical
      TabOrder = 11
    end
    object DBEdit5: TDBEdit
      Left = 158
      Top = 150
      Width = 70
      Height = 24
      DataField = 'SYNCORDER'
      DataSource = DataSource1
      TabOrder = 4
    end
    object DBMemo3: TDBMemo
      Left = 20
      Top = 232
      Width = 453
      Height = 84
      DataField = 'SYNCACTIONS'
      DataSource = DataSource1
      ScrollBars = ssVertical
      TabOrder = 9
      WordWrap = False
    end
    object DBEdit4: TDBEdit
      Left = 245
      Top = 150
      Width = 111
      Height = 24
      DataField = 'OPTIONS'
      DataSource = DataSource1
      TabOrder = 5
    end
    object Button2: TButton
      Left = 355
      Top = 152
      Width = 21
      Height = 23
      Caption = '...'
      TabOrder = 6
      TabStop = False
      OnClick = Button2Click
    end
  end
  inherited Table: TIBDataSet
    SelectSQL.Strings = (
      'SELECT * FROM'
      'RELATIONS'
      '')
    Left = 93
    Top = 16
    object TableSCHEMAID: TIntegerField
      FieldName = 'SCHEMAID'
      Required = True
    end
    object TableRELATIONID: TIntegerField
      FieldName = 'RELATIONID'
      Required = True
    end
    object TableRELATIONNAME: TIBStringField
      FieldName = 'RELATIONNAME'
      OnChange = TableRELATIONNAMEChange
      Size = 100
    end
    object TableGROUPID: TIntegerField
      FieldName = 'GROUPID'
      Required = True
    end
    object TableTARGETNAME: TIBStringField
      FieldName = 'TARGETNAME'
      Size = 100
    end
    object TableTARGETTYPE: TIBStringField
      FieldName = 'TARGETTYPE'
      Required = True
      Size = 1
    end
    object TableDISABLED: TIBStringField
      FieldName = 'DISABLED'
      Size = 1
    end
    object TableOPTIONS: TIntegerField
      FieldName = 'OPTIONS'
    end
    object TableS_KEEP: TIBStringField
      FieldName = 'S_KEEP'
      Required = True
      Size = 1
    end
    object TableSYNCACTIONS: TMemoField
      FieldName = 'SYNCACTIONS'
      BlobType = ftMemo
      Size = 8
    end
    object TableSYNCORDER: TIntegerField
      FieldName = 'SYNCORDER'
    end
    object TableCONDITION: TMemoField
      FieldName = 'CONDITION'
      BlobType = ftMemo
      Size = 8
    end
    object TableCOMMENT: TMemoField
      FieldName = 'COMMENT'
      BlobType = ftMemo
      Size = 8
    end
    object TableS_INSERT: TIntegerField
      FieldName = 'S_INSERT'
      Required = True
    end
    object TableS_UPDATE: TIntegerField
      FieldName = 'S_UPDATE'
      Required = True
    end
    object TableS_DELETE: TIntegerField
      FieldName = 'S_DELETE'
      Required = True
    end
    object TableS_CONFLICT: TIntegerField
      FieldName = 'S_CONFLICT'
      Required = True
    end
    object TableS_ERROR: TIntegerField
      FieldName = 'S_ERROR'
      Required = True
    end
    object TableS_MSEC: TIntegerField
      FieldName = 'S_MSEC'
      Required = True
    end
  end
  inherited DataSource1: TDataSource
    Left = 112
    Top = 32
  end
end
