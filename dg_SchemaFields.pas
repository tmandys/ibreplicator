(* IB Replication Suite
 * Copyright (C) 2002,2003 by Tomas Mandys-MandySoft
 *)

unit dg_SchemaFields;

interface

uses
  {$IFDEF LINUX}
  Libc, Qt, QGraphics, QForms, QControls, QStdCtrls, QButtons, QExtCtrls, QDialogs, QMask, QDBCtrls, QRxDBComb,
  {$ELSE}
  Windows, Messages, Graphics, Forms, Controls, StdCtrls, Buttons, ExtCtrls, Dialogs, Mask, DBCtrls, RxDBComb,
  {$ENDIF}
  SysUtils, Classes, dg_SchemaTemplate, Db, IBCustomDataSet;

type
  TSchemaFieldsDialog = class(TSchemaTemplateDialog)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    TableSCHEMAID: TIntegerField;
    TableGROUPID: TIntegerField;
    TableRELATIONID: TIntegerField;
    TableFIELDID: TIntegerField;
    TableFIELDNAME: TIBStringField;
    TableFIELDTYPE: TIntegerField;
    TableTARGETNAME: TIBStringField;
    RxDBComboBox1: TRxDBComboBox;
    DBEdit3: TDBEdit;
    Label7: TLabel;
    DBEdit4: TDBEdit;
    Button1: TButton;
    TableOPTIONS: TIntegerField;
    procedure TableNewRecord(DataSet: TDataSet);
    procedure TableFIELDNAMEChange(Sender: TField);
    procedure TableBeforeEdit(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure DataSource1DataChange(Sender: TObject; Field: TField);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Button1Click(Sender: TObject);
  private
    fLastFieldName: string;
    { Private declarations }
  public
    SchemaType: Integer;
  end;

var
  SchemaFieldsDialog: TSchemaFieldsDialog;

implementation
uses
  IBReplicator, dg_SchemaFieldsConflictOptions;

{$IFDEF LINUX}
{$R *.xfm}
{$ELSE}
{$R *.dfm}
{$ENDIF}

procedure TSchemaFieldsDialog.TableNewRecord(DataSet: TDataSet);
begin
  inherited;
  with DataSet do
  begin
    FieldByName('FIELDTYPE').asInteger:= 3;
    FieldByName('OPTIONS').asInteger:= 0;
  end;
end;

procedure TSchemaFieldsDialog.TableFIELDNAMEChange(Sender: TField);
begin
  inherited;
  if fLastFieldName = Sender.DataSet.FieldByName('TARGETNAME').asString then
    Sender.DataSet.FieldByName('TARGETNAME').asString:= Sender.asString;
  fLastFieldName:= Sender.asString;
end;

procedure TSchemaFieldsDialog.TableBeforeEdit(DataSet: TDataSet);
begin
  inherited;
  fLastFieldName:= DataSet.FieldByName('FIELDNAME').asString;
end;

procedure TSchemaFieldsDialog.FormShow(Sender: TObject);
begin
  inherited;
  DBEdit3.Enabled:= SchemaType = schtReplication;
end;

procedure TSchemaFieldsDialog.DataSource1DataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  DBEdit4.Enabled:= SchemaType = schtReplication; 
  Button1.Enabled:= DBEdit4.Enabled;
end;

procedure TSchemaFieldsDialog.DBEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = {$IFDEF LINUX}Key_down{$ELSE}VK_DOWN{$ENDIF}) and ([ssAlt] = Shift) then
  begin
    Button1Click(nil);
    Key:= 0;
  end;
end;

procedure TSchemaFieldsDialog.Button1Click(Sender: TObject);
begin
  inherited;
  with TSchemaFieldsConflictOptionsDialog.Create(Self) do
  try
    FieldType:= Table.FieldByName('FIELDTYPE').asInteger;
    Options:= StrToInt(DBEdit4.Text);
    if ShowModal = mrOk then
    begin
      Table.Edit;
      DBEdit4.Text:= IntToStr(Options);
    end;
  finally
    Release;
  end;
end;

end.
