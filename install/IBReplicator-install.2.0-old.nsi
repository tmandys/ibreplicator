; IBReplicator instalation script
!define PROJ_DIR "e:\proj\ibrepl"
!define DATA_DIR "e:\data\ibrepl"
;!define OUT_DIR "e:\products\ibrepl";
!define OUT_DIR "e:\proj\ibrepl\install"

!define APP_NAME "Interbase Replicator Suite"
!define APP_VERSION "2.0 beta"
!define APP_HOME_PAGE "http://www.2p.cz/en/interbase_replicator/"
!define APP_NICK "IBReplicator";

!define NSIS_CONFIG_LOG install.log
!include Sections.nsh

Name "${APP_NAME} ${APP_VERSION}"
OutFile "${OUT_DIR}\${APP_NICK}_install_${APP_VERSION}.exe"
Caption "${APP_NAME} -installation "

Icon "${NSISDIR}\Contrib\Graphics\Icons\classic-install.ico"
UninstallIcon "${NSISDIR}\Contrib\Graphics\Icons\classic-uninstall.ico"

CRCCheck on
SetCompress auto
SetDatablockOptimize on
AutoCloseWindow false
ShowInstDetails show
ShowUninstDetails show
SetDateSave on
AllowRootDirInstall false
; LogSet on   musi byt NSIS_CONFIG_LOG, nevim jak

InstallDir "$PROGRAMFILES\${APP_NICK}"
InstallDirRegKey HKEY_LOCAL_MACHINE "SOFTWARE\MandySoft\${APP_NICK}\InstallDir" ""

page license
page components
page directory
page instfiles

SubCaption 0 " [1]"
SubCaption 1 " [2]"
SubCaption 2 " [3]"
SubCaption 3 " [4]"
SubCaption 4 " [5]"

;InstType "Kompletn�"
;InstType "/CUSTOMSTRING=U�ivatelsk�"

LicenseData "${PROJ_DIR}\licence.txt"

;DirShow show

;SpaceTexts "Po�adovan� m�sto:" "Voln� m�sto:"
;LicenseText "U�ivatelsk� licence" "Souhlas�m"
;DirText "Adres��, do kter�ho bude nainstalov�n software Merkur" "Adres��" "Vybrat..."
;ComponentText "Komponenty software Merkur, kter� maj� b�t nainstalov�ny" " " "Vyberte, komponenty"
;CompletedText "Hotovo"

;SubCaption 0 " [1]"
;SubCaption 1 " [2]"
;SubCaption 2 " [3]"
;SubCaption 3 " [4]"
;SubCaption 4 " [5]"

;InstallButtonText "Instaluj"
;MiscButtonText "P�edchoz�" "Dal��" "Konec" "Zav��t"

;FileErrorText "Nelze zapisovat do souboru $\r$\n$0."

;UninstallText "Lok�ln� instalace software Merkur bude odinstalov�na ze stanice" "Odinstalovat z:"
;UninstallCaption "Merkur-odinstalace"
;UninstallSubCaption 0 " [1]"
;UninstallSubCaption 1 " [2]"
;UninstallSubCaption 2 " [3]"
;UninstallButtonText "Odinstalovat"
;DetailsButtonText "Podrobnosti"

Section "" 
SetOutPath "$INSTDIR"
CreateDirectory "$INSTDIR"
WriteRegStr HKEY_LOCAL_MACHINE "SOFTWARE\MandySoft\${APP_NICK}" "" "$INSTDIR"
WriteRegStr HKEY_LOCAL_MACHINE "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP_NICK}" "DisplayName" "${APP_NAME} (remove only)"
WriteRegStr HKEY_LOCAL_MACHINE "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP_NICK}" "UninstallString" '"$INSTDIR\uninst.exe"'
WriteUninstaller "$INSTDIR\uninst.exe"
File /oname=readme.txt "${PROJ_DIR}\IBReplicator.txt"
;File "${PROJ_DIR}\IBReplMan.exe"
;File "${PROJ_DIR}\IBReplServer.exe"
;File "${PROJ_DIR}\IBReplc.exe"
;File "${PROJ_DIR}\IBReplscr.exe"
;File "${PROJ_DIR}\IBReplPackage2XML.exe"
;File "${PROJ_DIR}\IBReplInst.exe"
;File "${PROJ_DIR}\transfer_netdir.dll"
;File "${PROJ_DIR}\transfer_ftp.dll"
;File "${PROJ_DIR}\transfer_email.dll"
;File "${PROJ_DIR}\enc_cmdline_email.dll"
;File "${PROJ_DIR}\ibrepludf.dll"

CreateDirectory "$SMPROGRAMS\${APP_NICK}"
;CreateShortCut "$SMPROGRAMS\${APP_NICK}\IBReplication Manager.lnk" "${INSTDIR}\IBReplMan.exe"
;CreateShortCut "$SMPROGRAMS\${APP_NICK}\IBReplication Server.lnk" "${INSTDIR}\IBReplMan.exe"
CreateShortCut "$SMPROGRAMS\${APP_NICK}\Uninstall ${APP_NAME}.lnk" "$INSTDIR\uninst.exe"
CreateShortCut "$SMPROGRAMS\${APP_NICK}\${APP_NAME} Homepage.lnk" "${APP_HOME_PAGE}"
CreateShortCut "$SMPROGRAMS\${APP_NICK}\README file.lnk" "$INSTDIR\readme.txt"

ReadRegStr $R1 HKEY_LOCAL_MACHINE "SOFTWARE\Borland\Interbase\CurrentVersion" "RootDirectory"
StrCmp $R1 "" IB_No
  DetailPrint 'Local Interbase/Firebird server detected at "$R1"'
  IfFileExists '$R1\ibrepludf.dll' IB_Ex

  CopyFiles /SILENT "$INSTDIR\ibrepludf.dll" "$R1\udf\"
  ifErrors IB_Err
  DetailPrint 'ibrepludf.dll copied to "$R1\udf\"'
  goto IB_Skip
IB_Ex:
  CopyFiles /SILENT "$R1\udf\ibrepludf.dll already exists" "$R1\udf"
  goto IB_Skip
IB_Err:
  DetailPrint 'ibrepludf.dll not copied to "$R1\udf", see README.txt'
  goto IB_Skip
IB_No:
  DetailPrint "Local Interbase/Firebird server not detected"
  DetailPrint 'You must enable using UDF library "ibrepludf" UDF library by Interbase/Firebird SQL server, read README.txt'
IB_Skip:

SectionEnd

Section "Example databases"
SectionIn 1 2

SetOutPath "$INSTDIR"
File /oname=ibrepl.ini "${PROJ_DIR}\IBRepl.ini.default"
WriteINIStr "$INSTDIR\ibrepl.ini" "IBReplicator.Environment" "DATADIR" "$INSTDIR\Data"

SetOutPath "$INSTDIR\Data"

File "${DATA_DIR}\Employee.gdb"
CopyFiles /SILENT "$INSTDIR\Data\Employee.gdb" "Employee2.gdb"
CopyFiles /SILENT "$INSTDIR\Data\Employee.gdb" "Employee3.gdb"
File /oname=employee-cfg.gdb "${DATA_DIR}\rc.gdb"
SectionEnd

Section "Delphi SDK files"
SectionIn 1 2
SetOutPath "$INSTDIR\Delphi"

SectionEnd

Section "Uninstall"

Delete "$INSTDIR\uninst.exe"
DeleteRegKey HKEY_LOCAL_MACHINE "SOFTWARE\MandySoft\${APP_NICK}"
DeleteRegKey HKEY_LOCAL_MACHINE "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${APP_NICK}"

Delete "$INSTDIR\IBReplMan.exe"
Delete "$INSTDIR\IBReplServer.exe"
;Delete "$INSTDIR\ibrepl.ini"
Delete "$INSTDIR\readme.txt"
RMDir "$INSTDIR"

Delete "$SMPROGRAMS\${APP_NICK}\Uninstall.lnk"
Delete "$SMPROGRAMS\${APP_NICK}\IBReplMan.lnk"
Delete "$SMPROGRAMS\${APP_NICK}\IBReplServer.lnk"
Delete "$SMPROGRAMS\${APP_NICK}\Homepage.lnk"
Delete "$SMPROGRAMS\${APP_NICK}\README file.lnk"
RMDir "$SMPROGRAMS\${APP_NICK}"

RMDir "$SMPROGRAMS\${APP_NICK}\Data"

SectionEnd

;`Function .onUserAbort
;  MessageBox MB_YESNO "Abort installation?" IDYES NoCancelAbort
;    Abort
;  NoCancelAbort:
;FunctionEnd

;Function .onInstSuccess
;  MessageBox MB_YESNO|MB_ICONQUESTION "Instalace prob�hla �sp�n�. Checete zobrazit soubor CTIMNE.TXT?" IDNO NoReadme
;    ExecShell open '$INSTDIR\ctimne.txt'
;  NoReadme:
;FunctionEnd


;Function .onInstFailed
;  MessageBox MB_OK "Instalace nebyla �sp�n�."
;FunctionEnd

function .onInit

  InitPluginsDir
  File /oname=$PLUGINSDIR\dbuser.ini dbuser.ini


;  SectionGetFlags ${Sec_Runtime} $R0
;  IntOp $R0 $R0 & ${SECTION_OFF}
;  SectionSetFlags ${Sec_Runtime} $R0

;  SectionGetFlags ${Sec_DAO} $R0
;  IntOp $R0 $R0 & ${SECTION_OFF}
;  SectionSetFlags ${Sec_DAO} $R0

FunctionEnd

Function .onSelChange

;  SectionGetFlags ${Sec_Runtime} $R0
;  SectionGetFlags ${Sec_DAO} $R1

  IntOp $R2 $R0 & ${SF_SELECTED}
  IntOp $R3 $R1 & ${SF_SELECTED}

 
; nejak mi nefunguje ~${SF_RO} 
  IntOp $R4 0xFFFFFFFF ^ ${SF_RO}

  IntOp $R0 $R0 & $R4
  IntOp $R1 $R1 & $R4

  IntCmp $R2 ${SF_SELECTED} 0 +3 +3
  IntOp $R1 $R1 & ${SECTION_OFF} 
  IntOp $R1 $R1 | ${SF_RO} 

  IntCmp $R3 ${SF_SELECTED} 0 +3 +3
  IntOp $R0 $R0 & ${SECTION_OFF}
  IntOp $R0 $R0 | ${SF_RO} 

;  SectionSetFlags ${Sec_Runtime} $R0
;  SectionSetFlags ${Sec_DAO} $R1

FunctionEnd

Function un.onInit
  MessageBox MB_YESNO "Are you sure, Opravdu chcete kompletn� odinstalovat Merkur ze stanice?" IDYES NoAbort
    Abort
  NoAbort:
FunctionEnd

