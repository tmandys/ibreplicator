LangString TEXT_ExecResult ${LANG_ENGLISH} "result: "

!macro ExecCmdLine_ ResultVar Uninst Command Prefix
    nsExec::ExecToStack `${Command}`
    Pop `${ResultVar}` # return value/error/timeout
    Pop $R1 # printed text, up to ${NSIS_MAX_STRLEN}

;    ExecWait '"$INSTDIR\IBReplServerSvc.exe" /INSTALL' $R0
    IntCmp `${ResultVar}` 0 +2
    DetailPrint `${Prefix}$(TEXT_ExecResult)${ResultVar}`

;   OEM -> Ansi conversion should do
;   CallDllInst OEMToChar of user32
    !ifdef CmdLine_UNINSTALL
      ${UnStrRep} $R1 $R1 $\r ""
    !else
      ${StrRep} $R1 $R1 $\r ""
    !endif
  loop:
    Push $R1
    !ifdef CmdLine_UNINSTALL
    StrCmp `${Uninst}` "0" +2
      ${UnStrLoc} $R0 $R1 "$\n" ">"
    !else
      ${StrLoc} $R0 $R1 "$\n" ">"
    !endif
    Pop $R1

    StrCmp $R0 "" 0 +2
    StrLen $R0 $R1

    StrCpy $R2 $R1 $R0
    IntOp $R0 $R0 + 1
    StrCpy $R1 $R1 ${NSIS_MAX_STRLEN} $R0

    StrCmp $R2 "" +2
    DetailPrint `${Prefix}$R2`

    StrCmp $R1 "" 0 loop

!macroend

!macro ExecCmdLine ResultVar Command Prefix ; command log_prefix returns: exitcode
  !insertmacro ExecCmdLine_ `${ResultVar}` "0" `${Command}` `${Prefix}`
!macroend

!macro unExecCmdLine ResultVar Command Prefix ; command log_prefix returns: exitcode
  !define CmdLine_UNINSTALL
  !insertmacro ExecCmdLine_ `${ResultVar}` "1" `${Command}` `${Prefix}`
  !undef CmdLine_UNINSTALL
!macroend

