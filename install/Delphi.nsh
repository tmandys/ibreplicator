LangString TEXT_DELPHI_PACKAGE_REGISTER ${LANG_ENGLISH} "Package: Register: Delphi:"
LangString TEXT_DELPHI_PACKAGE_SKIP ${LANG_ENGLISH} "Package: Skip: Delphi:"

Function FUNCTION_Delphi_InstallPackage
  Pop $0  ; version
  Pop $1  ; name
  Pop $2  ; description
  Pop $3  ; location

  Push $R0
  Push $R1
  Push $R2

  StrCpy $R1 "0"
loop:
  EnumRegValue $R0 HKCU "Software\Borland\Delphi\$0\Known Packages" $R1
  IntOp $R1 $R1 + 1
  StrCmp $R0 "" 0 checkkey   ; last key ?
    StrCpy $R0 "$3\$1"
    WriteRegStr HKCU "Software\Borland\Delphi\$0\Known Packages" $R0 $2
    Detailprint "$(TEXT_DELPHI_PACKAGE_REGISTER) $0: $1"
    goto skip
checkkey:
    ${StrLoc} $R2 $R0 $1 ">"
    StrCmp $R2 "" loop
    Detailprint "$(TEXT_DELPHI_PACKAGE_SKIP) $0: $1"
skip:
  Pop $R2
  Pop $R1
  Pop $R0
FunctionEnd

!macro Delphi_InstallPackage Version Name Description Location

  Push $0
  Push $1
  Push $2
  Push $3
  Push `${Location}`
  Push `${Description}`
  Push `${Name}`
  Push `${Version}`
  Call FUNCTION_Delphi_InstallPackage
  Pop $3
  Pop $2
  Pop $1
  Pop $0

!macroend

