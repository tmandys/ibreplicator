; NSIS Modern User Interface version 1.70

; IBReplicator instalation script
  !define PROJ_dir "e:\proj\ibrepl"
  !define UNIT_DIR "e:\lib\unit.pas"
  !define LIB_DIR "e:\lib\delphi3\vcl"
  !define PRODUCT_DIR "e:\products\IBRepl_2_0"

  !define DATA_DIR "e:\data\ibrepl"
  !define OUT_DIR "e:\products"
;  !define OUT_DIR "e:\proj\ibrepl\install"

  !define APP_NAME "Interbase Replication Suite"
  !define APP_VERSION "2.0"
  !define APP_BUILD "build_25.1"
  !define APP_HOME_PAGE "http://www.2p.cz/en/interbase_replicator/"
  !define APP_DLL_VER ".150"
  !define APP_NICK "IBReplicator"
  !define APP_REG_FILE "IBReplicator2.key"
  !define APP_SDKIDE_PACKAGE1 "MandySoft_Utils.bpl"
  !define APP_SDKIDE_PACKAGE2 "MandySoft_VCL.bpl"
  !define APP_SDKIDE_PACKAGE3 "MandySoft_IBReplicator.bpl"
  !define APP_IBREPL_UDF "ib_repl"

  !define NSIS_CONFIG_LOG install.log

;---------------------
;Include Modern UI

  !include "MUI.nsh"
  !include "Sections.nsh"
  !include "UpgradeDLL.nsh"
  !include "StrFunc.nsh"

;define StrFunc macro
  ${StrRep}
  ${StrLoc}
  
  ${UnStrStr}  ; uninstall support
  ${UnStrRep}
  ${UnStrLoc}

  !include "execCmdline.nsh"
  !include "Delphi.nsh"

;General
  Name "${APP_NAME} ${APP_VERSION}"
  OutFile "${OUT_DIR}\${APP_NICK}_${APP_VERSION}.${APP_BUILD}.win.exe"
;  Caption "${APP_NAME} -installation "

  ;Default installation folder
  InstallDir "$PROGRAMFILES\${APP_NICK} ${APP_VERSION}"
  ;Get installation folder from registry if available
  InstallDirRegKey HKLM "SOFTWARE\MandySoft\${APP_NICK}\${APP_VERSION}" "InstallDir"
;  !define MUI_ICON "..\I1x.ico"
  !define MUI_FINISHPAGE_NOAUTOCLOSE


  CRCCheck on
  SetCompress auto
  SetDatablockOptimize on
  AutoCloseWindow false
  ShowInstDetails show
  ShowUninstDetails show
  SetDateSave on
  AllowRootDirInstall false

;--------------------------------
;Variables
  Var MUI_TEMP
  Var STARTMENU_FOLDER
  var hwnd            ; custom window handle
  var Reg_Loaded
  var Reg_Licence
  var Do_Registration
  var Do_Copy_UDF
  var Do_Install_Service
  var Interbase_Folder
  Var INI_SYSDBA
  Var INI_PASSWORD
  var SDKIDE_Install

;--------------------------------
;Pages
  InstType $(TYPE_Full)
  InstType $(TYPE_Online)
  InstType $(TYPE_Offline)


  !insertmacro MUI_PAGE_WELCOME
  !insertmacro MUI_PAGE_LICENSE "${PROJ_DIR}\License.txt"
  !insertmacro MUI_PAGE_COMPONENTS
  !insertmacro MUI_PAGE_DIRECTORY
  
  ;Start Menu Folder Page Configuration
  !define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKCU"
  !define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\MandySoft\${APP_NICK}\${APP_VERSION}"
  !define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "Start Menu Folder"
  !define MUI_PAGE_CUSTOMFUNCTION_PRE StartMenu_pre

  !insertmacro MUI_PAGE_STARTMENU Application $STARTMENU_FOLDER

  Page custom registration_create registration_leave

  Page custom SDKIDE_create SDKIDE_leave

  Page custom dbuser_create dbuser_leave

  Page custom beforeInstall_create beforeInstall_leave

  !insertmacro MUI_PAGE_INSTFILES

  !define MUI_FINISHPAGE_SHOWREADME $INSTDIR\readme.txt
  !define MUI_FINISHPAGE_LINK "${APP_HOME_PAGE}"
  !insertmacro MUI_PAGE_FINISH


  !insertmacro MUI_UNPAGE_CONFIRM
;  !insertmacro MUI_UNPAGE_COMPONENTS
  !insertmacro MUI_UNPAGE_INSTFILES

;--------------------------------
;Interface Settings

  !define MUI_ABORTWARNING
  
;--------------------------------
;Languages
 
  !insertmacro MUI_LANGUAGE "English"

;--------------------------------
;Reserve Files
  
  ;These files should be inserted before other files in the data block
  ;Keep these lines before any File command
  ;Only for solid compression (by default, solid compression is enabled for BZIP2 and LZMA)
  
  ReserveFile "ioDBUser.ini"
  ReserveFile "ioRegistration.ini"
  ReserveFile "ioSDKIDE.ini"
  ReserveFile "ioBeforeInstall.ini"
  !insertmacro MUI_RESERVEFILE_INSTALLOPTIONS

;--------------------------------
;Installer Sections
Section "" SecGeneral

  SetOutPath "$INSTDIR"
  File /oname=readme.txt "${PROJ_DIR}\IBReplicator.txt"
  File "${PROJ_DIR}\License.txt"
  File "${PROJ_DIR}\l-LesserGPL.txt"
  File "${PROJ_DIR}\hlp\Help\Out\IBRepl2.hlp"

  ;Store installation folder
  WriteRegStr HKLM "SOFTWARE\MandySoft\${APP_NICK}\${APP_VERSION}" "InstallDir" $INSTDIR

  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"

  ; write uninstall strings
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP_NICK} ${APP_VERSION}" "DisplayName" "$(TEXT_Uninstaller)"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP_NICK} ${APP_VERSION}" "UninstallString" '"$INSTDIR\uninstall.exe"'

  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application

    ;Create shortcuts
    CreateDirectory "$SMPROGRAMS\$STARTMENU_FOLDER"
    CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\IBReplicator help.lnk" "$INSTDIR\IBRepl2.hlp"
    CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\Home page.lnk" ${APP_HOME_PAGE}
    CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\Read me.lnk" "$INSTDIR\readme.txt"
    CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\Uninstall.lnk" "$INSTDIR\Uninstall.exe"

  !insertmacro MUI_STARTMENU_WRITE_END

SectionEnd

SubSection /e $(TITLE_SecBasic) SecBasic

Section $(TITLE_SecBasic_Man) SecBasic_Man
  SectionIn 1
    SetOutPath "$INSTDIR"
  
    File "${PRODUCT_DIR}\IBReplMan.exe"

    !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
      ;Create shortcuts
      CreateDirectory "$SMPROGRAMS\$STARTMENU_FOLDER"
      CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\IBReplication Manager.lnk" "$INSTDIR\IBReplMan.exe"
    !insertmacro MUI_STARTMENU_WRITE_END

  SectionEnd

  Section $(TITLE_SecBasic_Server) SecBasic_Server
  SectionIn 1 2 3
    SetOutPath "$INSTDIR"

    File "${PRODUCT_DIR}\IBReplServer.exe"


    !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
      ;Create shortcuts
      CreateDirectory "$SMPROGRAMS\$STARTMENU_FOLDER"
      CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\IBReplication Server.lnk" "$INSTDIR\IBReplServer.exe"
    !insertmacro MUI_STARTMENU_WRITE_END

  SectionEnd

  Section $(TITLE_SecBasic_ServerNT) SecBasic_ServerNT
  SectionIn 1
    SetOutPath "$INSTDIR"
    File "${PRODUCT_DIR}\IBReplServerSvc.exe"

    StrCmp $Do_Install_Service "1" 0 skip_inst_service

    DetailPrint "$(TEXT_NTService)$(TEXT_NTServiceInstalling)"
    
    ; exec and capture output
    !insertmacro ExecCmdLine $R0 '"$INSTDIR\IBReplServerSvc.exe" /INSTALL' $(TEXT_NTService)
  skip_inst_service:
  SectionEnd

  Section $(TITLE_SecBasic_CmdLine) SecBasic_CmdLine
  SectionIn 1
    SetOutPath "$INSTDIR"

    File "${PRODUCT_DIR}\ibreplc.exe"
    File "${PRODUCT_DIR}\ibreplinst.exe"
    File "${PRODUCT_DIR}\IBReplPackage2XML.exe"
    File "${PRODUCT_DIR}\ibreplscr.exe"

    File "${PROJ_DIR}\offpackage.dtd"

  SectionEnd

  Section "-Registration"  ; invisible, cannot take from resource

    ; do registration only if selected a executable
    IntCmp $Do_Registration 0 skip_save
    ; registration

    FileOpen $R0 "$WINDIR\${APP_REG_FILE}" "r"
    FileRead $R0 $R1
    FileClose $R0

    StrCmp $R1 $Reg_Licence skip_save
    DetailPrint $(TEXT_LicenceRegistering)
    FileOpen $R0 "$WINDIR\${APP_REG_FILE}" "w"
    FileWrite $R0 $Reg_Licence
    FileClose $R0
  skip_save:

  SectionEnd

SubSectionEnd


Subsection $(TITLE_SecOffline) SecOffline

  Section $(TITLE_SecOffline_FTP) SecOffline_FTP
  SectionIn 1 3
    SetOutPath "$INSTDIR"
    File "${PRODUCT_DIR}\transfer_ftp${APP_DLL_VER}.dll"
  SectionEnd

  Section $(TITLE_SecOffline_Email) SecOffline_Email
  SectionIn 1
    SetOutPath "$INSTDIR"
    File "${PRODUCT_DIR}\transfer_email${APP_DLL_VER}.dll"
  SectionEnd

  Section $(TITLE_SecOffline_NetDir) SecOffline_NetDir
  SectionIn 1
    SetOutPath "$INSTDIR"
    File "${PRODUCT_DIR}\transfer_netdir${APP_DLL_VER}.dll"
  SectionEnd

  Section $(TITLE_SecOffline_Cmdline) SecOffline_CmdLine
  SectionIn 1
    SetOutPath "$INSTDIR"
    File "${PRODUCT_DIR}\enc_cmdline${APP_DLL_VER}.dll"
  SectionEnd

SubSectionEnd

Section $(TITLE_SecUDF) SecUDF
SectionIn 1 2 3

  SetOutPath $INSTDIR
  File "${PROJ_DIR}\UDF\c\${APP_IBREPL_UDF}.dll"
  File "${PROJ_DIR}\UDF\c\${APP_IBREPL_UDF}.so"

  StrCmp $Interbase_Folder "" IB_No
  DetailPrint $(TEXT_UDFInterbaseDetected)

  StrCmp $Do_Copy_UDF "1" 0 IB_Skip
  
  DetailPrint $(TEXT_UDFChecking)
  !define UPGRADEDLL_NOREGISTER
  !insertmacro UpgradeDLL "${PROJ_DIR}\UDF\c\${APP_IBREPL_UDF}.dll" "$Interbase_Folder\udf\${APP_IBREPL_UDF}.dll" $SYSDIR
  goto IB_Skip
IB_No:
  DetailPrint $(TEXT_UDFInterbaseNotDetected)
  DetailPrint $(TEXT_UDFReadme)
IB_Skip:

SectionEnd

Section $(TITLE_SecSDK) SecSDK
SectionIn 1
  SetOutPath "$INSTDIR\SDK"

  SetOutPath "$INSTDIR\SDK\DTD"
  File "${PROJ_DIR}\conflict.dtd"
  File "${PROJ_DIR}\offpackage.dtd"

  SetOutPath "$INSTDIR\SDK\Offline"
  File "${PROJ_DIR}\enc_cmdline.dpr"
  File "${PROJ_DIR}\enc_cmdline.res"
  File "${PROJ_DIR}\Transfer.inc"
  File "${PROJ_DIR}\Transfer.pas"
  File "${PROJ_DIR}\transfer_email.dpr"
  File "${PROJ_DIR}\transfer_email.res"
  File "${PROJ_DIR}\transfer_ftp.dpr"
  File "${PROJ_DIR}\transfer_ftp.res"
  File "${PROJ_DIR}\transfer_netdir.dpr"
  File "${PROJ_DIR}\transfer_netdir.res"

  SetOutPath "$INSTDIR\SDK\Lib"
  File "${PROJ_DIR}\encoder.inc"
  File "${PROJ_DIR}\encoder.pas"
  File "${PROJ_DIR}\Magic.pas"
  File "${PROJ_DIR}\ToolUtils.inc"
  File "${PROJ_DIR}\IBDataSet2.pas"
  File "${UNIT_DIR}\SystemD6.pas"
  File "${UNIT_DIR}\Crc32.pas"
  File "${LIB_DIR}\Connect.pas"

  File "${PROJ_DIR}\hlp\Pretty\Out\IBReplicator.txt"
  File "${PROJ_DIR}\hlp\Pretty\Out\IBReplicator.int"

; add only compiled desing packages (registration includes)
; File "${PROJ_DIR}\IBReplicator.dcr"
; do not include DPK, compilable but
;  File "${PROJ_DIR}\MandySoft_IBReplicator.dpk"

  SetOutPath "$INSTDIR\SDK\Lib\Hlp"
  File "${LIB_DIR}\Hlp\Help\Out\MandySoft_Utils.hlp"
  File "${LIB_DIR}\Hlp\Help\Out\MandySoft_VCL.hlp"

  
  SetOutPath "$INSTDIR\SDK\Lib\Delphi5"
  File "${PRODUCT_DIR}\SDK\Lib\Delphi5\*.*"

  SetOutPath "$INSTDIR\SDK\Lib\Delphi6"
  File "${PRODUCT_DIR}\SDK\Lib\Delphi6\*.*"

  SetOutPath "$INSTDIR\SDK\Lib\Delphi7"
  File "${PRODUCT_DIR}\SDK\Lib\Delphi7\*.*"

  SetOutPath "$INSTDIR\SDK\UDF"
  File "${PROJ_DIR}\udf\ib_repl.sql"
  SetOutPath "$INSTDIR\SDK\UDF\delphi"
  File "${PROJ_DIR}\udf\delphi\ib_repl.dpr"
  File "${PROJ_DIR}\udf\delphi\ib_repl.res"
  File "${PROJ_DIR}\udf\delphi\IBUDFUtils.pas"

  SetOutPath "$INSTDIR\SDK\UDF\c"
  File "${PROJ_DIR}\udf\c\ib_repl.c"
  File "${PROJ_DIR}\udf\c\ib_repl.bcc.def"
  File "${PROJ_DIR}\udf\c\ib_repl.msv.def"
 ; File "${PROJ_DIR}\udf\c\ib_util.c"
;  File "${PROJ_DIR}\udf\c\ib_util.h"
  File "${PROJ_DIR}\udf\c\makefile.gcc"
  File "${PROJ_DIR}\udf\c\makefile.bcc.mak"
  File "${PROJ_DIR}\udf\c\makefile.msv.bat"
;  File "${PROJ_DIR}\udf\c\ib_util.pas"
;  File "${PROJ_DIR}\udf\c\msvcrt.bcc.lib"

  StrCpy $R2 "5"
  StrCpy $R3 "1"
SDKIDE_Loop3:
  IntOp $R0 $SDKIDE_Install & $R3
  IntCmp $R0 0 SDKIDE_Skip3
  !insertmacro Delphi_InstallPackage "$R2.0" ${APP_SDKIDE_PACKAGE1} "Mandysoft common utils" "$INSTDIR\SDK\Lib\Delphi$R2"
  !insertmacro Delphi_InstallPackage "$R2.0" ${APP_SDKIDE_PACKAGE2} "Mandysoft VCL utils" "$INSTDIR\SDK\Lib\Delphi$R2"
  !insertmacro Delphi_InstallPackage "$R2.0" ${APP_SDKIDE_PACKAGE3} "Mandysoft IBReplicator" "$INSTDIR\SDK\Lib\Delphi$R2"
SDKIDE_Skip3:
  IntOp $R2 $R2 + 1
  IntOp $R3 $R3 * 2
  IntCmp $R2 7 SDKIDE_Loop3 SDKIDE_Loop3
SectionEnd

Section $(TITLE_SecExampleDB) SecExampleDB
SectionIn 1

  SetOutPath "$INSTDIR\Example"
  File /oname=IBRepl.ini ${PROJ_DIR}\IBRepl-example.ini

  File "${DATA_DIR}\Employee-original.gbk"
  File "${DATA_DIR}\Employee-original.gdb"
  AddSize 3300     ; 3x1100kb - 3 database copies
  CopyFiles /SILENT "$INSTDIR\Example\Employee-original.gdb" "Employee1.gdb"
  CopyFiles /SILENT "$INSTDIR\Example\Employee-original.gdb" "Employee2.gdb"
  CopyFiles /SILENT "$INSTDIR\Example\Employee-original.gdb" "Employee3.gdb"
  File "${DATA_DIR}\Employee-cfg.gbk"
  File "${DATA_DIR}\Employee-cfg.gdb"

  WriteINIStr "$INSTDIR\Example\IBRepl.ini" "IBReplicator.Environment" "DATADIR" "$INSTDIR\Example"
  WriteINIStr "$INSTDIR\Example\IBRepl.ini" "IBReplicator.Environment" "SYSDBA" $INI_SYSDBA
  WriteINIStr "$INSTDIR\Example\IBRepl.ini" "IBReplicator.Environment" "PASSWORD" $INI_PASSWORD

  ; do not overwrite existing file
  IfFileExists "$INSTDIR\IBRepl.ini" +2
    CopyFiles /SILENT "$INSTDIR\Example\IBRepl.ini" $INSTDIR
SectionEnd

;--------------------------------
;Installer Functions

Function .onInit
  ;Extract InstallOptions INI files
  !insertmacro MUI_INSTALLOPTIONS_EXTRACT "ioDBUser.ini"
  !insertmacro MUI_INSTALLOPTIONS_EXTRACT "ioRegistration.ini"
  !insertmacro MUI_INSTALLOPTIONS_EXTRACT "ioSDKIDE.ini"
  !insertmacro MUI_INSTALLOPTIONS_EXTRACT "ioBeforeInstall.ini"

  ;detect interbase folder
  ReadRegStr $Interbase_Folder HKEY_LOCAL_MACHINE "SOFTWARE\Borland\Interbase\CurrentVersion" "RootDirectory"
FunctionEnd

Function .onSelChange

FunctionEnd

Function StartMenu_pre
; do always because Uninstall is linked
FunctionEnd

Function dbuser_create
  SectionGetFlags ${SecExampleDB} $R0
  IntOp $R0 $R0 & ${SF_SELECTED}
  IntCmp $R0 ${SF_SELECTED} +2
    Abort
  
  !insertmacro MUI_HEADER_TEXT "$(TEXT_DBUSER_TITLE)" "$(TEXT_DBUSER_SUBTITLE)"
  !insertmacro MUI_INSTALLOPTIONS_DISPLAY "ioDBUser.ini"

FunctionEnd

Function dbuser_leave
  !insertmacro MUI_INSTALLOPTIONS_READ $INI_SYSDBA "ioDBUser.ini" "Field 3" "State"
  !insertmacro MUI_INSTALLOPTIONS_READ $INI_PASSWORD "ioDBUser.ini" "Field 5" "State"
FunctionEnd

Function registration_create
; do registration only if selected a executable
  SectionGetFlags ${SecBasic_Man} $R0
  SectionGetFlags ${SecBasic_Server} $R1
  IntOp $R0 $R0 | $R1
  SectionGetFlags ${SecBasic_ServerNT} $R1
  IntOp $R0 $R0 | $R1
  SectionGetFlags ${SecBasic_CmdLine} $R1
  IntOp $R0 $R0 | $R1

  IntOp $Do_Registration $R0 & ${SF_SELECTED}
  IntCmp $Do_Registration 0 0 +2 +2
    Abort

  StrCmp $Reg_Loaded "1" skip_load
  FileOpen $R0 "$WINDIR\${APP_REG_FILE}" "r"
  FileRead $R0 $R1
  FileClose $R0
  !insertmacro MUI_INSTALLOPTIONS_WRITE "ioRegistration.ini" "Field 4" "State" $R1
  StrCpy $Reg_Loaded "1"
skip_load:

  InstallOptions::initDialog /NOUNLOAD "$PLUGINSDIR\ioRegistration.ini"
  ; In this mode InstallOptions returns the window handle so we can use it
  Pop $hwnd
  ; Now show the dialog and wait for it to finish
  InstallOptions::show
  ; Finally fetch the InstallOptions status value (we don't care what it is though)
  Pop $0
FunctionEnd

Function Registration_leave
  !insertmacro MUI_INSTALLOPTIONS_READ $R0 "ioRegistration.ini" "Settings" "State"
  !insertmacro MUI_INSTALLOPTIONS_READ $R1 "ioRegistration.ini" "Field 2" "State"
  StrCmp $R0 0 validate  ; Next button?
  StrCmp $R0 1 adjustend ; groupbox
  StrCmp $R0 2 adjustend ; groupbox
  Abort

adjustend:
  GetDlgItem $R2 $hwnd 1203 ; PathRequest control (1200 + field 4 - 1)
  EnableWindow $R2 $R1
  ; Add the disabled flag too so when we return to this page it's disabled again
  StrCmp $R1 "1" +3 0
    !insertmacro MUI_INSTALLOPTIONS_WRITE "ioRegistration.ini" "Field 4" "Flags" "DISABLED|MULTILINE"
    Goto +2

    !insertmacro MUI_INSTALLOPTIONS_WRITE "ioRegistration.ini" "Field 4" "Flags" "MULTILINE"
  Abort ; Return to the page

validate:
  StrCpy $Do_Registration $R1
  StrCpy $Reg_Licence ""
  StrCmp $Do_Registration 0 skip_1
  !insertmacro MUI_INSTALLOPTIONS_READ $Reg_Licence "ioRegistration.ini" "Field 4" "State"
  StrCmp $Reg_Licence "" 0 skip_1
  MessageBox MB_OK "$(TEXT_REGISTRATION_NOLICENCE)"
  Abort
  ; check if string valid
skip_1:
FunctionEnd

Function SDKIDE_create
  !insertmacro MUI_HEADER_TEXT "$(TEXT_SDKIDE_TITLE)" "$(TEXT_SDKIDE_SUBTITLE)"
  
  SectionGetFlags ${SecSDK} $R0
  IntOp $R0 $R0 & ${SF_SELECTED}
  IntCmp $R0 ${SF_SELECTED} +2
    Abort
  StrCpy $R1 "5"
  StrCpy $R2 "1"
  StrCpy $R4 ""
SDKIDE_loop1:
  EnumRegKey $R3 HKCU "Software\Borland\Delphi\$R1.0" 0
  StrCmp $R3 "" +3
    StrCpy $R4 "1"
    !insertmacro MUI_INSTALLOPTIONS_WRITE "ioSDKIDE.ini" "Field $R2" "Flags" ""  ; default is state=0 and flags=DISABLES, do not expect change of Delphi registry during install

  IntOp $R1 $R1 + 1
  IntOp $R2 $R2 + 1

  IntCmp $R1 7 SDKIDE_loop1 SDKIDE_loop1
  ; found a Delphi key?
  StrCmp $R4 "" 0 +2
    Abort

  !insertmacro MUI_INSTALLOPTIONS_DISPLAY "ioSDKIDE.ini"
FunctionEnd

Function SDKIDE_leave
  StrCpy $SDKIDE_Install "0"
  StrCpy $R1 "1"
  StrCpy $R2 "1"

SDKIDE_loop2:
  !insertmacro MUI_INSTALLOPTIONS_READ $R0 "ioSDKIDE.ini" "Field $R1" "State"
  StrCmp $R0 "1" 0 +2
    IntOp $SDKIDE_Install $SDKIDE_Install | $R2
  IntOp $R1 $R1 + 1
  IntOp $R2 $R2 * 2
  IntCmp $R1 3 SDKIDE_loop2 SDKIDE_loop2
FunctionEnd

Function beforeInstall_create
  !insertmacro MUI_HEADER_TEXT "$(TEXT_BEFOREINSTALL_TITLE)" "$(TEXT_BEFOREINSTALL_SUBTITLE)"
  
  ; enable install NT service if checked
  SectionGetFlags ${SecBasic_ServerNT} $R0
  !insertmacro MUI_INSTALLOPTIONS_READ $Do_Install_Service "ioBeforeInstall.ini" "Field 1" "State"
  IntOp $R0 $R0 & ${SF_SELECTED}
  IntCmp $R0 0 0 +4 +4
    StrCpy $R0 "DISABLED"
    StrCpy $Do_Install_Service "0"
    Goto +2
    StrCpy $R0 ""
  !insertmacro MUI_INSTALLOPTIONS_WRITE "ioBeforeInstall.ini" "Field 1" "Flags" $R0
  !insertmacro MUI_INSTALLOPTIONS_WRITE "ioBeforeInstall.ini" "Field 1" "State" $Do_Install_Service

  ; enable/disable copy UDF to ib
  StrCpy $R0 0
  StrCmp $Interbase_Folder "" +2
  SectionGetFlags ${SecUDF} $R0
  
  !insertmacro MUI_INSTALLOPTIONS_READ $Do_Copy_UDF "ioBeforeInstall.ini" "Field 2" "State"
  IntOp $R0 $R0 & ${SF_SELECTED}
  IntCmp $R0 0 0 +4 +4
    StrCpy $R0 "DISABLED"
    StrCpy $Do_Copy_UDF "0"
    Goto +2
    StrCpy $Do_Copy_UDF ""
  !insertmacro MUI_INSTALLOPTIONS_WRITE "ioBeforeInstall.ini" "Field 2" "Flags" $R0
  !insertmacro MUI_INSTALLOPTIONS_WRITE "ioBeforeInstall.ini" "Field 2" "State" $Do_Copy_UDF

  !insertmacro MUI_INSTALLOPTIONS_DISPLAY "ioBeforeInstall.ini"
FunctionEnd

Function BeforeInstall_leave
  !insertmacro MUI_INSTALLOPTIONS_READ $Do_Install_Service "ioBeforeInstall.ini" "Field 1" "State"
  !insertmacro MUI_INSTALLOPTIONS_READ $Do_Copy_UDF "ioBeforeInstall.ini" "Field 2" "State"
FunctionEnd


;--------------------------------
;Descriptions

  ;Language strings
  LangString TYPE_Full ${LANG_ENGLISH} "Full"
  LangString TYPE_Online ${LANG_ENGLISH} "Online deploy"
  LangString TYPE_Offline ${LANG_ENGLISH} "Offline deploy"

  LangString TITLE_SecBasic ${LANG_ENGLISH} "Executables"
  LangString TITLE_SecBasic_Man ${LANG_ENGLISH} "Repl.manager"
  LangString TITLE_SecBasic_Server ${LANG_ENGLISH} "Repl.server"
  LangString TITLE_SecBasic_ServerNT ${LANG_ENGLISH} "NT repl.server"
  LangString TITLE_SecBasic_CmdLine ${LANG_ENGLISH} "Cmd.line tools"
  LangString TITLE_SecOffline ${LANG_ENGLISH} "Offline support"
  LangString TITLE_SecOffline_FTP ${LANG_ENGLISH} "FTP"
  LangString TITLE_SecOffline_Email ${LANG_ENGLISH} "Email"
  LangString TITLE_SecOffline_NetDir ${LANG_ENGLISH} "Net dir"
  LangString TITLE_SecOffline_CmdLine ${LANG_ENGLISH} "Cmd.line encoder"
  LangString TITLE_SecExampleDB ${LANG_ENGLISH} "Example databases"
  LangString TITLE_SecUDF ${LANG_ENGLISH} "UDF Interbase library"
  LangString TITLE_SecSDK ${LANG_ENGLISH} "SDK libraries"

  LangString DESC_SecBasic ${LANG_ENGLISH} "Basic executable files."
  LangString DESC_SecBasic_Man ${LANG_ENGLISH} "IBReplication manager."
  LangString DESC_SecBasic_Server ${LANG_ENGLISH} "IBReplication Server."
  LangString DESC_SecBasic_ServerNT ${LANG_ENGLISH} "IBReplication server running as NT service."
  LangString DESC_SecBasic_CmdLine ${LANG_ENGLISH} "IBReplicator command line tools enable replication tasks executed from command line (installer, package export, replicator, etc.)"
  LangString DESC_SecOffline ${LANG_ENGLISH} "IBReplication shared libraries enable offline replication"
  LangString DESC_SecOffline_FTP ${LANG_ENGLISH} "Enable offline transfer using FTP"
  LangString DESC_SecOffline_Email ${LANG_ENGLISH} "Enable offline transfer using email"
  LangString DESC_SecOffline_NetDir ${LANG_ENGLISH} "Enable offline transfer using shared net directory"
  LangString DESC_SecOffline_CmdLine ${LANG_ENGLISH} "Enable offline package encoding using a command line controled external program"
  LangString DESC_SecExampleDB ${LANG_ENGLISH} "Example databases that demonstrates replication features."
  LangString DESC_SecUDF ${LANG_ENGLISH} "Copy UDF dll library to Interbase/Firebird UDF directory."
  LangString DESC_SecSDK ${LANG_ENGLISH} "SDK libraries for custom application development using Delphi."

  LangString TEXT_DBUSER_TITLE ${LANG_ENGLISH} "Interbase/Firebird administrator page"
  LangString TEXT_DBUSER_SUBTITLE ${LANG_ENGLISH} "Identify database administrator to write it to INI file. You can do it manualy later or write it directly to configuration database"
  LangString TEXT_REGISTRATION_TITLE ${LANG_ENGLISH} "Registration page"
  LangString TEXT_REGISTRATION_SUBTITLE ${LANG_ENGLISH} "You can copy/paste your deploy licence key. If you have not it you will asked for registration in IBReplication Server."
  LangString TEXT_REGISTRATION_NOLICENCE ${LANG_ENGLISH} "Enter valid licence key."
  LangString TEXT_SDKIDE_TITLE ${LANG_ENGLISH} "SDK package install page"
  LangString TEXT_SDKIDE_SUBTITLE ${LANG_ENGLISH} "Register design-time packages to IDEs"
  LangString TEXT_BEFOREINSTALL_TITLE ${LANG_ENGLISH} "Ready to install page"
  LangString TEXT_BEFOREINSTALL_SUBTITLE ${LANG_ENGLISH} "All options are configured, confirm to install"

  LangString TEXT_NTService ${LANG_ENGLISH} "NT service: "
  LangString TEXT_NTServiceInstalling ${LANG_ENGLISH} "installing"
  LangString TEXT_NTServiceUninstalling ${LANG_ENGLISH} "uninstalling"
  LangString TEXT_NTServiceFailed ${LANG_ENGLISH} "failed: "
  LangString TEXT_LicenceRegistering ${LANG_ENGLISH} 'Licence: registering: ${APP_REG_FILE}'
  LangString TEXT_UDFInterbaseDetected ${LANG_ENGLISH} 'UDF library: local Interbase/Firebird server detected: $Interbase_Folder'
  LangString TEXT_UDFChecking ${LANG_ENGLISH} 'UDF library: checking: ${APP_IBREPL_UDF}'
  LangString TEXT_UDFInterbaseNotDetected ${LANG_ENGLISH} "UDF library: local Interbase/Firebird server not detected"
  LangString TEXT_UDFReadme ${LANG_ENGLISH} 'UDF library: you must enable using UDF library "${APP_IBREPL_UDF}" by Interbase/Firebird SQL server, see README.txt'

  LangString TEXT_Uninstaller ${LANG_ENGLISH} "${APP_NAME} ${APP_VERSION} (remove only)"

  ;Assign language strings to sections
  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
    !insertmacro MUI_DESCRIPTION_TEXT ${SecBasic} $(DESC_SecBasic)
    !insertmacro MUI_DESCRIPTION_TEXT ${SecBasic_Man} $(DESC_SecBasic_Man)
    !insertmacro MUI_DESCRIPTION_TEXT ${SecBasic_Server} $(DESC_SecBasic_Server)
    !insertmacro MUI_DESCRIPTION_TEXT ${SecBasic_ServerNT} $(DESC_SecBasic_ServerNT)
    !insertmacro MUI_DESCRIPTION_TEXT ${SecBasic_CmdLine} $(DESC_SecBasic_CmdLine)
    !insertmacro MUI_DESCRIPTION_TEXT ${SecOffline} $(DESC_SecOffline)
    !insertmacro MUI_DESCRIPTION_TEXT ${SecOffline_FTP} $(DESC_SecOffline_FTP)
    !insertmacro MUI_DESCRIPTION_TEXT ${SecOffline_Email} $(DESC_SecOffline_Email)
    !insertmacro MUI_DESCRIPTION_TEXT ${SecOffline_NetDir} $(DESC_SecOffline_NetDir)
    !insertmacro MUI_DESCRIPTION_TEXT ${SecOffline_CmdLine} $(DESC_SecOffline_CmdLine)
    !insertmacro MUI_DESCRIPTION_TEXT ${SecExampleDB} $(DESC_SecExampleDB)
    !insertmacro MUI_DESCRIPTION_TEXT ${SecUDF} $(DESC_SecUDF)
    !insertmacro MUI_DESCRIPTION_TEXT ${SecSDK} $(DESC_SecSDK)
  !insertmacro MUI_FUNCTION_DESCRIPTION_END


;--------------------------------
;Uninstaller Section

Section "Uninstall"

  IfFileExists "$INSTDIR\IBReplServerSvc.exe" 0 skip_uninst_service
    ; uninstall service
    DetailPrint "$(TEXT_NTService)$(TEXT_NTServiceUninstalling)"
    ; exec and capture output
    !insertmacro unExecCmdLine $R0 '"$INSTDIR\IBReplServerSvc.exe" /UNINSTALL' $(TEXT_NTService)
  skip_uninst_service:
  
  ;ADD YOUR OWN FILES HERE...
  ;Delete "$INSTDIR\Uninstall.exe"
  ;RMDir "$INSTDIR"
  
  MessageBox MB_YESNO|MB_ICONQUESTION "$(UNINST_Remove_InstDir)" IDNO NoDelete
    RMDir /r "$INSTDIR" ; remove whole direcory recursively

    IfFileExists "$INSTDIR\*.*" 0 SkipDel
      DetailPrint "$(UNINST_Cannot_Remove_InstDir)"
      goto NoDelete
  NoDelete:
    DetailPrint $(UNINST_Delete)
  SkipDel:

  !insertmacro MUI_STARTMENU_GETFOLDER Application $MUI_TEMP

  Delete "$SMPROGRAMS\$MUI_TEMP\*.lnk"

  ;Delete empty start menu parent diretories
  StrCpy $MUI_TEMP "$SMPROGRAMS\$MUI_TEMP"

  startMenuDeleteLoop:
    RMDir $MUI_TEMP
    GetFullPathName $MUI_TEMP "$MUI_TEMP\.."

    IfErrors startMenuDeleteLoopDone

    StrCmp $MUI_TEMP $SMPROGRAMS startMenuDeleteLoopDone startMenuDeleteLoop
  startMenuDeleteLoopDone:

  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP_NICK} ${APP_VERSION}"

  DeleteRegKey HKCU "Software\MandySoft\${APP_NICK}\${APP_VERSION}"
  DeleteRegKey /ifempty HKCU "Software\MandySoft\${APP_NICK}"
  DeleteRegKey /ifempty HKCU "Software\MandySoft"
  
  DeleteRegKey HKLM "SOFTWARE\MandySoft\${APP_NICK}\${APP_VERSION}"
  DeleteRegKey /ifempty HKLM "SOFTWARE\MandySoft\${APP_NICK}"
  DeleteRegKey /ifempty HKLM "SOFTWARE\MandySoft"

SectionEnd

LangString UNINST_Delete ${LANG_ENGLISH} 'Install dir: Files from "$INSTDIR" remove manually.'
LangString UNINST_Cannot_Remove_InstDir ${LANG_ENGLISH} 'Install dir: "$INSTDIR" could not be removed!'
LangString UNINST_Remove_InstDir ${LANG_ENGLISH} 'Would you like to remove the directory "$INSTDIR"? $\rAll data will be lost!'
