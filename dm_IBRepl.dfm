object IBReplDataModule: TIBReplDataModule
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 266
  Top = 243
  Height = 479
  Width = 741
  object IBCfgDB: TIBDatabase
    DatabaseName = 'e:\data\ibrepl\rc-new.gdb'
    Params.Strings = (
      'user_name=sysdba'
      'password=masterkey'
      'lc_ctype=WIN1250')
    LoginPrompt = False
    DefaultTransaction = IBCfgTransaction
    IdleTimer = 0
    SQLDialect = 1
    TraceFlags = []
    AfterConnect = IBCfgDBAfterConnect
    AfterDisconnect = IBCfgDBAfterDisconnect
    Left = 48
    Top = 32
  end
  object IBCfgTransaction: TIBTransaction
    Active = False
    DefaultDatabase = IBCfgDB
    AutoStopAction = saNone
    Left = 48
    Top = 80
  end
  object IBReplicator: TIBReplicator
    ConfigDatabase = IBCfgDB
    NowAsUTC = False
    TraceSQL = False
    Left = 128
    Top = 32
  end
  object IniTimer: TTimer
    Enabled = False
    Interval = 10000
    OnTimer = IniTimerTimer
    Left = 128
    Top = 80
  end
  object ReplExtFileQuery: TIBDataSet
    Database = IBDatabase1
    Transaction = IBTransaction1
    BufferChunks = 32
    CachedUpdates = False
    SelectSQL.Strings = (
      'SELECT * FROM '
      'REPL$EXT_FILE'
      'WHERE SCHEMAID=:SCHEMAID AND SEQID=:SEQID')
    ParamCheck = False
    Left = 365
    Top = 155
  end
  object IBDatabase1: TIBDatabase
    DatabaseName = 'e:\Products\Finep-Reklamace\Data\REKLAMACE.GDB '
    Params.Strings = (
      'user_name=sysdba'
      'password=masterkey'
      'lc_ctype=WIN1250')
    LoginPrompt = False
    IdleTimer = 0
    SQLDialect = 1
    TraceFlags = []
    AllowStreamedConnected = False
    AfterConnect = IBCfgDBAfterConnect
    AfterDisconnect = IBCfgDBAfterDisconnect
    Left = 216
    Top = 160
  end
  object IBTransaction1: TIBTransaction
    Active = False
    DefaultDatabase = IBCfgDB
    AutoStopAction = saNone
    Left = 208
    Top = 200
  end
  object ScheduleTimer: TTimer
    Enabled = False
    OnTimer = ScheduleTimerTimer
    Left = 184
    Top = 80
  end
end
