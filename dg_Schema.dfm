inherited SchemaDialog: TSchemaDialog
  HelpContext = 31210
  Caption = 'Schema properties'
  ClientHeight = 314
  ClientWidth = 510
  OnCreate = FormCreate
  PixelsPerInch = 120
  TextHeight = 16
  inherited Panel1: TPanel
    Left = 402
    Height = 314
  end
  inherited Panel2: TPanel
    Width = 402
    Height = 314
    object Label1: TLabel
      Left = 20
      Top = 20
      Width = 63
      Height = 16
      Caption = 'Schema&ID'
      FocusControl = DBEdit1
    end
    object Label2: TLabel
      Left = 20
      Top = 79
      Width = 37
      Height = 16
      Caption = '&Name'
      FocusControl = DBEdit2
    end
    object Label10: TLabel
      Left = 20
      Top = 167
      Width = 57
      Height = 16
      Caption = 'Co&mment'
      FocusControl = DBMemo2
    end
    object Label3: TLabel
      Left = 141
      Top = 20
      Width = 32
      Height = 16
      Caption = '&Type'
      FocusControl = RxDBComboBox1
    end
    object DBEdit1: TDBEdit
      Left = 20
      Top = 39
      Width = 70
      Height = 24
      DataField = 'SCHEMAID'
      DataSource = DataSource1
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Left = 20
      Top = 98
      Width = 373
      Height = 24
      DataField = 'NAME'
      DataSource = DataSource1
      TabOrder = 1
    end
    object DBMemo2: TDBMemo
      Left = 20
      Top = 187
      Width = 373
      Height = 110
      DataField = 'COMMENT'
      DataSource = DataSource1
      ScrollBars = ssVertical
      TabOrder = 3
    end
    object DBCheckBox2: TDBCheckBox
      Left = 22
      Top = 138
      Width = 208
      Height = 21
      Caption = '&Keep statistic'
      DataField = 'S_KEEP'
      DataSource = DataSource1
      TabOrder = 2
      ValueChecked = 'Y'
      ValueUnchecked = 'N'
    end
    object RxDBComboBox1: TRxDBComboBox
      Left = 140
      Top = 39
      Width = 253
      Height = 24
      Style = csDropDownList
      DataField = 'SCHEMATYPE'
      DataSource = DataSource1
      EnableValues = True
      ItemHeight = 16
      Items.Strings = (
        '0..replication'
        '1..record history')
      TabOrder = 4
      Values.Strings = (
        '0'
        '1')
    end
  end
  inherited Table: TIBDataSet
    SelectSQL.Strings = (
      'SELECT * FROM'
      'SCHEMATA'
      '')
    Left = 93
    Top = 16
  end
  inherited DataSource1: TDataSource
    OnDataChange = DataSource1DataChange
    Left = 112
    Top = 32
  end
end
