program ibreplinst;
{$APPTYPE CONSOLE}
uses
  SysUtils,
  dm_IBRepl,
  CmdLine,
  {$IFDEF LINUX}
  Libc,
  Types,
  {$ELSE}
  Windows,
  {$ENDIF}
  Connect, AuxProj, Classes, dm_IBReplC, IBReplicator;

{$R IBReplMan.res}

resourcestring
  sAbout2= 'Command line replication manager and install tool';
  sHelp1 = 'Usage: ibreplinst <type> /D:<cfgdb> /U:<user> /P:<psw> [/Q:<dial>] [/F:<pfx>] [/BF:<bck>] '#13#10+
           '       [/S:<schemaid>] [/G:<groupid>] [/B:<dbid>] [/R:<relationid>] [/K:<keylen>]'#13#10+
           '       [/L:<log>] [/LO:<logopt>] [/I:<ini>] [/V:<verb>] [/E:<par>=<val>] [/TRACESQL]'#13#10+
           '       [/SILENT] [/O:<stdout>] [/ALLOWCREATE] [/H] [/?]'#13#10;
  sHelp2 = 'Description:'#13#10+
           '   <type>     CREATESYSTEMOBJ .. {schemaid,groupid[,dbid]|dbid}'#13#10+
           '              DROPSYSTEMOBJ .. {schemaid,groupid[,dbid]|dbid}'#13#10+
           '              DROPTRIGGERS .. {schemaid,groupid[,dbid]|dbid}'#13#10+
           '              CREATESERVEROBJ .. {schemaid,groupid[,dbid]|dbid}'#13#10+
           '              DROPSERVEROBJ .. {schemaid,groupid[,dbid]|dbid}'#13#10+
           '              CLEANSYSTEMDATA .. {schemaid,groupid[,dbid]}'#13#10+
           '              CLONE .. {schemaid,groupid,dbid}'#13#10+
           '              CLONEEMPTY .. {schemaid,groupid,dbid}'#13#10+
           '              BACKUPDB.. {dbid,bck}'#13#10+
           '              RESTOREDB.. {dbid,bck}'#13#10+
           '              BACKUPCFGDB.. {bck}'#13#10+
           '              RESTORECFGDB.. {bck}'#13#10+
           '              CREATECFGDB.. {allowcreate}'#13#10+
           '              UPGRADECFGDB'#13#10+
           '              UPGRADEDB .. dbid'#13#10;
  sHelp3=  '   <ini>      ini file, default: IBREPL.INI in program dir'#13#10+
           '   <bck>      backup/restore GBK file'#13#10+
           '   INI overriding:'#13#10+
           '   <cfgdb>    configuration database file name'#13#10+
           '   <user>     login name'#13#10+
           '   <psw>      password'#13#10+
           '   <dial>     SQL dialect'#13#10+
           '   <pfx>      prefix of objects in configuration database'#13#10+
           '   <log>      log file name, default: '+devNull+#13#10+
           '   <logopt>   bit 0..log err cmds, bit 1..log err params'#13#10+
           '   <keylen>   length of primary/foreign keys in log'#13#10+
           ''#13#10;

  sHelp4 = '   <verb>     verbose bit 0..repllog, 1..dblog, default: 255'#13#10+
           '   <par>=<val> set environment parameter(s)'#13#10+
           '   /ALLOWCREATE allow create file if does not exist'#13#10+
           '   /TRACESQL  enable SQL tracing using sqlmonitor'#13#10+
           '   /SILENT    no console output, use <stdout> instead'#13#10+
           ''#13#10+
           'WARNING:'#13#10+
           'Long names containing spaces put as quoted parameter, fex. "/F:REP $"';
  sCreateSystemObjects = 'Creating system objects';
  sDropSystemObjects = 'Dropping system objects';
  sCreateServerObjects = 'Creating server objects';
  sDropServerObjects = 'Dropping server objects';
  sDropSystemTriggers = 'Dropping replication triggers';
  sUpgradeCfg = 'Upgrading configuration database';
  sUpgrade = 'Upgrading database (DBId: %d)';
  sClearStatistics = 'Clear statistics (SchemaId:%d, GroupId:%d, RelationId:%d)';
  sUnknownType = 'Unknown type parameter "%s"';
  sConfigDatabase = 'Config database';
type
  TIBReplDataModule2 = class(TIBReplDataModuleC)
  private
    fSchemaId: Integer;
    fGroupId: Integer;
    fDbId: Integer;
    fRelationId: Integer;
    fKeyLength: Integer;
    fAction: string;
    fBackupFile: string;
  public
    constructor Create(aOwner: TComponent); override;
    procedure CheckSchema(S: string);
  end;

procedure TIBReplDataModule2.CheckSchema(S: string);
resourcestring
  sSchemaId = ' (SchemaId:%d, GroupId:%d, RelationId:%d)';
  sDBId = ' (DBId:%d)';
  sNoSchemaId = 'No schemaid/dbid';
begin
  if (fSchemaId = 0) and (fDBId = 0) then
    raise Exception.Create(sNoSchemaId);
  if fSchemaId = 0 then
    S:= S + Format(sDBId, [fDBId])
  else
    S:= S + Format(sSchemaId, [fSchemaId, fGroupId, fDBId]);
  IBReplicator.ReplLog.Log('', lchNull, S);
end;

constructor TIBReplDataModule2.Create;
begin
  inherited;
  ReadFromIni(False, False);
  fSchemaId:= GetParInteger('S', 0);
  fGroupId:= GetParInteger('G', 0);
  fDBId:= GetParInteger('B', 0);
  fRelationId:= GetParInteger('R', 0);
  fKeyLength:= Ini.ReadInteger(DBIniSection+DBIniSection2, 'KeyLength', 255);
  fKeyLength:= GetParInteger('K', fKeyLength);
  GetCmdString('', clUpcase, fAction);
  GetCmdString('BF:', clUpcase or clValueCase, fBackupFile);
end;

begin
  WriteProgName;
  Writeln2(sAbout2);
  WriteLn2('');
  if IsThereCmd('H', clUpcase) or IsThereCmd('?', clUpcase) then
  begin
    Writeln2(sHelp1+sHelp2+sHelp3+sHelp4);
    Exit;
  end;

  DBIniSection2:= '.Manager';
  try
    IBReplDataModule:= TIBReplDataModule2.Create(nil);
    try
      try
        with IBReplDataModule as TIBReplDataModule2 do
        begin
          if (fAction <> 'UPGRADECFGDB') and (fAction <> 'RESTORECFGDB') and (fAction <> 'BACKUPCFGDB') and (fAction <> 'CREATECFGDB') then
            IBReplicator.ConfigDatabase.Open;
          if fAction = 'CREATESYSTEMOBJ' then
            begin
              CheckSchema(sCreateSystemObjects);
              if fSchemaId = 0 then
                IBReplicator.CreateSystemObjects(fDBId, fKeyLength)
              else
                IBReplicator.CreateSystemObjects(fSchemaId, fKeyLength, fGroupId, fDBId);
            end
          else if fAction = 'DROPSYSTEMOBJ' then
            begin
              CheckSchema(sDropSystemObjects);
              if fSchemaId = 0 then
                IBReplicator.DropSystemObjects(fDBId, False)
              else
                IBReplicator.DropSystemObjects(fSchemaId, fGroupId, fDBId, False);
            end
          else if fAction = 'DROPTRIGGERS' then
            begin
              CheckSchema(sDropSystemTriggers);
              if fSchemaId = 0 then
                IBReplicator.DropSystemObjects(fDBId, True)
              else
                IBReplicator.DropSystemObjects(fSchemaId, fGroupId, fDBId, True);
            end
          else if fAction = 'CREATESERVEROBJ' then
            begin
              CheckSchema(sCreateServerObjects);
              if fSchemaId = 0 then
                IBReplicator.CreateServerObjects(fDBId)
              else
                IBReplicator.CreateServerObjects(fSchemaId, fGroupId, fDBId);
            end
          else if fAction = 'DROPSERVEROBJ' then
            begin
              CheckSchema(sDropServerObjects);
              if fSchemaId = 0 then
                IBReplicator.DropServerObjects(fDBId)
              else
                IBReplicator.DropServerObjects(fSchemaId, fGroupId, fDBId);
            end
          else if fAction = 'CLEANSYSTEMDATA' then
            begin
              IBReplicator.DeleteSourceSystemData(fSchemaId, fGroupId, fDBId);
            end
          else if fAction = 'CLONE' then
            begin
              IBReplicator.CloneSourceDatabase(fSchemaId, fGroupId, fDBId, False);
            end
          else if fAction = 'CLONEEMPTY' then
            begin
              IBReplicator.CloneSourceDatabase(fSchemaId, fGroupId, fDBId, True);
            end
          else if fAction = 'CREATECFGDB' then
            begin
              Inc(IBReplDataModule.UpgradeFlag);
              try
                IBReplicator.CreateConfigDatabase(IsThereCmd('ALLOWCREATE', clUpcase));
              finally
                Dec(IBReplDataModule.UpgradeFlag);
              end;
            end
          else if fAction = 'UPGRADECFGDB' then
            begin
              IBReplicator.ReplLog.Log('', lchNull, Format(sUpgradeCfg, []));
              Inc(UpgradeFlag);
              try
                IBReplicator.UpgradeConfigDatabase;
              finally
                Dec(UpgradeFlag);
              end;
            end
          else if fAction = 'UPGRADEDB' then
            begin
              IBReplicator.ReplLog.Log('', lchNull, Format(sUpgrade, [fDBId]));
              IBReplicator.UpgradeDatabase(fDBId);
            end
          else if fAction = 'BACKUPDB' then
            begin
              IBReplicator.BackupDatabase(fDBId, fBackupFile);
            end
          else if fAction = 'RESTOREDB' then
            begin
              IBReplicator.RestoreDatabase(fDBId, fBackupFile);
            end
          else if fAction = 'BACKUPCFGDB' then
            begin
              IBReplicator.ConfigDatabase.BeforeConnect(IBReplicator.ConfigDatabase);
              IBReplicator.BackupDatabase(IBReplicator.ConfigDatabase, sConfigDatabase, fBackupFile);
            end
          else if fAction = 'RESTORECFGDB' then
            begin
              IBReplicator.ConfigDatabase.BeforeConnect(IBReplicator.ConfigDatabase);
              IBReplicator.RestoreDatabase(IBReplicator.ConfigDatabase, sConfigDatabase, fBackupFile);
            end
          else if fAction = 'CLEARSTAT' then
            begin
              IBReplicator.ReplLog.Log('', lchNull, Format(sClearStatistics, [fSchemaId, fGroupId, fRelationId]));
              if (fGroupId = 0) and (fRelationId = 0) then
                IBReplicator.ClearSchemaStatistics(fSchemaId);
              IBReplicator.ClearRelationStatistics(fSchemaId, fGroupId, fRelationId);
            end
          else
            begin
              IBReplicator.DBLog.Log('', lchNull, Format(sUnknownType, [fAction]));
            end;
        end;
      except
        on E: Exception do
        begin
          IBReplDataModule.IBReplicator.DBLog.Log('', lchError, E.Message);
          ExitCode:= 1;
        end;
      end;
    finally
      IBReplDataModule.Free;
    end;
  except
    on E: Exception do
    begin
      Writeln2(E.Message);
      ExitCode:= 1;
    end;
  end;
end.
