@echo OFF
for %%i IN (*.*) do @SET _CWD=%%~pi

SET _OPUS=D:\Program Files\OpusDocumentor\bin\

cd \proj\ibrepl\hlp

echo --- Documentor: Print ---------------------------------------
"%_OPUS%\Documentor.exe" -Print -tPrint\Temp -i+Temp -rDocu -oPub -oOverrides -i_Includes.hti -c_Contents.hti -map_Map.hti -intmap_IntMap.hti -pi"%_OPUS%" @IBRepl2.dpf
cd print
echo --- WebCompiler: Print --------------------------------------
"%_OPUS%\WebCompiler.exe" -Doc -f.\WebCodeDocRTF.fmt -rDocu_%s -r+Docu_%s_%s -r-Docu -r@..\IBRepl2.dpi -dtImageExtension=.gif IBRepl2.rtt
move *.rtf out\
cd ..

echo --- Documentor: Pretty INT -----------------------------------
"%_OPUS%\Documentor.exe" -Pretty -oNoTM -oOverwrite -tPretty\Out -oNoHelp -e.int -oPub -oOverrides -pi"%_OPUS%" @IBRepl2.dpf
echo --- Documentor: Pretty TXT -----------------------------------
"%_OPUS%\Documentor.exe" -Pretty -oNoTM -oOverwrite -tPretty\Out -e.txt -oPub -oOverrides -pi"%_OPUS%" @IBRepl2.dpf

echo --- Documentor: Help -----------------------------------------
"%_OPUS%\Documentor.exe" -Help -tHelp\Temp -i+Temp -rDocu -oPub -oOverrides -oALinks -i_Includes.hti -c_Contents.hti -map_Map.hti -intmap_IntMap.hti -pi"D:\Program Files\OpusDocumentor\Bin" @IBRepl2.dpf
cd help
echo --- WebCompiler: Help ----------------------------------------
"%_OPUS%\WebCompiler.exe" -Help -rDocu_%s -r+Docu_%s_%s -r-Docu -r@..\IBRepl2.dpi -dtImageExtension=.gif IBRepl2.rth
move *.rtf temp\
echo --- HCRTF: Help ----------------------------------------------
"D:\Program Files\Delphi7\Help\Tools\HCRTF.exe" /x IBRepl2.hpj

cd ..

SET _OPUS=

cd %_CWD%
set _CWD=


