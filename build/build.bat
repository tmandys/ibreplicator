@echo off
echo ======== Building project files     ================================
call cmpproj.bat
echo ======== Building SDK files         ================================

cd SDK\Lib
call cmpall.bat
cd ..\..

echo ======== Building documentation     ================================
call cmpopus.bat

echo ======== Building installation file ================================
"D:\Program Files\NSIS\makensis.exe"   "E:\Proj\IBRepl\install\IBReplicator-install.2.0.nsi"



