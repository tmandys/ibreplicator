for %%i IN (*.*) do @SET _CWD=%%~pi

cd e:\proj\ibrepl

@for %%i IN (*.dpr) do @"d:\program files\delphi7\bin\dcc32" %%i -B -E%_CWD% -dREGISTRATION -V- -R"D:\Program files\Delphi7\Lib"

cd %_CWD%

rem add version suffix, 150 for D7

@for %%i IN (*.150.dll) do @del %%i
@for %%i IN (enc_*.dll) do @rename %%i "%%~ni.150%%~xi"
@for %%i IN (transfer_*.dll) do @rename %%i "%%~ni.150%%~xi"

set _CWD=

