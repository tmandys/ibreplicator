@del .\delphi%1\. /Q

@for /F "delims=| tokens=1*" %%i in (package.lst) do @call cmp %1 %%i

@del RegIBReplicatorDsgn.dcu
@del RegIBReplicatorDlg.dcu
move *.dcu .\delphi%1\
move *.dcp .\delphi%1\
move *.bpl .\delphi%1\

echo ==Packages %1 done=========================================================

