program ibreplc;
{$APPTYPE CONSOLE}
uses
  SysUtils,
  dm_IBRepl,
  CmdLine,
  {$IFDEF LINUX}
  Libc,
  Types,
  {$ELSE}
  Windows,
  {$ENDIF}
  Connect,
  Classes,
  IBDatabase,
  dm_IBReplC,
  IBReplicator;

{$R IBReplServer.res}

resourcestring
  sAbout2= 'Command line replicator tool';
  sHelp1 = 'Usage: ibreplc <type> [/D:<cfgdb>] [/U:<usr>] [/P:<psw>] [/Q:<dial>] [/F:<pfx>]'#13#10+
           '       [/T:<task>] [/S:<schemaid>] [/G:<groupid>] [/C:<srcdbid>] [/B:<tgtdbid>]'#13#10+
           '       [/K:<pdir>] [/A:<act>] [/M:<max>] [/RO:<options>] [/UTC:<utc>] [/SL:<syslog>]'#13#10+
           '       [/L:<log>] [/LO:<logopt>] [/I:<ini>] [/V:<verb>] [/E:<var>=<par>]'#13#10+
           '       [/TRACESQL] [/SILENT] [/O:<stdout>] [/H] [/?]'#13#10;
  sHelp10 = 'Description:'#13#10+
           '   <type>     ONL  .. online replication'#13#10+
           '              OFFS .. offline replication, source side'#13#10+
           '              OFFT .. offline replication, target side'#13#10+
           '              ONLT .. online target replication'#13#10+
           '              SYNC .. online synchronization'#13#10+
           '   <task>     task name in INI'#13#10+
           ''#13#10+
           '   <cfgdb>    configuration database file name'#13#10+
           '   <usr>      login name'#13#10+
           '   <psw>      password'#13#10+
           '   <dial>     SQL dialect'#13#10+
           '   <pfx>      prefix of objects in configuration database'#13#10+
           ''#13#10;
  sHelp20 ='   <log>      log file name, default: '+devNull+#13#10+
           '   <logopt>   bit 0..log err cmds, bit 1..log err params'#13#10+
           '   <syslog>   identifier of syslog/event_log'#13#10+
           '   <ini>      ini file, default: IBREPL.INI in program dir'#13#10+
           '   <verb>     verbose bit 0..repllog, 1..dblog, default: 255'#13#10+
           '   <par>=<val> set environment parameter(s)'#13#10+
           '   <utc>      1..now date time in UTC'#13#10+
           '   /TRACESQL  enable SQL tracing using sqlmonitor'#13#10+
           '   /SILENT    no console output, use <stdout> instead'#13#10+
           ''#13#10;
  sHelp25 ='   <options>  bit 0.. 0..replicate records, 1..repl.log'#13#10+
           '              bit 1.. report to source'#13#10+
           '              bit 2.. report to target'#13#10+
           '              bit 3.. extended conflict checking'#13#10+
           '   <max>      max runtime of replication #[d,h,m,s][,...]'#13#10+
           ''#13#10+
           '   <schemaid> SchemaId1[,SchemaId2[,...]]'#13#10+
           '   <groupid>  GroupId1[,GroupId2[,...]]'#13#10+
           '   <srcdbid>  SrcDBId1[,SrcDBId2[,...]]'#13#10+
           '   <tgtdbid>  TgtDBId1[,TgtDBId2[,...]]'#13#10+
           ''#13#10;
  sHelp30= '  Offline replication:'#13#10+
           '   <pdir>     package directory'#13#10+
           '   <act>      1..receive, 2..send, 4..resend, 7(default)..rec.,send and resend'#13#10+
           ''#13#10+
           'WARNING:'#13#10+
           'Long names containing spaces put as quoted parameter, fex. "/F:REP $"'#13#10;

type
  TIBReplDataModule2 = class(TIBReplDataModuleC)
  public
    constructor Create(aOwner: TComponent); override;
    procedure ReadIBReplicatorFromIni(aIBReplicator: TIBReplicator; const aIniSection: string); override;
  end;

constructor TIBReplDataModule2.Create;
var
  T: TIBReplicatorTask;
begin
  inherited;
  ReadFromIni(False, False);
  IBCfgDB.Open;
  T:= TIBReplicatorTask.Create(nil);
  InsertTask(T);
  T.ReadFromIni(GetParString('T', ''));
  MultiThreading:= False;
end;

procedure TIBReplDataModule2.ReadIBReplicatorFromIni(
  aIBReplicator: TIBReplicator; const aIniSection: string);
var
  I: Integer;
  S: string;

  procedure GetParArr(const aPar: string; var Arr: TIntegerOpenArray);
  var
    S: string;
  begin
    S:= GetParString(aPar, '');
    if S <> '' then
    begin
      try
        Arr:= StringArrayToIntegerArray(StringToArray(S, ','));
      except
        SetLength(Arr, 0);
      end;
    end;
  end;
begin
  inherited;
  I:= GetParInteger('UTC', -1);
  if I >= 0 then
    IBReplicator.NowAsUTC:= I > 0;

  if aIBReplicator is TIBReplicatorTask then
  begin
    with TIBReplicatorTask(aIBReplicator) do
    begin
      GetCmdString('', clUpcase, S);
      if S = 'SYNC' then
        ReplType:= 4
      else if S = 'ONL' then
        ReplType:= 0
      else if S = 'OFFS' then
        ReplType:= 1
      else if S = 'OFFT' then
        ReplType:= 2
      else if S = 'ONLT' then
        ReplType:= 3
      else
        ReplType:= -1;

      GetParArr('S', SchemaIds);
      GetParArr('G', GroupIds);
      GetParArr('C', SrcDbIds);
      GetParArr('B', TgtDbIds);

      I:= GetParInteger('A', -1);
      if I >= 0 then
      begin
        ResendData:= I and $04 <> 0;
        SendData:= I and $02 <> 0;
        ReceiveData:= I and $01 <> 0;
      end;
      ReplOptions:= GetParInteger('RO', ReplOptions);

      S:= GetParString('M', '');
      if S <> '' then
        MaxReplRunTime:= DecodeInterval(S);

      OfflineDir:= GetParString('K', OfflineDir);
      ReplOnEvent:= False;
    end;
  end;
end;

begin
  WriteProgName;
  Writeln2(sAbout2);
  WriteLn2('');
  if IsThereCmd('H', clUpcase) or IsThereCmd('?', clUpcase) then
  begin
    WriteLn2('');
    Writeln2(sHelp1);
    Writeln2(sHelp10);
    Writeln2(sHelp20);
    Writeln2(sHelp25);
    Writeln2(sHelp30);
    Exit;
  end;

  DBIniSection2:= '.Server';
  try
    IBReplDataModule:= TIBReplDataModule2.Create(nil);
    try
      try
        TIBReplicatorTask(IBReplDataModule.Tasks[0]).DoReplicate;
      except
        on E: Exception do
        begin
          IBReplDataModule.IBReplicator.DBLog.Log('', lchError, E.Message);
          ExitCode:= 1;
        end;
      end;
    finally
      IBReplDataModule.Free;
    end;
  except
    on E: Exception do
    begin
      Writeln2(E.Message);
      ExitCode:= 1;
    end;
  end;
end.
