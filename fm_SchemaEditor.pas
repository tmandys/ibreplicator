(* IB Replication Suite
 * Copyright (C) 2002,2003 by Tomas Mandys-MandySoft
 *)

unit fm_SchemaEditor;

interface

uses
  {$IFDEF LINUX}
  Libc, Qt, QGraphics, QForms, QControls, QStdCtrls, QButtons, QExtCtrls, QDialogs, QMenus, QGrids, QDBGrids, QActnList, QComCtrls, QImgList, Variants, QTypes, QDBCtrls, Types,
  {$ELSE}
  Windows, Messages, Graphics, Forms, Controls, StdCtrls, Buttons, ExtCtrls, Dialogs, Menus, Grids, DBGrids, ActnList, ComCtrls, ImgList, ToolWin, DBCtrls,
  {$ENDIF}
  SysUtils, Classes, IBDatabase, Db, IBCustomDataSet, IBSQL, Terminal, Connect, LogTerm;

type
  TSchemaEditor = class(TForm)
    ToolBar1: TToolBar;
    MainMenu1: TMainMenu;
    ActionList1: TActionList;
    Panel1: TPanel;
    Panel2: TPanel;
    CommitAction: TAction;
    RollbackAction: TAction;
    Database1: TMenuItem;
    Commit1: TMenuItem;
    Rollback1: TMenuItem;
    DatabaseItem1: TMenuItem;
    SchemaItem1: TMenuItem;
    RelationItem1: TMenuItem;
    FieldItem1: TMenuItem;
    Databases_DBGrid: TDBGrid;
    Splitter2: TSplitter;
    Schema_DBGrid2: TDBGrid;
    Splitter3: TSplitter;
    Panel3: TPanel;
    Splitter4: TSplitter;
    SchemaDB2_DBGrid1: TDBGrid;
    Relations_DBGrid3: TDBGrid;
    Fields_DBGrid4: TDBGrid;
    IBTransaction: TIBTransaction;
    DatabasesQuery: TIBDataSet;
    SchemaQuery: TIBDataSet;
    SchemaDB1Query: TIBDataSet;
    SchemaDB2Query: TIBDataSet;
    RelationsQuery: TIBDataSet;
    DataSource1: TDataSource;
    DataSource2: TDataSource;
    DataSource3: TDataSource;
    DataSource4: TDataSource;
    DataSource5: TDataSource;
    Splitter5: TSplitter;
    SchemaDB1_DBGrid1: TDBGrid;
    FieldsQuery: TIBDataSet;
    DataSource6: TDataSource;
    DatabasesQueryDBID: TIntegerField;
    DatabasesQueryNAME: TIBStringField;
    SchemaQuerySCHEMAID: TIntegerField;
    SchemaQueryNAME: TIBStringField;
    DatabasesQueryFILENAME: TIBStringField;
    DatabasesQueryADMINUSER: TIBStringField;
    DatabasesQueryADMINPSW: TIBStringField;
    DatabasesQueryADMINROLE: TIBStringField;
    DatabasesQueryCHARSET: TIBStringField;
    DatabasesQuerySQLDIALECT: TIntegerField;
    DatabasesQueryCOMMENT: TMemoField;
    DatabasesQueryOBJPREFIX: TIBStringField;
    SchemaQueryS_KEEP: TIBStringField;
    SchemaQueryS_INSERT: TIntegerField;
    SchemaQueryS_UPDATE: TIntegerField;
    SchemaQueryS_DELETE: TIntegerField;
    SchemaQueryS_CONFLICT: TIntegerField;
    SchemaQueryS_ERROR: TIntegerField;
    SchemaQueryS_MSEC: TIntegerField;
    SchemaQueryCOMMENT: TMemoField;
    SchemaDB2QueryNAME: TIBStringField;
    SchemaDB2QuerySCHEMAID: TIntegerField;
    SchemaDB2QueryGROUPID: TIntegerField;
    SchemaDB2QueryDBID: TIntegerField;
    SchemaDB2QueryDBMASK: TIntegerField;
    SchemaDB2QueryDISABLED: TIBStringField;
    SchemaDB2QueryREPLUSER: TIBStringField;
    SchemaDB2QueryREPLROLE: TIBStringField;
    SchemaDB2QueryREPLPSW: TIBStringField;
    SchemaDB2QuerySEPARATOR: TIntegerField;
    SchemaDB2QueryCOMMENT: TMemoField;
    SchemaDB2QueryOFFTRANSFER: TIBStringField;
    SchemaDB2QueryOFFPARAMS: TMemoField;
    SchemaDB2QueryOFFENCODER: TIBStringField;
    SchemaDB2QueryOFFDBID: TIntegerField;
    RelationsQueryRELATIONNAME: TIBStringField;
    RelationsQueryTARGETNAME: TIBStringField;
    RelationsQueryTARGETTYPE: TIBStringField;
    RelationsQueryDISABLED: TIBStringField;
    RelationsQuerySCHEMAID: TIntegerField;
    RelationsQueryRELATIONID: TIntegerField;
    RelationsQueryGROUPID: TIntegerField;
    RelationsQueryS_KEEP: TIBStringField;
    RelationsQueryS_INSERT: TIntegerField;
    RelationsQueryS_UPDATE: TIntegerField;
    RelationsQueryS_DELETE: TIntegerField;
    RelationsQueryS_CONFLICT: TIntegerField;
    RelationsQueryS_ERROR: TIntegerField;
    RelationsQueryS_MSEC: TIntegerField;
    RelationsQuerySYNCORDER: TIntegerField;
    RelationsQueryCONDITION: TMemoField;
    FieldsQueryFIELDNAME: TIBStringField;
    FieldsQueryFIELDTYPE: TIntegerField;
    FieldsQueryTARGETNAME: TIBStringField;
    FieldsQuerySCHEMAID: TIntegerField;
    FieldsQueryGROUPID: TIntegerField;
    FieldsQueryRELATIONID: TIntegerField;
    FieldsQueryFIELDID: TIntegerField;
    SchemaQuerySRCNAME: TIBStringField;
    SchemaDB1QueryGROUPID: TIntegerField;
    SchemaDB1QueryCOUNT: TIntegerField;
    SchemaDB1QuerySCHEMAID: TIntegerField;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    FieldsDeleteAction: TAction;
    Delete1: TMenuItem;
    PopupMenu1: TPopupMenu;
    PopupMenu2: TPopupMenu;
    PopupMenu3: TPopupMenu;
    PopupMenu4: TPopupMenu;
    PopupMenu5: TPopupMenu;
    RelationsDeleteAction: TAction;
    RelationsDeleteAction2: TMenuItem;
    SchemaDeleteAction: TAction;
    DatabasesDeleteAction: TAction;
    Delete3: TMenuItem;
    Delete4: TMenuItem;
    GroupDeleteAction: TAction;
    GroupItem1: TMenuItem;
    Delete5: TMenuItem;
    FieldsEditAction: TAction;
    FieldsInsertAction: TAction;
    Edit1: TMenuItem;
    Insert1: TMenuItem;
    RelationsEditAction: TAction;
    RelationsInsertAction: TAction;
    Edit2: TMenuItem;
    Insert2: TMenuItem;
    DatabasesInsertAction: TAction;
    DatabasesEditAction: TAction;
    Insert3: TMenuItem;
    Edit3: TMenuItem;
    PopupMenu6: TPopupMenu;
    GroupGenerateAction: TAction;
    GroupDropObjectsAction: TAction;
    GroupCreateObjectsAction: TAction;
    GroupDropTriggersAction: TAction;
    Group2DropObjectsAction: TAction;
    Group2CreateObjectsAction: TAction;
    Group2DropTriggersAction: TAction;
    N1: TMenuItem;
    Createsystemobjects1: TMenuItem;
    Dropsystemobjects1: TMenuItem;
    Droptriggers1: TMenuItem;
    Group1Item1: TMenuItem;
    Createsystemobjects2: TMenuItem;
    Dropsystemobjects2: TMenuItem;
    Droptriggers2: TMenuItem;
    GeneratePKFKanddatafields1: TMenuItem;
    N2: TMenuItem;
    FieldsCopyFromAction: TAction;
    N3: TMenuItem;
    Copyfromrelation1: TMenuItem;
    RelationsCopyFromAction: TAction;
    N4: TMenuItem;
    Copyfromschema1: TMenuItem;
    SchemaSourceAction: TAction;
    Sourcedatabase1: TMenuItem;
    SchemaQuerySRCSCHID: TIntegerField;
    SchemaQueryGROUPID: TIntegerField;
    SchemaQueryDBID: TIntegerField;
    SchemaInsertAction: TAction;
    SchemaEditAction: TAction;
    N5: TMenuItem;
    Edit4: TMenuItem;
    Insert4: TMenuItem;
    GroupInsertAction: TAction;
    GroupEditAction: TAction;
    Edit5: TMenuItem;
    Insert5: TMenuItem;
    Group1InsertAction: TAction;
    Insert6: TMenuItem;
    N6: TMenuItem;
    SchemaDB1QueryDBID: TIntegerField;
    Help1: TMenuItem;
    AboutAction: TAction;
    About1: TMenuItem;
    StatusBar: TStatusBar;
    OpenDatabaseAction: TAction;
    CloseDatabaseAction: TAction;
    CreateDatabaseAction: TAction;
    ConfigAction: TAction;
    N7: TMenuItem;
    Opendatabase1: TMenuItem;
    Closedatabase1: TMenuItem;
    Createdatabase1: TMenuItem;
    N8: TMenuItem;
    Settings1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Splitter1: TSplitter;
    DBLog: TTerminal;
    TabSheet3: TTabSheet;
    StatSchemaQuery: TIBDataSet;
    StatSchemaQuerySCHEMAID: TIntegerField;
    StatSchemaQueryNAME: TIBStringField;
    StatSchemaQueryS_KEEP: TIBStringField;
    StatSchemaQueryS_INSERT: TIntegerField;
    StatSchemaQueryS_UPDATE: TIntegerField;
    StatSchemaQueryS_DELETE: TIntegerField;
    StatSchemaQueryS_CONFLICT: TIntegerField;
    StatSchemaQueryS_ERROR: TIntegerField;
    StatSchemaQueryS_MSEC: TIntegerField;
    DataSource7: TDataSource;
    Statistics_Relation: TDBGrid;
    Splitter6: TSplitter;
    StatSchemaDB1Query: TIBDataSet;
    StatSchemaDB1QueryGROUPID: TIntegerField;
    StatSchemaDB1QueryCOUNT: TIntegerField;
    StatSchemaDB1QuerySCHEMAID: TIntegerField;
    DataSource8: TDataSource;
    Statistics_Group: TDBGrid;
    Splitter7: TSplitter;
    Statistics_Schema: TDBGrid;
    StatRelationsQuery: TIBDataSet;
    StatRelationsQueryRELATIONID: TIntegerField;
    StatRelationsQueryRELATIONNAME: TIBStringField;
    StatRelationsQueryTARGETNAME: TIBStringField;
    StatRelationsQueryTARGETTYPE: TIBStringField;
    StatRelationsQueryDISABLED: TIBStringField;
    StatRelationsQuerySCHEMAID: TIntegerField;
    StatRelationsQueryGROUPID: TIntegerField;
    StatRelationsQueryS_KEEP: TIBStringField;
    StatRelationsQueryS_INSERT: TIntegerField;
    StatRelationsQueryS_UPDATE: TIntegerField;
    StatRelationsQueryS_DELETE: TIntegerField;
    StatRelationsQueryS_CONFLICT: TIntegerField;
    StatRelationsQueryS_ERROR: TIntegerField;
    StatRelationsQueryS_MSEC: TIntegerField;
    DataSource9: TDataSource;
    StatClearSchemaAction: TAction;
    StatClearGroupAction: TAction;
    StatClearRelationAction: TAction;
    Statistics1: TMenuItem;
    Clearschemastatistics1: TMenuItem;
    Cleargroupstatistics1: TMenuItem;
    Clearrelationstatistics1: TMenuItem;
    PopupMenu7: TPopupMenu;
    PopupMenu8: TPopupMenu;
    PopupMenu9: TPopupMenu;
    Clearschemastatistics2: TMenuItem;
    Cleargroupstatistics2: TMenuItem;
    Clearrelationstatistics2: TMenuItem;
    ImageList1: TImageList;
    ClearLogAction: TAction;
    Clearlog1: TMenuItem;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    HelpContentsAction: TAction;
    N9: TMenuItem;
    Content1: TMenuItem;
    SchemaQueryREPLUSER: TIBStringField;
    GroupCreateServerObjectsAction: TAction;
    GroupDropServerObjectsAction: TAction;
    Group2DropServerObjectsAction: TAction;
    Group2CreateServerObjectsAction: TAction;
    Createserverobjects1: TMenuItem;
    Dropserverobjects1: TMenuItem;
    Createserverobjects2: TMenuItem;
    Dropserverobjects2: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    DatabasesQueryDBTYPE: TIntegerField;
    SchemaQuerySCHEMATYPE: TIntegerField;
    UpgradeDatabaseAction: TAction;
    Upgradedatabase1: TMenuItem;
    Group2CreateStoredProcAction: TAction;
    Createstoredprocedures1: TMenuItem;
    DatabasesUpgradeAction: TAction;
    N12: TMenuItem;
    Upgradedatabase2: TMenuItem;
    SchemaQueryGROUPID1: TIntegerField;
    SchemaQueryGROUPID2: TIntegerField;
    ConfigEnvironmentAction: TAction;
    DatabasesEnvironmentAction: TAction;
    Environment1: TMenuItem;
    Environment2: TMenuItem;
    DBNavigator1: TDBNavigator;
    ToolButton7: TToolButton;
    N13: TMenuItem;
    DatabasesDropObjectsAction: TAction;
    DatabasesCreateObjectsAction: TAction;
    DatabasesDropTriggersAction: TAction;
    DatabasesCreateServerObjectsAction: TAction;
    DatabasesDropServerObjectsAction: TAction;
    Createsystemobjects3: TMenuItem;
    Dropsystemobjects3: TMenuItem;
    Droptriggers3: TMenuItem;
    N14: TMenuItem;
    Addserveruser1: TMenuItem;
    Dropsystemobjects4: TMenuItem;
    Prefix_Insert_ToolButton: TToolButton;
    Prefix_Edit_ToolButton: TToolButton;
    Prefix_Delete_ToolButton: TToolButton;
    ToolButton11: TToolButton;
    Prefix_CreateObjects_ToolButton: TToolButton;
    Prefix_CreateServerObjects_ToolButton: TToolButton;
    ToolButton14: TToolButton;
    ToolButton15: TToolButton;
    ToolButton16: TToolButton;
    ToolButton17: TToolButton;
    ToolButton18: TToolButton;
    ToolButton19: TToolButton;
    ToolButton20: TToolButton;
    Prefix_CopyFrom_ToolButton: TToolButton;
    StatToolButton: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    ImageList2: TImageList;
    SchemaQueryDISABLED: TIBStringField;
    DatabasesBackupAction: TAction;
    DatabasesRestoreAction: TAction;
    N15: TMenuItem;
    Backup1: TMenuItem;
    Restore1: TMenuItem;
    BackupOpenDialog: TOpenDialog;
    BackupSaveDialog: TSaveDialog;
    BackupDatabaseAction: TAction;
    RestoreDatabaseAction: TAction;
    N16: TMenuItem;
    Backup2: TMenuItem;
    Restore2: TMenuItem;
    Group2CloneSourceDatabaseAction: TAction;
    Group2CloneEmptySourceDatabaseAction: TAction;
    N17: TMenuItem;
    Clone1: TMenuItem;
    Cloneempty1: TMenuItem;
    Group2CleanData: TAction;
    GroupCleanData: TAction;
    Cleansourcedata1: TMenuItem;
    Cleansourcedata2: TMenuItem;
    SchemaDB2QueryDBTYPE: TIntegerField;
    RegisterAction: TAction;
    Register1: TMenuItem;
    HomePageAction: TAction;
    N18: TMenuItem;
    Homepage1: TMenuItem;
    DatabasesSortAction: TAction;
    SchemaSortAction: TAction;
    RelationsSortAction: TAction;
    FieldsSortAction: TAction;
    N19: TMenuItem;
    Sort1: TMenuItem;
    N20: TMenuItem;
    Sort2: TMenuItem;
    Sort3: TMenuItem;
    Sort4: TMenuItem;
    IncSearchTimer: TTimer;
    Prefix_Sort_ToolButton: TToolButton;
    StatSortRelationAction: TAction;
    StatSortSchemaAction: TAction;
    N21: TMenuItem;
    Schemastatisticsorder1: TMenuItem;
    Relationstatisticsorder1: TMenuItem;
    Upgradedatabase3: TMenuItem;
    Relationstatisticsorder2: TMenuItem;
    Group2SortAction: TAction;
    N22: TMenuItem;
    Orderby1: TMenuItem;
    SupportAction: TAction;
    Supportforum1: TMenuItem;
    RemoveOrphanedAction: TAction;
    Removeorphanedfields1: TMenuItem;
    Group1ValidateAction: TAction;
    Group2ValidateAction: TAction;
    Validate1: TMenuItem;
    N23: TMenuItem;
    Validate2: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure DatabasesQueryBeforeOpen(DataSet: TDataSet);
    procedure CommitActionExecute(Sender: TObject);
    procedure RollbackActionExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure SchemaDB1QueryCOUNTGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure FieldsDeleteActionExecute(Sender: TObject);
    procedure Databases_DBGridEnter(Sender: TObject);
    procedure RelationsQueryBeforeDelete(DataSet: TDataSet);
    procedure SchemaQueryBeforeDelete(DataSet: TDataSet);
    procedure RelationsDeleteActionExecute(Sender: TObject);
    procedure SchemaDB1QueryBeforeDelete(DataSet: TDataSet);
    procedure SchemaDeleteActionExecute(Sender: TObject);
    procedure DatabasesDeleteActionExecute(Sender: TObject);
    procedure GroupDeleteActionExecute(Sender: TObject);
    procedure FieldsEditActionExecute(Sender: TObject);
    procedure FieldsInsertActionExecute(Sender: TObject);
    procedure PopupMenu5Popup(Sender: TObject);
    procedure PopupMenu4Popup(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure PopupMenu2Popup(Sender: TObject);
    procedure PopupMenu3Popup(Sender: TObject);
    procedure Fields_DBGrid4DblClick(Sender: TObject);
    procedure Relations_DBGrid3DblClick(Sender: TObject);
    procedure Databases_DBGridDblClick(Sender: TObject);
    procedure Schema_DBGrid2DblClick(Sender: TObject);
    procedure SchemaDB2_DBGrid1DblClick(Sender: TObject);
    procedure RelationsEditActionExecute(Sender: TObject);
    procedure RelationsInsertActionExecute(Sender: TObject);
    procedure DatabasesInsertActionExecute(Sender: TObject);
    procedure DatabasesEditActionExecute(Sender: TObject);
    procedure GroupGenerateActionExecute(Sender: TObject);
    procedure GroupCreateObjectsActionExecute(Sender: TObject);
    procedure GroupDropObjectsActionExecute(Sender: TObject);
    procedure GroupDropTriggersActionExecute(Sender: TObject);
    procedure Group2CreateObjectsActionExecute(Sender: TObject);
    procedure Group2DropObjectsActionExecute(Sender: TObject);
    procedure Group2DropTriggersActionExecute(Sender: TObject);
    procedure PopupMenu6Popup(Sender: TObject);
    procedure FieldsCopyFromActionExecute(Sender: TObject);
    procedure RelationsCopyFromActionExecute(Sender: TObject);
    procedure SchemaSourceActionExecute(Sender: TObject);
    procedure SchemaInsertActionExecute(Sender: TObject);
    procedure SchemaEditActionExecute(Sender: TObject);
    procedure GroupInsertActionExecute(Sender: TObject);
    procedure GroupEditActionExecute(Sender: TObject);
    procedure Group1InsertActionExecute(Sender: TObject);
    procedure AboutActionExecute(Sender: TObject);
    procedure OpenDatabaseActionExecute(Sender: TObject);
    procedure CloseDatabaseActionExecute(Sender: TObject);
    procedure ConfigActionExecute(Sender: TObject);
    procedure CreateDatabaseActionExecute(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure StatClearRelationActionExecute(Sender: TObject);
    procedure StatClearGroupActionExecute(Sender: TObject);
    procedure StatClearSchemaActionExecute(Sender: TObject);
    procedure ClearLogActionExecute(Sender: TObject);
    procedure DataSource1DataChange(Sender: TObject; Field: TField);
    procedure HelpContentsActionExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure GroupCreateServerObjectsActionExecute(Sender: TObject);
    procedure GroupDropServerObjectsActionExecute(Sender: TObject);
    procedure Group2CreateServerObjectsActionExecute(Sender: TObject);
    procedure Group2DropServerObjectsActionExecute(Sender: TObject);
    procedure DataSource2DataChange(Sender: TObject; Field: TField);
    procedure UpgradeDatabaseActionExecute(Sender: TObject);
    procedure Group2CreateStoredProcActionExecute(Sender: TObject);
    procedure DatabasesUpgradeActionExecute(Sender: TObject);
    procedure SchemaQueryCalcFields(DataSet: TDataSet);
    procedure ConfigEnvironmentActionExecute(Sender: TObject);
    procedure DatabasesEnvironmentActionExecute(Sender: TObject);
    procedure FieldsQueryOPTIONSGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure DatabasesCreateObjectsActionExecute(Sender: TObject);
    procedure DatabasesDropObjectsActionExecute(Sender: TObject);
    procedure DatabasesDropTriggersActionExecute(Sender: TObject);
    procedure DatabasesCreateServerObjectsActionExecute(Sender: TObject);
    procedure DatabasesDropServerObjectsActionExecute(Sender: TObject);
    procedure Relations_DBGrid3DrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure Fields_DBGrid4DrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure Databases_DBGridDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure Schema_DBGrid2DrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure SchemaDB2_DBGrid1DrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure DatabasesBackupActionExecute(Sender: TObject);
    procedure DatabasesRestoreActionExecute(Sender: TObject);
    procedure BackupDatabaseActionExecute(Sender: TObject);
    procedure RestoreDatabaseActionExecute(Sender: TObject);
    procedure Group2CloneSourceDatabaseActionExecute(Sender: TObject);
    procedure Group2CloneEmptySourceDatabaseActionExecute(Sender: TObject);
    procedure Group2CleanDataExecute(Sender: TObject);
    procedure GroupCleanDataExecute(Sender: TObject);
    procedure RegisterActionExecute(Sender: TObject);
    procedure HomePageActionExecute(Sender: TObject);
    procedure SupportActionExecute(Sender: TObject);
    {$IFDEF LINUX}
    procedure FormShortCut(Key: Integer; Shift: TShiftState; var Handled: Boolean);
    {$ELSE}
    procedure FormShortCut(var Msg: TWMKey; var Handled: Boolean);
    {$ENDIF}
    procedure DatabasesSortActionExecute(Sender: TObject);
    procedure RelationsSortActionExecute(Sender: TObject);
    procedure SchemaSortActionExecute(Sender: TObject);
    procedure FieldsSortActionExecute(Sender: TObject);
    procedure Fields_DBGridKeyPress(Sender: TObject; var Key: Char);
    procedure IncSearchTimerTimer(Sender: TObject);
    procedure Databases_DBGridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Databases_DBGridExit(Sender: TObject);
    procedure StatSortRelationActionExecute(Sender: TObject);
    procedure StatSortSchemaActionExecute(Sender: TObject);
    procedure Group2SortActionExecute(Sender: TObject);
    procedure RemoveOrphanedActionExecute(Sender: TObject);
    procedure Group1ValidateActionExecute(Sender: TObject);
    procedure Group2ValidateActionExecute(Sender: TObject);
  private
    fChangedFlag: Boolean;
    fKeyLength: Integer;
    fIncSearch: string;
//    fNowUTC: Boolean;
//    procedure DBSqlExec(aSQL: string);
    function DBSqlRecord(aSQL: string): Variant;
    procedure ConfirmDelete;
    procedure DeleteDep(Q: TDataSet);
    procedure CopyMenuItems(aFrom: TMenuItem; aTo: TObject);
    procedure CheckSave;
    procedure StatusLineOnHint(Sender: TObject);
    procedure ReadFromIni;
    procedure IBCfgDBBeforeConnect(Sender: TObject);
    procedure IBCfgDBAfterDisconnect(Sender: TObject);
    function SetEnvironment(aDB: TIBDatabase; const aObjPrefix, aCtx: string): Boolean;
    procedure DrawCellBmp(Sender: TDBGrid; const Rect: TRect; Idx: Integer);
    procedure SetSort(aDataSet: TDataSet);
  public
    { Public declarations }
  end;

var
  SchemaEditor: TSchemaEditor;

implementation
uses
  AuxProj, dm_IBRepl, dm_IBRepl_Man, IBReplicator, IBHeader, dg_SchemaFields, dg_SchemaRelations, dg_SchemaDatabases, dg_CopyFrom, dg_SchemaSource, dg_SchemaTarget, dg_Schema,
  {$IFNDEF VER130}
  {$IFNDEF LINUX}
  Variants,
  {$ENDIF}
  {$ENDIF}
  {$IFNDEF LINUX}
  dg_About, ShellAPI,
  {$ENDIF}
  {$IFDEF REGISTRATION}
  {$IFDEF VER120}FileCtrl, {$ENDIF}{$IFDEF VER130}FileCtrl, {$ENDIF}
  RegIBReplicatorDlg, IBReplicatorDeploy, {$IFNDEF LINUX}Registry, {$ENDIF}{$ENDIF}
  dg_Environment,
  dg_ManSettings, dg_Sort;

{$IFNDEF LINUX}
{$R *.dfm}
{$ELSE}
{$R *.xfm}   // cause problem in D7 IDE if mentioned above *.dfm
{$ENDIF}

{$IFDEF REGISTRATION}
{$I RegClass.inc}
{$I RegIBReplicator.inc}
{$I RegCheckReg.inc}

{$I RegIBReplicatorCheckPro.inc}
{$ENDIF}

resourcestring
  sNotAvailableForTextFile = 'Not available for text file';
  sProfessional = ' Professional';
  sStandard = ' Standard';
{$IFDEF REGISTRATION}
  sProfessional2 = ' [%d day(s) remaining]';
  sStandard2 = ' [ purchase Professional ]';
{$ENDIF}
procedure TSchemaEditor.FormCreate(Sender: TObject);
var
  ProF: Integer;
begin
  Application.CreateForm(TIBReplDataModule2, IBReplDataModule);  // I need TTerminals!!!
{$IFDEF LINUX}
  HelpContentsAction.Visible:= False;
  HomePageAction.Visible:= False;
  SupportAction.Visible:= False;
{$ENDIF}
  {$IFDEF REGISTRATION}
  try
    _CheckRegistration(False);
    ProF:= TReg_CheckPro('');
  except
    ProF:= 0;
  end;
  {$ELSE}
  ProF:= MaxInt;
  RegisterAction.Enabled:= False;
  Register1.Visible:= False;
  {$ENDIF}
  if ProF > 0 then
    begin
      Caption:= Caption+sProfessional;
      {$IFDEF REGISTRATION}
      if ProF < MaxInt then
        Caption:= Caption+Format(sProfessional2, [Prof]);
      {$ENDIF}
    end
  else
    Caption:= Caption{$IFDEF REGISTRATION}+sStandard+sStandard2{$ENDIF};

  PageControl1.ActivePageIndex:= 0;
  IBReplDataModule.IBReplicator.ConfigDatabase.Close;
  IBReplDataModule.IBReplicator.ConfigDatabase.BeforeConnect:= IBCfgDBBeforeConnect;
  IBReplDataModule.IBReplicator.ConfigDatabase.AfterDisconnect:= IBCfgDBAfterDisconnect;
  Application.OnHint:= StatusLineOnHint;

  ReadFromIni;
  IBReplDataModule.ReadFromIni(False, True);
  PageControl1Change(nil);
end;

type
  TCustomSQLSort = record
    Captions: string;
    Fields: string;
  end;

const
  CustomSQLSort: array[1..7] of TCustomSQLSort = (
    (Captions:'Id,Name,Type,File name'; Fields:'DBID,NAME,DBTYPE,FILENAME'),
    (Captions:'Id,Name,Type,Database name,Disabled,Repl.user'; Fields:'S.SCHEMAID,S.NAME,S.SCHEMATYPE,D.NAME,SD.DISABLED,SD.REPLUSER'),
    (Captions:'Id,Database name,Database type,Disabled,Repl.user'; Fields:'D.DBID,D.NAME,D.DBTYPE,SD.DISABLED,SD.REPLUSER'),
    (Captions:'Id,Source name,Target name,Target type,Disabled,Sync.order'; Fields:'RELATIONID,RELATIONNAME,TARGETNAME,TARGETTYPE,DISABLED,SYNCORDER'),
    (Captions:'Id,Type,Source name,Target name'; Fields:'FIELDID,FIELDTYPE,FIELDNAME,TARGETNAME'),
    (Captions:'Id,Name,Insert #,Update #,Delete #,Conflict #,Error #,Time'; Fields:'SCHEMAID,NAME,S_INSERT,S_UPDATE,S_DELETE,S_CONFLICT,S_ERROR,S_MSEC'),
    (Captions:'Id,Source name,Target name,Type,Insert #,Update #,Delete #,Conflict #,Error #,Time'; Fields:'RELATIONID,RELATIONNAME,TARGETNAME,TARGETTYPE,S_INSERT,S_UPDATE,S_DELETE,S_CONFLICT,S_ERROR,S_MSEC')
  );

procedure TSchemaEditor.DatabasesQueryBeforeOpen(DataSet: TDataSet);
  procedure AdjSQL(aSQL: TStrings; const aParse, aReplS: string);
  var
    S, S2: string;
    I, J: Integer;
  begin
    S:= aSQL.Text;
    repeat
      I:= Pos(aParse, S);
      if I > 0 then
      begin
        Delete(S, I, Length(aParse));
        J:= I;
        while S[J] <> '"' do
          Inc(J);
        S2:= Copy(S, I, J-I);
        Delete(S, I, J-I+1);
        Insert(TIBReplicator.FormatIdentifier((DataSet as TIBDataSet).Database.SQLDialect, aReplS+S2), S, I);
      end;
    until I=0;
    aSQL.Text:= S;
  end;
var
  I, J, K: Integer;
  S, S2: string;
  Sg, Sg2: TStringOpenArray;
begin
  Sg:= StringToArray('', ',');   // unwarning
  Sg2:= Sg;
  if csDestroying in ComponentState then
    Exit;
  IBReplDataModule.IBReplicator.ConfigDatabase.Open;
  StatusLineOnHint(nil);

  with (DataSet as TIBDataSet) do
  begin
    AdjSQL(SelectSQL, '/*$*/"', IBReplDataModule.IBReplicator.ConfigDatabasePrefix);
    AdjSQL(DeleteSQL, '/*$*/"', IBReplDataModule.IBReplicator.ConfigDatabasePrefix);
    AdjSQL(ModifySQL, '/*$*/"', IBReplDataModule.IBReplicator.ConfigDatabasePrefix);
    RefreshSQL.Text:= SelectSQL.Text;
    I:= Pos('/*ORDER BY*/', SelectSQL.Text);
    if I > 0 then
    begin
      SelectSQL.Text:= Copy(SelectSQL.Text, 1, I+Length('/*ORDER BY*/'));
      S:= Ini.ReadString(DBIniSection+DBIniSection2, Format('Sort.%d', [DataSet.Tag]), '*0');
      Sg:= StringToArray(S, ',');
      S2:= '';
      for J:= 0 to Length(Sg)-1 do
      begin
        S:= Sg[J];
        if Pos('*', S) = 1 then
        begin
          System.Delete(S, 1, 1);
          K:= StrToIntDef(S, -1);
          Sg2:= StringToArray(CustomSQLSort[DataSet.Tag].Fields, ',');
          if (K >= 0) and (K < Length(Sg2)) then
          begin
            if S2 <> '' then
              S2:= S2+',';
            S2:= S2+Sg2[K];
          end;
        end;
      end;
      if S2 <> '' then
        SelectSQL.Text:= SelectSQL.Text + ' ORDER BY '+S2;
    end;
    RefreshSQL.Text:= StringReplace(RefreshSQL.Text, '/*WHERE*/', 'WHERE 1=1 ', [rfReplaceAll]);
    if DeleteSQL.Count > 1 then
    begin
      RefreshSQL.Text:= StringReplace(RefreshSQL.Text, '/*PRIMARY*/', 'AND '+DeleteSQL[1], [rfReplaceAll]);
      if DeleteSQL[0] = '/*' then
        DeleteSQL.Text:= '';
    end;
  end;
end;

procedure TSchemaEditor.CommitActionExecute(Sender: TObject);
begin
  IBTransaction.CommitRetaining;
  fChangedFlag:= False;
end;

procedure TSchemaEditor.RollbackActionExecute(Sender: TObject);
resourcestring
  sRollbackConfirm = 'Rollback all configuration changes. Are you sure?';
begin
  if fChangedFlag and (MessageDlg(sRollbackConfirm, mtConfirmation, [mbYes, mbNo], 0) <> mrYes) then
    Exit;
  IBTransaction.RollbackRetaining;
  fChangedFlag:= False;
  DatabasesQuery.Close;
  SchemaQuery.Close;
  StatSchemaQuery.Close;
  OpenDatabaseActionExecute(nil);
end;

procedure TSchemaEditor.CheckSave;
resourcestring
  sCommitChanges = 'Commit changes?';
begin
  if fChangedFlag then
    case MessageDlg(sCommitChanges, mtConfirmation, [mbYes, mbNo, mbCancel], 0) of
      mrYes: IBTransaction.CommitRetaining;
      mrNo: IBTransaction.RollbackRetaining;
      mrCancel: Abort;
    end;
end;

procedure TSchemaEditor.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
   CheckSave;
end;

procedure TSchemaEditor.SchemaDB1QueryCOUNTGetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
resourcestring
  sDatabaseNumber = '%d database(s)';
begin
  Text:= Format(sDatabaseNumber, [Sender.asInteger]);
end;

(*procedure TSchemaEditor.DBSQLExec(aSQL: string);
var
  Q: TIBSQL;
begin
  Q:= TIBSQL.Create(nil);
  try
    Q.Transaction:= IBTransaction;
    Q.Database:= Q.Transaction.DefaultDatabase;
    Q.SQL.Text:= aSQL;
    Q.ExecQuery;
  finally
    Q.Free;
  end;
end;*)

{$IFNDEF LINUX}
  {$IFDEF VER150}
    {$DEFINE __SqlVar}
  {$ENDIF}
  {$IFDEF VER160}
    {$DEFINE __SqlVar}
  {$ENDIF}
  {$IFDEF VER170}
    {$DEFINE __SqlVar}
  {$ENDIF}
  {$IFDEF VER180}
    {$DEFINE __SqlVar}
  {$ENDIF}
{$ENDIF}
function TSchemaEditor.DBSQLRecord;
var
  Q: TIBSQL;
  I: Integer;
  function GetAsVar(F: TIBXSQLVAR): Variant;
  begin
    case F.{$IFNDEF LINUX}{$IFDEF __SqlVar}SqlVar{$ELSE}asXSQLVar{$ENDIF}.{$ENDIF}sqltype and (not 1) of
      SQL_BLOB:
        Result := F.asString; {do not localize}
    else
      Result:= F.Value;
    end;
  end;
begin
  Result:= Null;
  Q:= TIBSQL.Create(nil);
  try
    Q.Transaction:= IBTransaction;
    Q.Database:= Q.Transaction.DefaultDatabase;
    Q.SQL.Text:= aSQL;
    Q.ExecQuery;
    if Q.RecordCount > 0 then
      if Q.Current.Count = 1 then
        Result:= GetAsVar(Q.Fields[0])
      else if Q.Current.Count > 1 then
        begin
          Result:= VarArrayCreate([0, Q.Current.Count-1], varVariant);
          for I:= 0 to Q.Current.Count-1 do
            Result[I]:= GetAsVar(Q.Fields[I]);
        end;
  finally
    Q.Free;
  end;
end;

procedure TSchemaEditor.FieldsDeleteActionExecute(Sender: TObject);
begin
  ConfirmDelete;
  FieldsQuery.Delete;
  fChangedFlag:= True;
end;

procedure TSchemaEditor.ConfirmDelete;
resourcestring
  sDeleteRecord = 'Delete record?';
begin
  if MessageDlg(sDeleteRecord, mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
    Abort;
end;

procedure TSchemaEditor.Databases_DBGridEnter(Sender: TObject);
var
  I, J: Integer;
  S, S2, S3: string;
  TgM: Integer;
  DS: TIBDataSet;
  C: TComponent;
  T: TToolButton;
begin
  TgM:= 0;
  if Sender = nil then
    S:= ' dummy '
  else
    begin
      S2:= (Sender as TComponent).Name;
      I:= Pos('_', S2+'_');
      S:= Copy(S2, 1, I-1);
      Delete(S2, 1, I);
      I:= Pos('_', S2);
      if I > 0 then
        Delete(S2, I, Length(S2));
      if Sender is TDBGrid then
      begin
        DS:= Pointer((Sender as TDBGrid).DataSource.DataSet);
        if DS <> nil then
        begin
          if not DS.IsEmpty then
            TgM:= TgM or 1;   // edit possible
          if DS.DataSource = nil then
            TgM:= TgM or 2   // no master, insert possible
          else
            begin
              DS:= Pointer(DS.DataSource.DataSet);
              if (DS <> nil) and not DS.IsEmpty then
                TgM:= TgM or 2;  // master is not empty, insert possible
            end;
        end;
      end;
    end;
  for I:= 0 to ActionList1.ActionCount-1 do
    with ActionList1.Actions[I] as TAction do
    begin
      if Category <> '' then
      begin
        Enabled:= ((Category = S) and (PageControl1.ActivePageIndex = 1) or
                  (Category = S) and (S='Statistics') and (PageControl1.ActivePageIndex = 2)) and
                  ((Tag = 0) or (Tgm and Tag <> 0));
        if Enabled then
        begin    // assign tool actions
          for J:= 0 to ToolBar1.ButtonCount -1 do
          begin
            {$IFDEF LINUX}
            T:= TToolButton(Toolbar1.Controls[J]);
            {$ELSE}
            T:= Toolbar1.Buttons[J];
            {$ENDIF}
            S3:= T.Name;
            if Pos('Prefix_', S3) = 1 then
            begin
              Delete(S3, 1, Length('Prefix_'));
              S3:= Copy(S3, 1, Pos('_', S3)-1);
              if Pos(S3, Name) > 0 then
                T.Action:= ActionList1.Actions[I];;
            end;
          end;
        end;
      end;
    end;

  if S = 'Statistics' then   // tool action for statistics
  begin
    C:= Self.FindComponent('StatClear'+S2+'Action');
    if C <> nil then
      StatToolButton.Action:= C as TAction
    else
      StatToolButton.Enabled:= False;
    C:= Self.FindComponent('StatSort'+S2+'Action');
    if C <> nil then
      Prefix_Sort_ToolButton.Action:= C as TAction
    else
      Prefix_Sort_ToolButton.Enabled:= False;
      

  end;

  if Sender is TDBGrid then
    begin
      DBNavigator1.DataSource:= TDBGrid(Sender).DataSource;
    end
  else
    DBNavigator1.DataSource:= nil;
end;

procedure TSchemaEditor.RelationsQueryBeforeDelete(DataSet: TDataSet);
begin
  DeleteDep(FieldsQuery);
end;

procedure TSchemaEditor.DeleteDep(Q: TDataSet);
begin
  Q.DisableControls;
  try
    Q.First;
    while not Q.IsEmpty do
      Q.Delete;
  finally
    Q.EnableControls;
  end;
end;

procedure TSchemaEditor.SchemaQueryBeforeDelete(DataSet: TDataSet);
begin
  DeleteDep(SchemaDB1Query);
end;

procedure TSchemaEditor.RelationsDeleteActionExecute(Sender: TObject);
begin
  ConfirmDelete;
  RelationsQuery.Delete;
  fChangedFlag:= True;
end;

procedure TSchemaEditor.SchemaDB1QueryBeforeDelete(DataSet: TDataSet);
begin
  DeleteDep(SchemaDB2Query);
  DeleteDep(RelationsQuery);
end;

procedure TSchemaEditor.SchemaDeleteActionExecute(Sender: TObject);
resourcestring
  sSchemaAll = 'Delete all groups and relations?';
begin
  if SchemaDB1Query.IsEmpty then
    ConfirmDelete
  else
    begin
      if MessageDlg(sSchemaAll, mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
        Abort;
    end;
  SchemaQuery.Delete;
  fChangedFlag:= True;
end;

procedure TSchemaEditor.DatabasesDeleteActionExecute(Sender: TObject);
var
  V: Variant;
resourcestring
  sDeleteDatabases = 'Delete this databases from SCHEMADB first';
begin

  V:= DBSQLRecord(Format('SELECT CAST(COUNT(*) AS INTEGER) FROM %s WHERE DBID=%d',
                         [TIBReplicator.FormatIdentifier(IBTransaction.DefaultDatabase.SQLDialect, IBReplDataModule.IBReplicator.ConfigDatabasePrefix+'SCHEMADB'), DatabasesQuery.FieldByName('DBID').asInteger]));
  if Integer(V) > 0 then
    IBReplicatorError(sDeleteDatabases, 1);
  ConfirmDelete;
  DatabasesQuery.Delete;
  fChangedFlag:= True;
end;

procedure TSchemaEditor.GroupDeleteActionExecute(Sender: TObject);
resourcestring
  sSchemaDB2All = 'Delete all relations?';
begin
  if (SchemaDB1Query.FieldByName('COUNT2').asInteger > 1) or RelationsQuery.IsEmpty then
    begin
      ConfirmDelete;
    end
  else
    begin
      if MessageDlg(sSchemaDB2All, mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
        Abort;
      DeleteDep(RelationsQuery);
    end;
  SchemaDB2Query.Delete;
  fChangedFlag:= True;
  SchemaDB1Query.Close;
  SchemaDB1Query.Open; //Refresh is not enough;
end;

procedure TSchemaEditor.FieldsEditActionExecute(Sender: TObject);
begin
  with TSchemaFieldsDialog.Create(nil) do
  try
    SchemaType:= SchemaQuery.FieldByName('SCHEMATYPE').asInteger;
    fChangedFlag:= EditRecord(FieldsQuery) or fChangedFlag;
  finally
    Release;
  end;
end;

procedure TSchemaEditor.FieldsInsertActionExecute(Sender: TObject);
begin
  with TSchemaFieldsDialog.Create(nil) do
  try
    SchemaType:= SchemaQuery.FieldByName('SCHEMATYPE').asInteger;
    fChangedFlag:= InsertRecord(FieldsQuery, RelationsQuery) or fChangedFlag;
  finally
    Release;
  end;
end;

procedure TSchemaEditor.PopupMenu5Popup(Sender: TObject);
begin
  CopyMenuItems(FieldItem1, Sender);
end;

procedure TSchemaEditor.CopyMenuItems(aFrom: TMenuItem; aTo: TObject);
var
  I: Integer;
  PM: TPopupMenu;
  MI: TMenuItem;
begin
  if aTo is TPopupMenu then
    PM:= Pointer(aTo)
  else
    Exit;   // when called for ShortCut
  PM.Items.Clear;
  for I:= 0 to aFrom.Count-1 do
  begin
    MI:= TMenuItem.Create(nil);
    MI.Caption:= aFrom.Items[I].Caption;
    MI.Action:= aFrom.Items[I].Action;
    {$IFDEF LINUX}
    {$ELSE}
    MI.Break:= aFrom.Items[I].Break;
    {$ENDIF}
    MI.Visible:= aFrom.Items[I].Visible;
    PM.Items.Add(MI);
  end;
end;

procedure TSchemaEditor.PopupMenu4Popup(Sender: TObject);
begin
  CopyMenuItems(RelationItem1, Sender);
end;

procedure TSchemaEditor.PopupMenu1Popup(Sender: TObject);
begin
  CopyMenuItems(DatabaseItem1, Sender);
end;

procedure TSchemaEditor.PopupMenu2Popup(Sender: TObject);
begin
  CopyMenuItems(SchemaItem1, Sender);
end;

procedure TSchemaEditor.PopupMenu3Popup(Sender: TObject);
begin
  CopyMenuItems(GroupItem1, Sender);
end;

procedure TSchemaEditor.Fields_DBGrid4DblClick(Sender: TObject);
begin
  if (Sender as TDBGrid).DataSource.DataSet.IsEmpty then
    FieldsInsertAction.Execute
  else
    FieldsEditAction.Execute;
end;

procedure TSchemaEditor.Relations_DBGrid3DblClick(Sender: TObject);
begin
  if (Sender as TDBGrid).DataSource.DataSet.IsEmpty then
    RelationsInsertAction.Execute
  else
    RelationsEditAction.Execute;
end;

procedure TSchemaEditor.Databases_DBGridDblClick(Sender: TObject);
begin
  if (Sender as TDBGrid).DataSource.DataSet.IsEmpty then
    DatabasesInsertAction.Execute
  else
    DatabasesEditAction.Execute;
end;

procedure TSchemaEditor.Schema_DBGrid2DblClick(Sender: TObject);
begin
  if (Sender as TDBGrid).DataSource.DataSet.IsEmpty then
    SchemaInsertAction.Execute
  else
    SchemaEditAction.Execute;
end;

procedure TSchemaEditor.SchemaDB2_DBGrid1DblClick(Sender: TObject);
begin
  if (Sender as TDBGrid).DataSource.DataSet.IsEmpty then
    GroupInsertAction.Execute
  else
    GroupEditAction.Execute;
end;

procedure TSchemaEditor.RelationsEditActionExecute(Sender: TObject);
begin
  with TSchemaRelationsDialog.Create(nil) do
  try
    SchemaType:= SchemaQuery.FieldByName('SCHEMATYPE').asInteger;
    fChangedFlag:= EditRecord(RelationsQuery) or fChangedFlag;
  finally
    Release;
  end;
end;

procedure TSchemaEditor.RelationsInsertActionExecute(Sender: TObject);
begin
  with TSchemaRelationsDialog.Create(nil) do
  try
    SchemaType:= SchemaQuery.FieldByName('SCHEMATYPE').asInteger;
    fChangedFlag:= InsertRecord(RelationsQuery, SchemaDB1Query) or fChangedFlag;
  finally
    Release;
  end;
end;

procedure TSchemaEditor.DatabasesInsertActionExecute(Sender: TObject);
begin
  with TSchemaDatabasesDialog.Create(nil) do
  try
    fChangedFlag:= InsertRecord(DatabasesQuery, nil) or fChangedFlag;
  finally
    Release;
  end;
end;

procedure TSchemaEditor.DatabasesEditActionExecute(Sender: TObject);
begin
  with TSchemaDatabasesDialog.Create(nil) do
  try
    fChangedFlag:= EditRecord(DatabasesQuery) or fChangedFlag;
    if not SchemaQuery.IsEmpty then
      SchemaQuery.Refresh;  // chtelo by open close aby se refreshovaly vsechny zdrojove databaze
    if not SchemaDB2Query.IsEmpty then
      SchemaDB2Query.Refresh;
  finally
    Release;
  end;
end;

procedure TSchemaEditor.GroupGenerateActionExecute(Sender: TObject);
begin
  CheckSave;
  IBReplDataModule.IBReplicator.GenerateFields(SchemaDB1Query.FieldByName('SCHEMAID').asInteger, SchemaDB1Query.FieldByName('GROUPID').asInteger);
  fChangedFlag:= True;
  SchemaDB1Query.Close;
  RelationsQuery.Close;
  SchemaDB1Query.Open; //Refresh is not enough;
  RelationsQuery.Open;
end;

procedure TSchemaEditor.RemoveOrphanedActionExecute(Sender: TObject);
resourcestring
  sRemoveOrphanedFields = 'Remove orphaned relations and fields from configuration ?';
begin
  CheckSave;
  if MessageDlg(sRemoveOrphanedFields, mtWarning, [mbOk, mbCancel], 0) <> mrOk then
    Abort;
  IBReplDataModule.IBReplicator.RemoveOrphanedFields(SchemaDB1Query.FieldByName('SCHEMAID').asInteger, SchemaDB1Query.FieldByName('GROUPID').asInteger);
  fChangedFlag:= True;
  RelationsQuery.Close;
  RelationsQuery.Open;
end;

procedure TSchemaEditor.GroupCreateObjectsActionExecute(Sender: TObject);
begin
  CheckSave;
  IBReplDataModule.IBReplicator.CreateSystemObjects(SchemaDB1Query.FieldByName('SCHEMAID').asInteger, fKeyLength, 0, 0);
end;

procedure TSchemaEditor.GroupDropObjectsActionExecute(Sender: TObject);
resourcestring
  sConfirmDelete = 'Do you want delete system objects of selected schema? All replication data will be lost.';
begin
  if MessageDlg(sConfirmDelete, mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
    Exit;
  CheckSave;
  IBReplDataModule.IBReplicator.DropSystemObjects(SchemaDB1Query.FieldByName('SCHEMAID').asInteger, 0, 0);
end;

procedure TSchemaEditor.GroupDropTriggersActionExecute(Sender: TObject);
begin
  CheckSave;
  IBReplDataModule.IBReplicator.DropSystemObjects(SchemaDB1Query.FieldByName('SCHEMAID').asInteger, 0, 0, True);
end;

procedure TSchemaEditor.GroupCreateServerObjectsActionExecute(
  Sender: TObject);
begin
  CheckSave;
  IBReplDataModule.IBReplicator.CreateServerObjects(SchemaDB1Query.FieldByName('SCHEMAID').asInteger, 0, 0);
end;

procedure TSchemaEditor.GroupDropServerObjectsActionExecute(
  Sender: TObject);
begin
  CheckSave;
  IBReplDataModule.IBReplicator.DropServerObjects(SchemaDB1Query.FieldByName('SCHEMAID').asInteger, 0, 0);
end;

procedure TSchemaEditor.Group2CreateObjectsActionExecute(Sender: TObject);
begin
  CheckSave;
  IBReplDataModule.IBReplicator.CreateSystemObjects(SchemaDB2Query.FieldByName('SCHEMAID').asInteger, fKeyLength, SchemaDB2Query.FieldByName('GROUPID').asInteger, SchemaDB2Query.FieldByName('DBID').asInteger);
end;

procedure TSchemaEditor.Group2DropObjectsActionExecute(Sender: TObject);
resourcestring
  sConfirmDelete = 'Do you want delete system objects of selected schema? All replication data will be lost.';
begin
  if MessageDlg(sConfirmDelete, mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
    Exit;
  CheckSave;
  IBReplDataModule.IBReplicator.DropSystemObjects(SchemaDB2Query.FieldByName('SCHEMAID').asInteger, SchemaDB2Query.FieldByName('GROUPID').asInteger, SchemaDB2Query.FieldByName('DBID').asInteger);
end;

procedure TSchemaEditor.Group2DropTriggersActionExecute(Sender: TObject);
begin
  CheckSave;  // probably no triggers in target database
  IBReplDataModule.IBReplicator.DropSystemObjects(SchemaDB2Query.FieldByName('SCHEMAID').asInteger, SchemaDB2Query.FieldByName('GROUPID').asInteger, SchemaDB2Query.FieldByName('DBID').asInteger, True);
end;

procedure TSchemaEditor.Group2CreateServerObjectsActionExecute(
  Sender: TObject);
begin
  CheckSave;
  IBReplDataModule.IBReplicator.CreateServerObjects(SchemaDB2Query.FieldByName('SCHEMAID').asInteger, SchemaDB2Query.FieldByName('GROUPID').asInteger, SchemaDB2Query.FieldByName('DBID').asInteger);
end;

procedure TSchemaEditor.Group2DropServerObjectsActionExecute(
  Sender: TObject);
begin
  CheckSave;
  IBReplDataModule.IBReplicator.DropServerObjects(SchemaDB2Query.FieldByName('SCHEMAID').asInteger, SchemaDB2Query.FieldByName('GROUPID').asInteger, SchemaDB2Query.FieldByName('DBID').asInteger);
end;

procedure TSchemaEditor.PopupMenu6Popup(Sender: TObject);
begin
  CopyMenuItems(Group1Item1, Sender);
end;

procedure TSchemaEditor.FieldsCopyFromActionExecute(Sender: TObject);
var
  Q: TIBSQL;
  aSchemaId, aGroupId, aRelationId: Integer;
begin
  with TCopyFromDlg.Create(nil) do
  try
    SchemaId.Text:= RelationsQuery.FieldByName('SCHEMAID').asString;
    GroupId.Text:= RelationsQuery.FieldByName('GROUPID').asString;
    RelationId.Text:= '';
    if ShowModal <> mrOk then
      Abort;
    aSchemaId:= StrToInt(SchemaId.Text);
    aGroupId:= StrToInt(GroupId.Text);
    aRelationId:= StrToInt(RelationId.Text);
  finally
    Release;
  end;
  Q:= TIBSQL.Create(nil);
  try
    Q.Database:= FieldsQuery.Database;
    Q.Transaction:= FieldsQuery.Transaction;
    Q.SQL.Text:= 'INSERT INTO '+FieldsQuery.SelectSQL[1]+'(SCHEMAID,GROUPID,RELATIONID,FIELDID,FIELDNAME,FIELDTYPE,TARGETNAME,OPTIONS)';
    Q.SQL.Add(Format('SELECT %d,%d,%d,FIELDID,FIELDNAME,FIELDTYPE,TARGETNAME,OPTIONS FROM %s', [RelationsQuery.FieldByName('SCHEMAID').asInteger, RelationsQuery.FieldByName('GROUPID').asInteger, RelationsQuery.FieldByName('RELATIONID').asInteger, FieldsQuery.SelectSQL[1]]));
    Q.SQL.Add(Format('WHERE SCHEMAID=%d AND GROUPID=%d AND RELATIONID=%d AND FIELDID NOT IN', [aSchemaId, aGroupId, aRelationId]));
    Q.SQL.Add(Format('(SELECT FIELDID FROM %s WHERE SCHEMAID=%d AND GROUPID=%d AND RELATIONID=%d)', [FieldsQuery.SelectSQL[1], RelationsQuery.FieldByName('SCHEMAID').asInteger, RelationsQuery.FieldByName('GROUPID').asInteger, RelationsQuery.FieldByName('RELATIONID').asInteger]));
    Q.ExecQuery;
    fChangedFlag:= True;
    FieldsQuery.Close;
    FieldsQuery.Open;
  finally
    Q.Free;
  end;
end;

procedure TSchemaEditor.RelationsCopyFromActionExecute(Sender: TObject);
var
  Q: TIBSQL;
  aSchemaId, aGroupId: Integer;
begin
  with TCopyFromDlg.Create(nil) do
  try
    SchemaId.Text:= SchemaDB1Query.FieldByName('SCHEMAID').asString;
    GroupId.Text:= SchemaDB1Query.FieldByName('GROUPID').asString;
    RelationId.Enabled:= False;
    RelationId.ParentColor:= True;
    if ShowModal <> mrOk then
      Abort;
    aSchemaId:= StrToInt(SchemaId.Text);
    aGroupId:= StrToInt(GroupId.Text);
  finally
    Release;
  end;
  Q:= TIBSQL.Create(nil);
  try
    Q.Database:= FieldsQuery.Database;
    Q.Transaction:= FieldsQuery.Transaction;
    Q.SQL.Text:= 'INSERT INTO '+RelationsQuery.SelectSQL[1]+'(SCHEMAID,GROUPID,RELATIONID,RELATIONNAME,TARGETNAME,TARGETTYPE,DISABLED,CONDITION,SYNCACTIONS,SYNCORDER,OPTIONS,S_KEEP,COMMENT)';
    Q.SQL.Add(Format('SELECT %d,%d,RELATIONID,RELATIONNAME,TARGETNAME,TARGETTYPE,DISABLED,CONDITION,SYNCACTIONS,SYNCORDER,OPTIONS,S_KEEP,COMMENT FROM %s', [SchemaDB1Query.FieldByName('SCHEMAID').asInteger, SchemaDB1Query.FieldByName('GROUPID').asInteger, RelationsQuery.SelectSQL[1]]));
    Q.SQL.Add(Format('WHERE SCHEMAID=%d AND GROUPID=%d AND RELATIONID NOT IN', [aSchemaId, aGroupId]));
    Q.SQL.Add(Format('(SELECT RELATIONID FROM %s WHERE SCHEMAID=%d AND GROUPID=%d)', [RelationsQuery.SelectSQL[1], SchemaDB1Query.FieldByName('SCHEMAID').asInteger, SchemaDB1Query.FieldByName('GROUPID').asInteger]));
    Q.ExecQuery;
    fChangedFlag:= True;

    Q.SQL.Clear;
    Q.SQL.Text:= 'INSERT INTO '+FieldsQuery.SelectSQL[1]+'(SCHEMAID,GROUPID,RELATIONID,FIELDID,FIELDNAME,FIELDTYPE,TARGETNAME,OPTIONS)';
    Q.SQL.Add(Format('SELECT %d,%d,RELATIONID,FIELDID,FIELDNAME,FIELDTYPE,TARGETNAME,OPTIONS FROM %s', [SchemaDB1Query.FieldByName('SCHEMAID').asInteger, SchemaDB1Query.FieldByName('GROUPID').asInteger, FieldsQuery.SelectSQL[1]]));
    Q.SQL.Add(Format('WHERE SCHEMAID=%d AND GROUPID=%d AND 1000000*RELATIONID+FIELDID NOT IN', [aSchemaId, aGroupId]));
    Q.SQL.Add(Format('(SELECT 1000000*RELATIONID+FIELDID FROM %s WHERE SCHEMAID=%d AND GROUPID=%d)', [FieldsQuery.SelectSQL[1], SchemaDB1Query.FieldByName('SCHEMAID').asInteger, SchemaDB1Query.FieldByName('GROUPID').asInteger]));
    Q.ExecQuery;

    RelationsQuery.Close;
    RelationsQuery.Open;
  finally
    Q.Free;
  end;
end;

procedure TSchemaEditor.SchemaSourceActionExecute(Sender: TObject);
begin
  with TSchemaSourceDialog.Create(nil) do
  try
    SchemaType:= SchemaQuery.FieldByName('SCHEMATYPE').asInteger;
    if SchemaQuery.FieldByName('SRCSCHID').isNull then
      fChangedFlag:= InsertRecord(SchemaQuery, SchemaQuery, 'SCHEMAID') or fChangedFlag
    else
      fChangedFlag:= EditRecord(SchemaQuery, 'SCHEMAID') or fChangedFlag;
  finally
    Release;
  end;
end;

procedure TSchemaEditor.SchemaInsertActionExecute(Sender: TObject);
begin
  with TSchemaDialog.Create(nil) do
  try
    fChangedFlag:= InsertRecord(SchemaQuery, nil) or fChangedFlag;
  finally
    Release;
  end;
end;

procedure TSchemaEditor.SchemaEditActionExecute(Sender: TObject);
begin
  with TSchemaDialog.Create(nil) do
  try
    fChangedFlag:= EditRecord(SchemaQuery) or fChangedFlag;
  finally
    Release;
  end;
end;

procedure TSchemaEditor.GroupInsertActionExecute(Sender: TObject);
begin
  with TSchemaTargetDialog.Create(nil) do
  try
    GroupId.ReadOnly:= True;
    GroupId.TabStop:= False;
    GroupId.ParentColor:= True;
    fChangedFlag:= InsertRecord(SchemaDB2Query, SchemaDB1Query) or fChangedFlag;
    if not SchemaDB1Query.IsEmpty then
    begin
      SchemaDB1Query.Refresh;
      SchemaQuery.Refresh;  // bad select of SchemaDB1Query (GROUPID1/2 ?)
    end;
  finally
    Release;
  end;
end;

procedure TSchemaEditor.GroupEditActionExecute(Sender: TObject);
begin
  with TSchemaTargetDialog.Create(nil) do
  try
    GroupId.ReadOnly:= True;
    GroupId.TabStop:= False;
    GroupId.ParentColor:= True;
    fChangedFlag:= EditRecord(SchemaDB2Query) or fChangedFlag;
  finally
    Release;
  end;
end;

resourcestring
  sNotAvailableForHistory = 'Not available for record history';
procedure TSchemaEditor.Group1InsertActionExecute(Sender: TObject);
begin
  with TSchemaTargetDialog.Create(nil) do
  try
    if SchemaQuery.FieldByName('SCHEMATYPE').asInteger = schtRecordHistory then
      raise Exception.Create(sNotAvailableForHistory);
    fChangedFlag:= InsertRecord(SchemaDB1Query, SchemaQuery) or fChangedFlag;
  finally
    Release;
  end;
end;

procedure TSchemaEditor.AboutActionExecute(Sender: TObject);
{$IFDEF LINUX}
resourcestring
  sAbout = 'About';
  sVersion = 'IBReplication Manager for Linux x86'#13#10'Copyright (c) 2004 by MandySoft'#13#10'Version %s(Build %s)';
{$ENDIF}
begin
  {$IFDEF LINUX}
  MessageDlg(sAbout, Format(sVersion, [ProductReleaseMajor, ProductReleaseMinor]), mtInformation, [mbOk], 0);
  {$ELSE}
  with TAboutDlg.Create(nil) do
  try
    ShowModal;
  finally
    Release;
  end;
  {$ENDIF}
end;

procedure TSchemaEditor.StatusLineOnHint(Sender: TObject);
var
  S: string;
begin
  S:= GetLongHint(Application.Hint);
  if S = '' then
  begin
    S:= fIncSearch;
    if (S = '') and IBReplDataModule.IBReplicator.ConfigDatabase.Connected then
      S:= IBReplDataModule.IBReplicator.ConfigDatabase.DatabaseName;
  end;
  StatusBar.Panels[0].Text:= S;
end;

procedure TSchemaEditor.ReadFromIni;
begin
  DBLog.MaxLines:= Ini.ReadInteger(DBIniSection+DBIniSection2, 'MaxLogLines', DBLog.MaxLines);
  fKeyLength:= Ini.ReadInteger(DBIniSection+DBIniSection2, 'KeyLength', 255);
end;

procedure TSchemaEditor.IBCfgDBAfterDisconnect(Sender: TObject);
begin
  inherited;
  {$IFDEF LINUX}
  if csDestroying in ComponentState then
    Exit;
  {$ENDIF}
  IBReplDataModule.IBCfgDBAfterDisconnect(Sender);
  StatusLineOnHint(nil);
  PageControl1.ActivePageIndex:= 0;
  CreateDatabaseAction.Enabled:= True;
  CommitAction.Enabled:= False;
  RollbackAction.Enabled:= False;
  CloseDatabaseAction.Enabled:= False;
  OpenDatabaseAction.Enabled:= True;
  ConfigEnvironmentAction.Enabled:= False;
  BackupDatabaseAction.Enabled:= False;
  RestoreDatabaseAction.Enabled:= True;
end;

procedure TSchemaEditor.IBCfgDBBeforeConnect(Sender: TObject);
begin
  inherited;
  IBReplDataModule.IBCfgDBBeforeConnect(Sender);
end;

procedure TSchemaEditor.OpenDatabaseActionExecute(Sender: TObject);
begin
  IBTransaction.DefaultDatabase.Open;
  StatusLineOnHint(nil);
  IBTransaction.Active:= True;
  CommitAction.Enabled:= True;
  RollbackAction.Enabled:= True;
  CreateDatabaseAction.Enabled:= False;
  CloseDatabaseAction.Enabled:= True;
  OpenDatabaseAction.Enabled:= False;
  ConfigEnvironmentAction.Enabled:= True;
  BackupDatabaseAction.Enabled:= True;
  RestoreDatabaseAction.Enabled:= False;

  if PageControl1.ActivePageIndex = 1 then
  begin
    DatabasesQuery.Open;
    SchemaQuery.Open;
    SchemaDB1Query.Open;
    SchemaDB2Query.Open;
    RelationsQuery.Open;
    FieldsQuery.Open;
  end;

  if PageControl1.ActivePageIndex = 2 then
  begin
    StatSchemaQuery.Open;
    StatSchemaDB1Query.Open;
    StatRelationsQuery.Open;
  end;
end;

procedure TSchemaEditor.CloseDatabaseActionExecute(Sender: TObject);
begin
  CheckSave;
  IBTransaction.DefaultDatabase.Close;
end;

procedure TSchemaEditor.ConfigActionExecute(Sender: TObject);
resourcestring
  sDatabaseCfgRefresh = 'Changed DB settings takes effect after database reopenning';
begin
  with TManSettingsDialog.Create(nil) do
  try
    if ShowModal = mrOk then
    begin
      ReadFromIni;
      if IBReplDataModule.IBReplicator.ConfigDatabase.Connected then
        MessageDlg(sDatabaseCfgRefresh, mtInformation, [mbOk], 0);
      IBReplDataModule.ReadFromIni(False, False);
    end;
  finally
    Release;
  end;
end;

procedure TSchemaEditor.CreateDatabaseActionExecute(Sender: TObject);
begin
  Inc(IBReplDataModule.UpgradeFlag);
  try
    IBReplDataModule.IBReplicator.CreateConfigDatabase(True);
  finally
    Dec(IBReplDataModule.UpgradeFlag);
  end;
  OpenDatabaseActionExecute(nil);
end;

procedure TSchemaEditor.PageControl1Change(Sender: TObject);
begin
  if PageControl1.ActivePageIndex > 0 then
    OpenDatabaseActionExecute(nil);  // OpenDatabaseAction.Execute;   disabled
  Databases_DBGridEnter(ActiveControl);
end;

procedure TSchemaEditor.StatClearRelationActionExecute(Sender: TObject);
begin
  with StatRelationsQuery do
  begin
    IBReplDataModule.IBReplicator.ClearRelationStatistics(FieldByName('SCHEMAID').asInteger, FieldByName('GROUPID').asInteger, FieldByName('RELATIONID').asInteger);
    Close;
    Open;
  end;
end;

procedure TSchemaEditor.StatClearGroupActionExecute(Sender: TObject);
begin
  with StatSchemaDB1Query do
  begin
    IBReplDataModule.IBReplicator.ClearRelationStatistics(FieldByName('SCHEMAID').asInteger, FieldByName('GROUPID').asInteger, 0);
    Close;
    Open;
  end;
end;

procedure TSchemaEditor.StatClearSchemaActionExecute(Sender: TObject);
begin
  with StatSchemaQuery do
  begin
    IBReplDataModule.IBReplicator.ClearSchemaStatistics(FieldByName('SCHEMAID').asInteger);
    IBReplDataModule.IBReplicator.ClearRelationStatistics(FieldByName('SCHEMAID').asInteger, 0, 0);
    Close;
    Open;
  end;
end;

procedure TSchemaEditor.ClearLogActionExecute(Sender: TObject);
begin
  DBLog.Lines.Clear;
end;

procedure TSchemaEditor.DataSource1DataChange(Sender: TObject;
  Field: TField);
begin
  Databases_DBGridEnter(ActiveControl);
end;

procedure TSchemaEditor.HelpContentsActionExecute(Sender: TObject);
begin
{$IFNDEF LINUX}
  Application.HelpCommand(HELP_CONTENTS, 0);
{$ENDIF}
end;

procedure TSchemaEditor.FormDestroy(Sender: TObject);
begin
{$IFNDEF LINUX}
  Application.HelpCommand(HELP_QUIT,0);
{$ENDIF}
end;

procedure TSchemaEditor.DataSource2DataChange(Sender: TObject;
  Field: TField);
var
  F: Boolean;
begin
  DataSource1DataChange(Sender, Field);
  F:= SchemaQuery.FieldByName('SCHEMATYPE').asInteger <> schtRecordHistory;
  SchemaDB2_DBGrid1.Enabled:= F;
  RelationsQuery.FieldByName('TARGETNAME').Visible:= F;
  RelationsQuery.FieldByName('SYNCACTIONS').Visible:= F;
  RelationsQuery.FieldByName('TARGETTYPE').Visible:= F;
  FieldsQuery.FieldByName('TARGETNAME').Visible:= F;
  FieldsQuery.FieldByName('OPTIONS').Visible:= F;
//  SchemaDB1_DBGrid1.Enabled:= SchemaDB2_DBGrid1.Enabled;
end;

procedure TSchemaEditor.UpgradeDatabaseActionExecute(Sender: TObject);
begin
  Inc(IBReplDataModule.UpgradeFlag);
  try
    IBReplDataModule.IBReplicator.UpgradeConfigDatabase;
  finally
    Dec(IBReplDataModule.UpgradeFlag);
  end;
  OpenDatabaseActionExecute(nil);
end;

procedure TSchemaEditor.Group2CreateStoredProcActionExecute(
  Sender: TObject);
begin
  if SchemaDB2Query.FieldByName('DBTYPE').asInteger <> dbtInterbase then
    raise Exception.Create(sNotAvailableForTextFile);
  CheckSave;
  IBReplDataModule.IBReplicator.CreateStoredProcedureTemplates(SchemaDB2Query.FieldByName('SCHEMAID').asInteger, SchemaDB2Query.FieldByName('GROUPID').asInteger, SchemaDB2Query.FieldByName('DBID').asInteger);
end;

procedure TSchemaEditor.DatabasesUpgradeActionExecute(Sender: TObject);
begin
  CheckSave;
  IBReplDataModule.IBReplicator.UpgradeDatabase(DatabasesQuery.FieldByName('DBID').asInteger);
end;

procedure TSchemaEditor.SchemaQueryCalcFields(DataSet: TDataSet);
begin
  with DataSet do
  begin
    if FieldByName('SCHEMATYPE').asInteger = schtReplication then
      begin   // for master/detail
        FieldByName('GROUPID1').asInteger:= 1;
        FieldByName('GROUPID2').asInteger:= MaxInt;
      end
    else
      begin
        FieldByName('GROUPID1').asInteger:= 0;
        FieldByName('GROUPID2').asInteger:= 0;
      end;
  end;
end;

procedure TSchemaEditor.ConfigEnvironmentActionExecute(Sender: TObject);
resourcestring
  sCtxConfigDB = 'Config DB';
begin
  CheckSave;
  if SetEnvironment(IBReplDataModule.IBReplicator.ConfigDatabase, IBReplDataModule.IBReplicator.ConfigDatabasePrefix, sCtxConfigDB) then
    fChangedFlag:= True;
end;

procedure TSchemaEditor.DatabasesEnvironmentActionExecute(Sender: TObject);
var
  DB: TIBDatabase;
  DBProps: TReplDatabaseProperties;
resourcestring
  sCtxReplDB = 'DB "%s"';
begin
  if DatabasesQuery.FieldByName('DBTYPE').asInteger <> dbtInterbase then
    raise Exception.Create(sNotAvailableForTextFile);
  DB:= TIBDatabase.Create(nil);
  try
    IBReplDataModule.IBReplicator.SetDBParams(DB, DatabasesQuery.FieldByName('DBID').asInteger, DBProps);
    SetEnvironment(DB, DBProps.ObjPrefix, Format(sCtxReplDB, [DatabasesQuery.FieldByName('NAME').asString]));
  finally
    DB.Free;
  end;
end;

function TSchemaEditor.SetEnvironment(aDB: TIBDatabase; const aObjPrefix,
  aCtx: string): Boolean;
begin
  with TEnvironmentDialog.Create(nil) do
  try
    Context:= aCtx;
    IBReplDataModule.IBReplicator.DBEnvironmentReadValues(aDB, aObjPrefix, Keys.Lines);
    Result:= ShowModal = mrOK;
    if Result then
    begin
      IBReplDataModule.IBReplicator.DBEnvironmentWriteValues(aDB, aObjPrefix, Keys.Lines);
    end;
  finally
    Free;
  end;
end;

procedure TSchemaEditor.FieldsQueryOPTIONSGetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
const
  YN: array[Boolean] of Char = ('N','Y');
begin
  if not Sender.IsNull then
    Text:= YN[Sender.asInteger and fldoptDoNotUpdate <> 0];
end;

procedure TSchemaEditor.DatabasesCreateObjectsActionExecute(
  Sender: TObject);
begin
  CheckSave;
  IBReplDataModule.IBReplicator.CreateSystemObjects(DatabasesQuery.FieldByName('DBID').asInteger, fKeyLength);
end;

procedure TSchemaEditor.DatabasesDropObjectsActionExecute(Sender: TObject);
resourcestring
  sConfirmDelete = 'Do you want delete system objects of all defined schemas? All replication data will be lost.';
begin
  if MessageDlg(sConfirmDelete, mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
    Exit;
  CheckSave;
  IBReplDataModule.IBReplicator.DropSystemObjects(DatabasesQuery.FieldByName('DBID').asInteger, False);
end;

procedure TSchemaEditor.DatabasesDropTriggersActionExecute(
  Sender: TObject);
begin
  if DatabasesQuery.FieldByName('DBTYPE').asInteger <> dbtInterbase then
    raise Exception.Create(sNotAvailableForTextFile);
  CheckSave;
  IBReplDataModule.IBReplicator.DropSystemObjects(DatabasesQuery.FieldByName('DBID').asInteger, True);
end;

procedure TSchemaEditor.DatabasesCreateServerObjectsActionExecute(
  Sender: TObject);
begin
  CheckSave;
  IBReplDataModule.IBReplicator.CreateServerObjects(DatabasesQuery.FieldByName('DBID').asInteger);
end;

procedure TSchemaEditor.DatabasesDropServerObjectsActionExecute(
  Sender: TObject);
begin
  CheckSave;
  IBReplDataModule.IBReplicator.DropServerObjects(DatabasesQuery.FieldByName('DBID').asInteger);
end;

procedure TSchemaEditor.Relations_DBGrid3DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  Idx: Integer;
begin
  Idx:= -1;
  if (Column.Field <> nil) and not Column.Field.IsNull then
  begin
    if Column.Field.FieldName = 'DISABLED' then
    begin
      Idx:= 6+Byte(Column.Field.AsString <> 'Y');
    end;
    if Column.Field.FieldName = 'TARGETTYPE' then
    begin
      Idx:= 8+Byte(Column.Field.AsString <> 'T');
    end;
  end;
  if (Column.Field <> nil) then
  begin
    if Column.Field.FieldName = 'SYNCACTIONS' then
    begin
      if not Column.Field.DataSet.IsEmpty then
        Idx:= 4+Byte(Trim(Column.Field.AsString) <> '')
      else
        Idx:= 16; // clear [Memo]
    end;
  end;

  if Idx >= 0 then
    DrawCellBmp(Sender as TDBGrid, Rect, Idx);
end;

procedure TSchemaEditor.DrawCellBmp(Sender: TDBGrid; const Rect: TRect;
  Idx: Integer);
begin
  ImageList2.Draw(Sender.Canvas, Rect.Left, Rect.Top, Idx);
//  Sender.Canvas.StretchDraw(Rect,
end;

procedure TSchemaEditor.Fields_DBGrid4DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  Idx: Integer;
begin
  Idx:= -1;
  if (Column.Field <> nil) and not Column.Field.IsNull then
  begin
    if (Column.Field.FieldName = 'FIELDTYPE') and (Column.Field.AsInteger in [1..3]) then
    begin
      Idx:= 14-1+Column.Field.AsInteger;
    end;
    if Column.Field.FieldName = 'OPTIONS' then
    begin
      if Column.Field.AsInteger and (fldoptDoNotInsert or fldoptDoNotUpdate) = (fldoptDoNotInsert or fldoptDoNotUpdate) then
        Idx:= 0
      else if (Column.Field.AsInteger and fldoptDoNotInsert <> 0) or ((Column.Field.AsInteger and fldoptDoNotUpdate <> 0)) then
        Idx:= 2
      else if Column.Field.DataSet.FieldByName('FIELDTYPE').asInteger = 1 then
        Idx:= 1
      else if Column.Field.AsInteger <> 0 then
        Idx:= 2
      else
        Idx:= 1;
    end;
  end;
  if Idx >= 0 then
    DrawCellBmp(Sender as TDBGrid, Rect, Idx);
end;

procedure TSchemaEditor.Databases_DBGridDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if (Column.Field <> nil) and not Column.Field.IsNull then
  begin
    if (Column.Field.FieldName = 'DBTYPE') and (Column.Field.AsInteger in [0,1]) then
    begin
      DrawCellBmp(Sender as TDBGrid, Rect, 10+Column.Field.AsInteger);
    end;
  end;
end;

procedure TSchemaEditor.Schema_DBGrid2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  Idx: Integer;
begin
  Idx:= -1;
  if (Column.Field <> nil) and not Column.Field.IsNull then
  begin
    if (Column.Field.FieldName = 'SCHEMATYPE') and (Column.Field.AsInteger in [0,1]) then
    begin
      Idx:= 12+Column.Field.AsInteger;
    end;
    if Column.Field.FieldName = 'DISABLED' then
    begin
      Idx:= 0+Byte(Column.Field.AsString <> 'Y');
    end;
  end;
  if Idx >= 0 then
    DrawCellBmp(Sender as TDBGrid, Rect, Idx);
end;

procedure TSchemaEditor.SchemaDB2_DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if (Column.Field <> nil) and not Column.Field.IsNull then
  begin
    if Column.Field.FieldName = 'DISABLED' then
    begin
      DrawCellBmp(Sender as TDBGrid, Rect, 0+Byte(Column.Field.AsString <> 'Y'));
    end;
    if (Column.Field.FieldName = 'DBTYPE') and (Column.Field.AsInteger in [0,1]) then
    begin
      DrawCellBmp(Sender as TDBGrid, Rect, 10+Column.Field.AsInteger);
    end;
  end;
end;

procedure TSchemaEditor.DatabasesBackupActionExecute(Sender: TObject);
begin
  CheckSave;
  if BackupSaveDialog.Execute then
  begin
    IBReplDataModule.IBReplicator.BackupDatabase(DatabasesQuery.FieldByName('DBID').asInteger, BackupSaveDialog.FileName);
    BackupOpenDialog.FileName:= BackupSaveDialog.FileName;
  end;
end;

procedure TSchemaEditor.DatabasesRestoreActionExecute(Sender: TObject);
resourcestring
  sRestoreWarning = 'This operation overwrites database "%s"!'#13#10'All data will be lost.';
begin
  if DatabasesQuery.FieldByName('DBTYPE').asInteger <> dbtInterbase then
    raise Exception.Create(sNotAvailableForTextFile);
  CheckSave;
  if BackupOpenDialog.Execute then
  begin
    if MessageDlg(Format(sRestoreWarning, [DatabasesQuery.FieldByName('NAME').asString]), mtWarning, [mbOk, mbCancel], 0) = mrOk then
      IBReplDataModule.IBReplicator.RestoreDatabase(DatabasesQuery.FieldByName('DBID').asInteger, BackupOpenDialog.FileName);
  end;
end;

resourcestring
  sConfigDatabase = 'Configuration database';
procedure TSchemaEditor.BackupDatabaseActionExecute(Sender: TObject);
begin
  CheckSave;
  if BackupSaveDialog.Execute then
  begin
    IBReplDataModule.IBReplicator.BackupDatabase(IBReplDataModule.IBReplicator.ConfigDatabase, sConfigDatabase, BackupSaveDialog.FileName);
    BackupOpenDialog.FileName:= BackupSaveDialog.FileName;
  end;
end;

procedure TSchemaEditor.RestoreDatabaseActionExecute(Sender: TObject);
resourcestring
  sRestoreWarning = 'This operation overwrites configuration database!'#13#10'All data will be lost.';
begin
  if BackupOpenDialog.Execute then
  begin
    if MessageDlg(Format(sRestoreWarning, [DatabasesQuery.FieldByName('NAME').asString]), mtWarning, [mbOk, mbCancel], 0) = mrOk then
    begin
      IBReplDataModule.IBReplicator.ConfigDatabase.BeforeConnect(IBReplDataModule.IBReplicator.ConfigDatabase);
      IBReplDataModule.IBReplicator.RestoreDatabase(IBReplDataModule.IBReplicator.ConfigDatabase, sConfigDatabase, BackupOpenDialog.FileName);
    end;
  end;
end;

resourcestring
  sCloneSource = 'Source database "%s" is to be cloned to target database "%s".'#13#10'It overwrites current target database and its data will be lost!';
procedure TSchemaEditor.Group2CloneSourceDatabaseActionExecute(
  Sender: TObject);
begin
  if SchemaDB2Query.FieldByName('DBTYPE').asInteger <> dbtInterbase then
    raise Exception.Create(sNotAvailableForTextFile);
  CheckSave;
  if MessageDlg(Format(sCloneSource, [SchemaQuery.FieldByName('NAME').asString, SchemaDB2Query.FieldByName('NAME').asString]), mtWarning, [mbOk, mbCancel], 0) <> mrOk then
    Abort;
  IBReplDataModule.IBReplicator.CloneSourceDatabase(SchemaDB2Query.FieldByName('SCHEMAID').asInteger, SchemaDB2Query.FieldByName('GROUPID').asInteger, SchemaDB2Query.FieldByName('DBID').asInteger, False);
end;

procedure TSchemaEditor.Group2CloneEmptySourceDatabaseActionExecute(
  Sender: TObject);
begin
  if SchemaDB2Query.FieldByName('DBTYPE').asInteger <> dbtInterbase then
    raise Exception.Create(sNotAvailableForTextFile);
  CheckSave;
  if MessageDlg(Format(sCloneSource, [SchemaQuery.FieldByName('NAME').asString, SchemaDB2Query.FieldByName('NAME').asString]), mtWarning, [mbOk, mbCancel], 0) <> mrOk then
    Abort;
  IBReplDataModule.IBReplicator.CloneSourceDatabase(SchemaDB2Query.FieldByName('SCHEMAID').asInteger, SchemaDB2Query.FieldByName('GROUPID').asInteger, SchemaDB2Query.FieldByName('DBID').asInteger, True);
end;

resourcestring
  sCleanData = 'All replication records concerning to target database(s) will be deleteted!';
procedure TSchemaEditor.Group2CleanDataExecute(Sender: TObject);
begin
  CheckSave;
  if MessageDlg(sCleanData, mtWarning, [mbOk, mbCancel], 0) <> mrOk then
    Abort;
  IBReplDataModule.IBReplicator.DeleteSourceSystemData(SchemaDB2Query.FieldByName('SCHEMAID').asInteger, SchemaDB2Query.FieldByName('GROUPID').asInteger, SchemaDB2Query.FieldByName('DBID').asInteger);
end;

procedure TSchemaEditor.GroupCleanDataExecute(Sender: TObject);
resourcestring
  sCleanDataVer = 'All record history data will be deleteted!';
begin
  CheckSave;
  if SchemaQuery.FieldByName('SCHEMATYPE').asInteger = schtRecordHistory then
    begin
      if MessageDlg(sCleanDataVer, mtWarning, [mbOk, mbCancel], 0) <> mrOk then
        Abort;
    end
  else
    begin
      if MessageDlg(sCleanData, mtWarning, [mbOk, mbCancel], 0) <> mrOk then
        Abort;
    end;
  IBReplDataModule.IBReplicator.DeleteSourceSystemData(SchemaDB1Query.FieldByName('SCHEMAID').asInteger, SchemaDB1Query.FieldByName('GROUPID').asInteger, 0);
end;

procedure TSchemaEditor.RegisterActionExecute(Sender: TObject);
{$IFDEF REGISTRATION}
var
  ProF: Integer;
  I: Integer;
{$ENDIF}
begin
  {$IFDEF REGISTRATION}
  try
    _CheckRegistration(True);
    ProF:= TReg_CheckPro('');
  except
    ProF:= 0;
  end;
  I:= Pos(sStandard, Caption);
  if I = 0 then
    I:= Pos(sProfessional, Caption);
  if I > 0 then
    Caption:= Copy(Caption, 1, I-1);
  if ProF > 0 then
    begin
      Caption:= Caption+sProfessional;
      if ProF < MaxInt then
        Caption:= Caption+Format(sProfessional2, [ProF]);
    end
  else
    Caption:= Caption+sStandard+sStandard2;
  {$ENDIF}
end;             

procedure TSchemaEditor.HomePageActionExecute(Sender: TObject);
begin
{$IFNDEF LINUX}
  CheckShellExecute(ShellExecute(0, 'open', PChar(sHomePage), '', '', SW_SHOWNORMAL));
{$ENDIF}
end;

procedure TSchemaEditor.SupportActionExecute(Sender: TObject);
begin
{$IFNDEF LINUX}
  CheckShellExecute(ShellExecute(0, 'open', PChar(sSupport), '', '', SW_SHOWNORMAL));
{$ENDIF}
end;

procedure TSchemaEditor.FormShortCut;
var
  PM: TPopupMenu;
  I: Integer;
begin
// there is some problem in MainForm when resolving shortcuts used in more items, does not metter if disabled
  for I:= 0 to Menu.Items.Count-1 do
  begin
    PM:= TPopupMenu.Create(nil);
    try
      CopyMenuItems(Menu.Items[I], PM);
      if PM.IsShortCut({$IFDEF LINUX}Key, Shift, PM.Items[I].Caption{$ELSE}Msg{$ENDIF}) then
      begin
        {$IFDEF LINUX}
        PM.Items[I].Click;
        {$ELSE}
        PM.ProcessMenuChar(TWMMenuChar(Msg));
        {$ENDIF}
        Handled:= True;
      end;
    finally
      PM.Free;
    end;
  end;
end;

procedure TSchemaEditor.DatabasesSortActionExecute(Sender: TObject);
begin
  SetSort(DatabasesQuery);
end;

procedure TSchemaEditor.SchemaSortActionExecute(Sender: TObject);
begin
  SetSort(SchemaQuery);
end;

procedure TSchemaEditor.Group2SortActionExecute(Sender: TObject);
begin
  SetSort(SchemaDB2Query);
end;

procedure TSchemaEditor.RelationsSortActionExecute(Sender: TObject);
begin
  SetSort(RelationsQuery);
end;

procedure TSchemaEditor.FieldsSortActionExecute(Sender: TObject);
begin
  SetSort(FieldsQuery);
end;

procedure TSchemaEditor.StatSortSchemaActionExecute(Sender: TObject);
begin
  SetSort(StatSchemaQuery);
end;

procedure TSchemaEditor.StatSortRelationActionExecute(Sender: TObject);
begin
  SetSort(StatRelationsQuery);
end;

procedure TSchemaEditor.SetSort(aDataSet: TDataSet);
var
  D: TSortDlg;
begin
  D:= TSortDlg.Create(nil);
  try
    D.AssignFields(CustomSQLSort[aDataSet.Tag].Captions);
    D.Sorting:= Ini.ReadString(DBIniSection+DBIniSection2, Format('Sort.%d', [aDataSet.Tag]), '*0');
    if D.ShowModal = mrOK then
    begin
      Ini.WriteString(DBIniSection+DBIniSection2, Format('Sort.%d', [aDataSet.Tag]), D.Sorting);
      aDataSet.Close;
      aDataSet.Open;
    end;
  finally
    D.Free;
  end;
end;

procedure TSchemaEditor.Fields_DBGridKeyPress(Sender: TObject;
  var Key: Char);
var
  F: Boolean;
begin
  if (Key >= ' ') and ((Sender as TDBGrid).SelectedField <> nil) then
    begin
      with Sender as TDBGrid, DataSource.DataSet as TIBDataSet do
        F:= Locate(SelectedField.FieldName, fIncSearch+Key, [loCaseInsensitive, loPartialKey]);
      if F then
        fIncSearch:= fIncSearch+Key;
      IncSearchTimer.Enabled:= False;
      IncSearchTimer.Enabled:= True;  // restart timer
      StatusLineOnHint(nil);
      Key:= #0;
    end
  else if (fIncSearch <> '') and (Key in [#8]) then  // backspace
    begin
      Delete(fIncSearch, Length(fIncSearch), 1);
      if fIncSearch = '' then
        IncSearchTimerTimer(nil)
      else
        begin
          IncSearchTimer.Enabled:= False;
          IncSearchTimer.Enabled:= True;  // restart timer
          StatusLineOnHint(nil);
        end;
      Key:= #0;
    end
  else if (fIncSearch <> '') and (Key in [#27,#13]) then
    begin
      IncSearchTimerTimer(nil);  // expire
      Key:= #0;
    end;
  TDrawGrid(Sender).EditorMode:= False;  // do not show editor
end;

procedure TSchemaEditor.IncSearchTimerTimer(Sender: TObject);
begin
  fIncSearch:= '';
  StatusLineOnHint(nil);
  IncSearchTimer.Enabled:= False;
end;

procedure TSchemaEditor.Databases_DBGridKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin     // catch it here because TLabel hotkey eat character and OnKeyPress is not called
  if ((Shift = []) or (Shift = [ssShift])) and (Key in [Ord('A')..Ord('Z')]) then
  begin
    TDrawGrid(Sender).Options:= TDrawGrid(Sender).Options + [goEditing];  // TLabel eats hotkeys and OnKeyPress is not called TCustomGrid.WMGetDlgCode returns DLGC_WANTCHARS;
  end;
end;

procedure TSchemaEditor.Databases_DBGridExit(Sender: TObject);
begin
  IncSearchTimerTimer(nil)  // cancel inc search
end;

procedure TSchemaEditor.Group1ValidateActionExecute(Sender: TObject);
var
  SaveFlags: TLogFormatFlags;
begin
  with IBReplDataModule.IBReplicator.DBLog as TStreamLogger do
  begin
    SaveFlags:= LogFlags;
    LogFlags:= LogFlags - [lfStamp];
  end;
  try
    IBReplDataModule.IBReplicator.ValidateSchema(SchemaDB1Query.FieldByName('SCHEMAID').asInteger, SchemaDB1Query.FieldByName('GROUPID').asInteger);
  finally
    with IBReplDataModule.IBReplicator.DBLog as TStreamLogger do
      LogFlags:= SaveFlags;
  end;
end;

procedure TSchemaEditor.Group2ValidateActionExecute(Sender: TObject);
var
  SaveFlags: TLogFormatFlags;
begin
  with IBReplDataModule.IBReplicator.DBLog as TStreamLogger do
  begin
    SaveFlags:= LogFlags;
    LogFlags:= LogFlags - [lfStamp];
  end;
  try
    IBReplDataModule.IBReplicator.ValidateSchema(SchemaDB1Query.FieldByName('SCHEMAID').asInteger, SchemaDB1Query.FieldByName('GROUPID').asInteger, SchemaDB2Query.FieldByName('DBID').asInteger);
  finally
    with IBReplDataModule.IBReplicator.DBLog as TStreamLogger do
      LogFlags:= SaveFlags;
  end;
end;

end.

