(* IB Replication Suite
 * Copyright (C) 2002,2003 by Tomas Mandys-MandySoft
 *)

unit dg_SchemaSource;

interface

uses
  {$IFDEF LINUX}
  Libc, QGraphics, QForms, QControls, QStdCtrls, QButtons, QExtCtrls, QMask, QDBCtrls,
  {$ELSE}
  Windows, Messages, Graphics, Forms, Controls, StdCtrls, Buttons, ExtCtrls, Mask, DBCtrls, DBLookup,
  {$ENDIF}
  SysUtils, Classes, dg_SchemaTemplate, Db, IBCustomDataSet, IBQuery, IBSQL;

type
  TSchemaSourceDialog = class(TSchemaTemplateDialog)
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label5: TLabel;
    Label6: TLabel;                                               
    Label7: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    DBMemo2: TDBMemo;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    IBQuery1: TIBQuery;
    DataSource2: TDataSource;
    DBCheckBox3: TDBCheckBox;
    Label2: TLabel;
    DBLookupComboBox1: TDBLookupComboBox;
    Button2: TButton;
    procedure TableNewRecord(DataSet: TDataSet);
    procedure TableBeforeOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure IBQuery1BeforeOpen(DataSet: TDataSet);
    procedure TableAfterOpen(DataSet: TDataSet);
    procedure DataSource1DataChange(Sender: TObject; Field: TField);
    procedure Button2Click(Sender: TObject);
    procedure DBLookupComboBox1DropDown(Sender: TObject);
  private
    ScrFlag: Byte;
    procedure OnSetTextPassword(Sender: TField; const Text: String);
    procedure OnValidateTableDBID(Sender: TField);
  public
    SchemaType: Integer;
  end;

var
  SchemaSourceDialog: TSchemaSourceDialog;

implementation

uses dm_IBRepl, IBReplicator{$IFNDEF VER130}, Variants{$ENDIF};

{$IFDEF LINUX}
{$R *.xfm}
{$ELSE}
{$R *.dfm}
{$ENDIF}

procedure TSchemaSourceDialog.TableNewRecord(DataSet: TDataSet);
begin
  inherited;
  with DataSet do
  begin
    FieldByName('DBID').Value:= Null;
    FieldByName('REPLUSER').asString:= 'REPL';
    FieldByName('SEPARATOR').asInteger:= 5;
    FieldByName('DISABLED').asString:= 'N';
    FieldByName('GROUPID').asInteger:= 0;
    FieldByName('DBMASK').asInteger:= 0;
  end;
end;

procedure TSchemaSourceDialog.TableBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  IBQuery1.Transaction:= Table.Transaction;
  IBQuery1.Open;
end;

procedure TSchemaSourceDialog.FormCreate(Sender: TObject);
begin
  inherited;
  BatchReindexingForbidden:= True;
  DoNotGenCountId:= True;
end;

procedure TSchemaSourceDialog.IBQuery1BeforeOpen(DataSet: TDataSet);
begin
  inherited;
  with DataSet as TIBQuery do
    SQL[1]:= TIBReplicator.FormatIdentifier(Database.SQLDialect, IBReplDataModule.IBReplicator.ConfigDatabasePrefix+TrimRight(SQL[1]));
end;

procedure TSchemaSourceDialog.TableAfterOpen(DataSet: TDataSet);
begin
  inherited;
  DataSet.FieldByName('REPLPSW').OnSetText:= OnSetTextPassword;
  DataSet.FieldByName('DBID').OnValidate:= OnValidateTableDBID;
end;

procedure TSchemaSourceDialog.OnSetTextPassword(Sender: TField;
  const Text: String);
begin
  if (ScrFlag = 0) and TIBReplicator.IsScrambled(Text) then
    Sender.AsString:= ''
  else
    Sender.AsString:= Text;
end;

procedure TSchemaSourceDialog.DataSource1DataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  Button2.Enabled:= not TIBReplicator.IsScrambled(Table.FieldByName('REPLPSW').asString);
end;

procedure TSchemaSourceDialog.Button2Click(Sender: TObject);
begin
  Inc(ScrFlag);
  try
    Table.Edit;
    Table.FieldByName('REPLPSW').asString:= TIBReplicator.ScramblePassword(Table.FieldByName('REPLPSW').asString, True);
  finally
    Dec(ScrFlag);
  end;
end;

procedure TSchemaSourceDialog.OnValidateTableDBID(Sender: TField);
resourcestring
  sDatabaseNotInterbase = '"Database" type of source database is required';
begin
  if not Sender.IsNull and (IBQuery1.Lookup('DBID', Sender.asInteger, 'DBTYPE') <> dbtInterbase) then
    raise Exception.Create(sDatabaseNotInterbase);
end;

procedure TSchemaSourceDialog.DBLookupComboBox1DropDown(Sender: TObject);
begin
  inherited;
  with IBQuery1 do
    if Tag = 0 then
    begin
      FetchAll;
      Tag:= 1;
    end;

end;

end.

