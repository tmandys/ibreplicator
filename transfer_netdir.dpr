(* transfer_netdir - Transfer library for IB Replicator
 * Copyright (C) 2003  Tomas Mandys-MandySoft
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA  02111-1307  USA
 *)

library transfer_netdir;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  {$IFDEF LINUX}
  Libc,
  {$ELSE}
  Windows,
  FileCtrl,
  {$ENDIF}
  Classes,
  SysUtils,
  Transfer in 'Transfer.pas';

{$R *.res}

{$IFDEF LINUX}
{$ENDIF}

const
  ToolName = 'netdir';

{$DEFINE IMP_TYPES}
{$I Transfer.inc}

{$DEFINE IMP_CLASS}
{$I Transfer.inc}

{$I ToolUtils.inc}

const
  PackageExtension = '.rpg';

type
  TNetDirTransferData = class(TTransferData)
    Dir: string;
    ReceivedNames: TStringList;
    destructor Destroy; override;
  end;

destructor TNetDirTransferData.Destroy;
begin
  ReceivedNames.Free;
  inherited;
end;

resourcestring
  sNoDir = 'Shared directory is not defined';


function TransferOpen(aTarget: Boolean; Params: PChar; var H: TNetDirTransferData): Integer; stdcall;
var
  S: TStrings;
begin
  H:= TNetDirTransferData.Create;
  try
    Result:= 0;
    S:= TStringList.Create;
    try
      S.Text:= StrPas(Params);

      H.Dir:= GetParString(S, 'dropoffdir', '', Byte(aTarget));
      if H.Dir = '' then
        H.ProjError(sNoDir);
      AdjustPath(H.Dir);
    finally
      S.Free;
    end;
  except
    on E: Exception do
    begin
      Result:= errErrorS;
      H.BlockedError:= Result;
      H.LastErrorS:= E.Message;
    end;
  end;
end;

function TransferClose(H: TNetDirTransferData): Integer; stdcall;
begin
  Result:= H.CheckHandle;
  if Result <> 0 then
    Exit;
  H.ClearError;
  H.Free;
end;

function TransferSendPackage(H: TNetDirTransferData; Name: PChar; Src: TStream): Integer; stdcall;
var
  St: TStream;
begin
  Result:= H.CheckHandle;
  if Result <> 0 then
    Exit;
  H.ClearError;
  try
    ForceDirectories(H.Dir);
    St:= TFileStream.Create(H.Dir+NamePrefix+Name+PackageExtension, fmCreate);
    try
      Src.Position:= 0;
      St.CopyFrom(Src, Src.Size);
    finally
      St.Free;
    end;
  except
    on E: Exception do
    begin
      H.LastErrorS:= E.Message;
      Result:= errErrorS;
    end;
  end;
end;

function TransferReceivePackageNames(H: TNetDirTransferData; NameMask: PChar; var N: Integer): Integer; stdcall;
var
  F: TSearchRec;
  Err: Integer;
  S: string;
begin
  Result:= H.CheckHandle;
  if Result <> 0 then
    Exit;
  H.ClearError;
  try
    ForceDirectories(H.Dir);
    if H.ReceivedNames = nil then
      H.ReceivedNames:= TStringList.Create;
    H.ReceivedNames.Sorted:= True;
    H.ReceivedNames.Clear;
    Err:= FindFirst(H.Dir+NamePrefix+NameMask+'*'+PackageExtension, 0, F);
    try
      repeat
        if Err = 0 then
        begin
          if F.Attr and not faArchive = 0 then
          begin
            S:= Copy(F.Name, 1+Length(NamePrefix), Length(F.Name)-Length(PackageExtension)-Length(NamePrefix));
            if H.ReceivedNames.IndexOf(S) = -1 then
              H.ReceivedNames.Add(S);
          end;
          Err:= FindNext(F);
        end;
      until Err <> 0;
    finally
      FindClose(F);
    end;
    N:= H.ReceivedNames.Count;
  except
    on E: Exception do
    begin
      H.LastErrorS:= E.Message;
      Result:= errErrorS;
    end;
  end;
end;

function TransferGetReceivedPackageName(H: TNetDirTransferData; I: Integer; Name: PChar; BufferLength: Integer): Integer; stdcall;
begin
  Result:= H.CheckHandle;
  if Result <> 0 then
    Exit;
  H.ClearError;
  try
    if H.ReceivedNames = nil then
      H.ProjError(sNamesNotReceived);
    StrPLCopy(Name, H.ReceivedNames[I], BufferLength);
  except
    on E: Exception do
    begin
      H.LastErrorS:= E.Message;
      Result:= errErrorS;
    end;
  end;
end;

function TransferReceivePackage(H: TNetDirTransferData; Name: PChar; Tgt: TStream): Integer; stdcall;
var
  S: string;
  St: TStream;
begin
  Result:= H.CheckHandle;
  if Result <> 0 then
    Exit;
  H.ClearError;
  try
    ForceDirectories(H.Dir);
    S:= H.Dir+NamePrefix+Name+PackageExtension;
    if not FileExists(S) then
      H.PackageNotFound(Name);
    St:= TFileStream.Create(S, fmOpenRead or fmShareDenyWrite);
    try
      Tgt.CopyFrom(St, St.Size);
    finally
      St.Free;
    end;
  except
    on E: Exception do
    begin
      H.LastErrorS:= E.Message;
      Result:= errErrorS;
    end;
  end;
end;

function TransferDeletePackage(H: TNetDirTransferData; Name: PChar): Integer; stdcall;
var
  S: string;
begin
  Result:= H.CheckHandle;
  if Result <> 0 then
    Exit;
  H.ClearError;
  try
    ForceDirectories(H.Dir);
    S:= H.Dir+NamePrefix+Name+PackageExtension;
    if FileExists(S) then
      DeleteFile(S);
  except
    on E: Exception do
    begin
      H.LastErrorS:= E.Message;
      Result:= errErrorS;
    end;
  end;
end;

function TransferError(H: TNetDirTransferData; ErrS: PChar; BufferLength: Integer): Integer; stdcall;
begin
  Result:= H.TransferError(ErrS, BufferLength);
end;


{$DEFINE EXP_PROCS}
{$I Transfer.inc}

begin
  TransferLibInit;
end.

