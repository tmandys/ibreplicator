(* enc_cmdline - Encoder library for IB Replicator
 * Copyright (C) 2003  Tomas Mandys-MandySoft
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA  02111-1307  USA
 *)

library enc_cmdline;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  {$IFDEF LINUX}
  Libc,
  {$ELSE}
  Windows,
  {$ENDIF}
  SysUtils,
  Classes,
  Magic;

{$R *.res}

{$IFDEF LINUX}
{$ENDIF}

const
  ToolName = 'cmdline';

{$DEFINE IMP_TYPES}
{$I Encoder.inc}

{$DEFINE IMP_CLASS}
{$I Encoder.inc}

{$I ToolUtils.inc}

resourcestring
  sCmdLineNotDefined = 'CmdLine parameter not defined';

procedure EncExecCmd(Encode: Boolean; SubType: string; Params: TStrings; Src, Tgt: TStream; Cmd: string);
  procedure SubstPar(aPar, aVal: string; var Cmd: string);
  var
    I: Integer;
  begin
    repeat
      I:= Pos(aPar, Cmd);
      if I > 0 then
      begin
        Delete(Cmd, I, Length(aPar));
        Insert(aVal, Cmd, I);
      end;
    until I = 0;
  end;
var
  SrcFile, DestFile: string;
  H: TOffEnvelope;
  Attr: TSecurityAttributes;
  aProcessInformation: TProcessInformation;
  aExitCode: Cardinal;
  aStartupInfo: TStartupInfo;
  TempDir, TempName, S: string;
  L: Byte;
begin
  TempDir:= GetTempDir(Params);
  if Encode then
    Src.Position:= 0
  else
    begin
      Src.Position:= SizeOf(TOffEnvelope);
      Src.ReadBuffer(L, SizeOf(L));
      SetLength(TempName, L);
      Src.ReadBuffer(TempName[1], Length(TempName));
      DestFile:= TempDir+TempName;
    end;
  SrcFile:= SaveToTempFile(SubType, Src, Params);
  try
    if Encode then
    begin
      TempName:= ExtractFileName(SrcFile);
      DestFile:= GetTempFileName(SubType, Params);
    end;
    SysUtils.DeleteFile(DestFile);  // length=0

    SubstPar('%src%', SrcFile, Cmd);
    SubstPar('%dest%', DestFile, Cmd);
    SubstPar('%name%', TempName, Cmd);
    S:= TempDir;
    SubstPar('%tmpdir2%', S, Cmd);
    if (S <> '') and (S[Length(S)] = '\') then
      SetLength(S, Length(S)-1);    // strip last slash
    SubstPar('%tmpdir%', S, Cmd);
    try

      FillChar(aStartupInfo, SizeOf(aStartupInfo), 0);
      aStartupInfo.CB:= SizeOf(aStartupInfo);
      with aStartupInfo do
      begin
        dwFlags:= startf_UseShowWindow;
        wShowWindow:= sw_Show;
      end;
      Attr.nLength:= SizeOf(Attr);
      Attr.lpSecurityDescriptor:= nil;
      Attr.bInheritHandle:= True;
      if not CreateProcess(nil, PChar(Cmd), @Attr, @Attr, True, Normal_Priority_Class, nil, nil, aStartupInfo, aProcessInformation) then
        ProjLastError('CreateProcess', Cmd);
      repeat
        if not GetExitCodeProcess(aProcessInformation.hProcess, aExitCode) then
          ProjLastError('GetExitCodeProcess', '');
      until aExitCode <> Still_Active;

      if Encode then
      begin
        FillEnvelope(H, SubType);
        Tgt.WriteBuffer(H, SizeOf(H));
        L:= Length(TempName);
        Tgt.WriteBuffer(L, SizeOf(L));
        Tgt.WriteBuffer(TempName[1], L);
      end;
      LoadFromTempFile(DestFile, Tgt);
    finally
      SysUtils.DeleteFile(DestFile);
    end;
  finally
    SysUtils.DeleteFile(SrcFile);
  end;
end;

function EncProcess(Encode: Boolean; SubType: PChar; Src, Tgt: TStream; Params: PChar; ErrS: PChar; BufferLength: Integer): Boolean; stdcall;
var
  St: TStrings;
  S, Pref: string;
begin
  Result:= False;
  try
    CheckInstance(Src); CheckInstance(Tgt);
    St:= TStringList.Create;
    try
      St.Text:= StrPas(Params);
      if (SubType <> nil) and (SubType <> '') then
        Pref:= '_'+StrPas(SubType);
      S:= St.Values[ToolName+Pref+'.cmd.'+IntToStr(Byte(Encode))];
      if SubType = 'zip' then
        begin
          if S = '' then
            if Encode then
              S:= 'pkzip "%dest%" "%src%"'
            else
              S:= 'pkunzip "%src%" "%tmpdir%"';
        end
      else if SubType = 'arj' then
        begin
          if S = '' then
            if Encode then
              S:= 'arj a "%dest%" "%src%"'
            else
              S:= 'arj e "%src%" "%tmpdir%"';
        end
      else
        ;
      if S = '' then
        ProjError(sCmdLineNotDefined);
      EncExecCmd(Encode, SubType, St, Src, Tgt, S);
    finally
      St.Free;
    end;
    Result:= True;
  except
    on E: Exception do
      StrPLCopy(ErrS, E.Message, BufferLength);
  end;
end;

{$DEFINE EXP_PROCS}
{$I Encoder.inc}

begin
end.
