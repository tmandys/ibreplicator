(* transfer - Transfer library tools for IB Replicator
 * Copyright (C) 2003  Tomas Mandys-MandySoft
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA  02111-1307  USA
 *)

// no string parameters and exception raising posiible during DLL function

{$IFDEF IMP_CLASS}
{$UNDEF IMP_CLASS}
const
  DllName = DllPrefix+ToolName;


resourcestring
  sTransferPackageNotFound = 'Transfer package "%s" not found';
var
  TransferHandleList: TList;

type
  TTransferData = class
    BlockedError: Integer;
    LastErrorS: string;
    constructor Create;
    destructor Destroy; override;
    procedure ClearError;
    function CheckHandle: Integer;
    function TransferError(ErrS: PChar; BufferLength: Integer): Integer;
    class procedure ProjError(S: string);
    class procedure PackageNotFound(S: string);
  end;

constructor TTransferData.Create;
begin
  inherited;
  TransferHandleList.Add(Pointer(Self));
end;

destructor TTransferData.Destroy;
begin
  TransferHandleList.Remove(Pointer(Self));
  inherited;
end;

procedure TTransferData.ClearError;
begin
  if Self <> nil then
    LastErrorS:= '';
end;

function TTransferData.CheckHandle: Integer;
begin
  if Self = nil then
    Result:= errNullHandle
  else
    if TransferHandleList.IndexOf(Self) = -1 then
      Result:= errBadHandle
  else
    begin
      Result:= BlockedError;
      if Result = 0 then
        LastErrorS:= '';
    end;
end;

class procedure TTransferData.ProjError(S: string);
begin
  raise Exception.Create(S);
end;

class procedure TTransferData.PackageNotFound(S: string);
begin
  ProjError(Format(sTransferPackageNotFound, [S]));
end;

function TTransferData.TransferError(ErrS: PChar; BufferLength: Integer): Integer; 
begin
  if Self = nil then
    Result:= errNullHandle
  else
    if TransferHandleList.IndexOf(Self) = -1 then
      Result:= errBadHandle
  else
    begin
      Result:= BlockedError;
      StrPLCopy(ErrS, LastErrorS, BufferLength);
      if LastErrorS <> '' then
        Result:= errErrorS;
      if BlockedError = 0 then
        ClearError;
    end;
end;

var
  SaveTransferExit: Pointer;

procedure TransferLibExit;
begin
  while TransferHandleList.Count > 0 do
  begin
    TTransferData(TransferHandleList[0]).Free;
    TransferHandleList.Delete(0);
  end;
  TransferHandleList.Free;
  ExitProc := SaveTransferExit;  // restore exit procedure chain
end;

procedure TransferLibInit;
begin
  TransferHandleList:= TList.Create;
  SaveTransferExit := ExitProc;  // save exit procedure chain
  ExitProc := @TransferLibExit;  // install LibExit exit procedure
end;

{$ENDIF}

{$IFDEF IMP_TYPES}
{$UNDEF IMP_TYPES}
type
  TTransferHandle = Pointer; // = TTransferData;

const
  NamePrefix = 'ibreplicatordatapackage_';

  DllPrefix = 'transfer_';
  DllVer = {$IFNDEF LINUX}
              {$IFDEF VER130}''{$ELSE} // D5, backward compatibility
              {$IFDEF VER140}'.140'{$ELSE} // D6
              {$IFDEF VER150}'.150'{$ELSE} // D7
              {$IFDEF VER160}'.160'{$ELSE} // D8
              {$IFDEF VER170}'.170'{$ELSE} // D2005
              {$IFDEF VER180}'.180'{$ELSE} // D2006
              ''
              {$ENDIF}{$ENDIF}{$ENDIF}{$ENDIF}{$ENDIF}{$ENDIF}
           {$ELSE}
              ''
           {$ENDIF};

  DllExt = {$IFDEF LINUX}'.so'{$ELSE}'.dll'{$ENDIF};

  errNullHandle = 1;
  errBadHandle = 2;
  errErrorS = 3;

resourcestring
  sNullTransferHandle = 'Null handle';
  sBadTransferHandle = 'Bad transfer handle';
  sNamesNotReceived = 'Package names not received';

type   { pro dynamicke volani }
  tTransferOpen = function (aTarget: Boolean; Params: PChar; var H: TTransferHandle): Integer; stdcall;
  tTransferClose = function (H: TTransferHandle): Integer; stdcall;
  tTransferSendPackage = function (H: TTransferHandle; Name: PChar; Src: TStream): Integer; stdcall;
  tTransferReceivePackageNames = function (H: TTransferHandle; NameMask: PChar; var N: Integer): Integer; stdcall;
  tTransferGetReceivedPackageName = function (H: TTransferHandle; I: Integer; Name: PChar; BufferLength: Integer): Integer; stdcall;
  tTransferReceivePackage = function (H: TTransferHandle; Name: PChar; Tgt: TStream): Integer; stdcall;
  tTransferDeletePackage = function (H: TTransferHandle; Name: PChar): Integer; stdcall;
  tTransferError = function (H: TTransferHandle; ErrS: PChar; BufferLength: Integer): Integer; stdcall;

{$ENDIF}

{$IFDEF IMP_PROCS}
{$UNDEF IMP_PROCS}
{ budou asi dynamicky loadovatelny tak je tady nanic }
function TransferOpen(aTarget: Boolean; Params: PChar; var H: TTransferHandle): Integer; stdcall;
function TransferClose(H: TTransferHandle): Integer; stdcall;
function TransferSendPackage(H: TTransferHandle; Name: PChar; Src: TStream): Integer; stdcall;
function TransferReceivePackageNames(H: TTransferHandle; NameMask: PChar; var N: Integer): Integer; stdcall;
function TransferGetReceivedPackageName(H: TTransferHandle; I: Integer; Name: PChar; BufferLength: Integer): Integer; stdcall;
function TransferReceivePackage(H: TTransferHandle; Name: PChar; Tgt: TStream): Integer; stdcall;
function TransferDeletePackage(H: TTransferHandle; Name: PChar): Integer; stdcall;
function TransferError(H: TTransferHandle; ErrS: PChar; BufferLength: Integer): Integer; stdcall;

implementation

function  TransferOpen; external DllName 4;
procedure TransferClose; external DllName 5;
function  TransferSendPackage; external DllName 6;
function  TransferReceivePackageNames; external DllName 7;
function  TransferGetReceivedPackageName; external DllName 8;
function  TransferReceivePackage; external DllName 9;
function  TransferDeletePackage; external DllName 10;
function  TransferError; external DllName 11;

{$ENDIF}

{$IFDEF EXP_PROCS}
{$UNDEF EXP_PROCS}


exports
  TransferOpen index 4,
  TransferClose index 5,
  TransferSendPackage index 6,
  TransferReceivePackageNames index 7,
  TransferGetReceivedPackageName index 8,
  TransferReceivePackage index 9,
  TransferDeletePackage index 10,
  TransferError index 11;

{$ENDIF}
