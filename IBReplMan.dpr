program IBReplMan;

uses
  {$IFDEF LINUX}
  QForms,
  {$ELSE}
  Forms,
  {$ENDIF}
  dm_IBRepl in 'dm_IBRepl.pas' {IBReplDataModule: TDataModule},
  dm_IBRepl_Man in 'dm_IBRepl_Man.pas' {IBReplDataModule2: TDataModule},
  fm_SchemaEditor in 'fm_SchemaEditor.pas' {SchemaEditor},
  dg_SchemaTemplate in 'dg_SchemaTemplate.pas' {SchemaTemplateDialog},
  dg_SchemaFields in 'dg_SchemaFields.pas' {SchemaFieldsDialog},
  dg_SchemaRelations in 'dg_SchemaRelations.pas' {SchemaRelationsDialog},
  dg_SchemaDatabases in 'dg_SchemaDatabases.pas' {SchemaDatabasesDialog},
  dg_CopyFrom in 'dg_CopyFrom.pas' {CopyFromDlg},
  dg_SchemaRelationsSyncActions in 'dg_SchemaRelationsSyncActions.pas' {SchemaRelationsSyncActionsDialog},
  dg_Schema in 'dg_Schema.pas' {SchemaDialog},
  dg_SchemaSource in 'dg_SchemaSource.pas' {SchemaSourceDialog},
  dg_SchemaTarget in 'dg_SchemaTarget.pas' {SchemaTargetDialog},
  dg_Environment in 'dg_Environment.pas' {EnvironmentDialog},
  dg_SchemaRelationsOptions in 'dg_SchemaRelationsOptions.pas' {SchemaRelationsOptionsDialog},
  dg_SchemaFieldsConflictOptions in 'dg_SchemaFieldsConflictOptions.pas' {SchemaFieldsConflictOptionsDialog},
  dg_ServerSettings in 'dg_ServerSettings.pas' {ServerSettingsDialog},
  dg_ManSettings in 'dg_ManSettings.pas' {ManSettingsDialog},
  dg_Sort in 'dg_Sort.pas' {SortDlg};

{$IFNDEF LINUX}
{$R *.res}
{$ENDIF}

begin
  Application.Initialize;
  Application.Title := 'Interbase Replication Manager';
  {$IFNDEF LINUX}
  Application.HelpFile := 'ibrepl2.hlp';
  {$ENDIF}
  DBIniSection2:= '.Manager';
  Application.CreateForm(TSchemaEditor, SchemaEditor);
  Application.Run;
end.
