program IBReplServerSvc;
{$APPTYPE CONSOLE}
uses
  ComObj,
  Classes,
  Windows,
  SysUtils,
  AuxProj,
  CmdLine,
{$IFDEF REGISTRATION}
  Registry,
  IBReplicatorDeploy,
  {$IFDEF VER120}FileCtrl, {$ENDIF}{$IFDEF VER130}FileCtrl, {$ENDIF}
{$ENDIF}
//  AclApi,   // error in library name in source, available only in >WinNT, must be dynamic library
  AccCtrl,
  Messages,
  MultiInst,
  SvcMgr,
  WinSvc,
  Math,
  Connect,
  IBReplicator,
  IBDatabase,
  SyncObjs,
  dm_IBRepl in 'dm_IBRepl.pas' {IBReplDataModule: TDataModule},
  dm_IBReplC in 'dm_IBReplC.pas' {IBReplDataModuleC: TDataModule};

{$R IBReplServer.res}

type
  TMappingRecord = record
    Terminate: Boolean;
    Pause: Boolean;
    Reset: Byte;
    ProcessId: DWORD;
    Status: array[0..4096] of Char;
  end;

var
  MappingRecord: TMappingRecord;
  FileMapping: THandle;
  ServiceStatus: TServiceStatus;
  ServiceStatusHandle: SERVICE_STATUS_HANDLE;
  SvcMachine: PChar;
  SvcName: string;
  SvcType: string;
  IsService: Boolean;
  SvcCriticalSection: TCriticalSection;

type
  TIBReplDataModule2 = class(TIBReplDataModuleC)
  private
  protected
    procedure IBCfgDBBeforeConnect(Sender: TObject);
    procedure UpdateStatus; override;
  public
    Terminate: Boolean;
    function Status: string;
    constructor Create(aOwner: TComponent); override;
    procedure ResetStatus;
    procedure ProcessFileMap;
  end;

procedure _RaiseLastWin32Error(aFunc: string);
begin
  try
    RaiseLastWin32Error;
  except
    on E: Exception do
    begin
      E.Message:= aFunc+':  '+E.Message;
      raise;
    end;
  end;
end;

{ TIBReplDataModule2 }

constructor TIBReplDataModule2.Create;
begin
  inherited;
  IBCfgDB.BeforeConnect:= IBCfgDBBeforeConnect; // no config db params override
end;

procedure TIBReplDataModule2.ResetStatus;
begin
{ dummy }
end;

procedure TIBReplDataModule2.UpdateStatus;
begin
  inherited;
  if not MultiThreading then
  begin
    try
      ProcessFileMap;
    except
    end;
  end;
end;

function TIBReplDataModule2.Status: string;
var
  IBR: TIBReplicatorTask;
  I: Integer;
  function LStr(const S: string; N: Integer): string;
  begin
    Result:= Copy(S+StringOfChar(' ', N), 1, N);
  end;
const
  OnlS: array[0..2*4+1] of string = ('ONL-R', 'ONL-L', 'OFF-S', 'OFF-S', 'OFF-T-R', 'OFF-T-L', 'ONL-T', 'ONL-T', 'SYN', 'SYN');
  DisS: array[Boolean] of string = ('', 'DIS');
  TimS: array[0..3] of string = ('', 'T', 'E', 'T+E');
  RunS: array[Boolean] of string = ('', 'RUN');
begin
  Result:= '';
  Result:= Result+Format('Name: %s'#13#10, [SvcType]);
  Result:= Result+Format('NT service: %d'#13#10, [Byte(IsService)]);
  Result:= Result+Format('Disable all: %d'#13#10, [Byte(DisableAll)]);
  Result:= Result+#13#10;

  for I:= 0 to Tasks.Count-1 do
  begin
    IBR:= Pointer(Tasks[I]);
    Result:= Result+
             Format('%.2d ', [I]) +
             LStr(IBR.TaskName, 15)+ ' '+
             LStr(DisS[IBR.Disabled], 3)+ ' '+
             LStr(OnlS[2*IBR.ReplType+Byte(IBR.ReplOptions and repoptReplicateLog<>0)], 7)+ ' '+
             LStr(TimS[Byte((IBR.ReplType in [0,1,3]) and IBR.DBEvent.Registered) shl 1 + Byte(IBR.Interval > 0 {enabled is toggled when replicatingt})], 3)+' '+
             LStr(RunS[IBR.IsRunning], 3)+ ' '+
             Format('DBId:%d/', [IBR.DBId]);
    if IBR.DBId = 0 then
      Result:= Result+'?'
    else
      Result:= Result+'"'+IBR.ReplDatabaseName+'" ';
    Result:= Result+'SchId:'+ArrayToString(IntegerArrayToStringArray(IBR.SchemaIds), ',')+' '+
                    'GrId:'+ArrayToString(IntegerArrayToStringArray(IBR.GroupIds), ',')+' '+
                    'SDbId:'+ArrayToString(IntegerArrayToStringArray(IBR.SrcDBIds), ',')+' '+
                    'TDbId:'+ArrayToString(IntegerArrayToStringArray(IBR.TgtDBIds), ',')+' '+
                    'Sec:'+IntToStr(IBR.UpdateStatusTicks div 1000)+' '+
                    'Cnt:'+IntToStr(IBR.UpdataStatusCount)+' '+
                    'Rate:';
    if IBR.UpdateStatusTicks = 0 then
      Result:= Result+'0'
    else
      Result:= Result+FloatToStrF(IBR.UpdataStatusCount / (IBR.UpdateStatusTicks / 1000 / 60), ffGeneral, 2, 0);

    if IBR.LastStarted <> 0 then
      Result:= Result+' '+DateTimeToStr(IBR.LastStarted);
    Result:= Result+#13#10;
  end;
  Result:= Result+#13#10;
end;

procedure TIBReplDataModule2.IBCfgDBBeforeConnect(Sender: TObject);
begin
  TIBReplDataModule(Self).IBCfgDBBeforeConnect(Sender); // skip config db params override
  TIBDatabase(Sender).LoginPrompt:= False;
end;

procedure TIBReplDataModule2.ProcessFileMap;
var
  MappingPtr: ^TMappingRecord;
  LastState, LastError: DWORD;
  Pause: Boolean;
begin
  // Debug('ProcessFileMap()', []);
  StrLCopy(@MappingRecord.Status, PChar(Status), SizeOf(MappingRecord.Status));

  SvcCriticalSection.Enter;
  try
    // Debug('MapViewOfFile(FILE_MAP_WRITE)', []);
    MappingPtr:= MapViewOfFile(FileMapping, FILE_MAP_WRITE, 0, 0, SizeOf(TMappingRecord));
    if MappingPtr = nil then
      _RaiseLastWin32Error('MapViewOfFile');
    try
      MappingPtr^.Status:= MappingRecord.Status;
    finally
      // Debug('UnmapViewOfFile()', []);
      UnmapViewOfFile(MappingPtr);
    end;
  finally
    SvcCriticalSection.Leave;
  end;

  // Debug('MapViewOfFile(FILE_MAP_READ)', []);
  MappingPtr:= MapViewOfFile(FileMapping, FILE_MAP_READ, 0, 0, SizeOf(TMappingRecord));
  if MappingPtr = nil then
    _RaiseLastWin32Error('MapViewOfFile');
  try
    MappingRecord.Terminate:= MappingPtr^.Terminate;
    Pause:= MappingPtr^.Pause; // multi-thread
    if not MappingPtr^.Terminate and (IBReplDataModule.Enabled{non-Pause} = Pause) then
    begin
      IBReplDataModule.Lock;
      try
        IBReplDataModule.Enabled{Pause}:= not Pause;
        try
          IBReplDataModule.StopAll;
          IBReplDataModule.ReadFromIni(False, False);
        except
          on E: Exception do
            Writeln2(E.Message);
        end;
      finally
        IBReplDataModule.Unlock;
      end;
    end;
    SvcCriticalSection.Enter;
    try
      if not MappingPtr^.Terminate and (IBReplDataModule.Enabled <> MappingPtr^.Pause) and (Pause = MappingPtr^.Pause {not pending}) then
      begin
        LastState:= ServiceStatus.dwCurrentState;
        if IBReplDataModule.Enabled then
          ServiceStatus.dwCurrentState:= SERVICE_RUNNING
        else
          ServiceStatus.dwCurrentState:= SERVICE_PAUSED;
        if (ServiceStatusHandle <> 0) and (LastState <> ServiceStatus.dwCurrentState) then
        begin
          Debug('ProcessFileMap(): SetServiceStatus(%x)', [ServiceStatus.dwCurrentState]);
          if not SetServiceStatus (ServiceStatusHandle, ServiceStatus) then
          begin
            LastError:= GetLastError;
            Writeln2(Format('SetServiceStatus(%x) error: %x, "%s"', [ServiceStatus.dwCurrentState, LastError, SysErrorMessage(LastError)]));   // do not call _RaiseLastWin32Error because is called also from console
          end;
        end;
      end;
      if MappingPtr^.Reset <> MappingRecord.Reset then
      begin
        MappingRecord.Reset:= MappingPtr^.Reset;
        ResetStatus;
      end;
    finally
      SvcCriticalSection.Leave;
    end;

  finally
    // Debug('UnmapViewOfFile()', []);
    UnmapViewOfFile(MappingPtr);
  end;
end;

procedure InstallService(aInstall: Boolean);
var
  SvcMgr, Svc: Integer;
  Path, S1, S2: string;
  P1, P2: PChar;
resourcestring
  sServiceDisplayName = 'IBReplServer service "%s"';
begin
  Debug('OpenSCManager()', []);
  SvcMgr := OpenSCManager(SvcMachine, nil, SC_MANAGER_ALL_ACCESS);
  if SvcMgr = 0 then
    _RaiseLastWin32Error('OpenSCManager');
  try
    if aInstall then
      begin
        Path := ParamStr(0);
        if SvcMachine <> nil then
          Path:= ExpandUNCFileName(Path);
        if (Pos(' ', Path) > 0) and (Path[1]<>'"') then
          Path:= '"'+Path+'"';
        Path:= Path+' '+SvcType+' /SILENT "/I:'+Ini.FileName+'"';

        S1:= '';
        GetCmdString('DBG:', clUpcase or clValueCase, S1);
        if S1 <> '' then
          Path:= Path+' "/DBG:'+S1+'"';

        S1:= '';
        GetCmdString('F:', clUpcase or clValueCase, S1);
        if S1 <> '' then
          Path:= Path+' "/F:'+S1+'"';

        S1:= '';
        GetCmdString('O:', clUpcase or clValueCase, S1);
        if S1 <> '' then
          Path:= Path+' "/O:'+S1+'"';

        S1:= '';
        GetCmdString('U:', clUpcase or clValueCase, S1);
        if S1 = '' then
          P1:= nil
        else
          P1:= PChar(S1);
        S2:= '';
        GetCmdString('P:', clUpcase or clValueCase, S2);
        if S2 = '' then
          P2:= nil
        else
          P2:= PChar(S2);
        Debug('CreateService("%s", "%s")', [SvcName, Path]);
        Svc := CreateService(SvcMgr, PChar(SvcName), PChar(Format(sServiceDisplayName, [SvcType])),
          SERVICE_ALL_ACCESS, SERVICE_WIN32_OWN_PROCESS, SERVICE_DEMAND_START, SERVICE_ERROR_NORMAL,
          PChar(Path), nil, nil, nil, P1, P2);
        if Svc = 0 then
          _RaiseLastWin32Error('CreateService');
        try
        finally
          CloseServiceHandle(Svc);
        end;
      end
    else
      begin
        Debug('OpenService("%s")', [SvcName]);
        Svc := OpenService(SvcMgr, PChar(SvcName), SERVICE_ALL_ACCESS);
        if Svc = 0 then
          _RaiseLastWin32Error('OpenService');
        try
          Debug('DeleteService()', []);
          if not DeleteService(Svc) then
            _RaiseLastWin32Error('DeleteService');
        finally
          CloseServiceHandle(Svc);
        end;
      end;
  finally
    CloseServiceHandle(SvcMgr);
  end;
end;

function CheckStartService(aStartCmd: Boolean): Boolean;   // check if starting service or console application
var
  Mgr, Svc, I: Integer;
  UserName, ServiceStartName: string;
  Config: Pointer;
  Size: DWord;
  ServiceStatus: TServiceStatus;
  PC: PChar;
begin
  try
    Debug('OpenSCManager()', []);
    Mgr := OpenSCManager(SvcMachine, nil, SC_MANAGER_ALL_ACCESS);
    if Mgr = 0 then
      _RaiseLastWin32Error('OpenSCManager()');
    try
      Debug('OpenService("%s")', [SvcName]);
      Svc := OpenService(Mgr, PChar(SvcName), SERVICE_ALL_ACCESS);
      Result := Svc <> 0;
      if Result then
      begin
        try
          // is service starting?
          Debug('QueryServiceStatus()', []);
          if not QueryServiceStatus(Svc, ServiceStatus) then
            _RaiseLastWin32Error('QueryServiceStatus()');
          Debug('ServiceStatus = %x', [ServiceStatus.dwCurrentState]);
          Result:= ServiceStatus.dwCurrentState = SERVICE_START_PENDING;
          if not Result then
          begin
            if aStartCmd then
            begin
              Debug('StartService()', []);
              if not StartService(Svc, 0, PC) then
                _RaiseLastWin32Error('QueryServiceConfig(nil)');
            end;
            Exit;
          end;
          // is currect user the same as NT service user?
          Debug('QueryServiceConfig(nil)', []);
          QueryServiceConfig(Svc, nil, 0, Size);  // raises error but Size is filled correctly
          Config := AllocMem(Size);
          try
            Debug('QueryServiceConfig(Config)', []);
            if not QueryServiceConfig(Svc, Config, Size, Size) then
              _RaiseLastWin32Error('QueryServiceConfig(nil)');
            ServiceStartName := PQueryServiceConfig(Config)^.lpServiceStartName;
            Debug('ServiceStartName = "%s"', [ServiceStartName]);
            if CompareText(ServiceStartName, 'LocalSystem') = 0 then
              ServiceStartName := 'SYSTEM'
            else // strip domain name  .\xxxx,  might not work compare when domain server is used ???
              begin
                repeat
                  I:= Pos('\', ServiceStartName);
                  if I > 0 then
                    ServiceStartName:= Copy(ServiceStartName, I+1, MaxInt);
                until I = 0;
              end;
          finally
            Dispose(Config);
          end;
        finally
          CloseServiceHandle(Svc);
        end;
      end;
    finally
      CloseServiceHandle(Mgr);
    end;
  except
    on E: Exception do
    begin
      Result:= False;
      Writeln2(E.Message);
    end;
  end;
  if Result then
  begin
    SetLength(UserName, Size);
    GetUserName(PChar(UserName), Size);
    Size := 256;
    SetLength(UserName, StrLen(PChar(UserName)));
    Debug('UserName = "%s"', [UserName]);
    Debug('ServiceUserName_ = "%s"', [ServiceStartName]);
    Result := CompareText(UserName, ServiceStartName) = 0;
  end;
end;

procedure DoService;
var
  Msg: TMsg;
  MappingPtr: ^TMappingRecord;
resourcestring
  sStarted = 'Started "%s"';
  sStopped = 'Stopped by remote';
  sTerminating = 'Terminated by itself';
begin
  Debug('DoService()', []);
  if InitProc <> nil then
    TProcedure(InitProc);   // not called in Console?, InitComObj of ComObj must be called!!!

  MainThreadID:= GetCurrentThreadID;   // set thread is which behaves as main thread for synchronization 

  DBIniSection2:= '.Server';
  Debug('IBReplDataModule.Create()', []);
  IBReplDataModule:= TIBReplDataModule2.Create(nil);
  try
    try
      MappingRecord.Pause:= IsThereCmd('PAUSE', clUpcase);
      IBReplDataModule.Enabled:= not MappingRecord.Pause;
      if not MappingRecord.Pause then
        IBReplDataModule.ReadFromIni(True, True);
      IBReplDataModule.IBReplicator.ReplLog.Log('', lchNull, Format(sStarted, [SvcType]));

      SvcCriticalSection.Enter;
      try
        Debug('MapViewOfFile(FILE_MAP_WRITE)', []);
        MappingPtr:= MapViewOfFile(FileMapping, FILE_MAP_WRITE, 0, 0, SizeOf(TMappingRecord));
        if MappingPtr = nil then
          _RaiseLastWin32Error('MapViewOfFile');
        try
          MappingPtr^:= MappingRecord;
        finally
          Debug('UnmapViewOfFile()', []);
          UnmapViewOfFile(MappingPtr);
        end;
      finally
        SvcCriticalSection.Leave;
      end;

      repeat
        // pred volanim timeru aby byl status a processid zapsany pred potencialni akci (dlouhy connect)
        TIBReplDataModule2(IBReplDataModule).ProcessFileMap;
        {$IFNDEF VER130}  // not Delphi5
        CheckSynchronize(100);
        {$ELSE}
        Sleep(100);
        {$ENDIF}

        if PeekMessage(Msg, 0, 0, 0, PM_REMOVE) then
        begin
          MappingRecord.Terminate:= MappingRecord.Terminate or (Msg.Message = WM_QUIT);
          if not MappingRecord.Terminate then
          begin
            TranslateMessage(Msg);
            DispatchMessage(Msg);
          end;
        end;
      until MappingRecord.Terminate or TIBReplDataModule2(IBReplDataModule).Terminate;

      if TIBReplDataModule2(IBReplDataModule).Terminate then
        IBReplDataModule.IBReplicator.ReplLog.Log(SvcType, lchNull, sTerminating)
      else
        IBReplDataModule.IBReplicator.ReplLog.Log(SvcType, lchNull, sStopped);
    except
      on E: Exception do
      begin
        IBReplDataModule.IBReplicator.DBLog.Log(SvcType, lchError, E.Message);
        ExitCode:= 1;
      end;
    end;
  finally
    Debug('IBReplDataModule.Free', []);
    IBReplDataModule.Free;
  end;
  Debug('End of DoService()', []);
end;

procedure ServiceCtrlHandler(CtrlCode: DWord); stdcall;
var
  MappingPtr: ^TMappingRecord;
  LastError: DWORD;
begin
  Debug('ServiceCtrlHandler(%x)', [CtrlCode]);
  SvcCriticalSection.Enter;
  try
    Debug('MapViewOfFile(FILE_MAP_WRITE)', []);
    MappingPtr:= MapViewOfFile(FileMapping, FILE_MAP_WRITE, 0, 0, SizeOf(TMappingRecord));
    try
      if MappingPtr = nil then
        _RaiseLastWin32Error('MapViewOfFile');
      case CtrlCode of
        SERVICE_CONTROL_PAUSE:
          begin
            MappingPtr^.Pause:= True;
            ServiceStatus.dwCurrentState:= SERVICE_PAUSE_PENDING;
          end;
        SERVICE_CONTROL_CONTINUE:
          begin
            MappingPtr^.Pause:= False;
            ServiceStatus.dwCurrentState:= SERVICE_CONTINUE_PENDING;
          end;
        SERVICE_CONTROL_STOP:
          begin
            ServiceStatus.dwWin32ExitCode:= 0;
            ServiceStatus.dwCurrentState:= SERVICE_STOP_PENDING;
            MappingPtr^.Terminate:= True;
          end;
        SERVICE_CONTROL_INTERROGATE:
          begin  // just return service status
          end;
        else
      end;
      Debug('SetServiceStatus(%x)', [ServiceStatus.dwCurrentState]);
      if (ServiceStatusHandle <> 0) and not SetServiceStatus (ServiceStatusHandle, ServiceStatus) then
      begin
        LastError:= GetLastError;
        Writeln2(Format('SetServiceStatus(%x) error: %x, "%s"', [ServiceStatus.dwCurrentState, LastError, SysErrorMessage(LastError)]));   // do not call _RaiseLastWin32Error because is called also from console
      end;
    finally
      Debug('UnmapViewOfFile()', []);
      UnmapViewOfFile(MappingPtr);
    end;
  finally
    SvcCriticalSection.Leave;
  end;
end;

procedure ServiceMain(Argc: DWord; Argv: PLPSTR); stdcall;
begin
  Debug('ServiceMain()', []);
  IsService:= True;
  FillChar(ServiceStatus, SizeOf(ServiceStatus), 0);
  ServiceStatus.dwServiceType:= SERVICE_WIN32;
  ServiceStatus.dwControlsAccepted:= SERVICE_ACCEPT_STOP or SERVICE_ACCEPT_PAUSE_CONTINUE;

  Debug('RegisterServiceCtrlHandler("%s")', [SvcName]);
  ServiceStatusHandle:= RegisterServiceCtrlHandler(PChar(SvcName), @ServiceCtrlHandler);

  if ServiceStatusHandle = 0 then
    _RaiseLastWin32Error('RegisterServiceCtrlHandler');

// initialization
// Initialization complete - report running status.
  ServiceStatus.dwCurrentState:= SERVICE_RUNNING;
  ServiceStatus.dwCheckPoint:= 0;
  ServiceStatus.dwWaitHint:= 0;

  Debug('SetServiceStatus(%x)', [ServiceStatus.dwCurrentState]);
  if not SetServiceStatus (ServiceStatusHandle, ServiceStatus) then
    _RaiseLastWin32Error('SetServiceStatus');

  DoService;
  ServiceStatus.dwCurrentState:= SERVICE_STOPPED;
  Debug('SetServiceStatus(%x)', [ServiceStatus.dwCurrentState]);
  if not SetServiceStatus (ServiceStatusHandle,  ServiceStatus) then
    _RaiseLastWin32Error('SetServiceStatus');
end;

procedure WriteSIDInfo(aSID: PSID);
var
  Buff1, Buff2: array[1..512] of Char;
  N1, N2: DWORD;
  Use: SID_NAME_USE;
const
  UseStr: array[sidTypeUser..sidTypeUnknown] of string = ('User', 'Group', 'Domain', 'Alias', 'WellKnownGroup', 'DeletedAccount', 'Invalid', 'Unknown');
resourcestring
  sSid = 'Sid: %s Owner:"%s" Group:"%s"';
begin
  Debug('LookupAccountSid()', []);
  if not LookupAccountSid(SvcMachine, aSid, @Buff1, N1, @Buff2, N2, Use) then
    _RaiseLastWin32Error('LookupAccountSid');
  Writeln2(Format(sSid, [UseStr[Use], @Buff1, @Buff2]));
end;

type
  PPSID = ^PSID;
  PPSECURITY_DESCRIPTOR = ^PSECURITY_DESCRIPTOR;

  SetEntriesInAclProc = function(cCountOfExplicitEntries: ULONG; pListOfExplicitEntries: PEXPLICIT_ACCESS_;
         OldAcl: PACL; var NewAcl: ACL): DWORD; stdcall;
  GetNamedSecurityInfoProc = function (pObjectName: PAnsiChar; ObjectType: SE_OBJECT_TYPE;
         SecurityInfo: SECURITY_INFORMATION; ppsidOwner, ppsidGroup: PPSID; ppDacl, ppSacl: PACL;
         var ppSecurityDescriptor: PSECURITY_DESCRIPTOR): DWORD; stdcall;
  GetSecurityInfoProc = function (handle: THandle; ObjectType: SE_OBJECT_TYPE;
         SecurityInfo: SECURITY_INFORMATION; ppsidOwner, ppsidGroup: PPSID; ppDacl, ppSacl: PACL;
         var ppSecurityDescriptor: PPSECURITY_DESCRIPTOR): DWORD; stdcall;

var
  S: string;
  H, FileH, LibH: THandle;
  ServiceTableEntry: array[0..1] of TServiceTableEntry;
  MappingPtr: ^TMappingRecord;
  SucurityAttr: TSecurityAttributes;
  SecurityDescriptor: TSecurityDescriptor;
  SecurityDescriptorPtr: PSecurityDescriptor;
//  Access: EXPLICIT_ACCESS;
  Dacl, Sacl: ACL;
  SidOwner, SidGroup: PSID;
  P: Pointer;
resourcestring
  sAbout2 = 'IBReplication server running as NT service or console application';
  sHelp1 = 'Usage: ibreplserversvc [<name>] [/STATUS] [/START] [/PAUSE] [/CONTINUE] [/STOP] [/KILL] [/RESET] [/I:<inifile>] [/N:<machine>] [/INSTALL] [/U:<user>] [/P:<psw>] [/F:<file>] [/UNINSTALL] [/SILENT] [/SECURITY] [/O:<stdout>] [/H] [/?]';
  sHelp10 = 'Description:'#13#10+
           '   <name>     name of service, enable running more ibrepl servers, default: "default"'#13#10+
           '   /START     start processing, default, /START /PAUSE start service in pause, /START force start service'#13#10+
           '   /PAUSE     pause processing'#13#10+
           '   /CONTINUE  continue processing'#13#10+
           '   /STOP      stop process'#13#10+
           '   /KILL      kill process'#13#10+
           '   /STATUS    get status'#13#10+
           '   /RESET     reset status'#13#10+
           '   /INSTALL   install NT service'#13#10+
           '   <user>     service account "DomainName\Username", default: "LocalSystem"'#13#10+
           '   <psw>      service account password'#13#10+
           '   <machine>  NT service machine name'#13#10+
           '   <file>     shared status file'#13#10+
           '   /UNINSTALL uninstall NT service'#13#10+
           '   <inifile>  configuration file, default is IBREPL.INI in program directory'#13#10+
           '   /SILENT    no stdout output'#13#10+
           '   <stdout>   stdout file'#13#10+
           '   /SECURITY  print security info'#13#10+
           '   /? /H      print this help'#13#10;
  sHelp60= 'WARNING:'#13#10+
           'Long names containing spaces put as quoted parameter'#13#10+
           ''#13#10;

  sHelp70= 'Example:'#13#10+
           '  ibrteplserversvc my'#13#10+
           '  ibrteplserversvc my /STOP'#13#10+
           '  ibrteplserversvc my /STATUS'#13#10+
           '  ibrteplserversvc my /INSTALL /O:stdout.txt /F:ibrepl.1.pid'#13#10;

  sMasterNotRunning = 'Program not running';
  sErrorCreatingMapping = 'Error creating mapping';
  sProcessId = 'ProcessId: $%x';
  sAlreadyRunning = 'Cannot start, instance already running';
  sInstalling = 'Installing service "%s"';
  sUninstalling = 'Uninstalling service "%s"';
  sStarting = 'Starting service "%s"';
  sPaused = 'PAUSED';
  sSvcType = 'Instance name: %s';
  sCannotLoadLibrary = 'Cannot load library "%s"';

{$IFDEF REGISTRATION}
{$I RegClass.inc}
{$I RegIBReplicator.inc}
{$I RegCheckReg.inc}

{$IFNDEF SDK}
{$I RegIBReplicatorCheckPro.inc}
{$DEFINE REGISTRATION_CHECK_PRO}
{$ENDIF}
{$ENDIF}

begin
  WriteProgName;
  WriteLn2(sAbout2);
  WriteLn2('');
  if IsThereCmd('H', clUpcase) or IsThereCmd('?', clUpcase) then
  begin
    Writeln2(sHelp1);
    WriteLn2('');
    Writeln2(sHelp10);
    Writeln2(sHelp60);
    Writeln2(sHelp70);
    Exit;
  end;

  try
    Debug('Main()', []);
    {$IFDEF REGISTRATION_CHECK_PRO}
    Debug('Checking registration', []);
    TReg_CheckPro('NT service replicator');
    Debug('Done', []);
    {$ENDIF}

    S:= 'default';
    GetCmdString('', 0, S);
    SvcType:= S;
    WriteLn2(Format(sSvcType, [SvcType]));

    S:= '';
    GetCmdString('N:', clUpcase, S);
    if S = '' then
      SvcMachine:= nil
    else
      begin
        SvcMachine:= PChar(S);
        Debug('SvcMachine: %s', [S]);
      end;
    SvcName:= 'IBREPLSERVER-'+SvcType;

    ServiceStatusHandle:= 0;
    FillChar(MappingRecord, SizeOf(MappingRecord), 0);
    FileH:= INVALID_HANDLE_VALUE;  // = $FFFFFFFF
    SvcCriticalSection:= TCriticalSection.Create;
    Debug('OpenFileMapping("%s")', ['IBReplServer_'+UpperCase(SvcType)]);
    FileMapping:= OpenFileMapping(FILE_MAP_WRITE, False, PChar('IBReplServer_'+UpperCase(SvcType)));
    if (FileMapping = 0) and not (GetLastError() in [ERROR_FILE_NOT_FOUND, ERROR_INVALID_NAME]) then
      _RaiseLastWin32Error('OpenFileMapping');
    try
      if FileMapping <> 0 then
        begin
          CloseHandle(FileMapping);  // open handle have security problem accessing master's filemapping, MapViewOfFile returns nil
          Debug('CreateFileMapping("%s")', ['IBReplServer_'+UpperCase(SvcType)]);
          FileMapping:= CreateFileMapping(INVALID_HANDLE_VALUE, nil, PAGE_READWRITE, 0, SizeOf(MappingRecord), PChar('IBReplServer_'+UpperCase(SvcType)));
          if IsThereCmd('STATUS', clUpcase) then
            begin
              Debug('MapViewOfFile(FILE_MAP_READ)', []);
              MappingPtr:= MapViewOfFile(FileMapping, FILE_MAP_READ, 0, 0, SizeOf(TMappingRecord));
              if MappingPtr = nil then
                _RaiseLastWin32Error('MapViewOfFile');
              try
                Writeln2(Format(sProcessId, [MappingPtr^.ProcessId]));
                Writeln2(MappingPtr^.Status);
                if MappingPtr^.Pause then
                  Writeln2(sPaused);
              finally
                Debug('UnmapViewOfFile()', []);
                UnmapViewOfFile(MappingPtr);
              end;
            end
          else if IsThereCmd('STOP', clUpcase) then
            begin
              ServiceCtrlHandler(SERVICE_CONTROL_STOP);
            end
          else if IsThereCmd('PAUSE', clUpcase) and not IsThereCmd('START', clUpcase) then
            begin
              ServiceCtrlHandler(SERVICE_CONTROL_PAUSE);
            end
          else if IsThereCmd('CONTINUE', clUpcase) then
            begin
              ServiceCtrlHandler(SERVICE_CONTROL_CONTINUE);
            end
          else if IsThereCmd('RESET', clUpcase) then
            begin
              Debug('MapViewOfFile(FILE_MAP_WRITE)', []);
              MappingPtr:= MapViewOfFile(FileMapping, FILE_MAP_WRITE, 0, 0, SizeOf(TMappingRecord));
              if MappingPtr = nil then
                _RaiseLastWin32Error('MapViewOfFile');
              try
                Inc(MappingPtr^.Reset);
              finally
                Debug('UnmapViewOfFile()', []);
                UnmapViewOfFile(MappingPtr);
              end;
            end
          else if IsThereCmd('SECURITY', clUpcase) then
            begin
              LibH:= LoadLibrary('ADVAPI32.DLL');
              if LibH = 0 then
                _RaiseLastWin32Error('LoadLibrary');
              try
                P:= GetProcAddress(LibH, 'GetSecurityInfo');
                Debug('GetSecurityInfoProc()', []);
                if GetSecurityInfoProc(P)(
                    FileMapping,  //PChar('IBReplServer_'+UpperCase(SvcType)),
                    SE_KERNEL_OBJECT {SE_SERVICE },
                    DACL_SECURITY_INFORMATION or GROUP_SECURITY_INFORMATION or OWNER_SECURITY_INFORMATION or SACL_SECURITY_INFORMATION,
                    @SidOwner, @SidGroup, @Dacl, @Sacl, PPSECURITY_DESCRIPTOR(SecurityDescriptorPtr) { it's crazy pointer to pointer as var param ? }
                  ) <> ERROR_SUCCESS then
                  _RaiseLastWin32Error('GetSecurityInfoProc');
                try
                  WriteSIDInfo(SidOwner);
                  WriteSIDInfo(SidGroup);
                finally
                //   LocalFree(SecurityDescriptorPtr); // M according msdn nut LocalFree need handle
                end;
              finally
                FreeLibrary(LibH);
              end;
            end
          else if IsThereCmd('KILL', clUpcase) then
            begin
              Debug('MapViewOfFile(FILE_MAP_READ)', []);
              MappingPtr:= MapViewOfFile(FileMapping, FILE_MAP_READ, 0, 0, SizeOf(TMappingRecord));
              if MappingPtr = nil then
                _RaiseLastWin32Error('MapViewOfFile');
              try
                Debug('OpenProcess()', []);
                H:= OpenProcess(PROCESS_ALL_ACCESS, True, MappingPtr^.ProcessId);
                try
                  if not TerminateProcess(H, 0) then
                    _RaiseLastWin32Error('OpenProcess');
                finally
                  CloseHandle(H);
                end;
              finally
                Debug('UnmapViewOfFile()', []);
                UnmapViewOfFile(MappingPtr);
              end;
            end
          else
            raise Exception.Create(sAlreadyRunning);
        end
      else
        begin
          if IsThereCmd('STATUS', clUpcase) or IsThereCmd('STOP', clUpcase) or IsThereCmd('KILL', clUpcase) or IsThereCmd('RESET', clUpcase) or (IsThereCmd('PAUSE', clUpcase) and not IsThereCmd('START', clUpcase)) or IsThereCmd('CONTINUE', clUpcase) or IsThereCmd('SECURITY', clUpcase)  then
            begin
              raise Exception.Create(sMasterNotRunning);
            end;
          MappingRecord.ProcessId:= GetCurrentProcessId();

          if IsThereCmd('INSTALL', clUpcase) then
            begin
              Writeln2(Format(sInstalling, [SvcName]));
              InstallService(True);
            end
          else if IsThereCmd('UNINSTALL', clUpcase) then
            begin
              Writeln2(Format(sUninstalling, [SvcName]));
              InstallService(False)
            end
          else if IsThereCmd('START', clUpcase) and not IsThereCmd('PAUSE', clUpcase) then
            begin
              Writeln2(Format(sStarting, [SvcName]));
              CheckStartService(True);
            end
          else
            begin
              SucurityAttr.nLength:= SizeOf(SucurityAttr);
              SucurityAttr.bInheritHandle:= True;
              SucurityAttr.lpSecurityDescriptor:= @SecurityDescriptor;
              Debug('InitializeSecurityDescriptor()', []);
              if not InitializeSecurityDescriptor(SucurityAttr.lpSecurityDescriptor, SECURITY_DESCRIPTOR_REVISION) then
                _RaiseLastWin32Error('InitializeSecurityDescriptor()');
(*
              SetSecurityDescriptorControl(SucurityAttr.lpSecurityDescriptor, SE_DACL_AUTO_INHERIT_REQ or SE_SACL_AUTO_INHERIT_REQ, SE_DACL_AUTO_INHERIT_REQ or SE_SACL_AUTO_INHERIT_REQ);
              InitializeAcl(Dacl, SizeOf(Dacl), 1{ACL_REVISION});

              Access.grfAccessPermissions:= SECTION_ALL_ACCESS;
              Access.grfAccessMode:= GRANT_ACCESS;
              Access.grfInheritance:= NO_INHERITANCE;
              Access.Trustee.MultipleTrusteeOperation:= NO_MULTIPLE_TRUSTEE;
              // change these informations to grant access to a group or other user
              Access.Trustee.TrusteeForm:= TRUSTEE_IS_NAME;
              Access.Trustee.TrusteeType:= TRUSTEE_IS_USER;
              Access.Trustee.ptstrName:= 'CURRENT_USER';  // 'CREATOR OWNER'

              LibH:= LoadLibrary('ADVAPI32.DLL');
              if LibH = 0 then
                _RaiseLastWin32Error('LoadLibrary');
              try
                P:= GetProcAddress(LibH, 'SetEntriesInAclA');
                if P <> nil then
                  SetEntriesInAclProc(P)(1, @Access, nil, Dacl);
              finally
                FreeLibrary(LibH);
              end;
*)
              Debug('SetSecurityDescriptorDacl()', []);
              if not SetSecurityDescriptorDacl(SucurityAttr.lpSecurityDescriptor, True, nil {@Dacl{ nil = all access }, False) then
                _RaiseLastWin32Error('SetSecurityDescriptorDacl');

              S:= '';
              GetCmdString('F:', clUpcase or clValueCase, S);
              if S <> '' then
              begin
                Debug('CreateFile(%s)', [S]);
                FileH:= CreateFile(PChar(S), GENERIC_WRITE or GENERIC_READ, FILE_SHARE_READ or FILE_SHARE_WRITE{?}, @SucurityAttr, CREATE_ALWAYS, FILE_ATTRIBUTE_TEMPORARY or FILE_FLAG_DELETE_ON_CLOSE, 0);
                { file is inaccessable in WinXP, not enough rights, even after mapping is closed, probably created file is impossible to share }
                if FileH = INVALID_HANDLE_VALUE then
                  _RaiseLastWin32Error('CreateFile');
              end;
              Debug('CreateFileMapping("%s")', ['IBReplServer_'+UpperCase(SvcType)]);
              FileMapping:= CreateFileMapping(FileH, @SucurityAttr, PAGE_READWRITE, 0, SizeOf(MappingRecord), PChar('IBReplServer_'+UpperCase(SvcType)));
              if FileMapping = 0 then
                _RaiseLastWin32Error('CreateFileMapping');
              if CheckStartService(False) then
                begin
                  FillChar(ServiceTableEntry, SizeOf(ServiceTableEntry), 0);
                  ServiceTableEntry[0].lpServiceName:= PChar(SvcName);
                  ServiceTableEntry[0].lpServiceProc:= @ServiceMain;
                  Debug('StartServiceCtrlDispatcher(%s)', [SvcName]);
                  if not StartServiceCtrlDispatcher(ServiceTableEntry[0]) then
                    _RaiseLastWin32Error('StartServiceCtrlDispatcher');
                end
              else
                DoService;
            end;
        end;
    finally
      if FileMapping <> 0 then
        CloseHandle(FileMapping);
      if FileH <> INVALID_HANDLE_VALUE then
        CloseHandle(FileH);
      SvcCriticalSection.Free;
    end;
  except
    on E: Exception do
    begin
      Writeln2(E.Message);
      ExitCode:= 1;
    end;
  end;
  Debug('Bye', []);
end.

