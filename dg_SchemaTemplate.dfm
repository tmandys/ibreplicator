object SchemaTemplateDialog: TSchemaTemplateDialog
  Left = 227
  Top = 108
  BorderStyle = bsDialog
  Caption = 'Schema template'
  ClientHeight = 532
  ClientWidth = 695
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 120
  TextHeight = 16
  object Panel1: TPanel
    Left = 587
    Top = 0
    Width = 108
    Height = 532
    Align = alRight
    BevelOuter = bvNone
    Caption = ' '
    TabOrder = 1
    object OKBtn: TButton
      Left = 10
      Top = 10
      Width = 92
      Height = 31
      Caption = 'OK'
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
    object CancelBtn: TButton
      Left = 10
      Top = 59
      Width = 92
      Height = 31
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 1
    end
    object HelpBtn: TButton
      Left = 10
      Top = 108
      Width = 92
      Height = 31
      Caption = '&Help'
      TabOrder = 2
      OnClick = HelpBtnClick
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 587
    Height = 532
    Align = alClient
    BevelOuter = bvNone
    Caption = ' '
    TabOrder = 0
  end
  object Table: TIBDataSet
    Database = IBReplDataModule.IBCfgDB
    Transaction = IBReplDataModule.IBCfgTransaction
    BeforeEdit = TableBeforeEdit
    BeforeInsert = TableBeforeInsert
    BeforeOpen = TableBeforeOpen
    BeforePost = TableBeforePost
    OnNewRecord = TableNewRecord
    BufferChunks = 32
    CachedUpdates = False
    SelectSQL.Strings = (
      'SELECT * FROM'
      'FIELDS'
      '')
    Left = 501
    Top = 160
  end
  object DataSource1: TDataSource
    DataSet = Table
    OnUpdateData = DataSource1UpdateData
    Left = 520
    Top = 176
  end
end
