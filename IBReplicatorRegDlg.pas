unit IBReplicatorRegDlg;
{$DEFINE INTERFACE_USES}
{$DEFINE INTERFACE}
{$I RegDlg.inc}

implementation
uses
  {$IFDEF LINUX}
  {$ELSE}
  ComObj,
  {$ENDIF}
  {$IFDEF VER120}FileCtrl, {$ENDIF}{$IFDEF VER130}FileCtrl, {$ENDIF}
  {$IFDEF CLR}System.Text, System.Runtime.InteropServices, {$ENDIF}
  IBReplicatorDeploy;

{$DEFINE IMPLEMENTATION}
{$I RegDlg.inc}

end.

