//  Do not add this unit to any distribution, delete it after compilation


unit RegIBReplicatorDlg;

// do not add RegDlg into uses not to do potential potential conflict of implicitly included RegDlg units into package

{$DEFINE INTERFACE}
{$DEFINE INTERFACE_USES}
{$DEFINE INTERFACE_CheckRegistration}
{$I RegDlg.inc}

type
  TIBReplicatorRegistrationDialog = class(TRegistrationDialog)
    constructor Create(aOwner: TComponent); override;
  end;

procedure _CheckRegistration(aAlwaysShow: Boolean);

implementation
uses
  {$IFDEF VER120}FileCtrl, {$ENDIF}{$IFDEF VER130}FileCtrl, {$ENDIF}
  IBReplicator, IBReplicatorDeploy;

{$I RegIBReplicator.inc}
{$DEFINE IMPLEMENTATION}
{$I RegDlg.inc}

constructor TIBReplicatorRegistrationDialog.Create;
begin
  inherited;
  OnlineURL:= Reg_OnlineRegistration;
  OnlineURL2:= Reg_OnlineRegistration2;
  DisableOffline:= Reg_DisableOffline;
  Email:= Reg_Email;
  Homepage:= Reg_Homepage;
end;

procedure _CheckRegistration;
var
  L: Word;
begin
  if aAlwaysShow then
    L:= 0
  else
    L:= $C000;
  CheckRegistration(L, TIBReplicatorRegistrationDialog, {$IFDEF SDK}True{$ELSE}False{$ENDIF}, Reg_Prefix, Reg_ApplicationKey, Reg_ApplicationName, Reg_ApplicationId, Reg_Expiration, Reg_SecretKey, Reg_KeyFileName, L);
end;

end.




