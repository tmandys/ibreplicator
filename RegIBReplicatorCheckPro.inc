const
  Reg_parPro = 0;
  Reg_ProPeriod = 21;  // enable using temporary pro features when activated

function TReg_CheckPro(const aFeature: string): Integer;
var
  R: TReg_RegistrationK12;
  E: TReg_RegistrationException;
resourcestring
  sNoProVersion = '%s is available only in professional version';
begin
  Result:= -1;
  R:= TReg_RegistrationK12.Create(Reg_Prefix, Reg_ApplicationKey, Reg_SecretKey, Reg_KeyFileName);
  try
    R.Expiration:= Reg_Expiration;
    R.ApplicationName:= Reg_ApplicationName;
    R.ReadRegistration;
    if (R.State <> Reg_stOK) or (R.wParam2 and (1 shl Reg_parPro) = 0) and (Trunc(Now) > R.DateOfAct+Reg_ProPeriod) then
    begin
      E:= TReg_RegistrationException.CreateFmt(sNoProVersion, [aFeature]);
      E.State:= R.State;
      raise E;
    end;
    if R.wParam2 and (1 shl Reg_parPro) <> 0 then
      Result:= MaxInt
    else
      begin
        Result:= Trunc(R.DateOfAct+Reg_ProPeriod-Now);
        if Result = 0 then
          Inc(Result);      // last day is 0
      end;
  finally
    R.Free;
  end;
end;
